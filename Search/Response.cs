﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;

namespace CaseManager.Util
{
    public class Response
    {
        public static string json(int status, object data, string message)
        {
            return JsonConvert.SerializeObject(new
            {
                status = status,
                data = data,
                message = message
            });
        }

        public static string json(int status, object data)
        {
            return JsonConvert.SerializeObject(new
            {
                status = status,
                data = data
            });
        }

        public static string json(int status, string message)
        {
            return JsonConvert.SerializeObject(new
            {
                status = status,
                message = message
            });
        }
    }
}