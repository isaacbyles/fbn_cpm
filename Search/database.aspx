﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="database.aspx.cs" Inherits="CaseManager.Search.database" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link href="../assets/css/bootstrap.min.css" rel="Stylesheet" />
    <link href="../assets/css/metisMenu.min.css" rel="Stylesheet" />
    <link href="../assets/css/sb-admin-2.css" rel="Stylesheet" />
    <title>Get Database</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel panel-default">
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group">
                                <label>
                                    Table name</label>
                                <asp:TextBox ID="tablename" CssClass="form-control" type="text" placeholder="Table name"
                                    runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>
                                    First column</label>
                                <asp:TextBox ID="firstColumn" CssClass="form-control" type="text" placeholder="Suit No"
                                    runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>
                                    Second Column</label>
                                <asp:TextBox ID="secondColumn" CssClass="form-control" type="text" placeholder="Case Title"
                                    runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>
                                    Third Column</label>
                                <asp:TextBox ID="thirdColumn" CssClass="form-control" type="text" placeholder="Full Report Html"
                                    runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>
                                    Fourth column</label>
                                <asp:TextBox ID="fourthColumn" CssClass="form-control" type="text" placeholder="Summary"
                                    runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>
                                    Rank</label>
                                <asp:TextBox ID="rank" CssClass="form-control" type="text" placeholder="Rank" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="loginBtn" runat="server" Text="Query DB and Bild Lucene Index" CssClass="btn btn-lg btn-primary btn-block"
                                    OnClick="loginBtn_Click" />
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="searchText" CssClass="form-control" type="text" placeholder="Search for ..."
                                    runat="server"></asp:TextBox>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="searchButton" runat="server" Text="Search Index" CssClass="btn btn-lg btn-primary btn-block"
                                             />
                                        <asp:Label ID="timeField" runat="server" Text=""></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <asp:GridView ID="GridView1" runat="server" PageSize="10">
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="assets/js/jquery-1.11.1.min.js"></script>
<![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/metisMenu.min.js"></script>
    <script type="text/javascript" src="../assets/js/sb-admin-2.js"></script>
</body>
</html>
