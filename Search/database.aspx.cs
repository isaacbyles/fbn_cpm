﻿using System;
using System.Collections;
using Directory = Lucene.Net.Store.Directory;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.Documents;
using Lucene.Net.Analysis;
using Lucene.Net.Store;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;
using System.Data;
using System.Diagnostics;
using CaseManager.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using Lucene.Net.Search.Highlight;
using Newtonsoft.Json.Linq;


namespace CaseManager.Search
{
    public partial class database : System.Web.UI.Page
    {
        public static int startpage=0;
        FileInfo stopLocation = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "resources\\stop-words.txt");
        String indexLocation = AppDomain.CurrentDomain.BaseDirectory + "resources\\Lucene";
        String jsonLocation = AppDomain.CurrentDomain.BaseDirectory + "resources\\JSon.json";
        
        DataTable resulTtable = new DataTable();
        DataRow row;
       

        protected void loginBtn_Click(object sender, EventArgs e)
        {

           createIndex(Value);
          
        }

        public DataTable Value
        {

            get
            {
                DbRetrieve ret = new DbRetrieve();
                DataTable merge = new DataTable();
                merge = ret.getAllFirst().Copy();
                merge.Merge(ret.getAllSecond(), true, MissingSchemaAction.Ignore);
                merge.Merge(ret.getAllThird(), true, MissingSchemaAction.Ignore);

                return merge;

            }
        }

        public void createIndex(DataTable table)
        {

            var allthewords = Lucene.Net.Analysis.WordlistLoader.GetWordSet(stopLocation);
            var getTable = tablename.Text.Trim();
            Stopwatch time = new Stopwatch();
            time.Start();
            var directory = FSDirectory.Open(new System.IO.DirectoryInfo(indexLocation));

            using (Lucene.Net.Analysis.Snowball.SnowballAnalyzer analyze = new Lucene.Net.Analysis.Snowball.SnowballAnalyzer(Lucene.Net.Util.Version.LUCENE_30, "English", allthewords))
            using (var indexWriter = new IndexWriter(directory, analyze,new IndexWriter.MaxFieldLength(System.Int32.MaxValue)))
            {
               IndexTokenizer tokenize = new IndexTokenizer();

                tokenize.TokenStream("English", TextReader.Null);
                foreach (DataRow row in table.Rows)
                {
                    var doc = new Document();

                    doc.Add(new Field("mYear", row["mYear"].ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                    doc.Add(new Field("mNumber", row["mNumber"].ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                    doc.Add(new Field("mTitle", (row["mTitle"].ToString()), Field.Store.YES, Field.Index.ANALYZED));
                    doc.Add(new Field("mContent", row["mContent"].ToString(), Field.Store.YES, Field.Index.ANALYZED));
                    doc.Add(new Field("tablename", row["tablename"].ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                    doc.Add(new Field("rank", row["rank"].ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                    doc.Add(new Field("casetitle2", row["casetitle2"].ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                    doc.Add(new Field("identifier", row["identifier"].ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                    //doc.Add(new Field("Full Text", String.Format("{0}{1}{2}",row["case_title"], row["fullreport_html"], row["summary"]), Field.Store.NO, Field.Index.ANALYZED));

                    indexWriter.AddDocument(doc);


                }

                indexWriter.Optimize();
                indexWriter.Flush(true, true, true);
            }
            time.Stop();
            timeField.Text = "Total Time Elapsed in building index :" + time.Elapsed;

        }

        public List<SearchResult> search(string Word)
        {
           
            
            List<SearchResult> searchResult = new List<SearchResult>();
            Stopwatch time = new Stopwatch();
            time.Start();
            var allthewords = Lucene.Net.Analysis.WordlistLoader.GetWordSet(stopLocation);
            // var getTable = tablename.Text.Trim();
            Directory dir = FSDirectory.Open(new System.IO.DirectoryInfo(indexLocation));
            using (var reader = IndexReader.Open(dir, true))
            using (var searcher = new IndexSearcher(reader))
            {
                //
                //Analyzer analyze = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30, allthewords)
                using (Lucene.Net.Analysis.Snowball.SnowballAnalyzer analyze = new Lucene.Net.Analysis.Snowball.SnowballAnalyzer(Lucene.Net.Util.Version.LUCENE_30, "English", allthewords))
                {
                    var finalQuery = new BooleanQuery();
                    var queryParser = new MultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_30,
                        "casetitle2|mTitle|mContent".Split('|'), analyze);


                    #region 

//var terms = Word.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);
                    //foreach (var segment in terms)
                    //{
                    //    var term = new Term("mC, segment);
                    //    var fuzzyQuery = new FuzzyQuery(term, minimumSimilarity, prefixLength);
                    //    finalQuery.Add(fuzzyQuery, Occur.SHOULD);
                    //}

                    #endregion


                    
                   var query = queryParser.Parse("\"" + Word + "\"~3");

                    
                    var collector = TopScoreDocCollector.Create(System.Int16.MaxValue, true);

                    searcher.Search(query, collector);
                    int value = collector.TotalHits;
                    int sizepage = 30;


                    ScoreDoc[] matches = collector.TopDocs(startpage, sizepage).ScoreDocs;


                    var formatter =
                        new SimpleHTMLFormatter(
                            @"<span style=""background-color: #FFFF00; font-weight: bold; color: green;"">",
                            "</span>");
                    var fragmenter = new SimpleFragmenter(100);
                    var scorer = new QueryScorer(query);
                    Highlighter highlighter = new Highlighter(formatter, scorer);
                    highlighter.TextFragmenter = fragmenter;

                    foreach (var item in matches)
                    {
                        List<string> res = new List<string>();
                        var docId = item.Doc;
                        var doc = searcher.Doc(docId);
                        float score = item.Score;

                        var content = doc.Get("mContent");
                        var stream = analyze.TokenStream("", new StringReader(content));
                        var sample = highlighter.GetBestFragments(stream, content, 8);
                        res.AddRange(sample);

                        String frag = "";

                        var arr = res.ToArray();
                        for (int k = 0; k < arr.Length; k++)
                        {
                            if (res[0] != null)
                            {
                                frag = res[0];
                            }
                            if (arr.Length > 1)
                            {
                                frag = frag + @"<span style="" font-weight: bold; color: blue;"">......   </span>" +
                                       res[k];
                            }

                        }
                        SearchResult result = new SearchResult();

                        result.mYear = doc.Get("mYear");
                        result.mTitle = doc.Get("mTitle");
                        result.mNumber = doc.Get("mNumber");
                        result.mContent = doc.Get("mContent");
                        result.summary = frag;
                        result.rank = doc.Get("rank");
                        result.tablename = doc.Get("tablename");
                        result.casetitle2 = doc.Get("casetitle2");
                        result.identifier = doc.Get("identifier");
                        result.score = score;
                        result.totalHits = value;

                        searchResult.Add(result);
                    }

                }

                reader.Flush();
                searcher.Dispose();
            }

            dir.Dispose();

            return searchResult;
        }


        public void TableColumn()
        {
            DataColumn column;

            //creating colums
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "s/n";
            resulTtable.Columns.Add(column);

            //creating colums
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Suit No";
            resulTtable.Columns.Add(column);

            //creating colums
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Case Title";
            resulTtable.Columns.Add(column);

            
            //creating colums
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Full Report";
            resulTtable.Columns.Add(column);

            //creating colums
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Summary";
            resulTtable.Columns.Add(column);

            //creating colums
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Rank";
            resulTtable.Columns.Add(column);

            //creating colums
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Table Name";
            resulTtable.Columns.Add(column);

            //creating colums
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Score";
            resulTtable.Columns.Add(column);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //TableColumn();
        }

        public List<SearchResult> searchDetails(string Word)
        {
            List<SearchResult> searchResult = new List<SearchResult>();

            var allthewords = Lucene.Net.Analysis.WordlistLoader.GetWordSet(stopLocation);
            Directory dir = FSDirectory.Open(new System.IO.DirectoryInfo(indexLocation));
            using (var reader = IndexReader.Open(dir, true))
            using (var searcher = new IndexSearcher(reader))
            {
                using (Lucene.Net.Analysis.Snowball.SnowballAnalyzer analyze = new Lucene.Net.Analysis.Snowball.SnowballAnalyzer(Lucene.Net.Util.Version.LUCENE_30, "English", allthewords))
                {
                    //IndexTokenizer tokenize = new IndexTokenizer();
                    var queryParser = new MultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_30, "casetitle2|mTitle|mContent".Split('|'), analyze);
                    var query = queryParser.Parse("\"" + Word + "\"~3");


                    var collector = TopScoreDocCollector.Create(System.Int16.MaxValue, true);
                    searcher.Search(query, collector);

                    int value = collector.TotalHits;
                    ScoreDoc[] matches = collector.TopDocs().ScoreDocs;

                    foreach (var item in matches)
                    {
                        List<string> res = new List<string>();
                        var docId = item.Doc;
                        var doc = searcher.Doc(docId);



                        // model calss
                        searchResult.Add(new SearchResult()
                        {
                            mYear = doc.Get("mYear"),
                            mTitle = doc.Get("mTitle"),
                            tablename = doc.Get("tablename"),

                        });


                    }
                    
                }
                reader.Flush();
                searcher.Dispose();

            }

           
            dir.Dispose();
            return searchResult;
        }



        
    }
}