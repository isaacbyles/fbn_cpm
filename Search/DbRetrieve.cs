﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CaseManager.Search
{
    public class DbRetrieve
    {
        public DataTable getAllFirst()
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKNEW"]);
            conn.Open();
            string query = " SELECT [year] as mYear,[suitno] as mNumber,[legal head] as mTitle,str(pk) as identifier,[subjectmatter] as mTitle2,[principle]as mContent ,'analysis' as tablename ,(select case_title2 from cases where suitno = analysis.suitno) as casetitle2 ,'1' as rank FROM [stknew].[dbo].[analysis] union all "+
" SELECT [year] as mYear,[suitno] as mNumber,[legal head] as mTitle,str(pk) as identifier,[subjectmatter] as mIssues,[principle]as mContent ,'analysis_ca' as tablename,(select case_title2 from cases where suitno = analysis_ca.suitno) as casetitle2 ,'2' as rank FROM [stknew].[dbo].[analysis_ca] union all " +
" SELECT [year] as mYear,[suitno] as mNumber,[legal head] as mTitle,str(pk) as identifier,[subjectmatter] as mIssues,[principle]as mContent ,'analysis_fhc' as tablename,(select case_title2 from cases where suitno = analysis_fhc.suitno) as casetitle2 ,'3' as rank FROM [stknew].[dbo].[analysis_fhc] union all " +
" SELECT [year] as mYear,[suitno] as mNumber,[legal head] as mTitle,str(pk) as identifier,[subjectmatter] as mIssues,[principle]as mContent ,'analysis_nic' as tablename ,(select case_title2 from cases where suitno = analysis_nic.suitno) as casetitle2,'4' as rank FROM [stknew].[dbo].[analysis_nic] union all " +
" SELECT [year] as mYear,[suitno] as mNumber,[legal head] as mTitle,str(pk) as identifier,[subjectmatter] as mIssues,[principle]as mContent ,'analysis_states'as  tablename,(select case_title2 from cases where suitno = analysis_states.suitno) as casetitle2 ,'3' as rank FROM [stknew].[dbo].[analysis_states] union all " +
" SELECT [year] as mYear,[suitno] as mNumber,[legal head] as mTitle,str(pk) as identifier,[subjectmatter] as mIssues,[principle]as mContent ,'analysis_tat' as tablename,(select case_title2 from cases where suitno = analysis_tat.suitno) as casetitle2,'4' as rank FROM [stknew].[dbo].[analysis_tat] union all " +
" SELECT [date] as mYear,[suitno] as mNumber,[case_title] as mTitle,suitno as identifier,[summary] as mIssues ,[fullreport_html] as mContent,'cases' as tablename ,[case_title] as casetitle2 ,'5' as rank FROM [stknew].[dbo].[cases] ";
            //string query = "SELECT " + suitno + "," + casetitle +"," + fullreport_html + ","+summary+" From "+tableName +"";
            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataAdapter rd = new SqlDataAdapter(cmd);
            rd.Fill(ds);
            return ds.Tables[0];

        }


        public DataTable getAllSecond()
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKNEW"]);
            conn.Open();
            string query =
" SELECT [date] as mYear,[suitno] as mNumber,[case_title] as mTitle,suitno as identifier,[summary] as mIssues ,[fullreport] as mContent,'cases_ca' as tablename ,[case_title] as casetitle2 ,'6' as rank FROM [stknew].[dbo].[cases_ca] union all " +
" SELECT [date] as mYear,[suitno] as mNumber,[case_title] as mTitle,suitno as identifier,[summary] as mIssues ,[fullreport] as mContent,'cases_fhc' as tablename ,[case_title] as casetitle2 ,'7' as rank FROM [stknew].[dbo].[cases_fhc] union all " +
" SELECT [date] as mYear,[suitno] as mNumber,[case_title] as mTitle,suitno as identifier,[summary] as mIssues ,[fullreport] as mContent,'cases_nic' as tablename ,[case_title] as casetitle2 ,'8' as rank FROM [stknew].[dbo].[cases_nic] ";
            //string query = "SELECT " + suitno + "," + casetitle +"," + fullreport_html + ","+summary+" From "+tableName +"";
            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataAdapter rd = new SqlDataAdapter(cmd);
            rd.Fill(ds);
            conn.Close();
            return ds.Tables[0];

        }


        public DataTable getAllThird()
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKNEW"]);
            conn.Open();
            string query =
" SELECT [date] as mYear,[suitno] as mNumber,[case_title] as mTitle,suitno as identifier,[summary] as mIssues ,[fullreport_html] as mContent,'cases_states' as tablename ,[case_title] as casetitle2 ,'7' as rank FROM [stknew].[dbo].[cases_states]  union all " +
" SELECT [date] as mYear,[suitno] as mNumber,[case_title] as mTitle,suitno as identifier,[summary] as mIssues ,[fullreport_html] as mContent,'cases_tat' as tablename ,[case_title] as casetitle2 ,'8' as rank FROM [stknew].[dbo].[cases_tat] union all " +
" SELECT STR(year)as mYear,[section] as mNumber,[lfntitle] as mTitle,[court] as mIssues ,[section_Content] as mContent, str(pk) as identifier, 'laws_sections' as tablename ,[lfntitle] as casetitle2, '9' as rank FROM [stknew].[dbo].[laws_sections] union all " +
" SELECT STR(year)as mYear,[section] as mNumber,[lfntitle] as mTitle,[court] as mIssues ,[section_Content] as mContent, str(pk) as identifier, 'laws_sections' as tablename ,[lfntitle] as casetitle2, '10' as rank FROM [stknew].[dbo].[subsidiaries_legislations] union all " +
" select STR(a.year) as mYear, c.section_number as mNumber, a.title as mTitle, c.section_description as mIssues, c.content as mContent, 'CPR' as tablename, str(c.order_id) as identifier,'9' as rank, a.title as casetitle2 from rules a join orders_table b on a.rule_id =b.rule_id join sections c on b.order_id = c.order_id ";
            //string query = "SELECT " + suitno + "," + casetitle +"," + fullreport_html + ","+summary+" From "+tableName +"";
            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataAdapter rd = new SqlDataAdapter(cmd);
            rd.Fill(ds);
            conn.Close();
            return ds.Tables[0];

        }
    }
}