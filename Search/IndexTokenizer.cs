﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using System.Text;
using Lucene.Net.Analysis.Tokenattributes;
using System.IO;
using Lucene.Net.Search;
using Lucene.Net.Search.Highlight;

namespace CaseManager.Search
{
    public class IndexTokenizer : Analyzer
    {
        //public  override TokenStream TokenStream(TextReader reader)
        //{
        //    return new PorterStemFilter(new StandardTokenizer(Lucene.Net.Util.Version.LUCENE_30, reader));
        //}

        public override TokenStream TokenStream(string fieldName, TextReader reader )
        {
            return new PorterStemFilter(new StandardTokenizer(Lucene.Net.Util.Version.LUCENE_30, reader));
        }
    }
}
