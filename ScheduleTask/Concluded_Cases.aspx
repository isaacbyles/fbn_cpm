﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Concluded_Cases.aspx.cs" Inherits="CaseManager.ScheduleTask.Concluded_Cases" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <meta charset="utf-8" content="">
    <%-- <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="-1"/>--%>


    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="cache-control" content="must-revalidate" />
    <meta http-equiv="cache-control" content="proxy-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="pragma" content="no-cache" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <link href="../assets/css/bootstrap.css" rel="Stylesheet" />
    <link href="../assets/css/metisMenu.min.css" rel="Stylesheet" />
    <link href="../assets/css/timeline.css" rel="Stylesheet" />
    <link href="../assets/css/font-awesome.min.css" rel="Stylesheet" />
    <link href="../assets/css/jquery.datetimepicker.css" rel="Stylesheet" />
    <link href="../assets/css/sb-admin-2.css" rel="Stylesheet" />
    <link href="../assets/css/fullcalendar.min.css" rel="Stylesheet" />
    <link href="../assets/css/processtree.css" rel="Stylesheet" />
    <link href="../assets/css/jqtree.css" rel="Stylesheet" />
    <link href="../assets/css/formbuilder-min.css" rel="Stylesheet" />
    <link href="../assets/css/jquery.dataTables.min.css" rel="Stylesheet" />
    <link href="../LawPavilion/Assets/css/style.css" rel="Stylesheet" />



    <style type="text/css">
        .navbar-top-links > li > a {
            color: #EEBA47;
        }
        /* .btn{
    white-space:normal !important;
    word-wrap:break-word; 
}*/
    </style>
    <!--[if lt IE 9]>
    
    <script>
    els = ['lpelr','cms','stk','total','nav'];
    for(i=0; i < els.length; i++){
        document.createElement(els[i]);
      }
      </script>
      <![endif]-->
    <title></title>
</head>



<body>
    <form id="form1" runat="server">
        <div>

            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="height: 52px; margin-bottom: 0; background-color: #034267;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img id="logo" src="../assets/images/logo.png" />
                    </a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">

                    <li><a href="#" class="ext-link" id="stk-link"><i class="fa fa-briefcase fa-fw"></i>LawPavilion Solicitors' Toolkit</a></li>
                    <li><a href="#" class="ext-link" id="lpelr-link"><i class="fa fa-gavel fa-fw"></i>LawPavilion Electronic Law Reports</a></li>
                    <li><a href="#" class="ext-link" id="cms-link"><i class="fa fa-desktop fa-fw"></i>FBN Case Manager</a></li>

                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="user_name"></span>
                            <i class="fa fa-user fa-fw"></i><i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <%-- <li><a id="all_requests" href="#"><i class="fa fa-tasks fa-fw"></i> All Requests</a>
                        </li>--%>
                            <%--<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>--%>



                            <li><a href="../logout.aspx" id="logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <stk id="stk-1"></stk>
                <lpelr id="lpelr-1"></lpelr>


                <cms>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav">
				<br>

					<div class="input-group input-group-sm" >
					 
					  <span class="input-group-btn">
						<button class="btn btn-success" id='refreshbtn' type="button" style="background-color: #034267; border-color: #034267; margin-left:20px;"><i class="fa fa-refresh"></i> Refresh</button>
					  </span>
					</div><!-- /input-group -->

					<br>
                    <ul class="nav menu_pane scrollbar" id="mainside-menu">

                        <li class="active">
                            <a class="side-menu-link" href="" onclick="javascript:window.location.reload();"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a>
                        </li>

                        <li id="perfection-menu">
                            <a class="collapsed" href="javascript:;" data-toggle="collapse" data-target="#perfection-collapse"><i class='fa fa-arrows-h'></i> Perfection &amp; Securities <span class="caret"></span></a>
                            <ul class="collapse" id="perfection-collapse"></ul>
                        </li>

                        <li id="corporate-menu">
                            <a class="collapsed" href="javascript:;" data-toggle="collapse" data-target="#corporate-collapse"><i class='fa fa-arrows-h'></i> Corporate &amp; Contracts <span class="caret"></span></a>
                            <ul class="collapse" id="corporate-collapse">
                               
                            </ul>
                        </li>

                        <li id="litigation-menu">
                            <a class="collapsed" href="javascript:;" data-toggle="collapse" data-target="#litigation-collapse"><i class='fa fa-arrows-h'></i> Litigation <span class="caret"></span></a>
                            <ul class="collapse" id="litigation-collapse"></ul>
                        </li>

                        <li id="bond-menu">
                            <a class="collapsed" href="javascript:;" data-toggle="collapse" data-target="#bond-collapse"><i class='fa fa-arrows-h'></i> Bonds & Guarantees <span class="caret"></span></a>
                            <ul class="collapse" id="bond-collapse"></ul>
                        </li>
                        
                        <li id="print-menu">
                            <a class="collapsed" href="javascript:;" data-toggle="collapse" data-target="#print-collapse"><i class='fa fa-arrows-h'></i> Templates <span class="caret"></span></a>
                            <ul class="collapse" id="print-collapse"></ul>
                        </li>

                          <li id="hub-menu">
                            <a href="javascript:;" class="collapsed" data-toggle="collapse" data-target="#hub-collapse"><i class='fa fa-arrows-h'></i> Hub Coordination Requests<span class="caret"></span></a>
                             <ul class="collapse" id="hub-collapse">
                                 <li>
                                    <a href="javascript:;" id="abuja-region" ><i class='fa fa-arrows-h'></i> Abuja Region Requests</a>
                                </li>
                                 <li>
                                    <a href="javascript:;" id="enugu-region"><i class='fa fa-arrows-h'></i>Enugu Region Requests</a>
                                </li>
                                 <li>
                                    <a href="javascript:;" id="PH-region" ><i class='fa fa-arrows-h'></i> Port-Harcourt Requests</a>
                                </li>
                                 
                                  <li>
                                    <a href="javascript:;" id="kaduna-region" ><i class='fa fa-arrows-h'></i> Kaduna Requests</a>
                                </li>
                                 
                                  <li>
                                    <a href="javascript:;" id="benin-region" ><i class='fa fa-arrows-h'></i> Benin Requests</a>
                                </li>
                                  <li>
                                    <a href="javascript:;" id="ibadan-region" ><i class='fa fa-arrows-h'></i> Ibadan Requests</a>
                                </li>
                                 
                                  <li>
                                    <a href="javascript:;" id="hubCordination" ><i class='fa fa-arrows-h'></i> All Hub Requests</a>
                                </li>
                          </ul>
                        </li>

                         <li id="completed-menu">
                            <a href="javascript:;" id="all_requests"><i class='fa fa-arrows-h'></i> All Completed Requests </a>
                            
                        </li>
                         <li id="all-ongoing-menu">
                            <a href="javascript:;" id="all_ongoing_requests"><i class='fa fa-arrows-h'></i> All Ongoing Requests </a>
                            
                        </li>
                        <li id="branchcompleted-menu">
                            <a href="javascript:;" id="branchall_requests"><i class='fa fa-arrows-h'></i> Branch Completed Requests </a>
                            
                        </li>
                        
                        <li id="report_rem" >
                            <a class="side-menu-link" id="report_link" href="#"><i class="fa fa-bar-chart"></i> <span>Reports</span></a>
                        </li>

                            <li >
                            <a class="side-menu-link"  href="../ScheduleTask/Schedule.aspx"><i class="fa fa-bar-chart"></i> <span>Schedule</span></a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
            </cms>
                <div>
                    <p class="navbar-text" style="color: White; font-size: 12px;">
                        <asp:Label ID="currentUserDesc" runat="server"></asp:Label>

                    </p>
                </div>
            </nav>




            <div class="page-wrapper" style="min-height: 800px; padding-bottom: 60px;">

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           
                        </h1>

                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <!-- /.row -->
                <div id="dashboard_panel" class="row">


                    <div class="panel panel-primary">
                        <input type="hidden" id="region" />
                        <div class="panel-heading">
                            <b id='page-title' class='text-uppercase'>Upload Concluded Cases</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div id="user_page" class="panel-body">

                            <asp:FileUpload ID="FileUpload1" runat="server" />
                            <br />
                            <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn btn-success" OnClick="btnUpload_Click" />

                        </div>
                        <!-- /.col-lg-4 -->
                    </div>
                    <!-- /.row -->
                </div>






            </div>

            <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation" style="height: 52px; background-color: #0a0a0a;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img style="height: 30px;" src="../assets/images/official-logo.png" />
                    </a>
                </div>
                <!-- /.navbar-header -->
            </nav>






        </div>
    </form>
</body>
</html>
