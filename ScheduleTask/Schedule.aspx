﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dasboard.Master" AutoEventWireup="true" CodeBehind="Schedule.aspx.cs" Inherits="CaseManager.ScheduleTask.Schedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageTitle" runat="server">

    <div class="row">
        <div class="col-lg-12">
        </div>
        <!-- /.col-lg-12 -->
    </div>


    <div id="" class="row" style="margin-top: 20px;">


        <div class="panel panel-primary">
            <input type="hidden" id="region" />
            <div class="panel-heading">
                <b id='page-title' class='text-uppercase'>DASHBOARD</b>
            </div>
            <!-- /.panel-heading -->
            <div id="" class="panel-body">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="container2" id="uniquereports">
                            <div class="row">
                                <div class="dropdown col-sm-3">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        LITIGATION UNIT
                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                        <li><a href="Consolidated.aspx" >Consolidated Litigation Schedule</a></li>

                                        <li><a href="#">Monthly Concluded Date Schedule</a></li>
                                        <li><a href="#">Garnishee and Mareva Schedule</a></li>
                                        <li><a href="#">Monthly Cost of Litigation Schedule</a></li>
                                        <li><a href="#">Probate Portfolio</a></li>
                                        <li><a href="#">External Solicitors’ Schedule</a></li>

                                    </ul>
                                </div>
                                <div class="dropdown col-sm-3">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        HUB COORDINATION
                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">

                                        <li><a repid="35" class="ajax-link3" href="#" data_url="../Components/upload_report/hub_coordination/hc2.htm">Report of Actual Payments on Litigation</a></li>
                                        <li><a repid="36" class="ajax-link3" href="#" data_url="../Components/upload_report/hub_coordination/hc3.htm">New Garnishee Schedule</a></li>
                                        <li><a repid="37" class="ajax-link3" href="#" data_url="../Components/upload_report/hub_coordination/hc4.htm">New Cases Returns</a></li>
                                        <li><a repid="38" class="ajax-link3" href="#" data_url="../Components/upload_report/hub_coordination/hc5.htm">Concluded Cases Report</a></li>
                                        <li><a repid="39" class="ajax-link3" href="#" data_url="../Components/upload_report/hub_coordination/hc6.htm">Appeal Cases Report</a></li>
                                    </ul>
                                </div>
                                <div class="dropdown col-sm-3">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        CORPORATE
                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">

                                        <li><a repid="45" class="ajax-link3" href="#" data_url="../Components/upload_report/corporate/cc3.htm">In-House Requisition and Advisory Report</a></li>

                                        <li><a repid="1051" class="ajax-link3" href="#" data_url="../Components/upload_report/corporate/cc5.htm">TradeMark Schedule</a></li>
                                        <li><a repid="42" class="ajax-link3" href="#" data_url="../Components/upload_report/corporate/cc6.htm">Perfection Schedule Bank Properties</a></li>
                                    </ul>
                                </div>
                                <div class="dropdown col-sm-3">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                        PERFECTION
                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                        <li><a repid="1053" class="ajax-link3" href="#" data_url="../Components/upload_report/perfection/ps1.htm">Search on Title Deeds</a></li>
                                        <li><a repid="42" class="ajax-link3" href="#" data_url="../Components/upload_report/perfection/ps2.htm">Perfection Schedule Security Document</a></li>
                                        <li><a repid="43" class="ajax-link3" href="#" data_url="../Components/upload_report/perfection/ps3.htm">Universal Perfection Schedule</a></li>
                                        <li><a repid="1053" class="ajax-link3" href="#" data_url="../Components/upload_report/perfection/ps5.htm">Deeds of Release</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="reporter" class="panel-body">
                        <label class="text-info text-justify">Select a Report Type</label>
                    </div>

                </div>
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
    </div>




    <link rel="stylesheet" href="../assets/css/styles.css" />
    <script src="../assets/js/jquery-1.11.3.js"></script>
    <script type="text/javascript">
        $(function () {
            /******************** Handle Link Clicks ****************************/
            $("#dashboard_panel").hide();
            $("#dashboard_panel").remove();
            $('.ajax-link3').click(function (event) {
                event.preventDefault();
                var das = $(this).attr('data_url');

                var repid = $(this).attr('repid');
                PageHandler.Resources.reportid = repid;
                // Lets Fetch Data First

                das += "?rand=" + randomString() + "&user=" + UserHandler.user.id + "&repid=" + repid;
                $('#reporter').empty().html("<div class='loading'>Loading ... please wait</div>");

                $('#reporter').load(das);
            });


            /******************** Handle Link Clicks ****************************/
        });
    </script>

    <!--  -->
</asp:Content>
