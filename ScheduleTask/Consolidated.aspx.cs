﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CaseManager.ScheduleTask
{
    public partial class Consolidated : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string folderPath = Server.MapPath("~/Files/");

            //Check whether Directory (Folder) exists.
            if (!Directory.Exists(folderPath))
            {
                //If Directory (Folder) does not exists. Create it.
                Directory.CreateDirectory(folderPath);
            }

            //Save the File to the Directory (Folder).
            string extension = System.IO.Path.GetExtension(Request.Files["FileUpload1"].FileName.ToLower());
            string connectionString = "";
            string[] validFileType = { ".xls", ".xlsx" };
            string filePath = string.Format("{0}/{1}", Server.MapPath("~/Files"), Request.Files["FileUpload1"].FileName);
            FileUpload1.SaveAs(folderPath + Path.GetFileName(FileUpload1.FileName));

            if (validFileType.Contains(extension))
            {
                if (extension == ".xls")
                {
                    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    System.Data.DataTable dt = Util.Utility.UploadContents(filePath, connectionString);
                    int i = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        string sol_id = dr[0].ToString();
                        string sol_name = dr[1].ToString();
                        string sol_name1 = dr[2].ToString();
                        string sol_name2= dr[3].ToString();
                        string sol_name3 = dr[4].ToString();
                        string sol_name4 = dr[5].ToString();
                        string sol_name5 = dr[6].ToString();
                        string sol_name6 = dr[7].ToString();
                        //ConsolidatedLitigationTbl tbl = new ConsolidatedLitigationTbl();
                       // tbl.
                   

                        //branch branch = new branch();
                        //branch.id = Convert.ToInt32(sol_id);
                        //branch.name = sol_name;
                        //branch.region = "";
                        //branch.state = "";
                        //branch.street = "";
                        //branch.town = "";
                        //branch.active_fg = true;
                        //branch.type = "branch";
                        //branch.country = "Nigeria";

                        //DAL dal = new DAL();
                        //bool store = dal.SaveBranch(branch);

                        i++;


                    }

                }

                /*

                if (Request.Files["file"].ContentLength > 0)
                {
                    string extension = System.IO.Path.GetExtension(Request.Files["file"].FileName.ToLower());
                    string connectionString = "";

                    string[] validFileType = { ".xls", ".xlsx" };

                    string filePath = string.Format("{0}/{1}", Server.MapPath("~/Content/Uploads"), Request.Files["file"].FileName);
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/Contents/Uploads"));
                    }

                    if (validFileType.Contains(extension))
                    {

                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        }
                        Request.Files["file"].SaveAs(filePath);

                        if (extension == ".xls")
                        {
                            connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                            DataTable dt = Utility.UploadContents(filePath, connectionString);
                            int i = 0;
                            foreach (DataRow dr in dt.Rows)
                            {
                                string sol_id = dr[0].ToString();
                                string sol_name = dr[1].ToString();

                                branch branch = new branch();
                                branch.id = Convert.ToInt32(sol_id);
                                branch.name = sol_name;
                                branch.region = "";
                                branch.state = "";
                                branch.street = "";
                                branch.town = "";
                                branch.active_fg = true;
                                branch.type = "branch";
                                branch.country = "Nigeria";

                                DAL dal = new DAL();
                                bool store = dal.SaveBranch(branch);

                                i++;


                            }

                            ViewBag.Uploaded = "Branch Excel Uploaded";
                        }

                    }
                    else
                    {
                        ViewBag.Error = "Please upload an .xls or .xlsx file";
                    }
                }
                 */
            }
        }

    }
}