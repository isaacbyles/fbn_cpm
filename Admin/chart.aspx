﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="chart.aspx.cs" Inherits="CaseManager.Admin.chart" %>

<!DOCTYPE html>
<html>
<head>

    <link href="../assets/css/bootstrap.min.css" rel="Stylesheet" />
    <link href="../assets/css/metisMenu.min.css" rel="Stylesheet" />
    <link href="../assets/css/timeline.css" rel="Stylesheet" />
    <link href="../assets/css/font-awesome.min.css" rel="Stylesheet" />
    <title></title>
    <style>
        .setFocus{
            fill:red;
        }
        .view{
            position: absolute;
        }
        #drawing{
            z-index: 1000;
        }
        #detail
        {
            width:23%;
            height:auto;
            
            float:right;
        }
        .detail_box
        {
            background-color:rgb(185, 218, 245);
            min-height:30px;
            padding:8px;
            margin-bottom:5px;
        }
    </style>
</head>
<body>
<form id="form1" runat="server">
<div id="drawing" style="width:75%;height:780px" class="view"></div>
<div  class="view" style="width:75%;height:780px">
    <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg">
        <defs>
        <marker id="Triangle"
                    viewBox="0 0 10 10"
                    refX="1" refY="5"
                    markerWidth="6"
                    markerHeight="6"
                    orient="auto">
                <path d="M 0 0 L 10 5 L 0 10 z" />
            </marker>
            <pattern id="smallGrid" width="4" height="4" patternUnits="userSpaceOnUse">
                <path class="cube" d="M 4 0 L 0 0 0 4" fill="none" stroke="gray" stroke-width="0.5"/>
            </pattern>
            <pattern id="grid" width="80" height="80" patternUnits="userSpaceOnUse">
                <rect width="80" height="80" fill="url(#smallGrid)"/>
                <path d="M 80 0 L 0 0 0 80" fill="none" stroke="gray" stroke-width="1"/>
            </pattern>
        </defs>

        <rect width="100%" height="100%" fill="url(#grid)" />
    </svg>
</div>
<div id="detail">
    <div class="detail_box">
        <div class="selectedItem">Hover on circles to reveal identity</div>
    </div>
    <br />
    <div class="detail_box">
        <h4>Workflow Map</h4>
        <div class="map"></div>
    </div>
    <br />
    <div>
    <button type="button" class="btn btn-default" onclick="javascript:window.location.reload();">Refresh Design</button>
    <button id="btn_can_draw" type="button" class="btn btn-info">Link Stages</button>
    <button id="save_workflow_btn" type="button" class="btn btn-primary">Save Workflow</button>
    </div>
    <br />
    <div class="alert alert-info">
        <h3>Instruction</h3>
        <ol>
            <li>Before commencing the design, ensure you have a draft of the design</li>
            <li>Drag circles to the design space. Ensure each circle is well placed based on your draft</li>
            <li>When you are done with stage 3, click on the "Link Stages" button</li>
            <li>Double tap circles to select them one after the other in the order</li>
            <li>In the case of branching, double-click on the design space, and repeat step 4 till no more branching is required </li>
        </ol>
    </div>
    
</div>
</form>
 <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
 <script type="text/javascript" src="../assets/js/PageHandler.js"></script>
<script type="text/javascript" src="../assets/js/svg.min.js"></script>
<script type="text/javascript" src="../assets/js/svg.draggable.min.js"></script>
<script type="text/javascript">
    (function () {
        //window.location.reload(); // To refresh the view for a newly selected process
        var id = window.location.hash.substr(window.location.hash.indexOf('#') + 1);
        var selectedItem = $(".selectedItem");
        var btn_can_draw = $("#btn_can_draw");
        var save_workflow_btn = $("#save_workflow_btn");
        btn_can_draw.on('click', function (e) {
            can_draw = !can_draw;
            if (can_draw) {
                btn_can_draw.html("Stop Linking");
                btn_can_draw.removeClass("btn-danger").addClass("btn-info");
            } else {
                btn_can_draw.html("Link Stages");
                btn_can_draw.addClass("btn-danger").removeClass("btn-info");
            }

        });

        var can_draw = false;
        var map = $(".map");
        if (id.trim().length <= 0) { return; }
        var _w = {};
        var _t = function () {
            var tobj = {
                end_point: null,
                start_point: null,
                line: null
            }
            return tobj;
        }


        var rects = {};
        var draw = SVG('drawing');
        var precedence = [], buffer = [];
        var connectionLines = {};
        var linesBuffer = {};
        var Po = function () {
            this.from = null;
            this.to = null;
        }
        var XPoint = function () {
            this.x = 0;
            this.y = 0;
        };
        var _p1 = new XPoint();
        var _p2 = new XPoint();

        //var roles = ["Admin", "HOD", "RM", "AM", "FM"];
        var w = 500, h = 50, padding = 100;
        var objIDMap = {};

        var data = PageHandler.getProcessStages(id, function (response) {

            if (response.status && response.data.status == 1) {
                var dataRoles = {};
                var dataStages = {};
                var dataStageRoles = {};
              
                var yaxis_count = 0;
                var xaxis_count = 1;
                $.each( response.data.data, function(i, v){ 
                //response.data.data.forEach(function (v, i) {
                    dataStages[v.id] = v;
                    if (!(v.role_id in dataRoles)) {
                        dataRoles[v.role_id] = v.role_name;
                    }
                    if (!(v.role_id in dataStageRoles)) {
                        dataStageRoles[v.role_id] = [];
                    }
                    dataStageRoles[v.role_id].push(v);
                    var rect = draw.circle(60).center(40 + xaxis_count, 65 * (yaxis_count + 1)).style({ fill: "#f06", cursor: "move" }).attr("title", v.role_name);
                    objIDMap[rect.node.id] = v.id;
                    if (yaxis_count > 4) {
                        xaxis_count += 80;
                        yaxis_count = 0;
                    } else {
                        yaxis_count += 1;
                    }
                    rect.draggable();
                    //rect.node.onclick = function(e){console.log(e)};
                    rect.node.onmouseover = function (e) {
                        //rects[e.target.id].style({ fill: "#f06", cursor: "move" });
                        //console.log(dataStages);
                        selectedItem.html("Role:<b> " + dataStages[objIDMap[e.target.id]].role_name + "</b><br/>Stage Name : <b>" + dataStages[objIDMap[e.target.id]].name + "</b>");
                    };
                    rect.node.onmouseout = function (e) {
                        //rects[e.target.id].style({ fill: "#000", cursor: "default" });
                        selectedItem.html("Hover on circles to reveal identity");
                    };
                    rect.on('dragmove.namespace', function (e) {
                        if (!can_draw) { return }
                        rects[e.target.id].style({ fill: "#f06", cursor: "move" });
                        if (buffer.length == 1) {
                            _p1 = new XPoint();
                            _p2 = new XPoint();
                            _p1.x = rects[buffer[0]].node.cx.baseVal.value;
                            _p1.y = rects[buffer[0]].node.cy.baseVal.value;

                            _p2.x = rects[e.target.id].node.cx.baseVal.value;
                            _p2.y = rects[e.target.id].node.cy.baseVal.value;

                            if (e.target.id in _w) {
                                //console.log('in');
                                updateLines(e.target.id, _p2.x, _p2.y);
                            } else {
                                //console.log('out');
                                var line = drawline(draw, _p1, _p2);
                                _w[e.target.id] = [];
                                _w[buffer[0]] = [];
                                var np = new _t();
                                var np2 = new _t();
                                np.line = line;
                                np.end_point = _p1;
                                np.start_point = _p2;
                                _w[e.target.id].push(np);
                                np2.line = line;
                                np2.end_point = _p2;
                                np2.start_point = _p1;
                                _w[buffer[0]].push(np2);
                                connectionLines[e.target.id + "_" + buffer[0]] = line;
                            }
                        }
                    });
                    rect.on('dragend.namespace', function (e) {
                        if (!can_draw) { return }
                        if (buffer.length >= 2) {
                            buffer = [];
                        }
                        showMap();
                    });
                    rect.on('dragstart.namespace', function (e) {
                        if (!can_draw) { return }
                        //console.log(rects[e.target.id].node.cx.baseVal.value);
                        //console.log(rects[e.target.id].node.cy.baseVal.value);
                        if (buffer.indexOf(e.target.id) < 0) {
                            buffer.push(e.target.id);
                            rects[e.target.id].fill('#f06');
                            if (buffer.length >= 2) {
                                var p = new Po();
                                _p2.x = rects[e.target.id].node.cx.baseVal.value;
                                _p2.y = rects[e.target.id].node.cy.baseVal.value;
                                p.from = buffer[0];
                                p.to = buffer[1];
                                buffer = [];
                                precedence.push(p);
                                //console.log('here');
                                var _id = (p.from + "_" + p.to in connectionLines) ? p.from + "_" + p.to :
                        (p.to + "_" + p.from in connectionLines) ? p.to + "_" + p.from : null;
                                if (_id != null) {
                                    if (_id in connectionLines) {
                                        var l = connectionLines[_id];
                                        //console.log('here 3');
                                        l.plot(_p1.x, _p1.y, _p2.x, _p2.y);
                                        //console.log('drawn');

                                    } else {
                                        //console.log("id is not registered");
                                    }
                                } else {
                                    //console.log("id is null");
                                    //console.log('out');
                                    var line = drawline(draw, _p1, _p2);
                                    _w[e.target.id] = [];
                                    _w[buffer[0]] = [];
                                    var np = new _t();
                                    var np2 = new _t();
                                    np.line = line;
                                    np.end_point = _p1;
                                    np.start_point = _p2;
                                    _w[e.target.id].push(np);
                                    np2.line = line;
                                    np2.end_point = _p2;
                                    np2.start_point = _p1;
                                    _w[buffer[0]].push(np2);
                                    connectionLines[e.target.id + "_" + buffer[0]] = line;
                                }

                                //connectionLines[p.from+"_"+p.to] = drawline(draw,_p1, _p2);
                                removeFocus();
                                _p1 = new XPoint();
                                _p2 = new XPoint();
                            } else {
                                _p1.x = rects[e.target.id].node.cx.baseVal.value;
                                _p1.y = rects[e.target.id].node.cy.baseVal.value;
                            }
                        }

                    });
                    rects[rect.node.id] = rect;


                });
            }


            draw.node.ondblclick = function (e) {
               
                resetSelections();
            }
            draw.plain("Stages").center(30, 10);
            //console.log(draw);

            function resetSelections() {
                for (var s in rects) {
                    rects[s].fill('#000');
                }
                buffer = [];
                //precedence = [];
            }
            function removeFocus() {
                for (var s in rects) {
                    rects[s].fill('#000');
                }
            }
            function clearIdleLines() {
                var d = null;
                var svg = document.querySelector('svg');
                for (var l in linesBuffer) {
                    //linesBuffer[l].clear();
                    // d = document.getElementById(l);
                    document.body.removeChild(linesBuffer[l].target);
                    //console.log(d);
                }
            }
            function updateLines(id, x, y) {
                if (id in _w) {
                    //console.log('start drawing');
                    for (var i = 0; i < _w[id].length; i++) {
                        //console.log('drawing...');
                        _w[id][i].line.plot(_w[id][i].end_point.x, _w[id][i].end_point.y, x, y);
                        _w[id][i].start_point.x = x;
                        _w[id][i].start_point.y = y;
                    }
                    //console.log('stop drawing');
                }
            }
            function showMap() {

                map.html("<ol>");
                for (var d in precedence) {
                    map.append("<li>From <b>" + dataStages[objIDMap[precedence[d].from]].name + "</b> to <b>" + dataStages[objIDMap[precedence[d].to]].name + "</b></li>");
                }
                map.append("<ol>");
            }
            function drawline(draw, origin, destination) {
                var dy = destination.y - origin.y;
                var dx = destination.x - origin.x;
                var delta = 100 * Math.floor(Math.sqrt((dy * dy) - (dx * dx)));

                return draw.line(origin.x - delta, origin.y - delta, destination.x - delta, destination.y - delta).stroke({ width: 2 }).attr('fill', '#f06').attr("marker-end", "url(#Triangle)");
            }
        });
        save_workflow_btn.on('click', function (e) {
            //console.log(precedence);
            if (confirm("Are you sure you have completed the process setup. Saved and Published processes cannot be altered. PLEASE NOTE!!!")) {
                var payload = [];
                for (var d in precedence) {
                    var t = new Po();
                    t.from = objIDMap[precedence[d].from];
                    t.to = objIDMap[precedence[d].to];
                    payload.push(t);
                }
                if (payload.length > 0) {
                    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Process.ashx?a=set_workflow", { process_id: id, stage_precedence: JSON.stringify(payload) }, PageHandler.HTTPMethods.POST, function (response) {
                        
                        if (response.status && response.data.status == 1) {
                            alert("Workflow created successfully. Close the window.");
                            window.parent.location.reload();
                        } else {
                            alert("Workflow not created. Refresh the page and try again. #Possible reason:" + response.data.message);
                        }
                        //window.close();
                    });
                }
            }
        });
    } ());
</script>
</body>
</html>