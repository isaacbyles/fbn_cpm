﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CaseManager.Util;
using CaseManager.Logic;
using System.Data;
using CaseManager.Model;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Web.Caching;
using Logger = CaseManager.HttpServerTest.Logger.Logger;

namespace CaseManager
{

    public partial class index : System.Web.UI.Page
    {
        private AuditLogic au = new AuditLogic();
        public HttpContext context = HttpContext.Current;
        private Object _userID;
        readonly AuditLogic audit = new AuditLogic();
        private HttpCookie cookie; private HttpCookie cookie2;
        protected void Page_Load(object sender, EventArgs e)
        {
           
           Response.ClearHeaders();

            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");

            Response.AppendHeader("Expires", "-1");
            HttpContext.Current.Session.Clear();
            TempStorage.TheData = HttpContext.Current.Request.Url.AbsoluteUri;

        }

        protected void proceedBtn_Click(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;
            //Response.Redirect("~/RM/", true);
        }

        protected void loginBtn_Click(object sender, EventArgs e)
        {

            List<string> keys = new List<string>();


            // retrieve application Cache enumerator
            IDictionaryEnumerator enumerator = Cache.GetEnumerator();


            // copy all keys that currently exist in Cache
            while (enumerator.MoveNext())
            {
                keys.Add(enumerator.Key.ToString());
            }


            // delete every key from cache
            for (int i = 0; i < keys.Count; i++)
            {
                Cache.Remove(keys[i]);
            }


            HttpContext.Current.Session.Clear();

            if (email.Text.Trim().Length > 0 && password.Text.Trim().Length > 0)
            {

                #region

                //---------active directory login-starts---
                //ActiveDIrectory ad = new ActiveDIrectory();
                //string adEmail = ad.AuthenticateUser(email.Text.Trim());
                //if (adEmail == "false")
                //{
                //    status_message.CssClass = "alert alert-danger";
                //    alert_message.Text = "Invalid Credentials. Verify and try again";
                //    return;
                //}
                // -------active directory login -ends---------

                #endregion

                #region

                UserLogic ul = new UserLogic();
                DataSet ds = ul.login(email.Text.Trim().ToLower(), password.Text.Trim());

                #endregion

                #region

                //WebService web = new WebService();

                //////Staff No and Password
                //string adWebService = web.Authenticate(email.Text.Trim().ToString());

                //if (adWebService == "false")
                //{
                //    status_message.CssClass = "alert alert-danger";
                //    alert_message.Text = "Invalid Credentials. Verify and try again";
                //    return;
                //}
                //DataSet ds = ul.login(adWebService.Trim(), "123456".Trim());

                //Logger.writelog("dataset response" + " " + ds.ToString());
                #endregion

                //active directory login
                // DataSet ds = ul.login(adEmail.ToLower(), "123456");
                //Logger.LogError(adEmail);
                if (ds != null)
                {

                    #region

                   

                    cookie = HttpContext.Current.Request.Cookies["ID"];

                    if (cookie == null)
                    {
                        HttpContext.Current.Response.Redirect("~/logout.aspx", true);
                    }
                    if (_userID == null)
                    {
                        _userID = cookie.Value;
                       
                    }
                   // IPStorage.SaveIp(_userID.ToString(), SystemInformation.GetIPAddress()); // storage IP Address into DB
                 
                    DateTime tds = DateTime.Now;
                    string _username = UsersClass.GetName(_userID.ToString());
                    bool res= audit.SaveAudit(_username, "User Login : " + "<b>[" + "User logged in to Case Manager" + "]</b>", "Login", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
                    #endregion

                    MultipleRoles mr = new MultipleRoles();
                    DataTable multiCheck = mr.hasMultiple(ds.Tables[0].Rows[0].ItemArray[0].ToString());

                    if (multiCheck == null || multiCheck.Rows.Count < 2)
                    {
                        Response.Redirect("~/RM/", true);

                    }
                    else
                    {
                        NameValueCollection secRoles = new NameValueCollection();
                        foreach (DataRow rowR in multiCheck.Rows)
                        {
                            secRoles.Add(rowR["role_id"].ToString(), rowR["name"].ToString());
                        }
                        WebExtensions.RedirectWithData(secRoles, "SecRoles/default.aspx");

                        //Response.Redirect("~/SecRoles/", true);
                        //Server.Transfer("~/SecRoles/default.aspx");
                    }

                 
                }
                else if(ds == null)
                {
                 
                    //save data of unsuccessful login
                    DateTime dts = DateTime.Now;
                    bool save_error = au.SaveAudit("No User ID",
                        "Unsuccessful Login :  Username Entered : <b>" + email.Text.Trim() + "</b>", "Login",
                        SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
                    status_message.CssClass = "alert alert-danger";
                    alert_message.Text = "Invalid Email or Password. Verify and try again  <br/>";   
                  
                }
            }
            else
            {
             
                alert_message.Text = "Empty fields not allowed";
                status_message.CssClass = "alert alert-danger";  
                //Server.Transfer("~/default.aspx");
            }

        }
    }
}