﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="CaseManager.index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">   
<head id="Head1" runat="server">
    <meta charset="utf-8" content="" />
   
     <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="cache-control" content="must-revalidate" />
    <meta http-equiv="cache-control" content="proxy-revalidate" />
    <meta http-equiv="expires" content="-1" />
   
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link href="assets/css/bootstrap.css" rel="Stylesheet" />
    <link href="assets/css/metisMenu.min.css" rel="Stylesheet" />
    <link href="assets/css/sb-admin-2.css" rel="Stylesheet" />
    

    <title>Law Pavilion :: Case Manager</title>
  
</head>
<body >

    <form id="form1" runat="server">
    <div>
         <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
            
                <div class="login-panel panel panel-default">
                <img width="100%" src="assets/images/logo.png" />
                    <div class="panel-heading">
                        
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <asp:Panel ID="status_message" CssClass="alert hidden" runat="server">
                            <asp:Label ID="alert_message" runat="server" Text="Label"></asp:Label>
                        </asp:Panel>
                            <div>
                           
                            <fieldset>
                                <div class="form-group">
                                    <label>Username</label>
                                    <asp:TextBox ID="email" CssClass="form-control" type="text" placeholder="username" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <asp:TextBox ID="password" CssClass="form-control" type="password" 
                                        placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                               
                                <!-- Change this to a button or input when using this as a form -->
                                <asp:Button ID="loginBtn" runat="server" Text="Login" 
                                    CssClass="btn btn-lg btn-primary btn-block" onclick="loginBtn_Click" />

                            </fieldset>

                        <div class='text-center'><br><br>
                            <small>Powered By</small>
                            <img style='height:60px;' src="assets/images/official-logo.png" />
                        </div>
                                
                                
                              <%--  <a href="#" onclick="getIPAddress()">Cleck to get IP Address</a>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>



   <%-- <div class="modal fade" id="sec_modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-danger" id="sec-title">Multiple Roles Detected</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="Div2" class="page_alert alert hide"></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Select a role to proceed</h4>
                        <div class="form-group" id="role_radio"></div>
            
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-success">Proceed</button>-->
                <asp:Button ID="proceedBtn" runat="server" Text="Proceed" CssClass="btn btn-success" />
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
  </div>
</div>
--%>
        </div>


    </form>
	  
    <script type="text/javascript" src="assets/js/jquery-1.11.3.js"></script>
 
    <script type="text/javascript" src="assets/js/metisMenu.min.js"></script>
    <script type="text/javascript" src="assets/js/sb-admin-2.js"></script>
   
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/html5shiv.js"></script>
    <script type="text/javascript" src="assets/js/respond.js"></script>
  
    
<%--    

<script type="text/javascript">
    function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs
        //compatibility for firefox and chrome
        var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
        var pc = new myPeerConnection({
            iceServers: []
        }),
        noop = function () { },
        localIPs = {},
        ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
        key;

        function iterateIP(ip) {
            if (!localIPs[ip]) onNewIP(ip);
            localIPs[ip] = true;
        }

        //create a bogus data channel
        pc.createDataChannel("");

        // create offer and set local description
        pc.createOffer().then(function (sdp) {
            sdp.sdp.split('\n').forEach(function (line) {
                if (line.indexOf('candidate') < 0) return;
                line.match(ipRegex).forEach(iterateIP);
            });

            pc.setLocalDescription(sdp, noop, noop);
        }).catch(function (reason) {
            // An error occurred, so handle the failure to connect
        });

        //listen for candidate events
        pc.onicecandidate = function (ice) {
            if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
            ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
        };
    }

    // Usage

    getUserIP(function (ip) {
        alert("Got IP! :" + ip);
    });


    var getIPAddress = function () {
        $.getJSON("https://jsonip.com?callback=?", function (data) {
            alert("Your IP Address is :- " + data.ip);
        });
    };
</script>--%>
  
</body>
 
     <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="cache-control" content="must-revalidate" />
    <meta http-equiv="cache-control" content="proxy-revalidate" />
    <meta http-equiv="expires" content="-1" />
  
    <meta http-equiv="pragma" content="no-cache" />

</html>
