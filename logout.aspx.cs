﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CaseManager.Util;
using CaseManager.Logic;

namespace CaseManager
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditLogic au = new AuditLogic();
            UserLogic ul = new UserLogic();
            
            ul.logout();

            Response.Redirect("~/", true);
        }
    }
}