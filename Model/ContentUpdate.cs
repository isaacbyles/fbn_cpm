﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Model
{
    public class ContentUpdate
    {
    }

    [Serializable]
    public class Contents
    {
        public List<FormContent> TheContent { get; set; }
    }

    
    public class FormContent
    {
        public string ID { get; set; }
        public string Value { get; set; }
        public string Form_Control_Id { get; set; }
    }
}