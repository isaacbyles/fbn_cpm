﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Form : Base
    {
        #region Constructors
        public Form()
        {

        }

        public Form(string name, string description, short status = Model.Status.ACTIVE)
        {
            this.Name = name;
            this.Description = description;
            this.Status = status;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
        }
        #endregion

        #region Data Members
        private int id;
        private string name;
        private string description;
        private DateTime created_date;
        private DateTime modified_date;
        private short status;
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"INSERT INTO [form]
(name, description, created_date, modified_date, status)
OUTPUT INSERTED.ID 
VALUES (@name, @description, @created_date, @modified_date, @status)";
        public const string SQL_UPADTE = "UPDATE [form] SET name=@name, description=@description, created_date=@created_date, modified_date=@modified_date, status=@status WHERE id = @id";
        #endregion

        #region Properties
        public int Id { get { return this.id; } set { this.id = value; } }
        public string Name { get { return this.name; } set { this.name = Text.trimmer.Replace(value.ToString().ToLower().Trim(), " "); } }
        public string Description { get { return this.description; } set { this.description = value.ToString().Trim(); } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public DateTime ModifiedDate { get { return this.modified_date; } set { this.modified_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@name"] = new Parameter(this.name, Parameter.ParameterType.STRING);
            param["@description"] = new Parameter(this.description, Parameter.ParameterType.STRING);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }

        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@id"] = new Parameter(this.id, Parameter.ParameterType.INT); 
            param["@name"] = new Parameter(this.name, Parameter.ParameterType.STRING);
            param["@description"] = new Parameter(this.description, Parameter.ParameterType.STRING);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_UPADTE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Property Changing Methods
        public void editDetails(string name, string description)
        {
            this.Name = name;
            this.Description = description;
            this.ModifiedDate = DateTime.Now;
        }

        public void changeStatus(short status)
        {
            this.Status = status;
            this.ModifiedDate = DateTime.Now;
        }
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[form]");

            if (filter.ContainsKey("id"))
            {
                qb.where("id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("name"))
            {
                qb.where("name= '%' + @name + '%'");
                param["@name"] = new Parameter(Text.trimmer.Replace(filter["name"].ToString().ToLower().Trim(), " "), Parameter.ParameterType.STRING);
            }

            if (filter.ContainsKey("status"))
            {
                qb.where("status=@status");
                param["@status"] = new Parameter(filter["status"], Parameter.ParameterType.INT);
            }
            else
            {
                qb.where("status=@status");
                param["@status"] = new Parameter(Model.Status.ACTIVE, Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static Form ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Form f = new Form();
            f.Id = dt.Rows[0].Field<int>("id");
            f.Name = dt.Rows[0].Field<string>("name");
            f.Description = dt.Rows[0].Field<string>("description");
            f.CreatedDate= dt.Rows[0].Field<DateTime>("created_date");
            f.ModifiedDate = dt.Rows[0].Field<DateTime>("modified_date");
            f.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());

            f.is_new = false;
            return f;
        }
        #endregion
    }
}