﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Model
{
    public class External_Solicitor
    {
        public int id { get; set; }

        public string FIRM { get; set; }
        public string  Email { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        public string Fax { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string role_id  { get; set; }
    }
}