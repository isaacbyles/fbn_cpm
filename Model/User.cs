﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
  
    public class User : Base
    {
        #region Constructors
        public User()
        {

        }

        public User (string email, string password, int branch_id, short role_id)
        {
            this.Email = email;
            this.Password = password;
            this.BranchId = branch_id;
            this.RoleId = role_id;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
            this.Status = Model.Status.INACTIVE;
        }
        #endregion

        #region Data Members
        private int id;
        private string email;
        private string password;
        private int branch_id;
        private short role_id;
        private DateTime created_date;
        private DateTime modified_date;
        private short status;
        #endregion

        #region Properties
        public int Id { get { return this.id; } set { this.id = value; } }
        public string Email { get { return this.email; } set { this.email = value.ToString().ToLower().Trim(); } }
        public string Password { set { this.password = PasswordGen.generate(value.ToString()); } }
        public int BranchId { get { return this.branch_id; } set { this.branch_id = value; } }
        public short RoleId { get { return this.role_id; } set { this.role_id = value; } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public DateTime ModifiedDate { get { return this.modified_date; } set { this.modified_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
        public short Rid { get { return this.status; } set { this.status = value; } }
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"
INSERT INTO [user]
(email, password, branch_id, role_id, created_date, modified_date, status) 
OUTPUT INSERTED.ID 
VALUES (@email, @password, @branch_id, @role_id, @created_date, @modified_date, @status)
";
        public const string SQL_UPADTE = "UPDATE [user] SET email=@email, :password branch_id=@branch_id, role_id=@role_id, created_date=@created_date, modified_date=@modified_date, status=@status WHERE id = @id";
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@email"] = new Parameter(this.email, Parameter.ParameterType.STRING);
            param["@password"] = new Parameter(this.password, Parameter.ParameterType.STRING);
            param["@branch_id"] = new Parameter(this.branch_id, Parameter.ParameterType.INT);
            param["@role_id"] = new Parameter(this.role_id, Parameter.ParameterType.INT);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(User.SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }
        }

        protected override void _update()
        {
            string sql = User.SQL_UPADTE;
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@id"] = new Parameter(this.id, Parameter.ParameterType.INT);
            param["@email"] = new Parameter(this.email, Parameter.ParameterType.STRING);

            if (this.password == null)
            {
                sql = sql.Replace(":password", "");
            }
            else
            {
                sql = sql.Replace(":password", "password=@password, ");
                param["@password"] = new Parameter(this.password, Parameter.ParameterType.STRING);
            }
            param["@branch_id"] = new Parameter(this.branch_id, Parameter.ParameterType.INT);
            param["@role_id"] = new Parameter(this.role_id, Parameter.ParameterType.INT);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Property Changing Methods
        public void changePassword(string password, short status = Model.Status.ACTIVE)
        {
            this.Password = password;
            this.ModifiedDate = DateTime.Now;
            this.Status = status;
        }

        public void changePriviledge(int branch_id, short role_id)
        {
            this.BranchId = branch_id;
            this.RoleId = role_id;
            this.ModifiedDate = DateTime.Now;
        }

        public void changeStatus(short status)
        {
            this.Status = status;
            this.ModifiedDate = DateTime.Now;
        }
        #endregion

        #region Fetch Methods
        public static DataSet fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[user]");
            qb.innerJoin("profile", "[user].id = profile.user_id");
            qb.innerJoin("role", "[user].role_id = role.id");
            qb.innerJoin("branch", "[user].branch_id = branch.id");
            qb.where("branch.active_fg = 1 AND role.active_fg = 1");

            qb.columns(
                "[user].id",
                "[user].email",
                "[user].branch_id",
                "[user].role_id",
                "[user].created_date",
                "[user].modified_date",
                "[user].status",
                "profile.firstname",
                "profile.lastname",
                "profile.middlename",
                "profile.telephone",
                "profile.created_date AS profile_created_date",
                "profile.modified_date AS profile_modified_date",
                "profile.status AS profile_status",
                "branch.name AS branch_name",
                "role.name AS role_name"
                );

            if (filter.ContainsKey("email"))
            {
                qb.where("[user].email=@email");
                param["@email"] = new Parameter(filter["email"], Parameter.ParameterType.STRING);
            }

            if (filter.ContainsKey("password"))
            {
                qb.where("[user].password=@password");
                param["@password"] = new Parameter(filter["password"], Parameter.ParameterType.STRING);
            }

            if (filter.ContainsKey("role_id"))
            {
                qb.where("[user].role_id=@role_id");
                param["@role_id"] = new Parameter(filter["role_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("branch_id"))
            {
                qb.where("[user].branch_id=@branch_id");
                param["@branch_id"] = new Parameter(filter["branch_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("id"))
            {
                qb.where("[user].id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("status"))
            {
                qb.where("[user].status=@status");
                param["@status"] = new Parameter(filter["status"], Parameter.ParameterType.INT);
            }
            else if (!filter.ContainsKey("all_status"))
            {
                qb.where("[user].status IN (1,2)");
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();


            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                return result;
            }
            return null;
        }

        public static User ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            User b = new User();

            b.Id = dt.Rows[0].Field<int>("id");
            b.Email = dt.Rows[0].Field<string>("email");
            b.BranchId = dt.Rows[0].Field<int>("branch_id");
            b.RoleId = short.Parse(dt.Rows[0].Field<object>("role_id").ToString());
            b.CreatedDate = dt.Rows[0].Field<DateTime>("created_date");
            b.ModifiedDate = dt.Rows[0].Field<DateTime>("modified_date");
            b.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());

            Profile p = new Profile();
            p.UserId = b.Id;
            p.FirstName = dt.Rows[0].Field<string>("firstname");
            p.LastName = dt.Rows[0].Field<string>("lastname");
            p.MiddleName = dt.Rows[0].Field<string>("middlename");
            p.Telephone = dt.Rows[0].Field<string>("telephone");
            p.CreatedDate = dt.Rows[0].Field<DateTime>("profile_created_date");
            p.ModifiedDate = dt.Rows[0].Field<DateTime>("profile_modified_date");
            p.Status = short.Parse(dt.Rows[0].Field<object>("profile_status").ToString());

            b.is_new = false;
            return b;
        }


        public static User ToObjects(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            User b = new User();

            b.Id = dt.Rows[0].Field<int>("id");
            b.Email = dt.Rows[0].Field<string>("email");
            b.BranchId = dt.Rows[0].Field<int>("branch_id");
            b.RoleId = short.Parse(dt.Rows[0].Field<object>("role_id").ToString());
            b.CreatedDate = dt.Rows[0].Field<DateTime>("created_date");
            b.ModifiedDate = dt.Rows[0].Field<DateTime>("modified_date");
            b.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());

            External_Solicitor p = new External_Solicitor();
            p.id = b.Id;
            p.fname = dt.Rows[0].Field<string>("FirstName");
            p.lname = dt.Rows[0].Field<string>("LastName");
            p.FIRM = dt.Rows[0].Field<string>("FIRM");
            p.Telephone = dt.Rows[0].Field<string>("TEL");
            //p.Email = dt.Rows[0].Field<string>("EMAIL");
            p.Address = dt.Rows[0].Field<string>("ADDRESS");
         //   p.Status = short.Parse(dt.Rows[0].Field<object>("profile_status").ToString());

            b.is_new = false;
            return b;
        }
        #endregion
    }
}