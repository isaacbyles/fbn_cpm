﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class FormControlData : Base
    {
        #region Constructors
        public FormControlData()
        {

        }

        public FormControlData(int form_control_id, string data)
        {
            this.FormControlId = form_control_id;
            this.Data = data;
            this.Status = Model.Status.ACTIVE;
        }

        public FormControlData(int form_control_id, Dictionary<string, string> pack)
        {
            this.FormControlId = form_control_id;
            this.Data = pack["data"];
            this.Status = Model.Status.ACTIVE;
        }
        #endregion

        #region Data Members
        private int id;
        private int form_control_id;
        private string data;
        private short status;
        #endregion

        #region Properties
        public int Id { get { return this.id; } set { this.id = value; } }
        public int FormControlId { get { return this.form_control_id; } set { this.form_control_id = value; } }
        public string Data { get { return this.data; } set { this.data = value.ToString().Trim(); } }
        public short Status { get { return this.status; } set { this.status = value; } }
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"INSERT INTO [form_control_data]
(form_control_id, data, status)
OUTPUT INSERTED.ID 
VALUES (@form_control_id, @data, @status)";
        public const string SQL_UPADTE = "UPDATE [form_control_data] SET form_control_id=@form_control_id, data=@data, status=@status WHERE id = @id";
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@form_control_id"] = new Parameter(this.form_control_id, Parameter.ParameterType.INT);
            param["@data"] = new Parameter(this.data, Parameter.ParameterType.STRING);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }
        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@id"] = new Parameter(this.id, Parameter.ParameterType.INT);
            param["@form_control_id"] = new Parameter(this.form_control_id, Parameter.ParameterType.INT);
            param["@data"] = new Parameter(this.data, Parameter.ParameterType.STRING);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_UPADTE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Property Changing Methods
        public void changeData(string data)
        {
            this.Data = data;
        }

        public void changeStatus(short status)
        {
            this.Status = status;
        }
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[form_control_data]");

            if (filter.ContainsKey("id"))
            {
                qb.where("id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("form_control_id"))
            {
                qb.where("form_control_id=@form_control_id");
                param["@form_control_id"] = new Parameter(filter["form_control_id"], Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static FormControlData ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            FormControlData f = new FormControlData();
            f.Id = dt.Rows[0].Field<int>("id");
            f.FormControlId = dt.Rows[0].Field<int>("form_control_id");
            f.Data = dt.Rows[0].Field<string>("data");
            f.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());

            f.is_new = false;
            return f;
        }
        #endregion
    }
}