﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Ref : Base
    {
        #region Constructor
        public Ref(string model)
        {
            this.model = model;
            this.ActiveFg = true;
        }

        public Ref(string model, string name)
        {
            this.model = model;
            this.Name = name;
            this.ActiveFg = true;
        }
        #endregion

        #region Data Members
        private string model;
        private object id = null;
        private string name = null;
        private bool? active_fg = null;
        #endregion

        #region Properties
        public object Id { get { return this.id; } set { this.id = value; } }
        public string Name { get { return this.name; } set { this.name = Text.trimmer.Replace(value.ToString().ToLower().Trim(), " "); } }
        public bool? ActiveFg { get { return this.active_fg; } set { this.active_fg = value; } }
        #endregion

        #region Sql statements
        public const string SQL_INSERT = "INSERT INTO @table (name, active_fg) OUTPUT INSERTED.ID VALUES (@name, @active_fg)";
        public const string SQL_UPADTE = "UPDATE @table SET name=@name, active_fg=@active_fg WHERE id = @id";
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@name"] = new Parameter(this.name, Parameter.ParameterType.STRING);
            param["@active_fg"] = new Parameter(this.active_fg, Parameter.ParameterType.BOOLEAN);

            object result = da.Add(Ref.SQL_INSERT.Replace("@table", this.model), param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = result;
            }
            
        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@id"] = new Parameter(this.id, Parameter.ParameterType.INT);
            param["@name"] = new Parameter(this.name, Parameter.ParameterType.STRING);
            param["@active_fg"] = new Parameter(this.active_fg, Parameter.ParameterType.BOOLEAN);

            int result = da.ExecuteNonQuery(Ref.SQL_UPADTE.Replace("@table", this.model), param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Methods
        public static DataTable fetch(string model, Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from(model);


            if (filter.ContainsKey("id"))
            {
                qb.where("id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("active_fg"))
            {
                qb.where("active_fg=@active_fg");
                param["@active_fg"] = new Parameter(filter["active_fg"], Parameter.ParameterType.BOOLEAN);
            }

            if (filter.ContainsKey("name"))
            {
                qb.where("name=@name");
                param["@name"] = new Parameter(Text.trimmer.Replace(filter["name"].ToString().ToLower().Trim(), " "), Parameter.ParameterType.STRING);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();


            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static Ref ToObject(string model, DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Ref b = new Ref(model);

            b.Id = dt.Rows[0].Field<object>("id");
            b.Name = dt.Rows[0].Field<string>("name");
            b.ActiveFg = dt.Rows[0].Field<bool?>("active_fg");
            b.is_new = false;
            return b;
        }
        #endregion
    }
}