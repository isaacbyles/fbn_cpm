﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Process : Base
    {
        #region Constructors
        public Process()
        {

        }

        public Process(string name, string description, short reassign_role_id)
        {
            this.Name = name;
            this.Description = description;
            this.ReassignRoleId = reassign_role_id;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
            this.Status = Model.Status.INACTIVE;
        }
        #endregion

        #region Data Members
        private int id;
        private string name;
        private string description;
        private short reassign_role_id;
        private DateTime created_date;
        private DateTime modified_date;
        private short status;
        #endregion

        #region Properties
        public int Id { get { return this.id; } set { this.id = value; } }
        public string Name { get { return this.name; } set { this.name = Text.trimmer.Replace(value.ToString().ToLower().Trim(), " "); } }
        public string Description { get { return this.description; } set { this.description = value.ToString().Trim(); } }
        public short ReassignRoleId { get { return this.reassign_role_id; } set { this.reassign_role_id = value; } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public DateTime ModifiedDate { get { return this.modified_date; } set { this.modified_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"INSERT INTO [process]
(name, description, reassign_role_id, created_date, modified_date, status)
OUTPUT INSERTED.ID 
VALUES (@name, @description, @reassign_role_id, @created_date, @modified_date, @status)";
        public const string SQL_UPDATE = "UPDATE [process] SET name=@name, description=@description, reassign_role_id=@reassign_role_id, created_date=@created_date, modified_date=@modified_date, status=@status WHERE id = @id";
        public const string SQL_LOGIN_FETCH = @"
SELECT process.* FROM process INNER JOIN stage ON process.id = stage.process_id WHERE stage.role_id = @role_id AND process.status = @status AND stage.status = @status;
SELECT stage.*, stage_form.form_id, [form].name AS form_name, [form].description AS form_description  FROM stage LEFT JOIN stage_form ON stage.id = stage_form.stage_id LEFT JOIN [form] ON [form].id = stage_form.form_id WHERE stage.role_id = @role_id AND stage.status = @status AND stage.type = @type;
";
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@name"] = new Parameter(this.name, Parameter.ParameterType.STRING);
            param["@description"] = new Parameter(this.description, Parameter.ParameterType.STRING);
            param["@reassign_role_id"] = new Parameter(this.reassign_role_id, Parameter.ParameterType.INT);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }

        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@id"] = new Parameter(this.id, Parameter.ParameterType.INT);
            param["@name"] = new Parameter(this.name, Parameter.ParameterType.STRING);
            param["@description"] = new Parameter(this.description, Parameter.ParameterType.STRING);
            param["@reassign_role_id"] = new Parameter(this.reassign_role_id, Parameter.ParameterType.INT);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_UPDATE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Property Changing Methods
        public void editDetails(string name, string description, short reassign_role_id)
        {
            this.Name = name;
            this.Description = description;
            this.ReassignRoleId = reassign_role_id;
            this.ModifiedDate = DateTime.Now;
        }

        public void changeStatus(short status)
        {
            this.Status = status;
            this.ModifiedDate = DateTime.Now;
        }
        #endregion

        #region Fetch Methods
        public static DataSet loginFetch(short role_id)
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@role_id"] = new Parameter(role_id, Parameter.ParameterType.INT);
            param["@status"] = new Parameter(Model.Status.ACTIVE, Parameter.ParameterType.INT);
            param["@type"] = new Parameter(Model.Stage.TYPE_START, Parameter.ParameterType.INT);

            DataSet result = da.Fetch(SQL_LOGIN_FETCH, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result;
                }
            }
            return null;
        }

        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[process]");

            if (filter.ContainsKey("reassign_role_id"))
            {
                qb.where("reassign_role_id=@reassign_role_id");
                param["@reassign_role_id"] = new Parameter(filter["reassign_role_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("name"))
            {
                //used to check if there is another process has that name
                if (filter.ContainsKey("id"))
                {
                    qb.where("id != @id");
                    param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.INT);
                }
                qb.where("name=@name");
                param["@name"] = new Parameter(Text.trimmer.Replace(filter["name"].ToString().ToLower().Trim(), " "), Parameter.ParameterType.STRING);
            }
            else if (filter.ContainsKey("id"))
            {
                qb.where("id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.INT);
            }

            else if (filter.ContainsKey("status"))
            {
                qb.where("status=@status");
                param["@status"] = new Parameter(filter["status"], Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static Process ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Process p = new Process();
            p.Id = dt.Rows[0].Field<int>("id");
            p.Name = dt.Rows[0].Field<string>("name");
            p.Description = dt.Rows[0].Field<string>("description");
            p.ReassignRoleId = short.Parse(dt.Rows[0].Field<object>("reassign_role_id").ToString());
            p.CreatedDate = dt.Rows[0].Field<DateTime>("created_date");
            p.ModifiedDate = dt.Rows[0].Field<DateTime>("modified_date");
            p.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());

            p.is_new = false;
            return p;
        }

        public static Process getById(int id)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();

            //get process by id
            filter["id"] = id;
            DataTable result = Process.fetch(filter: filter);
            if (Model.Base.hasError()) { return null; }
            else if (result.Rows.Count == 0) { return null; }

            // creating a process object from the datatable
            return Process.ToObject(result); 
        }
        #endregion
    }
}