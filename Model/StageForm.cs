﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class StageForm : Base
    {
        #region Constructors
        public StageForm()
        {
        }

        public StageForm(int form_id, int stage_id)
        {
            this.FormId = form_id;
            this.StageId = stage_id;
            this.Status = Model.Status.ACTIVE;
        }
        #endregion

        #region Data Members
        private int id;
        private int form_id;
        private int stage_id;
        private short status;
        #endregion

        #region Properties
        public int Id { get { return this.id; } set { this.id = value; } }
        public int FormId { get { return this.form_id; } set { this.form_id = value; } }
        public int StageId { get { return this.stage_id; } set { this.stage_id = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
        #endregion

        #region Sql statements
        public const string SQL_INSERT = "INSERT INTO stage_form (form_id, stage_id, status) OUTPUT INSERTED.ID VALUES (@form_id, @stage_id, @status)";
        public const string SQL_UPADTE = "UPDATE stage_form SET form_id=@form_id, stage_id=@stage_id, status=@status WHERE id = @id";
        public const string SQL_MASS_DELETE = "DELETE FROM stage_form WHERE stage_id=@stage_id";
        #endregion

        #region Mass Action
        public static void MassDelete(int stage_id)
        {
            //in case a stage can have more than one form

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@stage_id"] = new Parameter(stage_id, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_MASS_DELETE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }
        #endregion 

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@form_id"] = new Parameter(this.form_id, Parameter.ParameterType.INT);
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }

        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@id"] = new Parameter(this.id, Parameter.ParameterType.INT);
            param["@form_id"] = new Parameter(this.form_id, Parameter.ParameterType.INT);
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_UPADTE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion
    }
}