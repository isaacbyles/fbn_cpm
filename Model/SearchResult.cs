﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Model
{
    public class SearchResult
    {
        public string mYear { get; set; }
        public string mTitle { get; set; }
        public string mNumber { get; set; }
        public string mContent { get; set; }
        public string summary { get; set; }
        public string rank { get; set; }
        public string tablename { get; set; }
        public float score { get; set; }
        public string casetitle2 { get; set; }
        public string identifier { get; set; }
        public int totalHits{get; set; }

    }
}