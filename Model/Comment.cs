﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Comment : Base
    {
        #region Constructors
        public Comment()
        {

        }

        public Comment(long request_id, int process_id, int stage_id, int user_id, string message)
        {
            this.RequestId = request_id;
            this.ProcessId = process_id;
            this.StageId = stage_id;
            this.UserId = user_id;
            this.Message = message;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
            this.Status = Model.Status.ACTIVE;
        }
        #endregion

        #region Data Members
        private long id;
        private long request_id;
        private int process_id;
        private int stage_id;
        private int user_id;
        private string message;
        private DateTime created_date;
        private DateTime modified_date;
        private short status;
        #endregion

        #region Properties
        public long Id { get { return this.id; } set { this.id = value; } }
        public long RequestId { get { return this.request_id; } set { this.request_id = value; } }
        public int ProcessId { get { return this.process_id; } set { this.process_id = value; } }
        public int StageId { get { return this.stage_id; } set { this.stage_id = value; } }
        public int UserId { get { return this.user_id; } set { this.user_id = value; } }
        public string Message { get { return this.message; } set { this.message = value.Trim(); } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public DateTime ModifiedDate { get { return this.modified_date; } set { this.modified_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
        #endregion

        #region Sql Statements
        public const string SQL_INSERT = @"
INSERT INTO [comment]
(request_id, process_id, stage_id, user_id, message, created_date, modified_date, status)
OUTPUT INSERTED.ID
VALUES (@request_id, @process_id, @stage_id, @user_id, @message, @created_date, @modified_date, @status);
";
        public const string SQL_UPDATE = @"
UPDATE [comment]
SET request_id=@request_id, process_id=@process_id, stage_id=@stage_id, user_id=@user_id, 
    message=@message, modified_date=@modified_date, status=@status
WHERE id = @id;
";
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@request_id"] = new Parameter(this.request_id, Parameter.ParameterType.BIGINT);
            param["@process_id"] = new Parameter(this.process_id, Parameter.ParameterType.INT);
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@user_id"] = new Parameter(this.user_id, Parameter.ParameterType.INT);
            param["@message"] = new Parameter(this.message, Parameter.ParameterType.STRING);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }

        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@id"] = new Parameter(this.id, Parameter.ParameterType.BIGINT);
            param["@request_id"] = new Parameter(this.request_id, Parameter.ParameterType.BIGINT);
            param["@process_id"] = new Parameter(this.process_id, Parameter.ParameterType.INT);
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@user_id"] = new Parameter(this.user_id, Parameter.ParameterType.INT);
            param["@message"] = new Parameter(this.message, Parameter.ParameterType.STRING);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_UPDATE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[comment]");
            qb.column("[comment].*");
            qb.orderBy("modified_date", false);

            if (fetch_with.Contains("user"))
            {
                qb.innerJoin("[profile]", "[profile].user_id = [comment].user_id");
                qb.columns(
                    "[profile].firstname AS user_firstname",
                    "[profile].lastname AS user_lastname",
                    "[profile].middlename AS user_middlename"
                    );
            }

            if (fetch_with.Contains("stage"))
            {
                qb.innerJoin("stage", "stage.id = comment.stage_id");
                qb.columns(
                    "[stage].name AS stage_name"
                    );
            }

            if (filter.ContainsKey("id"))
            {
                qb.where("[comment].id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.BIGINT);
            }

            if (filter.ContainsKey("process_id"))
            {
                qb.where("[comment].process_id=@process_id");
                param["@process_id"] = new Parameter(filter["process_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("request_id"))
            {
                qb.where("[comment].request_id=@request_id");
                param["@request_id"] = new Parameter(filter["request_id"], Parameter.ParameterType.BIGINT);
            }

            if (filter.ContainsKey("stage_id"))
            {
                qb.where("[comment].stage_id=@stage_id");
                param["@stage_id"] = new Parameter(filter["stage_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("user_id"))
            {
                qb.where("[comment].user_id=@user_id");
                param["@user_id"] = new Parameter(filter["user_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("status"))
            {
                qb.where("[comment].status=@status");
                param["@status"] = new Parameter(filter["status"], Parameter.ParameterType.INT);
            }
            else
            {
                qb.where("[comment].status=@status");
                param["@status"] = new Parameter(Model.Status.ACTIVE, Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static Comment ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Comment c = new Comment();
            c.Id = long.Parse(dt.Rows[0].Field<object>("id").ToString());
            c.RequestId = long.Parse(dt.Rows[0].Field<object>("request_id").ToString());
            c.ProcessId = dt.Rows[0].Field<int>("process_id");
            c.StageId = dt.Rows[0].Field<int>("stage_id");
            c.UserId = dt.Rows[0].Field<int>("user_id");
            c.Message = dt.Rows[0].Field<string>("message");
            c.CreatedDate = dt.Rows[0].Field<DateTime>("created_date");
            c.ModifiedDate = dt.Rows[0].Field<DateTime>("modified_date");
            c.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());

            c.is_new = false;
            return c;
        }
        #endregion
    }
}