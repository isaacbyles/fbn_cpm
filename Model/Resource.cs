﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Resource:Base
    {
        #region Constructors
        public Resource()
        {

        }

        public Resource(long request_id, int stage_id, int user_id, string label, string description, string mime_type, short route)
        {
            this.RequestId = request_id;
            this.StageId = stage_id;
            this.UserId = user_id;
            this.Label = label;
            this.Description = description;
            this.MimeType = mime_type;
            this.Route = route;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
            this.Status = Model.Status.ACTIVE;
           // this.Role_Id = role_id;
           // this.Branch_Id = branch_id;

        }
        #endregion

        #region Data Members
        private long id;
        private long request_id;
        private int stage_id;
        private int user_id;
        private string label;
        private string description;
        private string mime_type;
        private short route; // 1 - input to the process, 2 - output to the process
        private DateTime created_date;
        private DateTime modified_date;
        private short status;
       // private int role_id;
        //private int branch_id;
        #endregion

        #region Properties
        public long Id { get { return this.id; } set { this.id = value; } }
        public long RequestId { get { return this.request_id; } set { this.request_id = value; } }
        public int StageId { get { return this.stage_id; } set { this.stage_id = value; } }
        public int UserId { get { return this.user_id; } set { this.user_id = value; } }
        public string Label { get { return this.label; } set { this.label = Text.trimmer.Replace(value.ToString().ToLower().Trim(), " "); } }
        public string Description { get { return this.description; } set { this.description = value.ToString().Trim(); } }
        public string MimeType { get { return this.mime_type; } set { this.mime_type = value.ToString().Trim(); } }
        public short Route { get { return this.route; } set { this.route = value; } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public DateTime ModifiedDate { get { return this.modified_date; } set { this.modified_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
       // public int Role_Id { get { return this.role_id; } set { this.role_id = value; } }
      //  public int Branch_Id { get { return this.branch_id; } set { this.branch_id = value; } }
        #endregion

        #region Constants
        public const short INPUT_ROUTE = 1;
        public const short OUTPUT_ROUTE = 2;
        #endregion

        #region Property Changing Methods
        public void changeDetails(string label, string description)
        {
            this.Label = label;
            this.Description = description;
            this.ModifiedDate = DateTime.Now;
        }

        public void changeStatus(short status)
        {
            this.Status = status;
            this.ModifiedDate = DateTime.Now;
        }
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"
INSERT INTO [resource]
(request_id, stage_id, user_id, label, description, mime_type, route, created_date, modified_date, status)
OUTPUT INSERTED.ID
VALUES (@request_id, @stage_id, @user_id, @label, @description, @mime_type, @route, @created_date, @modified_date, @status);
";
        public const string SQL_UPDATE = @"
UPDATE [resource] 
SET request_id=@request_id, stage_id=@stage_id, user_id=@user_id, label=@label, description=@description, 
    mime_type=@mime_type, route=@route, modified_date=@modified_date, status=@status
WHERE id = @id;
";
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@request_id"] = new Parameter(this.request_id, Parameter.ParameterType.BIGINT);
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@user_id"] = new Parameter(this.user_id, Parameter.ParameterType.INT);
            param["@label"] = new Parameter(this.label, Parameter.ParameterType.STRING);
            param["@description"] = new Parameter(this.description, Parameter.ParameterType.STRING);
            param["@mime_type"] = new Parameter(this.mime_type, Parameter.ParameterType.STRING);
            param["@route"] = new Parameter(this.route, Parameter.ParameterType.INT);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);
            //param["@role_id"] = new Parameter(this.role_id, Parameter.ParameterType.INT);
            //param["@branch_id"] = new Parameter(this.branch_id, Parameter.ParameterType.INT);
            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }

        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@id"] = new Parameter(this.id, Parameter.ParameterType.BIGINT);
            param["@request_id"] = new Parameter(this.request_id, Parameter.ParameterType.BIGINT);
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@user_id"] = new Parameter(this.user_id, Parameter.ParameterType.INT);
            param["@label"] = new Parameter(this.label, Parameter.ParameterType.STRING);
            param["@description"] = new Parameter(this.description, Parameter.ParameterType.STRING);
            param["@mime_type"] = new Parameter(this.mime_type, Parameter.ParameterType.STRING);
            param["@route"] = new Parameter(this.user_id, Parameter.ParameterType.INT);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_UPDATE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[resource]");
            qb.column("[resource].*");

            if (fetch_with.Contains("user"))
            {
                qb.innerJoin("[profile]", "[profile].user_id = [resource].user_id");
                qb.columns(
                    "[profile].firstname AS user_firstname",
                    "[profile].lastname AS user_lastname",
                    "[profile].middlename AS user_middlename"
                    );
            }

            if (fetch_with.Contains("stage"))
            {
                qb.innerJoin("stage", "stage.id = resource.stage_id");
                qb.columns(
                    "[stage].name AS stage_name"
                    );
            }

            if (filter.ContainsKey("id"))
            {
                qb.where("[resource].id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("request_id"))
            {
                qb.where("[resource].request_id=@request_id");
                param["@request_id"] = new Parameter(filter["request_id"], Parameter.ParameterType.BIGINT);
            }

            if (filter.ContainsKey("stage_id"))
            {
                qb.where("[resource].stage_id=@stage_id");
                param["@stage_id"] = new Parameter(filter["stage_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("user_id"))
            {
                qb.where("[resource].user_id=@user_id");
                param["@user_id"] = new Parameter(filter["user_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("label"))
            {
                qb.where("[resource].label= '%' + @label + '%'");
                param["@label"] = new Parameter(Text.trimmer.Replace(filter["label"].ToString().ToLower().Trim(), " "), Parameter.ParameterType.STRING);
            }

            if (filter.ContainsKey("status"))
            {
                qb.where("[resource].status=@status");
                param["@status"] = new Parameter(filter["status"], Parameter.ParameterType.INT);
            }
            else
            {
                qb.where("[resource].status=@status");
                param["@status"] = new Parameter(Model.Status.ACTIVE, Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static Resource ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Resource r = new Resource();
            r.Id = long.Parse(dt.Rows[0].Field<object>("id").ToString());
            r.RequestId = long.Parse(dt.Rows[0].Field<object>("request_id").ToString());
            r.StageId = dt.Rows[0].Field<int>("stage_id");
            r.UserId = dt.Rows[0].Field<int>("user_id");
            r.Label = dt.Rows[0].Field<string>("label");
            r.Description = dt.Rows[0].Field<string>("description");
            r.MimeType = dt.Rows[0].Field<string>("mime_type");
            r.Route = short.Parse(dt.Rows[0].Field<object>("route").ToString());
            r.CreatedDate = dt.Rows[0].Field<DateTime>("created_date");
            r.ModifiedDate = dt.Rows[0].Field<DateTime>("modified_date");
            r.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());

            r.is_new = false;
            return r;
        }
        #endregion
    }
}