﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Stage : Base
    {
        #region Constructors
        public Stage()
        {

        }

        public Stage(int process_id, short role_id, string name, string description, short type = TYPE_NORMAL)
        {
            this.ProcessId = process_id;
            this.RoleId = role_id;
            this.Type = type;
            this.Name = name;
            this.Description = description;
            this.Status = Model.Status.ACTIVE;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
        }
        #endregion

        #region Constants
        public const short TYPE_START = 2;
        public const short TYPE_NORMAL = 1;
        #endregion

        #region Data Members
        private int id;
        private int process_id;
        private short role_id;
        private short type;
        private string name;
        private string description;
        private DateTime created_date;
        private DateTime modified_date;
        private short status;
        private bool is_editable=false;
        #endregion

        #region Properties
        public int Id { get { return this.id; } set { this.id = value; } }
        public int ProcessId { get { return this.process_id; } set { this.process_id = value; } }
        public short RoleId { get { return this.role_id; } set { this.role_id = value; } }
        public short Type { get { return this.type; } set { this.type = value; } }
        public string Name { get { return this.name; } set { this.name = Text.trimmer.Replace(value.ToString().ToLower().Trim(), " "); } }
        public string Description { get { return this.description; } set { this.description = (value == null) ? null : value.ToString().Trim(); } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public DateTime ModifiedDate { get { return this.modified_date; } set { this.modified_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"INSERT INTO [stage]
(process_id, role_id, type, name, description, created_date, modified_date, status)
OUTPUT INSERTED.ID 
VALUES (@process_id, @role_id, @type, @name, @description, @created_date, @modified_date, @status)";
        public const string SQL_UPADTE = "UPDATE [stage] SET process_id=@process_id, role_id=@role_id, type=@type, name=@name, description=@description, created_date=@created_date, modified_date=@modified_date, status=@status WHERE id = @id";
        public const string SQL_DELETE = "DELETE FROM [stage] WHERE id=@id";
        #endregion

        #region Miscellaneous Methods
        public bool isEditable()
        {
            return is_editable;
        }
        #endregion

        #region Property Changing Methods
        public void editDetails(string name, string description)
        {
            this.Name = name;
            this.Description = description;
            this.ModifiedDate = DateTime.Now;
        }

        public void changeStatus(short status)
        {
            this.Status = status;
            this.ModifiedDate = DateTime.Now;
        }
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@process_id"] = new Parameter(this.process_id, Parameter.ParameterType.INT);
            param["@role_id"] = new Parameter(this.role_id, Parameter.ParameterType.INT);
            param["@type"] = new Parameter(this.type, Parameter.ParameterType.INT);
            param["@name"] = new Parameter(this.name, Parameter.ParameterType.STRING);
            param["@description"] = new Parameter(this.description, Parameter.ParameterType.STRING);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }

        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@id"] = new Parameter(this.id, Parameter.ParameterType.INT);
            param["@process_id"] = new Parameter(this.process_id, Parameter.ParameterType.INT);
            param["@role_id"] = new Parameter(this.role_id, Parameter.ParameterType.INT);
            param["@type"] = new Parameter(this.type, Parameter.ParameterType.INT);
            param["@name"] = new Parameter(this.name, Parameter.ParameterType.STRING);
            param["@description"] = new Parameter(this.description, Parameter.ParameterType.STRING);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_UPADTE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@id"] = new Parameter(this.id, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_DELETE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
                return false;
            }
            return true;
        }
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();

            qb.from("[stage]");
            qb.innerJoin("[role]", "[role].id = [stage].role_id");
            qb.leftJoin("[stage_form]", "[stage_form].stage_id = [stage].id");

            qb.columns(
                "[stage].*",
                "[stage_form].form_id",
                "[role].name AS role_name"
                );

            if (fetch_with.Contains("process"))
            {
                qb.innerJoin("[process]", "[process].id = [stage].process_id");
                qb.columns(
                    "[process].name AS process_name",
                    "[process].description AS process_description",
                    "[process].reassign_role_id AS process_reassign_role_id",
                    "[process].created_date AS process_created_date",
                    "[process].modified_date AS process_modified_date",
                    "[process].status AS process_status"
                    );
            }

            if (filter.ContainsKey("process_id"))
            {
                qb.where("[stage].process_id=@process_id");
                param["@process_id"] = new Parameter(filter["process_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("role_id"))
            {
                qb.where("[stage].role_id=@role_id");
                param["@role_id"] = new Parameter(filter["role_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("type"))
            {
                qb.where("[stage].type=@type");
                param["@type"] = new Parameter(filter["type"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("id"))
            {
                qb.where("[stage].id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("status"))
            {
                qb.where("[stage].status=@status");
                param["@status"] = new Parameter(filter["status"], Parameter.ParameterType.INT);
            }
            else
            {
                qb.where("[stage].status=@status");
                param["@status"] = new Parameter(Model.Status.ACTIVE, Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static Stage ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Stage s = new Stage();
            s.Id = dt.Rows[0].Field<int>("id");
            s.ProcessId = dt.Rows[0].Field<int>("process_id");
            s.RoleId = short.Parse(dt.Rows[0].Field<object>("role_id").ToString());
            s.Type = short.Parse(dt.Rows[0].Field<object>("type").ToString());
            s.Name = dt.Rows[0].Field<string>("name");
            s.Description = dt.Rows[0].Field<string>("description");
            s.CreatedDate = dt.Rows[0].Field<DateTime>("created_date");
            s.ModifiedDate = dt.Rows[0].Field<DateTime>("modified_date");
            s.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());

            s.is_new = false;

            if (dt.Columns.Contains("process_status"))
            {
                object process_status = dt.Rows[0]["process_status"];
                if (process_status != null)
                {
                    s.is_editable = (short.Parse(process_status.ToString()) == Model.Status.INACTIVE);
                }
            }
            return s;
        }
        #endregion 
    }
}