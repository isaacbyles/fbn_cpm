﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace CaseManager.Model
{
    public abstract class Base
    {
        static Base()
        {
            has_error = false;
        }

        #region Constants
        public const int DEFAULT_OFFSET = 1;
        public const int DEFAULT_COUNT = 20;
        #endregion

        #region Data Member
        protected bool is_new = true;
        public static bool has_error;
        #endregion

        #region Properties
        #endregion 

        #region Methods
        protected abstract void _insert();
        protected abstract void _update();
        public abstract bool delete();

        public bool isNew()
        {
            return this.is_new;
        }

        public static bool hasError()
        {
            return Base.has_error;
        }

        /// <summary>
        /// It saves the fields of the model into a table of the database. 
        /// If the table's id is provided, then it will perform an update else an insert
        /// </summary>
        public bool save()
        {
            if (this.is_new)
            {
                this._insert();
                return !Base.has_error;
            }
            else
            {
                this._update();
                return !Base.has_error;
            }
        }

        public static IEnumerable<Dictionary<string, object>> toAssociative(DataTable dt)
        {
            if (dt == null)
            {
                return null;
            }

             return dt.Select().Select(x => x.ItemArray.Select((a, i) => new { Name = dt.Columns[i].ColumnName, Value = a })
                                                                                   .ToDictionary(a => a.Name, a => a.Value));
        }

        //to be removed
        public static List<IEnumerable<Dictionary<string, object>>> toAssociative(DataSet ds)
        {
            List<IEnumerable<Dictionary<string, object>>> set = new List<IEnumerable<Dictionary<string, object>>>();
            foreach (DataTable dt in ds.Tables)
            {
                IEnumerable<Dictionary<string, object>> result = dt.Select().Select(x => x.ItemArray.Select((a, i) => new { Name = dt.Columns[i].ColumnName, Value = a })
                                                                                                   .ToDictionary(a => a.Name, a => a.Value));
                set.Add(result);
            }
            return set;
        }

        //to be removed
        public static Dictionary<string,IEnumerable<Dictionary<string, object>>> toAssociative(DataSet ds, string[] table_labels)
        {
            Dictionary<string, IEnumerable<Dictionary<string, object>>> set = new Dictionary<string, IEnumerable<Dictionary<string, object>>>();

            //checking if the number of tables in the data set matches the table labels
            if (ds.Tables.Count != table_labels.Count())
            {
                return null;
            }

            for (int j = 0; j < ds.Tables.Count; j++)
            {

                IEnumerable<Dictionary<string, object>> result = ds.Tables[j].Select().Select(x => x.ItemArray.Select((a, i) => new { Name = ds.Tables[j].Columns[i].ColumnName, Value = a })
                                                                                                       .ToDictionary(a => a.Name, a => a.Value));
                set[table_labels[j]] = result;
            }
            return set;
        }
        #endregion
    }
}