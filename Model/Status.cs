﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Model
{
    public class Status
    {
        public const short ACTIVE = 1;
        public const short INACTIVE = 2;
        public const short REMOVED = 3;
        public const short CANCELLED = 4;
        public const short IN_PROCESS = 5;
        public const short COMPLETED = 6;
    }
}