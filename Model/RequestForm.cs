﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;
using System.Transactions;
using Newtonsoft.Json;

namespace CaseManager.Model
{
    public class RequestForm : Base
    {
        #region Constructors
        public RequestForm()
        {
        }

        public RequestForm(long request_id, long request_trail_id, int form_id, int user_id, int stage_id)
        {
            this.RequestId = request_id;
            this.RequestTrailId = request_trail_id;
            this.FormId = form_id;
            this.StageId = stage_id;
            this.UserId = user_id;
            this.CreatedDate = DateTime.Now;
            this.Status = Model.Status.ACTIVE;
        }
        #endregion

        #region Data Members
        private long id;
        private long request_id;
        private long request_trail_id;
        private int form_id;
        private int stage_id;
        private int user_id;
        private DateTime created_date;
        private short status;
        #endregion 

        #region Properties
        public long Id { get { return this.id; } set { this.id = value; } }
        public long RequestId { get { return this.request_id; } set { this.request_id = value; } }
        public long RequestTrailId { get { return this.request_trail_id; } set { this.request_trail_id = value; } }
        public int FormId { get { return this.form_id; } set { this.form_id = value; } }
        public int StageId { get { return this.stage_id; } set { this.stage_id = value; } }
        public int UserId { get { return this.user_id; } set { this.user_id = value; } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }       
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"
INSERT INTO [request_form]
(request_id, request_trail_id, form_id, stage_id, user_id, created_date, status)
OUTPUT INSERTED.ID
VALUES (@request_id, @request_trail_id, @form_id, @stage_id, @user_id, @created_date, @status)
";
        public const string SQL_DELETE = @"
DELETE FROM [request_form] WHERE request_trail_id = @request_trail_id;
";
        #endregion

        #region Execution
        public static bool saveForm(long request_id, long request_trail_id, int form_id, int user_id, int stage_id, List<Dictionary<string, object>> form_data_list)
        {
            if (stage_id == 138)
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    RequestForm rf = new RequestForm(request_id, request_trail_id, form_id, user_id, stage_id);
                    rf.deletePrevious();
                    if (Model.Base.hasError()) { return false; }

                    if (!rf.save()) { return false; }

                    FormData.deleteData(request_trail_id);
                    if (Model.Base.hasError()) { return false; }

                    foreach (var form_data in form_data_list)
                    {
                       // object d = JsonConvert.DeserializeObject(form_data.ToString());
                        int co = form_data.Count;
                        object kkk = null;
                        if (form_data["value"].ToString().StartsWith("["))
                        {
                            object v = form_data["value"];
                            object kk = v.ToString().Substring(1);
                            kkk = kk.ToString().Substring(0, kk.ToString().Length - 1);
                            string[] ddd = kkk.ToString().Split(',');

                            FormData fd = new FormData(stage_id, form_id,
                                int.Parse(form_data["form_control_id"].ToString()), request_id, ddd, request_trail_id);
                            fd.insertData(short.Parse(form_data["form_control_type"].ToString()));
                            if (Model.Base.hasError())
                            {
                                return false;
                            }
                        }
                        else
                        {
                            FormData fdd = new FormData(stage_id, form_id,
                                int.Parse(form_data["form_control_id"].ToString()), request_id, form_data["value"],
                                request_trail_id);
                            fdd.insertData(short.Parse(form_data["form_control_type"].ToString()));
                            if (Model.Base.hasError())
                            {
                                return false;
                            }
                        }

                    }
                    tran.Complete();
                    return true;
                }
            }
            using (TransactionScope tran = new TransactionScope())
            {
                RequestForm rf = new RequestForm(request_id, request_trail_id, form_id, user_id, stage_id);
                rf.deletePrevious();
                if (Model.Base.hasError()) { return false; }

                if (!rf.save()) { return false; }

                FormData.deleteData(request_trail_id);
                if (Model.Base.hasError()) { return false; }

                foreach (var form_data in form_data_list)
                {
                    FormData fd = new FormData(stage_id, form_id, int.Parse(form_data["form_control_id"].ToString()), request_id, form_data["value"], request_trail_id);
                    fd.insertData(short.Parse(form_data["form_control_type"].ToString()));
                    if (Model.Base.hasError()) { return false; }
                }
                tran.Complete();
                return true;
            }
        }

        public void deletePrevious()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@request_trail_id"] = new Parameter(this.request_trail_id, Parameter.ParameterType.BIGINT);

            int result = da.ExecuteNonQuery(SQL_DELETE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@request_id"] = new Parameter(this.request_id, Parameter.ParameterType.BIGINT);
            param["@request_trail_id"] = new Parameter(this.request_trail_id, Parameter.ParameterType.BIGINT);
            param["@form_id"] = new Parameter(this.form_id, Parameter.ParameterType.INT);
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@user_id"] = new Parameter(this.user_id, Parameter.ParameterType.INT);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = long.Parse(result.ToString());
            }

        }

        protected override void _update()
        {
            return;
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[request_form]");
            qb.column("[request_form].*");

            if (fetch_with.Contains("user"))
            {
                qb.innerJoin("[profile]", "[profile].user_id = [request_form].user_id");
                qb.columns(
                    "[profile].firstname AS user_firstname",
                    "[profile].lastname AS user_lastname",
                    "[profile].middlename AS user_middlename"
                    );
            }

            if (fetch_with.Contains("form"))
            {
                qb.innerJoin("[form]", "[form].id = [request_form].form_id");
                qb.columns(
                    "[form].name AS form_name",
                    "[form].description AS form_description"
                    );
            }

            if (filter.ContainsKey("request_id"))
            {
                qb.where("[request_form].request_id=@request_id");
                param["@request_id"] = new Parameter(filter["request_id"], Parameter.ParameterType.BIGINT);
            }

            if (filter.ContainsKey("request_trail_id"))
            {
                qb.where("[request_form].request_trail_id=@request_trail_id");
                param["@request_trail_id"] = new Parameter(filter["request_trail_id"], Parameter.ParameterType.BIGINT);
            }

            if (filter.ContainsKey("user_id"))
            {
                qb.where("[request_form].user_id=@user_id");
                param["@user_id"] = new Parameter(filter["user_id"], Parameter.ParameterType.BIGINT);
            }

            if (filter.ContainsKey("form_id"))
            {
                qb.where("[request_form].form_id=@form_id");
                param["@form_id"] = new Parameter(filter["form_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("stage_id"))
            {
                qb.where("[request_form].stage_id=@stage_id");
                param["@stage_id"] = new Parameter(filter["stage_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("status"))
            {
                qb.where("[request_form].status=@status");
                param["@status"] = new Parameter(filter["status"], Parameter.ParameterType.INT);
            }
            else
            {
                qb.where("[request_form].status=@status");
                param["@status"] = new Parameter(Model.Status.ACTIVE, Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }
        #endregion
    }
}