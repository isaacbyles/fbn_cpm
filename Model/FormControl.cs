﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class FormControl : Base
    {
        #region Constructors
        public FormControl()
        {
        }

        public FormControl(int form_id, short type, string label, string note, bool is_required, bool has_multiple)
        {
            this.FormId = form_id;
            this.Type = type;
            this.Label = label;
            this.Note = note;
            this.IsRequired = is_required;
            this.HasMultiple = has_multiple;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
            this.Status = Model.Status.ACTIVE;
        }

        public FormControl(int form_id, Dictionary<string, string> pack) 
        {
            this.FormId = form_id;
            this.Type = short.Parse(pack["type"]);
            this.Label = pack["label"];
            this.Note = pack["note"];
            this.IsRequired = int.Parse(pack["is_required"]) == 1;
            this.HasMultiple = int.Parse(pack["has_multiple"]) == 1;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
            this.Status = Model.Status.ACTIVE;
        }
        #endregion

        #region Types
        public const short TYPE_CHAR = 1;
        public const short TYPE_DATE = 2;
        public const short TYPE_DECIMAL = 3;
        public const short TYPE_INT = 4;
        public const short TYPE_TEXT = 5;
        public const short TYPE_DROPDOWN = 6;
        public const short TYPE_RADIO = 7;
        public const short TYPE_CHECKBOX = 8;
        #endregion

        #region Data Members
        private int id;
        private int form_id;
        private short type;
        private string label;
        private string note;
        private bool is_required;
        private bool has_multiple;
        private DateTime created_date;
        private DateTime modified_date;
        private short status;
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"INSERT INTO [form_control]
(form_id, [type], [label], [note], is_required, has_multiple, created_date, modified_date, status)
OUTPUT INSERTED.ID 
VALUES (@form_id, @type, @label, @note, @is_required, @has_multiple, @created_date, @modified_date, @status)";
        public const string SQL_UPADTE = "UPDATE [form_control] SET form_id=@form_id, [type]=@type, [label]=@label,  [note]=@note, is_required=@is_required, has_multiple=@has_multiple, created_date=@created_date, modified_date=@modified_date, status=@status WHERE id = @id";
        public const string SQL_MASS_UPDATE_STATUS = "UPDATE [form_control] SET status=@status WHERE form_id=@form_id";
        #endregion

        #region Properties
        public int Id { get { return this.id; } set { this.id = value; } }
        public int FormId { get { return this.form_id; } set { this.form_id = value; } }
        public short Type { get { return this.type; } set { this.type = value; } }
        public string Label { get { return this.label; } set { this.label = Text.trimmer.Replace(value.ToString().ToLower().Trim(), " "); } }
        public string Note { get { return this.note; } set { this.note = value.ToString().Trim(); } }
        public bool HasMultiple { get { return this.has_multiple; } set { this.has_multiple = value; } }
        public bool IsRequired { get { return this.is_required; } set { this.is_required = value; } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public DateTime ModifiedDate { get { return this.modified_date; } set { this.modified_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@form_id"] = new Parameter(this.form_id, Parameter.ParameterType.INT);
            param["@type"] = new Parameter(this.type, Parameter.ParameterType.INT);
            param["@label"] = new Parameter(this.label, Parameter.ParameterType.STRING);
            param["@note"] = new Parameter(this.note, Parameter.ParameterType.STRING);
            param["@is_required"] = new Parameter(this.is_required, Parameter.ParameterType.BOOLEAN);
            param["@has_multiple"] = new Parameter(this.has_multiple, Parameter.ParameterType.BOOLEAN);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }
        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@id"] = new Parameter(this.id, Parameter.ParameterType.INT);
            param["@form_id"] = new Parameter(this.form_id, Parameter.ParameterType.INT);
            param["@type"] = new Parameter(this.type, Parameter.ParameterType.INT);
            param["@label"] = new Parameter(this.label, Parameter.ParameterType.STRING);
            param["@note"] = new Parameter(this.note, Parameter.ParameterType.STRING);
            param["@is_required"] = new Parameter(this.is_required, Parameter.ParameterType.BOOLEAN);
            param["@has_multiple"] = new Parameter(this.has_multiple, Parameter.ParameterType.BOOLEAN);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_UPADTE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Property Changing Methods

        public void changeLabel(string label)
        {
            this.Label = label;
            this.ModifiedDate = DateTime.Now;
        }

        public void changeStatus(short status)
        {
            this.Status = status;
            this.ModifiedDate = DateTime.Now;
        }
        #endregion

        #region Mass Actions
        public static void changeStatusByFormId(int form_id, short status)
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@form_id"] = new Parameter(form_id, Parameter.ParameterType.INT);
            param["@status"] = new Parameter(status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_MASS_UPDATE_STATUS, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[form_control]");
            qb.leftJoin("[form_control_data]", "[form_control_data].form_control_id = [form_control].id");
            qb.where("[form_control].status=" + Model.Status.ACTIVE);

            qb.columns(
                "[form_control].id",
                "[form_control].form_id",
                "[form_control].type",
                "[form_control].label",
                "[form_control].note",
                "[form_control].is_required",
                "[form_control].has_multiple",
                "[form_control].created_date",
                "[form_control].modified_date",
                "[form_control].status",
                "[form_control_data].data"
                );

            if (filter.ContainsKey("id"))
            {
                qb.where("id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("form_id"))
            {
                qb.where("form_id=@form_id");
                param["@form_id"] = new Parameter(filter["form_id"], Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static FormControl ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            FormControl f = new FormControl();
            f.Id = dt.Rows[0].Field<int>("id");
            f.FormId = dt.Rows[0].Field<int>("form_id");
            f.Type = short.Parse(dt.Rows[0].Field<object>("type").ToString());
            f.Label = dt.Rows[0].Field<string>("label");
            f.Note = dt.Rows[0].Field<string>("note");
            f.IsRequired = dt.Rows[0].Field<bool>("is_required");
            f.HasMultiple = dt.Rows[0].Field<bool>("has_multiple");
            f.CreatedDate = dt.Rows[0].Field<DateTime>("created_date");
            f.ModifiedDate = dt.Rows[0].Field<DateTime>("modified_date");
            f.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());

            f.is_new = false;
            return f;
        }
        #endregion
    }
}