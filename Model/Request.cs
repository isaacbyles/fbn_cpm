﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Request : Base
    {
        #region Constructors
        public Request()
        {

        }

        public Request(int process_id, int stage_id, int init_user_id, int current_user_id, string title, int lifespan, string description, string remark, string note, string ref_no, int user_role)
        {
            this.ProcessId = process_id;
            this.StageId = stage_id;
            this.InitUserId = init_user_id;
            this.CurrentUserId = current_user_id;
            this.Title = title;
            this.Description = description;
            this.Remark = remark;
            this.Note = note;
            this.Lifespan = lifespan;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
            this.Status = Model.Status.IN_PROCESS;
            this.Ref_No = ref_no;
            this.User_Role = user_role;
        }
        #endregion

        #region Data Members
        private long id;
        private int process_id;
        private int stage_id;
        private int init_user_id;
        private int current_user_id;
        private string title;
        private string description;
        private string remark;
        private string note;
        private int lifespan;
        private DateTime created_date;
        private DateTime modified_date;
        private short status;
        private string ref_no;
        private int user_role;
        #endregion

        #region Properties
        public long Id { get { return this.id; } set { this.id = value; } }
        public int ProcessId { get { return this.process_id; } set { this.process_id = value; } }
        public int StageId { get { return this.stage_id; } set { this.stage_id = value; } }
        public int InitUserId { get { return this.init_user_id; } set { this.init_user_id = value; } }
        public int CurrentUserId { get { return this.current_user_id; } set { this.current_user_id = value; } }
        public string Title { get { return this.title; } set { this.title = value.Trim(); } }
        public string Description { get { return this.description; } set { this.description = value; } }
        public string Remark { get { return this.remark; } set { this.remark = value; } }
        public string Note { get { return this.note; } set { this.note = value; } }
        public int Lifespan { get { return this.lifespan; } set { this.lifespan = value; } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public DateTime ModifiedDate { get { return this.modified_date; } set { this.modified_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
        public string Ref_No { get { return this.ref_no; } set { this.ref_no = value; } }
        public int User_Role { get { return this.user_role; } set { this.user_role = value; } }
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"
INSERT INTO [request]
(process_id, stage_id, init_user_id, current_user_id, title, lifespan, created_date, modified_date, status, description, note, remark, ref_no, user_role)
OUTPUT INSERTED.ID
VALUES (@process_id, @stage_id, @init_user_id, @current_user_id, @title, @lifespan, @created_date, @modified_date, @status, @description, @note, @remark, @ref_no , @user_role)
";
        public const string SQL_UPDATE = @"
UPDATE [request] SET process_id=@process_id, stage_id=@stage_id, init_user_id=@init_user_id, current_user_id=@current_user_id,
title=@title, lifespan=@lifespan, modified_date=@modified_date, status=@status WHERE id = @id
";
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@process_id"] = new Parameter(this.process_id, Parameter.ParameterType.INT);
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@init_user_id"] = new Parameter(this.init_user_id, Parameter.ParameterType.INT);
            param["@current_user_id"] = new Parameter(this.current_user_id, Parameter.ParameterType.INT);
            param["@title"] = new Parameter(this.title, Parameter.ParameterType.STRING);
            param["@description"] = new Parameter(this.description, Parameter.ParameterType.STRING);
            param["@note"] = new Parameter(this.note, Parameter.ParameterType.STRING);
            param["@remark"] = new Parameter(this.remark, Parameter.ParameterType.STRING);
            param["@lifespan"] = new Parameter(this.lifespan, Parameter.ParameterType.INT);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);
            param["@ref_no"] = new Parameter(this.ref_no, Parameter.ParameterType.STRING);
            param["@user_role"] = new Parameter(this.user_role, Parameter.ParameterType.INT);
            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = long.Parse(result.ToString());
            }

        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@id"] = new Parameter(this.id, Parameter.ParameterType.BIGINT);
            param["@process_id"] = new Parameter(this.process_id, Parameter.ParameterType.INT);
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@init_user_id"] = new Parameter(this.init_user_id, Parameter.ParameterType.INT);
            param["@current_user_id"] = new Parameter(this.current_user_id, Parameter.ParameterType.INT);
            param["@title"] = new Parameter(this.title, Parameter.ParameterType.STRING);
            param["@lifespan"] = new Parameter(this.lifespan, Parameter.ParameterType.INT);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(SQL_UPDATE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Property Changing Methods
        public void changeStatus(short status)
        {
            this.Status = status;
            this.ModifiedDate = DateTime.Now;
        }

        public void moveToNextStage(int stage_id)
        {
            this.StageId = stage_id;
            this.ModifiedDate = DateTime.Now;
        }

        public void moveToNextStage(int stage_id, int current_id)
        {
            this.StageId = stage_id;
            this.CurrentUserId = current_id;
            this.ModifiedDate = DateTime.Now;
        }

        public void end()
        {
            this.CurrentUserId = this.InitUserId;
            this.ModifiedDate = DateTime.Now;
            this.Status = Model.Status.COMPLETED;
        }

        public void reassign(int current_user_id)
        {
            this.CurrentUserId = current_user_id;
            this.ModifiedDate = DateTime.Now;
        }
        
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[request]");
            qb.column("[request].*");
            qb.orderBy("[request].id", false); 
            if (fetch_with.Contains("process"))
            {
                qb.innerJoin("[process]", "[process].id = [request].process_id");
                qb.columns(
                    "[process].name AS process_name",
                    "[process].description AS process_description",
                    "[process].reassign_role_id AS process_reassign_role_id",
                    "[process].created_date AS process_created_date",
                    "[process].modified_date AS process_modified_date",
                    "[process].status AS process_status"
                    );
            }

            if (fetch_with.Contains("stage"))
            {
                qb.innerJoin("[stage]", "[stage].id = [request].stage_id");
                qb.leftJoin("[stage_form]", "[stage_form].stage_id = [stage].id");
                qb.leftJoin("[form]", "[form].id = [stage_form].form_id");
                qb.columns(
                    "[stage].role_id AS stage_role_id",
                    "[stage].type AS stage_type",
                    "[stage].name AS stage_name",
                    "[stage].description AS stage_description",
                    "[stage].created_date AS stage_created_date",
                    "[stage].modified_date AS stage_modified_date",
                    "[stage].status AS stage_status",
                    "[stage_form].form_id AS stage_form_id",
                    "[form].name AS form_name",
                    "[form].description AS form_description"
                    );
            }

            if (fetch_with.Contains("init_user"))
            {
                qb.innerJoin("[profile] AS init_user_profile", "init_user_profile.user_id = [request].init_user_id");
                qb.columns(
                    "[init_user_profile].firstname AS init_user_firstname",
                    "[init_user_profile].lastname AS init_user_lastname",
                    "[init_user_profile].middlename AS init_user_middlename"
                    );
            }

            if (fetch_with.Contains("current_user"))
            {
                qb.innerJoin("[profile] AS current_user_profile", "current_user_profile.user_id = [request].current_user_id");
                qb.columns(
                    "[current_user_profile].firstname AS current_user_firstname",
                    "[current_user_profile].lastname AS current_user_lastname",
                    "[current_user_profile].middlename AS current_user_middlename"
                    );
            }

            if (filter.ContainsKey("id"))
            {
                qb.where("[request].id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.BIGINT);
            }

            if (filter.ContainsKey("process_id"))
            {
                qb.where("[request].process_id=@process_id");
                param["@process_id"] = new Parameter(filter["process_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("stage_id"))
            {
                qb.where("[request].stage_id=@stage_id");
                param["@stage_id"] = new Parameter(filter["stage_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("init_user_id"))
            {
                qb.where("[request].init_user_id=@init_user_id");
                param["@init_user_id"] = new Parameter(filter["init_user_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("current_user_id"))
            {
                qb.where("[request].current_user_id=@current_user_id");
                param["@current_user_id"] = new Parameter(filter["current_user_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("status"))
            {
                qb.where("[request].status=@status");
                param["@status"] = new Parameter(filter["status"], Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static Request ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Request r = new Request();
            r.Id = long.Parse(dt.Rows[0].Field<object>("id").ToString());
            r.ProcessId = dt.Rows[0].Field<int>("process_id");
            r.StageId = dt.Rows[0].Field<int>("stage_id");
            r.InitUserId = dt.Rows[0].Field<int>("init_user_id");
            r.CurrentUserId = dt.Rows[0].Field<int>("current_user_id");
            r.Title = dt.Rows[0].Field<string>("title");
            r.Description = dt.Rows[0].Field<string>("description");
            r.Remark = dt.Rows[0].Field<string>("remark");
            r.Note = dt.Rows[0].Field<string>("note");
            r.CreatedDate = dt.Rows[0].Field<DateTime>("created_date");
            r.ModifiedDate = dt.Rows[0].Field<DateTime>("modified_date");
            r.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());
            r.Lifespan = dt.Rows[0].Field<int>("lifespan");
            r.Ref_No = dt.Rows[0].Field<string>("ref_no");
            r.is_new = false;
            return r;
        }
        #endregion
    }
}