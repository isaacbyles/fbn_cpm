﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Profile : Base
    {
        #region Constructors
        public Profile()
        {

        }

        public Profile(int user_id, string firstname, string lastname, string middlename, string telephone, string initial)
        {
            this.user_id = user_id;
            this.FirstName = firstname;
            this.LastName = lastname;
            this.MiddleName = middlename;
            this.Telephone = telephone;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
            this.Status = Model.Status.ACTIVE;
            this.Initial = initial;
        }
        #endregion

        #region Data Members
        private int user_id;
        private string firstname;
        private string lastname;
        private string middlename;
        private string telephone;
        private DateTime created_date;
        private DateTime modified_date;
        private short status;
        private string Initial;
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"INSERT INTO profile
(user_id, firstname, lastname, middlename, telephone, created_date, modified_date, status, initial)
VALUES (@user_id, @firstname, @lastname, @middlename, @telephone, @created_date, @modified_date, @status, @initial)";
        public const string SQL_UPADTE = "UPDATE profile SET firstname=@firstname, lastname=@lastname, middlename=@middlename, telephone=@telephone, created_date=@created_date, modified_date=@modified_date, status=@status WHERE user_id = @user_id";
        #endregion

        #region Properties
        public int UserId { get { return this.user_id; } set { this.user_id = value; } }
        public string FirstName { get { return this.firstname; } set { this.firstname = Text.trimmer.Replace(value.ToString().ToLower().Trim(), " "); } }
        public string LastName { get { return this.lastname; } set { this.lastname = Text.trimmer.Replace(value.ToString().ToLower().Trim(), " "); } }
        public string MiddleName { get { return this.middlename; } set { this.middlename = Text.trimmer.Replace(value.ToString().ToLower().Trim(), " "); } }
        public string Telephone { get { return this.telephone; } set { this.telephone = value.ToString().Trim(); } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public DateTime ModifiedDate { get { return this.modified_date; } set { this.modified_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@user_id"] = new Parameter(this.user_id, Parameter.ParameterType.INT);
            param["@firstname"] = new Parameter(this.firstname, Parameter.ParameterType.STRING);
            param["@lastname"] = new Parameter(this.lastname, Parameter.ParameterType.STRING);
            param["@middlename"] = new Parameter(this.middlename, Parameter.ParameterType.STRING);
            param["@telephone"] = new Parameter(this.telephone, Parameter.ParameterType.STRING);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);
            param["@initial"] = new Parameter(this.Initial, Parameter.ParameterType.STRING);

            object result = da.Add(Profile.SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            param["@user_id"] = new Parameter(this.user_id, Parameter.ParameterType.INT);
            param["@firstname"] = new Parameter(this.firstname, Parameter.ParameterType.STRING);
            param["@lastname"] = new Parameter(this.lastname, Parameter.ParameterType.STRING);
            param["@middlename"] = new Parameter(this.middlename, Parameter.ParameterType.STRING);
            param["@telephone"] = new Parameter(this.telephone, Parameter.ParameterType.STRING);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            int result = da.ExecuteNonQuery(Profile.SQL_UPADTE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Property Changing Methods
        public void edit(string firstname, string lastname, string middlename, string telephone)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.MiddleName = middlename;
            this.Telephone = telephone;
            this.ModifiedDate = DateTime.Now;
        }
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("profile");

            if (filter.ContainsKey("user_id"))
            {
                qb.where("user_id=@user_id");
                param["@user_id"] = new Parameter(filter["user_id"], Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "user_id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static Profile ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Profile b = new Profile();
            b.UserId = dt.Rows[0].Field<int>("user_id");
            b.FirstName = dt.Rows[0].Field<string>("firstname");
            b.LastName = dt.Rows[0].Field<string>("lastname");
            b.MiddleName = dt.Rows[0].Field<string>("middlename");
            b.Telephone = dt.Rows[0].Field<string>("telephone");
            b.CreatedDate= dt.Rows[0].Field<DateTime>("created_date");
            b.ModifiedDate = dt.Rows[0].Field<DateTime>("modified_date");
            b.Status = short.Parse(dt.Rows[0].Field<object>("status").ToString());

            b.is_new = false;
            return b;
        }

        #endregion
    }
}