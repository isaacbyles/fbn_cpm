﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class RequestTrail : Base
    {
        #region Constructors
        public RequestTrail()
        {

        }

        public RequestTrail(long request_id, int stage_id, int user_id, int time_taken)
        {
            this.RequestId = request_id;
            this.StageId = stage_id;
            this.UserId = user_id;
            this.TimeTaken = time_taken;
            this.CreatedDate = DateTime.Now;
        }
        #endregion

        #region Data Members
        private long id;
        private long request_id;
        private int stage_id;
        private int user_id;
        private int time_taken;
        private DateTime created_date;
        #endregion

        #region Properties
        public long Id { get { return this.id; } set { this.id = value; } }
        public long RequestId { get { return this.request_id; } set { this.request_id = value; } }
        public int StageId { get { return this.stage_id; } set { this.stage_id = value; } }
        public int UserId { get { return this.user_id; } set { this.user_id = value; } }
        public int TimeTaken { get { return this.time_taken; } set { this.time_taken = value; } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        #endregion

        #region Sql Statements
        public const string SQL_INSERT = @"
INSERT INTO [request_trail] 
(request_id, stage_id, user_id, time_taken, created_date)
OUTPUT INSERTED.ID
VALUES (@request_id, @stage_id, @user_id, @time_taken, @created_date);
";
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@request_id"] = new Parameter(this.request_id, Parameter.ParameterType.INT);
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@user_id"] = new Parameter(this.user_id, Parameter.ParameterType.INT);
            param["@time_taken"] = new Parameter(this.time_taken, Parameter.ParameterType.INT);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = long.Parse(result.ToString());
            }

        }

        protected override void _update()
        {
            return;
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[request_trail]");
            qb.column("[request_trail].*");
            qb.orderBy("[request_trail].id", false); // true

            if (fetch_with.Contains("stage"))
            {
                qb.innerJoin("[stage]", "[stage].id = [request_trail].stage_id");
                qb.leftJoin("[stage_form]", "[stage_form].stage_id = [stage].id");
                qb.innerJoin("[form]", "[form].id = [stage_form].form_id");
                qb.columns(
                    "[stage].role_id AS stage_role_id",
                    "[stage].type AS stage_type",
                    "[stage].name AS stage_name",
                    "[stage].description AS stage_description",
                    "[stage].created_date AS stage_created_date",
                    "[stage].modified_date AS stage_modified_date",
                    "[stage].status AS stage_status",
                    "[stage_form].form_id AS stage_form_id",
                    "[form].name AS form_name",
                    "[form].description AS form_description"
                    );
            }

            if (fetch_with.Contains("user"))
            {
                qb.innerJoin("[profile]", "[profile].user_id = [request_trail].user_id");
                qb.columns(
                    "[profile].firstname AS user_firstname",
                    "[profile].lastname AS user_lastname",
                    "[profile].middlename AS user_middlename"
                    );
            }

            if (filter.ContainsKey("request_id"))
            {
                qb.where("[request_trail].request_id=@request_id");
                param["@request_id"] = new Parameter(filter["request_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("stage_id"))
            {
                qb.where("[request_trail].stage_id=@stage_id");
                param["@stage_id"] = new Parameter(filter["stage_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("user_id"))
            {
                qb.where("[request_trail].user_id=@user_id");
                param["@user_id"] = new Parameter(filter["user_id"], Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }
        #endregion
    }
}