﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Branch : Base
    {
         #region Constructor
        public Branch()
        {

        }

        public Branch(string name, string street, string town, string state, string region, string country,string type)
        {
            this.Name = name;
            this.Street = street;
            this.Town = town;
            this.State = state;
            this.Region = region;
            this.Country = country;
            this.Type = type;
            this.ActiveFg = true;
        }
        #endregion

        #region Data Members
        private object id = null;
        private string name = null;
        private string street = null;
        private string town = null;
        private string state = null;
        private string region = null;
        private string country = null;
        private string type = null;
        private bool? active_fg = null;
        #endregion

        #region Properties
        public object Id { get { return this.id; } set { this.id = value; } }
        public string Name { get { return this.name; } set { this.name = Text.trimmer.Replace(value.ToString().ToLower().Trim(), " "); } }
        public string Street { get { return this.street; } set { this.street = value.Trim(); } }
        public string Town { get { return this.town; } set { this.town = value.Trim(); } }
        public string State { get { return this.state; } set { this.state = value.Trim(); } }
        public string Region { get { return this.region; } set { this.region = value.Trim(); } }
        public string Country { get { return this.country; } set { this.country = value.Trim(); } }
        public string Type { get { return this.type; } set { this.type = value.Trim(); } }
        public bool? ActiveFg { get { return this.active_fg; } set { this.active_fg = value; } }
        #endregion

        #region Sql statements
        public const string SQL_INSERT = "INSERT INTO branch (name, active_fg, street, town, state, region, country,[type]) OUTPUT INSERTED.ID VALUES (@name, @active_fg, @street, @town, @state, @region, @country, @type)";
        public const string SQL_UPADTE = "UPDATE branch SET name=@name, active_fg=@active_fg, street=@street, town=@town, state=@state, region=@region, country=@country WHERE id = @id";
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@name"] = new Parameter(this.name, Parameter.ParameterType.STRING);
            param["@street"] = new Parameter(this.street, Parameter.ParameterType.STRING);
            param["@town"] = new Parameter(this.town, Parameter.ParameterType.STRING);
            param["@state"] = new Parameter(this.state, Parameter.ParameterType.STRING);
            param["@region"] = new Parameter(this.region, Parameter.ParameterType.STRING);
            param["@country"] = new Parameter(this.country, Parameter.ParameterType.STRING);
            param["@type"] = new Parameter(this.type, Parameter.ParameterType.STRING);
            param["@active_fg"] = new Parameter(this.active_fg, Parameter.ParameterType.BOOLEAN);

            object result = da.Add(Branch.SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = result;
            }
            
        }

        protected override void _update()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@id"] = new Parameter(this.id, Parameter.ParameterType.INT);
            param["@name"] = new Parameter(this.name, Parameter.ParameterType.STRING);
            param["@street"] = new Parameter(this.street, Parameter.ParameterType.STRING);
            param["@town"] = new Parameter(this.town, Parameter.ParameterType.STRING);
            param["@state"] = new Parameter(this.state, Parameter.ParameterType.STRING);
            param["@region"] = new Parameter(this.region, Parameter.ParameterType.STRING);
            param["@country"] = new Parameter(this.country, Parameter.ParameterType.STRING);
            param["@active_fg"] = new Parameter(this.active_fg, Parameter.ParameterType.BOOLEAN);

            int result = da.ExecuteNonQuery(Branch.SQL_UPADTE, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Property Changing Method
        public void changeDetails(string name, string street, string town, string state, string region, string country)
        {
            this.Name = name;
            this.Street = street;
            this.Town = town;
            this.State = state;
            this.Region = region;
            this.Country = country;
        }
        #endregion

        #region Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("branch");


            if (filter.ContainsKey("id"))
            {
                qb.where("id=@id");
                param["@id"] = new Parameter(filter["id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("active_fg"))
            {
                qb.where("active_fg=@active_fg");
                param["@active_fg"] = new Parameter(filter["active_fg"], Parameter.ParameterType.BOOLEAN);
            }

            if (filter.ContainsKey("name"))
            {
                qb.where("name=@name");
                param["@name"] = new Parameter(Text.trimmer.Replace(filter["name"].ToString().ToLower().Trim(), " "), Parameter.ParameterType.STRING);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();


            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }

        public static Branch ToObject(DataTable dt)
        {
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Branch b = new Branch();

            b.Id = dt.Rows[0].Field<object>("id");
            b.Name = dt.Rows[0].Field<string>("name");
            b.Street = dt.Rows[0].Field<string>("street");
            b.Town = dt.Rows[0].Field<string>("town");
            b.State = dt.Rows[0].Field<string>("state");
            b.Region = dt.Rows[0].Field<string>("region");
            b.Country = dt.Rows[0].Field<string>("country");
            b.ActiveFg = dt.Rows[0].Field<bool?>("active_fg");
            b.is_new = false;
            return b;
        }
        #endregion
    }
}