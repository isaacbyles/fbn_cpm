﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class FormData : Base
    {
        #region Constructors
        public FormData()
        {

        }

        public FormData(int stage_id, int form_id, int form_control_id, long request_id, object value, long request_trail_id)
        {
            this.StageId = stage_id;
            this.FormId = form_id;
            this.FormControlId = form_control_id;
            this.RequestId = request_id;
            this.RequestTrailId = request_trail_id;
            this.Value = value;
            this.Status = Model.Status.ACTIVE;
            this.CreatedDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
        }
        #endregion

        #region Data Members
        private long id;
        private int stage_id;
        private int form_id;
        private int form_control_id;
        private long request_id;
        private long request_trail_id;
        private object value;
        private DateTime created_date;
        private DateTime modified_date;
        private short status;
        #endregion

        #region Data Tables
        public const string T_CHAR = "process_form_char_data";
        public const string T_DATE = "process_form_date_data";
        public const string T_DECIMAL = "process_form_decimal_data";
        public const string T_INT = "process_form_int_data";
        public const string T_TEXT = "process_form_text_data";
        #endregion

        #region Properties
        public long Id { get { return this.id; } set { this.id = value; } }
        public int StageId { get { return this.stage_id; } set { this.stage_id = value; } }
        public int FormId { get { return this.form_id; } set { this.form_id = value; } }
        public int FormControlId { get { return this.form_control_id; } set { this.form_control_id = value; } }
        public long RequestId { get { return this.request_id; } set { this.request_id = value; } }
        public long RequestTrailId { get { return this.request_trail_id; } set { this.request_trail_id = value; } }
        public object Value { get { return this.value; } set { this.value = value; } }
        public DateTime CreatedDate { get { return this.created_date; } set { this.created_date = value; } }
        public DateTime ModifiedDate { get { return this.modified_date; } set { this.modified_date = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }        
        #endregion

        #region TableMapping
        public string TableMap(short form_control_type)
        {
            switch (form_control_type)
            {
                case FormControl.TYPE_CHAR:
                case FormControl.TYPE_CHECKBOX:
                case FormControl.TYPE_DROPDOWN:
                case FormControl.TYPE_RADIO:
                    return T_CHAR;
                case FormControl.TYPE_DATE:
                    return T_DATE;
                case FormControl.TYPE_DECIMAL:
                    return T_DECIMAL;
                case FormControl.TYPE_INT:
                    return T_INT;
                case FormControl.TYPE_TEXT:
                    return T_TEXT;
                default:
                    return null;
            }
        }

        public string getSQLForTable(string sql_template, short form_control_type)
        {
            string table = this.TableMap(form_control_type);
            if (table == null) return null;

            return sql_template.Replace("@table", table);
        }

        public static string getSQLForAllTable(string sql_template)
        {
            string sql = sql_template.Replace("@table", T_CHAR);
            sql += sql_template.Replace("@table", T_DATE);
            sql += sql_template.Replace("@table", T_DECIMAL);
            sql += sql_template.Replace("@table", T_INT);
            sql += sql_template.Replace("@table", T_TEXT);

            return sql;
        }
        #endregion

        #region Sql Statement
        public const string SQL_INSERT = @"
INSERT INTO [@table]
(stage_id, form_id, form_control_id, request_id, request_trail_id, value, created_date, modified_date, status)
OUTPUT INSERTED.ID 
VALUES (@stage_id, @form_id, @form_control_id, @request_id, @request_trail_id, @value, @created_date, @modified_date, @status);
            ";
        public const string SQL_DELETE = @"
DELETE FROM [@table] WHERE request_trail_id=@request_trail_id;
";
        public const string SQL_FETCH = @"
SELECT * FROM [@table] WHERE request_trail_id=@request_trail_id;
";
        #endregion

        #region Database Execution
        public void insertData(short form_control_type)
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@stage_id"] = new Parameter(this.stage_id, Parameter.ParameterType.INT);
            param["@form_id"] = new Parameter(this.form_id, Parameter.ParameterType.INT);
            param["@form_control_id"] = new Parameter(this.form_control_id, Parameter.ParameterType.INT);
            param["@request_id"] = new Parameter(this.request_id, Parameter.ParameterType.BIGINT);
            param["@request_trail_id"] = new Parameter(this.request_trail_id, Parameter.ParameterType.BIGINT);
            param["@value"] = new Parameter(this.value, Parameter.ParameterType.STRING);
            param["@created_date"] = new Parameter(this.created_date, Parameter.ParameterType.DATETIME);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(this.getSQLForTable(SQL_INSERT, form_control_type), param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }
        }

        public static void deleteData(long request_trail_id)
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@request_trail_id"] = new Parameter(request_trail_id, Parameter.ParameterType.BIGINT);

            int result = da.ExecuteNonQuery(getSQLForAllTable(SQL_DELETE), param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
        }
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            return;
        }

        protected override void _update()
        {
            return;
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Fetch Methods
        public static DataSet fetch(long request_trail_id)
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@request_trail_id"] = new Parameter(request_trail_id, Parameter.ParameterType.BIGINT);

            DataSet result = da.Fetch(getSQLForAllTable(SQL_FETCH), param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result;
                }
            }
            return null;
        }
        #endregion
    }
}