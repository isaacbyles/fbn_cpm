﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Models
{
    public class Audit_trail
    {
        public int id { get; set; }
        public string user_id { get; set; }
        public string action { get; set; }
        public string details { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Ip { get; set; }
        public string MacAddress { get; set; }
    }
}