﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Stat
    {
        #region SQL Statements
        public const string SQL_ACTIVE_COUNT = @"
SELECT COUNT(*) AS count FROM [branch] WHERE active_fg=1;
SELECT COUNT(*) AS count FROM [process] WHERE status !=3;
SELECT COUNT(*) AS count FROM [form] WHERE status !=3;
SELECT COUNT(*) AS count FROM [user] WHERE status != 3;
";
        #endregion

        #region Execute Query
        public static DataSet query(string sql)
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result;
                }
            }
            return null;
        }
        #endregion 
    }
}