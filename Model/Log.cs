﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class Log : Base
    {
        #region Constructors
        public Log()
        {

        }

        public Log (int subject_id,int object_id, string action, string old_value, string new_value)
        {
            this.SubjectId = subject_id;
            this.ObjectId = object_id;
            this.Action = action;
            this.OldValue = old_value;
            this.NewValue = new_value;
            this.DateStamp = DateTime.Now;

        }
        #endregion


        #region Data Members
        private int id;
        private string action;
        private string old_value;
        private string new_value;
        private int object_id;
        private int subject_id;
        private DateTime date_stamp;

        #endregion
        
        
        #region Properties
        public int Id { get { return this.id; } set { this.id = value; } }
        public string Action { get { return this.action; } set { this.action = value; } }
        public string OldValue { get { return this.old_value; } set { this.old_value = value; } }
        public string NewValue { get { return this.new_value; } set { this.new_value = value; } }
        public int ObjectId { get { return this.object_id; } set { this.object_id = value; } }
        public int SubjectId { get { return this.subject_id; } set { this.subject_id = value; } }
        
        public DateTime DateStamp { get { return this.date_stamp; } set { this.date_stamp = value; } }
       
        
        #endregion


        #region Sql statements
        public const string SQL_INSERT = @"
INSERT INTO [log]
(stamp, subject_id, object_id, action, old_value, new_value) 
OUTPUT INSERTED.ID 
VALUES (@date_stamp, @subject_id, @object_id, @action, @old_value, @new_value)
";
        
        #endregion


        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@date_stamp"] = new Parameter(this.date_stamp, Parameter.ParameterType.DATETIME);
            param["@subject_id"] = new Parameter(this.subject_id, Parameter.ParameterType.INT);
            param["@object_id"] = new Parameter(this.object_id, Parameter.ParameterType.INT);
            param["@action"] = new Parameter(this.action, Parameter.ParameterType.STRING);
            param["@old_value"] = new Parameter(this.old_value, Parameter.ParameterType.STRING);
            param["@new_value"] = new Parameter(this.new_value, Parameter.ParameterType.STRING);
            

            object result = da.Add(Log.SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                this.id = int.Parse(result.ToString());
            }
        }

        protected override void _update()
        {
            
        }

        public override bool delete()
        {
            return true;
        }
        #endregion
    }
}