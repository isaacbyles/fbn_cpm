﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Util;
using System.Data;

namespace CaseManager.Model
{
    public class StagePrecedence : Base
    {
        #region Constructor
        public StagePrecedence()
        {

        }

        public StagePrecedence(int process_id, int current_id, int next_id)
        {
            this.ProcessId = process_id;
            this.CurrentId = current_id;
            this.NextId = next_id;
            this.Status = Model.Status.ACTIVE;
        }

        public StagePrecedence(int process_id, Dictionary<string, string> pack)
        {
            this.ProcessId = process_id;
            this.CurrentId = int.Parse(pack["from"]);
            this.NextId = int.Parse(pack["to"]);
            this.Status = Model.Status.ACTIVE;
        }
        #endregion

        #region Data Members
        private long id;
        private int process_id;
        private int current_id;
        private int next_id;
        private short status;
        #endregion

        #region Properties
        public long Id { get { return this.id; } set { this.id = value; } }
        public int ProcessId { get { return this.process_id; } set { this.process_id = value; } }
        public int CurrentId { get { return this.current_id; } set { this.current_id = value; } }
        public int NextId { get { return this.next_id; } set { this.next_id = value; } }
        public short Status { get { return this.status; } set { this.status = value; } }
        #endregion

        #region Sql statements
        public const string SQL_INSERT = @"INSERT INTO [stage_precedence]
(process_id, current_id, next_id, status)
OUTPUT INSERTED.ID
VALUES (@process_id, @current_id, @next_id, @status)
";
        #endregion

        #region Inherited Methods
        protected override void _insert()
        {
            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            param["@process_id"] = new Parameter(this.process_id, Parameter.ParameterType.INT);
            param["@current_id"] = new Parameter(this.current_id, Parameter.ParameterType.INT);
            param["@next_id"] = new Parameter(this.next_id, Parameter.ParameterType.INT);
            param["@status"] = new Parameter(this.status, Parameter.ParameterType.INT);

            object result = da.Add(SQL_INSERT, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }

        }

        protected override void _update()
        {
            return;
        }

        public override bool delete()
        {
            return true;
        }
        #endregion

        #region Fetch Methods
        public static DataTable fetch(Dictionary<string, object> filter = null, List<string> fetch_with = null, int? offset = null, int? count = null)
        {
            if (filter == null)
            {
                filter = new Dictionary<string, object>();
            }

            if (fetch_with == null)
            {
                fetch_with = new List<string>();
            }

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();
            QueryBuilder qb = new QueryBuilder();
            qb.from("[stage_precedence]");
            qb.columns(
                "[stage_precedence].id AS stage_precedence_id",
                "[stage_precedence].current_id",
                "[stage_precedence].next_id"
                );

            if (fetch_with.Contains("next_stage"))
            {
                qb.innerJoin("stage AS next_stage", "next_stage.id = [stage_precedence].next_id");
                qb.innerJoin("[role]", "[role].id = [next_stage].role_id");
                qb.leftJoin("[stage_form]", "[stage_form].stage_id = [next_stage].id");
                qb.columns(
                "[next_stage].id AS next_stage_id",
                "[next_stage].process_id AS next_stage_process_id",
                "[next_stage].role_id AS next_stage_role_id",
                "[next_stage].type AS next_stage_type",
                "[next_stage].name AS next_stage_name",
                "[next_stage].description AS next_stage_description",
                "[next_stage].created_date AS next_stage_created_date",
                "[next_stage].modified_date AS next_stage_modified_date",
                "[next_stage].status AS next_stage_status",
                "[stage_form].form_id AS next_stage_form_id",
                "[role].name AS next_stage_role_name"
                );
            }
            else if (fetch_with.Contains("current_stage"))
            {
                qb.innerJoin("stage AS current_stage", "current_stage.id = [stage_precedence].current_id");
                qb.innerJoin("[role]", "[role].id = [current_stage].role_id");
                qb.leftJoin("[stage_form]", "[stage_form].stage_id = [current_stage].id");
                qb.columns(
                "[current_stage].id AS current_stage_id",
                "[current_stage].process_id AS current_stage_process_id",
                "[current_stage].role_id AS current_stage_role_id",
                "[current_stage].type AS current_stage_type",
                "[current_stage].name AS current_stage_name",
                "[current_stage].description AS current_stage_description",
                "[current_stage].created_date AS current_stage_created_date",
                "[current_stage].modified_date AS current_stage_modified_date",
                "[current_stage].status AS current_stage_status",
                "[stage_form].form_id AS current_stage_form_id",
                "[role].name AS current_stage_role_name"
                );
            }

            if (filter.ContainsKey("process_id"))
            {
                qb.where("process_id=@process_id");
                param["@process_id"] = new Parameter(filter["process_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("current_id"))
            {
                qb.where("current_id=@current_id");
                param["@current_id"] = new Parameter(filter["current_id"], Parameter.ParameterType.INT);
            }

            if (filter.ContainsKey("next_id"))
            {
                qb.where("next_id=@next_id");
                param["@next_id"] = new Parameter(filter["next_id"], Parameter.ParameterType.INT);
            }

            string sql = (count != null && offset != null) ? qb.build(offset.Value, count.Value, "id", "result") : qb.build();

            DataSet result = da.Fetch(sql, param);

            if (da.HasError)
            {
                Base.has_error = true;
                Logger.LogError(da.AppError.ToString());
            }
            else
            {
                if (result.Tables.Count != 0)
                {
                    return result.Tables[0];
                }
            }
            return null;
        }
        #endregion
    }
}