<%@ Master Language="C#" AutoEventWireup="true" CodeBehind="Dasboard.master.cs" Inherits="CaseManager.MasterPages.Dasboard" %>

<!DOCTYPE html> 

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <meta charset="utf-8" content="">
     <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="-1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <link href="../assets/css/bootstrap.css" rel="Stylesheet" />
    <link href="../assets/css/metisMenu.min.css" rel="Stylesheet" />
    <link href="../assets/css/timeline.css" rel="Stylesheet" />
    <link href="../assets/css/font-awesome.min.css" rel="Stylesheet" />
    <link href="../assets/css/jquery.datetimepicker.css" rel="Stylesheet" />
    <link href="../assets/css/sb-admin-2.css" rel="Stylesheet" />
    <link href="../assets/css/fullcalendar.min.css" rel="Stylesheet" />
    <link href="../assets/css/processtree.css" rel="Stylesheet" />
    <link href="../assets/css/jqtree.css" rel="Stylesheet" />
    <link href="../assets/css/formbuilder-min.css" rel="Stylesheet" />
    <link href="../assets/css/jquery.dataTables.min.css" rel="Stylesheet" />
    <link href="../LawPavilion/Assets/css/style.css" rel="Stylesheet" />
    
    
  
    <style type="text/css">
        .navbar-top-links > li > a {
            color: #EEBA47;
        }
        /* .btn{
    white-space:normal !important;
    word-wrap:break-word; 
}*/
    </style>
    <!--[if lt IE 9]>
    
    <script>
    els = ['lpelr','cms','stk','total','nav'];
    for(i=0; i < els.length; i++){
        document.createElement(els[i]);
      }
      </script>
      <![endif]-->
    <title></title>
    <asp:ContentPlaceHolder ID="head" runat="server">
    </asp:ContentPlaceHolder>
</head>
<body>

    <!-- Button trigger modal -->

    <form id="form1" runat="server">
        <asp:HiddenField ID="Hidden1" runat="server" />
        <asp:ScriptManager ID="JSScriptTrigger" runat="server" EnablePageMethods="true" />
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="height: 52px; margin-bottom: 0; background-color: #034267;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                <img id="logo" src="../assets/images/logo.png" />
                </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

				<li><a href="#" class="ext-link" id="stk-link"><i class="fa fa-briefcase fa-fw"></i> LawPavilion Solicitors' Toolkit</a></li>
				<li><a href="#" class="ext-link" id="lpelr-link"><i class="fa fa-gavel fa-fw"></i> LawPavilion Electronic Law Reports</a></li>
                <li><a href="#" class="ext-link" id="cms-link"><i class="fa fa-desktop fa-fw"></i> FBN Case Manager</a></li>

                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="user_name"></span>
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                       <%-- <li><a id="all_requests" href="#"><i class="fa fa-tasks fa-fw"></i> All Requests</a>
                        </li>--%>
                        <%--<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>--%>
                        
                        

                        <li><a href="../logout.aspx"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <stk id="stk-1"></stk>
            <lpelr id="lpelr-1"></lpelr>


            <cms>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav">
				<br>

					<div class="input-group input-group-sm" >
					 
					  <span class="input-group-btn">
						<button class="btn btn-success" id='refreshbtn' type="button" style="background-color: #034267; border-color: #034267; margin-left:20px;"><i class="fa fa-refresh"></i> Refresh</button>
					  </span>
					</div><!-- /input-group -->

					<br>
                    <ul class="nav menu_pane scrollbar" id="mainside-menu">

                        <li class="active">
                            <a class="side-menu-link" href="" onclick="javascript:window.location.reload();"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a>
                        </li>

                        <li id="perfection-menu">
                            <a class="collapsed" href="javascript:;" data-toggle="collapse" data-target="#perfection-collapse"><i class='fa fa-arrows-h'></i> Perfection &amp; Securities <span class="caret"></span></a>
                            <ul class="collapse" id="perfection-collapse"></ul>
                        </li>

                        <li id="corporate-menu">
                            <a class="collapsed" href="javascript:;" data-toggle="collapse" data-target="#corporate-collapse"><i class='fa fa-arrows-h'></i> Corporate &amp; Contracts <span class="caret"></span></a>
                            <ul class="collapse" id="corporate-collapse">
                               
                            </ul>
                        </li>

                        <li id="litigation-menu">
                            <a class="collapsed" href="javascript:;" data-toggle="collapse" data-target="#litigation-collapse"><i class='fa fa-arrows-h'></i> Litigation <span class="caret"></span></a>
                            <ul class="collapse" id="litigation-collapse"></ul>
                        </li>

                        <li id="bond-menu">
                            <a class="collapsed" href="javascript:;" data-toggle="collapse" data-target="#bond-collapse"><i class='fa fa-arrows-h'></i> Bonds & Guarantees <span class="caret"></span></a>
                            <ul class="collapse" id="bond-collapse"></ul>
                        </li>
                        
                        <li id="print-menu">
                            <a class="collapsed" href="javascript:;" data-toggle="collapse" data-target="#print-collapse"><i class='fa fa-arrows-h'></i> Templates <span class="caret"></span></a>
                            <ul class="collapse" id="print-collapse"></ul>
                        </li>

                          <li id="hub-menu">
                            <a href="javascript:;" id="hubCordination"><i class='fa fa-arrows-h'></i> Hub Coordination </a>
                            
                        </li>

                         <li id="completed-menu">
                            <a href="javascript:;" id="all_requests"><i class='fa fa-arrows-h'></i> All Completed Requests </a>
                            
                        </li>
                         <li id="all-ongoing-menu">
                            <a href="javascript:;" id="all_ongoing_requests"><i class='fa fa-arrows-h'></i> All Ongoing Requests </a>
                            
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
            </cms>
                           <div>
      <p class = "navbar-text" style="color:White; font-size: 12px;" >
      <asp:Label id="currentUserDesc" runat="server" ></asp:Label>
       
      </p>
   </div>
        </nav>

        <stk id="stk-2"></stk>
        <lpelr id="lpelr-2"></lpelr>


        <cms>
         <div class="page-wrapper" style="min-height:800px; padding-bottom: 60px;">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    <asp:ContentPlaceHolder ID="pageTitle"
                            runat="server">
                        </asp:ContentPlaceHolder>
                    </h1>

                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- /.row -->
            <div id="" class="row">


                <div class="panel panel-primary">
                        <div class="panel-heading">
                            <b id='page-title' class='text-uppercase'>DASHBOARD</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div id="user_page" class="panel-body">

                        <!-- /.panel-body -->
                    </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>


            <div class="modal fade" id="loading_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">

                  <div class="modal-body">
                    <div class="process-state loading">
                        <img src="../assets/images/loading.GIF" />
                    </div>
                     <div class="process-state error alert alert-danger alert_error">

                    </div>
                     <div class="process-state success alert alert-success alert_success">

                    </div>
                  </div>

                </div>
              </div>
            </div>

            <div class="modal fade" id="form_renderer" tabindex="-1" role="dialog" aria-labelledby="formModal">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">New Request</h4>
                  </div>
                  <div id="" class="modal-body">
                    <div id="form_view"></div>

                  </div>
                  <div class="modal-footer">
                    <%--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>--%>
                  </div>
                </div>
              </div>
        </div>


            <div class="modal fade" id="proceed_renderer" tabindex="-1" role="dialog" aria-labelledby="formModal" style="max-height: 800px;overflow-x:wrap;overflow-y:auto">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Modal title</h4>
                  </div>
                  <div id="Div2" class="modal-body">
                    <div id="proceed_form_view"></div>

                  </div>
                  <div class="modal-footer">
                    <%--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>--%>
                  </div>
                </div>
              </div>
        </div>

        <div class="modal fade" id="cl_renderer" tabindex="-1" role="dialog" aria-labelledby="formModal">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="evtHead">Event Detail</h4>
                  </div>
                  <div class="modal-body">
                    <div id="evtContent"></div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="pull-right btn-sm btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                  </div>
                </div>
              </div>
        </div>

        </div>

        <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation" style="height: 52px;background-color: #0a0a0a;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img style="height:30px;" src="../assets/images/official-logo.png" />
                </a>
            </div>
            <!-- /.navbar-header -->
        </nav>
</cms>
       
        
     

    <script type="text/javascript" src="../assets/js/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.js"></script>
        
       <%-- <script type="text/javascript" src="../assets/js/jquery.dform-0.1.4.min.js"></script>--%>

            <script type="text/javascript" src="../assets/js/bootbox.min.js"></script>
        <script type="text/javascript" src="../assets/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../assets/js/jquery.datetimepicker.js"></script>
        <script type="text/javascript" src="../assets/js/jquery.timeago.js"></script>
        <script type="text/javascript" src="../assets/js/tree.jquery.js"></script>
        <script type="text/javascript" src="../assets/js/metisMenu.min.js"></script>
        <script type="text/javascript" src="../assets/js/sb-admin-2.js"></script>
        <script type="text/javascript" src="../assets/js/underscore.js"></script>
        <script type="text/javascript" src="../assets/js/moment.min.js"></script>
        <script type="text/javascript" src="../assets/js/fullcalendar.js"></script>
        <script type="text/javascript" src="../assets/js/PageHandler.js?ver=<%=DateTime.Now.Ticks.ToString() %>"></script>
        <script type="text/javascript" src="../assets/js/templates.js?vers=<%=DateTime.Now.Ticks.ToString() %>"></script>
        <script type="text/javascript" src="../assets/js/UserHandler.js?us=<%=DateTime.Now.Ticks.ToString() %>"></script>
        <%--<script src="../assets/js/idle.js"></script>--%>
        <script type="text/javascript" src="../Lawpavilion/Assets/js/lawpavilion.js"></script>
        <script type="text/javascript" src="../Lawpavilion/Assets/js/numeral.min.js"></script>
        <script src="../assets/js/jquery-ui.min.js" type="text/javascript"></script>
            <script type="text/javascript" src="../assets/js/html5shiv.js"></script>
    <script type="text/javascript" src="../assets/js/respond.js"></script>
        <script type="text/javascript">

            function proxy(d) {
                //var f = JSON.parse(d);
                alert(d);
            }

            $(document).ready(function() {

              

               PageHandler.Handles._setDisplayWindow(PageHandler.Global.adminPane);
                if (UserHandler.user.role_id == "29") {

                    $("#print-menu").remove();
                    $("#hub-menu").remove();
                    $("#completed-menu").remove();
                    $("#mainside-menu  #report_link").hide();
                    $("#all-ongoing-menu").remove();

                }
                else if (UserHandler.user.role_id == "28") {

                    $("#print-menu").remove();
                    $("#hub-menu").remove();
                    $("#completed-menu").remove();
                    $("#mainside-menu  #report_link").hide();
                    $("#all-ongoing-menu").remove();

                }
                else if (UserHandler.user.role_id == "27") {

                    $("#print-menu").remove();
                    $("#hub-menu").remove();
                    $("#completed-menu").remove();
                    $("#mainside-menu  #report_link").hide();
                    $("#all-ongoing-menu").remove();

                }

                else if (UserHandler.user.role_id == "19") {

                    $("#print-menu").remove();
                    $("#hub-menu").remove();
                    $("#completed-menu").remove();
                    $("#mainside-menu  #report_link").hide();
                    $("#all-ongoing-menu").remove();

                }

                $("#form_renderer").on('hidden.bs.modal', function (event) {
                    var modal = $(this);
                    $("#form_id").val(0);
                    modal.find('.modal-title').text('');
                    modal.find('#form_view').text('');
                });
                $('#form_renderer').on('shown.bs.modal', function (event) {
                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var process_id = button.data('process') // Extract info from data-* attributes
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                    var modal = $(this);
                    var process_name = button.data('process_name'); //data-process_name

                   
                    //Added GIT 02-01-2015
                    var stage = UserHandler.process_stages[getIndex(UserHandler.process_stages, process_id)];
                    //------------

                    //var stage = UserHandler.process_stages[process_id];
                    if (stage || UserHandler.user.role_name == "branch office" || UserHandler.user.role_name == "relationship manager" || UserHandler.user.role_name == "branch manager") {
                       
                        var form_id = stage.form_id;

                        $("#form_id").val(form_id); log(form_id);
                        modal.find('.modal-title').text(process_name);
                        modal.find('#form_view').text('Loading... Please wait');

                        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Process.ashx?rand=" + randomString() + "&a=get_workflow&with_next_stage=1&current_id=" + stage.id, {}, PageHandler.HTTPMethods.GET, function (response) {


                        });
                        UserHandler.d.getNextStageDetail(stage.id, function (request_detail) {
                            //log(request_detail.data.data[0]);
                            //log(stage);
                            if (request_detail.status && request_detail.data.status == 1) {
                                PageHandler.getFormsCOntrols(form_id, function (response) {
                                    if (response.status && response.data.status == 1) {

                                        PageHandler.render("#form_view", response.data.data, {
                                            "id": form_id,
                                            "name": "New Request",
                                            "description": "Please fill the fields <br/><br/> Click Submit button before attaching any document.",
                                            "created_date": "2015-07-07T06:38:43",
                                            "modified_date": "2015-07-07T06:38:43",
                                            "status": 1
                                        }, true, { form_id: form_id }, function (form) {
                                            if (request_detail.data.data[0].next_stage_role_id != stage.role_id || request_detail.data.data[0].next_stage_role_id == stage.role_id) {
                                                form.html("<label>Loading... Please wait</label>");
                                             
                                                PageHandler.getRoleUsers(request_detail.data.data[0].next_stage_process_id, request_detail.data.data[0].next_stage_role_id, UserHandler.user.branch_id, function (response1) {
                                                  
                                                    form.html("");

                                                   

                                                    if (request_detail.data.data[0].next_stage_process_id === 23) {
                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append("<label class='hidden'>To:" + request_detail.data.data[0].next_stage_role_name.toUpperCase() + "</label>");
                                                        form.append($("<input type='hidden' name='c[next_user_id]' id='role_users' value='" + UserHandler.user.id + "'/>"));
                                                        //form.append("<input type='hidden' name='' value='Select One' />");
                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        //PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        //PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                        //    var id = $(this).attr("data-id");

                                                        //    window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "Document Upload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                        //});
                                                        // form.append(FormRenderer.renderDateTimePicker("Expected Time of Completion", "lifespan", "", "", "Click to insert date and time", ""));
                                                    }
                                                    else if (request_detail.data.data[0].next_stage_process_id === 24 && request_detail.data.data[0].current_id === 208) {
                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title' /></div>");

                                                        form.append("<label class='hidden'>To:" + request_detail.data.data[0].next_stage_role_name.toUpperCase() + "</label>");
                                                        form.append($("<input type='hidden' name='c[next_user_id]' id='role_users' value='" + UserHandler.user.id + "'/>"));
                                                        //form.append("<input type='hidden' name='' value='Select One' />");
                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");


                                                        //PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        //PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                        //    var id = $(this).attr("data-id");

                                                        //    window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "Document Upload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                        //});


                                                    } // Verification of Probate instrument
                                                    else if (request_detail.data.data[0].next_stage_process_id === 6 && request_detail.data.data[0].current_id === 51) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });
                                                       
                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='"+ _user +"'/>"));
                                                    
                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");
                                                      
                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;
                                                            
                                                            $(PageHandler.Global.attach_doc).trigger(evt);

                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    }
                                                        // search for title deeds
                                                    else if (request_detail.data.data[0].next_stage_process_id === 3 && request_detail.data.data[0].current_id === 43) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });

                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='" + _user + "'/>"));

                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;

                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                            window.resizeTo(300, 200);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    } // verification of incorporate document
                                                    else if (request_detail.data.data[0].next_stage_process_id === 7 && request_detail.data.data[0].current_id === 58) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });

                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='" + _user + "'/>"));

                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;

                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                            window.resizeTo(300, 200);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    } //customer complainint
                                                    else if (request_detail.data.data[0].next_stage_process_id === 10 && request_detail.data.data[0].current_id === 85) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });

                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='" + _user + "'/>"));

                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;

                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                           // window.resizeTo(400, 400);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    } //calling in lending
                                                    else if (request_detail.data.data[0].next_stage_process_id === 12 && request_detail.data.data[0].current_id === 94) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });

                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='" + _user + "'/>"));

                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;

                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                            //window.resizeTo(300, 200);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    }
                                                         // preparation of mortgage deeds
                                                    else if (request_detail.data.data[0].next_stage_process_id === 16 && request_detail.data.data[0].current_id === 138) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });

                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='" + _user + "'/>"));

                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;

                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                           // window.resizeTo(400, 400);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    }
                                                        // preparation of apgs (bank standard format)
                                                    else if (request_detail.data.data[0].next_stage_process_id === 17 && request_detail.data.data[0].current_id === 148) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });

                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='" + _user + "'/>"));

                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;

                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                           // window.resizeTo(500, 500);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    }
                                                        //apgs and bid bonds (not in bank standard format)
                                                    else if (request_detail.data.data[0].next_stage_process_id === 19 && request_detail.data.data[0].current_id === 162) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });

                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='" + _user + "'/>"));

                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;

                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                           // window.resizeTo(300, 200);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    }
                                                        //preparation & execution of deed of release
                                                    else if (request_detail.data.data[0].next_stage_process_id === 20 && request_detail.data.data[0].current_id === 170) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });

                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='" + _user + "'/>"));

                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;

                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                            //window.resizeTo(300, 200);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    }
                                                        //provision of legal advice and drafting
                                                    else if (request_detail.data.data[0].next_stage_process_id === 21 && request_detail.data.data[0].current_id === 175) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });

                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='" + _user + "'/>"));

                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;

                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                           // window.resizeTo(300, 200);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    }
                                                        // bid bonds and apgs (bank standard format)
                                                    else if (request_detail.data.data[0].next_stage_process_id === 24 && request_detail.data.data[0].current_id === 208) {
                                                        var _user = ""; // legal officer
                                                        $.each(response1.data.data, function (i, jsondata) {

                                                            $.each(jsondata, function (ij, data) {
                                                                _user = data.id;

                                                            });
                                                        });

                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append($("<input type='hidden' class='form-control' name='c[next_user_id]' id='role_users' value='" + _user + "'/>"));

                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;

                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                           // window.resizeTo(300, 200);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    }
                                                    else {
                                                        form.append("<div class='form-group mt-10'><label class='text-success'>TITLE</label><input class='form-control' placeholder='Please Specify the Request Title' name='c[title]' id='title'></div>");

                                                        form.append("<label>To:" + request_detail.data.data[0].next_stage_role_name.toUpperCase() + "</label>");
                                                        form.append($("<select class='form-control' name='c[next_user_id]' id='role_users'></select>"));
                                                        //form.append("<input type='hidden' name='' value='Select One' />");
                                                        form.append("<input type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                        form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                        form.append("<input type='hidden' name='c[form_type]' value='start' />");
                                                        form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");
                                                        //form.append(FormRenderer.renderDateTimePicker("Expected Time of Completion", "lifespan", "", "", "Click to insert date and time", ""));
                                                        //form.append(FormRenderer.renderAddUpload(data.id, "xhidden"));

                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("disabled"); // uncomment by Festus


                                                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                            var id = $(this).attr("data-id");
                                                            var evt = jQuery.Event("keypress");
                                                            evt.which = 17;
                                                            evt.ctrlKey = true;
                                                            
                                                            $(PageHandler.Global.attach_doc).trigger(evt);
                                                           // window.resizeTo(300, 200);
                                                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                            //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                            //window.showModalDialog(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1", "DocumentUpload", "center:yes;resizable:yes;dialogWidth:400px;dialogHeight:200px");
                                                        });
                                                    }

                                                    //response1.data.data.forEach(function(v2, i) {
                                                    //    T.bindTo.genericBinding("#role_users", T.templates.userDropDown, v2, function (_response) {
                                                    //        PageHandler.BindToDocument();
                                                    //    });
                                                    //});

                                                    $.each(response1.data.data, function(i, v2) {
                                                        T.bindTo.genericBinding("#role_users", T.templates.userDropDown, v2, function (_response) {
                                                            PageHandler.BindToDocument();
                                                        });
                                                    });
                                                    //MODIFIED BY FESTUS

                                                    //modified by GIT 02-01-2016
                                                   // PageHandler.getFilteredUsers(process_id, UserHandler.user.id, request_detail.data.data[0].next_stage_role_id, UserHandler.user.branch_id, function (response1) {
                                                        
                                                        alert("Click submit before attaching document");
                                                    //});
                                                });
                                               
                                            }

                                        }, function (form) {
                                           
                                            $(".main_btn").removeClass("hidden");
                                            //var el = document.getElementById('mainbtn');
                                            //el.className = el.className.replace("hidden","");
//                                            var attach = "<br/><div  class='btn-group clearfix main_btn ' role='group' >";
//                                            attach += "<button type='submit' class='btn btn-primary trigga_proceed'>Submit</button>";
//                                            attach += "</div><div id='sendingmail' style='width:100%;font-family:verdana, sans-serif;font-size:15px;color:#ff0000'></div>";
//                                            form.append(attach);
//                                           
                                            PageHandler.BindToDocument();
                                        }, true, 0);


                                        //FESTUS COMMENT OUT
                                        // PageHandler.Handles.b(PageHandler.Global.attach_doc).on('click', function (e) {
                                        // alert('CLICKED, BUT DISABLED!!');
                                        //});



                                    } else {
                                        alert("Error rendering this form! Refresh the page. If this continues, then you have to re-create this form");
                                    }

                                });
                            }
                        });

                    }

                })

                // $('#role_users').prepend("<option>Select One..</option>");
                $("#proceed_renderer").on('hidden.bs.modal', function (event) {
                    var modal = $(this);
                    $("#form_id").val(0);
                    modal.find('.modal-title').text('');
                    modal.find('#proceed_form_view').text('');
                });
                $('#proceed_renderer').on('shown.bs.modal', function (event) {
                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var process_id = button.data('process_id') // Extract info from data-* attributes
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                    var modal = $(this);
                    var form_id = button.data('form_id');
                    var data = button.data('payload');
                    if (form_id) {
                        $("#form_id").val(form_id);
                       // log(form_id);
                        modal.find('.modal-title').text('Treat Request - ' + data.form_name.capitalizeFirstLetter());
                        modal.find('#proceed_form_view').text('Loading... Please wait');
                        UserHandler.d.getNextStageDetail(data.stage_id, function (request_detail) {
                            //log("detail---");
                            //log(request_detail);
                            if (request_detail.status && request_detail.data.status == 1) {
                                PageHandler.getFormsCOntrols(form_id, function (response) {

                                    if (response.status && response.data.status == 1) {

                                        PageHandler.render("#proceed_form_view", response.data.data, {
                                            "id": form_id,
                                            "name": data.form_name,
                                            "description": data.form_description,
                                            "created_date": "2015-07-07T06:38:43",
                                            "modified_date": "2015-07-07T06:38:43",
                                            "status": 1
                                        }, false, { form_id: form_id }, function (form) {
                                            var has_next = (typeof request_detail.data.data[0] != "undefined" && typeof request_detail.data.data[0].next_stage_role_id != "undefined");
                                            if (has_next) {

                                                //Next line modified by Laloye 2015-10-09: added second condition
                                                //if (request_detail.data.data[0].next_stage_role_id != data.stage_role_id) {
                                                if (request_detail.data.data[0].next_stage_role_id != data.stage_role_id || request_detail.data.data[0].next_stage_role_id == data.stage_role_id) {
                                                    form.html("<label>Loading... Please wait</label>");
                                                    form.html("");

                                                    FormRenderer.renderSpecialButtons(request_detail.data.data, form);
                                                    form.append(FormRenderer.renderAddUpload(data.id));
                                                    PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                        var id = $(this).attr("data-id");
                                                        var evt = $.Event("keypress");
                                                        evt.keyCode = 17;
                                                        evt.ctrlKey = true;

                                                        $(PageHandler.Global.attach_doc).trigger(evt);
                                                        // window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                        //window.resizeTo(500, 500);
                                                        window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                       // window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=2", "Upload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                    });
                                                    var role_idd = request_detail.data.data[0].next_stage_role_id;

                                                    $.each(request_detail.data.data, function(i1, v1) {
                                                   // request_detail.data.data.forEach(function (v1, i1) {
                                                        //log("v1");
                                                     

                                                        if (i1 === 0 || i1 === 1) {
                                                           
                                                            PageHandler.getRoleUsers(request_detail.data.data[0].next_stage_process_id, request_detail.data.data[0].next_stage_role_id, UserHandler.user.branch_id, function (response1) {
                                                              
                                                                
                                                                var dataq = button.data('payload');
                                                              
                                                                var init_user = dataq.init_user_id;
                                                                var process_id_color = dataq.process_id;
                                                                var requ = dataq.id;
                                                                
                                                                $.each(response1.data.data, function (i, v2) {
                                                                    
                                                                //response1.data.data.forEach(function (v2, i) {
                                                                  
                                                                    var user = UserHandler.user.id;
                                                                    //var end = request_detail.data.data[2].next_stage_id;
                                                                   // alert(end);
                                                                    if (process_id_color === 21 ) {

                                                                        $(".trigga").eq(2).addClass("btn-danger").removeClass("btn-primary");
                                                                    }
                                                                   



                                                                    //form.append("<div class='hidden control_lbl_x label" + v2[i].role_id + "'><label>To:" + v2[i].role_name.toUpperCase() + "</label>");
                                                                    //var user_list = $("<select class='form-control hidden control_lbl_x role_list roles_next" + v2[i].role_id + "' id='proceed_role_users" + v2[i].role_id + "'></select>");
                                                                    //form.append(user_list);

                                                                    //T.bindTo.genericBinding("#proceed_role_users" + v2[i].role_id, T.templates.userDropDown, v2, function (_response) {
                                                                    //    PageHandler.BindToDocument();
                                                                    //    user_list.prepend("<option value='-1' selected >Select...</option>");
                                                                    //});

                                                                  PageHandler.Handles.b(".trigga").unbind('click').on('click', function(e) {
                                                                        //e.preventDefault();
                                                                        var st = $(this).data("stage_id");


                                                                       
                                                                        var user_list = "";
                                                                        var user_list1 = "";
                                                                        var parsed = null;
                                                                        var st = $(this).data("stage_id");
                                                                        // alert(st);

                                                                       
                                                                        var user_list = "";
                                                                        var user_list1 = "";
                                                                        var parsed = null;

                                                                        switch (st) {

                                                                            case 229:
                                                                                $(document).ready(function () {
                                                                                    var postData = { 'request_id': requ }
                                                                                    postData = JSON.stringify(postData);

                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contentType: "application/json; charset=utf-8",
                                                                                        success: function (data) {
                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                           
                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "24" + "' id='proceed_role_users" + "24" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].user_id + "' />");
                                                                                            form.append(user_listqq);
                                                                                            PageHandler.BindToDocument();
                                                                                        }


                                                                                    });
                                                                                });
                                                                                break;
                                                                            // probate [branch user]
                                                                            case 51:
                                                                            case 57:

                                                                                $(document).ready(function () {
                                                                                    var postData = { 'request_id': requ }
                                                                                    postData = JSON.stringify(postData);

                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contentType: "application/json; charset=utf-8",
                                                                                        success: function (data) {
                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                           
                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].init_user_id + "' />");
                                                                                            form.append(user_listqq);
                                                                                            PageHandler.BindToDocument();
                                                                                        }


                                                                                    });
                                                                                });
                                                                                break;



                                                                            case 58:
                                                                            case 62:
                                                                            case 64:
                                                                            case 94:

                                                                                $(document).ready(function () {
                                                                                    var postData = { 'request_id': requ }
                                                                                    postData = JSON.stringify(postData);

                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contentType: "application/json; charset=utf-8",
                                                                                        success: function (data) {
                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                            console.log(team_parsed);
                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].init_user_id + "' />");
                                                                                            form.append(user_listqq);
                                                                                            PageHandler.BindToDocument();
                                                                                        }


                                                                                    });
                                                                                });
                                                                                break;
                                                                            //Send to external solicitor
                                                                            case 53:
                                                                                $(document).ready(function() {
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a="+"getRoleExternal",
                                                                                        type: "GET",
                                                                                        contenttype: "application/json",
                                                                                        success: function(data) {
                                                                                            var parsed_external = $.parseJSON(JSON.stringify(data.ExternalSolicitor));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "11" + "'><label>To:" + "External Solicitor" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "11" + "' id='proceed_role_users" + "11" + "'></select>");
                                                                                           // form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "11" + "' id='proceed_role_users" + "11" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "11", T.templates.userDropDown, parsed_external, function(_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected='selected'>Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;




                                                                            case 60:


                                                                                //yes

                                                                                $(document).ready(function() {
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleExternal",
                                                                                        type: "GET",
                                                                                        contenttype: "application/json",
                                                                                        success: function(data) {
                                                                                            var parsed_external = $.parseJSON(JSON.stringify(data.ExternalSolicitor));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "11" + "'><label>To:" + "External Solicitor" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "11" + "' id='proceed_role_users" + "11" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "11" + "' id='proceed_role_users" + "11" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "11", T.templates.userDropDown, parsed_external, function(_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });

                                                                                break;
                                                                                //To Unit Head

                                                                            case 3:

                                                                                $(document).ready(function() {
                                                                                    var postData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                    postData = JSON.stringify(postData);
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getUnitHead",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contenttype: "application/json",
                                                                                        success: function(data) {
                                                                                            var parsed_unit_head = $.parseJSON(JSON.stringify(data.UsersUnitHead));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "25" + "'><label>To:" + "Unit Head" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'></select>"));

                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "25", T.templates.userDropDown, parsed_unit_head, function(_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });


                                                                                break;

                                                                                // To team Lead
                                                                            case 4:
                                                                            case 15:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request to former Team Lead?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                          
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "24" + "' id='proceed_role_users" + "24" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].user_id + "' /><input id='next_stage'  type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {

                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleTeamLead",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var parsed_team = $.parseJSON(JSON.stringify(data.UsersTeamLead));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "24" + "'><label>To:" + "Team Lead" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "24" + "' id='proceed_role_users" + "24" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "24" + "' id='proceed_role_users" + "24" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "24", T.templates.userDropDown, parsed_team, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });

                                                                                break;

                                                                            case 16:
                                                                                
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    postData = JSON.stringify(postData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getUnitHead",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var parsed_unit_head = $.parseJSON(JSON.stringify(data.UsersUnitHead));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "25" + "'><label>To:" + "Unit Head" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "25", T.templates.userDropDown, parsed_unit_head, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                               // user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            
                                                                                    });

                                                                                }


                                                                            });

                                                                        });

                                                                                break;
                                                                                //Send to external solicitor on Litigation suit Against the Bank
                                                                            case 13:
                                                                            case 21:

                                                                                $(document).ready(function() {
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleExternal",
                                                                                        type: "GET",
                                                                                        contenttype: "application/json",
                                                                                        success: function(data) {
                                                                                            var parsed_role_external = $.parseJSON(JSON.stringify(data.ExternalSolicitor));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "11" + "'><label>To:" + "External Solicitor" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "11" + "' id='proceed_role_users" + "11" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "11" + "' id='proceed_role_users" + "11" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "11", T.templates.userDropDown, parsed_role_external, function(_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;


                                                                            case 12:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request back to former Legal Officer?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[2].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                                // stage 7 - To legal officer

                                                                            case 7:

                                                                           
                                                                            case 18:
                                                                            case 20:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request back to former Legal Officer?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                         
                                                                                                            //
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[2].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                           
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });

                                                                                break;

                                                                            case 19:

                                                                            case 23:
                                                                            case 22:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                           
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                             

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });

                                                                                break;
                                                                            case 14:

                                                                                                $(document).ready(function() {
                                                                            var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                            mypostData = JSON.stringify(mypostData);
                                                                            $.ajax({
                                                                                url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                type: "POST",
                                                                                data: mypostData,
                                                                                contenttype: "application/json",
                                                                                success: function(data) {
                                                                                    var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                    //var userId = UserHandler.user.id;
                                                                                    //var res = removeData(LO, function() { return this.id == userId });
                                                                                    form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");

                                                                                    form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                    T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO , function(_response) {
                                                                                        PageHandler.BindToDocument();



                                                                                    });

                                                                                }


                                                                            });

                                                                        });

                                                                                                       

                                                                                break;

                                                                                // HOD LEGAL
                                                                            case 17:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request to former HOD LEGAL?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var hodLegal = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            // alert(hodLegal[7].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + hodLegal[7].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = {
                                                                                                        'branch_id': UserHandler.user.branch_id,
                                                                                                        'process_id': process_id
                                                                                                    };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getHODLEGAL",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var HOD = $.parseJSON(JSON.stringify(data.HODLEGAL));
                                                                                                            
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "9" + "'><label>To:" + "HOD LEGAL" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "9", T.templates.userDropDown, HOD, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                                //send to branch office
                                                                            
                                                                            case 55:
                                                                            case 57:


                                                                                //bootbox.dialog({
                                                                                //    message: "Do you want to send the request back?",
                                                                                //    title: "Request",
                                                                                //    buttons: {
                                                                                //        success: {
                                                                                //            label: "Yes!",
                                                                                //            className: "btn-success",
                                                                                //            callback: function() {

                                                                                                var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + init_user + "' />");
                                                                                                form.append(user_listqq);
                                                                                                PageHandler.BindToDocument();

                                                                                //            }
                                                                                //        },
                                                                                //        danger: {
                                                                                //            label: "No!",
                                                                                //            className: "btn-danger",
                                                                                //            callback: function() {
                                                                                //                $(document).ready(function() {
                                                                                //                    var postData = { 'branch_id': UserHandler.user.branch_id }
                                                                                //                    postData = JSON.stringify(postData);
                                                                                //                    $.ajax({
                                                                                //                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleBranch",
                                                                                //                        type: "POST",
                                                                                //                        data: postData,
                                                                                //                        contenttype: "application/json",
                                                                                //                        success: function(data) {
                                                                                //                            var parsed_branch = $.parseJSON(JSON.stringify(data.UsersBranch));

                                                                                //                            form.append("<div class='hidden control_lbl_x label" + "29" + "'><label>To:" + "Branch Office" + "</label>");
                                                                                //                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>");
                                                                                //                            //form.append(user_all);
                                                                                //                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>"));
                                                                                //                            T.bindTo.genericBinding("#proceed_role_users" + "29", T.templates.userDropDown, parsed_branch, function(_response) {
                                                                                //                                PageHandler.BindToDocument();
                                                                                //                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                //                            });

                                                                                //                        }


                                                                                //                    });

                                                                                //                });
                                                                                //            }
                                                                                //        }
                                                                                //    }
                                                                                //});

                                                                                break;
                                                                            case 58:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request back?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {

                                                                                                var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + init_user + "' />");
                                                                                                form.append(user_listqq);
                                                                                                PageHandler.BindToDocument();

                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'branch_id': UserHandler.user.branch_id }
                                                                                                    postData = JSON.stringify(postData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleBranch",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var parsed_branch = $.parseJSON(JSON.stringify(data.UsersBranch));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "29" + "'><label>To:" + "Branch Office" + "</label>");
                                                                                                          
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>"));

                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "29", T.templates.userDropDown, parsed_branch, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                             

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;

                                                                                //Garnishee
                                                                            //case 71:

                                                                            //    $(document).ready(function () {
                                                                            //        var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                            //        mypostData = JSON.stringify(mypostData);
                                                                            //        $.ajax({
                                                                            //            url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                            //            type: "POST",
                                                                            //            data: mypostData,
                                                                            //            contenttype: "application/json",
                                                                            //            success: function (data) {
                                                                            //                var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                            //                form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                            //                //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                            //                //form.append(user_all);
                                                                            //                form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                            //                T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function (_response) {
                                                                            //                    PageHandler.BindToDocument();
                                                                            //                    //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                            //                });

                                                                            //            }


                                                                            //        });

                                                                            //    });

                                                                            case 73:
                                                                            case 75:
                                                                            case 81:
                                                                            case 83:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request to former Legal Officer?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            // alert(team_parsed[2].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[4].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;

                                                                                //LEGAL OFFICER GARNISHEE WORK ON REUQEST

                                                                            case 77:
                                                                            case 78:
                                                                            case 79:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            // alert(team_parsed[2].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                      
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function() { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");

                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                           
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                
                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                                //send to Internal Control
                                                                            case 72:
                                                                            case 80:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request to former Internal Control?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "18" + "' id='proceed_role_users" + "18" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[3].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getIC",
                                                                                                        type: "GET",
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.IC));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "18" + "'><label>To:" + "Internal Control" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "18" + "' id='proceed_role_users" + "18" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "18" + "' id='proceed_role_users" + "18" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "18", T.templates.userDropDown, LO, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                                //Unit Head @Garnishee
                                                                            case 74:
                                                                            case 84:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request to former Unit Head?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[1].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    postData = JSON.stringify(postData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getUnitHead",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var parsed_unit_head = $.parseJSON(JSON.stringify(data.UsersUnitHead));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "25" + "'><label>To:" + "Unit Head" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "25", T.templates.userDropDown, parsed_unit_head, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });

                                                                                break;

                                                                                //Appoint Receiver Manager start

                                                                                //Legal officer
                                                                           // case 91:

                                                                            case 92:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            // alert(team_parsed[2].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                           
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                              
                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;


                                                                                //ends


                                                                                // Customer Complaints

                                                                            case 88:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                          
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                              
                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;


                                                                            case 87:
                                                                                $(document).ready(function() {
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getInternalAudit",
                                                                                        type: "GET",
                                                                                        contenttype: "application/json",
                                                                                        success: function(data) {
                                                                                            var LO = $.parseJSON(JSON.stringify(data.IA));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "17" + "'><label>To:" + "Internal Audit" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "17" + "' id='proceed_role_users" + "17" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "17" + "' id='proceed_role_users" + "17" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "17", T.templates.userDropDown, LO, function(_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;


                                                                            case 86:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request to former Legal Officer?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'request_id': requ };
                                                                                                    mypostData = JSON.stringify(mypostData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            // alert(team_parsed[2].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                                //end

                                                                                //VERIFICATION OF PROBATE INSTRUMENT 
                                                                            case 54:
                                                                            case 56:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request to former Legal Officer?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'request_id': requ };
                                                                                                    mypostData = JSON.stringify(mypostData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            // alert(team_parsed[2].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                                //Perfection 
                                                                                //Verification

                                                                            case 43:
                                                                                $(document).ready(function() {
                                                                                    var postData = { 'branch_id': UserHandler.user.branch_id }
                                                                                    postData = JSON.stringify(postData);
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleBranch",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contenttype: "application/json",
                                                                                        success: function(data) {
                                                                                            var LO = $.parseJSON(JSON.stringify(data.UsersBranch));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "29" + "'><label>To:" + "Branch Office" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "29", T.templates.userDropDown, LO, function(_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;

                                                                          
                                                                           
                                                                            case 49:
  $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
break;
                                                                            case 50:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            // alert(team_parsed[0].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                               
                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;


                                                                            case 47:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this report?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            // alert(team_parsed[0].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;

                                                                           
                                                                                //end

                                                                                //CALLING IN LENDING (LITIGATION PROCESS)
                                                                            case 96:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            //if (team_parsed[0].current_user_id === UserHandler.user.id) {
                                                                                                            //    alert("dd");
                                                                                                            //    var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].current_user_id + "' />");
                                                                                                            //    form.append(user_listqq);
                                                                                                            //    PageHandler.BindToDocument();
                                                                                                            //} else {
                                                                                                                var user_listq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                                form.append(user_listq);
                                                                                                                PageHandler.BindToDocument();
                                                                                                            //}
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;


                                                                            case 98:
                                                                            case 147:


                                                                                $(document).ready(function() {
                                                                                    var postData = { 'request_id': requ }
                                                                                    postData = JSON.stringify(postData);

                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contentType: "application/json; charset=utf-8",
                                                                                        success: function(data) {
                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                            // alert(team_parsed[0].init_user_id);
                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].init_user_id + "' />");
                                                                                            form.append(user_listqq);
                                                                                            PageHandler.BindToDocument();
                                                                                        }


                                                                                    });
                                                                                });

                                                                                break;
                                                                                //Mortgage deed and agreements 
                                                                                //start

                                                                            case 141:
                                                                            case 142:
                                                                            case 143:
                                                                            case 144:
                                                                            case 145:
                                                                            case 146:
                                                                            case 196:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            //if (team_parsed[1].current_user_id === UserHandler.user.id) {

                                                                                                            //    var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[1].current_user_id + "' />");
                                                                                                            //    form.append(user_listqq);
                                                                                                            //    PageHandler.BindToDocument();
                                                                                                            //} else {
                                                                                                                var user_listq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                                form.append(user_listq);
                                                                                                                PageHandler.BindToDocument();
                                                                                                            //}
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;


                                                                            //case 147:
                                                                            //    bootbox.dialog({
                                                                            //        message: "Do you want to send the request back?",
                                                                            //        title: "Request",
                                                                            //        buttons: {
                                                                            //            success: {
                                                                            //                label: "Yes!",
                                                                            //                className: "btn-success",
                                                                            //                callback: function() {

                                                                            //                    $(document).ready(function() {
                                                                            //                        var postData = { 'request_id': requ }
                                                                            //                        postData = JSON.stringify(postData);

                                                                            //                        $.ajax({
                                                                            //                            url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                            //                            type: "POST",
                                                                            //                            data: postData,
                                                                            //                            contentType: "application/json; charset=utf-8",
                                                                            //                            success: function(data) {
                                                                            //                                var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                            //                                // alert(team_parsed[0].init_user_id);
                                                                            //                                var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].init_user_id + "' />");
                                                                            //                                form.append(user_listqq);
                                                                            //                                PageHandler.BindToDocument();
                                                                            //                            }


                                                                            //                        });
                                                                            //                    });

                                                                            //                }
                                                                            //            },
                                                                            //            danger: {
                                                                            //                label: "No!",
                                                                            //                className: "btn-danger",
                                                                            //                callback: function() {
                                                                            //                    $(document).ready(function() {
                                                                            //                        var postData = { 'branch_id': UserHandler.user.branch_id }
                                                                            //                        postData = JSON.stringify(postData);
                                                                            //                        $.ajax({
                                                                            //                            url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleBranch",
                                                                            //                            type: "POST",
                                                                            //                            data: postData,
                                                                            //                            contenttype: "application/json",
                                                                            //                            success: function(data) {
                                                                            //                                var parsed_branch = $.parseJSON(JSON.stringify(data.UsersBranch));

                                                                            //                                form.append("<div class='hidden control_lbl_x label" + "29" + "'><label>To:" + "Branch Office" + "</label>");
                                                                            //                                //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>");
                                                                            //                                //form.append(user_all);
                                                                            //                                form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>"));
                                                                            //                                T.bindTo.genericBinding("#proceed_role_users" + "29", T.templates.userDropDown, parsed_branch, function(_response) {
                                                                            //                                    PageHandler.BindToDocument();
                                                                            //                                    //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                            //                                });

                                                                            //                            }


                                                                            //                        });

                                                                            //                    });
                                                                            //                }
                                                                            //            }
                                                                            //        }
                                                                            //    });
                                                                                break;
                                                                                //end


                                                                                //Release of Deed
                                                                            case 173:
                                                                            case 174:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            // alert(team_parsed[1].current_user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",

                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                           
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;

                                                                                //end


                                                                                //Litigation suit by the bank
                                                                            case 24:
                                                                            case 30:
                                                                            case 32:
                                                                            case 36:
                                                                            case 38:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send to the same Legal Officer ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            //team_parsed[0].init_user_id
                                                                                                            //alert(team_parsed[0].init_user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].init_user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",

                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;


                                                                                //to team lead
                                                                            case 35:
                                                                                $(document).ready(function () {
                                                                                    var postData = { 'process_id': process_id, 'branch_id' : UserHandler.user.branch_id }
                                                                                    postData = JSON.stringify(postData);

                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getHODLEGAL",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var HOD = $.parseJSON(JSON.stringify(data.HODLEGAL));
                                                                                            
                                                                                            form.append("<div class='hidden control_lbl_x label" + "9" + "'><label>To:" + "HOD LEGAL" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "9", T.templates.userDropDown, HOD, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;
                                                                            case 25:
                                                                            case 33:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send to the same Team Lead ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            //alert(team_parsed[0].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "24" + "' id='proceed_role_users" + "24" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };  postData = JSON.stringify(postData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleTeamLead",
                                                                                                        type: "POST",
												   
                                                                                                        contenttype: "application/json",
                                                                                                        data: postData, 
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.UsersTeamLead));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "24" + "'><label>To:" + "Team Lead" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "24" + "' id='proceed_role_users" + "24" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "24" + "' id='proceed_role_users" + "24" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "24", T.templates.userDropDown, LO, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });

                                                                                break;

                                                                            case 26:
                                                                                $(document).ready(function() {

                                                                                    var postData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                    postData = JSON.stringify(postData);

                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getUnitHead",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contenttype: "application/json",
                                                                                        success: function(data) {
                                                                                            var parsed_unit_head = $.parseJSON(JSON.stringify(data.UsersUnitHead));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "25" + "'><label>To:" + "Unit Head" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "25", T.templates.userDropDown, parsed_unit_head, function(_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;
                                                                            case 34:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request to former Unit Head?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            // alert(team_parsed[4].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[4].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {

                                                                                                    var postData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getUnitHead",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var parsed_unit_head = $.parseJSON(JSON.stringify(data.UsersUnitHead));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "25" + "'><label>To:" + "Unit Head" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "25" + "' id='proceed_role_users" + "25" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "25", T.templates.userDropDown, parsed_unit_head, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                            case 27:
                                                                                $(document).ready(function () {
                                                                                    var postData = { 'process_id': process_id, 'branch_id' : UserHandler.user.branch_id }
                                                                                    postData = JSON.stringify(postData);

                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getHODLEGAL",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var HOD = $.parseJSON(JSON.stringify(data.HODLEGAL));
          
                                                                                            form.append("<div class='hidden control_lbl_x label" + "9" + "'><label>To:" + "HOD LEGAL" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "9", T.templates.userDropDown, HOD, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                        
                                                                                break;
                                                                            

                                                                                // To external solicitor


                                                                            case 31:
                                                                            case 45:
                                                                            case 39:


                                                                                $(document).ready(function () {
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleExternal",
                                                                                        type: "GET",
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var HOD = $.parseJSON(JSON.stringify(data.ExternalSolicitor));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "11" + "'><label>To:" + "External Solicitor" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "11" + "' id='proceed_role_users" + "11" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "11" + "' id='proceed_role_users" + "11" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "11", T.templates.userDropDown, HOD, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;

                                                                            case 37:

                                                                            case 40:
                                                                            case 41:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            //if (team_parsed[0].init_user_id == UserHandler.user.id) {
                                                                                                            //    var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].init_user_id + "' />");
                                                                                                            //    form.append(user_listqq);
                                                                                                            //    PageHandler.BindToDocument();
                                                                                                            //} else {
                                                                                                                var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                                form.append(user_listqq);
                                                                                                                PageHandler.BindToDocument();
                                                                                                            //}
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;


                                                                            


                                                                                //Bonds and Guarantee Bank standard Format

                                                                            case 220:
                                                                            case 221:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            //if (team_parsed[3].current_user_id == UserHandler.user.id) {
                                                                                                            //    var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[3].current_user_id + "' />");
                                                                                                            //    form.append(user_listqq);
                                                                                                            //    PageHandler.BindToDocument();
                                                                                                            //} else {
                                                                                                                var user_listq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                                form.append(user_listq);
                                                                                                                PageHandler.BindToDocument();
                                                                                                            //}
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;


                                                                                //To Legal Secretary
                                                                            case 222:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send the request to former LEGAL SECRETARY?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var ls = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            //alert(hodLegal[5].user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "27" + "' id='proceed_role_users" + "27" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + ls[3].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLEGALSECRETARY",
                                                                                                        type: "GET",
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LS = $.parseJSON(JSON.stringify(data.HODLEGAL));
                                                                                                         
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "27" + "'><label>To:" + "LEGAL SECRETARY" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "27" + "' id='proceed_role_users" + "27" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "27" + "' id='proceed_role_users" + "27" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "27", T.templates.userDropDown, LS, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;


                                                                                //end
                                                                            case 108:
                                                                                $(document).ready(function () {
                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                        type: "POST",
                                                                                        data: mypostData,
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;

                                                                                //error
                                                                            case 94:

                                                                                $(document).ready(function () {
                                                                                    var postData = { 'branch_id': UserHandler.user.branch_id }
                                                                                    postData = JSON.stringify(postData);
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleBranch",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var LS = $.parseJSON(JSON.stringify(data.UsersBranch));
                                                                                           
                                                                                            form.append("<div class='hidden control_lbl_x label" + "29" + "'><label>To:" + "Branch Office" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "29", T.templates.userDropDown, LS, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;

                                                                            case 120:
                                                                            case 121:
                                                                            case 122:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                   
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                           
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));

                                                                                                           
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                               
                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                            case 197:
                                                                            case 127:
                                                                            case 128:
                                                                            case 129:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            //team_parsed[0].init_user_id
                                                                                                            // alert(team_parsed[3].current_user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                           
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                                //send to legal officer // legal officer work on request

                                                                            case 181:
                                                                            case 190:
                                                                            case 193:
                                                                            case 182:
                                                                            case 186:
                                                                            case 188:
                                                                            case 189:
                                                                            case 192:
                                                                            case 184:
                                                                            case 185:
                                                                            case 187:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }

                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                            case 61:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send back to same legal officer?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                            case 66:


                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));

                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                            case 62:

                                                                                $(document).ready(function () {
                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleBranch",
                                                                                        type: "POST",
                                                                                        data: mypostData,
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var LO = $.parseJSON(JSON.stringify(data.UsersBranch));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "29" + "'><label>To:" + "Branch Office" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "29", T.templates.userDropDown, LO, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                          
                                                                                break;

                                                                                //case 96:
                                                                                //    $(document).ready(function () {
                                                                                //        var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                //        mypostData = JSON.stringify(mypostData);
                                                                                //        $.ajax({
                                                                                //            url: "../API/RequestTrail.ashx?a=" + "getLegalOfficer",
                                                                                //            type: "POST",
                                                                                //            data: mypostData,
                                                                                //            contenttype: "application/json",
                                                                                //            success: function (data) {
                                                                                //                var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                //                form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                //                var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                //                form.append(user_all);

                                                                                //                T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function (_response) {
                                                                                //                    PageHandler.BindToDocument();
                                                                                //                    user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                //                });

                                                                                //            }


                                                                                //        });

                                                                                //    });
                                                                                //    break;
                                                                                //perfection
                                                                            case 101:
                                                                            case 102:
                                                                            case 103:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            //team_parsed[0].init_user_id
                                                                                                            // alert(team_parsed[3].current_user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id+ "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                            case 63:
                                                                            case 201:

                                                                                $(document).ready(function () {
                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                        type: "POST",
                                                                                        data: mypostData,
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, LO, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;
                                                                            case 64:
                                                                                $(document).ready(function () {
                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleBranch",
                                                                                        type: "POST",
                                                                                        data: mypostData,
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var LO = $.parseJSON(JSON.stringify(data.UsersBranch));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "29" + "'><label>To:" + "Branch Office" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "29", T.templates.userDropDown, LO, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                           
                                                                                break;
                                                                            case 97:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                           
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                            case 194:
                                                                                $(document).ready(function () {
                                                                                    var postData = { 'request_id': requ }
                                                                                    postData = JSON.stringify(postData);

                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contentType: "application/json; charset=utf-8",
                                                                                        success: function (data) {
                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                            form.append(user_listqq);
                                                                                            PageHandler.BindToDocument();
                                                                                        }


                                                                                    });
                                                                                });
                                                                                break;
                                                                           
                                                                            case 178:
                                                                            case 179:
                                                                            case 180:
                                                                            


                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            //team_parsed[0].init_user_id
                                                                                                            // alert(team_parsed[3].current_user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;


                                                                                //edit

                                                                            case 166:
                                                                            case 165:
                                                                            case 168:

                                                                                bootbox.dialog({
                                                                                    message: "Do you want to work on this request ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function (data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            //team_parsed[0].init_user_id
                                                                                                            // alert(team_parsed[3].current_user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function () {
                                                                                                $(document).ready(function () {
                                                                                                    var mypostData = { 'branch_id': UserHandler.user.branch_id, 'process_id': process_id };
                                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLegalOfficer",
                                                                                                        type: "POST",
                                                                                                        data: mypostData,
                                                                                                        contenttype: "application/json",
                                                                                                        success: function (data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LegalOfficer));
                                                                                                            var userId = UserHandler.user.id;
                                                                                                            var res = removeData(LO, function () { return this.id == userId });
                                                                                                            form.append("<div class='hidden control_lbl_x label" + "15" + "'><label>To:" + "Legal Officer" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "15", T.templates.userDropDown, res, function (_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;
                                                                            case 167:
                                                                                $(document).ready(function () {
                                                                                    var postData = { 'branch_id': UserHandler.user.branch_id }
                                                                                    postData = JSON.stringify(postData);
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getRoleBranch",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var LS = $.parseJSON(JSON.stringify(data.UsersBranch));
                                                                                            
                                                                                            form.append("<div class='hidden control_lbl_x label" + "29" + "'><label>To:" + "Branch Office" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "29" + "' id='proceed_role_users" + "29" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "29", T.templates.userDropDown, LS, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;

                                                                            case 169:
                                                                                bootbox.dialog({
                                                                                    message: "Do you want to send request back to former Legal Secretary ?",
                                                                                    title: "Request",
                                                                                    buttons: {
                                                                                        success: {
                                                                                            label: "Yes!",
                                                                                            className: "btn-success",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    var postData = { 'request_id': requ }
                                                                                                    postData = JSON.stringify(postData);

                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                                        type: "POST",
                                                                                                        data: postData,
                                                                                                        contentType: "application/json; charset=utf-8",
                                                                                                        success: function(data) {
                                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                                            //team_parsed[0].init_user_id
                                                                                                            // alert(team_parsed[3].current_user_id);
                                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "27" + "' id='proceed_role_users" + "27" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + team_parsed[0].user_id + "' />");
                                                                                                            form.append(user_listqq);
                                                                                                            PageHandler.BindToDocument();
                                                                                                        }


                                                                                                    });
                                                                                                });
                                                                                            }
                                                                                        },
                                                                                        danger: {
                                                                                            //no
                                                                                            label: "No!",
                                                                                            className: "btn-danger",
                                                                                            callback: function() {
                                                                                                $(document).ready(function() {
                                                                                                    $.ajax({
                                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getLEGALSECRETARY",
                                                                                                        type: "GET",
                                                                                                        contenttype: "application/json",
                                                                                                        success: function(data) {
                                                                                                            var LO = $.parseJSON(JSON.stringify(data.LS));

                                                                                                            form.append("<div class='hidden control_lbl_x label" + "27" + "'><label>To:" + "Legal Secretary" + "</label>");
                                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "27" + "' id='proceed_role_users" + "27" + "'></select>");
                                                                                                            //form.append(user_all);
                                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "27" + "' id='proceed_role_users" + "27" + "'></select>"));
                                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "27", T.templates.userDropDown, LO, function(_response) {
                                                                                                                PageHandler.BindToDocument();
                                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                                            });

                                                                                                        }


                                                                                                    });

                                                                                                });
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                });
                                                                                break;

                                                                            case 8:
                                                                            


                                                                                $(document).ready(function () {
                                                                                    var mypostData = {
                                                                                        'branch_id': UserHandler.user.branch_id,
                                                                                        'process_id': process_id
                                                                                    };
                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getHODLEGAL",
                                                                                        type: "POST",
                                                                                        data: mypostData,
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var HOD = $.parseJSON(JSON.stringify(data.HODLEGAL));

                                                                                            form.append("<div class='hidden control_lbl_x label" + "9" + "'><label>To:" + "HOD LEGAL" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "9", T.templates.userDropDown, HOD, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;
                                                                            case 123:
                                                                             
                                                                                $(document).ready(function () {
                                                                                    var mypostData = {
                                                                                        'branch_id': UserHandler.user.branch_id,
                                                                                        'process_id': process_id
                                                                                    };
                                                                                    mypostData = JSON.stringify(mypostData);
                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getHODLEGAL",
                                                                                        type: "POST",
                                                                                        data: mypostData,
                                                                                        contenttype: "application/json",
                                                                                        success: function (data) {
                                                                                            var HOD = $.parseJSON(JSON.stringify(data.HODLEGAL));
                                                                                         

                                                                                            form.append("<div class='hidden control_lbl_x label" + "9" + "'><label>To:" + "HOD LEGAL" + "</label>");
                                                                                            //var user_all = $("<select class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'></select>");
                                                                                            //form.append(user_all);
                                                                                            form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + "9" + "' id='proceed_role_users" + "9" + "'></select>"));
                                                                                            T.bindTo.genericBinding("#proceed_role_users" + "9", T.templates.userDropDown, HOD, function (_response) {
                                                                                                PageHandler.BindToDocument();
                                                                                                //user_all.prepend("<option value='-1' selected >Select...</option>");

                                                                                            });

                                                                                        }


                                                                                    });

                                                                                });
                                                                                break;

                                                                                //branch / bonds and gurarntee
                                                                            case 210:
                                                                            case 211:
                                                                            case 212:
                                                                            case 213:
                                                                            case 214:
                                                                            case 215:
                                                                            case 216:
                                                                            case 217:

                                                                                $(document).ready(function () {
                                                                                    var postData = { 'request_id': requ }
                                                                                    postData = JSON.stringify(postData);

                                                                                    $.ajax({
                                                                                        url: "../API/RequestTrail.ashx?rand=" + randomString() + "&a=" + "getTrailWithReq",
                                                                                        type: "POST",
                                                                                        data: postData,
                                                                                        contentType: "application/json; charset=utf-8",
                                                                                        success: function (data) {
                                                                                            var team_parsed = $.parseJSON(JSON.stringify(data.Users_Team));
                                                                                            var user_listqq = $("<input type='hidden' class='form-control hidden control_lbl_x role_list roles_next" + "15" + "' id='proceed_role_users" + "15" + "'  /> <input id='next_user_id' type='hidden' name='c[next_user_id]' value='" + UserHandler.user.id + "' />");
                                                                                            form.append(user_listqq);
                                                                                            PageHandler.BindToDocument();
                                                                                        }


                                                                                    });
                                                                                });

                                                                                break;
                                                                            default:
                                                                                form.append("<div class='hidden control_lbl_x label" + v2[i].role_id + "'><label>To:" + v2[i].role_name.toUpperCase() + "</label>");
                                                                                //var user_all = $("<select class='hidden form-control control_lbl_x role_list roles_next" + v2[i].role_id + "' id='proceed_role_users" + v2[i].role_id + "'></select>");
                                                                                //form.append(user_all);
                                                                                form.append($("<select class='form-control hidden control_lbl_x role_list roles_next" + v2[i].role_id + "' id='proceed_role_users" + v2[i].role_id + "'></select>"));
                                                                                T.bindTo.genericBinding("#proceed_role_users" + v2[i].role_id, T.templates.userDropDown, v2, function (_response) {
                                                                                    PageHandler.BindToDocument();
                                                                                    //user_all.prepend("<option value='-1' selected >Select...</option>");
                                                                                    //$(".main_btn").removeClass('hidden');
                                                                                });

                                                                                break;
                                                                        }


                                                                    });

                                                                });

                                                            });

                                                        }
                                                    });


                                                    form.append("<input id='next_stage' type='hidden' name='c[next_stage_id]' value='0' />");
                                                    form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                    form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");
                                                    form.append("<input type='hidden' name='c[form_type]' value='proceed' />");
                                                    form.append("<input type='hidden' name='c[req_id]' value='" + data.id + "' />");
                                                    form.append("<input id='next_user_id' type='hidden' name='c[next_user_id]' value='-1' />");

                                                } else {
                                                    form.html("");
                                                    form.append("<input id='next_stage'  type='hidden' name='c[next_stage_id]' value='" + request_detail.data.data[0].next_stage_id + "' />");
                                                    form.append("<input type='hidden' name='c[current_stage_id]' value='" + request_detail.data.data[0].current_id + "' />");
                                                    form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");
                                                    form.append("<input type='hidden' name='c[form_type]' value='proceed' />");
                                                    form.append("<input type='hidden' name='c[req_id]' value='" + data.id + "' />");
                                                    FormRenderer.renderSpecialButtons(request_detail.data.data, form);
                                                    form.append(FormRenderer.renderAddUpload(data.id));
                                                    PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                        var id = $(this).attr("data-id");
                                                        var evt = $.Event("keypress");
                                                        evt.keyCode = 17;
                                                        evt.ctrlKey = true;

                                                        $(PageHandler.Global.attach_doc).trigger(evt);
                                                        window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=2";
                                                       // window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=1";
                                                       // window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=2", "Upload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                    });
                                                }
                                            } else {
                                                //alert('last stage');
                                                form.html("");
                                                form.append("<input type='hidden' name='c[form_id]' value='" + form_id + "' />");
                                                form.append("<input type='hidden' name='c[form_type]' value='end' />");
                                                form.append("<input type='hidden' name='c[req_id]' value='" + data.id + "' />");
                                                form.append("<input type='submit' class='btn btn-primary' name='' value='Request Complete' />");
                                                PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {
                                                    var id = $(this).attr("data-id");
                                                    var evt = $.Event("keypress");
                                                    evt.keyCode = 17;
                                                    evt.ctrlKey = true;

                                                    $(PageHandler.Global.attach_doc).trigger(evt);
                                                    window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=2";
                                                    //window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=2", "Upload", "toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400");
                                                });
                                            }



                                        }, function (form) {
                                            PageHandler.BindToDocument();
                                        }, true, data.id);


                                    } else {
                                        alert("Error rendering this form! Refresh the page. If this continues, then you have to re-create this form");
                                    }

                                });
                            }
                        });

                    }

                })
            });


        </script>
    </form>
    <script type="text/javascript">
        var Item = function () {
            this.form_control_id = 0;
            this.value = "";
            this.form_control_type = 0;
        }


        function submitOverride(d) {
            try {
                var x = ($(d).serializeAssoc());
                //log(x);
                var formD = x.d;
                var formC = x.c;
                if (formC.next_user_id == "-1") {
                    // alert("Please select a user");
                    //return false;
                }

                if (formC.form_type == 'start' && formC.title.trim() == "") {
                    alert("Please enter a Title for this request");
                    return false;
                }
                var d = new Date(formC.lifespan);
                var d2 = new Date();
                var lifespan = Math.ceil((d - d2) / 1000);
                if (isNaN(lifespan)) { lifespan = 0; }
                var control_data = [];
                for (var u in formD) {
                    try{
                        var _item = new Item();
                        _item.form_control_id = u;
                        _item.value = formD[u];
                        _item.form_control_type = UserHandler.currentFormData[u].type;
                    
                        control_data.push(_item);
                    } catch(e){
                        
                    }
                }

                if (formC.form_type == 'start') {
                    var payloadData = {
                        next_stage_id: formC.next_stage_id,
                        next_user_id: (formC.next_user_id == '-1' ? null : formC.next_user_id),
                        title: formC.title,
                        description: '',
                        remark: '',
                        note: '',
                        lifespan: lifespan,
                        form_id: formC.form_id[0],
                        form_data_list: JSON.stringify(control_data)
                    }
                    // Intimate User in form of a progress
                    var sendingmail = document.getElementById("sendingmail");
                    $(sendingmail).empty().html("Notifying Parties, please wait ...");

                    UserHandler.d.createRequest(payloadData, function (r) {
                        if (r.status) {
                            if (r.data.status == 1) {
                                $(sendingmail).empty().html("Completed.");
                                alert("Submitted Successfully.");
                                if (confirm("Do you have any document to attach to this submission?")) {
                                   
                                    PageHandler.Handles.b(PageHandler.Global.attach_doc).attr("data-id", r.data.data).removeClass("disabled");
                                    $(".resetform").trigger('click');
                                } else {
                                    window.location.reload();
                                }
                            } else {
                                $(sendingmail).empty().html(r.data.message);
                                alert(r.data.message);
                            }
                        }

                    });
                } else if (formC.form_type == 'proceed') {
                    var payloadData = {
                        id: formC.req_id,
                        next_stage_id: formC.next_stage_id,
                        next_user_id: (formC.next_user_id == '-1' ? null : formC.next_user_id),
                        form_id: formC.form_id[0],
                        form_data_list: JSON.stringify(control_data)
                    }
                    // Intimate User Sending Mail
                    var sendingmail = document.getElementById("sendingmail");
                    $(sendingmail).empty().html("Notifying Parties, please wait ...");
                    UserHandler.d.proceedRequest(payloadData, function (r) {
                        if (r.status) {
                            if (r.data.status == 1) {
                                $(sendingmail).empty();
                                alert("Submitted Successfully.");
                                window.location.reload();
                            } else {
                                $(sendingmail).empty().html(r.data.message);
                                alert(r.data.message);
                            }
                        }
                    });
                } else if (formC.form_type == 'end') {
                    var payloadData = {
                        id: formC.req_id,
                        form_id: formC.form_id[0],
                        form_data_list: JSON.stringify(control_data)
                    }
                    UserHandler.d.endRequest(payloadData, function (r) {
                        if (r.status) {
                            if (r.data.status == 1) {
                                alert("Submitted Successfully.");
                                window.location.reload();
                            } else {
                                alert(r.data.message);
                            }
                        }
                    });
                }


            } catch (e) {
                //log(e);
                PageHandler.Handles.b(PageHandler.Global.attach_doc).addClass("hidden");
            }
            return false;
        }
        (function () {

            var f = window.location.hash;
            f = f.substr(1, f.length);
            var pageForm = document.getElementsByTagName("form");
            $(pageForm).attr("action", "");
            $(pageForm).attr("onsubmit", "return submitOverride(this);");


            //var tempfxn2 = pageForm.onsubmit;
            //pageForm.onsubmit = function () { alert('test'); return false; }
        })();

        $(document).ready(function () {
            $('#dataTable').dataTable().fnDestroy();
            $('#all_requests').unbind('click').on('click', function () {
                // alert("h");
                $("stk").hide();
                $("cms").show();
                $("lpelr").hide();
                PageHandler.Handles._setDisplayWindow(PageHandler.Global.user_page);
                var link = "../Components/requests.htm";
                PageHandler.Handles.load(PageHandler.Global.user_page, link, function (d) {
                    $('#page-title').html("ALL REQUESTS");

                    var container = $("#allrequests-list > tbody");

                    //container.html("No Request Found.");
                    UserHandler.d.getAllRequestsByUserID(UserHandler.user.id, function (response) {
                        //alert(response);
                        if (response.length) {

                            $.each(response, function(i, v2) {
                                container.append(T.templates.renderAllRequests(v2, i + 1));
                            });
                            
                            UserHandler.bindToDom();
                            PageHandler.BindToDocument();
                            $('#allrequests-list').DataTable();
                        } else {
                            alert("No Requests Found");
                        }
                    });




                    PageHandler.BindToDocument();
                    UserHandler.bindToDom();

                });

                //For Ongoing Requests

               
            });

         
            $('#trade').unbind('click').on('click', function () {
                // alert("d");
                var link = "../Components/trademarks.htm";
                $("stk").hide();
                $("cms").show();
                $("lpelr").hide();
                PageHandler.Handles._setDisplayWindow(PageHandler.Global.user_page);
                PageHandler.Handles.load(PageHandler.Global.user_page, link, function (d) {
                    $('#page-title').html("TRADEMARKS");

                });
            });

            $('#searchBtn').bind('click', function () {

                var term = $('#txtSearch').val();

                if (term.length > 3) {
                    UserHandler.d.doSearch(term, function (response) {

                        var results = response.data.SearchResultItems;

                        if (results.length) {
                            var link = "../Components/search.htm";
                            PageHandler.Handles.load(PageHandler.Global.user_page, link, function (d) {
                                var container = $("#results");
                                container.html("Loading ...");

                                UserHandler.bindToDom();
                            });
                        } else {
                            alert('No Results Found!');
                        }
                    });
                } else {
                    alert('Please enter at least 3 characters to search for!');
                }
            });

            PageHandler.Handles._setDisplayWindow(PageHandler.Global.user_page);
            var link = "../Components/dashboard.htm"; //user_page
            if (link) {

               PageHandler.Handles.load(PageHandler.Global.user_page, link, function (d) {
                            var container = $("#pending_requests");
                            container.html("Loading ...");



                            UserHandler.d.getRequests(UserHandler.user.id, UserHandler.user.branch_id, UserHandler.user.role_id, function (response) {

                                if (response.status) {

                                    container.html("<ul class='list-group '>");

                                    var pending = _.filter(response.data, function (d) { return d.status != 6; });

                                    //pending.forEach(function (v, i) {
                                    //    container.append(T.templates.pendingLists(v, i + 1));
                                    //});

                                    $.each(response.data, function (i, v) {
                                        container.append(T.templates.pendingLists(v, i + 1));
                                    });


                                    container.append('</ul>');

                                    UserHandler.d.getCalendar(function (d) {
                                        var events = [];
                                        _.each(d.data.Processes_Data, function (pd) {
                                            var event = { process_id: pd.Process_Id };

                                            _.each(pd.Process_Data, function (req) {
                                                event.request_id = req.Request_Id;
                                                event.request_title = req.Request_CaseTitle;

                                                _.each(req.Request_Data, function (stage) {
                                                    event.trail_id = stage.Request_Trail_Id;
                                                    event.stage_id = stage.Request_Trail_Stage;
                                                    event.title = stage.Request_Trail_Stage;

                                                    _.each(stage.Request_Trail_Data, function (form) {
                                                        event.form_id = form.Form_Id;
                                                        var hd = nh = ns = na = com = summ = '';
                                                        var fd = form.Form_Data;

                                                        hd = _.find(fd, function (f) { return f.Label.indexOf('date of hearing') == 0; }).Value;
                                                        nh = _.find(fd, function (f) { return f.Label.indexOf('date of next hearing') == 0; }).Value;
                                                        ns = _.find(fd, function (f) { return f.Label.indexOf('stage of next hearing') == 0; }).Value;
                                                        na = _.find(fd, function (f) { return f.Label.indexOf('next action plan') == 0; }).Value;
                                                        com = _.find(fd, function (f) { return f.Label.indexOf('comment') == 0; }).Value;
                                                        summ = _.find(fd, function (f) { return f.Label.indexOf("summary of todays") == 0; }).Value;

                                                        if (hd.length && nh.length) {

                                                            nh = nh.substring(0, nh.lastIndexOf(' '));
                                                            var evtClass = '';

                                                            if (ns.indexOf('judg') > -1) evtClass = "label label-important the-judgs";
                                                            else if (ns.indexOf('rul') > -1) evtClass = "label label-inverse the-rulings";
                                                            else evtClass = "label label-success";

                                                            events.push({
                                                                form_id: parseInt(event.form_id),
                                                                stage_id: parseInt(event.stage_id),
                                                                trail_id: parseInt(event.trail_id),
                                                                "data-id": parseInt(event.request_id),
                                                                process_id: parseInt(event.process_id),
                                                                hearng_date: hd,
                                                                start: moment(nh).add(8, 'hours'),
                                                                title: event.request_title,
                                                                next_hearing: moment(nh),
                                                                next_stage: ns,
                                                                comments: com,
                                                                summary: summ,
                                                                next_action: na,
                                                                className: evtClass,
                                                                "data_url": '../Components/user_requestdetail.htm'
                                                            });
                                                        }
                                                    });
                                                });
                                            });
                                        });

                                        $('#calendar').fullCalendar({
                                            header: {
                                                center: "title",
                                                //left: "agendaDay,agendaWeek,month",
                                                left: "agendaDay,month",
                                                right: "prev,today,next"
                                            },
                                            buttonText: {
                                                today: "Today",
                                                agendaDay: "Day",
                                                agendaWeek: "Week",
                                                month: "Month"
                                            },
                                            // put your options and callbacks here
                                            dayClick: function (date, jsEvent, view) {
                                                //log(date.format("dddd, MMMM Do YYYY, h:mm:ss a"));
                                            },
                                            eventClick: function (calEvent, jsEvent, view) {

                                                var evtModal = $('#cl_renderer').modal({ backdrop: 'static' });
                                                var evtContent = $('#evtContent');
                                                $('#evtHead').html(calEvent.title);

                                                evtContent.html('').append(T.templates.calendarDetail(calEvent));

                                            },
                                            events: events
                                        });
                                    });

                                    PageHandler.BindToDocument();
                                    UserHandler.bindToDom();
                                } else {
                                    $("#requests_home").html("No Pending Request");
                                }
                            });
                            PageHandler.BindToDocument();
                            UserHandler.bindToDom();
                        });
                    
            }

            


           

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


           


            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        });

        $(document).ready(function () {


            $('#receipt_certificate_date').datetimepicker({
                timepicker: false,
                format: 'Y/m/d'
            });

            $('#request_date').datetimepicker({
                timepicker: false,
                format: 'Y/m/d'
            });



            $('#request_filling').datetimepicker({
                timepicker: false,
                format: 'Y/m/d'
            });





            //Hub
            $('#AllHubDataTable').dataTable().fnDestroy();

            $('#hubCordination').unbind('click').on('click', function (e) {
                $("stk").hide();
                $("cms").show();
                $("lpelr").hide();
                PageHandler.Handles._setDisplayWindow(PageHandler.Global.user_page);
                var link = "../Components/all_hub_requests.htm";

                PageHandler.Handles.load(PageHandler.Global.user_page, link, function (d) {
                    $('#page-title').html("ALL REQUESTS");

                    var container = $("#allhubrequests-list > tbody");

                    //container.html("No Request Found.");
                    UserHandler.d.getAllHubRequests(function (response) {
                      

                        var parsed = $.parseJSON(JSON.stringify(response.Hub));
                        var strBuffer = "";
                        $.each(parsed, function (i, data) {
                            var dd = data.created_date.split('T')[0];
                            dd = dd.split('-');
                            dd = dd[2] + "-" + dd[1] + "-" + dd[0];
                            strBuffer = "<tr>" +
                                     "<td>" + (i + 1) + "</td>" +
                                     "<td><span data-process_id='" + data.process_id + "' data-id='" + data.id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm' >" + data.process.ucWords() + "</span></td>" +
                                     "<td><a href='#' data-process_id='" + data.process_id + "' data-id='" + data.id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm'>" + T.templates.returnEmptyForNull(data.title).ucWords() + "</a></td>" +
                                     "<td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.created_by).ucWords() + "</span></td>" +
                                     "<td><span class='detail_link'>" + dd + "</span></td>" +
                                     "<td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.branch).ucWords() + "</span></td>" +
                                     "<td><span class='detail_link'>" + data.current_stage.ucWords() + "</span></td>" +
                                     "<td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.held_by).ucWords() + "</span></td>" +
                                     "<td><span data-payload='" + JSON.stringify(data) + "' class='detail_link'>" + data.status_desc.ucWords() + "</a></td>" +
                                    "</tr>";

                            $("#allhubrequests-list > tbody").append(strBuffer);
                        });

                        UserHandler.bindToDom();
                        PageHandler.BindToDocument();
                        $('#allhubrequests-list').DataTable();

                    });
                    //PageHandler.BindToDocument();
                    //UserHandler.bindToDom();
                  
                });

            });

            $('#dataTable').dataTable().fnDestroy();
            $('#all_ongoing_requests').unbind('click').on('click', function () {
                // alert("h");
                $("stk").hide();
                $("cms").show();
                $("lpelr").hide();
                PageHandler.Handles._setDisplayWindow(PageHandler.Global.user_page);
                var link = "../Components/all_ongoing_request.htm";
                PageHandler.Handles.load(PageHandler.Global.user_page, link, function (d) {
                    $('#page-title').html("ALL ONGOING REQUESTS");

                    var container = $("#all-ongoing-requests-list > tbody");

                    //container.html("No Request Found.");
                    UserHandler.d.getAllOngoingRequestsByUserID(UserHandler.user.id, function (response) {
                        //alert(response);
                        if (response.length) {
                            $.each(response,  function(i, v2) {
                           // response.forEach(function (v2, i) {
                                container.append(T.templates.renderAllOngoingRequests(v2, i + 1));

                            });

                            UserHandler.bindToDom();
                            PageHandler.BindToDocument();
                            $('#all-ongoing-requests-list').DataTable();
                        } else {
                            alert("No Requests Found");
                        }
                    });


                    PageHandler.BindToDocument();
                    UserHandler.bindToDom();

                });
            });


          


        });
    </script>
    
    <!--Timeout code -->
    <script type="text/javascript">
        
        $(document).ready(function () {
            $(window).load(function() {
                if (window.location.href.indexOf('reload') == -1) {
                    window.location.replace(window.location.href + '?reload='+ randomString());
                }
            });

            $("#refreshbtn").on('click', function() {
                location.reload();
            });


        });
       

        function removeData(arr, func) {
            for (var i = 0; i < arr.length; i++) {
                if (func.call(arr[i])) {
                    arr.splice(i, 1);
                    return arr;
                }
            }
        }

      
</script>

</body>
    <head>

<meta http-equiv="pragma" content="no-cache">
</head>
</html>
