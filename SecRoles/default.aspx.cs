﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CaseManager.Util;
using CaseManager.Logic;
using System.Data;
using CaseManager.Model;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;

namespace CaseManager.SecRoles
{
    public partial class _default : System.Web.UI.Page
    {
        private HttpCookie cookie = new HttpCookie("ID");
        private HttpCookie cookie2 = new HttpCookie("RoleID");
        private HttpContext context = HttpContext.Current;
        public Object _roleID;
        public Object _userID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                proceedBtn_Click(sender, e);

            }
            // if (Request.UrlReferrer == null || Request.UrlReferrer.AbsolutePath != "~/")
            if (Request.UrlReferrer == null)
            {
                Auth.clear();
                Response.Redirect("~/");

            }


            foreach (string key in Request.Form.Keys)
            {
                RadioButtonList1.Items.Add(new ListItem(Request.Form[key].ToUpper(), key));
            }
            RadioButtonList1.Items[0].Selected = true;
        }


        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void proceedBtn_Click(object sender, EventArgs e)
        {
            //User u = (User)HttpContext.Current.Session["user"];
            //u.RoleId = Int16.Parse(RadioButtonList1.SelectedItem.Value);
            //Auth.set(u);
            //DataSet user = (DataSet)HttpContext.Current.Session["login_data"];
            //user.Tables["user"].Rows[0]["role_id"] = Int16.Parse(RadioButtonList1.SelectedItem.Value);
            //User u = (User)HttpContext.Current.Session["user"];
            //u.RoleId = Int16.Parse(RadioButtonList1.SelectedItem.Value);
            //For Temporary db
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Upload Document",
               "alert('" + RadioButtonList1.SelectedItem.Value + " ');", true);

            cookie = HttpContext.Current.Request.Cookies["ID"];
            cookie2 = HttpContext.Current.Request.Cookies["RoleID"];
            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                if (cookie != null) _userID = cookie.Value;
            }
            if (_roleID == null)
            {
                if (cookie2 != null) _roleID = cookie2.Value;
            }

            if (_userID != null)
            {
                DataSet result = Temp.GetUserLogin(_userID.ToString());
           

                result.Tables["user"].Rows[0]["role_id"] = Int16.Parse(RadioButtonList1.SelectedItem.Value);
                UserLogic.RoleID = Int16.Parse(RadioButtonList1.SelectedItem.Value);
                string role_user = Convert.ToString(RadioButtonList1.SelectedItem.Value);
                // string new_roleId = u.RoleId.ToString();


                /* FESTUS  */
                //Start

                //

                //UPDATE selected role id in the temp database

                string sql = @"UPDATE tmp_data_login SET role_id = '" + role_user + "' WHERE id = '" + _userID +
                             "'";
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    conn.Open();
                    int i = cmd.ExecuteNonQuery();
                    conn.Close();

                }
                //8/4/2016

                string sql2 = @"UPDATE tmp_data SET RoleId = '" + role_user + "' WHERE id = '" + _userID +
                           "'";
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                using (SqlCommand cmd = new SqlCommand(sql2, conn))
                {
                    conn.Open();
                    int i = cmd.ExecuteNonQuery();
                    conn.Close();
                }


                short role_id = short.Parse(_roleID.ToString());
                //fetching related processes and start stage information
                DataSet process_resultt = Process.loginFetch(role_id);



                foreach (DataRow item in process_resultt.Tables[0].Rows)
                {

                    Temp.SetUserProcess(UserLogic.RoleID.ToString(), item.ItemArray[0].ToString(),
                        item.ItemArray[1].ToString(),
                        item.ItemArray[2].ToString(), item.ItemArray[3].ToString(), item.ItemArray[4].ToString(),
                        item.ItemArray[5].ToString(), item.ItemArray[6].ToString());
                }
                foreach (DataRow item in process_resultt.Tables[1].Rows)
                {

                    Temp.SetStartStage(item.ItemArray[0].ToString(), item.ItemArray[1].ToString(),
                        item.ItemArray[2].ToString(),
                        item.ItemArray[3].ToString(), item.ItemArray[4].ToString(), item.ItemArray[5].ToString(),
                        item.ItemArray[6].ToString(), item.ItemArray[7].ToString(), item.ItemArray[8].ToString(),
                        item.ItemArray[9].ToString(), item.ItemArray[10].ToString(), item.ItemArray[11].ToString());
                }
                //merge process data into user DataSet
                // cookie2 = HttpContext.Current.Request.Cookies["RoleID"];

                DataSet process_result = Temp.GetUserProcess(_roleID.ToString());
                DataSet stage_result = Temp.GetStartStage(_roleID.ToString());

                result.Tables[0].TableName = "user";
                stage_result.Tables[0].TableName = "start_stage";
                process_result.Tables[0].TableName = "process";

                //result.Tables.Remove(process_result.Tables[0].TableName);
                //result.Tables.Remove(stage_result.Tables[0].TableName);
                result.Tables.Add(process_result.Tables[0].Copy());
                result.Tables.Add(stage_result.Tables[0].Copy());


                //Auth.set(uu);
                //Auth.setLoginData(resultt);

                //Ends
                if (UserLogic.RoleID.ToString() == Constant.ROLE_ADMIN.ToString())
                {
                    Response.Redirect("~/Admin/", true);
                }
                else
                {
                    Response.Redirect("~/RM/", true);
                }
            }
        }


    }
}