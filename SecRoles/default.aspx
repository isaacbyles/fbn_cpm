﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="CaseManager.SecRoles._default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link href="../assets/css/bootstrap.min.css" rel="Stylesheet" />
    <link href="../assets/css/metisMenu.min.css" rel="Stylesheet" />
    <link href="../assets/css/sb-admin-2.css" rel="Stylesheet" />

  

    <title>Law Pavilion :: Case Manager</title>
</head>
<body>
    <form id="selectRoleForm" runat="server">
        <div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="login-panel panel panel-default">
                            <img width="100%" src="../assets/images/logo.png" />
                            <div class="panel-heading">
                                <h3 class="panel-title">Multiple roles detected. Select one</h3>
                            </div>
                            <div class="panel-body">
                                <asp:Panel ID="status_message" CssClass="alert hidden" runat="server">
                                <asp:Label ID="alert_message" runat="server" Text="Label"></asp:Label></asp:Panel>
                                <div>
                                    <fieldset>
                                        <div class="form-group">
                                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" onselectedindexchanged="RadioButtonList1_SelectedIndexChanged">

                                            </asp:RadioButtonList>
                                        </div>
                                        <asp:Button ID="proceedBtn" runat="server" Text="Proceed" CssClass="btn btn-lg btn-success btn-block" onclick="proceedBtn_Click" UseSubmitBehavior="false"/>
                                    </fieldset>
                                </div>
                            </div>
                         </div>
                     </div>
                </div>
             </div>
        </div>
    </form>
    
      <script type="text/javascript" src="../assets/js/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../assets/js/metisMenu.min.js"></script>
    <script type="text/javascript" src="../assets/js/sb-admin-2.js"></script>
   
    <script type="text/javascript" src="../assets/js/bootstrap.js"></script>
    <script type="text/javascript" src="../assets/js/html5shiv.js"></script>
    <script type="text/javascript" src="../assets/js/respond.js"></script>
   
</body>
</html>
