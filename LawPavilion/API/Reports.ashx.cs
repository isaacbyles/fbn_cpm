﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for Reports
    /// </summary>
    public class Reports : IHttpHandler
    {



        private Regulation myRegulation;
        private Agency myAgency;
        private LPCMConnection objConnection;
        private DataSet ds;
        private DataRow dRow;
        private DataTable dtRequest;
        private DataTable dtForm;
        private DataTable dtStage;

        int MaxRows;
        int inc = 0;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            //context.Response.Write("Hello World");
            //Agency myAgency = new Agency { Acronym = "CBN", Fullname = Handler(context.Request.PathInfo).ToString() };
            objConnection = new LPCMConnection();


            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);


            context.Response.Write(json);
        }


        private DataSet Handler(string pathinfo)
        {

            string user = "";
            string report = "";
            string type = "";
            //DataSet dsResult;

            String[] parameters = pathinfo.Split('/');
            switch (parameters.Length)
            {

               /**case 2:
                    user = parameters[1];
                    return this.getReports(user);**/

                case 3:
                    user = parameters[1];
                    report = parameters[2];
                    if (report == "main" || report == "others")
                    {
                        return this.getReports(user,report);
                    }
                    else
                    {
                        return this.getRequests(user, report);
                    }
                case 4:
                    user = parameters[1];
                    report = parameters[2];
                    type = parameters[3];
                    return this.getData(user, report,type);

                default:

                    break;

            }

            return null;
        }


        private DataSet getReports(string user,string report)
        {

            objConnection.Sql = "select report_id,report_name,process_id from report join [profile] on "+ 
  "[report].[report_unit] collate SQL_Latin1_General_CP1_CI_AS = [profile].middlename collate SQL_Latin1_General_CP1_CI_AS "+
  "where user_id = "+user+" and [type] = '"+report+"'order by report_name";
            return ds = objConnection.GetConnection;

        }




        private DataSet getRequests(string user, string report)
        {
            objConnection.Sql = "select '' as title, "+
  "request.id as request_id,"+
  "created_date,"+
  "(select concat(firstname,' ',lastname) from [profile] where user_id=init_user_id) as created_by,"+
  "(select name from stage where id = stage_id) as current_stage,"+
  "(select concat(firstname,' ',lastname) from [profile] where user_id=current_user_id) as held_by," +
  "(select top 1 name from stage join stage_precedence on stage.id = stage_precedence.next_id where stage_precedence.process_id = (select process_id from report where report_id ="+report+") order by stage_precedence.id desc) as last_stage," +
  "modified_date "+
  "from request join report on request.process_id = report.process_id "+
  "where report_id = "+report;
            return ds = objConnection.GetConnection;
        }


        private DataSet getData(string user, string report, string type)
        {
           //DataSet dsOutput;
            DataTable dtBuffer;
            DataTable dtBuffer2;
            
            string process_id="";
            string tblName="";

            DataSet dsOutput = new System.Data.DataSet();

            // get process id for report
            objConnection.Sql = "select process_id from [report] where [report_id] = " + report +" and [type] = '"+type+"'";
            objConnection.Table_name = "Table_Data_1";
            dtBuffer = objConnection.GetTable;
            foreach (DataRow row in dtBuffer.Rows)
            {
                process_id = row["process_id"].ToString();
            }


            // get all requests for report
             objConnection.Sql = "select id as request_id from request where process_id ="+ process_id;
            dtBuffer = objConnection.GetTable;
            foreach (DataRow rowReqTrail in dtBuffer.Rows)
            {
                // get all request trail from request
                objConnection.Sql = "select id as request_trail_id, request_id, stage_id from [request_trail] where request_id = " + rowReqTrail["request_id"].ToString() +" order by id asc";
                dtBuffer2 = objConnection.GetTable;
                foreach (DataRow rowReqID in dtBuffer2.Rows)
                {
                    //get data for request trail
                    objConnection.Sql = "SELECT [id]" +
                        ",[stage_id]"+
                        ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name "+
	                    ",(select name from stage where id = stage_id) as stage_name "+
                        ",[form_id]"+
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                            "FROM [process_form_text_data] where request_id = " + rowReqTrail["request_id"].ToString() + " and request_trail_id = " + rowReqID["request_trail_id"].ToString() +
                            " union " +
                            "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name" +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_date_data] where request_id = " + rowReqTrail["request_id"].ToString() + " and request_trail_id = " + rowReqID["request_trail_id"].ToString() +
                       "union " +
                                        "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_decimal_data] where request_id = " + rowReqTrail["request_id"].ToString() + " and request_trail_id = " + rowReqID["request_trail_id"].ToString() +
                       " union " +
                                        "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                         " ,(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_int_data] where request_id = " + rowReqTrail["request_id"].ToString() + " and request_trail_id = " + rowReqID["request_trail_id"].ToString() +
                       "union " +
                    "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",[value]" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_char_data] where request_id = " + rowReqTrail["request_id"].ToString() + " and request_trail_id = " + rowReqID["request_trail_id"].ToString();
                    tblName = "trail_" + rowReqID["request_trail_id"].ToString();
                    objConnection.Table_name = "Step_" + rowReqID["request_trail_id"].ToString();
                    dsOutput.Tables.Add(objConnection.GetTable);

                }
            }


            return dsOutput;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}