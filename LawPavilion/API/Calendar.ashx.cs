﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for Calendar
    /// </summary>
    /// 

    public class Processes {
        public string Name { get; set; }
        public Object[] Processes_Data { get; set; }
    }

    public class Process
    {
        public string Process_Id { get; set; }
        public Object[] Process_Data { get; set; }
    }

    public class FormControl
    {
        public string Control_Id { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
    }


    public class Form
    {
        public string Form_Id { get; set; }
        public object[] Form_Data { get; set; }
    }


    public class Request_Trail
    {
        public string Request_Trail_Id { get; set; }
        public string Request_Trail_Stage { get; set; }
        public object[] Request_Trail_Data { get; set; }
    }


    public class Request
    {
        public string Request_CaseTitle { get; set; }
        public string Request_LegalOfficer { get; set; }
        public string Request_Court { get; set; }
        public string Request_ExternalSolicitor { get; set; }
        public string Request_Id { get; set; }
        public string Request_Process_Id { get; set; }
        public object[] Request_Data { get; set; }
    }

    public class Calendar : IHttpHandler
    {




        private Regulation myRegulation;
        private Agency myAgency;
        private LPCMConnection objConnection;
        private DataSet ds;
        private DataRow dRow;
        private DataTable dtRequest;
        private DataTable dtForm;
        private DataTable dtStage;

        int MaxRows;
        int inc = 0;

        public void ProcessRequest(HttpContext context)
        {

            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";

            objConnection = new LPCMConnection();


            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);


            context.Response.Write(json);
        }


        private object Handler(string pathinfo)
        {

            string user = "";

            //DataSet dsResult;
            String[] parameters = pathinfo.Split('/');
            switch (parameters.Length)
            {

                case 2:
                    user = parameters[1];
                    return this.getCalendar(user);

               

                default:

                    break;

            }

            return null;
        }





        private object getCalendar(string user)
        {

            DataTable dtBuffer;
            DataTable dtBuffer2;
            DataTable dtBuffer3;
            DataTable dtBuffer4;

            // old processes: string[] processes = new string[] {"2","18"};
            string[] processes = new string[] { "1", "2" };
            //string[] stages = new string[] { "140", "151" };
            //string stages = "9,10,28,30,125,128,138,140";

            // old stages: string stages = "30,140";
             string stages = "21,39";

            //string process_id = "";
            string tblName = "";

            DataSet dsOutput = new System.Data.DataSet();
            List<Process> bufferProcess = new List<Process>();
            foreach (string p in processes)
            {
                Process myProcess = new Process();
                myProcess.Process_Id = p;
                // get all requests for processes
                objConnection.Sql = "select [title],id as request_id from request where process_id =" + p;
                dtBuffer = objConnection.GetTable;
                List<Request> bufferRequest = new List<Request>();
                foreach (DataRow rowReq in dtBuffer.Rows)
                {
                    //create request object
                    Request myRequest = new Request();

                    myRequest.Request_CaseTitle = rowReq["title"].ToString();
                    myRequest.Request_Id = rowReq["request_id"].ToString();
                    myRequest.Request_Process_Id = p;

                    //get all request_trail for request 

                    objConnection.Sql = "select id as request_trail_id,stage_id from request_trail where request_id = " + myRequest.Request_Id +" and stage_id in ("+stages+")";
                    dtBuffer2 = objConnection.GetTable;
                    List<Request_Trail> bufferTrail = new List<Request_Trail>();
                    foreach(DataRow rowReqTrail in dtBuffer2.Rows){
                        //create a new Request_Trail Object
                        Request_Trail myRequestTrail = new Request_Trail();
                        myRequestTrail.Request_Trail_Id = rowReqTrail["request_trail_id"].ToString();
                        myRequestTrail.Request_Trail_Stage = rowReqTrail["stage_id"].ToString();
                        //create form for current  request trail
                        objConnection.Sql = "select * from request_form where request_id=" + myRequest.Request_Id + " and stage_id=" + myRequestTrail.Request_Trail_Stage + " and request_trail_id = " + myRequestTrail.Request_Trail_Id;
                        Dictionary<string, object> myTrailDic = new Dictionary<string, object>();
                        dtBuffer3 = objConnection.GetTable;
                        List<Form> bufferForm = new List<Form>();
                        foreach (DataRow rowReqForm in dtBuffer3.Rows)
                        {
                            //create form object to hold controls
                            Form myForm = new Form();
                            myForm.Form_Id = rowReqForm["form_id"].ToString();
                            //get data for each form
                            objConnection.Sql = "SELECT [id]" +
                        ",[stage_id]" +
                        ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                        ",(select name from stage where id = stage_id) as stage_name " +
                        ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                            "FROM [process_form_text_data] where request_id = " + myRequest.Request_Id + " and stage_id = " + myRequestTrail.Request_Trail_Stage + " and request_trail_id = " + myRequestTrail.Request_Trail_Id + " and form_id = " + myForm.Form_Id +
                            " union " +
                            "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name" +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_date_data] where request_id = " + myRequest.Request_Id + " and stage_id = " + myRequestTrail.Request_Trail_Stage + " and request_trail_id = " + myRequestTrail.Request_Trail_Id + " and form_id = " + myForm.Form_Id +
                       "union " +
                                        "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_decimal_data] where request_id = " + myRequest.Request_Id + " and stage_id = " + myRequestTrail.Request_Trail_Stage + " and request_trail_id = " + myRequestTrail.Request_Trail_Id + " and form_id = " + myForm.Form_Id +
                       " union " +
                                        "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                         " ,(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_int_data] where request_id = " + myRequest.Request_Id + " and stage_id = " + myRequestTrail.Request_Trail_Stage + " and request_trail_id = " + myRequestTrail.Request_Trail_Id + " and form_id = " + myForm.Form_Id +
                       "union " +
                    "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",[value]" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_char_data] where request_id = " + myRequest.Request_Id + " and stage_id = " + myRequestTrail.Request_Trail_Stage + " and request_trail_id = " + myRequestTrail.Request_Trail_Id + " and form_id = " + myForm.Form_Id;
                            dtBuffer4 = objConnection.GetTable;
                            Dictionary<string, FormControl> myControlDic = new Dictionary<string, FormControl>();

                            List<FormControl> bufferControl = new List<FormControl>();
                            foreach (DataRow rowFormCon in dtBuffer4.Rows)
                            {
                                
                                //Create control objects
                                FormControl myControl = new FormControl();
                                myControl.Control_Id = rowFormCon["form_control_id"].ToString();
                                myControl.Label = rowFormCon["label"].ToString();
                                myControl.Value = rowFormCon["value"].ToString();
                                //myRequestTrail.Request_Trail_Data[x] = myControl;
                                

                                //myControlDic.Add("Form_"+rowFormCon["form_control_id"].ToString(), myControl);
                                //ends Control dictionary   
                                bufferControl.Add(myControl);
                            }
                            myForm.Form_Data = bufferControl.Cast<object>().ToArray();

                            bufferForm.Add(myForm);
                        }

                        myRequestTrail.Request_Trail_Data = bufferForm.Cast<object>().ToArray();
                        bufferTrail.Add(myRequestTrail);
                    }
                    myRequest.Request_Data = bufferTrail.Cast<object>().ToArray();
                    bufferRequest.Add(myRequest);
                }
                myProcess.Process_Data = bufferRequest.Cast<object>().ToArray();
                bufferProcess.Add(myProcess);
            }

            Processes myProcesses = new Processes();
            myProcesses.Name = "Calendar_Data";
            myProcesses.Processes_Data = bufferProcess.Cast<object>().ToArray();
            return myProcesses;
}
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}