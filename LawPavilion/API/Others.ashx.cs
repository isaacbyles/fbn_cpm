﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for Others
    /// 
    /// </summary>
    /// 
    public class ProcessFields
    {
        public string ProcessId { set; get; }
        public object[] Stages { set; get; }

    }

    public class StageFields
    {
        public string StageId { set; get; }
        public string FormId { set; get; }
        public string[] FormLabels { set; get; }

    }

    public class FormFields
    {
        public string ControlId { set; get; }
        public string ControlLabel { set; get; }


    }

    public class ReportProcess
    {
        public string ProcessId { set; get; }
        public string ProcessName { set; get; }
        public object[] Requests { set; get; }

    }

    public class ReportRequest
    {
        public string RequestId { set; get; }
        public string RequestTitle { set; get; }
        public string RequestStatus { set; get; }
        public object[] Stages { set; get; }
        public External External { get; set; }
        public string LegalOfficer { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }


    }

    public class External
    {
        public string user_email { get; set; }
        public string address { get; set; }
        public string external_name { get; set; }
        public string external_phone { get; set; }

    }

    public class Legal
    {
        public string fullname { get; set; }
    }

    public class ReportStage
    {
        public string StageId { set; get; }
        public Dictionary<string, string> FormData { set; get; }
       

    }

    public class ReportFormData
    {
        public Dictionary<string,string> formData { set; get; }


    }

    public class Others : IHttpHandler
    {
        private LPCMConnection objConnection;
        public static string id;
        public void ProcessRequest(HttpContext context)
        {
           
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            objConnection = new LPCMConnection();
            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);
            context.Response.Write(json);
        }




        private dynamic Handler(string pathinfo)
        {
            string user = "";
            string report = "";
            //DataSet dsResult;
            String[] parameters = pathinfo.Split('/');
            switch (parameters.Length)
            {
                case 2:
                    report = parameters[1];
                    return this.getFields(report);
                case 3:
                    user = parameters[1];
                    report = parameters[2];
                    return this.getOthers(user, report);
                   
                default:
                    break;
            }
            return null;
        }

        private dynamic getFields(string report)
        {
            string[] processes = getProcesses(report);
            List<object> allProcesses = new List<object>();
            foreach (string process in processes)
            {
                ProcessFields myProcessField = new ProcessFields();
                List<object> mySF = new List<object>();
                myProcessField.ProcessId = process;
                DataTable stagesForms = getStageForm(process);
                foreach(DataRow stageform in stagesForms.Rows)
                {
                    //create new stage object
                    StageFields myStageField = new StageFields();
                    List<string> myObj = new List<string>();
                    myStageField.StageId = stageform["stage_id"].ToString();
                    myStageField.FormId = stageform["form_id"].ToString();
                    objConnection.Sql = "select id,label from form_control where form_id = " + myStageField.FormId;
                    DataTable dtBuffer = objConnection.GetTable;
                    foreach (DataRow control in dtBuffer.Rows)
                    {
                        
                        //FormFields myFormControl = new FormFields();
                        //myFormControl.ControlId = control["id"].ToString();
                        //myFormControl.ControlLabel = control["label"].ToString();
                        //myObj.Add(myFormControl);
                        myObj.Add(control["label"].ToString());
                    }
                    //myStageField.FormFields = myObj.Cast<object>().ToArray();
                    myStageField.FormLabels = myObj.Cast<string>().ToArray();
                    mySF.Add(myStageField);
                }
                myProcessField.Stages = mySF.Cast<object>().ToArray();
                allProcesses.Add(myProcessField);
            }
            return allProcesses;

        }

        private object getOthers(string user, string report)
        {
            //get array of processes
            string[] processes = getProcesses(report);
            //create list to load processes
            List<object> processObj = new List<object>();
            External external = new External();
            Legal legal = new Legal();
            string email = null, phone = null, address = null, name = null;
            #region 

            foreach (string process in processes)
            {
                //create a process object
                ReportProcess my_process = new ReportProcess();



                my_process.ProcessId = process;
                my_process.ProcessName = getProcessName(process);

                //create list to hold requests
                List<object> reqObj = new List<object>();
                //get all requests for process
                DataTable allRequests = getRequests(process);

                #region

                foreach (DataRow requestid in allRequests.Rows)
                {
                    //create request object
                    ReportRequest my_request = new ReportRequest();
                    my_request.RequestId = requestid["id"].ToString();
                    my_request.RequestTitle = requestid["title"].ToString();
                    my_request.RequestStatus = requestid["status_desc"].ToString();
                    id = my_request.RequestId;

                    Dictionary<string, string> list = GetListOfUsers(requestid["id"].ToString());
                    if (list.Count == 0)
                    {
                    }
                    else if (list.Count != 0)
                    {
                        email = list["email"];
                        phone = list["phone"];
                        address = list["address"];
                        name = list["name"];
                    }



                    //foreach (DataRow itemRow in list.Rows)
                    //{
                    //    string fname = itemRow["firstname"].ToString();
                    //    string lname = itemRow["lastname"].ToString();
                    //    string fullname = fname + " " + lname;
                    //    external.external_name = fullname.ToString();
                    //    external.user_email = itemRow["user_email"].ToString();
                    //    external.address = itemRow["Address"].ToString();
                    //    external.external_phone = itemRow["external_phone"].ToString();
                    //}




                    my_request.Email = email;


                    my_request.Phone = phone;


                    my_request.Address = address;


                    my_request.Name = name;


                    string getL = GetLegalOfficer(id).ToString();

                    if (getL == "")
                    {
                        my_request.LegalOfficer = "";
                    }
                    else
                    {
                        my_request.LegalOfficer = getL.ToString();
                    }
                    //create list to hold stages
                    List<object> stageObj = new List<object>();
                    //get requestTrailid
                    DataTable dtRT = getRequestsTrails(requestid["id"].ToString()).Tables[0];
                    //get form data from request_form for each trail in requests

                    #region

                    foreach (DataRow drRT in dtRT.Rows)
                    {
                        DataTable dtStage = getStageData(drRT["request_trail_id"].ToString());
                        ReportStage my_reportstage = new ReportStage();
                        //create list to formData 
                        Dictionary<string, string> fdObj = new Dictionary<string, string>();
                        foreach (DataRow drStage in dtStage.Rows)
                        {

                            my_reportstage.StageId = drStage["stage_id"].ToString();
                            if (!fdObj.ContainsKey(drStage["label"].ToString()))
                            {
                                fdObj.Add(drStage["label"].ToString(), drStage["value"].ToString());
                            }
                        }
                        my_reportstage.FormData = fdObj;
                        stageObj.Add(my_reportstage);
                    }

                    #endregion


                    my_request.Stages = stageObj.Cast<object>().ToArray();
                    reqObj.Add(my_request);
                }

                #endregion


                my_process.Requests = reqObj.Cast<object>().ToArray();
                processObj.Add(my_process);
            }

            #endregion

           

            return processObj;
        }

     
        
        private string[] getProcesses(string report_id)
        {
            string processes="";
            objConnection.Sql = "select processes from [report] where [report_id] = " + report_id + " and [type] = 'others'";
            
            DataTable dtBuffer = objConnection.GetTable;
            foreach (DataRow row in dtBuffer.Rows)
            {
                processes = row["processes"].ToString();
            }

            string[] processes_array = processes.Split(',');
            return processes_array;
        }



        private DataTable getRequests(string process_id)
        {
            objConnection.Sql = @"select [id],[title],

                            CASE request.[status]
                            WHEN 6 THEN 'completed'
                            WHEN 5 THEN 'on-going'
                            WHEN 4 THEN 'terminated'
                            END  as [status_desc]
                            
from [request] where [process_id] = " + process_id + "order by created_date desc";
            DataTable dtBuffer = objConnection.GetTable;
            return dtBuffer;
        }


        private DataTable getRequestForm(string request_trail_id)
        {
            objConnection.Sql = "select form_id, id as formData_id from request_form where request_trail_id = " + request_trail_id;
            DataTable dtBuffer = objConnection.GetTable;
            return dtBuffer;
        }


        private DataTable getStageForm(string process_id)
        {
            objConnection.Sql = "select [stage_id],[form_id] from [stage_form] where [stage_id] in (select [id] from [stage] where process_id = " + process_id+" and [status] = 1)";
            DataTable dtBuffer = objConnection.GetTable;
            return dtBuffer;
        }

        private DataSet getRequestsTrails(string request)
        {
            
            if (request == "") { return null; }
            string strQry = @"SELECT min(id) as request_trail_id, stage_id, request_id from request_trail  
 where request_id= " + request + " group by request_id,stage_id order by request_id asc, stage_id asc";
            objConnection.Sql = strQry;
            return objConnection.GetConnection;

        }

        private DataTable getStageData(string request_trail_id) 
        {
            string strQry = @"SELECT [stage_id],[label],cast(ISNULL([value],'') AS NVARCHAR(MAX)) as value
  FROM process_form_char_data join form_control on form_control.id = form_control_id where request_trail_id = " + request_trail_id + @" 
  union
  SELECT [stage_id],[label],cast(ISNULL([value],'') AS NVARCHAR(MAX)) as value
  FROM process_form_date_data join form_control on form_control.id = form_control_id where request_trail_id = " + request_trail_id + @" 
  union
  SELECT [stage_id],[label],cast(ISNULL([value],'') AS NVARCHAR(MAX)) as value
  FROM process_form_decimal_data join form_control on form_control.id = form_control_id where request_trail_id = " + request_trail_id + @" 
  union
  SELECT [stage_id],[label],cast(ISNULL([value],'') AS NVARCHAR(MAX)) as value
  FROM process_form_int_data join form_control on form_control.id = form_control_id where request_trail_id = " + request_trail_id + @"  
  union
  SELECT [stage_id],[label],cast(ISNULL([value],'') AS NVARCHAR(MAX)) as value
  FROM process_form_text_data join form_control on form_control.id = form_control_id where request_trail_id = " + request_trail_id ;
            objConnection.Sql = strQry;
            return objConnection.GetTable;     
        }


        private string getProcessName(string process)
        {

            if (process == "") { return ""; }
            string strQry = @"select [name] from process where id = "+process;
            objConnection.Sql = strQry;
            DataTable dt = objConnection.GetTable;
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0]["name"].ToString();
            }

            return "";

        }


        private Dictionary<string, string> GetListOfUsers(string requestId)
        {
            Dictionary<string, string> external = new Dictionary<string, string>();
            

            string strQry = @"SELECT 
                              [request_id]
                              ,[stage_id]
                              ,[request_trail].[user_id]
                              ,[time_taken]
                              ,[request_trail].[created_date]
	                          , [profile].firstname
	                          , profile.lastname,
		                        role.name as role_name
		                        , [external_solicitors].EMAIL as user_email
		                        ,[external_solicitors].ADDRESS as Address
		                        ,[external_solicitors].FIRM as external_name
	                        ,[external_solicitors].PHONE as external_phone
                          FROM [lpcm].[dbo].[request_trail]
                           inner join [profile] on [profile].user_id = request_trail.user_id
                           inner join [user] on [user].id = [profile].user_id
                           inner join [role] on [role].id = [user].role_id
                           inner join [external_solicitors] on [external_solicitors].id = [user].id
                          where request_id = " + requestId;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(strQry, conn))
            {
                conn.Open();

                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        external.Add("name", rd["external_name"].ToString());
                        external.Add("phone", rd["external_phone"].ToString());
                        external.Add("email", rd["user_email"].ToString());
                        external.Add("address", rd["Address"].ToString());
                    }

                }
                conn.Close();
            }
            return external;  
        }


        public string GetLegalOfficer(string requestID)
        {
           // List<string> list = new List<string>();
            string name = "";
            string strQry = @" SELECT 
                                 CONCAT(profile.firstname, +' '+ profile.lastname) as fullname
		
		
                              FROM [lpcm].[dbo].[request_trail]
                               inner join [profile] on [profile].user_id = request_trail.user_id
                               inner join [user] on [user].id = [profile].user_id
                               inner join [role] on [role].id = [user].role_id
                             
                              where request_id = '" + requestID+"' and role.id = 15";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(strQry, conn))
            {
                conn.Open();

                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        name = rd["fullname"].ToString();
                       // list.Add(name);
                    }
                    
                }

            }
            return name;
            //objConnection.Sql = strQry;
            //return objConnection.GetConnection;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}