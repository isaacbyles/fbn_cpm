﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using CaseManager.Model;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for SearchCount
    /// </summary>
    public class SearchCount : IHttpHandler
    {
        List<SearchResult> _result = new List<SearchResult>();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");



            CaseManager.Search.database sb = new CaseManager.Search.database();
            context.Response.ContentType = "application/json";
            var definition = new { Search = "" };
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();

            var jsonData = JsonConvert.DeserializeAnonymousType(strJson, definition);

            var refNo = jsonData.Search.Trim();

            _result = sb.searchDetails(refNo);

            string json = JsonConvert.SerializeObject(_result, Formatting.Indented);

            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}