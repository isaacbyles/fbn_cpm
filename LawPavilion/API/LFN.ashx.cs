﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for LFN
    /// </summary>
    public class LFN : IHttpHandler
    {

        private StkConnection objConnection;
        private DataSet ds;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");



            context.Response.ContentType = "application/json";
            //context.Response.Write("Hello World");
            //Agency myAgency = new Agency { Acronym = "CBN", Fullname = Handler(context.Request.PathInfo).ToString() };
            objConnection = new StkConnection();


            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);


            context.Response.Write(json);
        }


        private dynamic Handler(string pathinfo)
        {


            string law = "";
            //DataSet dsResult;

            String[] parameters = pathinfo.Split('/');



            switch (parameters.Length)
            {
                case 1:
                    return this.getList();
                case 2:
                    law = parameters[1];

                    return this.getContent(law);
                default:
                    break;
            }
            return null;
        }






        private object getList()
        {
            objConnection.Sql = "SELECT [name],last_amended from area_of_law order by [name]";
            return objConnection.GetConnection;
        }




        private dynamic getContent(string law)
        {
            System.Text.StringBuilder strBuff = new System.Text.StringBuilder("<div class='row'><div class='col-lg-12'>");
            objConnection.Sql = "SELECT * from [laws_sections] where lfntitle = '" + law+"' order by cast(section_Number as money)";

                DataTable dtSection = objConnection.GetTable;

                foreach (DataRow drSection in dtSection.Rows)
                {
                    string strHold = "<div class='row' style='margin-top:20px; margin-bottom:20px; display:block;'><div class='col-lg-1' style='width:20px;'><strong>" + drSection["section_Number"].ToString() + @"</strong></div><div class='col-lg-9' style='text-align:justify;white-space:pre-line;
    border-right: 1px solid #EEE;'>" + drSection["section_Content"].ToString() + "</div><div class='col-lg-2'><i>" + drSection["section_Desc"].ToString() + "</i></div></div>";
                    strBuff.Append(strHold);

                }
                strBuff.Append("</div></div>");
             return strBuff;
            
           

        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}