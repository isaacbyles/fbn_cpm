﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for Forms
    /// </summary>
    /// 

    public class Result
    {
        public object[] Table_Data_1 { get; set; }
    }
    public class FormDetails{
 
        public string pk { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string court { get; set; }
        public string[] industries { get; set; }

    }



    public class Forms : IHttpHandler
    {

        private StkConnection objConnection;
        private DataSet ds;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            //context.Response.Write("Hello World");
            //Agency myAgency = new Agency { Acronym = "CBN", Fullname = Handler(context.Request.PathInfo).ToString() };
            objConnection = new StkConnection();


            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);


            context.Response.Write(json);
        }


        private dynamic Handler(string pathinfo)
        {

            string pk = "";
            string type = "";
            //DataSet dsResult;

            String[] parameters = pathinfo.Split('/');



            switch (parameters.Length)
            {
                case 1:
                    return this.getForms();
                 case 2:
                    pk = parameters[1];
                    if (pk == "industries")
                    {
                        return this.getIndustries();
                    }
                    else
                    {
                        return this.getContent(pk);
                    }
            case 3:
                    pk = parameters[1];
                    type = parameters[2];
                    return this.getOthers(pk,type);

                
                default:
                    
                    break;
                    
            }

            return null;
        }


        private DataSet getIndustries()
        {

            objConnection.Sql = "SELECT [id],[name] from industries order by [name]";
            return ds = objConnection.GetConnection;

        }



        private object getForms()
        {
            DataTable dtTable,dtTable2;
            

            objConnection.Sql = "SELECT [pk],[title],[type],[court] from tbl_form order by [type],[title] asc";
            dtTable = objConnection.GetTable;
            List<FormDetails> bufferRequest = new List<FormDetails>();
            foreach (DataRow rowReq in dtTable.Rows)
            {
                FormDetails myFormDetails = new FormDetails();
                myFormDetails.pk = rowReq["pk"].ToString();
                myFormDetails.title = rowReq["title"].ToString();
                myFormDetails.type = rowReq["type"].ToString();
                myFormDetails.court = rowReq["court"].ToString();
                //get industries for form
                objConnection.Sql = "select industry_id from tbl_form_industry where form_id = " + rowReq["pk"].ToString();
                dtTable2 = objConnection.GetTable;
                List<string> myArray = new List<string>();
                //string[] bufIndie = new string[]{};
                foreach (DataRow rowIndie in dtTable2.Rows)
                {
                    myArray.Add(rowIndie["industry_id"].ToString());
                }

                myFormDetails.industries = myArray.Cast<string>().ToArray();
                
                bufferRequest.Add(myFormDetails);
            }
            Result myResult = new Result();
            myResult.Table_Data_1 = bufferRequest.Cast<object>().ToArray();
            return myResult;
        }




        private DataSet getContent(string pk)
        {
            objConnection.Sql = "SELECT * from [tbl_form] where pk = "+pk;
            return ds = objConnection.GetConnection;
        }

        private DataSet getOthers(string pk, string type)
        {

            switch (type){
                case "case":
                    type = "tbl_form_case";
                    break;
                case "clause":
                    type="tbl_form_clause";
                    break;
                case "industry":
                    type = "tbl_form_industry";
                    break;
                case "law":
                    type = "tbl_form_law";
                    break;
                default:
                    return null;
            }
            if (type == "tbl_form_industry")
            {
                objConnection.Sql = "SELECT industries.name as [industry],["+type+"].* from [" + type + "] left join industries on ["+type+"].industry_id = industries.id where form_id = " + pk;
            }
            else
            {
                objConnection.Sql = "SELECT * from [" + type + "] where form_id = " + pk;
            }
            return ds = objConnection.GetConnection;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}