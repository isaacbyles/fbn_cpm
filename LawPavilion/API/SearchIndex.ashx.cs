﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using CaseManager.Model;
using CaseManager.Search;
using Newtonsoft.Json;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for SearchIndex
    /// </summary>
    public class SearchIndex : IHttpHandler
    {
        List<SearchResult> _result = new List<SearchResult>();
        List<SearchResultQuery> _resultData = new List<SearchResultQuery>();
        public static string _wordSearch = null;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            CaseManager.Search.database sb = new CaseManager.Search.database();
            context.Response.ContentType = "application/json";
            var definition = new { Search = "" };
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            var jsonData = JsonConvert.DeserializeAnonymousType(strJson, definition);
            _wordSearch = jsonData.Search.ToLower();
            _result = sb.search(_wordSearch);


            string json = JsonConvert.SerializeObject(_result, Formatting.Indented);
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}