﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Util;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for MainReports
    /// </summary>
    /// 
    public class Connection
    {
        public LPCMConnection objConnection = new LPCMConnection();
        
    }

    public class AllReports
    {
        public object litigation { get; set; }
        public object perfection { get; set; }
        public object corporate { get; set; }
    }

    public class LitigationReports
    {
        public object[] Reports { get; set; }
    }


    public class PerfectionReports
    {
        public object[] Reports { get; set; }
    }


    public class CorporateReports
    {
        public object[] Reports { get; set; }
    }

    public class Report
    {
        public string report_id{ get; set; }
        public string report_name{ get; set; }
        public string process_id { get; set; }
    }

    
    public class User
    {
        
        public string Id { get; set; }
        public string Hub { get; set; }
        public string Role { get; set; }
        public string Unit { get; set; }
        public void getUser(string id)
        {
            Connection con = new Connection();
            con.objConnection.Sql = @"select [user].id as [id], 
                                        [middlename] as [unit],
                                        [role].[name] as [role], 
                                        [branch].[region] as region, 
                                        branch_id from [user] join [profile] on [user].id = [profile].user_id 
                                        join [branch] on [user].branch_id = [branch].id 
                                        join [role] on [user].role_id = [role].id where [user].id = "+id;
            DataTable dtTable = con.objConnection.GetTable;
            foreach (DataRow row in dtTable.Rows){
                this.Id = row["id"].ToString();
                this.Unit = row["unit"].ToString();
                this.Role = row["role"].ToString();
                //check if in headoffice to set Hub
                if ((int)row["branch_id"] == 332)
                {
                    this.Hub = "HQ";
                }else{
                    this.Hub = row["region"].ToString();
                }
            }
            //return this;
        }
    }



    public class MainReports : IHttpHandler
    {
        
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";

           // objConnection = new LPCMConnection();


            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo, context.Request.Url.AbsoluteUri), Formatting.Indented);


            context.Response.Write(json);
        }

        public dynamic Handler(string pathinfo,string url)
        {
            string[] route = pathinfo.Split('/');
            User user = new User();
            if (route.Length >= 2 && route.Length <= 4)
            {
                //create user object for reports
                //User user = new User();
                user.getUser(route[1]);
                
                
            }
            else
            {
                return null;
            }
            switch(route.Length){
                case 2:
                    return this.getReportsList(user);
                case 3:
                    if (url.Split('?').Length==2){
                        return this.getRequestData(url.Split('?')[1]);
                    }else{
                        return this.getRequestsList(user,route[2]);
                    }
                case 4:
                    //return this.getRequestData(route[1], route[2],route[3]);
                    return this.getRequestsList(user, route[2]);
                default:
                    return null;
            }
            
        }
        private dynamic getReportsList(User user)
        {
             string[] menuItems = filterReports(user);
             string sqlQuery="";
             if (menuItems.Length == 3)
             {
                 //get list of reports that can be initiated by hub office
                 if (user.Role == "branch office")
                 {
                     sqlQuery = @"select distinct report_id,report_name,report.[type],report.process_id,report_unit 
                           from stage join [role] on stage.role_id = [role].id join report on stage.process_id = report.process_id 
                           where [role].name = 'branch office' and report.[type] ='main'";
                 }
                 else
                 {
                     sqlQuery = @"select distinct report_id,report_name,report.[type],report.process_id,report_unit 
                           from stage join [role] on stage.role_id = [role].id join report on stage.process_id = report.process_id 
                           where report.[type] ='main'";
                 }
             }
             else {
                 sqlQuery = @"select distinct report_id,report_name,report.[type],report.process_id,report_unit 
                           from stage join [role] on stage.role_id = [role].id join report on stage.process_id = report.process_id 
                           where report.[type] ='main' and report_unit = '"+user.Unit+"'";
             }
                        Connection con = new Connection();
                        con.objConnection.Sql = sqlQuery;
                        DataTable dtTable = con.objConnection.GetTable;
                        AllReports allReports = new AllReports();
                        LitigationReports litigationReports = new LitigationReports();
                        PerfectionReports perfectionReports = new PerfectionReports();
                        CorporateReports corporateReports = new CorporateReports();
                        List<Report> bufferLit = new List<Report>();
                        List<Report> bufferPerf = new List<Report>();
                        List<Report> bufferCorp = new List<Report>();

                        foreach (DataRow row in dtTable.Rows)
                        {
                            Report myReport = new Report();
                            myReport.report_name = row["report_name"].ToString();
                            myReport.report_id = row["report_id"].ToString();
                            myReport.process_id = row["process_id"].ToString();
                            if (row["report_unit"].ToString() == "litigation")
                            {
                                bufferLit.Add(myReport);
                            }


                            if (row["report_unit"].ToString() == "perfection")
                            {
                                bufferPerf.Add(myReport);
                            }


                            if (row["report_unit"].ToString() == "corporate")
                            {
                                bufferCorp.Add(myReport);
                            }
                        }

                        litigationReports.Reports = bufferLit.Cast<object>().ToArray();
                        perfectionReports.Reports = bufferPerf.Cast<object>().ToArray();
                        corporateReports.Reports = bufferCorp.Cast<object>().ToArray();
                        allReports.litigation = litigationReports;
                        allReports.perfection = perfectionReports;
                        allReports.corporate = corporateReports;
            return allReports;
        }


        private dynamic getRequestsList(User user,string report_id)
        {
           //get role and unit of user
            string userRole = user.Role;
            string userUnit = user.Unit;
            string sqlQuery;

            if (userRole == "executive director" || userRole == "hod legal" || userUnit == "Hub Coordination")
            {
            //return all requests
                sqlQuery = "select request.title, request.[status] as [status], " +
                    "lifespan," +
  "request.id as request_id," +
  "created_date," +
  "(select concat(firstname,' ',lastname) from [profile] where user_id=init_user_id) as created_by," +
  "(select name from stage where id = stage_id) as current_stage," +
  "(select concat(firstname,' ',lastname) from [profile] where user_id=current_user_id) as held_by," +
  "(select top 1 name from stage join stage_precedence on stage.id = stage_precedence.next_id where stage_precedence.process_id = (select process_id from report where report_id =" + report_id + ") order by stage_precedence.id desc) as last_stage," +
  "modified_date " +
  "from request join report on request.process_id = report.process_id " +
  "where report_id = " + report_id;
                Connection con = new Connection();
                con.objConnection.Sql = sqlQuery;
                return con.objConnection.GetTable;
                
            }else if (userRole == "branch office")
            { 
                //return all requests within branch office
                return sameTypeRequests(user, report_id, "branch");
            }
            else
            {
                //return for other users i.e all reports within unit
                return sameTypeRequests(user, report_id, "unit");
            }


            
        }

        private dynamic getRequestData(string request_id)
        {
            DataSet dsOutput = new System.Data.DataSet();
            string tblName = "";
            Connection con= new Connection();
            con.objConnection.Sql = "select id as request_trail_id, request_id, stage_id from [request_trail] where request_id = " + request_id +" order by id asc";
                DataTable dtBuffer2 = con.objConnection.GetTable;
                foreach (DataRow rowReqID in dtBuffer2.Rows)
                {

                    //get data for request trail
                    con.objConnection.Sql = "SELECT [id]" +
                        ",[stage_id]" +
                        ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                        ",(select name from stage where id = stage_id) as stage_name " +
                        ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                            "FROM [process_form_text_data] where request_id = " + request_id + " and request_trail_id = " + rowReqID["request_trail_id"].ToString() +
                            " union " +
                            "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name" +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_date_data] where request_id = " + request_id + " and request_trail_id = " + rowReqID["request_trail_id"].ToString() +
                       "union " +
                                        "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_decimal_data] where request_id = " + request_id + " and request_trail_id = " + rowReqID["request_trail_id"].ToString() +
                       " union " +
                                        "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                         " ,(select label from form_control  where id = form_control_id) as label" +
                          ",cast([value] AS NVARCHAR(MAX)) as value" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_int_data] where request_id = " + request_id + " and request_trail_id = " + rowReqID["request_trail_id"].ToString() +
                       "union " +
                    "SELECT [id]" +
                          ",[stage_id]" +
                          ",(select name from process join request on process.id = request.process_id where request.id = request_id) as process_name " +
                          ",(select name from stage where id = stage_id) as stage_name " +
                          ",[form_id]" +
                          ",[form_control_id]" +
                          ",[request_id]" +
                          ",[request_trail_id]" +
                          ",(select label from form_control  where id = form_control_id) as label" +
                          ",[value]" +
                          ",[created_date]" +
                          ",[modified_date]" +
                          ",[status] " +
                      "FROM [process_form_char_data] where request_id = " + request_id + " and request_trail_id = " + rowReqID["request_trail_id"].ToString();
                    tblName = "trail_" + rowReqID["request_trail_id"].ToString();
                    con.objConnection.Table_name = "Request_Trail_" + rowReqID["request_trail_id"].ToString();
                    dsOutput.Tables.Add(con.objConnection.GetTable);

                }
                return dsOutput;
        }

        public string[] filterReports(User user)
        {

            return new string[] { "litigation", "perfection", "corporate" };
            ////if (user.Hub == "HQ")
            ////{
            ////    if (user.Role == "executive director" || user.Role == "hod legal" || user.Unit == "Hub Coordination")
            ////    {
            ////        return new string[] { "litigation", "perfection", "corporate" };
            ////    }
            ////    else
            ////    {
            ////        return new string[] { user.Unit };
            ////    }
            ////}
            ////else { return new string[] { "litigation", "perfection", "corporate" }; }
        }


        public dynamic sameTypeRequests(User user, string report_id, string type)
        {

            //get process_id of report
            Connection con = new Connection();
            string processId = "";
            con.objConnection.Sql = "select process_id from report where report_id = "+report_id;
            DataTable dt = con.objConnection.GetTable;
            foreach (DataRow row in dt.Rows)
            {
                processId = row["process_id"].ToString();
            }
            //retrieve next_id for start stage for process of report
            string sqlQuery = @"select user_id,
request.title,
request.[status] as [status],
lifespan,
request_id,
request.created_date,
process_id,
(select concat(firstname,' ',lastname) from [profile] where user_id=init_user_id) as created_by,
(select name from stage where id = request.stage_id) as current_stage,
(select concat(firstname,' ',lastname) from [profile] where user_id=current_user_id) as held_by,
(select top 1 name from stage join stage_precedence on stage.id = stage_precedence.next_id where stage_precedence.process_id = " + processId+@" order by stage_precedence.id desc) as last_stage,
modified_date
from request_trail join request on 
request_trail.request_id = request.id 
where request_trail.stage_id in 
(select next_id from stage_precedence where current_id = (select id from stage where process_id = "+processId+" and [type] = 2))";
            con.objConnection.Sql = sqlQuery;
            int x = 0;
            dt = con.objConnection.GetTable;
            foreach (DataRow row in dt.Rows)
            {
                User nextUser = new User();
                nextUser.getUser(row["user_id"].ToString());
                if (type == "unit")
                {
                    if (nextUser.Unit != user.Unit)
                    {
                        //remove row from table
                        dt.Rows[x].Delete();
                    }
                }
                else if(type=="branch")
                {
                    if (nextUser.Hub != user.Hub)
                    {
                        //remove row from table
                        dt.Rows[x].Delete();
                    }
                }
                x = x + 1;
            }
            dt.Columns.RemoveAt(0);
            dt.AcceptChanges();
            return dt;
        }



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    
}