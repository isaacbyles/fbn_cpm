﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for CPR
    /// </summary>
    public class CPR : IHttpHandler
    {

        private LPELRConnection objConnection;
        private DataSet ds;

        public void ProcessRequest(HttpContext context)
        {

            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");



            context.Response.ContentType = "application/json";
            //context.Response.Write("Hello World");
            //Agency myAgency = new Agency { Acronym = "CBN", Fullname = Handler(context.Request.PathInfo).ToString() };
            objConnection = new LPELRConnection();


            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);


            context.Response.Write(json);
        }


        private dynamic Handler(string pathinfo)
        {

            
            string rule_id = "";
            //DataSet dsResult;

            String[] parameters = pathinfo.Split('/');



            switch (parameters.Length)
            {
                case 1:
                    return this.getList();
                case 2:
                    rule_id = parameters[1];
                    
                    return this.getContent(rule_id);
                default:
                    break;
            }
            return null;
        }






        private object getList()
        {
            objConnection.Sql = "SELECT rule_id,[title],[year] from rules order by [title]";
            return objConnection.GetConnection;
        }




        private dynamic getContent(string rule_id)
        {
            System.Text.StringBuilder strBuff = new System.Text.StringBuilder("");
            objConnection.Sql = "SELECT * from [rules] where rule_id = " + rule_id;
            DataTable dtRule = objConnection.GetTable;
            string fmatter = dtRule.Rows[0]["frontmatter"].ToString() ?? "";
            string ematter = dtRule.Rows[0]["endmatter"].ToString() ?? "";

            objConnection.Sql = "SELECT * from [orders_table] where rule_id = " + rule_id + " order by order_number";
            DataTable dtOrder = objConnection.GetTable;
            //strBuff.Append(fmatter.Replace(Environment.NewLine, "<br />"));
            //build content
            foreach (DataRow drOrder in dtOrder.Rows)
            
            {
                strBuff.Append("<div class='row'><div class='col-lg-12'>");
                strBuff.Append("<h5 class='text-primary'><strong>ORDER "+drOrder["order_number"].ToString()+": " + drOrder["description"].ToString().ToUpper() + "</strong></h5>");
                objConnection.Sql = @"select section_number,sections.order_id,sub_no,sub_description,section_description,content 
from sections left join suborders_table on suborder_id = sub_id where sections.order_id = " + drOrder["order_id"].ToString()+ 
@" order by cast(section_number as money)";
                DataTable dtSection = objConnection.GetTable;
                
                foreach(DataRow drSection in dtSection.Rows)
                {
                    string strHold = "<div class='row' style='margin-top:20px; margin-bottom:20px; display:block;'><div class='col-lg-1' style='width:20px;'><strong>"+drSection["section_number"].ToString()+@"</strong></div><div class='col-lg-9' style='text-align:justify;white-space:pre-line;
    border-right: 1px solid #EEE;'>" + drSection["content"].ToString() + "</div><div class='col-lg-2'><i>"+drSection["section_description"].ToString()+"</i></div></div>";
                    strBuff.Append(strHold);
    
                }
                strBuff.Append("</div></div>");
            }
            return strBuff;
           
        }

    
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}