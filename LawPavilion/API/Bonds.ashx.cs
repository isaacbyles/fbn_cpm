﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;


namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for Bonds
    /// </summary>
    /// 
    public class Airtel
    {
        public string ProcessId;
        public string RequestTitle;
        public string RequestId;
        public string Ref;
        public string NameOfParty;
        public string CompanyAddress;
        public string DateOnOfferLetter;
        public string TotalAmountFigure;
        public string TotalAmountWords;
        public string TotalAmountGuaranteedFigure;
        public string TotalAmountGuaranteedWords;
        public string SignDate;
    }

    public class Advanced { }
    public class NDDC { }
    public class IATA { }
    public class BidBond { }
    public class CustomBond { }
    public class DraftCustom { }
    public class EMTS { }
    public class MTN { }

    public class Bonds : IHttpHandler
    {

        private LPCMConnection objConnection;
        private DataSet ds;
        private DataRow dRow;
        int MaxRows;
        int inc = 0;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";

            objConnection = new LPCMConnection();


            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);


            context.Response.Write(json);
        }
        private dynamic Handler(string pathinfo)
        {
            string type = "";
            string request = "";
            string request_trail = "";

            //DataSet dsResult;

            String[] parameters = pathinfo.Split('/');
            type = parameters[1];
            switch (parameters.Length)
            {
                case 2:
                    return this.getRequests(type);
                case 3:
                    return this.getFields(type);
                case 4:
                    request = parameters[2];
                    request_trail = parameters[3];
                    return this.getData(type, request, request_trail);
                default:
                    return null;
            }


        }


        public dynamic getRequests(string type)
        {
            string process = getStageForm(type)[2];
            string stage = getStageForm(type)[0];
            //get min request_trail id of requests
            string[] strArray;
            objConnection.Sql = "select min(id) from request_trail where stage_id = "+stage+" group by request_id";

            //string[] strArrray = objConnection.GetTable.Rows.OfType<DataRow>().Select(k => k[0].ToString()).ToArray();
                //var stringArr = objConnection.GetTable.Rows.ItemArray.Select(x => x.ToString()).ToArray();
          

            if (stage == "") { return null; }
                string strQry = @"select id as request_trail_id, request_id, stage_id, 
    (select title from request where process_id = " + process + @" and request.id  = request_id) as title,
    (select isnull(ref_no,'*** MISSING REF # ***') from request where process_id = " + process + @" and request.id  = request_id) as ref_no,
    (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = request_trail.user_id)  as [created_by],
    created_date,
    (select [status] from request where request.id = request_trail.request_id) as request_status   
    from request_trail where stage_id = " + stage + " and id in (select min(id) from request_trail where stage_id = " + stage + " group by request_id) order by [created_date] desc";
            objConnection.Sql = strQry;
            return objConnection.GetConnection;

        }

        public dynamic getData(string type, string request, string request_trail)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            //get name and date for ref

            DataTable dtRef = getRequests(type).Tables[0];
            //old bond templates ref
            //////string initials = dtRef.Rows[0]["created_by"].ToString().Replace(' ', '.').ToUpper();
            //////string year = (dtRef.Rows[0]["created_date"].ToString().Split('/')[2]).Split(' ')[0];
            //////string month = dtRef.Rows[0]["created_date"].ToString().Split('/')[0];
            //////data.Add("Ref", "FBNLS/" + request + request_trail + "/"+initials+"/"+month+"/"+year);
            data.Add("Ref", dtRef.Rows[0]["ref_no"].ToString());


            //get form id for request_trail
            string form_id = getStageForm(type)[1];
            objConnection.Sql = "select id,label from form_control where form_id = " + form_id;
            DataTable dtBuffer = objConnection.GetTable;
            DataTable dtBuffer2;
            foreach (DataRow rowReq in dtBuffer.Rows)
            {
                string strHold = "";
                objConnection.Sql = "select cast([value] AS NVARCHAR(MAX)) as value from process_form_char_data where form_control_id = " + rowReq["id"].ToString() + " and request_trail_id = " + request_trail;
                dtBuffer2 = objConnection.GetTable;
                if (dtBuffer2.Rows.Count > 0)
                {
                    strHold = dtBuffer2.Rows[0]["value"].ToString();
                }
                else
                {
                    objConnection.Sql = "select cast([value] AS NVARCHAR(MAX)) as value from process_form_date_data where form_control_id = " + rowReq["id"].ToString() + " and request_trail_id = " + request_trail;
                    dtBuffer2 = objConnection.GetTable;
                    if (dtBuffer2.Rows.Count > 0)
                    {
                        strHold = dtBuffer2.Rows[0]["value"].ToString();
                    }
                    else
                    {
                        objConnection.Sql = "select cast([value] AS NVARCHAR(MAX)) as value from process_form_decimal_data where form_control_id = " + rowReq["id"].ToString() + " and request_trail_id = " + request_trail;
                        dtBuffer2 = objConnection.GetTable;
                        if (dtBuffer2.Rows.Count > 0)
                        {
                            strHold = dtBuffer2.Rows[0]["value"].ToString();
                        }
                        else
                        {
                            objConnection.Sql = "select cast([value] AS NVARCHAR(MAX)) as value from process_form_int_data where form_control_id = " + rowReq["id"].ToString() + " and request_trail_id = " + request_trail;
                            dtBuffer2 = objConnection.GetTable;
                            if (dtBuffer2.Rows.Count > 0)
                            {
                                strHold = dtBuffer2.Rows[0]["value"].ToString();
                            }
                            else
                            {
                                objConnection.Sql = "select cast([value] AS NVARCHAR(MAX)) as value from process_form_text_data where form_control_id = " + rowReq["id"].ToString() + " and request_trail_id = " + request_trail;
                                dtBuffer2 = objConnection.GetTable;
                                if (dtBuffer2.Rows.Count > 0)
                                {
                                    strHold = dtBuffer2.Rows[0]["value"].ToString();
                                }
                            }
                        }
                    }

                }
                //add item to dictionary
                if (!data.ContainsKey(rowReq["label"].ToString()))
                {
                data.Add(rowReq["label"].ToString(), strHold);
                }
            }
            return data;
        }


        public dynamic getFields(string type)
        {
            string form_id = getStageForm(type)[1];
            objConnection.Sql = "select label from form_control where form_id = " + form_id;
            return objConnection.GetTable;
        }


        public string[] getStageForm(string type)
        {
            //return arrays: {"stage_id","form_id","process_id"}
            switch (type)
            {
                case "adv":
                    return new string[] { "210", "67", "24" };

                case "airtel":
                    return new string[] { "211", "68", "24" };

                case "nddc":
                    return new string[] { "212", "69", "24" };

                case "iata":
                    return new string[] { "213", "70", "24" };

                case "bidbond":
                    return new string[] { "214", "71", "24" };

                case "custombond":
                    return new string[] { "215", "72", "24" };

                case "draftcustom":
                    return new string[] { "215", "72", "24" };

                case "emts":
                    return new string[] { "216", "73", "24" };

                case "mtn":
                    return new string[] { "217", "74", "24" };

                case "tlm_corp_ind":
                    return new string[] { "145", "39", "16" };

                case "tlm_corp_corp":
                    return new string[] { "144", "38", "16" };

                case "mort_comp":
                    return new string[] { "143", "37", "16" };

                case "mort_ind":
                    return new string[] { "142", "36", "16" };

                case "asset_deb":
                    return new string[] { "141", "35", "16" };

                case "insp_agreement":
                    return new string[] { "181", "46", "21" };

                case "hypo_business":
                    return new string[] { "182", "47", "21" };

                case "hypo_llc":
                    return new string[] { "184", "48", "21" };

                case "pledge":
                    return new string[] { "185", "49", "21" };

                case "loan_ind":
                    return new string[] { "186", "52", "21" };

                case "loan_biz":
                    return new string[] { "187", "53", "21" };

                case "loan_llc":
                    return new string[] { "188", "54", "21" };

                case "wearhouse":
                    //return new string[] { "189", "45", "21" };
                    return new string[] { "189", "55", "21" };

                case "receipt_biz":
                    return new string[] { "190", "56", "21" };

                case "receipt_llc":
                    return new string[] { "192", "57", "21" };


                case "nonbankformat":
                    return new string[] { "164", "77", "19" };

                case "la":
                    return new string[] { "178", "2", "21" }; //{"stage_id","form_id","process_id"}
                default:
                    return new string[] { "", "", "" };

            }
        }

        static string ConvertStringArrayToStringJoin(string[] array)
        {
            //
            // Use string Join to concatenate the string elements.
            //
            string result = string.Join(",", array);
            return result;
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}