﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CaseManager.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for FullReport
    /// </summary>
    public class FullReport : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");



            context.Response.ContentType = "application/json";
            string suitno = context.Request.QueryString["suitno"].Trim(); //getting suitno from Jquery Ajax
            string table = context.Request.QueryString["table"].Trim(); //getting table from Jquery Ajax
            string pk = context.Request.QueryString["pk"].TrimStart();
            DataTable data = new DataTable();
            data = GetFullDetails(suitno,table, pk);
            string json = JsonConvert.SerializeObject(data, Formatting.Indented);
            context.Response.Write(json);
            

        }

        public DataTable GetFullDetails(string mNumber, string table_name,string pk)
        {
            DataSet ds = new DataSet();
            string query = "";
            SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
            conn.Open();
            if (table_name == "analysis" || table_name == "analysis_ca" || table_name == "analysis_fhc" ||
                table_name == "analysis_states" || table_name == "analysis_nic" || table_name == "analysis_tat") 
            {
                query = "SELECT * FROM " + table_name + " WHERE pk= '" + pk + "'";
            }
            else if (table_name == "cases" || table_name == "cases_ca" || table_name == "cases_fhc" ||
                table_name == "cases_nic" || table_name == "cases_states" || table_name == "cases_tat")
            {
                query = "SELECT * FROM " + table_name + " WHERE suitno= '" + pk + "'";
            }
            else if (table_name == "laws_sections" || table_name=="subsidiaries_legislations")
            {
                query = "SELECT * FROM " + table_name + " WHERE pk ='" + pk + "'";
            }
            else if (table_name == "CPR")
            {
                query =
                    "SELECT STR(a.year), c.section_number, a.title, a.jurisdiction, a.rule_id, c.section_description, c.content FROM rules a join orders_table b on a.rule_id = b.rule_id join sections c on b.order_id = c.order_id WHERE c.order_id= '" +
                    pk + "' AND section_number= '" + mNumber + "'";
            }

            DataTable dt = new DataTable(table_name);
            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataAdapter rd = new SqlDataAdapter(cmd);
            rd.Fill(ds);

            return ds.Tables[0];
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}