﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using CaseManager.Model;
using CaseManager.Search;
using Newtonsoft.Json;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for SearchPrev
    /// </summary>
    public class SearchPrev : IHttpHandler
    {
        List<SearchResult> _result = new List<SearchResult>();

        public void ProcessRequest(HttpContext context)
        {
            string word = SearchIndex._wordSearch;
            CaseManager.Search.database sb = new CaseManager.Search.database();
            int str = database.startpage;
            str -= 100;
            database.startpage = str;

            context.Response.ContentType = "application/json";

            _result = sb.search(word);

            string json = JsonConvert.SerializeObject(_result, Formatting.Indented);

            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}