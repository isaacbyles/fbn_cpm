﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;


namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for stk
    /// </summary>
    public class stk : IHttpHandler
    {
        private Regulation myRegulation;
        private Agency myAgency;
        private StkConnection objConnection;
        private DataSet ds;
        private DataRow dRow;
        int MaxRows;
        int inc = 0;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            //context.Response.Write("Hello World");
            //Agency myAgency = new Agency { Acronym = "CBN", Fullname = Handler(context.Request.PathInfo).ToString() };
            objConnection = new StkConnection();


            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);


            context.Response.Write(json);
        }


        private DataSet Handler(string pathinfo)
        {
            string agency ="";
            string category ="";
            string pk = "";
            //DataSet dsResult;

            String[] parameters = pathinfo.Split('/');
            switch (parameters.Length)
            {
                case 1:
                    return this.getAgencies(agency);
                case 2:
                    agency = parameters[1];
                    return this.getCategories(agency);
                case 3:
                    agency = parameters[1];
                    category = parameters[2];
                    return this.getRegulations(agency,category);
                case 4:
                    agency = parameters[1];
                    category = parameters[2];
                    pk = parameters[3];
                    return this.getContent(agency, category,pk);
                default:
                    
                    break;
                    
            }

            return null;
        }


        private DataSet getAgencies(string agency)
        {

            objConnection.Sql = "SELECT [acronym],[fullname] from agencies order by [fullname] asc";
            return ds = objConnection.GetConnection;

        }


        private DataSet getCategories(string agency)
        {
            //objConnection.Sql = "SELECT distinct [type] from [regulations] where [agency] ='"+agency+"' order by [type]";
            objConnection.Sql = "SELECT [pk],[title],[year],[type] from [regulations] where [agency] ='" + agency + "' and [deleted] = 0 order by [title]";
            return ds = objConnection.GetConnection;
        }


        private DataSet getRegulations(string agency, string category)
        {
            objConnection.Sql = "SELECT [pk],[title],[year],[short_title] from [regulations] where agency ='"+agency+"' and [type] = '"+category+"' order by [title]";
            return ds = objConnection.GetConnection;
        }


        private DataSet getContent(string agency, string category, string pk)
        {
            objConnection.Sql = "SELECT * from [regulations] where pk = "+pk+" order by [title]";
            return ds = objConnection.GetConnection;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}