﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CaseManager.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for Supreme
    /// </summary>
    public class Supreme : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            CaseManager.Search.database sb = new CaseManager.Search.database();
            context.Response.ContentType = "application/json";

            var _wordSearch = context.Request.QueryString["search"].ToString();
            var _result = sb.searchSupreme(_wordSearch);
            string json = JsonConvert.SerializeObject(_result, Formatting.Indented);
            string decode = JsonConvert.DeserializeObject(json).ToString();
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}