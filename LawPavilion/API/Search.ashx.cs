﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;

namespace CaseManager.LawPavilion.API
{

    public class SearchResults
    {
        public string Name { get; set; }
        public Object[] SearchResultItems { get; set; }
    }

    public class SearchItem
    {
        public string Process_Id { get; set; }
        public string Request_Id { get; set; }
        public string Request_Title { get; set; }
        public string Request_Trail_Id { get; set; }
        public string Stage_Id { get; set; }
        public string Form_Id { get; set; }
        public string Form_Control_Id { get; set; }
        public string Control_Label { get; set; }
        public string Control_Value { get; set; }

    }


    /// <summary>
    /// Summary description for Search
    /// </summary>
    public class Search : IHttpHandler
    {

        private Regulation myRegulation;
        private Agency myAgency;
        private LPCMConnection objConnection;
        private DataSet ds;
        private DataRow dRow;
        private DataTable dtRequest;
        private DataTable dtForm;
        private DataTable dtStage;

        int MaxRows;
        int inc = 0;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            objConnection = new LPCMConnection();
            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);
            context.Response.Write(json);
        }

        private object Handler(string pathinfo)
        {

            string user = "";
            string search_parameter = "";
            String[] parameters = pathinfo.Split('/');
            switch (parameters.Length)
            {

                case 3:
                    user = parameters[1];
                    search_parameter = parameters[2];
                    return this.getResults(user,search_parameter);
                default:
                    break;
            }
            return null;
        }


        private object getResults(string user, string search_parameter)
        {
            DataTable dtBuffer;
            objConnection.Sql = "SELECT [id]" +
                       ",[stage_id]" +
                       ",(select process.id from process join request on process.id = request.process_id where request.id = request_id) as process_id " +
                       ",(select name from stage where id = stage_id) as stage_name " +
                       ",[form_id]" +
                         ",[form_control_id]" +
                         ",[request_id]" +
                         ",[request_trail_id]" +
                         ",(select label from form_control  where id = form_control_id) as label" +
                         ",cast([value] AS NVARCHAR(MAX)) as value" +
                         ",[created_date]" +
                         ",[modified_date]" +
                         ",[status] " +
                           "FROM [process_form_text_data] where cast([value] AS NVARCHAR(MAX)) like '%"+search_parameter+"%'" +
                           " union " +
                           "SELECT [id]" +
                         ",[stage_id]" +
                         ",(select process.id from process join request on process.id = request.process_id where request.id = request_id) as process_id" +
                         ",(select name from stage where id = stage_id) as stage_name " +
                         ",[form_id]" +
                         ",[form_control_id]" +
                         ",[request_id]" +
                         ",[request_trail_id]" +
                         ",(select label from form_control  where id = form_control_id) as label" +
                         ",cast([value] AS NVARCHAR(MAX)) as value" +
                         ",[created_date]" +
                         ",[modified_date]" +
                         ",[status] " +
                     "FROM [process_form_date_data] where cast([value] AS NVARCHAR(MAX)) like '%"+search_parameter+"%'" +
                      "union " +
                                       "SELECT [id]" +
                         ",[stage_id]" +
                         ",(select process.id from process join request on process.id = request.process_id where request.id = request_id) as process_id " +
                         ",(select name from stage where id = stage_id) as stage_name " +
                         ",[form_id]" +
                         ",[form_control_id]" +
                         ",[request_id]" +
                         ",[request_trail_id]" +
                         ",(select label from form_control  where id = form_control_id) as label" +
                         ",cast([value] AS NVARCHAR(MAX)) as value" +
                         ",[created_date]" +
                         ",[modified_date]" +
                         ",[status] " +
                     "FROM [process_form_decimal_data] where cast([value] AS NVARCHAR(MAX)) like '%"+search_parameter+"%'" +
                      " union " +
                                       "SELECT [id]" +
                         ",[stage_id]" +
                         ",(select process.id from process join request on process.id = request.process_id where request.id = request_id) as process_id " +
                         ",(select name from stage where id = stage_id) as stage_name " +
                         ",[form_id]" +
                         ",[form_control_id]" +
                         ",[request_id]" +
                         ",[request_trail_id]" +
                        " ,(select label from form_control  where id = form_control_id) as label" +
                         ",cast([value] AS NVARCHAR(MAX)) as value" +
                         ",[created_date]" +
                         ",[modified_date]" +
                         ",[status] " +
                     "FROM [process_form_int_data] where cast([value] AS NVARCHAR(MAX)) like '%"+search_parameter+"%'" +
                      "union " +
                   "SELECT [id]" +
                         ",[stage_id]" +
                         ",(select process.id from process join request on process.id = request.process_id where request.id = request_id) as process_id " +
                         ",(select name from stage where id = stage_id) as stage_name " +
                         ",[form_id]" +
                         ",[form_control_id]" +
                         ",[request_id]" +
                         ",[request_trail_id]" +
                         ",(select label from form_control  where id = form_control_id) as label" +
                         ",[value]" +
                         ",[created_date]" +
                         ",[modified_date]" +
                         ",[status] " +
                     "FROM [process_form_char_data] where cast([value] AS NVARCHAR(MAX)) like '%"+search_parameter+"%'";
            dtBuffer = objConnection.GetTable;
            List<SearchItem> bufferItem = new List<SearchItem>();
            foreach (DataRow rowResult in dtBuffer.Rows)
            {
                //create new searctitem object
                SearchItem myItem = new SearchItem();
                myItem.Request_Title = "Request Title";
                myItem.Process_Id = rowResult["process_id"].ToString();
                myItem.Request_Id = rowResult["request_id"].ToString();
                myItem.Request_Trail_Id = rowResult["request_trail_id"].ToString();
                myItem.Stage_Id = rowResult["stage_id"].ToString();
                myItem.Form_Id = rowResult["form_id"].ToString();
                myItem.Form_Control_Id = rowResult["form_control_id"].ToString();
                myItem.Control_Label = rowResult["label"].ToString();
                myItem.Control_Value = rowResult["value"].ToString();
                bufferItem.Add(myItem);

            }
            SearchResults myResults = new SearchResults();
            myResults.Name = "SearchResult";
            myResults.SearchResultItems = bufferItem.Cast<object>().ToArray();
            

            return myResults;
        }



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}