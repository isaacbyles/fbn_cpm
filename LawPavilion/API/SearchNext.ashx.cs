﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using CaseManager.Search;
using Newtonsoft.Json;
using CaseManager.Model;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for SearchNext
    /// </summary>
    public class SearchNext : IHttpHandler, IRequiresSessionState
    {
        List<SearchResult> _result = new List<SearchResult>();
        public void ProcessRequest(HttpContext context)
        {
            //context.Request.F
            string word = SearchIndex._wordSearch;
            CaseManager.Search.database sb = new CaseManager.Search.database();
            int str = database.startpage;
            str += 100;
            database.startpage = str;

            context.Response.ContentType = "application/json";

            _result = sb.search(word);

            string json = JsonConvert.SerializeObject(_result, Formatting.Indented);

            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}