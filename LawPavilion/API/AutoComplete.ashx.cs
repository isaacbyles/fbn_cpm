﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using CaseManager.Search;
using Newtonsoft.Json;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for AutoComplete
    /// </summary>
    public class AutoComplete : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            #region 

            context.Response.ContentType = "application/json";
            var definition = new { txt = "" };
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();

            var jsonData = JsonConvert.DeserializeAnonymousType(strJson, definition);

            var refNo = jsonData.txt.Trim();

            List<string> get = GetAutoComplete(refNo);
            string json = JsonConvert.SerializeObject(get, Formatting.Indented);
            context.Response.Write(json);

            #endregion


            #region 

            //context.Response.ContentType = "application/json";
            //var definition = new { txt = "" };
            //string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();

            //var jsonData = JsonConvert.DeserializeAnonymousType(strJson, definition);

            //var refNo = jsonData.txt.Trim();


            //List<string> suggestions = Global.SpellEngine["en"].Suggest(refNo);

            //List<string> data = suggestions.ToList();

            //string json = JsonConvert.SerializeObject(data, Formatting.Indented);
            //context.Response.Write(json);

            #endregion

        }
        private String indexLocation = AppDomain.CurrentDomain.BaseDirectory + "resources\\Lucene";
        public List<string> GetAutoComplete(string txt)
        {
            #region 

//List<string> returnData = new List<string>();
            ////string query = "select case_title2 as txt from cases where case_title2 LIKE '%'+@SearchText+'%'";
            ////string query2 = "select principle as txt from analysis where principle LIKE '%'+@SearchText+'%'";
            //using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKNEW"]))
            //using (SqlCommand cmd = new SqlCommand("select case_title2 as txt from cases where case_title2 LIKE '%'+@SearchText+'%';select principle as txt from analysis where principle LIKE '%'+@SearchText+'%'", conn))
            //{
            //    conn.Open();
            //    cmd.Parameters.AddWithValue("@SearchText", txt);
            //    SqlDataReader dr = cmd.ExecuteReader();
            //    while (dr.Read())
            //    {
            //        returnData.Add(dr["txt"].ToString());
            //    }

            //    return returnData;
            //}

            #endregion


            SearchAutoComplete com = new SearchAutoComplete(indexLocation);
            List<string> data = com.SuggestTermsFor(txt).ToList();

            return data;

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}