﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for OtherLawReports
    /// </summary>
    public class OtherLawReports : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "text/plain";

            string action = context.Request.QueryString["a"];
            DataSet response = null;
            switch (action)
            {
                case "getSupreme":
                    response = this.LoadSupreme(context);
                    break;
                case "getFhc":
                    response = this.LoadFhc(context);
                    break;
                case  "getAppeal":
                    response = this.LoadAppeal(context);
                    break;

                case "getnic":
                    response = this.LoadNic(context);
                    break;

                case "gettat":
                    response = this.LoadTAT(context);
                    break;

                case "getstate":
                    response = this.LoadState(context);
                    break;

                case "getLFN":
                    response = this.getLFN(context);
                    break;

                case "getCPR":
                    response = this.getCPR(context);
                    break;
            }
            string json = JsonConvert.SerializeObject(response, Formatting.Indented);
            context.Response.Write(json);
        }

        private string top = ConfigurationManager.AppSettings["Top"];

        public DataSet LoadSupreme(HttpContext _context)
        {
            DataSet ds = new DataSet();
            string query =
                @"select  distinct TOP  " + top + " [date],cases.suitno,case_title2,[desc] from cases left join library_books on cases.suitno = library_books.suitno order by date desc";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTK"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "aaData");
            }
            return ds;
        }

        public DataSet LoadAppeal(HttpContext context)
        {
            DataSet ds = new DataSet();

            string query =
                @"select distinct Top " + top + "  [date],cases_ca.suitno,case_title2,[desc] from cases_ca left join library_books on cases_ca.suitno = library_books.suitno order by date desc";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTK"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "Appeal");
            }
            return ds;
        }
        public DataSet LoadFhc(HttpContext context)
        {
            DataSet ds = new DataSet();
        
            string query =
                @"select Top " + top + " [date],cases_fhc.suitno,case_title2,[desc] from cases_fhc left join library_books on cases_fhc.suitno = library_books.suitno order by date desc";
           
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTK"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "FHC");
            }
            return ds;
        }

      
        public DataSet LoadNic(HttpContext  context)
        {
             DataSet ds = new DataSet();
            string query =
                @"select Top " + top + " [date],cases_nic.suitno,case_title2,[desc] from cases_nic left join library_books on cases_nic.suitno = library_books.suitno order by date desc";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTK"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "NIC");
            }
            return ds;
        }

        public DataSet LoadTAT(HttpContext context)
        {
            DataSet ds = new DataSet();
            string query =
                @"select Top " + top + " [date],cases_tat.suitno,case_title2,[desc] from cases_tat left join library_books on cases_tat.suitno = library_books.suitno order by date desc";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTK"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "TAT");
            }
            return ds;
        }

        public DataSet LoadState(HttpContext context)
        {
            DataSet ds = new DataSet();
            string query =
                @"select  Top " + top + " [date],cases_states.suitno,case_title2,[desc] from cases_states left join library_books on cases_states.suitno = library_books.suitno order by date desc";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTK"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "STATES");
            }
            return ds;  
        }

        public DataSet getLFN(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            string query =
                @"SELECT Top " + top + " [name],last_amended from area_of_law order by [name]";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTK"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "LFN");
            }
            return ds;  
        }


        public DataSet getCPR(HttpContext ctx)
        {
            //SELECT rule_id,[title],[year] from rules order by [title]
            DataSet ds = new DataSet();
            string query =
                @"SELECT Top " + top + " rule_id,[title],[year] from rules order by [title]";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTK"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "CPR");
            }
            return ds;  
        }
        private string[] getTable(string court)
        {
            switch (court)
            {
                case "supreme":
                    return new string[] { "cases", "analysis" };
                case "appeal":
                    return new string[] { "cases_ca", "analysis_ca" };
                case "fhc":
                    return new string[] { "cases_fhc", "analysis_fhc" };
                case "state":
                    return new string[] { "cases_states", "analysis_states" };
                case "tat":
                    return new string[] { "cases_tat", "analysis_tat" };
                case "nic":
                    return new string[] { "cases_nic", "analysis_nic" };
                default:
                    return new string[] { "", "" };
            }

        }



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}