﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;


namespace CaseManager.LawPavilion.API
{
    /// <summary>
    /// Summary description for LawReports
    /// </summary>
    public class LawReports : IHttpHandler
    {


        private LPELRConnection objLPELRConnection;
        private StkConnection objStkConnection;
        private DataSet ds;
        private dynamic objConnection;


        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            objLPELRConnection = new LPELRConnection();
            objStkConnection = new StkConnection();

            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo, context.Request.Url.AbsoluteUri), Formatting.Indented);
            context.Response.Write(json);
        }


        private DataSet Handler(string pathinfo,string url)
        {
            
            string court = "";
            string state = "";
            string type = "";
            string query = "";
            string pageno ="";
            

            string[] stkArray = { "state", "fhc", "tat","nic" };
            string[] lpelrArray = { "supreme", "appeal" };
            String[] parameters = pathinfo.Split('/');
            court = parameters[1];

            if (stkArray.Contains(court))
            {
                objConnection = objStkConnection;
            }
            else
            {
                objConnection = objLPELRConnection;
            }


            if (parameters.Length != 4){return null;}
            //parse pathinfo array
            court = parameters[1];
            type = parameters[2];
            query = parameters[3];
            switch (type){
                case "index":
                    return this.indexList(court, query);
                    //return this.getNoPages(court,query);
                case "principle":
                    query = url.Split('?')[1];
                    return this.getPrinciple(court, query);
                case "fullreport":
                     query = url.Split('?')[1];
                    return this.getJudgment(court, query);
                case "list":
                    state = url.Split('?')[1];
                    if (court == "state")
                    {
                        pageno = state.Split('-')[1];
                        state = state.Split('-')[0];

                    }
                    else
                    {
                        pageno = state;
                        state = "";
                    }
                    return this.getList(court, state,query,pageno);
                default:
                    return null;
            }
        }





        private DataSet getList(string court, string state,string alphabet,string pageno)
        {
            try
            {
                string query = "";
                //if
                int offset = (Int32.Parse(pageno) - 1) * 30;
                court = getTable(court)[0];
                if (state == "")
                {
                    objConnection.Sql = "select [date]," + court + ".suitno,case_title2,[desc] from " + court + " left join library_books" +
        " on " + court + ".suitno = library_books.suitno where substring(LTRIM(case_title2),1,1) = '" + alphabet + "' order by LTRIM(case_title2) offset " + offset.ToString() + " rows fetch next 30 rows only";
                }
                else
                {
                    objConnection.Sql = "select [date]," + court + ".suitno,case_title2,[desc] from " + court + " left join library_books" +
                            " on " + court + ".suitno = library_books.suitno where substring(LTRIM(case_title2),1,1) = '" + alphabet + "' and [state]='" + state + "' order by LTRIM(case_title2) offset " + offset.ToString() + " rows fetch next 30 rows only";
               
                }
                return ds = objConnection.GetConnection;
            }catch {
                return null;
            }

        }



        private DataSet getNoPages(string court, string alphabet)
        {
            court = getTable(court)[0];
            objConnection.Sql = "SELECT count(*) from "+court+" where LTRIM(case_title) LIKE '" + alphabet+"%'";
            return ds = objConnection.GetConnection;
        }


        private DataSet indexList(string court, string query)
        {
            string strQuery="";
            court = getTable(court)[0];
            if (court == "cases_states")
            {
                strQuery = "select distinct substring(ltrim(case_title2),1,1) as [index], count(*) as [count] from " + court + " where case_title2 is not null and case_title2 <>''  and state = '" + query + "' group by substring(ltrim(case_title2),1,1) order by [index]";
            }
            else
            {
                strQuery = "select distinct substring(ltrim(case_title2),1,1) as [index], count(*) as [count] from " + court + " where case_title2 is not null and case_title2 <>'' group by substring(ltrim(case_title2),1,1) order by [index]";
            }
            objConnection.Sql = strQuery;
            return ds = objConnection.GetConnection;
            //return null;
        }


        private DataSet getJudgment(string court,string suitno)
        {
            court = getTable(court)[0];
            objConnection.Sql = "SELECT [desc] as [citation], format([date],'dddd, d MMMM yyyy') as [release_date]," + court + ".* from " + court + " left join library_books on " + court + ".suitno = library_books.suitno where " + court + ".suitno ='" + suitno + "' and " + court + ".deleted = 0";
            return ds = objConnection.GetConnection;
        }


        private DataSet getPrinciple(string court, string suitno)
        {
            
            court = getTable(court)[1];
            objConnection.Sql = "SELECT pk,convert(varchar, [date], 106) as [date],[suitno],[legal head],[subjectmatter],[issues1],[principle] from " + court + " where suitno ='" + suitno + "' and deleted = 0";
            return ds = objConnection.GetConnection;
        }


        private string[] getTable(string court){
            switch (court)
            {
                case "supreme":
                    return new string[]{"cases","analysis"};
                case "appeal":
                    return new string[]{"cases_ca","analysis_ca"};
                case "fhc":
                    return new string[]{"cases_fhc","analysis_fhc"};
                case "state":
                    return new string[]{"cases_states","analysis_states"};
                case "tat":
                    return new string[]{"cases_tat","analysis_tat"};
                case "nic":
                    return new string[]{"cases_nic","analysis_nic"};
                default:
                    return new string[]{"",""};
            }

        }





        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}