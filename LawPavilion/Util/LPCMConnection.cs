﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace CaseManager.LawPavilion.Util
{
    public class LPCMConnection
    {
        private string sql_string;
        private string table_name;

        private string strCon = ConfigurationManager.AppSettings.Get("conn");
        System.Data.SqlClient.SqlDataAdapter da_1;

        private System.Data.DataSet MyDataSet()
        {
            System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(strCon);
            con.Open();
            da_1 = new System.Data.SqlClient.SqlDataAdapter(sql_string, con);
            System.Data.DataSet dat_set = new System.Data.DataSet();
            da_1.Fill(dat_set, "Table_Data_1");
            con.Close();
            return dat_set;
        }

        public string Sql
        {
            set { sql_string = value; }
        }


        public string Table_name
        {
            set { table_name = value; }
        }


        public string connection_string
        {
            set { sql_string = value; }
        }

        public System.Data.DataSet GetConnection
        {
            get { return MyDataSet(); }
        }


        private System.Data.DataTable MyDataTable()
        {
            try
            {
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(strCon);
                con.Open();
                da_1 = new System.Data.SqlClient.SqlDataAdapter(sql_string, con);
                System.Data.DataTable dat_tab = new System.Data.DataTable(table_name);
                da_1.Fill(dat_tab);
                con.Close();
                return dat_tab;
            }catch(Exception e){
                return null;
            }
        }

        public System.Data.DataTable GetTable
        {
            get { return MyDataTable(); }
        }
    }
}