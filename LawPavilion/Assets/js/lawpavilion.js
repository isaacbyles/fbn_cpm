﻿var url_reg = "../LawPavilion/API/Regulations.ashx";
var url_forms = "../LawPavilion/API/Forms.ashx";
var url_reports = "../LawPavilion/API/LawReports.ashx";
var url_cpr = "../LawPavilion/API/CPR.ashx";
var url_lfn = "../LawPavilion/API/LFN.ashx";
//var court="";
var navigation_button = "<div class='btn-group pull-right' style='display:inline; margin-top:-5px;'><button class='btn btn-default btn-danger dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false' id='index-button'> Select Index<span class='caret'></span></button>" +
"<ul class='dropdown-menu' id='index-menu'></ul>" +
"</div>";
var navigation_row = "<div id='report_navigation_row' class='row pull-right' style='margin:5px;'>" +
"<div class='btn-group'><button class='btn btn-default btn-info dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false' id='index-button'> Alphabelical Index<span class='caret'></span></button>"+
"<ul class='dropdown-menu' id='index-menu'></ul>"+
"</div>"+
"</div>";

var heading_row = "<div id='report_heading_row' class='row' style='margin-top:5px; margin-bottom:5px;margin-left:-25px; margin-right:-25px;'></div>";
var content_row = "<div id='report_content_row' class='row'></div>";
var pagination_row ="<h2>Laloye</h2>";




function ucwords(str, force) {
    str = force ? str.toLowerCase() : str;
    return str.replace(/(\b)([a-zA-Z])/g,
           function (firstLetter) {
               return firstLetter.toUpperCase();
           });
       }



       $(document).ready(function () {
           $("#lpelr-1").load("../Lawpavilion/Components/lpelr1.htm"); $("#lpelr-2").load("../Lawpavilion/Components/lpelr2.htm"); $("lpelr").hide();
           //$("#stk-1").load("../Lawpavilion/Components/stk1.htm"); $("#stk-2").load("../Lawpavilion/Components/stk2.htm"); $("stk").hide();
           $("#stk-1").hide(); $("#stk-2").load("../Lawpavilion/Components/stk3.htm", function () {
               $('#stk-window').css('height', $(window).height() - 52);

           }); $("stk").hide();




           $("#cms-link").hide();
           $(".ext-link").on('click', function () {
               $('.ext-link').hide();
               $('.ext-link').not($(this)).show();
           });

           $("#stk-link").on('click', function () {
               $("#user_page2").html("<div class='panel-group' id='accordion_regulation' role='tablist' aria-multiselectable='false'></div>")
               loadForms();
               $('stk').show();
               $("cms").hide();
               $("lpelr").hide();
           });

           $("#cms-link").on('click', function () {
               $("stk").hide();
               $("cms").show();
               $("lpelr").hide();
           });


           $("#lpelr-link").on('click', function () {
               //loadCases($("#supreme_link"));
               loadUpdates();
               $('lpelr').show();
               $("cms").hide();
               $("stk").hide();
           });
           });

       function loadAgencies() {
           $('stk[id=stk-2]').show();
           $('lpelr[id=lpelr-2]').hide();
           $("#user_page2").html("<div class='panel-group' id='accordion_regulation' role='tablist' aria-multiselectable='false'></div>")
           $("#stk-heading").html("<i class='fa fa-briefcase fa-fw'></i><strong style='font-family:verdana;'> LAWPAVILION STK > REGULATIONS</strong>");
           $.ajax({
               type: "POST",
               url: url_reg,
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: function (data) {
                   var strBuffer = "";
                   var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));
                   $.each(parsed, function (i, jsondata) {
                       strBuffer = "<div class='panel panel-primary'>" +
    "<div class='panel-heading' title='" + jsondata.fullname + "' data-toggle='tooltip' role='tab' id=''>" +
        "<h4 class='panel-title'>" +
            "<a onclick='loadCategories(this)' class='collapsed accord' tag='agency' role='button' data-toggle='collapse' data-parent='#accordion' href='#" + jsondata.acronym + "' aria-expanded='false' aria-controls='collapseTwo'>" +
                "<i class='fa fa-minus-square-o'></i> " +
                jsondata.fullname +
            "</a>" +
          "</h4>" +
   "</div>" +
   "<div aria-expanded='false' id='" + jsondata.acronym + "' class='panel-collapse collapse agency'  role='tabpanel' aria-labelledby='headingTwo'>" +
        "<div class='panel-body' tag='" + jsondata.acronym + "'></div>" +
   "</div>" +
 "</div>";
                       $("#accordion_regulation").append(strBuffer);
                   });
               },
               error: function (XHR, errStatus, errorThrown) {
                   var err = JSON.parse(XHR.responseText);
                   errorMessage = err.Message;
                   alert(errorMessage);
               }
           });
       }

       function loadCategories(e) {
           var tag = $(e).attr("href").replace("#", "");
           $.ajax({
               type: "POST",
               url: url_reg + "/" + tag,
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: function (data) {
                   var strBuffer = "";
                   var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));
                   $("div[tag=" + tag + "]").html('');
                   $("div[tag=" + tag + "]").append("<div class='list-group'>");
                   $.each(parsed, function (i, jsondata) {
                       strBuffer = "<a style='font-family: verdana;' onclick='loadContent(this)' href='#' class='list-group-item' id='" + tag + "/" + jsondata.type + "/" + jsondata.pk + "'><span class='badge'>" + jsondata.type + "</span> " + jsondata.title + "</a>";
                       //strBuffer = "<a onclick='loadContent(this)' href='#' id='" + tag + "/" + jsondata.type + "/" + jsondata.pk + "'><p style='font-family: verdana; text-decoration:underline;' class='text-danger'><i class='fa fa-caret-right'></i> " + jsondata.title + " <small class='text-info'>(" + jsondata.type + ")</small></p></a>";
                       $("div[tag=" + tag + "]").append(strBuffer);
                   });
                   $("div[tag=" + tag + "]").append("</div>");
               },
               error: function (XHR, errStatus, errorThrown) {
                   var err = JSON.parse(XHR.responseText);
                   errorMessage = err.Message;
                   alert(errorMessage);
               }
           });
           //alert();
       }



       function loadContent(e) {
           var tag = $(e).attr("id");
           var others = tag.split("/");
           $.ajax({
               type: "POST",
               url: url_reg + "/" + tag,
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: function (data) {
                   var strBuffer = "<section id='regulation-content' style='padding:20px'></section>";

                   var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));
                   $("#user_page2").html(strBuffer);
                   $.each(parsed, function (i, jsondata) {
                       strBuffer = "<h2 style='text-align:center;'>" + jsondata.title + "</h2><br/>" +
                            "<p style='text-align:center'>" + jsondata.short_title + "</p>" +
                            "<p style='font-family: verdana; text-align:justify;line-height:2'>" + jsondata.long_title + "</p>" +
                            "<p style='font-family: verdana; text-align:justify;line-height:2'>" + jsondata.front_matter + "</p>" +
                            "<p style='font-family: verdana; text-align:justify;line-height:2'>" + jsondata.content + "</p>" +
                            "<p style='font-family: verdana; text-align:justify;line-height:2'>" + jsondata.end_matter + "</p>" +
                            "<p style='font-family: verdana; text-align:justify;line-height:2'>" + jsondata.schedule + "</p>";

                       $("#regulation-content").append(strBuffer);
                       $("#stk-heading").html("<i class='fa fa-briefcase fa-fw'></i> " + jsondata.title + "<button class='btn btn-xs btn-warning pull-right'  type='button' onclick='loadAgencies()'><i class='fa fa-chevron-left'></i> BACK TO AGENCIES</button>");
                   });
               },
               error: function (XHR, errStatus, errorThrown) {
                   var err = JSON.parse(XHR.responseText);
                   errorMessage = err.Message;
                   alert(errorMessage);
               }
           });
           //alert();
       }



       function loadRegulations(e) {
           $("#stk-heading").html("<i class='fa fa-briefcase fa-fw'></i> LAWPAVILION STK | REGULATIONS");
           var tag = $(e).attr("id");
           var others = tag.split("/");
           $.ajax({
               type: "POST",
               url: url_reg + "/" + tag,
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: function (data) {
                   var strBuffer = "<table class='table table-striped table-bordered table-hover no-footer' id='' role='grid'>" +
                                "<thead>" +
                                    "<tr role='row'>" +
                                        "<th>#</th>" +
                                        "<th>Agency</th>" +
                                        "<th>Type</th>" +
                                        "<th>Title</th>" +
                                        "<th>Year</th>" +
                                        "<th>Action</th>" +
                                     "</tr>" +
                                  "</thead>" +
                                  "<tbody id='regulation-list'></tbody>" +
                             "</table>";

                   var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));
                   $("#user_page2").html(strBuffer);
                   $.each(parsed, function (i, jsondata) {
                       strBuffer = "<tr>" +
                                "<td>" + (i + 1) + "</td>" +
                                "<td>" + others[0] + "</td>" +
                                "<td>" + others[1] + "</td>" +

                                "<td>" + jsondata.title + "</td>" +
                                "<td>" + jsondata.year + "</td>" +
                                "<td><button onclick='loadContent(this)' type='button' id='" + tag + "/" + jsondata.pk + "'class='btn btn-sm request_detail_link'>View</button></td>" +
                             "</tr>";

                       $("#regulation-list").append(strBuffer);
                   });
               },
               error: function (XHR, errStatus, errorThrown) {
                   var err = JSON.parse(XHR.responseText);
                   errorMessage = err.Message;
                   alert(errorMessage);
               }
           });
           //alert();
       }


       function loadForms() {
           $('stk[id=stk-2]').show();
           $('lpelr[id=lpelr-2]').hide();
           $("#stk-bread").hide();
           $("#stk-panel").css("margin-top", "80px");

           $("#stk-heading").html("<i class='fa fa-briefcase fa-fw'></i> <strong style='font-family:verdana'>LAWPAVILION STK > FORMS AND AGREEMENT</strong>");
           $("#user_page2").html("<h5><strong>Loading...</strong></h5>");
           $.ajax({
               type: "POST",
               url: url_forms,
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: function (data) {
                   var strBuffer = "<table class='table table-striped table-bordered table-hover no-footer' role='grid'>" +
                                "<thead>" +
                                    "<tr role='row'>" +
                                        "<th>#</th>" +
                                        "<th>Title</th>" +
                                        "<th>Type</th>" +
                                        "<th>Action</th>" +
                                     "</tr>" +
                                  "</thead>" +
                                  "<tbody id='regulation-list'></tbody>" +
                             "</table>";

                   var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));
                   $("#user_page2").html(strBuffer);
                   $.each(parsed, function (i, jsondata) {
                       strBuffer = "<tr>" +
                                "<td>" + (i + 1) + "</td>" +
                                "<td class='text-primary' style='font-weight:bold; font-family:verdana; '><i class='fa fa-tag'></i> " + ucwords(jsondata.title, true) + "</td>" +
                                "<td class='text-success'><span class='badge'>" + ucwords(jsondata.type, true) + "</span></td>" +
                                "<td><button onclick='loadFormContent(this)' type='button' id='" + jsondata.pk + "' class='btn btn-xs btn-warning btn-block request_detail_link'><i class='fa fa-eye'></i>View</button></td>" +
                             "</tr>";

                       $("#regulation-list").append(strBuffer);
                   });
               },
               error: function (XHR, errStatus, errorThrown) {
                   var err = JSON.parse(XHR.responseText);
                   errorMessage = err.Message;
                   alert(errorMessage);
               }
           });
           //alert();

       }


       function loadFormContent(e) {
           var tag = $(e).attr("id");

           $.ajax({
               type: "POST",
               url: url_forms + "/" + tag,
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: function (data) {
                   var strBuffer = "<div class='col-sm-12' style='padding:70px; border-color: #DDD;" +
"border-style: solid;" +
"border-width: 1px;text-align:justify;line-height:2; font-family:verdana;'><section id='form-content'></section></div>";

                   var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));
                   $("#user_page2").html(strBuffer);
                   $.each(parsed, function (i, jsondata) {
                       strBuffer = "<!--<h2 style='text-align:center; font-family:verdana;'>" + jsondata.title + "</h2><br/>-->" +
                            "<div class='alert alert-success'><strong>TYPE: </strong>" + jsondata.type + "</div>" +
                            "<div class='alert alert-warning'><strong>NOTE: </strong>" + jsondata.notes + "</div>" +
                            "<p>" + jsondata.content + "</p>";


                       $("#form-content").append(strBuffer);
                       $("#stk-heading").html("<i class='fa fa-briefcase fa-fw'></i> <strong style='font-family:verdana'>LAWPAVILION STK > REGULATIONS > " + jsondata.title + "</strong><button class='btn btn-xs btn-warning pull-right'  type='button' onclick='loadForms()'><i class='fa fa-chevron-left'></i><strong style='font-family:verdana;'> BACK TO FORMS LIST</strong></button>");
                   });




               },
               error: function (XHR, errStatus, errorThrown) {
                   var err = JSON.parse(XHR.responseText);
                   errorMessage = err.Message;
                   alert(errorMessage);
               }
           });
           //alert();
       }

       
       function loadCases(e) {
            $("#previous-page").remove();
            $("#span-report").data("court",$(e).data("court"));
            $('stk[id=stk-2]').hide();
            $('lpelr[id=lpelr-2]').show();
            $("cms").hide();


            $("#report_navigation_row").remove();
            $("#report_heading_row").remove();
            $("#report_heading_row_search").remove(); //remove from search result
            $("#report_content_row").remove();

            $("#lpelr-wrapper").append(heading_row);
            //$("#lpelr-wrapper").append(navigation_row);
            $("#lpelr-wrapper").append(content_row);
            $("#report_content_row").load("../Lawpavilion/Components/reportlist.htm", function () { loadIndex(e); });
    //alert("Done");
    //appendPagination();
    

}

       function loadUpdates(e) {
           $("#previous-page").remove();
          
           $('stk[id=stk-2]').hide();
           $('lpelr[id=lpelr-2]').show();
           $("cms").hide();


           //$("#report_navigation_row").remove();
           //$("#report_heading_row").remove();
           //$("#report_heading_row_search").remove(); //remove from search result
           //$("#report_content_row").remove();

           //$("#lpelr-wrapper").append(heading_row);
           ////$("#lpelr-wrapper").append(navigation_row);
           //$("#lpelr-wrapper").append(content_row);
           //$("#report_content_row").load("../Lawpavilion/Components/updates.htm");
           //alert("Done");
           //appendPagination();

           $("#report_navigation_row").remove();
           $("#lfn_report").remove();
           $("#report_heading_row").remove();
           $("#report_content_row").remove();
           $("#lpelr-wrapper").append(heading_row);
           //$("#report_heading_row").append("<div class='alert alert-warning' style='margin-bottom:0px;'><strong>Latest Updates Court </strong></div>");
           $("#lpelr-wrapper").append(content_row);
           $("#report_content_row").load("../Lawpavilion/Components/updates.htm");
           $('#prev').hide();
           $('#next').hide();
           $("#report-back").remove();

       }



function loadList(e) {
    //$("#report-back").data("url", $(e).data("url"));
    //$("#report-back").data("count", $(e).data("count"));
    //$("#report-back").attr("onclick","backToList(this)");
    //$("#report-back").addClass("disabled");
    $("#report_heading_row_search").remove();
    $("#report-back").hide();

    var strBuffer = "<table class='table table-striped table-bordered table-hover no-footer' id='table-lawreports' role='grid'>" +
    "<thead>" +
    "<tr role='row'>" +
    "<th class='text-center' style='width: 25px;'>#</th>" +
    "<th>Release Date</th>" +
    "<th class='col-sm-4'>Case Title</th>" +
    "<th class='col-sm-3'>Suit No</th>" +
    "<th>Citation</th>" +
    "<th class='col-sm-1 text-center'>Action</th>" +
    "</tr>" +
    "</thead>" +
    "<tbody id='tbody-lawreports'></tbody></table>";
    $("#panel-lawreports").html("");
    $("#table-lawreports").remove();
    $("#panel-lawreports").append(strBuffer);
    var court = $(e).data("url").split("/")[0];
    var pageno = parseInt($(e).data("url").split("?")[1]);
    var base = $(e).data("url").split("?")[0];
    var index = base.split("/")[2];
    //get lists
    $.ajax({
        type: "POST",
        url: url_reports + "/" + $(e).data("url"),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {


            var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));

            $.each(parsed, function (i, jsondata) {
                strBuffer = "<tr role='row'  style='font-size:12px; font-weight:bold;'>" +
                                "<td>" + (((pageno - 1) * 30) + (i + 1)).toString() + "</td>" +
                                "<td><span class='text-primary'><i class='fa fa-calendar'></i></span> " + jsondata.date.split("T")[0] + "</td>" +
                                "<td>" + jsondata.case_title2 + "</td>" +
                                "<td>" + jsondata.suitno + "</td>" +
                                "<td>" + emptyNull(jsondata.desc) + "</td>" +
                                "<td class='center'><button type='button' class='btn-warning btn-small' onclick='loadReport(this)' data-url='" + court + "/fullreport/suitno?" + jsondata.suitno + "'> <i class='fa fa-eye'></i><strong> View </strong></button></td>" +
                            "</tr>";

                $("#tbody-lawreports").append(strBuffer);



            });
            if (parseInt($(e).data("count")) <= 30 && pageno == 1) { $("#report-pagination").children().remove(); }
            //load pagination if pageno = 1
            var noOfpages = Math.ceil(parseInt($(e).data("count")) / 30); ;
            if (pageno == 1 && parseInt($(e).data("count")) > 30) {
                $("#report-pagination").children().remove();
                 
                for (i = 1; i <= noOfpages; i++) {
                    $("#report-pagination").append("<li><a data-count='" + $(e).data("count") + "' data-url='" + base + "?" + i.toString() + "' onclick='loadList(this)' href='#'>" + i + "</a></li>");
                }
            }

            //var buffer = $("#report_heading_row div strong").text();
            buffer = " | Showing: Page "+pageno+" of " + noOfpages.toString() + ", INDEX #"+index;
            $("desc").text(buffer);
        },
        error: function (XHR, errStatus, errorThrown) {
            var err = JSON.parse(XHR.responseText);
            errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}




function loadIndex($e) {
    $("#report_heading_row_search").remove();
    $("#report_heading_row").append("<div class='alert alert-warning' style='margin-bottom:0px;'><strong>" + $e.data("title") + "</strong><total></total><desc class='text-primary'></desc></div>");
    $("#report_heading_row .alert").append(navigation_button);
    //retrieve alphabetical index from api
    var options = $e.data("court").split("-");
    var strBuffer = "";
    var strQueryString = "";
    if (options.length == 2) {
        strBuffer = options[0].toString() + '/index/' + options[1].toString();
        strQueryString = options[1].toString() +"-";
    } else {
    strBuffer = options[0].toString()+'/index/';
    }

    $.ajax({
        type: "POST",
        url: url_reports + "/" + strBuffer,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.Table_Data_1.length == 0) {
                $("#panel-lawreports").html("<span class='text-danger'> No " + $e.data("title") + " found</span>");
                return;
            }
            var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));
            var total = 0;
            //$("#user_page2").html(strBuffer);

            $.each(parsed, function (i, jsondata) {
                $("#index-menu").append("<li><a onclick='loadList(this)' href='#' data-count='" + jsondata.count + "' data-url='" + options[0].toString() + "/list/" + jsondata.index + "?" + strQueryString + "1'>" + jsondata.index + "</a></li>");
                total = total + parseInt(jsondata.count);
            });
            //trigger click
            //var buffer = $("#report_heading_row div strong").text();
            buffer = " | Total: " + numeral(total.toString()).format('0,0');
            $("total").text(buffer);
            $("#index-menu li a").first().trigger("click");
        },
        error: function (XHR, errStatus, errorThrown) {
            var err = JSON.parse(XHR.responseText);
            errorMessage = err.Message;
            alert(errorMessage);
        }
    });

}




function getPagination() {

}

function loadReport(e) {
    //$("#report-back").removeClass("disabled");
    //kept previous page html and hide
    $("#report-back").show();
    $("#report_heading_row_search").remove();
    var temp = $("#lpelr-wrapper").html();
    


    var myUrl = $(e).data("url");
    $("#report_navigation_row").remove();
    $("#report_heading_row").remove();
    $("#report_content_row").remove();

    $("#lpelr-wrapper").append(heading_row);

    $("#lpelr-wrapper").append(content_row);
    $("#report_content_row").load("../Lawpavilion/Components/reportcontent.htm");
    $("#lpelr-wrapper").append("<div class='row' id='previous-page' style='display:none;'>" + temp + "</div>");
    $.ajax({
        type: "POST",
        url: url_reports + "/" + $(e).data("url"),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));
            $('[data-name="fullreport"]').html("");
            $.each(parsed, function (i, jsondata) {
                $("#report_heading_row").append("<div class='alert alert-warning' style='margin-bottom:5px;'><strong>" + jsondata.case_title2 + "</strong> |<span class='text-success'> <strong>" + jsondata.suitno + "</strong></span> | <span class='text-danger'><strong>" + getCourt($("#span-report").data("court")) + "</strong></span></div>");
                
                $('[data-name="fullreport"]').append("<h5 style='color:red; text-align:center; font-weight:bold;'>" + jsondata.case_title2.toUpperCase() + "</h5>");

                $('[data-name="fullreport"]').append("<p style='color:green; text-align:center; font-weight:bold;'>CITATION: " + emptyNull(jsondata.citation).toUpperCase() + "</p>");
                $('[data-name="fullreport"]').append("<p style='text-align:center; font-weight:bold;'><img src='../Lawpavilion/Assets/images/coa.jpg'/></p>");
                $('[data-name="fullreport"]').append("<p style='text-align:center; color:blue; font-weight:normal;'>" + coverDesc($("#span-report").data("court"), jsondata) + "</p>");
                $('[data-name="fullreport"]').append("<p style='text-align:center; color:blue; font-weight:normal;'>ON " + jsondata.release_date.toUpperCase() + "</p>");
                $('[data-name="fullreport"]').append("<p style='text-align:center; color:red; font-weight:bold;'>Suit No: " + jsondata.suitno.toUpperCase() + "</p>");
                $('[data-name="fullreport"]').append("<p style='text-align:center; color:blue; font-weight:normal;'>Before their Lordships:</p>");
                $('[data-name="fullreport"]').append("<div style='display:block; text-align:center; color:blue; font-weight:normal;'></div>");
                if (typeof (jsondata.judge1) != "undefined" && jsondata.judge1 !== null && jsondata.judge1 !== "") {
                    $('[data-name="fullreport"]').append("<div class='row' style='display:block; text-align:center; color:blue; font-weight:normal; padding-left:60px; padding-right:60px;'><span class='col-md-6' style='text-align:left;'>" + jsondata.judge1 + "</span><span class='col-md-6 '>" + justiceTitle($("#span-report").data("court")) + "</span></div>");
                }

                if (typeof (jsondata.judge2) != "undefined" && jsondata.judge2 !== null && jsondata.judge2 !== "") {
                    $('[data-name="fullreport"]').append("<div class='row' style='display:block;text-align:center; color:blue; font-weight:normal; padding-left:60px; padding-right:60px;'><span class='col-md-6' style='text-align:left;'>" + jsondata.judge2 + "</span><span class='col-md-6 '>" + justiceTitle($("#span-report").data("court")) + "</span></div>");
                }

                if (typeof (jsondata.judge3) != "undefined" && jsondata.judge3 !== null && jsondata.judge3 !== "") {
                    $('[data-name="fullreport"]').append("<div class='row' style='display:block;text-align:center; color:blue; font-weight:normal; padding-left:60px; padding-right:60px;'><span class='col-md-6' style='text-align:left;'>" + jsondata.judge3 + "</span><span class='col-md-6 '>" + justiceTitle($("#span-report").data("court")) + "</span></div>");
                }

                if (typeof (jsondata.judge4) != "undefined" && jsondata.judge4 !== null && jsondata.judge4 !== "") {
                    $('[data-name="fullreport"]').append("<div class='row' style='display:block;text-align:center; color:blue; font-weight:normal; padding-left:60px; padding-right:60px;'><span class='col-md-6' style='text-align:left;'>" + jsondata.judge4 + "</span><span class='col-md-6 '>" + justiceTitle($("#span-report").data("court")) + "</span></div>");
                }


                if (typeof (jsondata.judge5) != "undefined" && jsondata.judge5 !== null && jsondata.judge5 !== "") {
                    $('[data-name="fullreport"]').append("<div class='row' style='display:block;text-align:center; color:blue; font-weight:normal; padding-left:60px; padding-right:60px;'><span class='col-md-6' style='text-align:left;'>" + jsondata.judge5 + "</span><span class='col-md-6 '>" + justiceTitle($("#span-report").data("court")) + "</span></div>");
                }


                if (typeof (jsondata.judge6) != "undefined" && jsondata.judge6 !== null && jsondata.judge6 !== "") {
                    $('[data-name="fullreport"]').append("<div class='row' style='display:block;text-align:center; color:blue; font-weight:normal; padding-left:60px; padding-right:60px;'><span class='col-md-6' style='text-align:left;'>" + jsondata.judge6 + "</span><span class='col-md-6 '>" + justiceTitle($("#span-report").data("court")) + "</span></div>");
                }

                if (typeof (jsondata.judge7) != "undefined" && jsondata.judge7 !== null && jsondata.judge7 !== "") {
                    $('[data-name="fullreport"]').append("<div class='row' style='display:block;text-align:center; color:blue; font-weight:normal; padding-left:60px; padding-right:60px;'><span class='col-md-6' style='text-align:left;'>" + jsondata.judge7 + "</span><span class='col-md-6 '>" + justiceTitle($("#span-report").data("court")) + "</span></div>");
                }

                if (typeof (jsondata.judge8) != "undefined" && jsondata.judge8 !== null && jsondata.judge8 !== "") {
                    $('[data-name="fullreport"]').append("<div class='row' style='display:block;text-align:center; color:blue; font-weight:normal; padding-left:60px; padding-right:60px;'><span class='col-md-6' style='text-align:left;'>" + jsondata.judge8 + "</span><span class='col-md-6 '>" + justiceTitle($("#span-report").data("court")) + "</span></div>");
                }

                $('[data-name="fullreport"]').append("<div style='display:block; text-align:center; color:blue; font-weight:bold;'>Between</div>");

                if (typeof (jsondata.parties) != "undefined") {
                    var parties = jsondata.parties.split(" v ");
                } else {
                    var parties = "Appellant(s) v Respondent(s)".split(" v ");
                }
                $('[data-name="fullreport"]').append("<div class='row' style='display:block; text-align:left; color:blue; font-weight:normal; padding-left:60px; padding-right:60px;'><span class='col-md-6' style='text-align:left;'>" + nl2br(jsondata.appelant) + "</span><span class='col-md-6' style='font-weight:bold;text-align:right'>" + parties[0] + "</span></div>");
                $('[data-name="fullreport"]').append("<div class='row' style='display:block; text-align:center; color:blue; font-weight:bold;'>And</div>");
                $('[data-name="fullreport"]').append("<div class='row' style='display:block; text-align:center; color:blue; font-weight:normal; padding-left:60px; padding-right:60px;'><span class='col-md-6' style='text-align:left;'>" + nl2br(jsondata.defendant) + "</span><span class='col-md-6' style='font-weight:bold;text-align:right'>" + parties[1] + "</span></div>");
                $('[data-name="fullreport"]').append("<div class='row'><hr/></div>");
                $('[data-name="fullreport"]').append("<div class='row' style='display:block; padding-left:60px;text-align:left; color:red; font-weight:bold;'>RATIO DECIDENDI</div>");
                $('[data-name="fullreport"]').append("<div class='row' id='div-principle' style='padding-left:60px; padding-right:60px; text-align:justify; display:block;'></div>");
                getRatios(myUrl);
                $('[data-name="fullreport"]').append("<div class='row'><hr/></div>");
                $('[data-name="fullreport"]').append("<div class='row' style='padding-right:60px;padding-left:60px;text-align:justify; font-weight:normal; line-height:2;color:#0E3BBC;'>" + jsondata.fullreport_html + "</div>");
            });

        },
        error: function (XHR, errStatus, errorThrown) {
            var err = JSON.parse(XHR.responseText);
            errorMessage = err.Message;
            alert(errorMessage);
        }

    });

}
function emptyNull(str) {
    if (str == null) {
        str = "";
    }
    return str;
}


function coverDesc(court, data) {
var result;
    switch (court) {
        case "supreme":
            result = "In the Supreme Court of Nigeria";
            break;
        case "appeal":
            result = "In the Court of Appeal<br/>In the " + data.division + "Judicial Division<br/>Holden at " + data.division;
            break;
        case "fhc":
            result = "In The Federal High Court of Nigeria<br/>Holden at "+data.division;
            break;
        case "nic":
            result = "In The National Industrial Court of Nigeria<br/>Holden at " + data.division;
            break;
        case "tat":
            result = "In the Tax Appeal Tribunal";
            break;
        case "state-lagos":
        case "state-abuja":
            result = "In The High Court of " + data.state + "<br/>In the " + data.state + " Judicial Division<br/>Holden at " + data.state;
            break;
        default:
            result = "";
            break;
    }
    return ucwords(result,true);
}

function justiceTitle(court) {
    switch (court) {
        case "supreme":
            return "Justice of the Supreme Court";
            
        case "appeal":
            return "Justice, Court of Appeal";
            
        case "fhc":
            return "Justice, Federal High Court";
            
        case "nic":
            return "Hon. Justice";
            
        case "tat":
            return "Commissioner";
            
        case "state-lagos":
        case "state-abuja":
            return "Justice, State High Court";

        default:
            return "";
    }

}


function getRatios(myUrl) {
    //load principles
    //===================================================================
    $.ajax({
        type: "POST",
        url: url_reports + "/" + myUrl.split('/')[0] + "/principle/suitno?" + myUrl.split('?')[1],
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var parsed2 = $.parseJSON(JSON.stringify(data.Table_Data_1));
            $('[data-name="principle"]').html("");
            $.each(parsed2, function (i, jsondata) {
                $('#div-principle').append("<span>" + (i + 1) + ". </span><span style='color:#990000;font-weight:bold'>" + jsondata["legal head"] + "</span> - " +
"<span style='color:#009933;font-weight:bold'>" + jsondata["subjectmatter"] + "</span>:" +
"<span style='color:#009933;font-weight:normal'>" + jsondata["issues1"] + "</span><br/>" +
"<span style='color:#000099;font-weight:normal'>" + jsondata["principle"] + "</span><br/><br/>"
);

                //list out principles
                $('[data-name="principle"]').append("<div class='bs-callout bs-callout-primary'><span style='color:#990000;font-weight:bold'>" + jsondata["legal head"] + " - </span><span style='color:green;font-weight:bold;'>" + jsondata["subjectmatter"] + ": <span><span style='color:blue;font-weight:normal;'>" + jsondata["issues1"] + " <span><a style='color:red;text-decoration:underline;' href='#" + jsondata.pk + "'>read in context</a></div>");

            });


        },
        error: function (XHR, errStatus, errorThrown) {
            var err = JSON.parse(XHR.responseText);
            errorMessage = err.Message;
            alert(errorMessage);
        }
    });


    //==================================================================       
}

function nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function getCourt(court) {
    switch (court) {
        case "supreme":
            return "Supreme Court";
        case "appeal":
            return "Court of Appeal";
        case "nic":
            return "National Industrial Court";
        case "tat":
            return "Tax Appeal Tribunal";
        case "fhc":
            return "Federal High Court";
        case "state-abuja":
            return "FCT High Court";
        case "state-lagos":
            return "Lagos State High Court";
        default:
            return "";
    }

}

function navigateBack(e) {
    var temp = $("#previous-page").html();
    //$("#report_content_row").load("../Lawpavilion/Components/reportlist.htm");
    //alert($(e).data("url"));
    //loadList(e);
    $("#lpelr-wrapper").html(temp);
    $("#report-back").hide();
}


function loadCPR() {
    $("#report_heading_row_search").remove();
    $("#previous-page").remove();
    $('stk[id=stk-2]').hide();
    $('lpelr[id=lpelr-2]').show();
    $("cms").hide();
    $("#report_navigation_row").remove();
    $("#report_heading_row").remove();
    $("#report_content_row").remove();
    $("#lpelr-wrapper").append(heading_row);
    $("#report_heading_row").append("<div class='alert alert-warning' style='margin-bottom:0px;'><strong>Civil Procedure Rules</strong></div>");
    $("#lpelr-wrapper").append(content_row);
    $("#report_content_row").load("../Lawpavilion/Components/reportlist.htm", function () {
        var strBuffer = "<table style='font-family:verdana;' class='table table-striped table-bordered table-hover no-footer' id='table-lawreports' role='grid'>" +
    "<thead>" +
    "<tr role='row'>" +
    "<th class='text-center' style='width: 25px;'>#</th>" +
    "<th>Title</th>" +
    "<th>Year</th>" +
    "<th></th>" +
    "</tr>" +
    "</thead>" +
    "<tbody id='tbody-lawreports'></tbody></table>";
        $("#panel-lawreports").html("");
        $("#table-lawreports").remove();
        $("#panel-lawreports").append(strBuffer);
        loadCPRList();

    });
    

}

function loadCPRList() {

    $("#report_heading_row_search").remove();
    //get lists
    $.ajax({
        type: "POST",
        url: url_cpr,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));
            var i = 0;
            $.each(parsed, function (i, jsondata) {
                strBuffer = "<tr role='row'  style='font-size:12px; font-weight:bold;'>" +
                                "<td>" + (i + 1).toString() + "</td>" +
                                "<td>" + jsondata.title + "</td>" +
                                "<td>" + emptyNull(jsondata.year) + "</td>" +
                                "<td class='center'><button type='button' class='btn-warning btn-small' onclick='loadRule(this)' data-ruleid='" + jsondata.rule_id + "' data-ruletitle='" + jsondata.title + "' data-year='"+jsondata.year+"'> <i class='fa fa-eye'></i><strong> View </strong></button></td>" +
                            "</tr>";

                $("#tbody-lawreports").append(strBuffer);

                i += 1;

            });

        },
        error: function (XHR, errStatus, errorThrown) {
            var err = JSON.parse(XHR.responseText);
            errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}

function loadRule(e) {

    var temp = $("#lpelr-wrapper").html();
    
    $("#report_navigation_row").remove();
    $("#report_heading_row").remove();
    $("#report_content_row").remove();
    $("#report_heading_row_search").remove();
    $("#lpelr-wrapper").append(heading_row);

    $("#lpelr-wrapper").append(content_row);
    $("#report_content_row").load("../Lawpavilion/Components/reportcontent.htm", function () {
        $("#principle_col").remove();
        $("#contentview").removeClass("col-lg-9").addClass("col-lg-12");
        $("#report_heading_row").append("<div class='alert alert-warning' style='margin-bottom:5px;'><strong>" + $(e).data("ruletitle") + "</strong>  |<span class='text-success'> <strong>YEAR: " + $(e).data("year") + "</strong></span></div>");
        $("#report_heading_row .alert").append(navigation_button);
        $("#index-button").html("<strong>Select Order <span class='caret'></span></strong>");
        $("#report-back").show();
        $(".panel-footer").remove();
        alert(url_cpr + "/" + $(e).data('ruleid'));
        $.ajax({
            type: "POST",
            url: url_cpr + "/" + $(e).data('ruleid'),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
             
                var parsed = $.parseJSON(JSON.stringify(data));
                $('[data-name="fullreport"]').css('line-height', '2');
                $('[data-name="fullreport"]').html(parsed.m_StringValue);
            },

            error: function (XHR, errStatus, errorThrown) {
                var err = JSON.parse(XHR.responseText);
                errorMessage = err.Message;
                alert(errorMessage);
            }
        });
    });
    

    $("#lpelr-wrapper").append("<div class='row' id='previous-page' style='display:none;'>" + temp + "</div>");
}


function loadLFN() {
    $("#report_heading_row_search").remove();
    $("#previous-page").remove();
    $('stk[id=stk-2]').hide();
    $('lpelr[id=lpelr-2]').show();
    $("cms").hide();
    $("#report_navigation_row").remove();
    $("#report_heading_row").remove();
    $("#report_content_row").remove();
    $("#lpelr-wrapper").append(heading_row);
    $("#report_heading_row").append("<div class='alert alert-warning' style='margin-bottom:0px;'><strong>Laws of the Federation</strong></div>");
    $("#lpelr-wrapper").append(content_row);
    $("#report_content_row").load("../Lawpavilion/Components/reportlist.htm", function () {
        var strBuffer = "<table style='font-family:verdana;' class='table table-striped table-bordered table-hover no-footer' id='table-lawreports' role='grid'>" +
    "<thead>" +
    "<tr role='row'>" +
    "<th class='text-center' style='width: 25px;'>#</th>" +
    "<th>Law</th>" +
    "<th>Last Amended</th>" +
    "<th></th>" +
    "</tr>" +
    "</thead>" +
    "<tbody id='tbody-lawreports'></tbody></table>";
        $("#panel-lawreports").html("");
        $("#table-lawreports").remove();
        $("#panel-lawreports").append(strBuffer);
        loadLFNList();

    });
}

function loadLFNList() {

    
    //get lists
    $.ajax({
        type: "POST",
        url: url_lfn,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var parsed = $.parseJSON(JSON.stringify(data.Table_Data_1));
            var i = 0;
            $.each(parsed, function (i, jsondata) {
                strBuffer = "<tr role='row'  style='font-size:12px; font-weight:bold;'>" +
                                "<td>" + (i + 1).toString() + "</td>" +
                                "<td class='col-lg-8'>" + jsondata.name.toUpperCase() + "</td>" +
                                "<td>" + emptyNull(jsondata.last_amended) + "</td>" +
                                "<td class='center'><button type='button' class='btn-warning btn-small' onclick='loadLaw(this)' data-law='" + jsondata.name + "' data-year='"+jsondata.last_amended+"'> <i class='fa fa-eye'></i><strong> View </strong></button></td>" +
                            "</tr>";

                $("#tbody-lawreports").append(strBuffer);

                i += 1;

            });

        },
        error: function (XHR, errStatus, errorThrown) {
            var err = JSON.parse(XHR.responseText);
            errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}



function loadLaw(e) {

    var temp = $("#lpelr-wrapper").html();
   
    $("#report_navigation_row").remove();
    $("#report_heading_row").remove();
    $("#report_content_row").remove();
    $("#report_heading_row_search").remove();
    $("#lpelr-wrapper").append(heading_row);

    $("#lpelr-wrapper").append(content_row);
    $("#report_content_row").load("../Lawpavilion/Components/reportcontent.htm", function () {
        $("#principle_col").remove();
        $("#contentview").removeClass("col-lg-9").addClass("col-lg-12");
        $("#report_heading_row").append("<div class='alert alert-warning' style='margin-bottom:5px;'><strong>" + $(e).data("law") + "</strong>  |<span class='text-success'> <strong>LAST AMENDED: " + $(e).data("year") + "</strong></span></div>");
        $("#report_heading_row .alert").append(navigation_button);
        $("#index-button").html("<strong>GO TO SECTION <span class='caret'></span></strong>");
        $("#report-back").show();
        $(".panel-footer").remove();

        alert(url_lfn + "/" + $(e).data('law'));
        $.ajax({
            type: "POST",
            url: url_lfn + "/" + $(e).data('law'),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var parsed = $.parseJSON(JSON.stringify(data));
                $('[data-name="fullreport"]').css('line-height', '2');
               // alert(parsed.m_StringValue);
                $('[data-name="fullreport"]').html(parsed.m_StringValue);
            },

            error: function (XHR, errStatus, errorThrown) {
                var err = JSON.parse(XHR.responseText);
                errorMessage = err.Message;
                alert(errorMessage);
            }
        });
    });


    $("#lpelr-wrapper").append("<div class='row' id='previous-page' style='display:none;'>" + temp + "</div>");
}