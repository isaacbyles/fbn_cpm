﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;


namespace CaseManager.LawPavilion.Models
{
    public class Regulation
    {
        public string Title { get; set; }
        public string Agency { get; set; }
        public string Year { get; set; }
        public string Type { get; set; }
        public string Table_of_content { get; set; }
        public string Front_matter { get; set; }
        public string End_matter { get; set; }
        public string Content { get; set; }
        public string Schedule { get; set; }
        public string Long_title { get; set; }
        public string Short_title { get; set; }
        public string Comment { get; set; }


    }


    public class Agency
    {
        public string Acronym { get; set; }
        public string Fullname { get; set; }
        

    }


    public class Form
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Note { get; set; }
        public string Content { get; set; }
    }
}