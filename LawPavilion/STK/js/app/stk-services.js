// Generated by CoffeeScript 1.7.1
(function() {
  angular.module('stkServices', []).service('stkAPIService', [
    '$http', 'alertService', function($http, alertService) {
      var stkAPI;
      stkAPI = {
        newSAVE:new Boolean(),
        currPK:new Number(0),
        currFORMname:"",
        currFormContent:"",
        //baseUrl: 'http://stk.local/api/index.php?_url=',
        //baseUrl: 'http://192.168.1.83/CM/LawPavilion/API/',
		baseUrl: '../API/',
        getFormTypes: function() {
            return $http({
                method: 'JSONP',
                url: this.baseUrl + 'formTypes/JSON_CALLBACK'
            });
        },
        getAllLetters: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getAllLetters/JSON_CALLBACK'].join('/')
            });
        },

        getAllAffidavits: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getAllAffidavits/JSON_CALLBACK'].join('/')
            });
        },
        getForms: function(type) {
          return $http({
            method: 'GET',
            url: this.baseUrl + 'Forms.ashx'
          });
        },
        getAllForms: function() {
          return $http({
            method: 'GET',
            url: this.baseUrl + 'Forms.ashx'
          });
        },
        getForm: function(id) {
          return $http({
            method: 'GET',
            url: this.baseUrl + 'Forms.ashx/'+id
          });
        },

        getFormCases: function(id) {
          return $http({
            method: 'GET',
            url: this.baseUrl + 'Forms.ashx/'+id+'/case'
          });
        },

        getFormClauses: function(id) {
          return $http({
            method: 'GET',
            url: this.baseUrl + 'Forms.ashx/'+id+'/clause'
          });
        },

        getFormLaws: function(id) {
          return $http({
            method: 'GET',
            url: this.baseUrl + 'Forms.ashx/'+id+'/law'
          });
        },

        getAllFormsandLettersForIndustry: function(industry) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getAllFormsandLettersForIndustry/'+industry+'/JSON_CALLBACK'].join('/')
            });
        },

        getAllFormsIndustries: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getAllFormsIndustries/JSON_CALLBACK'].join('/')
            });
        },



        getNICByTopicalIndex: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getNICByTopicalIndex/JSON_CALLBACK'].join('/')
            });
        },

        getNICByIndustryIndex: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getNICByIndustryIndex/JSON_CALLBACK'].join('/')
            });
        },

        getFHCByIndustryIndex: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getFHCByIndustryIndex/JSON_CALLBACK'].join('/')
            });
        },

        getFHCByPartIndustryIndex : function(industry) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getFHCByPartIndustryIndex/'+industry+'/JSON_CALLBACK'].join('/')
            });
        },

        getNICByPartIndustryIndex : function(industry) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getNICByPartIndustryIndex/'+industry+'/JSON_CALLBACK'].join('/')
            });
        },

        getFHCByTopicalIndex: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getFHCByTopicalIndex/JSON_CALLBACK'].join('/')
            });
        },

        deleteForm: function(formname,pk) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'deleteformPersonal',formname,pk,'JSON_CALLBACK'].join('/')
            });
        },
        getAllFormsandLetters: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getAllFormsandLetters/JSON_CALLBACK'].join('/')
            });
        },
        getAllFormsPersonal: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'formsAllPersonal/JSON_CALLBACK'].join('/')
            });
        },

        formPersonal:function(id) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'formPersonal', id , 'JSON_CALLBACK'].join('/')
            });
        },
        getAgencies: function() {
            return $http({
                method: 'JSONP',
                url: this.baseUrl + 'agencies/JSON_CALLBACK'
            });
        },
        getRegulationTypes: function(agency) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'regulationTypes', agency, 'JSON_CALLBACK'].join('/')
            });
        },
        getRegulationsByType: function(agency, type) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'regulations', agency, type, 'JSON_CALLBACK'].join('/')
            });
        },
        getRegulation: function(id) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'regulation', id, 'JSON_CALLBACK'].join('/')
            });
        },
        getAllFHCCases: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getAllFHCCases/JSON_CALLBACK'].join('/')
            });
        },
        getAllNICCases: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getAllNICCases/JSON_CALLBACK'].join('/')
            });
        },

        getNICCaseBySuitNo: function(suitno) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl +'getNICCaseBySuitNo/' +suitno+ '/JSON_CALLBACK'].join('/')
            });
        },

        getFHCCaseBySuitNo: function(suitno) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl +'getFHCCaseBySuitNo/' +suitno+ '/JSON_CALLBACK'].join('/')
            });
        },

        getFHCCasePagesBySuitNo: function(suitno,table) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl +'getFHCCasePagesBySuitNo/' +suitno+ '/' +table+ '/JSON_CALLBACK'].join('/')
            });
        },

        getNICCasePagesBySuitNo: function(suitno) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl +'getNICCasePagesBySuitNo/' +suitno+ '/JSON_CALLBACK'].join('/')
            });
        },

        getNICRatiosBySuitNo: function(suitno) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl +'getNICRatiosBySuitNo/' +suitno+ '/JSON_CALLBACK'].join('/')
            });
        },

        getFHCRatiosBySuitNo: function(suitno) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl +'getFHCRatiosBySuitNo/' +suitno+ '/JSON_CALLBACK'].join('/')
            });
        },

        getNICByNames: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getNICByNames/JSON_CALLBACK'].join('/')
            });
        },

        getFHCByNames: function() {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getFHCByNames/JSON_CALLBACK'].join('/')
            });
        },

        getAVAILABILITYofALPHABETICALINDEX : function(table) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'getAVAILABILITYofALPHABETICALINDEX/'+table+'/JSON_CALLBACK'].join('/')
            });
        },

        getAllCasesFHCByTopicalIndex : function(topicalindex) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl +'getAllCasesFHCByTopicalIndex/' +topicalindex+'/JSON_CALLBACK'].join('/')
            });
        },

        getAllCasesNICByTopicalIndex : function(topicalindex) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl +'getAllCasesNICByTopicalIndex/' +topicalindex+'/JSON_CALLBACK'].join('/')
            });
        },

        getCasesByIndex : function(alphabet,table) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl +'getCasesByIndex/' +alphabet+ '/' +table+ '/JSON_CALLBACK'].join('/')
            });
        },

        getFULLJudgement: function(suitno,table) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl +'getFULLJudgement/' +suitno+ '/' + table +'/JSON_CALLBACK'].join('/')
            });
        },

        doSearch: function(term) {
            return $http({
                method: 'JSONP',
                url: [this.baseUrl + 'search', term, 'JSON_CALLBACK'].join('/')
            });
        },
        updateRegulation: function(regulation) {
            return $http({
                url: [this.baseUrl + 'regulation', regulation.pk].join('/'),
                method: 'POST',
                data: regulation
            });
        },
        updateTemplateMODANG:function(pkofform,industry) {
            $("body").find("div").each(function(){
                if($(this).attr("standard")) {
                    var $cas = $(this);
                    $($cas).find("div").each(function(){
                        if($(this).attr("id") == undefined) {
                            ;
                        } else {
                            if(($(this).attr("id") == "toolbarWC") || ($(this).attr("id") == "toolbarCC")) {
                                ;
                            } else {
                                var $templatename = document.getElementById("templatename");
                                var tempname = $($templatename).val();
                                var $exact = document.getElementById($(this).attr("id"));
                                var payload = $($exact).html();
                                stkAPI.setVars(tempname,payload,industry);
                                stkAPI.updateTemplate(tempname,pkofform,payload,industry).success(function (response) {
                                    if(response.templatename != "") {
                                        alert("Template Updated.");
                                    } else {
                                        alert("Database Server Busy, try again later.");
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        saveAsTemplateMODANGWITHOLDNAME:function(formername,pkofform) {
            var $templatename = document.getElementById("templatename");
            if($($templatename).css('visibility') === "visible") {
                ;
            } else {
                var $savehiddendiv = document.getElementById("savehiddendiv");
                $($savehiddendiv).hide().css({'visibility':'visible'}).slideDown('slow').show();
            }
            $($templatename).val(formername);
            $("body").find("div").each(function(){
                if($(this).attr("standard")) {
                    var $cas = $(this);
                    $($cas).find("div").each(function() {
                        if($(this).attr("id") == undefined) {
                            ;
                        } else {
                            if(($(this).attr("id") == "toolbarWC") || ($(this).attr("id") == "toolbarCC")) {
                                ;
                            } else {
                                var $templatename = document.getElementById("templatename");
                                var tempname = $($templatename).val();
                                var $exact = document.getElementById($(this).attr("id"));
                                var payload = $($exact).html();
                                stkAPI.setVars(tempname,payload);
                                stkAPI.updateTemplate(tempname,pkofform,payload).success(function (response) {
                                    if(response.templatename != "") {
                                        alert("Template Saved");
                                    } else {
                                        alert("Database Server Busy, try again later.");
                                    }
                                });
                            }
                        }
                    });
                }
            });
        },

        updateTemplate:function(formname,pk,templatecontent,industry) {
            return $http({
                url: [this.baseUrl + 'updateTemplate'].join('/'),
                method: 'POST',
                data: {
                    templatename: formname,
                    pk: pk,
                    content: templatecontent,
                    industry: industry
                }
            });
        },

        generatePDF : function(formname,templatecontent) {
            return $http({
                url: [this.baseUrl + 'generatePDF'].join('/'),
                method: 'POST',
                data: {
                    templatename: formname,
                    content: templatecontent
                }
            });
        },

        generateWinWord : function(formname,templatecontent) {
            return $http({
                url: [this.baseUrl + 'generateWinWord'].join('/'),
                method: 'POST',
                data: {
                    templatename: formname,
                    content: templatecontent
                }
            });

        },


        saveAsTemplateMODANG:function() {
            var $templatename = document.getElementById("templatename");
            if($($templatename).css('visibility') === "visible") {
                ;
            } else {
                var $savehiddendiv = document.getElementById("savehiddendiv");
                $($savehiddendiv).hide().css({'visibility':'visible'}).slideDown('slow').show();
            }
            if($($templatename).val() === "" || $($templatename).val() == undefined) {
                alertService.show("Give your template a name");
                //$rootScope.alerts.push({msg: 'Give your template a name'});
            } else {
                $("body").find("div").each(function(){
                    if($(this).attr("standard")) {
                        var $cas = $(this);
                        $($cas).find("div").each(function() {
                            if($(this).attr("id") == undefined) {
                                ;
                            } else {
                                if(($(this).attr("id") == "toolbarWC") || ($(this).attr("id") == "toolbarCC")) {
                                    ;
                                } else {
                                    var $templatename = document.getElementById("templatename");
                                    var tempname = $($templatename).val();
                                    var $exact = document.getElementById($(this).attr("id"));
                                    var payload = $($exact).html();
                                    stkAPI.setVars(tempname,payload);
                                    stkAPI.saveTemplate(tempname,payload).success(function (response) {
                                        if(response.templatename != "") {
                                            alertService.show("Template Saved");
                                        } else {
                                            alertService.show("Database Server Busy, try again later.");
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }


        },

        saveTemplate:function(formname,templatecontent,industry) {
            return $http({
                url: [this.baseUrl + 'saveTemplate'].join('/'),
                method: 'POST',
                data: {
                    templatename: formname,
                    content: templatecontent,
                    category: "My Documents",
                    industry: industry
                }
            });
        },

        setSAVEDSTATUSforNEW:function() {
            stkAPI.newSAVE = true;
        },

        getSAVEDSTATUSforNEW:function() {
            return stkAPI.newSAVE;
        },

        setSAVEDSTATUSforOLD:function() {
            stkAPI.newSAVE = false;
        },

        getSAVEDSTATUSforOLD:function() {
            return stkAPI.newSAVE;
        },

        setVars:function (tempname,tempcontent,industry) {
            stkAPI.ourForm = tempname;
            stkAPI.ourContent = tempcontent;
            stkAPI.ourIndustry = industry;
        },

        getVars:function () {
            return stkAPI.ourForm;
        }
      };
      return stkAPI;
    }
  ]).service('storageService', function() {
    var myStorage;
    myStorage = {
      fetch: function(key) {
        return window.localStorage.getItem(key);
      },
      save: function(key, value) {
        return window.localStorage.setItem(key, value);
      },
      clear: function(key) {
        return window.localStorage.removeItem(key);
      }

    };
    return myStorage;
  }).service('alertService', [
    '$rootScope', '$timeout', function($rootScope, $timeout) {
      var alert, doTimeout;
      doTimeout = function() {
        return $rootScope.alertMessage = '';
      };
      return alert = {
        show: function(msg) {
          $rootScope.alertMessage += msg;
          return $timeout(doTimeout, 5000);
        }
      };
    }
  ]);

}).call(this);