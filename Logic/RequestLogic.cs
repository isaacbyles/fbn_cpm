﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Threading;
using CaseManager.Model;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Mail;
using CaseManager.Util;
using System.Transactions;
using System.Web.UI;
using CaseManager.LawPavilion.Util;
using Org.BouncyCastle.Math;

namespace CaseManager.Logic
{
    public class RequestLogic : Base
    {
        #region Methods

        #region
        //public long create(int next_stage_id, int next_user_id, string title, string description, string remark, string note, int lifespan, int? form_id, List<Dictionary<string, object>> form_data_list)
        //{
        //    User initiator = Auth.getInfo();
        //    if (initiator == null) { this.setAsAccessDenied(); return Base.NO_ID; }

        //    Dictionary<string, object> filter = new Dictionary<string, object>();
        //    List<string> fetch_with = new List<string>();

        //    //get next user
        //    filter["id"] = next_user_id;
        //    filter["status"] = Model.Status.ACTIVE;

        //    DataSet result = User.fetch(filter: filter);
        //    if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }
        //    else if (result.Tables.Count == 0) { this.setAsError(); return Base.NO_ID; }
        //    else if (result.Tables[0].Rows.Count == 0) { this.setAsError("Assigned user does not exist"); return Base.NO_ID; }

        //    User next_user = User.ToObject(result.Tables[0]);

        //    //get next stage
        //    filter.Clear();
        //    filter["id"] = next_stage_id;
        //    DataTable result_table = Stage.fetch(filter: filter);
        //    if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }
        //    else if (result_table.Rows.Count == 0) { this.setAsError("Next stage does not exist"); return Base.NO_ID; }

        //    Stage next_stage = Stage.ToObject(result_table);

        //    if (next_stage.RoleId != next_user.RoleId) { this.setAsError("Next stage cannot be assigned to user"); return Base.NO_ID; }

        //    //check if request can be initialised by user
        //    filter.Clear();
        //    filter["role_id"] = initiator.RoleId;
        //    filter["process_id"] = next_stage.ProcessId;
        //    filter["type"] = Model.Stage.TYPE_START;
        //    result_table = Stage.fetch(filter: filter);
        //    if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }
        //    else if (result_table.Rows.Count == 0) { this.setAsError("Request cannot be initialised by user for this process"); return Base.NO_ID; }

        //    Stage init_stage = Stage.ToObject(result_table);

        //    Request r = new Request(next_stage.ProcessId, next_stage.Id, initiator.Id, next_user.Id, title, lifespan, description, remark, note);

        //    /**** Instantiate Mail Sending Object ****/
        //    /**** Added 27th January 2016 ****/

        //    Mail ms = new Mail();

        //    //ms.mailDuringRequest(init_stage.Id.ToString(), initiator.Id.ToString(), next_stage.RoleId.ToString(), initiator.BranchId.ToString());
        //    ms.mailDuringRequest(next_stage.ProcessId.ToString(), initiator.Id.ToString(), next_stage.RoleId.ToString(), initiator.BranchId.ToString());

        //    /**** End Mail Sending Object ****/

        //    using (TransactionScope tran = new TransactionScope())
        //    {
        //        if (!r.save()) { this.setAsError(); return Base.NO_ID; }

        //        RequestTrail rt = new RequestTrail(r.Id, init_stage.Id, r.CurrentUserId, 0);

        //        if (!rt.save()) { this.setAsError(); return Base.NO_ID; }

        //        if (form_id != null)
        //        {
        //            if (!RequestForm.saveForm(r.Id, rt.Id, form_id.Value, initiator.Id, init_stage.Id, form_data_list)) { this.setAsError(); return Base.NO_ID; }
        //        }
        //        tran.Complete();
        //    }
        //    this.setAsSuccess();
        //    return r.Id;
        //}
        #endregion
        Request r = null;
        Random rnd = new Random();
        private static String ref_number;
        private HttpContext context;
        private static long dd = 1000000000;
        public static object UserID;
        public static object _id;
        private LPCMConnection objConnection = new LPCMConnection();
        public Object _roleID;
        public Object _userID;
        private HttpCookie cookie = new HttpCookie("ID");
        AuditLogic audit = new AuditLogic();

        public long create(int next_stage_id, int next_user_id, string title, string description, string remark,
            string note, int lifespan, int? form_id, List<Dictionary<string, object>> form_data_list)
        {

            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
                HttpServerTest.Logger.Logger.writelog(_userID.ToString() + "coo");
            }


            HttpServerTest.Logger.Logger.writelog(_userID.ToString());

            User initiator = Temp.GetUser((string)_userID);

            if (initiator == null)
            {
                this.setAsAccessDenied();
                return Base.NO_ID;
            }

            Dictionary<string, object> filter = new Dictionary<string, object>();
            List<string> fetch_with = new List<string>();

            //get next user
            filter["id"] = next_user_id;
            filter["status"] = Model.Status.ACTIVE;

            DataSet result = User.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return Base.NO_ID;
            }
            else if (result.Tables.Count == 0)
            {
                this.setAsError();
                return Base.NO_ID;
            }
            else if (result.Tables[0].Rows.Count == 0)
            {
                this.setAsError("Assigned user does not exist");
                return Base.NO_ID;
            }

            User next_user = User.ToObject(result.Tables[0]);

            //get next stage
            filter.Clear();
            filter["id"] = next_stage_id;
            DataTable result_table = Stage.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return Base.NO_ID;
            }
            else if (result_table.Rows.Count == 0)
            {
                this.setAsError("Next stage does not exist");
                return Base.NO_ID;
            }

            Stage next_stage = Stage.ToObject(result_table);

            #region

            List<string> getList = new List<string>();
            string sec_role = null;
            short _sec = 0;
            string sql = @"select role_id from user_role where user_id = '" + next_user_id + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    while (rd.Read())
                    {
                        sec_role = rd.GetInt32(0).ToString();

                        getList.Add(sec_role);
                    }

                }

            }


            #endregion

            //festus


            if (sec_role == null)
            {

            }
            else if (getList.Contains(next_stage.RoleId.ToString()))
            {
                string role = next_stage.RoleId.ToString();
                _sec = short.Parse(role);
            }


            //check if request can be initialised by user
            filter.Clear();
            filter["role_id"] = initiator.RoleId;
            filter["process_id"] = next_stage.ProcessId;
            filter["type"] = Model.Stage.TYPE_START;
            result_table = Stage.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return Base.NO_ID;
            }
            else if (result_table.Rows.Count == 0)
            {
                this.setAsError("Request cannot be initialised by user for this process");
                return Base.NO_ID;
            }

            Stage init_stage = Stage.ToObject(result_table);
            //festus edit
            if (next_stage.ProcessId == 1 || next_stage.ProcessId == 2)
            {
                // DataSet u = Auth.getLoginData();

                DataSet u = Temp.GetUserLogin(_userID.ToString());
                UserDetails ud = new UserDetails();
                string user_full_name = getInitial(u.Tables["user"].Rows[0]["id"].ToString());


                //string fn = u.Tables["user"].Rows[0]["firstname"].ToString();
                //string ln = u.Tables["user"].Rows[0]["lastname"].ToString();

                //string user_full_name = fn + " " + ln;

                if (user_full_name != null)
                {
                    DateTime strDate = DateTime.Now;


                    dd += 1; // Serial Number Generator

                    if (next_stage.ProcessId == 1)
                    {


                        ref_number = "LIT/1/" + dd + "/" + user_full_name + "/" + strDate.Day + "/" + strDate.Month +
                                     "/" +
                                     strDate.Year;
                    }
                    else if (next_stage.ProcessId == 2)
                    {
                        ref_number = "LIT/2/" + dd + "/" + user_full_name + "/" + strDate.Day + "/" + strDate.Month +
                                     "/" +
                                     strDate.Year;
                    }
                }
                r = new Request(next_stage.ProcessId, next_stage.Id, initiator.Id, next_user.Id, title, lifespan,
                    description, remark, note, ref_number, next_stage.RoleId);
                /* Thread */

                //Mail ms = new Mail();
                //Thread emailThread = new Thread(delegate()
                //{
                //    ms.mailDuringRequest(next_stage.ProcessId.ToString(), initiator.Id.ToString(),
                //        next_stage.RoleId.ToString(), initiator.BranchId.ToString());
                //});

                //emailThread.IsBackground = true;
                //emailThread.Start();
                /**** End Mail Sending Object ****/

                using (TransactionScope tran = new TransactionScope())
                {
                    if (!r.save())
                    {
                        this.setAsError();
                        return Base.NO_ID;
                    }

                    RequestTrail rt = new RequestTrail(r.Id, init_stage.Id, r.InitUserId, 0);

                    if (!rt.save())
                    {
                        this.setAsError();
                        return Base.NO_ID;
                    }

                    if (form_id != null)
                    {
                        if (
                            !RequestForm.saveForm(r.Id, rt.Id, form_id.Value, initiator.Id, init_stage.Id,
                                form_data_list))
                        {
                            this.setAsError();
                            return Base.NO_ID;
                        }
                    }
                    tran.Complete();
                }
               this.setAsSuccess();
                return r.Id;
            }
            else if (next_stage.ProcessId == 23)
            {
                r = new Request(next_stage.ProcessId, next_stage.Id, initiator.Id, next_user.Id, title, lifespan,
                    description, remark, note, "", next_stage.RoleId);
                /**** Instantiate Mail Sending Object ****/
                /**** Added 27th January 2016 ****/
                //Mail ms = new Mail();

                //ms.mailDuringRequest(init_stage.Id.ToString(), initiator.Id.ToString(), next_stage.RoleId.ToString(), initiator.BranchId.ToString());
                //ms.mailDuringRequest(next_stage.ProcessId.ToString(), initiator.Id.ToString(),
                //    next_stage.RoleId.ToString(), initiator.BranchId.ToString());

                /**** End Mail Sending Object ****/

                using (TransactionScope tran = new TransactionScope())
                {
                    if (!r.save())
                    {
                        this.setAsError();
                        return Base.NO_ID;
                    }

                    RequestTrail rt = new RequestTrail(r.Id, init_stage.Id, r.InitUserId, 0);

                    if (!rt.save())
                    {
                        this.setAsError();
                        return Base.NO_ID;
                    }

                    if (form_id != null)
                    {
                        if (
                            !RequestForm.saveForm(r.Id, rt.Id, form_id.Value, initiator.Id, init_stage.Id,
                                form_data_list))
                        {
                            this.setAsError();
                            return Base.NO_ID;
                        }
                    }
                    tran.Complete();
                }

                this.setAsSuccess();

                #region
             
               /* 
                * FESTUS COMMENT OUT, IT IS TRADEMARK PROCE
                * long trade_id = r.Id;

                string query = @"UPDATE [lpcm].[dbo].[request] SET status = 6 WHERE id= " + trade_id;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();

                    int i = cmd.ExecuteNonQuery();
                    if (i == 1)
                    {

                    }
                    else
                    {

                    }
                }*/
                return r.Id;

                #endregion

            } // Festus Edit  For Termination Process
            else if (next_stage.ProcessId == 22)
            {
                r = new Request(next_stage.ProcessId, next_stage.Id, initiator.Id, next_user.Id, title, lifespan,
                    description, remark, note, "", next_stage.RoleId);
                /**** Instantiate Mail Sending Object ****/
                /**** Added 27th January 2016 ****/
                //Mail ms = new Mail();

                //ms.mailDuringRequest(init_stage.Id.ToString(), initiator.Id.ToString(), next_stage.RoleId.ToString(), initiator.BranchId.ToString());
                //ms.mailDuringRequest(next_stage.ProcessId.ToString(), initiator.Id.ToString(),
                //    next_stage.RoleId.ToString(), initiator.BranchId.ToString());

                /**** End Mail Sending Object ****/

                using (TransactionScope tran = new TransactionScope())
                {
                    if (!r.save())
                    {
                        this.setAsError();
                        return Base.NO_ID;
                    }

                    RequestTrail rt = new RequestTrail(r.Id, init_stage.Id, r.InitUserId, 0);

                    if (!rt.save())
                    {
                        this.setAsError();
                        return Base.NO_ID;
                    }

                    if (form_id != null)
                    {
                        if (
                            !RequestForm.saveForm(r.Id, rt.Id, form_id.Value, initiator.Id, init_stage.Id,
                                form_data_list))
                        {
                            this.setAsError();
                            return Base.NO_ID;
                        }
                    }
                    tran.Complete();
                }

                this.setAsSuccess();

                #region

                long trade_id = r.Id;

                string query = @"UPDATE [lpcm].[dbo].[request] SET status = 4 WHERE id= '" + trade_id + "'";
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();

                    int i = cmd.ExecuteNonQuery();
                    if (i == 1)
                    {

                    }
                    else
                    {

                    }
                }
                return r.Id;

                #endregion
            }
            else
            {
                r = new Request(next_stage.ProcessId, next_stage.Id, initiator.Id, next_user.Id, title, lifespan,
                    description, remark, note, "", next_stage.RoleId);
                /**** Instantiate Mail Sending Object ****/
                /**** Added 27th January 2016 ****/
                //Mail ms = new Mail();

                //ms.mailDuringRequest(init_stage.Id.ToString(), initiator.Id.ToString(), next_stage.RoleId.ToString(), initiator.BranchId.ToString());
                //ms.mailDuringRequest(next_stage.ProcessId.ToString(), initiator.Id.ToString(),
                //    next_stage.RoleId.ToString(), initiator.BranchId.ToString());

                /**** End Mail Sending Object ****/

                using (TransactionScope tran = new TransactionScope())
                {
                    if (!r.save())
                    {
                        this.setAsError();
                        return Base.NO_ID;
                    }

                    RequestTrail rt = new RequestTrail(r.Id, init_stage.Id, r.InitUserId, 0);

                    if (!rt.save())
                    {
                        this.setAsError();
                        return Base.NO_ID;
                    }

                    if (form_id != null)
                    {
                        if (
                            !RequestForm.saveForm(r.Id, rt.Id, form_id.Value, initiator.Id, init_stage.Id,
                                form_data_list))
                        {
                            this.setAsError();
                            return Base.NO_ID;
                        }
                    }
                    tran.Complete();
                }



            }

            this.setAsSuccess();
            DateTime tds = DateTime.Now;
            string _username = UsersClass.GetName(_userID.ToString());
            bool res = audit.SaveAudit(_username, "Request Created : " + "<b>[" + title + "]</b>", "Request", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
            return r.Id;

        }

        public void proceed(long id, int next_stage_id, int? next_user_id, int? form_id,
            List<Dictionary<string, object>> form_data_list)
        {
            //get user in secondary role if exist 

            #region

            List<string> getList = new List<string>();
            string sec_role = null;
            short _sec = 0;
            string sql = @"select role_id from user_role where user_id = '" + next_user_id + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    while (rd.Read())
                    {
                        sec_role = rd.GetInt32(0).ToString();

                        getList.Add(sec_role);
                    }

                }

            }


            #endregion


            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {

                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
            }


            User u = Temp.GetUser((string)_userID);
            if (u == null)
            {
                this.setAsAccessDenied();
                return;
            }

            Dictionary<string, object> filter = new Dictionary<string, object>();
            List<string> fetch_with = new List<string>();

            //get request
            filter["id"] = id;

            DataTable result_table = Request.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return;
            }
            else if (result_table.Rows.Count == 0)
            {
                this.setAsError("Request does not exist");
                return;
            }

            r = Request.ToObject(result_table);

            //check if the request is on the logged-in user's desk
            //if (r.CurrentUserId != u.Id)
            //{
            //    this.setAsAccessDenied();
            //    return;
            //} 

            //get current stage
            filter.Clear();
            filter["id"] = r.StageId;
            result_table = Stage.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return;
            }
            else if (result_table.Rows.Count == 0)
            {
                this.setAsError();
                return;
            }

            Stage current_stage = Stage.ToObject(result_table);

            //get next stage
            filter.Clear();
            filter["id"] = next_stage_id;
            result_table = Stage.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return;
            }
            else if (result_table.Rows.Count == 0)
            {
                this.setAsError("Next stage does not exist");
                return;
            }

            Stage next_stage = Stage.ToObject(result_table);

            //festus


            if (sec_role == null)
            {

            }
            else if (getList.Contains(next_stage.RoleId.ToString()))
            {
                string role = next_stage.RoleId.ToString();
                _sec = short.Parse(role);
            }

            //15/9/2016
            #region

            string update = @"UPDATE request SET user_role='" + next_stage.RoleId + "' WHERE [request].id='" + r.Id +
                            "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(update, conn))
            {
                conn.Open();
                cmd.ExecuteNonQuery();

                conn.Close();
            }
            #endregion

            //check if user is assigned for next stage when the role for the next stage is different from the current
            if ((next_stage.RoleId != u.RoleId) && (next_user_id == null))
            {
                this.setAsError("User not assigned for next stage");
                return;
            }

            //check if the current stage and the next stage are in the same process
            if (current_stage.ProcessId != next_stage.ProcessId)
            {
                this.setAsError("The next stage provided is not in the same process");
                return;
            }

            //check if the current stage is a prerequisite to the next stage
            filter.Clear();
            filter["current_id"] = current_stage.Id;
            filter["next_id"] = next_stage.Id;
            string vaule = GetIfLastStage(next_stage.Id.ToString());
            if (vaule == "" || vaule == StatusMessage.not_available.ToString())
            {
                string _updateRequest = @"UPDATE request SET [status]= 6, user_role='"+ next_stage.RoleId + "' where [request].id='" + r.Id +
                            "'";
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                using (SqlCommand cmd = new SqlCommand(_updateRequest, conn))
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    this.setAsSuccess();
                }
            }
            else
            {
                result_table = StagePrecedence.fetch(filter: filter);
                if (Model.Base.hasError())
                {
                    this.setAsError();
                    return;
                }
                else if (result_table.Rows.Count == 0)
                {
                    this.setAsError("The current stage is not a pre-requisite to the next stage specified");
                    return;
                }



                //set trail
                int time_taken = Convert.ToInt32((DateTime.Now - r.ModifiedDate).TotalSeconds);
                RequestTrail rt = new RequestTrail(r.Id, r.StageId, r.CurrentUserId, time_taken);

                //update request
                if (next_user_id == null)
                {
                    r.moveToNextStage(next_stage_id);
                }
                else
                {

                    /*********** Mail Sending Part *************/
                    /*********** Added on 27th January 2016 *************/

                    //Mail mx = new Mail();

                    //mx.mailDuringProceed(current_stage.ProcessId.ToString(), u.Id.ToString(),
                    //    next_stage.RoleId.ToString(),
                    //    u.BranchId.ToString());

                    /************************/
                    filter["id"] = next_user_id;
                    filter["status"] = Model.Status.ACTIVE;

                    /**************/



                    #region

                    DataSet result = User.fetch(filter: filter);
                    if (Model.Base.hasError())
                    {
                        this.setAsError();
                        return;
                    }
                    else if (result.Tables.Count == 0)
                    {
                        this.setAsError();
                        return;
                    }
                    else if (result.Tables[0].Rows.Count == 0)
                    {
                        this.setAsError("Assigned user does not exist");
                        return;
                    }

                    User next_user = User.ToObject(result.Tables[0]);
                    if (_sec == 0)
                    {

                    }
                    else
                    {
                        //checking if role exist in secondary role
                        string data = Convert.ToString(getList.Contains(next_stage.RoleId.ToString()));
                        next_user.RoleId = _sec;
                    }



                    // 24th May 2016
                    string externalSolicitorRole = ConfigurationManager.AppSettings["EXT"].ToString();
                    //get external solicitor value from webconfig
                    short converterExternalSolicitorRole = short.Parse(externalSolicitorRole); //convert to short

                    if (next_stage.RoleId == converterExternalSolicitorRole) // compare the two values
                    {
                        //get user data
                        string userEmail = next_user.Email.ToString();
                        //get request title
                        string requestTitle = r.Title.ToString();
                        string desc = r.Description.ToString();
                        long resoureId = 0;
                        string doc = null;
                        string mimeType = null;
                        //get resource

                        #region

                        string getResource = @"select * from resource where request_id ='" + r.Id + "'";

                        using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                        using (SqlCommand cmd = new SqlCommand(getResource, conn))
                        {
                            conn.Open();
                            SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                            if (rd.HasRows)
                            {
                                while (rd.Read())
                                {
                                    resoureId = rd.GetInt64(0);
                                    doc = rd.GetString(4);
                                    mimeType = rd.GetString(6);
                                }
                            }
                        }

                        //get the actual 
                        if (doc != null && mimeType != null)
                        {
                            //string path = Path.GetFullPath(doc);
                            List<string> ext = new List<string>();
                            ext.Add(".pdf");
                            ext.Add(".doc");
                            ext.Add(".docx");
                            ext.Add(".xls");
                            string path = String.Empty;
                            string extension = doc.Substring(doc.LastIndexOf('.') + 1);
                            path = HttpContext.Current.Server.MapPath("~/resources/") + resoureId + "." + extension;


                            //send Email
                            string defaultEmail = ConfigurationManager.AppSettings["email"];
                            string defaultPassword = ConfigurationManager.AppSettings["emailPassword"];
                            using (MailMessage maill = new MailMessage(defaultEmail, userEmail))
                            {
                                maill.Subject = "FBN CASE MANAGER";
                                maill.Body = "Request Title : " + requestTitle + "\n \n" + " Request Description : " +
                                             desc;
                                maill.Attachments.Add(new Attachment(path, mimeType));

                                maill.IsBodyHtml = false;
                                SmtpClient smtp = new SmtpClient();
                                smtp.Host = "smtp.gmail.com";
                                smtp.EnableSsl = true;
                                NetworkCredential NetworkCred = new NetworkCredential(defaultEmail,
                                   defaultPassword);
                                smtp.UseDefaultCredentials = true;
                                smtp.Credentials = NetworkCred;
                                smtp.Port = 587;
                                smtp.Send(maill);

                            }

                        }

                        #endregion

                    }

                    //
                    if ((next_stage.RoleId != next_user.RoleId))
                    {
                        this.setAsError("Next stage cannot be assigned to user");
                        return;
                    }
                    else if (next_user.RoleId == _sec)
                    {

                    }

                    r.moveToNextStage(next_stage_id, next_user_id.Value);

                    #endregion

                }

                using (TransactionScope tran = new TransactionScope())
                {
                    if (!r.save() || !rt.save())
                    {
                        this.setAsError();
                        return;
                    }
                    if (form_id != null)
                    {
                        if (!RequestForm.saveForm(r.Id, rt.Id, form_id.Value, u.Id, current_stage.Id, form_data_list))
                        {
                            this.setAsError();
                            return;
                        }
                    }
                    tran.Complete();
                }
                this.setAsSuccess();


            }

        }

        public void end(long id, int? form_id, List<Dictionary<string, object>> form_data_list)
        {


            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {

                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
            }

            User u = Temp.GetUser((string)_userID);
            if (u == null)
            {
                this.setAsAccessDenied();
                return;
            }

            Dictionary<string, object> filter = new Dictionary<string, object>();
            List<string> fetch_with = new List<string>();

            //get request
            filter["id"] = id;

            DataTable result_table = Request.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return;
            }
            else if (result_table.Rows.Count == 0)
            {
                this.setAsError("Request does not exist");
                return;
            }

            Request r = Request.ToObject(result_table);

            //check if the request is on the logged-in user's desk
            if (r.CurrentUserId != u.Id) { this.setAsAccessDenied(); return; }

            //check if the current stage is a prerequisite to the next stage
            filter.Clear();
            filter["current_id"] = r.StageId;
            result_table = StagePrecedence.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return;
            }
            else if (result_table.Rows.Count != 0)
            {
                this.setAsError("The stage does not end the process");
                return;
            }

            //set trail
            int time_taken = Convert.ToInt32((DateTime.Now - r.ModifiedDate).TotalSeconds);
            RequestTrail rt = new RequestTrail(r.Id, r.StageId, r.CurrentUserId, time_taken);

            r.end();

            using (TransactionScope tran = new TransactionScope())
            {
                if (!r.save() || !rt.save())
                {
                    this.setAsError();
                    return;
                }
                if (form_id != null)
                {
                    if (!RequestForm.saveForm(r.Id, rt.Id, form_id.Value, u.Id, r.StageId, form_data_list))
                    {
                        this.setAsError();
                        return;
                    }
                }
                tran.Complete();
            }
            this.setAsSuccess();

        }

        public void cancel(long id)
        {
            //if (HttpContext.Current.Session["ID"] != null)
            //{
            //    _userID = HttpContext.Current.Session["ID"];
            //}
            //if (HttpContext.Current.Session["RoleID"] != null)
            //{
            //    _roleID = HttpContext.Current.Session["RoleID"];
            //}

            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {

                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
            }

            User initiator = Temp.GetUser((string)_userID);
            if (initiator == null)
            {
                this.setAsAccessDenied();
                return;
            }

            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["id"] = id;

            DataTable result = Request.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return;
            }
            else if (result.Rows.Count == 0)
            {
                this.setAsError("Request does not exist");
                return;
            }

            Request r = Request.ToObject(result);

            if (!Auth.isAdmin())
            {
                if (r.InitUserId != initiator.Id)
                {
                    this.setAsAccessDenied();
                    return;
                }
            }

            r.changeStatus(Model.Status.CANCELLED);

            if (!r.save())
            {
                this.setAsError();
                return;
            }

            this.setAsSuccess();

        }

        public void reassign(long id, int new_user_id)
        {
            //if (HttpContext.Current.Session["ID"] != null)
            //{
            //    _userID = HttpContext.Current.Session["ID"];
            //}
            //if (HttpContext.Current.Session["RoleID"] != null)
            //{
            //    _roleID = HttpContext.Current.Session["RoleID"];
            //}

            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {

                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
            }
            User u = Temp.GetUser((string)_userID);
            if (u == null)
            {
                this.setAsAccessDenied();
                return;
            }

            Dictionary<string, object> filter = new Dictionary<string, object>();
            //get request by id
            filter["id"] = id;
            DataTable result = Request.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return;
            }
            else if (result.Rows.Count == 0)
            {
                this.setAsError("Request does not exist");
                return;
            }

            Request r = Request.ToObject(result);

            //check if the request has already been completed. 
            if (r.Status == Model.Status.COMPLETED)
            {
                this.setAsError("Request already completed");
                return;
            }

            //getting the process
            filter.Clear();
            filter["id"] = r.ProcessId;
            filter["status"] = Model.Status.ACTIVE;
            result = Process.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return;
            }
            else if (result.Rows.Count == 0)
            {
                this.setAsError("Process does not exist");
                return;
            }

            Process p = Process.ToObject(result);

            //checking if user can reassign request
            if (p.ReassignRoleId != u.RoleId)
            {
                this.setAsError("User cannot reassign request");
                return;
            }

            //get new user to be assigned
            filter["id"] = new_user_id;
            filter["status"] = Model.Status.ACTIVE;

            DataSet resultset = User.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return;
            }
            else if (resultset.Tables.Count == 0)
            {
                this.setAsError();
                return;
            }
            else if (resultset.Tables[0].Rows.Count == 0)
            {
                this.setAsError("Assigned user does not exist");
                return;
            }

            User new_user = User.ToObject(resultset.Tables[0]);

            //get current stage
            filter.Clear();
            filter["id"] = r.StageId;
            result = Stage.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return;
            }
            else if (result.Rows.Count == 0)
            {
                this.setAsError();
                return;
            }

            Stage current_stage = Stage.ToObject(result);

            //ensuring that the role of the current stage and the new user are the same.
            if (new_user.RoleId != current_stage.RoleId)
            {
                this.setAsError("New user cannot be assigned to this request due to role difference");
                return;
            }

            r.reassign(new_user.Id);
            if (!r.save())
            {
                this.setAsError();
                return;
            }

            this.setAsSuccess();

        }

        public DataTable get(Dictionary<string, object> filter, List<string> fetch_with, int? offset, int? count)
        {
            DataTable data = Request.fetch(filter, fetch_with, offset, count);

            if (data == null)
            {
                this.setAsError();
            }
            else
            {
                this.setAsSuccess();
            }
            return data;
        }

        public DataTable getTrail(Dictionary<string, object> filter, List<string> fetch_with, int? offset, int? count)
        {
            DataTable data = RequestTrail.fetch(filter, fetch_with, offset, count);

            if (data == null)
            {
                this.setAsError();
            }
            else
            {
                this.setAsSuccess();
            }
            return data;
        }

        public DataTable getForms(Dictionary<string, object> filter, List<string> fetch_with, int? offset, int? count)
        {
            DataTable data = RequestForm.fetch(filter, fetch_with, offset, count);

            if (data == null)
            {
                this.setAsError();
            }
            else
            {
                this.setAsSuccess();
            }
            return data;
        }

        public DataSet getFormData(long request_trail_id)
        {
            DataSet data = FormData.fetch(request_trail_id);
            if (data == null)
            {
                this.setAsError();
            }
            else
            {
                this.setAsSuccess();
            }
            return data;
        }

        public long addComment(long request_id, int process_id, int stage_id, string message)
        {
            Comment c = new Comment();
            if (HttpContext.Current.Session["ID"] != null)
            {
                _userID = HttpContext.Current.Session["ID"];
            }
            if (HttpContext.Current.Session["RoleID"] != null)
            {
                _roleID = HttpContext.Current.Session["RoleID"];
            }

            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {

                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
            }

            User u = Temp.GetUser((string)_userID);
            if (u == null)
            {
                this.setAsAccessDenied();
                return Base.NO_ID;
            }

            //checking if message is empty
            if (message.Trim().Length == 0)
            {
                this.setAsError("Empty messages are not allowed");
                return Base.NO_ID;
            }

            c = new Comment(request_id, process_id, stage_id, u.Id, message);
            if (!c.save())
            {
                this.setAsError();
                return Base.NO_ID;
            }

            this.setAsSuccess();


            return c.Id;
        }

        public DataTable getComments(Dictionary<string, object> filter, List<string> fetch_with, int? offset, int? count)
        {
            DataTable data = Comment.fetch(filter, fetch_with, offset, count);

            if (data == null)
            {
                this.setAsError();
            }
            else
            {
                this.setAsSuccess();
            }
            return data;
        }


        private string getInitial(string user_id)
        {
            objConnection.Sql = @"SELECT [initial]
                                  FROM [lpcm].[dbo].[profile]
                                  where  user_id ='" + user_id + "'";
            DataTable dt = objConnection.GetTable;
            return dt.Rows[0]["initial"].ToString();
        }

        #endregion
        private string getInitiatorRole(string request_id)
        {
            objConnection.Sql = @"SELECT role_id      
                                FROM [lpcm].[dbo].[request]
                                inner join [user] on [user].id = request.init_user_id
                                where request.id ='" + request_id + "'";
            DataTable dt = objConnection.GetTable;
            return dt.Rows[0]["role_id"].ToString();
        }

        public DataSet GetExternal(string userid)
        {
            DataSet ds = new DataSet();
            string query = @"SELECT DISTINCT  [user].[id]
                                        ,[user].[email]
                                       
                                        ,[branch_id]
                                        ,[user].[role_id] 
                                        ,[created_date]
                                        ,[modified_date]
                                          ,[status]
	                                    ,FIRM,
                                         FirstName, LastName, ADDRESS,TEL,
	                          CASE [user].role_id
                            WHEN 11 THEN 'External Solicitor'
                          
                            END  as [role_name] 
                                        FROM [lpcm].[dbo].[user]
                                        inner join [external_solicitors] on [external_solicitors].id = [user].id
                                        where [external_solicitors].role_id = '" +
                           ConfigurationManager.AppSettings["EXT"] + "' and [user].[id]='" + userid + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds);
            }
            return ds;
        }



        #region
        public string GetIfLastStage(string id)
        {
            string sql = @"select next_id from [stage_precedence] Where current_id ='" + id + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        return StatusMessage.available.ToString();
                    }
                    else
                    {
                        return StatusMessage.not_available.ToString();
                    }
                }
                else
                {
                    return "";
                }
                conn.Close();
            }

        }
        #endregion

    }
}