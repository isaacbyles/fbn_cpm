﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Model;
using System.Data;
using CaseManager.Util;
using System.Transactions;
using Logger = CaseManager.HttpServerTest.Logger.Logger;

namespace CaseManager.Logic
{
    public class FormLogic : Base
    {
        #region Helper Methods
        #endregion 
        private static AuditLogic audit = new AuditLogic();
        private HttpCookie cookie;
        private Object _userID;
        public int add(string name, string description, IEnumerable<Dictionary<string, string>> control_data_list)
        {
            Form form = new Form(name, description);
            using (TransactionScope tran = new TransactionScope())
            {
                form.save();
                if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }

                foreach (var control_data in control_data_list)
                {
                    FormControl c = new FormControl(form.Id, control_data);
                    c.save();
                    if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }

                    FormControlData cd = new FormControlData(c.Id, control_data);
                    if (cd.Data != null)
                    {
                        cd.save();
                        if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }
                    }
                }

                tran.Complete();
            }

            this.setAsSuccess();
            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
                Logger.writelog(_userID.ToString() + "coo");
            }

            DateTime tds = DateTime.Now;
            string _username = UsersClass.GetName(_userID.ToString());
            bool res = audit.SaveAudit(_username, "Form Created : " + "<b>[" + name + "]</b>", "Form", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
            return form.Id;
        }

        public void edit(int form_id, string name, string description, IEnumerable<Dictionary<string, string>> control_data_list)
        {
            //fetching form
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["id"] = form_id;

            DataTable result = Form.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count != 0) { this.setAsError("Form does not exist"); return; }

            using (TransactionScope tran = new TransactionScope())
            {
                Form f = Form.ToObject(result);
                f.editDetails(name, description);
                f.save();
                if (Model.Base.hasError()) { this.setAsError(); return; }
                
                //Deactivate all of it's previous controls
                FormControl.changeStatusByFormId(f.Id, Model.Status.REMOVED);

                //add the new controls
                foreach (var control_data in control_data_list)
                {
                    FormControl c = new FormControl(f.Id, control_data);
                    c.save();
                    if (Model.Base.hasError()) { this.setAsError(); return; }

                    FormControlData cd = new FormControlData(c.Id, control_data);
                    if (cd.Data != null)
                    {
                        cd.save();
                        if (Model.Base.hasError()) { this.setAsError(); return; }
                    }
                }

                tran.Complete();
            }

            this.setAsSuccess();
        }

        public void changeStatus(int form_id, short status)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["id"] = form_id;

            DataTable result = Form.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError("Form does not exist"); return; }

            Form f = Form.ToObject(result);

            f.changeStatus(status);
            if (!f.save()) { this.setAsError(); return; }

            this.setAsSuccess();
        }

        public DataTable get(Dictionary<string, object> filter, int? offset, int? count)
        {
            this.setAsSuccess();
            return Form.fetch(filter: filter, offset: offset, count: count);
        }

        public DataTable getControls(int form_id)
        {
            this.setAsSuccess();
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["form_id"] = form_id;
            return FormControl.fetch(filter: filter);
        }
    }
}