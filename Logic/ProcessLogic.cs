﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Model;
using System.Data;
using CaseManager.Util;
using System.Transactions;
using Logger = CaseManager.HttpServerTest.Logger.Logger;

namespace CaseManager.Logic
{
    public class ProcessLogic : Base
    {

        #region Helper methods
        #endregion
        private static AuditLogic audit = new AuditLogic();
        private HttpCookie cookie;
        private Object _userID;
        #region Process methods
        public int add(string name, string description, short reassign_role_id)
        {
            //check if there is another process with that name
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["name"] = name;

            DataTable result = Process.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }
            else if (result.Rows.Count != 0) { this.setAsError("A process with name exists in the system"); return Base.NO_ID; }

            Process p = new Process(name, description, reassign_role_id);
            if (!p.save()) { this.setAsError(); return Base.NO_ID; }

            this.setAsSuccess();
            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
                Logger.writelog(_userID.ToString() + "coo");
            }

            DateTime tds = DateTime.Now;
            string _username = UsersClass.GetName(_userID.ToString());
            bool res = audit.SaveAudit(_username, "Process Created : " + "<b>[" + name + "]</b>", "Process", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
            return p.Id;
        }

        public void edit(int id, string name, string description, short reassign_role_id)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();

            //get process by id
            filter["id"] = id;
            DataTable result = Process.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError(); return; }

            // creating a process object from the datatable
            Process p = Process.ToObject(result);

            //check if there is another process with that name
            filter["name"] = name;

            result = Process.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count != 0) { this.setAsError("A process with name exists in the system"); return; }

            p.editDetails(name, description, reassign_role_id);

            if (p.save()) { this.setAsSuccess(); return; }
            else { this.setAsError(); return; }
        }

        public void changeStatus(int id, short status)
        {
            //get process by id
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["id"] = id;

            DataTable result = Process.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError("Process does not exist"); return; }

            Process p = Process.ToObject(result);

            p.changeStatus(status);
            if (!p.save()) { this.setAsError(); return; }

            this.setAsSuccess();
        }

        public DataTable get(Dictionary<string, object> filter, int? offset, int? count)
        {
            this.setAsSuccess();
            return Process.fetch(filter: filter);
        }
        #endregion

        #region Stage
        public int addStage(int process_id, short role_id, string name, string description, int? form_id = null, short type = Stage.TYPE_NORMAL)
        {
            Process p = Process.getById(process_id);
            if (p == null)
            {
                this.setAsError(); 
               return Base.NO_ID;
            }

            if (p.Status != Model.Status.INACTIVE)
            {
                this.setAsError("Creation of a stage not allowed"); 
               return Base.NO_ID;
            }

            Stage s = new Stage(process_id, role_id, name, description, type);
            using (TransactionScope tran = new TransactionScope())
            {
                if (!s.save())
                {
                    this.setAsError(); 
                }
                if (form_id != null)
                {
                    StageForm sf = new StageForm(form_id.Value, s.Id);
                    if (!sf.save())
                    {
                        this.setAsError(); 
                       return Base.NO_ID;
                    }
                }

                tran.Complete();
            }

            this.setAsSuccess();
            //DateTime tds = DateTime.Now;
            //string _username = UsersClass.GetName(_userID.ToString());
            //bool res = audit.SaveAudit(_username, "Stage Created : " + "<b>[" + name + "]</b>", "Stage", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
            return s.Id;
        }

        public void editStageDetails(int stage_id, string name, string description)
        {
            //fetching the stage with its process
            Dictionary<string, object> filter = new Dictionary<string, object>();
            List<string> fetch_with = new List<string>();

            filter["id"] = stage_id;

            DataTable result = Stage.fetch(filter: filter, fetch_with: fetch_with);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError("Stage does not exist"); return; }

            Stage s = Stage.ToObject(result);

            s.editDetails(name, description);
            if (!s.save()) { this.setAsError(); return; }
            this.setAsSuccess();
        }

        public void addStageForm(int stage_id, int form_id)
        {
            //fetching the stage with its process
            Dictionary<string, object> filter = new Dictionary<string, object>();
            List<string> fetch_with = new List<string>();

            filter["id"] = stage_id;
            fetch_with.Add("process");

            DataTable result = Stage.fetch(filter: filter, fetch_with: fetch_with);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError("Stage does not exist"); return; }

            Stage s = Stage.ToObject(result);

            if (!s.isEditable()) { this.setAsError("Stage Form cannot be added"); return; }

            using (TransactionScope tran = new TransactionScope())
            {
                StageForm.MassDelete(stage_id);
                if (Model.Base.hasError()) { this.setAsError(); return; }

                StageForm sf = new StageForm(form_id, s.Id);
                if (!sf.save()) { this.setAsError(); return; }

                tran.Complete();
            }
            
            this.setAsSuccess();
        }

        public void removeStageForm(int stage_id)
        {
            //fetching the stage with its process
            Dictionary<string, object> filter = new Dictionary<string, object>();
            List<string> fetch_with = new List<string>();

            filter["id"] = stage_id;
            fetch_with.Add("process");

            DataTable result = Stage.fetch(filter: filter, fetch_with: fetch_with);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError("Stage does not exist"); return; }

            Stage s = Stage.ToObject(result);

            if (!s.isEditable()) { this.setAsError("Stage Form cannot be removed"); return; }

            StageForm.MassDelete(stage_id);
            if (Model.Base.hasError()) { this.setAsError(); return; }

            this.setAsSuccess();
        }

        public DataTable getStages(Dictionary<string, object> filter, List<string> fetch_with, int? offset, int? count)
        {
            this.setAsSuccess();
            return Stage.fetch(filter: filter, fetch_with: fetch_with, offset: offset, count: count);
        }

        public void removeStage(int stage_id)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();
            List<string> fetch_with = new List<string>();

            filter["id"] = stage_id;
            fetch_with.Add("process");

            DataTable result = Stage.fetch(filter: filter, fetch_with: fetch_with);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError("Stage does not exist"); return; }

            Stage s = Stage.ToObject(result);

            if (!s.isEditable()) { this.setAsError("Stage cannot be removed"); return; }

            using (TransactionScope tran = new TransactionScope())
            {
                StageForm.MassDelete(stage_id);
                if (Model.Base.hasError()) { this.setAsError(); return; }

                if (!s.delete()) { this.setAsError(); return; }

                tran.Complete();
            }
            this.setAsSuccess();
        }

        public void setWorkFlow(int process_id, IEnumerable<Dictionary<string, string>> stage_precedence)
        {
            Process p = Process.getById(process_id);
            if (p == null) { this.setAsError(); return; }

            if (p.Status != Model.Status.INACTIVE) { this.setAsError("Creation of work flow is not allowed"); return; }

            using (TransactionScope tran = new TransactionScope())
            {
                foreach (var link in stage_precedence)
                {
                    StagePrecedence sp = new StagePrecedence(process_id, link);
                    if (!sp.save()) { this.setAsError(); return; }
                }

                p.changeStatus(Model.Status.ACTIVE);
                if (!p.save()) { this.setAsError(); return; }

                tran.Complete();
            }
            this.setAsSuccess();
        }

        public DataTable getWorkFlow(Dictionary<string, object> filter, List<string> fetch_with)
        {
            this.setAsSuccess();
            return StagePrecedence.fetch(filter: filter, fetch_with: fetch_with);
        }
        #endregion
    }
}