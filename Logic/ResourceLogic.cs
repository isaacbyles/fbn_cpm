﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Model;
using System.Data;
using CaseManager.Util;

namespace CaseManager.Logic
{
    public class ResourceLogic : Base
    {
        private HttpCookie cookie = new HttpCookie("ID");
        private Resource _r;
        public  Object _roleID;
        public  Object _userID;
        AuditLogic audit = new AuditLogic();
        public long add(long request_id, string label, string description, string mime_type, short route)
        {

            cookie = HttpContext.Current.Request.Cookies["ID"];
            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
            }
            User u = Temp.GetUser((string) _userID);
            if (u == null)
            {
                this.setAsAccessDenied();
                return Base.NO_ID;
            }

            Dictionary<string, object> filter = new Dictionary<string, object>();
            List<string> fetch_with = new List<string>();

            //get request
            filter["id"] = request_id;

            DataTable result_table = Request.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return Base.NO_ID;
            }
            else if (result_table.Rows.Count == 0)
            {
                this.setAsError("Request does not exist");
                return Base.NO_ID;
            }

            Request request = Request.ToObject(result_table);
            _r = new Resource(request_id, request.StageId, u.Id, label, description, mime_type, route);
            if (!_r.save())
            {
                this.setAsError();
                return Base.NO_ID;
            }
            this.setAsSuccess();
            DateTime tds = DateTime.Now;
            string _username = UsersClass.GetName(_userID.ToString());
            bool res = audit.SaveAudit(_username, "Resource Created : " + "<b></b>", "Resource", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
           
            return _r.Id;
        }

        public void editDetails(long resource_id, string label, string description)
        {
        

            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {

                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                 _userID = cookie.Value;
            }

                User u = Temp.GetUser((string)_userID);
                if (u == null)
                {
                    this.setAsAccessDenied();
                    return;
                }

                Dictionary<string, object> filter = new Dictionary<string, object>();
                filter["id"] = resource_id;

                DataTable result = Resource.fetch(filter: filter);
                if (Model.Base.hasError())
                {
                    this.setAsError();
                    return;
                }
                else if (result.Rows.Count == 0)
                {
                    this.setAsError("Resource does not exist");
                    return;
                }

                Resource r = Resource.ToObject(result);

                //check if the resource was sent by user.
                if (r.UserId != u.Id)
                {
                    this.setAsAccessDenied();
                    return;
                }

                r.changeDetails(label, description);

                if (!r.save())
                {
                    this.setAsError();
                    return;
                }
                this.setAsSuccess();
            
            return;
        }

        //admin function
        public void changeStatus(long resource_id, short status)
        {
            

            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {

                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
               _userID = cookie.Value;
            }

                User u = Temp.GetUser((string)_userID);
                if (u == null)
                {
                    this.setAsAccessDenied();
                    return;
                }

                Dictionary<string, object> filter = new Dictionary<string, object>();
                filter["id"] = resource_id;

                DataTable result = Resource.fetch(filter: filter);
                if (Model.Base.hasError())
                {
                    this.setAsError();
                    return;
                }
                else if (result.Rows.Count == 0)
                {
                    this.setAsError("Resource does not exist");
                    return;
                }

                Resource r = Resource.ToObject(result);

                //check if the resource was sent by user.
                if (r.UserId != u.Id)
                {
                    this.setAsAccessDenied();
                    return;
                }

                r.changeStatus(status);

                if (!r.save())
                {
                    this.setAsError();
                    return;
                }
                this.setAsSuccess();
            
            return;
        }

        public DataTable get(Dictionary<string, object> filter, List<string> fetch_with, int? offset, int? count)
        {
            DataTable data = Resource.fetch(filter, fetch_with, offset, count);
            if (data == null)
            {
                this.setAsError();
            }
            else
            {
                this.setAsSuccess();
            }
            return data;
        }
    }
}