﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Model;
using System.Data;
using CaseManager.Util;

namespace CaseManager.Logic
{
    public class StatLogic : Base
    {
        public DataSet getActiveCount()
        {
            DataSet data = Stat.query(Stat.SQL_ACTIVE_COUNT);

            if (data == null)
            {
                this.setAsError();
            }
            else
            {
                this.setAsSuccess();
            }
            return data;
        }
    }
}