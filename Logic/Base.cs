﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Logic
{
    public abstract class Base
    {
        #region Constants
        public const int STATUS_SUCCESS = 1;
        public const int STATUS_ERROR = 2;
        public const int STATUS_ACCESS_DENIED = 3;
        public const int STATUS_UNKNOWN = -1;
        public const string DEFAULT_ERROR_MSG = "Oops! An error has occurred";
        public const int NO_ID = -1;
        #endregion

        #region Data Members
        protected int _status = Base.STATUS_UNKNOWN;
        protected string _message = "";
        #endregion

        #region Properties
        public int Status
        {
            get { return this._status; }
        }

        public string Message
        {
            get { return this._message; }
        }
        #endregion

        #region Data Members
        /// <summary>
        /// It shows the status of action performed in the logic class is successful.
        /// </summary>
        /// <returns>True if successful else False</returns>
        public bool isSuccessful()
        {
            return this._status == Base.STATUS_SUCCESS;
        }

        /// <summary>
        /// Sets the status of the logic instance as successful.
        /// It is to be used within an action method in the logic class
        /// </summary>
        protected void setAsSuccess()
        {
            this._status = Base.STATUS_SUCCESS;
            this._message = "";
        }

        /// <summary>
        /// Sets the status of the logic instance as error.
        /// It is to be used within an action method in the logic class
        /// </summary>
        /// <param name="message">The error message stating the issue</param>
        protected void setAsError(string message = Base.DEFAULT_ERROR_MSG)
        {
            this._status = Base.STATUS_ERROR;
            this._message = message;
        }

        /// <summary>
        /// Sets the status of the logic instance to a custom value
        /// and also sets a corresponding message
        /// </summary>
        /// <param name="status">The custom status</param>
        /// <param name="message">The corresponding message</param>
        protected void setAsCustom(int status, string message)
        {
            this._status = status;
            this._message = message;
        }

        protected void setAsAccessDenied()
        {
            this._status = Base.STATUS_ACCESS_DENIED;
            this._message = "Access denied!";
        }
        #endregion
    }
}