﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Net.Mail;
using CaseManager.Model;
using System.Data;
using System.EnterpriseServices;
using CaseManager.Util;
using System.Transactions;
using Newtonsoft.Json;
using System.Threading;

namespace CaseManager.Logic
{
    public class Mail
    {
       
        // Mail Host E.g Google's SMTP Server
        string mailhost = "smtp.gmail.com";
        // Mail Port
        int mailPort = 587;
        // TimeOut Time is Max 20 Seconds
        int timeout = 20000;
        // Administrator's EMail Address
        string mailAdmin = "tundeesanju@gmail.com";
        // Administrator's EMail Readable Name
        string mailAdminRead = "Case Manager Administrator";
        // Mail Admin Password
        string mailAdminPass = "Florence@4039";

        public void mailDuringRequest(string initialStage, string initiator, string nextstage, string initiatorBranch) 
        {
            CaseManager.API.FilterUsers ade = new CaseManager.API.FilterUsers();

            string _json = ade.HandlerNonWeb(initialStage, initiator, nextstage, initiatorBranch);

            string processName = Access.GetProcessName(initialStage);
            string branchName = Access.GetBranchName(initiatorBranch);
            dynamic dynJson = JsonConvert.DeserializeObject(_json);

            SmtpClient client = new SmtpClient();

            client.Port = mailPort;

            client.Host = mailhost;

            client.EnableSsl = true;

            // TimeOut Time is Max 20 Seconds
            client.Timeout = timeout;

            client.DeliveryMethod = SmtpDeliveryMethod.Network;

            client.UseDefaultCredentials = false;

            MailAddress from = new MailAddress(mailAdmin, mailAdminRead, System.Text.Encoding.UTF8);

            client.Credentials = new System.Net.NetworkCredential(mailAdmin, mailAdminPass);

            // Set destinations for the e-mail message.
            MailAddressCollection TO_addressList = new MailAddressCollection();


            // Append EMail Addresses

            foreach (var item in dynJson)
            {
                MailAddress mytoAddress = new MailAddress(((Object)item.email).ToString());
                TO_addressList.Add(mytoAddress);
            }
            if (TO_addressList.Count == 0)
            {

            }
            else
            {
                MailMessage mm = new MailMessage(from.ToString(), TO_addressList.ToString());

                string body = "You have a pending request on " + processName + " from the  " + branchName + " " +
                              DateTime.Now.ToLongDateString();
                body += "<b/> Click the link to login " + Access.Url();
                mm.Body = body;

                mm.Subject = "A New Request Has Been Created";

                mm.SubjectEncoding = System.Text.Encoding.UTF8;

                mm.BodyEncoding = System.Text.Encoding.UTF8;

                mm.IsBodyHtml = true;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                try
                {
                    client.Send(mm);
                    mm.Dispose();
                }
                catch (Exception exe)
                {
                    //Mail.setAsError("Mail Sending Failed. Reason is " + exe.Message.ToString());
                    mm.Dispose();
                }
            }
            
           
            
        }

        
        public void mailDuringProceed(string currentStagePid, string userid, string nextstageRoleId, string userBranchId)
        {
            CaseManager.API.FilterUsers ade = new CaseManager.API.FilterUsers();
            string processName = Access.GetProcessName(currentStagePid);
            string branchName = Access.GetBranchName(userBranchId);
            string _json = ade.HandlerNonWeb(currentStagePid, userid, nextstageRoleId, userBranchId);

            dynamic dynJson = JsonConvert.DeserializeObject(_json);

            SmtpClient client = new SmtpClient();

            client.Port = mailPort;

            client.Host = mailhost;

            client.EnableSsl = true;

            // TimeOut Time is Max 20 Seconds
            client.Timeout = timeout;

            client.DeliveryMethod = SmtpDeliveryMethod.Network;

            client.UseDefaultCredentials = false;

            MailAddress from = new MailAddress(mailAdmin, mailAdminRead, System.Text.Encoding.UTF8);

            client.Credentials = new System.Net.NetworkCredential(mailAdmin, mailAdminPass);

            // Set destinations for the e-mail message.
            MailAddressCollection TO_addressList = new MailAddressCollection();

            // Append EMail Addresses

            foreach (var item in dynJson)
            {
                MailAddress mytoAddress = new MailAddress(((Object)item.email).ToString());
                TO_addressList.Add(mytoAddress);
            }
            if (TO_addressList.Count == 0)
            {

            }
            else
            {

                MailMessage mm = new MailMessage(from.ToString(), TO_addressList.ToString());

                string body = "You have a pending request on " + processName + " from the  " + branchName + " " +
                         DateTime.Now.ToLongDateString();
                

                if (nextstageRoleId == ConfigurationManager.AppSettings["EXT"].ToString())
                {
                    body =
                        "You have a pending case request from the Legal Department First Bank Nigeria Plc for your action by 00:00, dd-mm-yyyy.";

                    string url = "http://www.lawpavilion.com/fbn/pages/index.php";
                    body += "<br /><a href = '" + url + "'>Click here to enter</a>";

                    body += "<br /><br />Thanks";


                }
                else
                {
                    
                }
                // Message Body
                body += "<b/> Click the link to login " + Access.Url();
                mm.Body = body;

                // Message Subject
                mm.Subject = "A Request Stage Has Been Assigned";

                mm.SubjectEncoding = System.Text.Encoding.UTF8;

                mm.BodyEncoding = System.Text.Encoding.UTF8;
                mm.IsBodyHtml = true;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                try
                {
                    client.Send(mm);
                    mm.Dispose();
                    //System.Diagnostics.Debug.WriteLine("Mail Sending Succedded");
                }
                catch (Exception exe)
                {
                    //System.Diagnostics.Debug.WriteLine("Mail Sending Failed " + exe.Message.ToString());
                    //Mail.setAsError("Mail Sending Failed.");
                    mm.Dispose();
                }

            }


        }

        internal static void setAsError(string f)
        {
            throw new NotImplementedException();
        }
    }
}