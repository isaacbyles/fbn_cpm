﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Policy;
using System.Web;
using CaseManager.Util;

namespace CaseManager.Logic
{
    public class Access
    {

        private static string objBranch;
        private static string objProcess;
        public static string GetBranchName(string branchID)
        {
            string sql = @"SELECT name FROM branch WHERE id= '" + branchID + "'";
            using(SqlConnection conn =  new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        objBranch = rd.GetString(0);
                    }
                }
            }
            return objBranch;

        }

        public static string GetProcessName(string processID)
        {
            string sql = @"SELECT name FROM process WHERE id= '" + processID + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        objProcess = rd.GetString(0);
                    }
                }
            }
            return objProcess;
        }


        public static string Url()
        {
            return (string) TempStorage.TheData;
        }
       
    }
}