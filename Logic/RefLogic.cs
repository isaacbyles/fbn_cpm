﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using CaseManager.Model;
using System.Data;
using CaseManager.Util;
using Logger = CaseManager.HttpServerTest.Logger.Logger;

namespace CaseManager.Logic
{
    public class RefLogic : Base
    {
        #region Constructor
        public RefLogic(string model)
        {
            this.model = model;
        }
        #endregion

        #region Data Member
        private string model;
        #endregion

        #region Helpers
        #endregion

        #region Methods
        private static AuditLogic audit = new AuditLogic();
        private HttpCookie cookie;
        private Object _userID;
        public object add(string name)
        {
            //checking if a branch with that name exists
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["name"] = name;

            DataTable result = Ref.fetch(this.model, filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }
            else if (result.Rows.Count != 0) { this.setAsError("A " + this.model + " with name exists in the systyem"); return Base.NO_ID; }

            //adding the new ref
            Ref b = new Ref(this.model, name);
            b.save();
            if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }

            this.setAsSuccess();
            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
                Logger.writelog(_userID.ToString() + "coo");
            }

            DateTime tds = DateTime.Now;
            string _username = UsersClass.GetName(_userID.ToString());
            bool res = audit.SaveAudit(_username, "Role Created : " + "<b>[" + name + "]</b>", "Role", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
            return b.Id;
        }

        public void edit(object id, string name)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();

            //fetching the ref by id
            filter["id"] = id;
            DataTable result = Ref.fetch(this.model, filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError(); return; }

            // creating a ref from the datatable
            Ref b = Ref.ToObject(this.model, result);

            //checking if update is needed
            if (b.Name == Text.trimmer.Replace(name.ToLower().Trim(), " ")){
                this.setAsSuccess();
                return;
            }

            //checking if a ref with that name exists
            filter.Clear();
            filter["name"] = name;

            result = Ref.fetch(this.model, filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count != 0) { this.setAsError("Another " + this.model + " with name exists in the systyem"); return; }

            b.Name = name;
            b.save();
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else { this.setAsSuccess(); return; }
        }

        public void changeActivefg(object id, bool active_fg)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();

            //fetching the ref by id
            filter["id"] = id;
            DataTable result = Ref.fetch(this.model, filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError(); return; }

            // creating a ref from the datatable
            Ref b = Ref.ToObject(this.model, result);

            //checking if update is needed
            if (b.ActiveFg == active_fg)
            {
                this.setAsSuccess();
                return;
            }

            b.ActiveFg = active_fg;

            b.save();
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else { this.setAsSuccess(); return; }
        }

        public DataTable getAll(bool? active_fg=null)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();

            if (active_fg != null)
            {
                filter["active_fg"] = active_fg;
            }

            DataTable result = Ref.fetch(this.model, filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return null; }
            else { this.setAsSuccess(); return result; }
        }
        #endregion
    }
}