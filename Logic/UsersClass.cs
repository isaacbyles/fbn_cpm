﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CaseManager.Logic
{
    public class UsersClass
    {
        public static string GetName(string userID)
        {
            string name = "";
            string sql = @"select concat([firstname],[lastname]) as fullname
                                from tmp_data_login where id= '" + userID + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    while (rd.Read())
                    {
                        name = rd.GetString(0);
                    }
                }
            }
            return name;
        }
    }
}