﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CaseManager.Logic
{

    public class AuditLogic
    {

        public bool SaveAudit(string user_id, string details, string action, string ip, string mac)
        {
            string query =
                @"insert into [lpcm].[dbo].[tbl_audit] (user_id, action, details, timestamp, ip,  mac_address) values ('" +
                user_id +
                "',  '" + action + "', '" + details + "',  '" + DateTime.Now.ToLongDateString() + "','" + ip + "', '" +
                mac + "')";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            {
                conn.Open();
                using (IDbTransaction tran = conn.BeginTransaction())
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    try
                    {
                        cmd.Transaction = tran as SqlTransaction;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            tran.Commit();
                            return true;

                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        throw;
                    }
                    conn.Close();
                }
            }
        }
    }
}