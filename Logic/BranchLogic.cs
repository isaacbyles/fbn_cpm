﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using CaseManager.Model;
using System.Data;
using System.Data.SqlClient;
using CaseManager.Util;
using Logger = CaseManager.HttpServerTest.Logger.Logger;


namespace CaseManager.Logic
{
    public class BranchLogic : Base
    {
        private HttpCookie cookie;
        private static AuditLogic audit = new AuditLogic();

        private Object _userID;
        #region Methods
        public object add(string name, string street, string town, string state, string region, string country,string type)
        {
            //checking if a branch with that name exists
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["name"] = name;

            DataTable result = Branch.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }
            else if (result.Rows.Count != 0) { this.setAsError("A branch with this name exists in the systyem"); return Base.NO_ID; }

            //adding the new ref
            Branch b = new Branch(name, street, town, state, region, country,type);
            b.save();
            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
                Logger.writelog(_userID.ToString() + "coo");
            }
           // string _id = this.context.Session["User_id"].ToString();
            DateTime tds = DateTime.Now;
            string _username = UsersClass.GetName(_userID.ToString());
            bool res = audit.SaveAudit(_username, "Branch Created : " + "<b>[" + name + "]</b>", "Branch", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
            if (Model.Base.hasError()) { this.setAsError(); return Base.NO_ID; }

            this.setAsSuccess();
            return b.Id;
        }

        public void edit(object id, string name, string street, string town, string state, string region, string country)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();

            //fetching the ref by id
            filter["id"] = id;
            DataTable result = Branch.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError(); return; }

            // creating a ref from the datatable
            Branch b = Branch.ToObject(result);

            //checking if update is needed
            if (b.Name != Text.trimmer.Replace(name.ToLower().Trim(), " "))
            {
                //checking if a ref with that name exists
                filter.Clear();
                filter["name"] = name;

                result = Branch.fetch(filter: filter);
                if (Model.Base.hasError()) { this.setAsError(); return; }
                else if (result.Rows.Count != 0) { this.setAsError("Another branch with name exists in the systyem"); return; }
            }

            b.changeDetails(name, street, town, state, region, country);
            b.save();
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else { this.setAsSuccess(); return; }
        }

        public void changeActivefg(object id, bool active_fg)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();

            //fetching the ref by id
            filter["id"] = id;
            DataTable result = Branch.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError(); return; }

            // creating a ref from the datatable
            Branch b = Branch.ToObject(result);

            //checking if update is needed
            if (b.ActiveFg == active_fg)
            {
                this.setAsSuccess();
                return;
            }

            b.ActiveFg = active_fg;

            b.save();
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else { this.setAsSuccess(); return; }
        }

        public DataTable getAll(bool? active_fg = null)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();

            if (active_fg != null)
            {
                filter["active_fg"] = active_fg;
            }

            DataTable result = Branch.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return null; }
            else { this.setAsSuccess(); return result; }
        }


        #endregion

        #region
        //get user details
        
        #endregion
    }
}