﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using CaseManager.Model;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mime;
using CaseManager.Util;
using System.Transactions;
using Logger = CaseManager.HttpServerTest.Logger.Logger;

namespace CaseManager.Logic
{
    public class UserLogic : Base
    {
        public static DataSet result;
        public static Object IDD;
        public static Object RoleID;
        public Object _userID;
        AuditLogic audit = new AuditLogic();
        
       // private HttpCookie cookie = new HttpCookie("ID");
        #region Methods
        private HttpCookie cookie = new HttpCookie("ID");
        public object add(string email, string password, int branch_id, short role_id, string firstname, string lastname,
            string middlename, string telephone, string initial)
        {

            /****  fetch id from cookie   ***/
            cookie = HttpContext.Current.Request.Cookies["ID"];
            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                if (cookie != null) _userID = cookie.Value;
            }
           

            //
            //checking if user with this email exists
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["email"] = email.ToLower().Trim();
            filter["all_status"] = ""; //used to check all users independent of status

            DataSet result = User.fetch(filter: filter);
            if (Model.Base.hasError())
            {
                this.setAsError();
                return Base.NO_ID;
            }
            else if (result.Tables.Count == 0)
            {
                this.setAsError();
                return Base.NO_ID;
            }
            else if (result.Tables[0].Rows.Count != 0)
            {
                this.setAsError("A user with the provided email exists in the systyem");
                return Base.NO_ID;
            }

            //adding the new user
            //User u = new User(email, password, branch_id, role_id);


            //ADDING user
            string sql = @"INSERT INTO [user]
                            (email, password, branch_id, role_id, created_date, modified_date, status, adminID) 
                            OUTPUT INSERTED.ID 
                            VALUES ('" + email + "', '" + password + "', '" + branch_id + "', '" + role_id + "', '" +
                         DateTime.Now + "', '" +
                         DateTime.Now + "', '" + Model.Status.INACTIVE + "' , '" + _userID + "')";
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["conn"]);
            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(sql, sqlConnection);
            object id = cmd.ExecuteScalar();

            string concat = "";
            Profile p = new Profile(Convert.ToInt32(id), firstname, lastname, middlename, telephone, initial);
            p.save();

            this.setAsSuccess();

            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
                //Logger.writelog(_userID.ToString() + "coo");
            }
           
            DateTime tds = DateTime.Now;
            string _username = UsersClass.GetName(_userID.ToString());
            bool res = audit.SaveAudit(_username, "User Created : " + "<b>[" + firstname + " " + lastname + "]</b>", "User", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
            return id;
        }

        public void changePassword(string oldPassword, string newPassword)
        {
              HttpCookie cookie = new HttpCookie("ID");
            cookie = HttpContext.Current.Request.Cookies["ID"];
            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            else
            {
                User u = Temp.GetUser(cookie.Value);
                if (u == null)
                {
                    this.setAsAccessDenied();
                    return;
                }

                //checking if the old password is correct
                Dictionary<string, object> filter = new Dictionary<string, object>();
                filter["id"] = u.Id;
                filter["password"] = PasswordGen.generate(oldPassword);
                filter["all_status"] = ""; //used to check all users independent of status

                DataSet result = User.fetch(filter: filter);
                if (Model.Base.hasError())
                {
                    this.setAsError();
                    return;
                }
                else if (result.Tables.Count == 0)
                {
                    this.setAsError();
                    return;
                }
                else if (result.Tables[0].Rows.Count == 0)
                {
                    this.setAsError("Invalid old password provided");
                    return;
                }

                u = User.ToObject(result.Tables[0]);
                u.changePassword(newPassword);
                u.save();

                if (Model.Base.hasError())
                {
                    this.setAsError();
                    return;
                }

                this.setAsSuccess();
            }
        }

        public void editProfile(int user_id, string firstname, string lastname, string middlename, string telephone)
        {
            //if (!Auth.isAdmin()) { this.setAsAccessDenied(); return; }

            //checking if the old password is correct
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["user_id"] = user_id;

            DataTable result = Profile.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Rows.Count == 0) { this.setAsError("User does not exist"); return; }

            Profile p = Profile.ToObject(result);

            p.edit(firstname, lastname, middlename, telephone);
            p.save();
            
            if (Model.Base.hasError()) { this.setAsError(); return; }

            this.setAsSuccess();
        }

        public void changePriviledge(int user_id, int branch_id, short role_id)
        {
            //if (!Auth.isAdmin()) { this.setAsAccessDenied(); return; }

            //checking if the old password is correct
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["id"] = user_id;
            filter["all_status"] = ""; //used to check all users independent of status

            DataSet result = User.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Tables.Count == 0) { this.setAsError(); return; }
            else if (result.Tables[0].Rows.Count == 0) { this.setAsError("User does not exist"); return; }

            User u = User.ToObject(result.Tables[0]);

            u.changePriviledge(branch_id, role_id);
            u.save();

            if (Model.Base.hasError()) { this.setAsError(); return; }

            this.setAsSuccess();
        }

        public int CheckAdminPriviledge(int id)
        {
            int retrieveID = 0;
            string check = @"SELECT adminID FROM [user] where id= '" + id + "'";
            using (SqlConnection mycon = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmdd = new SqlCommand(check, mycon))
            {
                mycon.Open();
                SqlDataReader rd = cmdd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        retrieveID = rd.GetInt32(0);
                    }
                }
                mycon.Close();
            }
            return retrieveID;
        }

        public void changeStatus(int user_id, short status)
        {
            #region

//if (!Auth.isAdmin()) { this.setAsAccessDenied(); return; }

            //checking if the old password is correct
            //Dictionary<string, object> filter = new Dictionary<string, object>();
            //filter["id"] = user_id;
            //filter["all_status"] = ""; //used to check all users independent of status

            //DataSet result = User.fetch(filter: filter);
            //if (Model.Base.hasError()) { this.setAsError(); return; }
            //else if (result.Tables.Count == 0) { this.setAsError(); return; }
            //else if (result.Tables[0].Rows.Count == 0) { this.setAsError("User does not exist"); return; }

            //User u = User.ToObject(result.Tables[0]);

            //u.changeStatus(status);
            //u.save();

            //if (Model.Base.hasError()) { this.setAsError(); return; }

            #endregion
              HttpCookie cookie = new HttpCookie("ID");
              cookie = HttpContext.Current.Request.Cookies["ID"];
            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                if (cookie != null) _userID = cookie.Value;
            }

            // _userID  = adminID that create the user
            // user_id = id of the user created

            /* 27/9/2016 */

            int id = CheckAdminPriviledge(user_id);
            if (id == int.Parse(_userID.ToString()))
            {

            }

            /* End */
            else
            {


                int i = 0;
                string sql = @"UPDATE [user] set [status] = '" + status + "' WHERE id= '" + user_id + "'";
                string sql2 = @"UPDATE [profile] set [status] = '" + status + "' WHERE user_id = '" + user_id + "'";
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                {
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        conn.Open();
                        i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            this.setAsSuccess();
                        }
                        else
                        {
                            i = 2;
                        }
                        conn.Close();
                    }
                }
                using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                {
                    using (SqlCommand cmd2 = new SqlCommand(sql2, conn2))
                    {
                        conn2.Open();
                        i = cmd2.ExecuteNonQuery();
                        if (i == 1)
                        {
                            this.setAsSuccess();
                        }
                        else
                        {
                            i = 2;
                        }
                        conn2.Close();
                    }
                }

            }
        }

        public void resetPassword(int user_id, string password)
        {
            //if (!Auth.isAdmin()) { this.setAsAccessDenied(); return; }

            //checking if the old password is correct
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["id"] = user_id;
            filter["all_status"] = ""; //used to check all users independent of status

            DataSet result = User.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return; }
            else if (result.Tables.Count == 0) { this.setAsError(); return; }
            else if (result.Tables[0].Rows.Count == 0) { this.setAsError("User does not exist"); return; }

            User u = User.ToObject(result.Tables[0]);
            u.changePassword(password, Model.Status.ACTIVE);
            u.save();

            if (Model.Base.hasError()) { this.setAsError(); return; }

            this.setAsSuccess();
        }

        public DataSet login(string email, string password)
        {
            //checking if the old password is correct
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter["email"] = email.ToLower().Trim();
            filter["password"] = PasswordGen.generate(password).Trim();

            DataSet result = User.fetch(filter: filter);
            if (Model.Base.hasError()) { this.setAsError(); return null; }
            else if (result.Tables.Count == 0) { this.setAsError(); return null; }
            else if (result.Tables[0].Rows.Count == 0) { this.setAsError("Invalid email or password"); return null; }

            User u = User.ToObject(result.Tables[0]);
            //IDD = u.Id; //store id temporary
            //RoleID = u.RoleId;
           
            //HttpContext.Current.Application["ID"] = u.Id;
            //HttpContext.Current.Application["RoleID"] = u.RoleId;

            HttpContext.Current.Session["ID"] = u.Id.ToString();
            HttpContext.Current.Session["RoleID"] = u.RoleId.ToString();

            HttpCookie cookie = new HttpCookie("ID");
            cookie.Value = u.Id.ToString();
            HttpContext.Current.Response.Cookies.Add(cookie);

            HttpCookie cookie2 = new HttpCookie("RoleID");
            cookie2.Value = u.RoleId.ToString();
            HttpContext.Current.Response.Cookies.Add(cookie2);

            //TempStorage.TheData = u.Id;
            //TempStorage.TheData2 = u.RoleId;

            Temp.SetUser(u);
            Temp.SetUserLogin(result.Tables[0].Rows[0].ItemArray[0].ToString(),
                result.Tables[0].Rows[0].ItemArray[1].ToString(),
                result.Tables[0].Rows[0].ItemArray[2].ToString(), result.Tables[0].Rows[0].ItemArray[3].ToString(),
                result.Tables[0].Rows[0].ItemArray[4].ToString(), result.Tables[0].Rows[0].ItemArray[5].ToString(),
                result.Tables[0].Rows[0].ItemArray[6].ToString(), result.Tables[0].Rows[0].ItemArray[7].ToString(),
                result.Tables[0].Rows[0].ItemArray[8].ToString(), result.Tables[0].Rows[0].ItemArray[9].ToString(),
                result.Tables[0].Rows[0].ItemArray[10].ToString(), result.Tables[0].Rows[0].ItemArray[11].ToString(),
                result.Tables[0].Rows[0].ItemArray[12].ToString(), result.Tables[0].Rows[0].ItemArray[13].ToString(),
                result.Tables[0].Rows[0].ItemArray[14].ToString(), result.Tables[0].Rows[0].ItemArray[15].ToString());

            //fetching related processes and start stage information
            DataSet process_result = Process.loginFetch(u.RoleId);
            if (Model.Base.hasError()) { this.setAsError(); return null; }
            else if (process_result.Tables.Count == 0) { this.setAsError(); return null; }

            //storing into tempdb (TEMPORARY DATA)
            foreach (DataRow item in process_result.Tables[0].Rows)
            {
               
                Temp.SetUserProcess(u.RoleId.ToString(), item.ItemArray[0].ToString(), item.ItemArray[1].ToString(),
                    item.ItemArray[2].ToString(), item.ItemArray[3].ToString(), item.ItemArray[4].ToString(),
                    item.ItemArray[5].ToString(), item.ItemArray[6].ToString());
            }
            foreach (DataRow item in process_result.Tables[1].Rows)
            {

                Temp.SetStartStage(item.ItemArray[0].ToString(), item.ItemArray[1].ToString(),
                    item.ItemArray[2].ToString(),
                    item.ItemArray[3].ToString(), item.ItemArray[4].ToString(), item.ItemArray[5].ToString(),
                    item.ItemArray[6].ToString(), item.ItemArray[7].ToString(), item.ItemArray[8].ToString(),
                    item.ItemArray[9].ToString(), item.ItemArray[10].ToString(), item.ItemArray[11].ToString());
            }
            //merge process data into user DataSet
            result.Tables[0].TableName = "user";
            process_result.Tables[0].TableName = "process";
            process_result.Tables[1].TableName = "start_stage";

            result.Tables.Add(process_result.Tables[0].Copy());
            result.Tables.Add(process_result.Tables[1].Copy());




           // Auth.set(u);
            //Auth.setLoginData(result);

            this.setAsSuccess();
            return result;
        }

        public void logout()
        {
            HttpCookie cookie = new HttpCookie("ID");
            cookie = HttpContext.Current.Request.Cookies["ID"];
            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                if (cookie != null) _userID = cookie.Value;
            }

            AuditLogic logic = new AuditLogic();
            string full = getFullName(_userID.ToString());
            logic.SaveAudit(full, "User successfully logged out", "Logout", SystemInformation.GetIPAddress(),
                SystemInformation.GetMACAddress());

            Auth.clear();
            this.setAsSuccess();
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();

        }

        public DataSet get(Dictionary<string, object> filter, int? offset, int? count)
        {
            this.setAsSuccess();
            return User.fetch(filter: filter, offset: offset, count: count);
        }

        public string getFullName(string userID)
        {
            string _full = String.Empty;
            string sql = @"SELECT concat(firstname, ' ',lastname) as Fullname FROM profile WHERE [user_id]= '" + userID +
                         "'";
            using (SqlConnection connn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            {
                using (SqlCommand cmd = new SqlCommand(sql, connn))
                {
                    connn.Open();
                    SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    if (rd.HasRows)
                    {
                        if (rd.Read())
                        {
                            _full = (string) rd["Fullname"];
                        }
                    }
                }
                connn.Close();
                return _full;
            }
        }
        #endregion
    }
}