﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CaseManager.Logic;
using CaseManager.Util;
using CaseManager.Model;

namespace CaseManager
{

    public partial class changepassword : System.Web.UI.Page
    {
        private HttpCookie cookie = new HttpCookie("ID");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Auth.isActive()) // changed to isloggein
            {
                if (Auth.isAdmin())
                {
                    Response.Redirect("~/Admin/", true);
                }
                
            }
            else
            {
                Response.Redirect("~/", true);
            }
        }

        protected void changepwdBtn_Click(object sender, EventArgs e)
        {

            if (new_password.Text.Trim().Length > 0 && confirm_password.Text.Trim().Length > 0)
            {
                if (new_password.Text.Trim().Length >= 6)
                {
                    if (new_password.Text.Trim().Equals(confirm_password.Text.Trim()))
                    {
                        UserLogic ul = new UserLogic();
                        cookie = HttpContext.Current.Request.Cookies["ID"];
                        if (cookie == null)
                        {
                            Response.Redirect("~/logout.aspx", true);
                        }
                        else
                        {
                            User usersession = Temp.GetUser(cookie.Value);
                            ul.resetPassword(usersession.Id, new_password.Text.Trim());
                            Response.Redirect("~/logout.aspx", true);
                        }
                        
                    }else{
                        status_message.CssClass = "alert alert-danger";
                        alert_message.Text = "Password fields must match";
                    }
                }
                else
                {
                    status_message.CssClass = "alert alert-danger";
                    alert_message.Text = "Password length must be more than or equal to 6 ";
                }
            }
            else
            {
                status_message.CssClass = "alert alert-danger";
            }
        }
    }
}