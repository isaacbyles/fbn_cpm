﻿using HttpServer.HttpModules;
using System;
using HttpServer;
using HttpServer.Sessions;
using HttpServerTest.Model;
using HttpServerTest.Handler;
using System.Data;
using CaseManager.HttpServerTest.Handler;
using Newtonsoft.Json;
using CaseManager.HttpServerTest.Model;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
//using CaseManager.LawPavilionUpdates;

//using CaseManager.LawPavilionUpdates;

namespace CaseManager.HttpServerTest
{
    public class HttpModleCustom : HttpModule
    {
        String json = null;
        byte[] buffer;
        UserProfile profile = null;
        LoginHandle log = new LoginHandle();
        MemorySessionStore mem = new MemorySessionStore();
       // LawPavilionUpdates.Updates updates = new Updates();

        public override bool Process(IHttpRequest request, IHttpResponse response, IHttpSession session)
        {
            mem.ExpireTime = 60;
            mem.Cleanup();
            buffer = System.Text.Encoding.UTF8.GetBytes("");
            //Handles login of each user

            #region 

            if (request.Uri.LocalPath.ToLower().Equals("/login"))
            {

                // string strJson = request.Form[""].Value; // url to fetch json string 

                string ipAddress = request.RemoteEndPoint.Address.ToString();


                String email = request.Form["email"].Value.ToLower();
                String pass = request.Form["password"].Value;


                if (email != null && pass != null)
                {
                    Logger.Logger.writelog("Login with email: " + email);
                    String encryptPass = PasswordGen.generate(pass);

                    DataTable tab = log.userValidity(email);
                    String validEmail = null;
                    String validPass = null;
                    String name = null;
                    int count = 0;
                    foreach (DataRow row in tab.Rows)
                    {
                        validEmail = row["email"].ToString().ToLower();
                        count = Convert.ToInt32(row["loginattempt"]);
                        validPass = row["password"].ToString();
                        name = row["FirstName"].ToString() + " " + row["LastName"].ToString();

                    }
                    if (email.Equals(validEmail) && validEmail != null)
                    {

                        if (count < 3)
                        {
                            if (encryptPass.Equals(validPass))
                            {
                                json = log.login(email, encryptPass, session.Id);
                                profile = log.profile;

                                session["user_id"] = profile.User_id;
                                mem.Save(session);
                                buffer = Encoding.UTF8.GetBytes(json);
                                log.updateAuditTrail(name, "Successful Login ", "Login");
                                log.externalUpdate(0, "valid", email);
                                Logger.Logger.writelog("Login with " + email + " successful");
                            }
                            else
                            {
                                json = "[{\"loginDetails\":[{\"Login_status\" :\"unsuccesful\"}]}]";
                                buffer = Encoding.UTF8.GetBytes(json);
                                count++;
                                String status = null;
                                if (count >= 3)
                                {
                                    count = 3;
                                    status = "blocked";
                                    log.updateAuditTrail(name, "Unsuccessful Login: User Blocked", "Login");
                                    Logger.Logger.writelog("Login from " + email + "has been blocked");
                                }
                                else
                                {
                                    status = "valid";
                                    log.updateAuditTrail(name, "Unsuccessful Login: Wrong Password", "Login");
                                }
                                log.externalUpdate(count, status, email);

                            }
                        }
                        else if (count == 3 || count > 3)
                        {
                            json = "[{\"loginDetails\":[{\"Login_status\" :\"blocked\"}]}]";
                            buffer = Encoding.UTF8.GetBytes(json);
                            log.updateAuditTrail(name,
                                "Unsuccessful Login: User Has been blocked (<b>" + email + "</b>)", "Login");
                            Logger.Logger.writelog("Login from " + email + "has been blocked");
                        }

                    }
                    else
                    {
                        json = "[{\"loginDetails\":[{\"Login_status\" :\"unsuccesful\"}]}]";
                        buffer = Encoding.UTF8.GetBytes(json);
                        log.updateAuditTrail("", "Unsuccessful Login: Wrong Email(<b>" + email + "</b>)", "Login");
                    }
                }
                else
                {
                    json = "[{\"loginDetails\":[{\"Login_status\" :\"unsuccesful\"}]}]";
                    buffer = Encoding.UTF8.GetBytes(json);
                }
            }

            #endregion


            //Handles request to get processes

            #region 

            if (request.Uri.LocalPath.ToLower().Equals("/requests"))
            {
                string session_id = request.Form["session_id"].Value;
                string process_id = request.Form["process_id"].Value;


                if (!process_id.Equals("") || !session_id.Equals(""))
                {
                    session = mem.Load(session_id);
                    if (session != null)
                    {
                        Logger.Logger.writelog("/requests service was called with process_id: " + process_id +
                                               " and session_id:" + session_id);
                        if (session.Id.Equals(session_id))
                        {
                            Logger.Logger.writelog("session successfully loaded in /request service");
                            String user_id = session["user_id"].ToString();
                            HandleRequests requestList = new HandleRequests();
                            Logger.Logger.writelog("Fetching Request with the process_id: " + process_id);
                            String reqs = requestList.getList(user_id, process_id);
                            buffer = Encoding.UTF8.GetBytes(reqs);
                            Logger.Logger.writelog("Encoding Data To Buffer");
                        }
                        else
                        {
                            buffer = Encoding.UTF8.GetBytes("Invalid Session Please Login");
                            Logger.Logger.writelog("Invalid Session Please Login For Process Service");
                        }
                    }
                    else
                    {
                        buffer =
                            Encoding.UTF8.GetBytes(
                                "[{\"loginDetails\":[{\"Login_status\" :\"Session expired\"}]}]");
                    }
                }
                else
                {
                    buffer = Encoding.UTF8.GetBytes("Session cannot be empty");
                }


            }

            #endregion


            #region 

            if (request.Uri.LocalPath.ToLower().Equals("/stage"))
            {
                String session_id = request.Form["session_id"].Value;
                String request_id = request.Form["request_id"].Value;

                Logger.Logger.writelog("/stage service was called with Request_id: " + request_id + " and session_id:" +
                                       session_id);
                if (!request_id.Equals("") || !session_id.Equals(""))
                {
                    session = mem.Load(session_id);
                    if (session != null)
                    {
                        if (session.Id.Equals(session_id))
                        {
                            Logger.Logger.writelog("Session Successfully Loaded For /stage service");
                            String user_id = session["user_id"].ToString();
                            FormValueLabel value = new FormValueLabel();
                            Logger.Logger.writelog("Fetching RequestTrail with the request_id: " + request_id);
                            String reqs = value.valueLabel(request_id, user_id);
                            buffer = Encoding.UTF8.GetBytes(reqs);
                            Logger.Logger.writelog("Encoding Data To Buffer");
                        }
                        else
                        {
                            buffer = Encoding.UTF8.GetBytes("Session Not Valid");
                            Logger.Logger.writelog("Session Not Valid");
                        }
                    }
                    else
                    {
                        buffer =
                            Encoding.UTF8.GetBytes(
                                "[{\"loginDetails\":[{\"Login_status\" :\"Session expired\"}]}]");
                    }
                }

            }

            #endregion


            #region 

            if (request.Uri.LocalPath.ToLower().Equals("/calendar"))
            {
                Logger.Logger.writelog("/calender service was called for external Solicitor");
                String session_id = request.Form["session_id"].Value;

                session = mem.Load(session_id);
                if (session != null)
                {
                    Logger.Logger.writelog("Session Successfully Loaded For /calendar service");
                    String user_id = session["user_id"].ToString();

                    ExternalSolicitorCalender sol = new ExternalSolicitorCalender();
                    Logger.Logger.writelog("Getting Data to populate the the calendar for external solicitor");
                    Logger.Logger.writelog("Serializing data recieved from calendar to son");
                    json = JsonConvert.SerializeObject(sol.getCalendar(user_id), Formatting.Indented);

                    buffer = Encoding.UTF8.GetBytes(json);
                    Logger.Logger.writelog("Encoding Data To Buffer");
                }
                else
                {
                    buffer =
                        Encoding.UTF8.GetBytes(
                            "[{\"loginDetails\":[{\"Login_status\" :\"Session expired\"}]}]");
                }


            }

            #endregion


            #region 

            if (request.Uri.LocalPath.ToLower().Equals("/save"))
            {

                Logger.Logger.writelog("/save service was called for external Solicitor to save data");
                RequestJson requestJson = new RequestJson();
                Logger.Logger.writelog("Getting the request body from the client");
                byte[] formJson = request.GetBody();
                String strJson = Encoding.Default.GetString(formJson);
                Logger.Logger.writelog("Loggin the request body : " + strJson);

                requestJson = JsonConvert.DeserializeObject<RequestJson>(strJson);
                Logger.Logger.writelog("Deserializing from the request Body to Model Data class");
                session = mem.Load(requestJson.session_id);
                if (session != null)
                {
                    Logger.Logger.writelog("Session successfully loadeed for :" + requestJson.session_id);
                    String request_id = requestJson.request_id;
                    String user_id = session["user_id"].ToString();
                    String next_stage_id = requestJson.next_stage_id;
                    String next_user_id = requestJson.next_user_id;
                    String form_id = requestJson.form_id;
                    List<RequestInnerJson> form_data = requestJson.form_Data;
                    Logger.Logger.writelog("Data to be saved in db is request_id:" + request_id + " user_id:" + user_id +
                                           " next_stage_id:" + next_stage_id +
                                           " next_user_id:" + next_user_id + " form_id:" + form_id);
                    var i = 1;
                    foreach (var data in form_data)
                    {

                        Logger.Logger.writelog("Form Data " + i + " with control_id:" + data.control_id + " and Value:" +
                                               data.value + " will be saved");
                        i++;
                    }

                    TreatRequest treat = new TreatRequest(request_id, user_id, next_stage_id, next_user_id, form_id,
                        form_data);

                    bool resp = treat.RequestForm();
                    Dictionary<string, string> d = new Dictionary<string, string>();
                    if (resp == true)
                    {
                        d.Add("statusCode", "201");
                        d.Add("message", "Form successfully saved");
                        Logger.Logger.writelog("Response after saving " + d.Values.ToString());
                    }
                    else
                    {
                        d.Add("statusCode", "500");
                        d.Add("message", "Operation Failed");
                        Logger.Logger.writelog("Response after saving " + d.Values.ToString());
                    }
                     json = JsonConvert.SerializeObject(d, Formatting.Indented); //string json
                    buffer = Encoding.UTF8.GetBytes(json);

                    Logger.Logger.writelog("Encoding Data To Buffer");
                }

                else
                {
                    buffer =
                        Encoding.UTF8.GetBytes(
                            "[{\"loginDetails\":[{\"Login_status\" :\"Session expired\"}]}]");
                }


            }

            #endregion


            #region 

            if (request.UriParts[0].ToLower().Equals("pending"))
            {
                String session_id = request.Form["session_id"].Value;
                session = mem.Load(session_id);
                if (session != null)
                {
                    String user_id = session["user_id"].ToString();
                    LoginPendingRequest pReq = new LoginPendingRequest(user_id);

                    dynamic collectionWrapper = new
                    {

                        PendingRequest = pReq.pendingRequest()

                    };

                    json = JsonConvert.SerializeObject(collectionWrapper, Formatting.Indented);
                    buffer = Encoding.UTF8.GetBytes(json);
                }
                else
                {
                    buffer =
                        Encoding.UTF8.GetBytes(
                            "[{\"loginDetails\":[{\"Login_status\" :\"Session expired\"}]}]");
                }

            }

            #endregion


            #region

            if (request.UriParts[0].ToLower().Equals("logout"))
            {
                String session_id = request.Form["session_id"].Value;
                session = mem.Load(session_id);
                if (session != null)
                {
                    mem.Remove(session_id);
                }
            }

            #endregion




            //LAWPAVILION UPDATES

            #region 

            //request.Uri = new Uri("http://www.lawpavilionplus.com/cases_update/index.php");
            //request.Method = "POST";

            //string dtModified = updates.GetDateModifed(Updates.Supreme); // get table cases

            //string url = Updates.BASE_URL; // get the url 

            //Updates.UpdateData updateData = new Updates.UpdateData();
            //updateData.dt_modified = dtModified;
            //updateData.table = Updates.Supreme;

            //json = JsonConvert.SerializeObject(updateData, Formatting.Indented);
            //buffer = Encoding.UTF8.GetBytes(json);
            //response.ContentType = "application/x-www-form-urlencoded";

            #endregion

            //System.Timers.Timer timer = new System.Timers.Timer();
            //timer.Interval = 5;
            //timer.Elapsed += (send, ex) =>
            //{
            //    string dtModified = updates.GetDateModifed(Updates.Supreme); // get table cases

            //    string url = Updates.BASE_URL; // get the url 

            //    Updates.UpdateData updateData = new Updates.UpdateData();
            //    updateData.dt_modified = dtModified;
            //    updateData.table = Updates.Supreme;

            //    json = JsonConvert.SerializeObject(updateData, Formatting.Indented);
            //    buffer = Encoding.UTF8.GetBytes(json);
               
            //};

           

           
            //LAWPAVILION UPDATES

            response.ContentType = "application/json; charset=utf-8";

            response.ContentLength = buffer.Length;
            response.SendHeaders();
            response.SendBody(buffer);
            json = "";
            //Logger.Logger.writelog("Sends Response Back to the Client");
            //Logger.Logger.writelog("UPDATE STARTING 6......." + " " + buffer);
            //byte[] res = request.GetBody();
            //Logger.Logger.writelog("UPDATE STARTING 7......." + " " + res);
            //String resJson = Encoding.Default.GetString(res);
            //Logger.Logger.writelog("UPDATE STARTING 8......." + " " + resJson);
            //RootObject obj = JsonConvert.DeserializeObject<RootObject>(resJson);
            //Logger.Logger.writelog("UPDATE STARTING 9......." + " " + obj);

            return true;
        }


    }
}