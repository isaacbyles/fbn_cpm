﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CaseManager.HttpServerTest.Logger
{
    public static class Logger
    {
        public static void writelog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory+"\\LogFile.txt",true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim());
                sw.Flush();
                sw.Close();

            }
            catch { }
        }

        public static void writeiplog(String ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\IP_LOG_FILE.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + ex);
                sw.Flush();
                sw.Close();

            }
            catch { }
        }
        public static void writelog(String message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": "+ message);
                sw.Flush();
                sw.Close();

            }
            catch { }
        }


        public static void writelogForUpload(String message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\UploadLog.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + message);
                sw.Flush();
                sw.Close();

            }
            catch { }
        }
    }
}