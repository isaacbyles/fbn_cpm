﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.HttpServerTest.SqlData
{
    public class SqlQuery
    {
        public const String FORM_CONTROL_TYPE = @"SELECT [type] FROM [lpcm].[dbo].[form_control] where id= @control_id";
        public const String TREATREQUEST_REQUEST_FETCH = @"select process_id,[modified_date], stage_id from request where id= @request_id";
        public const String TREATREQUEST_REQUEST_UPDATE = @"UPDATE [request] SET stage_id=@stage_id, current_user_id=@current_user_id, modified_date=@modified_date WHERE id = @id";
        public const String LOGINHANDLE_EXTERNAL_SOLICITOR_LOGIN = @"select * from [user] where email ='@email' and password ='@passss'";
        public const String LOGINHANDLE_STAGES_FETCH = @"select stage.*,process.name as process_name from stage join process on stage.process_id = process.id where role_id = 11";
        public const String LOGINHANDLE_EXTERNAL_SOLICITOR_PROFILE = @"select * from [external_solicitors] where [external_solicitors].email = '@email'";
        public const String LOGINHANDLE_UPDATE_LOGINATTEMPT = @"UPDATE [external_solicitors] SET [LoginAttempt] = @attempts , [account_status] ='@status' WHERE email = '@email'";
        public const String LOGINPENDINGREQUEST_REQUEST_FETCH = @"SELECT * from request where current_user_id =@user_id order by id desc ";
        public const String LOGINPENDINGREQUEST_GET_SENDERS_INFO = @"select [user_id] from request_trail where request_id = @request_id order by id desc";
        public const String LOGINPENDINGREQUEST_GET_SENDERS_NAME =@"select firstname+' '+lastname as 'senders name' from profile where user_id = @senders_id";
        public const String HANDLEREQUEST_GETREQUEST = @"SELECT [id] as request_id,[process_id],[stage_id],(select name from stage where id =stage_id ) as stage_name,[init_user_id],(select firstname+' '+lastname from profile where [profile].[user_id] =init_user_id) as init_name,[current_user_id],(select firstname+' '+lastname from profile where [profile].[user_id] =current_user_id) as current_name,[title],[created_date],[modified_date],[status]FROM request where current_user_id = @user_id or id IN (SELECT[request_id] FROM request_trail where user_id = @user_id) order by request_id desc";
        public const String FORMVALUELABEL_GET_REQUESTTRAIL = @"SELECT [id],[request_id],[stage_id],[user_id] FROM [request_trail] where request_id = @request_id ";
        public const String FORMVALUELABEL_TEST_REQUESTTRAIL = @"select user_id from request_trail where request_id = @request_id union all select current_user_id as user_id from request where id = @request_id";

        public const String FORMVALUELABEL_REQUESTTRAIL_VALUE = @"SELECT (select label from form_control  where id = form_control_id and status = 1) as label,cast([value] AS NVARCHAR(MAX)) as value FROM [process_form_text_data] where request_id = @request_id and request_trail_id = @id and status =(select status from form_control  where id = form_control_id) " +
                            "union " +
                            "SELECT (select label from form_control  where id = form_control_id and status=1) as label,cast([value] AS NVARCHAR(MAX)) as value FROM [process_form_int_data] where request_id = @request_id and request_trail_id = @id and status =(select status from form_control  where id = form_control_id) " +
                            "union " +
                            "SELECT (select label from form_control  where id = form_control_id and status=1) as label,cast([value] AS NVARCHAR(MAX)) as value FROM [process_form_decimal_data] where request_id = @request_id and request_trail_id = @id and status =(select status from form_control  where id = form_control_id) " +
                            "union " +
                            "SELECT (select label from form_control  where id = form_control_id and status=1) as label,cast([value] AS NVARCHAR(MAX)) as value FROM [process_form_date_data] where request_id = @request_id and request_trail_id = @id and status =(select status from form_control  where id = form_control_id) " +
                            "union " +
                            "SELECT (select label from form_control  where id = form_control_id and status=1) as label,cast([value] AS NVARCHAR(MAX)) as value FROM [process_form_char_data] where request_id = @request_id and request_trail_id = @id and status =(select status from form_control  where id = form_control_id) " +
                            "union " +
                            "SELECT 'stage' as label,cast([stage_id] AS NVARCHAR(MAX)) as value FROM [process_form_text_data] where request_id = @request_id and request_trail_id in(SELECT [id] FROM [lpcm].[dbo].[request_trail] where request_id = @request_id) and request_trail_id = @id " +
                            "union " +
                            "SELECT 'Role' as label,cast((select [name] from lpcm.dbo.role where id =(select [role_id] from lpcm.dbo.[user] where id= @user_id) ) AS NVARCHAR(MAX)) as value FROM lpcm.dbo.[role]" +
                            "union " +
                            "SELECT 'Role_id' as label,cast((select [role_id] from lpcm.dbo.[user] where id=@user_id ) AS NVARCHAR(MAX)) as value " +
                            "union " +
                            "SELECT 'Branch' as label,cast((select [name] from lpcm.dbo.branch where id =(select [branch_id] from lpcm.dbo.[user] where id=@user_id) ) AS NVARCHAR(MAX)) as value " +
                            "union " +
                            "select 'branch_id' as label,cast([branch_id] AS NVARCHAR(MAX)) as value from lpcm.dbo.[user] where id=@user_id " +
                            "union " +
                            "SELECT 'Name' as label,cast((select [firstname]+' '+[lastname] collate SQL_Latin1_General_CP1_CI_AS from lpcm.dbo.[profile] where user_id =@user_id ) AS NVARCHAR(MAX)) as value from lpcm.dbo.profile " +
                            "union " +
                            "select 'request_trail_id' as label, cast(@id AS NVARCHAR(MAX) ) as value " +
                            "union " +
                            "select 'user_id' as label, cast(@user_id AS NVARCHAR(MAX)) as value " +
                            "union " +
                            "SELECT label,value FROM (SELECT ROW_NUMBER() OVER (ORDER BY id ASC) AS rownumber,'prev_stage_id' as label, cast(current_id as nvarchar(max)) as value FROM stage_precedence where next_id =(select stage_id from [request_trail] where [id] = @id)) AS foo WHERE rownumber = 1 " +
                            "union " +
                            "SELECT label,value FROM (SELECT ROW_NUMBER() OVER (ORDER BY id ASC) AS rownumber,'next_stage_id' as label, cast(current_id as nvarchar(max)) as value FROM stage_precedence where next_id =(select stage_id from [request_trail] where [id] =@id)) as foo WHERE rownumber = 2 " +
                            "union "+
                            "SELECT  'present_stage' as label, cast(stage.[name] as nvarchar(max))as value FROM [stage] where stage.id = (select request_trail.stage_id from request_trail where request_trail.id =@id) "+
                            "union "+
                            "SELECT  'previous_stage' as label, cast(stage.[name] as nvarchar(max))as value FROM [stage] where stage.id = (SELECT value FROM (SELECT ROW_NUMBER() OVER (ORDER BY id ASC) AS rownumber,'prev_stage_id' as label, cast(current_id as nvarchar(max)) as value FROM stage_precedence where next_id =(select stage_id from [request_trail] where [id] = @id)) as foo WHERE rownumber = 1 ) " +
                            "union "+
                            "SELECT  'next_stage' as label, cast(stage.[name] as nvarchar(max))as value FROM [stage] where stage.id = (SELECT value FROM (SELECT ROW_NUMBER() OVER (ORDER BY id ASC) AS rownumber,'next_stage_id' as label, cast(current_id as nvarchar(max)) as value FROM stage_precedence where next_id =(select stage_id from [request_trail] where [id] = @id)) as foo WHERE rownumber = 2 ) ";
    }
}