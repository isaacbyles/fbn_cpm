﻿using System;

namespace HttpServerTest.Model
{
    class RequestTrailPending
    {
        private String request_id;

        public String Request_id
        {
            get { return request_id; }
            set { request_id = value; }
        }
        private String proccess_id;

        public String Proccess_id
        {
            get { return proccess_id; }
            set { proccess_id = value; }
        }
        private String stage_id;

        public String Stage_id
        {
            get { return stage_id; }
            set { stage_id = value; }
        }
        private String init_user_id;

        public String Init_user_id
        {
            get { return init_user_id; }
            set { init_user_id = value; }
        }
        private String current_user_id;

        public String Current_user_id
        {
            get { return current_user_id; }
            set { current_user_id = value; }
        }
        private String title;

        public String Title
        {
            get { return title; }
            set { title = value; }
        }
        private String status;

        public String Status
        {
            get { return status; }
            set { status = value; }
        }

        public string Created_date
        {
            get { return created_date; }
            set { created_date = value; }
        }

        public string Modified_date
        {
            get { return modified_date; }
            set { modified_date = value; }
        }

        public string Stage_name
        {
            get
            { return stage_name; }

            set
            { stage_name = value; }
        }

        public int Senders_id
        {
            get
            {
                return senders_id;
            }

            set
            {
                senders_id = value;
            }
        }

        public string Senders_name
        {
            get
            {
                return senders_name;
            }

            set
            {
                senders_name = value;
            }
        }

        private int senders_id;
        private string senders_name;

        private string stage_name;
      
        private string created_date;

        private string modified_date;
    }
}
