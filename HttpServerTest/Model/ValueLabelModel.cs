﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HttpServerTest.Model
{
    class ValueLabelModel
    {
        private string label;
        private string value;
        
        public string Label
        {
            get { return label; }
            set { label = value; }
        }

        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
    }
}
