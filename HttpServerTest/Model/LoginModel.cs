﻿
namespace HttpServerTest.Model
{
    
    class LoginModel
    {
        
        private string username;
       
        private string password;
        
        private string id;
        
        private string process_id;
        
        private string role_id;
        
        private string type;
        
        private string name;
        
        private string description;
        
        private string status;
        
        private string process_name;


        
        
        public string Id
        {
            get { return id; }
            set { id = value; }
        }


        public string Process_id
        {
            get { return process_id; }
            set { process_id = value; }
        }


        public string Role_id
        {
            get { return role_id; }
            set { role_id = value; }
        }


        public string Type
        {
            get { return type; }
            set { type = value; }
        }


        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        } 



        public string Process_name
        {
            get { return process_name; }
            set { process_name = value; }
        }


        public string Username
        {
            get { return username; }
            set { username = value; }
        }


        public string Password
        {
            get { return password; }
            set { password = value; }
        }
    }
}
