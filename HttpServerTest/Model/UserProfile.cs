﻿
namespace HttpServerTest.Model
{
    
  class UserProfile
    {
        
        private string user_id;
        
        private string firstname;
        
        private string lastname;
        
        private string middlename;
        
        private string telephone;
        
        private string session_id;

        private string login_status;

        private string role_id;

        public string Session_id
        {
            get { return session_id; }
            set { session_id = value; }
        }

        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        public string Middlename
        {
            get { return middlename; }
            set { middlename = value; }
        }

        public string Lastname
        {
            get { return lastname; }
            set { lastname = value; }
        }

        public string Firstname
        {
            get { return firstname; }
            set { firstname = value; }
        }

        public string User_id
        {
            get { return user_id; }
            set { user_id = value; }
        }

        public string Login_status
        {
            get { return login_status; }
            set { login_status = value; }
        }

        public string Role_id
        {
            get
            {
                return role_id;
            }

            set
            {
                role_id = value;
            }
        }
    }
}
