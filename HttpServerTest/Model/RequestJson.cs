﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.HttpServerTest.Model
{
    public class RequestJson
    {
        public String request_id { get; set; }
        public String role_id { get; set; }
        public String next_stage_id { get; set; }
        public String form_id { get; set; }
        public String session_id { get; set; }
        public String next_user_id { get; set; }
        public List<RequestInnerJson> form_Data { get; set; }
        public String user_id { get; set; }
        public List<attachmentsInnerJson> attachments { get; set; }
        //public byte[] file { get; set; }
        //public string ext { get; set; }

    }

    public class RequestInnerJson
    {
        public string control_type { get; set; }
        public string control_id { get; set; }
        public string value { get; set; }
    }

    /// <summary>
    /// Class : Shapes the file into content and extention.
    /// </summary>
    public class attachmentsInnerJson
    {
        public byte[] filecontent { get; set; }
        public string fileExtention { get; set; }
    }
}