﻿using CaseManager.HttpServerTest.SqlData;
using HttpServerTest.Handler;
using HttpServerTest.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CaseManager.HttpServerTest.Handler
{
    public class LoginPendingRequest
    {
        private string user_id;
        private string DbUrl;
        private DataTable data;

        public LoginPendingRequest(String user_id)
        {
            this.user_id = user_id;
            DbUrl = DBConnectivity.dburl;
            data = new DataTable();
        }

        public object pendingRequest()
        {
            using (SqlConnection con = new SqlConnection(DbUrl))
            {
                con.Open();
                
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = SqlQuery.LOGINPENDINGREQUEST_REQUEST_FETCH.Replace("@user_id", user_id);
                    cmd.CommandType = System.Data.CommandType.Text;

                    using (SqlDataAdapter reader = new SqlDataAdapter(cmd))
                    {
                        reader.Fill(data);
                    }
                }
            }
            return getRequestWithSendersId();
        }

        public object getRequestWithSendersId()
        {
            List<RequestTrailPending> pend = new List<RequestTrailPending>();
            foreach (DataRow row in data.Rows)
            {
                String request_id = row["id"].ToString();
                int senders_id = 0;
                String senders_name = null;

                using (SqlConnection con = new SqlConnection(DbUrl))
                {
                    con.Open();

                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = SqlQuery.LOGINPENDINGREQUEST_GET_SENDERS_INFO.Replace("@request_id", request_id);
                        cmd.CommandType = CommandType.Text;

                        senders_id = Convert.ToInt32(cmd.ExecuteScalar());
                    }
                }

                using (SqlConnection con = new SqlConnection(DbUrl))
                {
                    con.Open();

                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = SqlQuery.LOGINPENDINGREQUEST_GET_SENDERS_NAME.Replace("@senders_id", senders_id.ToString());
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataReader read = cmd.ExecuteReader())
                        {
                            if (read.Read())
                            {
                                senders_name = read["senders name"].ToString();
                            }
                        }
                           
                    }
                }

                pend.Add(new RequestTrailPending
                {
                    Request_id = row["id"].ToString(),
                    Proccess_id = row["process_id"].ToString(),
                    Stage_id = row["stage_id"].ToString(),
                    Init_user_id = row["init_user_id"].ToString(),
                    Current_user_id = row["current_user_id"].ToString(),
                    Senders_id = senders_id,
                    Senders_name = senders_name,
                    Title = row["title"].ToString(),
                    Status = row["status"].ToString(),
                    Created_date = row["created_date"].ToString(),
                    Modified_date = row["modified_date"].ToString()
                });

            }
            return pend;
        }
    }
}