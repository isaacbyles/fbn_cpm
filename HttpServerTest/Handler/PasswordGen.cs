﻿using System.Text;
using System.Security.Cryptography;

namespace HttpServerTest.Handler
{
    public class PasswordGen
    {
        public static string generate(string password)
        {
            byte[] data2 = Encoding.UTF8.GetBytes(password);

            SHA1 sha = new SHA1CryptoServiceProvider();
            byte[] encrypted_arr = sha.ComputeHash(data2);

            StringBuilder s = new StringBuilder();
            foreach (byte b in encrypted_arr)
            {
                s.Append(b.ToString("x2"));
            }
            return s.ToString();
        }

        //private static byte[] GetBytes(string str)
        //{
        //    byte[] bytes = new byte[str.Length * sizeof(char)];
        //    System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        //    return bytes;
        //}

        //private static string GetString(byte[] bytes)
        //{
        //    char[] chars = new char[bytes.Length / sizeof(char)];
        //    System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        //    return new string(chars);
        //}
    }
}