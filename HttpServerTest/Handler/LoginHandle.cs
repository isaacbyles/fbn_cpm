﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Data.SqlClient;
using HttpServerTest.Model;
using CaseManager.Logic;
using System.Data;
using CaseManager.HttpServerTest.SqlData;
using CaseManager.Util;

namespace HttpServerTest.Handler
{
    class LoginHandle
    {
        String json;
        String info;
        public UserProfile profile = new UserProfile();
        string str = DBConnectivity.dburl;
        String role_id = null;

        public string login(String email, String passss, String session_id)
        {
            List<LoginModel> feedback = new List<LoginModel>();
            using (SqlConnection con = new SqlConnection(str))
            {
                con.Open();
                SqlCommand cmd = con.CreateCommand();

                LoginModel details = new LoginModel();

                String query = @"select * from [user] where email ='" + email + "' and password ='" + passss + "'";
                cmd.CommandText = query;

                cmd.CommandType = System.Data.CommandType.Text;

                SqlDataReader read = cmd.ExecuteReader();


                if (read.Read())
                {
                    details.Username = read["email"].ToString().ToLower() ;
                    details.Password = read["password"].ToString();
                    role_id = read["role_id"].ToString();

                    if (details.Username.Equals(email.ToLower()) && details.Password.Equals(passss))
                    {
                        read.Close();
                        cmd.Dispose();
                        using (SqlCommand cmd2 = con.CreateCommand())
                        {
                            cmd2.CommandText = SqlQuery.LOGINHANDLE_STAGES_FETCH;
                            cmd2.CommandType = System.Data.CommandType.Text;
                            using (SqlDataReader read2 = cmd2.ExecuteReader())
                            {
                                while (read2.Read())
                                {

                                    feedback.Add(new LoginModel()
                                    {
                                        Id = read2["id"].ToString(),
                                        Process_id = read2["process_id"].ToString(),
                                        Role_id = read2["role_id"].ToString(),
                                        Type = read2["type"].ToString(),
                                        Name = read2["name"].ToString(),
                                        Description = read2["description"].ToString(),
                                        Status = read2["status"].ToString(),
                                        Process_name = read2["process_name"].ToString(),

                                    });


                                }
                            }
                        }

                        using (SqlCommand cmd3 = con.CreateCommand())
                        {

                            cmd3.CommandText = SqlQuery.LOGINHANDLE_EXTERNAL_SOLICITOR_PROFILE.Replace("@email", details.Username);
                            cmd3.CommandType = System.Data.CommandType.Text;
                            using (SqlDataReader read3 = cmd3.ExecuteReader())
                            {
                                if (read3.Read())
                                {
                                    profile.User_id = read3["id"].ToString();
                                    profile.Firstname = read3["firstname"].ToString();
                                    profile.Lastname = read3["lastname"].ToString();
                                    profile.Telephone = read3["phone"].ToString();
                                    profile.Role_id = role_id;
                                    profile.Session_id = session_id;
                                    profile.Login_status = "Successful";

                                }
                            }
                            //info = JsonConvert.SerializeObject(profile, Formatting.Indented);
                            //info = "[{\"loginDetails\":[" + info + "]},";
                        }

                        //json = JsonConvert.SerializeObject(feedback , Formatting.Indented);
                        //json = "{\"stages\":" + json + "}]";


                    }

                    else if (!details.Username.Equals(email) || !details.Password.Equals(passss))
                    {
                        return json = "";
                    }

                }
                else
                {
                    return json = "";
                }

            }

            dynamic collectionWrapper = new
            {

                LoginDetails = profile,
                Stages = feedback

            };

            return JsonConvert.SerializeObject(collectionWrapper, Formatting.Indented); ;
        }

        public DataTable userValidity(String email)
        {
            bool check = false;
            DataTable tab = new DataTable();
            using (SqlConnection connect = new SqlConnection(str))
            {
                connect.Open();
                using (SqlCommand connectCmd = connect.CreateCommand())
                {
                    connectCmd.CommandText = @"select * from [external_solicitors] where [external_solicitors].email = '" + email + "'";
                    connectCmd.CommandType = System.Data.CommandType.Text;

                    using (SqlDataAdapter reader = new SqlDataAdapter(connectCmd))
                    {
                        reader.Fill(tab);
                    }
                }
            }

            return tab;
        }

        public void updateAuditTrail(String user, String details, String action)
        {
            AuditLogic logic = new AuditLogic();
            logic.SaveAudit(user, details, action, SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
        }

        public void externalUpdate(int attempts, String status, String email)
        {
            using (SqlConnection con = new SqlConnection(str))
            {
                con.Open();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    String Query = SqlQuery.LOGINHANDLE_UPDATE_LOGINATTEMPT.Replace("@attempts", attempts.ToString());
                    Query = Query.Replace("@status",status);
                    Query = Query.Replace("@email", email);
                    cmd.CommandText = Query;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
            }
        }

    }
}
