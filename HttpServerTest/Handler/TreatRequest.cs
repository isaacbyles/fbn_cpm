﻿using CaseManager.HttpServerTest.Model;
using CaseManager.HttpServerTest.SqlData;
using CaseManager.Model;
using CaseManager.Util;
using HttpServerTest.Handler;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;

namespace CaseManager.HttpServerTest.Handler
{

    
    public class TreatRequest
    {
        public TreatRequest()
        {

        }
        
        public TreatRequest(String request_id, String user_id,String next_stage, String next_user, String form_id, List<RequestInnerJson> form_data)
        {
            this.request_id = long.Parse(request_id);
            this.user_id = Convert.ToInt32(user_id);
            this.next_stage_id = Convert.ToInt32(next_stage);
            this.next_user_id = Convert.ToInt32(next_user);
            this.form_id = Convert.ToInt32(form_id);
            this.form_data = form_data;
           // this.role_id = Convert.ToInt32(role_id);

        }

        String dburl = DBConnectivity.dburl;

        /******Data Members*******/
        private long request_id;
        private int user_id;
        private int next_stage_id;
        private int next_user_id;
        //private int role_id;
        private DateTime modified_date;
        private int form_id;
        private int ext_stage_id;
        private List<RequestInnerJson> form_data;


        private long insertRequestTrail()
        {
            DataRow row = getData().Rows[0];
            ext_stage_id = Convert.ToInt32(row["stage_id"]);
            Logger.Logger.writelog("Inserting data into the request trail");
            int time_taken = Convert.ToInt32((DateTime.Now - (DateTime) row["modified_date"]).TotalSeconds);

            RequestTrail rt = new RequestTrail(request_id, ext_stage_id, user_id, time_taken);
            Logger.Logger.writelog("About to save to request trail");
            rt.save();
            Logger.Logger.writelog("Saved data to the request Trail");
            return rt.Id;
        }

        private int updateRequest()
        {
            modified_date = DateTime.Now;

            DataAccess da = new DataAccess();
            Dictionary<String, Parameter> param = new Dictionary<String, Parameter>();

            
            param["@stage_id"] = new Parameter(this.next_stage_id, Parameter.ParameterType.INT);
            param["@current_user_id"] = new Parameter(this.next_user_id, Parameter.ParameterType.INT);
            param["@modified_date"] = new Parameter(this.modified_date, Parameter.ParameterType.DATETIME);
            param["@id"] = new Parameter(this.request_id, Parameter.ParameterType.INT);


            Logger.Logger.writelog("Updating data into the request Table");
            int result = da.ExecuteNonQuery(SqlQuery.TREATREQUEST_REQUEST_UPDATE, param);
            
            String roleId = GetRole(this.next_stage_id);
            int iii = Convert.ToInt32(roleId);
            Logger.Logger.writelog("Getting Role Id " + " " + roleId);

            /**/
            /*UPDATE REQUEST ID  ROLE ID*/

            string sql = @"UPDATE [request] SET user_role = '" + iii + "' WHERE id= '" + this.request_id +
                         "'";
            using (SqlConnection sqlconn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, sqlconn))
            {
                sqlconn.Open();
                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                    Logger.Logger.writelog("Updating request table");
                }
                sqlconn.Close();
            }
           


            return result;
        }

        public bool RequestForm()
        {
            long request_trail_id = insertRequestTrail();
            int requestValue = updateRequest();
             
            RequestForm reqForm = new RequestForm(request_id, request_trail_id,form_id,user_id,ext_stage_id);
            Logger.Logger.writelog("/save sent data to request Form class to save data");
            bool response = saveForm(request_id, request_trail_id, form_id, user_id, ext_stage_id,form_data);
            return response;
        }

        public DataTable getData()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(dburl))
            {
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = SqlQuery.TREATREQUEST_REQUEST_FETCH.Replace("@request_id",request_id.ToString());
                    cmd.CommandType = System.Data.CommandType.Text;

                    using (SqlDataAdapter rd = new SqlDataAdapter(cmd))
                    {
                        rd.Fill(dt);
                    }
                }
            }

            return dt;
        }


        public bool saveForm(long request_id, long request_trail_id, int form_id, int user_id, int stage_id,
            List<RequestInnerJson> form_data_list)
        {

            RequestForm rf = new RequestForm(request_id, request_trail_id, form_id, user_id, stage_id);
            rf.deletePrevious();

            FormData.deleteData(request_trail_id);

            Logger.Logger.writelog("About Saving treated Request Form Data");
            foreach (var form_data in form_data_list)
            {

                using (SqlConnection con = new SqlConnection(dburl))
                {
                    con.Open();
                    Logger.Logger.writelog("Opening database connection for control id :" + form_data.control_id);
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = SqlQuery.FORM_CONTROL_TYPE.Replace("@control_id", form_data.control_id);
                        cmd.CommandType = System.Data.CommandType.Text;

                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {
                            Logger.Logger.writelog("Retrieving control Type for control id");
                            while (rd.Read())
                            {
                                form_data.control_type = rd.GetSqlValue(0).ToString();
                                Logger.Logger.writelog("Control Type:" + form_data.control_type + "Retrieved");
                            }


                        }
                    }
                }
                FormData fd = new FormData(ext_stage_id, form_id, int.Parse(form_data.control_id), request_id,
                    form_data.value, request_trail_id);
                Logger.Logger.writelog("Saving the form Data for the control id with the control type");

                fd.insertData(short.Parse(form_data.control_type));

            }

            return true;

        }

        private string GetRole(int stage_id)
        {
            int role = 0;
            string sql_role = @"select role_id FROM [stage] WHERE id= '" + stage_id + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmdd = new SqlCommand(sql_role, conn))
            {
                conn.Open();
                SqlDataReader rd = cmdd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        role = (int)rd.GetSqlByte(0);
                    }
                }
                conn.Close();
            }
            return role.ToString();
        }

    }
}