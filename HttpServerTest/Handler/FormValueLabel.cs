﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using HttpServerTest.Model;
using Newtonsoft.Json;
using System.Data;
using CaseManager.HttpServerTest.SqlData;
using CaseManager.HttpServerTest.Logger;

namespace HttpServerTest.Handler
{
    class FormValueLabel
    {
        String json = "";
        int id = 1;
        List<ValueLabelModel> value = new List<ValueLabelModel>();
        String str = DBConnectivity.dburl;

        bool check = false;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request_id"></param>
        /// <param name="user_id"></param>
        /// <returns>JSon</returns>
        public String valueLabel(String request_id, String user_id)
        {



            using (SqlConnection con = new SqlConnection(str))
            {

                con.Open();

                Logger.writelog("Database Connection was open for /stage ");
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandText = SqlQuery.FORMVALUELABEL_TEST_REQUESTTRAIL.Replace("@request_id", request_id);
                cmd.CommandType = System.Data.CommandType.Text;
                Logger.writelog("Checking the Database if the request id:"+request_id+ " is on the External Solicitor dashBoard");

                using (SqlDataReader rrr = cmd.ExecuteReader())
                {
                    List<string> valToCheck = new List<string>();
                    while (rrr.Read())
                    {
                        valToCheck.Add(rrr["user_id"].ToString());
                    }

                    check = valToCheck.Any(val => val.Equals(user_id));

                }

                if (check == true)
                {
                    Logger.writelog("The request Was Found On the External Solicitor Dashboard");
                    DataTable data = new DataTable();
                    SqlCommand cmd2 = con.CreateCommand();
                    cmd2.CommandText = SqlQuery.FORMVALUELABEL_GET_REQUESTTRAIL.Replace("@request_id", request_id);
                    cmd2.CommandType = System.Data.CommandType.Text;
                    Logger.writelog("Getting the requestrail from the database for the request_id:"+ request_id);
                    using (SqlDataAdapter rd = new SqlDataAdapter(cmd2))
                    {
                        Logger.writelog("reading the data from the database");

                        rd.Fill(data);
                    }

                    foreach (DataRow row in data.Rows)
                    {
                        value = new List<ValueLabelModel>();


                        // external solicitors response
                        using (SqlCommand cmd3 = con.CreateCommand())
                        {
                            String query = SqlQuery.FORMVALUELABEL_REQUESTTRAIL_VALUE.Replace("@request_id", request_id);
                            query = query.Replace("@id", row["id"].ToString());
                            query = query.Replace("@user_id", row["user_id"].ToString());

                            cmd3.CommandText = query;

                            cmd3.CommandType = System.Data.CommandType.Text;
                            using (SqlDataReader read2 = cmd3.ExecuteReader())
                            {

                                while (read2.Read())
                                {
                                    value.Add(new ValueLabelModel()
                                    {
                                        Value = read2["value"].ToString(),
                                        Label = read2["label"].ToString()
                                    });

                                }
                            }
                        }
                        if (json == "")
                        {
                            json = "\"data" + id + "\":" + JsonConvert.SerializeObject(value.ToDictionary(x => x.Label, y => y.Value), Formatting.Indented);
                            id++;
                        }
                        else
                        {
                            json = json + "\n,\"data" + id + "\":" + JsonConvert.SerializeObject(value.ToDictionary(x => x.Label, y => y.Value), Formatting.Indented);
                            id++;
                        }

                    }
                    Logger.writelog("Building Data");
                    Logger.writelog("Serialize Data to json");

                }
            }

            return "[{" + json + "}]";
        }

    }
}
