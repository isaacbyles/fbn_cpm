﻿using System;
using System.Data.SqlClient;
using HttpServerTest.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
using CaseManager.HttpServerTest.SqlData;
using CaseManager.HttpServerTest;
using CaseManager.HttpServerTest.Logger;


namespace HttpServerTest.Handler
{
    class HandleRequests
    {
        List<RequestTrailCompleted> completedRequest =  new List<RequestTrailCompleted>();
        List<RequestTrailPending> pendingRequest = new List<RequestTrailPending>();
        public String getList(String user_id, String process_id)
        {
            string str = DBConnectivity.dburl;
            using (SqlConnection con = new SqlConnection(str))
            {
                con.Open();
                Logger.writelog("Database Connection Open for /request service to Get List of request");
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = SqlQuery.HANDLEREQUEST_GETREQUEST.Replace("@user_id", user_id);
                    cmd.CommandType = System.Data.CommandType.Text;
                    Logger.writelog("Built A query for Process service to get Requests");

                    using (SqlDataReader read = cmd.ExecuteReader())
                    {
                       Logger.writelog("opening a Sqlreader to read content from database");
                        while (read.Read())
                        {
                            Logger.writelog("Pulled information with requestid :"+ read["request_id"].ToString()+ "from the database");
                            // Gets only completed requests by the external solicitors
                            if (read["process_id"].ToString().Equals(process_id) && !read["current_user_id"].ToString().Equals(user_id))
                            {
                                completedRequest.Add(new RequestTrailCompleted()
                                {
                                    Request_id = read["request_id"].ToString(),
                                    Proccess_id = read["process_id"].ToString(),
                                    Stage_id = read["stage_id"].ToString(),
                                    Stage_name = read["stage_name"].ToString(),
                                    Init_user_id = read["init_user_id"].ToString(),
                                    Init_name = read["init_name"].ToString(),
                                    Current_user_id = read["current_user_id"].ToString(),
                                    Current_name = read["current_name"].ToString(),
                                    Title = read["title"].ToString(),
                                    Status = read["status"].ToString(),
                                    Created_date = read["created_date"].ToString(),
                                    Modified_date = read["modified_date"].ToString(),
                                    
                                }
                                );
                            }

                            //Gets request that are pending
                            if (read["process_id"].ToString().Equals(process_id) && read["current_user_id"].ToString().Equals(user_id))
                            {
                                pendingRequest.Add(new RequestTrailPending()
                                {
                                    Request_id = read["request_id"].ToString(),
                                    Proccess_id = read["process_id"].ToString(),
                                    Stage_id = read["stage_id"].ToString(),
                                    Init_user_id = read["init_user_id"].ToString(),
                                    Current_user_id = read["current_user_id"].ToString(),
                                    Title = read["title"].ToString(),
                                    Status = read["status"].ToString(),
                                    Created_date = read["created_date"].ToString(),
                                    Modified_date = read["modified_date"].ToString(),
                                    Stage_name = read["stage_name"].ToString()
                                }
                                );
                            }


                        }
                        if (!read.Read())
                        {
                            Logger.writelog("No data was read from the database");
                        }
                    }
                }

            }
            dynamic collectionWrapper = new
            {

                CompletedRequest = completedRequest,
                PendingRequest = pendingRequest

            };
            Logger.writelog("Serializing the read data to Json");
            return  JsonConvert.SerializeObject(collectionWrapper, Formatting.Indented);
        }
    }
}
