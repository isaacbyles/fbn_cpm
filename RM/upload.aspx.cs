﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Text;
using CaseManager.Logic;
using CaseManager.Util;
using CaseManager.Model;
using  System.Timers;
using CaseManager.HttpServerTest.Model;
using CaseManager.RM.Helper;
using Newtonsoft.Json;
using Logger = CaseManager.HttpServerTest.Logger.Logger;

namespace CaseManager.RM
{
    public partial class upload : System.Web.UI.Page
    {
        public static string request_id = "";
        public static string route = "";
        public static string roleId = "";
        //public string JsonObject { get; set; }
        //HttpFileCollection SelectedFiles { get; set; }

        public string JsonObject = "";

        protected void Page_Load(object sender, EventArgs e)
        {


            //if (Auth.isActive())  // changed the isloggedin
            //{

            //}
            //else
            //{
            //    Response.Redirect("~/RM/", true);
            //}

    


            if (Request.QueryString.Get("request") != null && Request.QueryString.Get("r") != null)
            {
                request_id = Request.QueryString.Get("request");
                route = Request.QueryString.Get("r");

                Logger.writelogForUpload("Request:" + request_id.ToString());
                Logger.writelogForUpload("route: " + route);
            }
            else
            {
                StatusLabel.ForeColor = Color.Red;
                StatusLabel.Text = "No Request Specified";
                FileUploadControl.Enabled = false;
                //uploadFileBtn.Enabled = false;
            }

        }

        protected void upload_doc_Click(object sender, EventArgs e)
        {
          
            if (FileUploadControl.HasFile)
            {
                Logger.writelogForUpload("Upload status: Has File !");

                try
                {
                    List<AttachmentHelper> SelectedFiles = getFiles();
                    if (FileUploadControl.PostedFile.ContentLength <= 8388608)
                    {
                        Logger.writelogForUpload("Upload status: Content Length  < 10 MB !");

                        ResourceLogic rl = new ResourceLogic();
                        long id = 0;
                        
                        for (int i = 0; i < SelectedFiles.Count; i++)
                        {
                            AttachmentHelper hpf = SelectedFiles[i];

                            id = rl.add((long.Parse(request_id)), hpf.FileName, description.Text,
                                FileUploadControl.PostedFile.ContentType, short.Parse(route));

                            Logger.writelogForUpload("Upload status: Content Length  < 8 MB !" + " " + id.ToString());


                            string filename = Path.GetFileName(hpf.FileName);
                            string extension = filename.Substring(filename.LastIndexOf('.') + 1);


                            // saving of file.   

                            string path = Server.MapPath("~/resources/");



                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            //hpf.SaveAs(Server.MapPath("../resources/") + id.ToString() + "." + extension);


                            #region binary reading.
                            byte[] byteArray = hpf.filecontent;

                            MemoryStream fs = new MemoryStream(byteArray);
                            BinaryReader br = new BinaryReader(fs);
                            byte[] bytes = br.ReadBytes((Int32)fs.Length);

                            //Save the Byte Array as File.
                            //Path.Combine()
                            string filePath = "~/resources/" + id.ToString().Replace(':', '-') + "." + extension;
                            File.WriteAllBytes(Server.MapPath(filePath), bytes);

                            #endregion


                            StatusLabel.Text = "Upload status: File uploaded!";
                            Logger.writelogForUpload("Upload status: File uploaded!");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Upload Document",
                                "alert('File Uploaded'); window.location= '../RM/';", true);





                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Upload Document",
                                         "alert('The file could not be uploaded. Because the size is greater than 10MB '); window.location= '../RM/';", true);

                    }
                    Logger.writelogForUpload("File size end 8MB");
                  
                }
                catch (Exception ex)
                {
                    Logger.writelogForUpload("Catch Exception:   File size > 8MB");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Upload Document",
                                          "alert('The file could not be uploaded. Because the size is greater than 8MB '); window.location= '../RM/';", true);

                  StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.ToString();
                }
            }
        }

        //[WebMethod]
        public List<AttachmentHelper> getFiles()
        {
            List<AttachmentHelper> _files = new List<AttachmentHelper>();

            // call js uploadDocument() function.

            string json = "";

            _files = JsonConvert.DeserializeObject<List<AttachmentHelper>>(json);
            return _files;
        }

        //protected void removeFile(int fileIndex)
        //{

        //    if (FileUploadControl.HasFile)
        //    {
        //   try
        //        {
        //            if (FileUploadControl.PostedFile.ContentLength <= 8388608)
        //            {
        //                Logger.writelogForUpload("Upload status: Content Length  < 10 MB !");

        //                ResourceLogic rl = new ResourceLogic();
        //                long id = 0;
        //                 SelectedFiles = Request.Files;

        //                for (int i = 0; i < SelectedFiles.Count; i++)
        //                {
        //                    HttpPostedFile hpf = SelectedFiles[i];

        //                    id = rl.add((long.Parse(request_id)), hpf.FileName, description.Text,
        //                        FileUploadControl.PostedFile.ContentType, short.Parse(route));

        //                    Logger.writelogForUpload("Upload status: Content Length  < 8 MB !" + " " + id.ToString());


        //                    string filename = Path.GetFileName(hpf.FileName);
        //                    string extension = filename.Substring(filename.LastIndexOf('.') + 1);
        //                    string path = Server.MapPath("../resources/");
        //                    if (!Directory.Exists(path))
        //                    {
        //                        Directory.CreateDirectory(path);
        //                    }
        //                    hpf.SaveAs(Server.MapPath("../resources/") + id.ToString() + "." + extension);
        //                    StatusLabel.Text = "Upload status: File uploaded!";
        //                    Logger.writelogForUpload("Upload status: File uploaded!");
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Upload Document",
        //                        "alert('File Uploaded'); window.location= '../RM/';", true);
        //                }

        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Upload Document",
        //                                 "alert('The file could not be uploaded. Because the size is greater than 10MB '); window.location= '../RM/';", true);

        //            }
        //            Logger.writelogForUpload("File size end 8MB");

        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.writelogForUpload("Catch Exception:   File size > 8MB");
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Upload Document",
        //                                  "alert('The file could not be uploaded. Because the size is greater than 8MB '); window.location= '../RM/';", true);

        //            StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.ToString();
        //        }
        //    }


        //}

    }
}