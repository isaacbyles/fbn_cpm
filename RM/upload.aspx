﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="upload.aspx.cs" Inherits="CaseManager.RM.upload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="cache-control" content="must-revalidate" />
    <meta http-equiv="cache-control" content="proxy-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="pragma" content="no-cache" />

    <link href="../assets/css/bootstrap.min.css" rel="Stylesheet" />
    <link href="../assets/css/metisMenu.min.css" rel="Stylesheet" />
    <link href="../assets/css/timeline.css" rel="Stylesheet" />
    <link href="../assets/css/font-awesome.min.css" rel="Stylesheet" />
    <link href="../assets/css/sb-admin-2.css" rel="Stylesheet" />
    <link href="../assets/css/processtree.css" rel="Stylesheet" />
    <link href="../assets/css/jqtree.css" rel="Stylesheet" />
    <link href="../assets/css/formbuilder-min.css" rel="Stylesheet" />
    <link href="../assets/css/jquery.dataTables.min.css" rel="Stylesheet" />
    
   
    
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="container">
        <div class="row">
            <div class="alert alert-info">
                <h3>Instruction</h3> File extensions allowed are Excel Files, Word Document, PDF, Images
            </div>

            <div>
            <asp:Label runat="server" id="StatusLabel" text="Upload status: " />
            </div>
        </div>
        <div>
            <label>Describe This File</label>
            <asp:TextBox ID="description" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
        <br />
        <div>
            <asp:FileUpload ID="FileUploadControl" CssClass="form-control" runat="server" AllowMultiple="true" onchange="javascript:updateList();"  />
            
            
                 <div class="progress progress-striped active">
                         
                         <div class="progress-bar" style="width:0%; height: 10px"></div>
                     
                     </div>
            
            <br/>
            
            <ul id="bag"> </ul>
            


             <label id="lblData"></label>
            
            <asp:HiddenField runat="server" ID=""/>
        </div>
        <br /> 
        
         <div  runat="server">
        </div>
        <div>
           <%-- <asp:Button ID="uploadFileBtn" CssClass="btn btn-primary" runat="server" 
                Text="Upload File" onclick="uploadFileBtn_Click"  />--%>
            <asp:Button ID="upload_doc" runat="server" Text="Upload Document" OnClick="upload_doc_Click" />
           <div id="FileList"></div> <br />     <br />     
        </div>

       &nbsp;</div>
    </form>
     <script src="../assets/js/jquery-1.11.3.js" type="text/javascript"></script>
   <script type = "text/javascript">
       
       var fileArrays = [];

       // var ul = document.querySelector("#bag");;
       var ul = $("#bag");
       var file_upload = $("#FileUploadControl");
       // var li = document.createElement("li");









       file_upload.change(function (event) {

           var input = event.target;
           var files = input.files;

           while (ul.hasChildNodes) {
               ul.removeChild(ul.firstChild);
           }
           // Read first file
           setup_reader(files, 0);

       });

       // Set up the file reader function.
       function setup_reader(files, i) {
           var file = files[i];
           var name = file.name;
           var reader = new FileReader();


           reader.onprogress = function (e) {
               var percent = e.loaded / e.total * 100;
               $(".progress").children("div").width(percent + '%');

              <%-- console.log('<%= JsonObject %>');--%>
               // console.log(percent);
           }


           reader.onload = function (e) {

               readerLoaded(e, files, i, name);

           };
           reader.readAsBinaryString(file);
           // After reading, read the next file.
       }

       // Gets the Extention from file name. return string.
       //function getExtention(text) {


       //    var dotPostion = text.lastIndexOf(".");
       //    var sliceResult = text.slice(dotPostion + 1, text.length);
       //    return sliceResult;
       //}

       // Read in file lodaded by the javascript file loader function.
       function readerLoaded(e, files, i, name) {



           // get file content  
           var bin = e.target.result;


           var fileName = name;
           // console.log(exts);
           var fileObject = { filecontent: bin, FileName: fileName }

           fileArrays.push(fileObject);

           var fileIndex = fileArrays.indexOf(fileObject);

          
           // do sth with text



           var li = document.createElement("li");

           var li2 = document.createElement("div");

           var hl = document.createElement("div");

           li.id = 'f' + fileIndex;
           li.className = ' glyphicon glyphicon-remove '; // Class name
           li.onclick = dynamicEvent;
           hl.className = 'rmvbtn';

           var text = document.createTextNode(name);
           ul.append(hl);
           hl.append(li2);
           li2.prepend(text);


           li2.append(li);
           // If there's a file left to load
           if (i < files.length - 1) {
               // Load the next file
               setup_reader(files, i + 1);
           }
       }


       function dynamicEvent(event) {

           var rmvElement = $("#" + event.currentTarget.id);
          
           var rmv = rmvElement.parent("div");
           rmv.remove();
           var rmvedId = event.currentTarget.id;

           var temp = rmvedId.split("f");
           var indexNo = temp[1];
           // fileArrays.remove(index_no);
           //console.log(indexNo);
           fileArrays[indexNo] = null;
           // fileArrays.removeNullFromArray();
           // console.log(fileArrays);



       }


       $("#upload_doc").click(function uploadDocument() {

           // file attachemnts array.
           var attachments = [];


           // remove holes(object that are null) in the file array by sorting into attachment. 
           for (var i = 0; i < fileArray.length; i++) {

               if (fileArray[i] != null) {
                   attachments.push(fileArray[i]);
               }

           }


           $("hiddenHolder").innerHTML = JSON.stringify(attachments);
           //  JsonObject 

       });
















       function updateList() {
           
//           var input = document.getElementById('<%= FileUploadControl.ClientID %>');
//          <%-- var output = document.getElementById('<%= ListBox1.ClientID  %>');--%>
//           var listItems = [];
//           var mySel = output;
//           for (var i = 0; i < input.files.length; ++i) {
//
//               //listItems.push('<option value="' +
//                               //// i + '">' + input.files.item(i).name
//                               // + '</option>');
//
//               var opt = document.createElement("Option");
//             <%--//document.getElementById('<%= ListBox1.ClientID  %>').options.add(opt);--%>
//               opt.text = input.files.item(i).name;
//               opt.value = i;
//              
//               mySel.add(opt);

           // }
         
         

       }

       function deletevalue() {
           <%-- var s = 1;
           var index;
            var output = document.getElementById('<%= ListBox1.ClientID  %>');
           if (output.selectedIndex == -1) {
               alert("select from the list");
               return true;
           }
           while (s > 0) {
               
               index = output.selectedIndex;
               
               if (index >= 0) {
                   output.options[index] = null;
                   --i;
               }
               else s = 0;
           }
           return true;--%>
       }
   </script>  
</body>
    
    
</html>
