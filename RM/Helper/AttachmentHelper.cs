﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.RM.Helper
{
    public class AttachmentHelper
    {
        /// <summary>
        /// Class : Shapes the file into content and extention.
        /// </summary>
  
            public byte[] filecontent { get; set; }
            public string FileName { get; set; }
      
    }
}