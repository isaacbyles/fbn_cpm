﻿using PdfSharp.Pdf;
using System;
using System.Text;
using System.Web;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace CaseManager.Components.exports
{
    /// <summary>
    /// Summary description for pdf
    /// </summary>
    public class pdf : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var html = context.Request.Form["data"];
            string img = HttpContext.Current.Server.MapPath("~/assets/images/logo.png");
            StringBuilder strHtmlContent = new StringBuilder();
            //strHtmlContent.Append("<div style='float:left'><img border='0' src='" + img +
            //                         "' width='100' height='50'></div>");
            //strHtmlContent.Append("<div style='float:left'><p>First Bank of Nigeria Ltd. <br/>Head Office Address:  &nbps;&nbps;Samuel Asabia House 35 Marina P.O. Box 5216, Lagos,  &nbps;&nbps;Nigeria.</p></div>");
            strHtmlContent.Append(html);
           // strHtmlContent.Append("<div style='float:left'><img border='0' src='" + img + "' width='150' height='100'></div>");

            PdfDocument pdf = PdfGenerator.GeneratePdf(strHtmlContent.ToString(), PdfSharp.PageSize.LargePost);
            string LPath;
            int range = new Random().Next(1000, 41111111);
            string _path = HttpContext.Current.Server.MapPath("~/App_Data/");
            if (!System.IO.Directory.Exists(_path))
            { // if it doesn't exist, create
                System.IO.Directory.CreateDirectory(_path);
            }
            string Location = HttpContext.Current.Server.MapPath("~/App_Data/" + range + "_gen.pdf");
            int index;
            index = Location.IndexOf("bin");
            if (index > 0)
            {
                LPath = Location.Remove(index, 15);
            }
            else
            {
                LPath = Location;
            }
            
            pdf.Save(LPath);
            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("content-disposition", "attachment;filename=" + LPath);
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.WriteFile(LPath);
            //string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + "/";
            //baseUrl += "resources/temp/" + range + "_gen.pdf";
            //context.Response.Write(baseUrl);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}