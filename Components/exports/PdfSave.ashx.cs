﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PdfSharp.Pdf;
using System.IO;
using System.Text;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace CaseManager.Components.exports
{
    /// <summary>
    /// Summary description for PdfSave
    /// </summary>
    public class PdfSave : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            String inText = context.Request.Form["data"]; // retrieving the Html from the Jquery
            string nameData = context.Request.Form["nameDataValue"]; // retrieving the RefNo from the Jquery
            string title = context.Request.Form["title"];

            string img = HttpContext.Current.Server.MapPath("~/assets/images/logo.png");
            StringBuilder sb = new StringBuilder();
            //sb.Append("<div style='float:left'><img border='0' src='" + img + "' width='150' height='100'></div>");
            //sb.Append(
            //    "<div style='float:left'><p>First Bank of Nigeria Ltd <br/>Head Office Address:  &nbsp;&nbsp; Samuel Asabia House 35 Marina P.O. Box 5216, Lagos, &nbsp;&nbsp;Nigeria.<br/></p></div>");
            sb.Append(inText);
         
            #region

            //var definition = new { Name = "", Html = "" };
            //string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
            //var json = JsonConvert.DeserializeAnonymousType(strJson, definition);
            //var refNo = json.Name; // retrieve name from json
            //var Html = json.Html;   //retrieve html from json

            #endregion

            var repRefNo = nameData.Replace("/", "-");
          
            if (repRefNo == "*** MISSING REF # ***")
            {
                PdfDocument pdf = PdfGenerator.GeneratePdf(sb.ToString(), PdfSharp.PageSize.LargePost);

              //  string _hiddenTitle = context.Request
              // string FileName = DateTime.Now.ToString("ddMMyyyyhhmmss");
                string path = context.Server.MapPath(("~/resources/Export_PDF/Pdf/"));
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string location = AppDomain.CurrentDomain.BaseDirectory + "resources\\Export_PDF\\Pdf\\" + nameData +
                                  ".pdf";


                pdf.Save(location); // save the file into the folder
                string _name = nameData + ".pdf";
                context.Response.ContentType = "application/pdf";
                context.Response.AddHeader("content-disposition", "attachment;filename=\"" + _name + "\"");
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                context.Response.WriteFile(location);
            }
            else
            {
                PdfDocument pdf = PdfGenerator.GeneratePdf(sb.ToString(), PdfSharp.PageSize.LargePost);
                 string path = context.Server.MapPath(("~/resources/Export_PDF/Pdf/"));
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string Location = AppDomain.CurrentDomain.BaseDirectory + "resources\\Export_PDF\\Pdf\\" + nameData +
                                  ".pdf";

                string _name = nameData + ".pdf";
                pdf.Save(Location); // save the file into the folder
                context.Response.ContentType = "application/pdf";
                context.Response.AddHeader("content-disposition", "attachment;filename=\"" + _name + "\"");
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                context.Response.WriteFile(Location);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}