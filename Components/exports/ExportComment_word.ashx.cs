﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using CaseManager.Model;
using CaseManager.Util;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using NotesFor.HtmlToOpenXml;
using System.Diagnostics;
using System.Threading;

namespace CaseManager.Components.exports
{
    /// <summary>
    /// Summary description for ExportComment_word
    /// </summary>
    public class ExportComment_word : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            HttpServerTest.Logger.Logger.writelog("Here Export");

            String inText = context.Request.Form["data"];  // retrieving the Html from the Jquery

            HttpServerTest.Logger.Logger.writelog(inText);
          
            string nameData = context.Request.Form["commentTitle"]; // retrieving the RefNo from the Jquery
            HttpServerTest.Logger.Logger.writelog(nameData);
            var repRefNo = nameData.Replace(":", "-");
            HttpServerTest.Logger.Logger.writelog(nameData);
            context.Response.Buffer = true;
            string FileName = DateTime.Now.ToString("ddMMyyyyhhmmss");
            string path = context.Server.MapPath(("~/resources/Comments/Word/"));

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string img = HttpContext.Current.Server.MapPath("~/assets/images/logo.png");
            StringBuilder strHtmlContent = new StringBuilder();
            strHtmlContent.Append(
                "<html><head><title></title></head><body><div style='float:left'><img border='0' src='" +
                img +
                "' width='100' height='50'/></div>");
            strHtmlContent.Append(
                "<div style='float:left'><p>First Bank of Nigeria Ltd <br/>Head Office Address:  &nbsp;&nbsp; Samuel Asabia House 35 Marina P.O. Box 5216, Lagos, &nbsp;&nbsp;Nigeria.<br/></p></div>");
            strHtmlContent.Append(inText);
         
            strHtmlContent.Append("</body></html>");
            HttpServerTest.Logger.Logger.writelog(strHtmlContent.ToString());
            context.Response.ContentType = "text/html";
            MainDocumentPart mainPart = null;
            using (MemoryStream generatedDocument = new MemoryStream())
            {
                using (
                    WordprocessingDocument package = WordprocessingDocument.Create(generatedDocument,
                        WordprocessingDocumentType.Document))
                {
                    mainPart = package.MainDocumentPart;
                    if (mainPart == null)
                    {
                        mainPart = package.AddMainDocumentPart();
                        new Document(new Body()).Save(mainPart);
                    }

                    HtmlConverter converter = new HtmlConverter(mainPart);
                    converter.ParseHtml(strHtmlContent.ToString());

                    mainPart.Document.Save();

                }
                bool started = false;
             
                byte[] bytesInStream = generatedDocument.ToArray(); // simpler way of converting to array
                generatedDocument.Close();

                string d = repRefNo + ".doc";
                context.Response.Clear();
                context.Response.ContentType = "application/msword";
                context.Response.AddHeader("content-disposition", "attachment;filename=\"" + d + "\"");
                context.Response.BinaryWrite(bytesInStream);
                context.Response.End();

            }


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}