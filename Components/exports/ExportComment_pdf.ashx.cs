﻿using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace CaseManager.Components.exports
{
    /// <summary>
    /// Summary description for ExportComment_pdf
    /// </summary>
    public class ExportComment_pdf : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            String inText = context.Request.Form["data"];
            string nameData = context.Request.Form["commentTitle"];

            var repRefNo = nameData.Replace(":", "-");
            string img = HttpContext.Current.Server.MapPath("~/assets/images/logo.png");
            StringBuilder sb = new StringBuilder();
            sb.Append("<div style='float:left'><img border='0' src='" + img + "' width='150' height='100'></div>");
            sb.Append(
                "<div style='float:left'><p>First Bank of Nigeria Ltd <br/>Head Office Address:  &nbsp;&nbsp; Samuel Asabia House 35 Marina P.O. Box 5216, Lagos, &nbps;&nbps;Nigeria.<br/></p></div>");
            sb.Append(inText);
           
            PdfSharp.Pdf.PdfDocument pdf = PdfGenerator.GeneratePdf(sb.ToString(), PdfSharp.PageSize.LargePost);
            string FileName = repRefNo + "-" + DateTime.Now.ToString("ddMMyyyyhhmmss"); // use date as name
            
            string path = HttpContext.Current.Server.MapPath(("~/resources/Comments/Pdf/"));
           
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
           
            string Location = path + repRefNo +".pdf"; //path + the filename
            string _name = repRefNo + ".pdf";
            pdf.Save(Location); // save the file into the folder
            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("content-disposition", "attachment;filename=\"" + _name + "\"");
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.WriteFile(Location);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}