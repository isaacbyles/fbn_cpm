﻿using System.Web;

namespace CaseManager.Components.exports
{
    /// <summary>
    /// Summary description for excel
    /// </summary>
    public class excel : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            var html = context.Request.Form["html"];
            context.Response.AppendHeader("content-disposition", "attachment;filename=ExportedHtml.xls");

            context.Response.Charset = "";

            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

            context.Response.ContentType = "application/vnd.ms-excel";

            context.Response.Write(html);

            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}