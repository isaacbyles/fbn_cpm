﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CaseManager.Components.exports
{
    /// <summary>
    /// Summary description for AuditExcel
    /// </summary>
    public class AuditExcel : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var html = context.Request.Form["data"];
           // var date = context.Request.Form["nameDataValue"];
            string date = DateTime.Now.ToString("ddMMyyyyhhmmss");
            context.Response.ClearContent();
            context.Response.Buffer = true;
            context.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "AuditTrail.xls"));
            context.Response.ContentType = "application/ms-excel";
            DataTable dt = GetAll();
            string str = string.Empty;
            foreach (DataColumn dtcol in dt.Columns)
            {
                context.Response.Write(str + dtcol.ColumnName);
                str = "\t";
            }
            context.Response.Write("\n");
            foreach (DataRow dr in dt.Rows)
            {
                str = "";
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    context.Response.Write(str + Convert.ToString(dr[j]));
                    str = "\t";
                }
                context.Response.Write("\n");
            }
            context.Response.End();
        }

        public DataTable GetAll()
        {
            DataTable dss = new DataTable();
            SqlDataAdapter rd = null;
            string query = @"select * from [lpcm].[dbo].[tbl_audit]";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(dss);
            }

            return dss;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}