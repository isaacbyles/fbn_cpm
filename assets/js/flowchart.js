﻿var _w = {};
var _t = function () {
    var tobj = {
        end_point: null,
        start_point: null,
        line: null
    }
    return tobj;
}





var rects = {};
var draw = null;
var precedence = [], buffer = [];
var connectionLines = {};
var linesBuffer = {};
var Po = function () {
    this.from = null;
    this.to = null;
}
var XPoint = function () {
    this.x = 0;
    this.y = 0;
};
var _p1 = new XPoint();
var _p2 = new XPoint();

var nodeArr = [];
var nodeGrpArr = [];
var roles = ["Admin", "HOD", "RM", "AM", "FM"];
var roleArr = []; var w = 500, h = 40, padding = 50;

function initDesigner() {
    draw = SVG('drawing');
    for (var x = 0; x < roles.length; x++) {
        var role = draw.rect(900, 80).center(w, (h * x * 3) + padding).fill('#AEB6AA').stroke('#0fa').opacity(0.3);
        drawBubbles(Math.floor(w / 2) - padding, (h * x * 3) + padding);
        draw.plain(roles[x]).center(150, (h * x * 3) + padding);
        roleArr.push(role);
    }

    for (var i = 0; i < 1; i++) {
        //    var rect = draw.circle(60).center(30, 60 * (i + 1));
        //    rect.draggable();
        //    //rect.node.onclick = function(e){console.log(e)};
        //    rect.node.onmouseover = function (e) {
        //        rects[e.target.id].style({ fill: "#f06", cursor: "move" });
        //    };
        //    rect.node.onmouseout = function (e) {
        //        rects[e.target.id].style({ fill: "#000", cursor: "default" });
        //    };
        //    rect.on('dragmove.namespace', function (e) {
        //        //console.log(e);
        //        //clearIdleLines();
        //        if (buffer.length == 1) {
        //            _p1 = new XPoint();
        //            _p2 = new XPoint();
        //            _p1.x = rects[buffer[0]].node.cx.baseVal.value;
        //            _p1.y = rects[buffer[0]].node.cy.baseVal.value;

        //            _p2.x = rects[e.target.id].node.cx.baseVal.value;
        //            _p2.y = rects[e.target.id].node.cy.baseVal.value;

        //            if (e.target.id in _w) {
        //                updateLines(e.target.id, _p2.x, _p2.y);
        //            } else {
        //                var line = drawline(draw, _p1, _p2);
        //                _w[e.target.id] = [];
        //                _w[buffer[0]] = [];
        //                var np = new _t();
        //                var np2 = new _t();
        //                np.line = line;
        //                np.end_point = _p1;
        //                np.start_point = _p2;
        //                _w[e.target.id].push(np);
        //                np2.line = line;
        //                np2.end_point = _p2;
        //                np2.start_point = _p1;
        //                _w[buffer[0]].push(np2);
        //            }
        //        }
        //    });
        //    rect.node.onclick = function (e) {
        //        console.log(rects[e.target.id].node.cx.baseVal.value);
        //        console.log(rects[e.target.id].node.cy.baseVal.value);

        //        if (buffer.indexOf(e.target.id) < 0) {
        //            buffer.push(e.target.id);
        //            rects[e.target.id].fill('#f06');
        //            if (buffer.length >= 2) {
        //                var p = new Po();
        //                _p2.x = rects[e.target.id].node.cx.baseVal.value;
        //                _p2.y = rects[e.target.id].node.cy.baseVal.value;
        //                p.from = buffer[0];
        //                p.to = buffer[1];
        //                buffer = [];
        //                precedence.push(p);
        //                if (p.from + "_" + p.to in connectionLines) {
        //                    var l = connectionLines[p.from + "_" + p.to];
        //                    l.plot(_p1.x, _p1.y, _p2.x, _p2.y);
        //                    console.log('drawn');
        //                }
        //                //connectionLines[p.from+"_"+p.to] = drawline(draw,_p1, _p2);
        //                removeFocus();
        //                console.log(precedence);
        //                _p1 = new XPoint();
        //                _p2 = new XPoint();
        //            }
        //        }
        //        _p1.x = rects[e.target.id].node.cx.baseVal.value;
        //        _p1.y = rects[e.target.id].node.cy.baseVal.value;
        //    };
        //    rects[rect.node.id] = rect;
    }

    draw.node.ondblclick = function (e) {
        resetSelections();
    }
}
function resetSelections() {
    for (var s in rects) {
        rects[s].fill('#000');
    }
    buffer = [];
    precedence = [];
}
function removeFocus() {
    for (var s in rects) {
        rects[s].fill('#000');
    }
}
function clearIdleLines() {
    var d = null;
    var svg = document.querySelector('svg');
    for (var l in linesBuffer) {
        //linesBuffer[l].clear();
        // d = document.getElementById(l);
        document.body.removeChild(linesBuffer[l].target);
     
    }
}
function drawBubbles(p1, p2) {
    var g1 = null
    for (var t = 0; t < 5; t++) {
        g1 = draw.group().translate(p1 + (t * 79), p2 - 30);
        g1.node.onclick = function (e) {
            buffer.push(e); 
            if (buffer.length == 2) {
                _p1.x = buffer[0].x;
                _p1.y = buffer[0].y;

                _p2.x = buffer[1].x;
                _p2.y = buffer[1].y;
                drawline(draw, _p1, _p2);
                buffer = [];
                _p1 = new XPoint();
                _p2 = new XPoint();
            }
        };
        //g1.node.style({ fill: "#f06", cursor: "move" });
        nodeArr.push(g1.circle(60).fill("#FF5252").style({ cursor: 'move' }).draggable());
        nodeGrpArr.push(g1);
    }

}
function updateLines(id, x, y) {
    if (id in _w) {
        for (var i = 0; i < _w[id].length; i++) {
            _w[id][i].line.plot(_w[id][i].end_point.x, _w[id][i].end_point.y, x, y);
            _w[id][i].start_point.x = x;
            _w[id][i].start_point.y = y;
        }
    }
}
function drawline(draw, origin, destination) {
    var dy = destination.y - origin.y;
    var dx = destination.x - origin.x;
    var delta = 0;//   Math.floor(Math.sqrt((dy * dy) - (dx * dx)));

    return draw.line(origin.x - delta, origin.y - delta, destination.x - delta, destination.y - delta).stroke({ width: 2 }).attr('fill', '#f06'); ; //.markerEnd(marker);
}