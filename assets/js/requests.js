﻿$(document).ready(function() {
    $("#admin_allrequests-list").dataTable().fnDestroy();
    $('#getrequest_sent').on('click', function() {
     
        PageHandler.Handles._setDisplayWindow(PageHandler.Global.user_page);
        var link = "../Components/get_request.htm";

        PageHandler.Handles.load(PageHandler.Global.adminPane, link, function(d) {
            $('#page-title').html("ALL REQUESTS ");

            var container = $("#admin_allrequests-list > tbody");

            UserHandler.d.AdmingetAllRequests(function(response) {

                if (response.length) {
                    // alert(response);
                    $.each(response, function(i, v2){
                   // response.forEach(function(v2, i) {
                        container.append(T.templates.renderAllAdminRequests(v2, i + 1));

                    });

                    UserHandler.bindToDom();
                    PageHandler.BindToDocument();
                    $('#admin_allrequests-list').DataTable();
                } else {
                    alert("No Requests Found");
                }
            });

            PageHandler.BindToDocument();
            UserHandler.bindToDom();


        });

    });
});