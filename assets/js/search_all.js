﻿$(document).ready(function() {


    $("#supremeLabel").click(function () {
        var heading_row = "<div id='report_heading_row_search' class='row' style='margin-top:5px; margin-bottom:5px;margin-left:-25px; margin-right:-25px;'></div>";
        var content_row = "<div id='report_content_row' class='row'></div>";
        $("#table-lawreports").remove();
        $("#table-lawreports").remove();
        $("#panel-lawreports").remove();
        $('#report_content_row').remove();
        $('#report_heading_row').remove();

        $("#previous-page").remove();

        $('stk[id=stk-2]').hide();
        $('lpelr[id=lpelr-2]').show();
        $("cms").hide();


        $("#report_navigation_row").remove();
        $("#report_heading_row").remove();
        $("#report_content_row").remove();

        $("#lpelr-wrapper").append(heading_row);

        $("#lpelr-wrapper").append(content_row);
        $("#report_content_row").load("../Lawpavilion/Components/searchresult.htm");
        $('#prev').hide();
        $('#next').hide();
        var url = "/LawPavilion/API/SearchIndex.ashx";

        var supremeCourt = 0; var appealCourt = 0; var fhc = 0; var tat = 0; var nic = 0; var state = 0; var lawsSections = 0; var subsidiary = 0; var civil_procedure_rule = 0; var str_show = "";
        var searchText = $('#input-search').val();

        if (searchText != "") {
            var jsonData = { 'search': searchText }
            jsonData = JSON.stringify(jsonData);

            $.ajax({
                url: url,
                type: 'POST',
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    var items = response;
                    if (items.length === 0) {
                        str_show = "<div class='alert alert-warning' style='margin-bottom: 0px;'><strong> No Results for found</strong></div>";
                        $('#report_heading_row_search').show().html(str_show);
                        $('#prev').hide();
                        $('#nextbtn').hide();
                        $('#sidehide').hide();
                        $('#sidehide').remove();
                    }
                    var fragment = "";
                    response.sort(function(a, b) {
                        return a.rank - b.rank;
                    });


                    $.each(items, function(index, val) {

                        
                        str_show = "<div class='alert alert-warning' style='margin-bottom: 0px;'><strong> Results for : " + searchText + "</strong>   | <total> Abouts " + val.totalHits + " results </total></div>";
                        $('#report_heading_row_search').show().html(str_show);

                        var mYear = val.mYear; // year
                        var mTitle = val.mTitle; //title
                        var mNumber = val.mNumber; //suitno
                        var mContent = val.mContent; // full report
                        var rank = val.rank; // rank
                        var summary = val.summary; // summary
                        var tableName = val.tablename; // table name
                        var pk = val.identifier; //primary Key
                        var caseTitle2 = val.casetitle2;
                        var law = "";
                        var year = new Date(mYear);
                        var convertToYear = year.getFullYear();

                        if (tableName === "cases" || tableName === "analysis") {
                            law = " <strong id='fade' style='color: #EAC357;'> Supreme Court </strong>";
                      
                        fragment += "<div id='push' class='list-group'> " +
                                "<a id='GetData' href='#' onclick='GetSummary(this)' data-url='../LawPavilion/API/FullReport.ashx?suitno=" + mNumber + "&table=" + tableName + "&pk=" + pk + "' class='list-group-item' style='color:blue; font-weight:bold;'> " +
                                "<h6 class=''>" + caseTitle2 + " |  " + mNumber + "  |  " + "[" + convertToYear + " - " + law + "]" + " </h6>  " +
                                "</a> <br/>" +
                                "<p>" + "<strong style=color:green;>[" + searchText + "]</strong>  " + summary + "</p> <hr/> " +
                                "</div>";
                        } else {
                            alert("supreme result is not available");
                        }

                    });
                    $('#prev').show();
                    $('#nextbtn').show();
                    $("#panel-searchreports").show();
                    $("#panel-searchreports").html(fragment);


                },
                error: function(error) {
                    console.log(error);
                }
            });
        } else {
            alert('Please enter at least 3 characters to search for');
        }
    });

});
