﻿var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();
$('#email').keyup(function () {
    delay(function () {
        var email = $('#email').val();
        //var searchText = $('#input-search').val();
        var jsonData = { 'email': email }
        jsonData = JSON.stringify(jsonData);


        $.ajax({
            url: "../API/ValidationAPI.ashx",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: jsonData,
            success: function (data) {
                if (data === "Exist") {
                    
                    $("#email").css("border", "solid 1px red");
                    alert("Email Already Exist");
                }else {

                    $("#email").css('border', 'solid 1px green');
                }
            },
            error: function (resp) {

            }
        });


    }, 5000);
});

$('#create_external').on('click', function (e) {

    var firm = $('#firm').val();
    var address = $('#address').val();
    var tel = $('#telephone').val();
    var email = $('#email').val();
    var website = $('#website').val();
    var fax = $('#fax').val();
    var phone = $('#phone').val();
    var password = $('#password').val();
    

    if (firm != "" && address != "" && email != "" ) {

        var postData = { 'FIRM': firm, 'EMAIL': email, 'TEL': tel, 'ADDRESS': address, 'PASSWORD': password, 'PHONE': phone, 'FAX': fax, 'WEBSITE': website }
        postData = JSON.stringify(postData);
       
        $.ajax({

            url: "../API/ExternalSolicitor.ashx?rand="+randomString()+"&a=" + "add",
            type: "POST",
            data: postData,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                
                var parsed = $.parseJSON(JSON.stringify(data));
                $.each(parsed, function (i, jsondata) {
                    if (jsondata.Result === "Save") {
                        alert("Successfully Saved !!!");
                        $('#myModal').modal('toggle');
                        location.reload(true);
                    } else {
                        alert("Error Occurred !!! Please try  again later");
                    }
                });
            },
            error: function (resp) {
              
            }


        });

    }

    else {
        alert("Some field are empty !!!");
    }

    
});



