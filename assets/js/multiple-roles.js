﻿var url_sec = "../API/SecRole.ashx";

function removeActive() {
    $(".list-group-item").removeClass("active");
    $("#add-sec-role, #remove-sec-role").data("url", "");
}
function selectAddRole(e) {
    removeActive();
    $(e).addClass("active");
    $("#add-sec-role").data("url", $(e).data("user") + "/add/" + $(e).data("roleid"));
}


function selectRemoveRole(e) {
    removeActive();
    $(e).addClass("active");
    $("#remove-sec-role").data("url", $(e).data("user") + "/remove/" + $(e).data("roleid"));
}

function addRole(e) {
    //alert($(e).data("url"));
    if ($(e).data("url")==="undefined") {
    return;
    } 
    $.ajax({
        type: "POST",
        url: url_sec + "/" + $(e).data("url"),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            parsed = $.parseJSON(JSON.stringify(data));
            if (parsed.status == "1") {
                alert("Secondary Role added successfully!");
                //move available to assigned
                $("#assigned-role").append($(".list-group-item.available.active").removeClass("available").addClass("assigned").attr("onclick","selectRemoveRole(this)"));
                removeActive();
            } else {
                alert("Secondary Role could not be added!");
            }

        },
        error: function (XHR, errStatus, errorThrown) {
            var err = JSON.parse(XHR.responseText);
            errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}

function removeRole(e) {
    if ($(e).data("url") === "undefined") {
        return;
    }
    $.ajax({
        type: "POST",
        url: url_sec + "/" + $(e).data("url"),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            parsed = $.parseJSON(JSON.stringify(data));
            if (parsed.status == "1") {
                alert("Secondary Role removed successfully!");
                //move available to assigned
                $("#available-role").append($(".list-group-item.assigned.active").removeClass("assigned").addClass("available").attr("onclick","selectAddRole(this)"));
                removeActive();
            } else {
                alert("Secondary Role could not be removed!");
            }

        },
        error: function (XHR, errStatus, errorThrown) {
            var err = JSON.parse(XHR.responseText);
            errorMessage = err.Message;
            alert(errorMessage);
        }
    });
}


function getSec(e) {

    var userid = $(e).data("id");
    //alert($(e).data("name"));
    $("#sec-title").html("MANAGE SECONDARY ROLES: " + $(e).data("name").toUpperCase() + " (" + $(e).data("role").toUpperCase() + ")");
    $.ajax({
        type: "POST",
        url: url_sec + "/" + userid,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var strBuffer = "";
            var parsed = $.parseJSON(JSON.stringify(data.available));
            $("#available-role").children().remove();

            $.each(parsed, function (i, jsondata) {
                strBuffer = "<a href='#' class='list-group-item available' data-sectype='available' data-roleid='" + jsondata.id + "' data-user='" + userid + "' onclick='selectAddRole(this)'>" + (jsondata.name).toUpperCase() + "</a>";
                $("#available-role").append(strBuffer);
            });
            parsed = $.parseJSON(JSON.stringify(data.assigned));
            $("#assigned-role").children().remove();
            $.each(parsed, function (i, jsondata) {
                strBuffer = "<a href='#' class='list-group-item assigned' data-sectype='assigned' data-roleid='" + jsondata.id + "' data-user='" + userid + "' onclick='selectRemoveRole(this)'>" + (jsondata.name).toUpperCase() + "</a>";
                $("#assigned-role").append(strBuffer);
            });

            $("#secRoleModal").modal("show");
        },
        error: function (XHR, errStatus, errorThrown) {
            var err = JSON.parse(XHR.responseText);
            errorMessage = err.Message;
            alert(errorMessage);
        }
    });

}