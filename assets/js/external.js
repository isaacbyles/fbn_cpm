﻿
$(document).ready(function() {


    GetExternalSolicitor();


    $("#update_external").on('click', function() {

        var id = $("#ext_id").val();
       
        var email = $("#ext_email").val();
        var name = $("#ext_name").val();
        var tel = $("#ext_telephone").val();
        var add = $("#ext_address").val();


        var jsonData = { 'id': id, 'EMAIL': email, 'FIRM': name, 'TEL': tel, 'ADDRESS': add };
        jsonData = JSON.stringify(jsonData);
 

        $.ajax({
            url: "../API/ExternalSolicitor.ashx?rand=" + randomString() + "&a=" + "edit",
            type: "POST",
            data: jsonData,
            success: function(resp) {

                var parsed = $.parseJSON(JSON.stringify(resp));
                $.each(parsed, function(i, jsondata) {
                    if (jsondata.Name === "success") {
                        alert("Edit Successful");
                        $('#myModa').modal('toggle');
                        //window.location.href = window.location.href;
                        //history.go(0);
                        location.reload(true);
                    } else {
                        alert("Error Occurred");
                    }
                });
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert("Error");
            }
        });

    });


    $("#external_refresh").on('click', function() {
    });


    $("#resetMyPassword").on('click', function () {

        var id = $("#ext_idd").val();
        var new_password = $("#new_password").val();
        var jsonData = { 'id': id, 'PASSWORD': new_password };
        jsonData = JSON.stringify(jsonData);
      

        $.ajax({
            url: "../API/ExternalSolicitor.ashx?rand="+randomString()+"&a=" + "reset",
            type: "POST",
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (resp) {
               
                var parsed = $.parseJSON(JSON.stringify(resp));
                $.each(parsed, function (i, jsondata) {
                    if (jsondata.Name === "Reset") {
                        alert("Reset Successful");
                        location.reload(true);
                    } else {
                        alert("Error Occurred");
                    }
                });
            },
            error: function (resp) {
               
            }

        });
    });
});

function GetExternalSolicitor() {
    $.ajax({
        url: "../API/ExternalSolicitor.ashx?rand="+randomString()+"&a=" + "getall",
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            var parsed = $.parseJSON(JSON.stringify(data));

            var result = parsed;

            var strBuffer = "";
            $.each(result, function (i, jsondata) {
                strBuffer = "<tr>" +
                         "<td>" + (i + 1) + "</td>" +
                         "<td>" + jsondata.FIRM + "</td>" +
                         "<td>" + jsondata.ADDRESS + "</td>" +
                         "<td>" + jsondata.TEL + "</td>" +
                         "<td>" + jsondata.EMAIL + "</td>" +
                         "<td>" + jsondata.WEBSITE + "</td>" +
                         "<td>" + jsondata.FAX + "</td>" +
                         "<td>" + jsondata.PHONE + "</td>" +
                         "<td><button onclick='updaterecords(this)' data-toggle='modal' data-target='#myModa' type='button' id='" + jsondata.id + "/" + jsondata.FIRM + "/" + jsondata.ADDRESS + "/" + jsondata.EMAIL + "/" + jsondata.TEL + "' class='btn btn-sm btn-info'>Edit</button> <button type=button class='btn btn-sm btn-danger' onclick=deleterecords(this) id='" + jsondata.id + "'> Delete</button> <button type=button class='btn btn-sm btn-info' data-toggle='modal' data-target='#resetModal' onclick=resetExternalPassword(this) id='" + jsondata.id + "'> Reset Password</button></td>" +
                    "</tr>";

                $("#external_List").append(strBuffer);
            });
        },
        error: function (resp) {
            alert("error");
        }
    });
}

function updaterecords(e) {
    
    var tag = $(e).attr("id");

    var id = tag.split("/");
    var firm = tag.split("/");
    var address = tag.split("/");
    var email = tag.split("/");
    var telephone = tag.split("/");
    

    $('#edit #ext_id').val(id[0]);
    $('#edit #ext_name').val(firm[1]);
    $('#edit #ext_address').val(address[2]);
    $('#edit #ext_email').val(email[3]);
    $('#edit #ext_telephone').val(telephone[4]);


}

function deleterecords(e) {

    if (confirm("Are you sure?")) {
        var tag = $(e).attr("id");

        var jsonData = { 'Id': tag };
        jsonData = JSON.stringify(jsonData);
        $.ajax({
            url: "../API/ExternalSolicitor.ashx?rand="+randomString()+"&a=" + "delete",
            type: "POST",
            data: jsonData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(resp) {
                var parsed = $.parseJSON(JSON.stringify(resp));
                $.each(parsed, function(i, jsondata) {
                    if (jsondata.result === "Deleted") {
                        alert("Deleted Successful");
                        location.reload(true);
                    } else {
                        alert("Error Occurred");
                    }
                });
            },
            error: function(resp) {
                
            }

        });
    }
}

function resetExternalPassword(e) {

    var tag = $(e).attr("id");
   
    $('#resetPassword #ext_idd').val(tag);  
  
}

function resetMyPassword() {
    
}