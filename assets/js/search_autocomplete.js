﻿$(document).ready(function () {

    SearchText();

    function SearchText() {
        $("#input-search").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/LawPavilion/API/AutoComplete.ashx",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: "{ 'txt' : '" + $("#input-search").val() + "'}",
                    dataFilter: function (data) { return data; },
                    success: function (data) {

                        response($.map(data, function (item) {
                            return {
                                label: item,
                                value: item
                            }
                        }))
                        //debugger;
                    },
                    error: function (result) {
                        console.log(result);
                    }
                });
            },
            minLength: 1,
            delay: 1000
        });
    }

});