﻿String.prototype.ucWords = function () {
    //   example 1: ucwords('kevin van  zonneveld');
    //   returns 1: 'Kevin Van  Zonneveld'
    //   example 2: ucwords('HELLO WORLD');
    //   returns 2: 'Hello World'
    if (this !== null) {
        return (this + '').toLowerCase().replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
            return $1.toUpperCase();
        });
    } else {
        return this;
    }
}

var T = {};
T.bindTo = {
    branchList: function (element, data, callback) {
        $(element).html('');
        $.each(data, function (i, v) {
            // data.forEach(function (v, i) {
            $(element).append(T.templates.branchListItem(v, i + 1));
        });
        if (typeof callback == 'function') { callback(); }
    },
    genericBinding: function (element, template, data, callback) {
        console.log(data);
        if (element === "#role_users") {
            $(element).html('<option value="-1">Select One...</option>');
        }

        else if (element === "#proceed_role_users" + data[0].role_id) {
            $(element).html('<option value="-1">Select One...</option>');
        }
        else {
            $(element).html('');
        }

        $.each(data, function (i, v) {
            // data.forEach(function (v, i) {
            $(element).append(template(v, i + 1));
        });
        if (typeof callback == 'function') { callback(); }
    }

    //proceedBinding

}

T.templates = {
    returnEmptyForNull: function (data) {
        if (data != null) {
            return data;
        } else {
            return ''
        }
    },
    branchListItem: function (data, i) {
        var lbl = data.active_fg ? 'label-success' : 'label-danger';
        var ac = data.active_fg ? 'Active' : 'Disabled';
        var ac_btn = !data.active_fg ? 'Activate' : 'Deactivate';
        var str = "<tr>\
                    <td><input data-id='" + data.id + "' class='branch_chk' type='checkbox' /></td>\
                    <td>" + i + "</td>\
                    <td id='p" + data.id + "' class='capitalize'><div contenteditable>" + data.name + "</div></td>\
                    <td id='s" + data.id + "' class='capitalize'><div contenteditable>" + data.street + "</div></td>\
                    <td id='t" + data.id + "' class='capitalize'><div contenteditable>" + data.town + "</div></td>\
                    <td id='st" + data.id + "' class='capitalize'><div contenteditable>" + data.state + "</div></td>\
                    <td id='r" + data.id + "' class='capitalize'><div contenteditable>" + data.region + "</div></td>\
                    <td id='c" + data.id + "' class='capitalize'><div contenteditable>" + data.country + "</div></td>\
                    <td><span class='label " + lbl + "'>" + ac + "</span></td>\
                    <td>\
                        <a data-id='" + data.id + "' class='btn edit_branch btn-default btn-sm'>Edit</a>\
                        <a click-action='" + ac + "' data-id='" + data.id + "'  class='btn btn-default ac_btn btn-sm'>" + ac_btn + "</a>\
                    </td>\
                </tr>";
        return str;
    },
    roleListItem: function (data, i) {
        var lbl = data.active_fg ? 'label-success' : 'label-danger';
        var ac = data.active_fg ? 'Active' : 'Disabled';
        var ac_btn = !data.active_fg ? 'Activate' : 'Deactivate';
        var str = "<tr>\
                    <td><input data-id='" + data.id + "' class='branch_chk' type='checkbox' /></td>\
                    <td>" + i + "</td>\
                    <td id='p" + data.id + "' class='capitalize'>" + data.name + "</td>\
                    <td><span class='label " + lbl + "'>" + ac + "</span></td>\
                    <td>\
                        <a data-id='" + data.id + "' class='btn edit_role btn-default btn-sm'>Edit</a>\
                        <a click-action='" + ac + "' data-id='" + data.id + "'  class='btn btn-default role_ac_btn btn-sm'>" + ac_btn + "</a>\
                    </td>\
                </tr>";
        return str;
    },
    dropDown: function (data) {
        var title = "";
        if (typeof data.note != "undefined") {
            title = data.note;
        }
        return "<option class='capitalize' data-title='" + title + "' value='" + data.id + "'>" + data.name.capitalizeFirstLetter() + "</option>";
    },
    formListItem: function (data) {
        return "<a data-id='" + data.id + "' href='#' title='" + data.name + "' class='list-group-item form-item capitalize'>" + data.name + "</a>";
    },
    processListItem: function (data) {
        if (data.status == 3) return "";
        var publish_status = (data.status == 1 ? "<label class='badge label-success'>PUBLISHED</label>" : "<label class='badge label-info'>UNPUBLISHED</label>");
        var str = "<a id='process_link" + data.id + "'  data-status='" + data.status + "' data-resign_role_id='" + data.reassign_role_id + "' data-id='" + data.id + "' href='#' data-name='" + data.name + "' title='" + data.description + "' class='list-group-item process_list_item capitalize'><h4 class='list-group-item-heading'><i class='fa fa-link'></i><span id='pname" + data.id + "'>" + data.name + "</span></h4><span id='pdesc" + data.id + "'>" + data.description + "</span> " + publish_status + "<br/><br/>";
        str += "<button type='button' id='edit_process_link" + data.id + "' data-resign_role_id='" + data.reassign_role_id + "' data-id='" + data.id + "' class='fa fa-pencil-square-o btn btn-sm btn-primary edit_process_link'>Edit</button>\
                <button type='button' data-id='" + data.id + "' class='fa fa-times btn btn-sm btn-danger delete_process_link'>Delete</button></a>";
        return str;
    },
    userListItem: function (data, i) {

        var lbl = data.status == 1 ? 'label-success' : 'label-danger';
        var ac = data.status == 1 ? 'Active' : 'In Active';
        var is_a = (data.role_id == 1) ? "hidden" : "";
        var ac_btn = (data.status != 1) ? 'Activate' : 'Deactivate';
        var str = "<tr>\
                    <td>" + i + "</td>\
                    <td class=''>" + data.email + "</td>\
                    <td class='capitalize' id='f" + data.id + "'><div contenteditable>" + data.firstname + "</div></td>\
                    <td class='capitalize' id='l" + data.id + "'><div contenteditable>" + data.lastname + "</div></td>\
                    <td class='capitalize' id='t" + data.id + "'><div contenteditable>" + data.telephone + "</div></td>\
                    <td class='text-uppercase' id='m" + data.id + "'>" + data.middlename + "</td>\
                    <td class='capitalize'>" + data.branch_name + "</td>\
                    <td class='capitalize'>" + data.role_name + "</td>\
                    <td><span class='label " + lbl + "'>" + ac + "</span></td>\
                    <td>\
                        <a data-id='" + data.id + "' class='btn edit_user btn-default btn-sm'>Edit</a>\
                        <a data-id='" + data.id + "' data-role='" + data.role_name + "' data-name='" + data.firstname + " " + data.lastname + "'  class='btn btn-danger sec_role_ac_btn btn-sm btn-info'>Sec. Roles</a>\
                        <a click-action='" + ac + "' data-id='" + data.id + "'  class='btn btn-default change_user_ac_btn btn-sm " + is_a + "'>" + ac_btn + "</a>\
                        <a click-action='" + ac + "' data-id='" + data.id + "'  class='btn btn-default reset_user_ac_btn btn-sm btn-info'>Reset Password</a>\
                    </td>\
                </tr>";
        return str;
    },
    stageListItem: function (data) {
        var str = "<a data-id='" + data.id + "' href='#' id='stage_list" + data.id + "' class='list-group-item stage_list'>\
                <h4 id='stage_list_head" + data.id + "' class='list-group-item-heading capitalize'>" + data.name + "</h4>\
                <p id='stage_list_description" + data.id + "' class='list-group-item-text'>" + data.description + "</p>\
                <br/><p><label>ROLE: " + data.role_name.toUpperCase() + "</label></p>\
                <br />\
                <button type='button' id='edit_stage_link" + data.id + "' data-id='" + data.id + "' class='fa fa-pencil-square-o btn btn-sm btn-primary edit_stage_link'>Edit</button>\
                <button type='button' data-id='" + data.id + "' class='fa fa-times btn btn-sm btn-danger delete_stage_link'>Delete</button>\
              </a>";
        return str;
    },
    newUserNavItem: function (data) {
        // var str = "<li>\
        //             <a data-payload='" + JSON.stringify(data) + "' class='userajax-link side-menu-link' title='" + data.report_name + "' data-process_id='"+data.process_id+"' data-name='" + data.report_name + "' data-id='" + data.report_id + "'  data_url='../Components/user_home2.html'   xdata-toggle='modal' xdata-target='#form_renderer'  href='#'><i class='fa fa-arrows-h'></i>  <span>" +data.report_name.ucWords() + "</span></a>\
        //                 </li>";
        var str = "<li>\
                    <a data-payload='" + JSON.stringify(data) + "' class='userajax-link side-menu-link' title='" + data.report_name + "' data-process_id='" + data.process_id + "' data-name='" + data.report_name + "' data-id='" + data.report_id + "'  data_url='../Components/user_home2.html'   xdata-toggle='modal' xdata-target='#form_renderer'  href='#'><i class='fa fa-chevron-right'></i> <span>" + data.report_name.ucWords() + "</span></a>\
                        </li>";
        return str;
    },
    userNavItem: function (data) {
        if (data.status != 1) { return ""; }
        var str = "<li style='height: 50px;padding-top: 13px;'>\
                    <a data-payload='" + JSON.stringify(data) + "' class='userajax-link' title='" + data.description + "' data-name='" + data.name.toUpperCase() + "' data-id='" + data.id + "'  data_url='../Components/user_home.htm'   xdata-toggle='modal' xdata-target='#form_renderer'  href='#'><i class='fa fa-list-ul'></i>  " + data.name.toUpperCase() + "</a>\
                        </li>";
        return str;
    },
    userDropDown: function (data) {

        if (data.role_id == 11) {
            return "<option class='capitalize' data-title='" + data.FIRM + "' value='" + data.id + "'>" + (data.FIRM).capitalizeFirstLetter() + "</option>";
        } else {
            return "<option class='capitalize' data-title='" + data.firstname + " " + data.lastname + "' value='" + data.id + "'>" + (data.firstname + " " + data.lastname).capitalizeFirstLetter() + "</option>";
        }

    },
    calendarDetail: function (data) {
        var str = "<div class='well well-sm'>\
					<label class='text-uppercase' for=''>Date of Last Hearing</label>\
				    <div>" + moment(data.hearing_date).format("dddd, MMMM Do YYYY") + "</div>\
				  </div>";
        str += "<div class='well well-sm'>\
					<label class='text-uppercase' for=''>Summary of Last Hearing</label>\
				    <div>" + data.summary + "</div>\
				  </div>";
        str += "<div class='well well-sm'>\
					<label class='text-uppercase' for=''>Date of Next Hearing</label>\
				    <div>" + data.next_hearing.format("dddd, MMMM Do YYYY") + "</div>\
				  </div>";
        str += "<div class='well well-sm'>\
					<label class='text-uppercase' for=''>Stage of Next Hearing</label>\
				    <div>" + data.next_stage + "</div>\
				  </div>";
        str += "<div class='well well-sm'>\
					<label class='text-uppercase' for=''>Next Action Plan</label>\
				    <div>" + data.next_action + "</div>\
				  </div>";
        return str;
    },
    renderPrintRequests: function (data, i) {
        var str = "<tr>\
                <td class='sorting_1'>" + i + ".</td>\
                <td><a href='#' data-payload='" + JSON.stringify(data) + "' class='detail_link'><i class='fa fa-tag'></i>  " + T.templates.returnEmptyForNull(data.title).ucWords() + "</a></td>\
                <td><a href='#' data-payload='" + JSON.stringify(data) + "' class='detail_link'><i class='fa fa-user'></i>  " + T.templates.returnEmptyForNull(data.created_by).ucWords() + "</a></td>\
                <td class='center'><button type='button' data-payload='" + JSON.stringify(data) + "' class='btn btn-xs btn-warning btn-block detail_link'><i class='fa fa-file-text'></i> View Template</button></td>\
            </tr>";
        return str;
    },



    renderAllRequests: function (data, i) {
        var dd = data.created_date.split('T')[0];
        dd = dd.split('-');
        dd = dd[2] + "-" + dd[1] + "-" + dd[0];
        var str = "<tr>\
                <td class='sorting_1'>" + i + ".</td>\
                <td><span data-process_id='"+ data.process_id + "' data-id='" + data.id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm' >" + data.process.ucWords() + "</span></td>\
                <td><a href='#' data-process_id='" + data.process_id + "' data-id='" + data.id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm'>" + T.templates.returnEmptyForNull(data.title).ucWords() + "</a></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.created_by).ucWords() + "</span></td>\
                <td><span class='detail_link'>" + dd + "</span></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.branch).ucWords() + "</span></td>\
                <td><span class='detail_link'>" + data.current_stage.ucWords() + "</span></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.held_by).ucWords() + "</span></td>\
                <td><span data-payload='" + JSON.stringify(data) + "' class='detail_link'>" + data.status_desc.ucWords() + "</a></td>\
                <td><span class='text-danger' data-payload='" + JSON.stringify(data) + "' id='generated_ref_" + data.request_id + "'>" + data.ref_no.toUpperCase() + "</span></td>\
                <td class='center'><button type='button' data-payload='" + JSON.stringify(data) + "' class='btn btn-xs btn-primary btn-block btn-comments'> All Comments </button>\
                <button data-payload = '" + JSON.stringify(data) + "' class='btn btn-xs btn-success btn-block btn-gen-ref' style='display:" + data.show_gen_btn + "'> Generate Ref </button>\
                </tr>";
        return str;
    },
    renderAllBranchRequests: function (data, i) {
        var dd = data.created_date.split('T')[0];
        dd = dd.split('-');
        dd = dd[2] + "-" + dd[1] + "-" + dd[0];
        var str = "<tr>\
                <td class='sorting_1'>" + i + ".</td>\
                <td><span data-process_id='"+ data.process_id + "' data-id='" + data.id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm' >" + data.process.ucWords() + "</span></td>\
                <td><a href='#' data-process_id='" + data.process_id + "' data-id='" + data.id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm'>" + T.templates.returnEmptyForNull(data.title).ucWords() + "</a></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.created_by).ucWords() + "</span></td>\
                <td><span class='detail_link'>" + dd + "</span></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.branch).ucWords() + "</span></td>\
                <td><span class='detail_link'>" + data.current_stage.ucWords() + "</span></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.held_by).ucWords() + "</span></td>\
                <td><span data-payload='" + JSON.stringify(data) + "' class='detail_link'>" + data.status_desc.ucWords() + "</a></td>\
               </tr>";
        return str;
    },


    renderAllOngoingRequests: function (data, i) {
        var dd = data.created_date.split('T')[0];
        dd = dd.split('-');
        dd = dd[2] + "-" + dd[1] + "-" + dd[0];
        var str = "<tr>\
                <td class='sorting_1'>" + i + ".</td>\
                <td><span data-process_id='"+ data.process_id + "' data-id='" + data.id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm' >" + data.process + "</span></td>\
                <td><a href='#' data-process_id='" + data.process_id + "' data-id='" + data.id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm'>" + T.templates.returnEmptyForNull(data.title) + "</a></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.created_by) + "</span></td>\
                <td><span class='detail_link'>" + dd + "</span></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.branch) + "</span></td>\
                <td><span class='detail_link'>" + data.current_stage + "</span></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.held_by) + "</span></td>\
                <td><span data-payload='" + JSON.stringify(data) + "' class='detail_link'>" + data.status_desc + "</a></td>\
                <td><span class='text-danger' data-payload='" + JSON.stringify(data) + "' id='generated_ref_" + data.request_id + "'>" + data.ref_no.toUpperCase() + "</span></td>\
                <td class='center'><button type='button' data-payload='" + JSON.stringify(data) + "' class='btn btn-xs btn-primary btn-block btn-comments'> All Comments </button>\
                <button data-payload = '" + JSON.stringify(data) + "' class='btn btn-xs btn-success btn-block btn-gen-ref' style='display:" + data.show_gen_btn + "'> Generate Ref </button>\
                <button data-payload = '" + JSON.stringify(data) + "' class='btn btn-xs btn-danger btn-block generic-start' style='display:" + data.show_term_btn + "' \
                data-stage='-1' \
                data-process='22' \
                data-start_stage_id='" + data.start_stage_id + "' \
                data-start_form_id='" + data.start_form_id + "' \
                data-toggle='modal' data-backdrop='static' data-target='#form_renderer' data-process_name='Termination of Ongoing Perfection'> Terminate </button></td>\
            </tr>";
        return str;
    },
    //<th>Process</th>
    //            <th>Request Title</th>
    //            <th>Created By</th>
    //            <th>Date Created</th>
    //            <th>Branch</th>
    //            <th>Current Stage</th>
    //            <th>Current User</th>
    //            <th>Status</th>
    renderAllHubRequests: function (data, i) {
        var dd = data.created_date.split('T')[0];
        dd = dd.split('-');
        dd = dd[2] + "-" + dd[1] + "-" + dd[0];
        var str = "<tr>\
                <td class='sorting_1'>" + i + ".</td>\
                <td><span data-process_id='"+ data.process_id + "' data-id='" + data.id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm' >" + data.process + "</span></td>\
                <td><a href='#' data-process_id='" + data.process_id + "' data-id='" + data.id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm'>" + T.templates.returnEmptyForNull(data.title) + "</a></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.created_by) + "</span></td>\
                <td><span class='detail_link'>" + dd + "</span></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.branch) + "</span></td>\
                <td><span class='detail_link'>" + data.current_stage + "</span></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.held_by) + "</span></td>\
                <td><span data-payload='" + JSON.stringify(data) + "' class='detail_link'>" + data.status_desc + "</a></td>\
                </tr>";
        return str;
    },

    renderAudits: function (data, i) {

        var str = "<tr>\
                <td class='sorting_1'>" + i + ".</td>\
                <td><span class='detail_link'>" + data.user_id + "</span></td>\
                <td><span class='detail_link'>" + data.action + "</span></td>\
                <td><span class='detail_link'>" + data.details + "</span></td>\
                <td><span class='detail_link'>" + data.timestamp + "</span></td>\ <td><span class='detail_link'>" + data.ip + "</span></td>\
                </tr>";
        return str;
    },

    //admin

    renderAllAdminRequests: function (data, i) {
        var dd = data.created_date.split('T')[0];
        dd = dd.split('-');
        dd = dd[2] + "-" + dd[1] + "-" + dd[0];
        var str = "<tr>\
                <td class='sorting_1'>" + i + ".</td>\
                <td><span data-process_id='"+ data.process_id + "' data-id='" + data.request_id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm' >" + data.process.ucWords() + "</span></td>\
                <td><a href='#' data-process_id='" + data.process_id + "' data-id='" + data.request_id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm'>" + T.templates.returnEmptyForNull(data.request_title).ucWords() + "</a></td>\
                <td><span class='detail_link'>" + data.title + "</span></td>\
                <td><span class='detail_link'>" + dd + "</span></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.branch_name).ucWords() + "</span></td>\
                <td><span class='detail_link'>" + data.stage_name.ucWords() + "</span></td>\
                <td><span data-payload='" + JSON.stringify(data) + "' class='detail_link'>" + data.status_desc.ucWords() + "</a></td>\
               <td class='center'><button type='button' data-payload='" + JSON.stringify(data) + "' class='btn btn-xs btn-primary btn-block btn-reassign'> Reassign Request </button></td>\
            </tr>";
        return str;
    },


    renderAllAdminRequestsStage: function (data, i) {
        var dd = data.created_date.split('T')[0];
        dd = dd.split('-');
        dd = dd[2] + "-" + dd[1] + "-" + dd[0];
        var str = "<tr>\
                <td class='sorting_1'>" + i + ".</td>\
                <td><span data-process_id='"+ data.process_id + "' data-id='" + data.request_id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm' >" + data.process.ucWords() + "</span></td>\
                <td><a href='#' data-process_id='" + data.process_id + "' data-id='" + data.request_id + "' data-payload='" + JSON.stringify(data) + "' class='request_detail_link' data_url='../Components/user_requestdetail.htm'>" + T.templates.returnEmptyForNull(data.request_title).ucWords() + "</a></td>\
                <td><span class='detail_link'>" + data.title + "</span></td>\
                <td><span class='detail_link'>" + dd + "</span></td>\
                <td><span class='detail_link'>" + T.templates.returnEmptyForNull(data.branch_name).ucWords() + "</span></td>\
                <td><span class='detail_link'>" + data.stage_name.ucWords() + "</span></td>\
                <td><span data-payload='" + JSON.stringify(data) + "' class='detail_link'>" + data.status_desc.ucWords() + "</a></td>\
               <td class='center'><button type='button' data-payload='" + JSON.stringify(data) + "' class='btn btn-xs btn-primary btn-block btn-move_forward'> Move Request </button></td>\
            </tr>";
        return str;
    },

    renderAllComments: function (data, i) {
        var dd = data.created_date.split('T')[0];
        var tt = data.created_date.split('T')[1]
        dd = dd.split('-');
        dd = dd[2] + "/" + dd[1] + "/" + dd[0];
        var str = "<div class='row'>\
                <div class='well' style='margin:5px;'>\
                <div class='row'><b>"+ data.username.toUpperCase() + "</b> commented on <b>" + dd + " (" + tt + ")</b>:</div><br/>\
                <div class='row'>"+ data.value + "</div>\
                </div>\
                </div>";

        return str;
    },
    //FESTUS
    pendingLists: function (data, i) {
        //var highlight = (UserHandler.user.id == data.current_user_id) ? "style='background-color: rgb(249, 237, 177);'" : "";
        var span = Math.ceil(data.lifespan / 60);
        var str = "<li class='list-group-item'><!--<i class='fa fa-list-ul'></i>--> <a data-payload='" + JSON.stringify(data) + "' data-process_id='" + data.process_id + "' data-id='" + data.id + "' data_url='../Components/user_requestdetail.htm' class='request_detail_link'>" + data.title.ucWords() + "</a></li>"; // <span class='pull-right label label-warning'>" + secondstotime(span) + "</span>
        return str;
    },


    pendingList: function (data, i) {
        var highlight = (UserHandler.user.id == data.current_user_id) ? "style='background-color: rgb(249, 237, 177);'" : "";
        var span = Math.ceil(data.lifespan / 60);
        var str = "<li class='list-group-item'><!--<i class='fa fa-list-ul'></i>--> <a data-payload='" + JSON.stringify(data) + "' data-process_id='" + data.process_id + "' data-id='" + data.id + "' data_url='../Components/user_requestdetail.htm' class='request_detail_link'>" + data.title.ucWords() + "</a></li>"; //<span class='pull-right label label-warning'>" + secondstotime(span) + "</span>
        return str;
    },
    requestTabularList2: function (data, i) {
        var highlight = (UserHandler.user.id == data.current_user_id) ? "style='background-color: rgb(249, 237, 177);'" : "";
        var span = Math.ceil(data.lifespan / 60);
        var str = "<tr role='row' " + highlight + ">\
                                <td class='sorting_1'>" + i + ".</td>\
                                <td><a data-payload='" + JSON.stringify(data) + "' data-process_id='" + data.process_id + "' data-id='" + data.request_id + "' data_url='../Components/user_requestdetail.htm' class='request_detail_link'><i class='fa fa-tag'></i>  " + data.title.ucWords() + "</a></td>\
                                <td><span class ='age timeago' title='" + data.created_date + "'></span> | <span class='text-success'>" + secondstotime(span) + "</span></td>\
                                <td class='center'><i class='fa fa-edit'></i>" + data.created_by.ucWords() + "</td>\
                                <td class='center'>- <span class='text-uppercase label label-danger'>" + data.current_stage + "</span></td>\
                                <td class='center'><button type='button' data-payload='" + JSON.stringify(data) + "' data-process_id='" + data.process_id + "' data-id='" + data.request_id + "' data_url='../Components/user_requestdetail.htm'  class='btn btn-xs btn-warning btn-block request_detail_link'><i class='fa fa-eye'></i> View</button></td>\
                            </tr>";
        return str;
    },
    requestTabularList: function (data, i) {
        /*var today = new Date();
        var birth = new Date(data.created_date);
        var age = Math.ceil((today - birth) / 1000);
        var test = new Date(age);
        log(test);*/
        //log(UserHandler.user);
        var highlight = (UserHandler.user.id == data.current_user_id) ? "style='background-color: rgb(249, 237, 177);'" : "";
        var status_lbl = data.status == 6 ? 'Completed' : 'In Progress';
        var span = Math.ceil(data.lifespan / 60);
        var str = "<tr class='gradeA odd' role='row' " + highlight + ">\
                                <td class='sorting_1'>" + i + "</td>\
                                <td><i class='fa fa-tag'></i>  " + data.process_name.capitalizeFirstLetter() + "</td>\
                                <td>" + data.created_date + "</td>\
                                <td class='center'>" + (data.current_user_firstname + " " + data.current_user_lastname).capitalizeFirstLetter() + "</td>\
                                <td>" + secondstotime(span) + "</td>\
                                <td><span class ='age timeago' title='" + data.created_date + "'></span></td>\
                                <td class='center'>" + (data.init_user_firstname + " " + data.init_user_lastname).capitalizeFirstLetter() + "</td>\
                                <td>" + status_lbl + "</td>\
                                <td class='center'><button type='button' data-payload='" + JSON.stringify(data) + "' data-process_id='" + data.process_id + "' data-id='" + data.id + "' data_url='../Components/user_requestdetail.htm'  class='btn btn-sm request_detail_link'>View</button></td>\
                            </tr>";
        return str;
    },
    formDataHistory: function (data) {
      //  console.log(data);

        var min_spent = Math.round(parseInt(data.time_taken) / 60);
        var d = new Date(data.created_date);
        //alert(data.created_date);
        // alert(d.toUTCString());
        var dd = data.created_date.split('T')[0];

        var tt = data.created_date.split('T')[1];
        var m = moment(dd, "MMMM Do YYYY");
        var mm = moment(data.created_date);

        // alert(m);
        /*var str = "<div class='panel panel-primary'>\
        <div class='panel-heading' title='" + data.form_description + "' data-toggle='tooltip' role='tab' id='headingTwo'>\
        <h4 class='panel-title'>\
        <a class='collapsed accord' role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse" + data.id + "' aria-expanded='false' aria-controls='collapseTwo'><i class='fa fa-minus-square-o'></i>\
        " + data.stage_name.capitalizeFirstLetter() + " - #" + data.user_firstname.capitalizeFirstLetter() + " " + data.user_lastname.capitalizeFirstLetter() + "\
        </a>\
        <span class='label label-danger pull-right'>" + min_spent + "mins</span>\
        <span class='label label-warning pull-right'>" + d.toUTCString() + "</span>\
        </h4>\
        </div>\
        <div data-payload='" + JSON.stringify(data) + "' data-id='" + data.id + "' id='collapse" + data.id + "' class='panel-collapse collapse' role='tabpanel' aria-labelledby='headingTwo'>\
        <div class='panel-body'>\
        \
        </div>\
        </div>\
        </div>";*/
       // console.log(UserHandler.user.role_id);
        var str = '';

        str = '<div class="vertical-timeline-block animated-panel zoomIn" style="animation-delay: 0.2s;"><div class="vertical-timeline-icon navy-bg"><i class="fa fa-calendar"></i></div><div class="vertical-timeline-content"><div class="p-sm"><span class="vertical-date pull-right">';

        str += '<br> </span><h2>' + data.stage_description.ucWords();
        str += ' - #' + data.user_firstname.ucWords() + ' ' + data.user_lastname.ucWords() + '</h2>';
        str += '<div data-payload="' + JSON.stringify(data) + '" data-id="' + data.id + '" id="collapse' + data.id + '"><div class="panel-body no-padding"></div></div></div><div class="panel-footer">';
        str += "<button data-target='#collapse" + data.id + "' type='button' class='btn btn-xs btn-danger accord' data-payload='" + JSON.stringify(data) + "'>";
        str += '<i class="fa fa-list-ul"></i> Show Details</button>  <span class="pull-right">' + mm.format("dddd, MMMM Do YYYY") + '</span> <span class="pull-right" style="margin-left: 10px; margin-right: 10px;">' + min_spent + 'mins</span> </div></div></div>';

        if (UserHandler.user.role_id == "24" || UserHandler.user.role_id == "15") {

            str = '<div class="vertical-timeline-block animated-panel zoomIn" style="animation-delay: 0.2s;"><div class="vertical-timeline-icon navy-bg"><i class="fa fa-calendar"></i></div><div class="vertical-timeline-content"><div class="p-sm"><span class="vertical-date pull-right">';

            str += '<br> </span><h2>' + data.stage_description.ucWords();
            str += ' - #' + data.user_firstname.ucWords() + ' ' + data.user_lastname.ucWords() + '</h2>';
            str += '<div data-payload="' + JSON.stringify(data) + '" data-id="' + data.id + '" id="collapse' + data.id + '"><div class="panel-body no-padding"></div></div></div><div class="panel-footer">';
            str += "<button data-target='#collapse" + data.id + "' type='button' class='btn btn-xs btn-danger accord' data-payload='" + JSON.stringify(data) + "'>";
            str += '<i class="fa fa-list-ul"></i> Show Details</button>  <span class="pull-right">' + mm.format("dddd, MMMM Do YYYY") + '</span> <span class="pull-right" style="margin-left: 10px; margin-right: 10px;">' + min_spent + 'mins</span>';
            str += "<button data-target='#dataModal" + data.id + "' type='submit' class='btn btn-xs btn-primary edit' data-toggle='modal' data-payload='" + JSON.stringify(data) + "'>";
            str += '<i class="fa fa-list-ul"></i> Edit Content</button>   </div></div></div>';
        }

        return str;
    },
    renderData: function (control, data) {
        console.log(data);
        var _value = control.type == 2 ? moment(data.value).format("dddd, MMMM Do YYYY, hh:mm:ss a") : (data.value == 'on' ? 'YES' : data.value.capitalizeFirstLetter());
        var str = "<div class='well well-sm'>\
					<label for=''>" + control.label.capitalizeFirstLetter() + "</label>\
				    <div>" + _value + "</div>\
                   				  </div>";
        return str;
    },

    renderDataTextFormat: function (control, data) {
        console.log(data);
        var _value = control.type == 2 ? moment(data.value).format("dddd, MMMM Do YYYY, hh:mm:ss a") : (data.value == 'on' ? 'YES' : data.value.capitalizeFirstLetter());
        var str = "<div class='well well-sm'>\
					<label for=''>" + control.label.capitalizeFirstLetter() + "</label>\
				   <br/> <input type='text' value='" + _value + "' id='" + data.id + "' data-form_control_id='" + data.form_control_id + "' class='form-control'/>\
                   				  </div>";
        return str;
    },

    renderComment: function (data) {
        var d = new Date(data.created_date);
        var str = "<a href='#' class='list-group-item'>\
                        <label>" + data.user_firstname.capitalizeFirstLetter() + " " + data.user_lastname.capitalizeFirstLetter() + " " + data.user_middlename.capitalizeFirstLetter() + " @" + data.stage_name.capitalizeFirstLetter() + "</label><br />\
                        <i class='fa fa-comment fa-fw'></i>" + data.message + "\
                        <br/><br/><span class='label label-warning'>" + d.toUTCString() + "</span>\
                    </a>";
        return str;
    },
    renderResourceItem: function (data) {
        var file_name = data.id + PageHandler.Files[data.mime_type];
        var str = "<a href='../resources/" + file_name + "' download='" + data.label + "' class='list-group-item'><i class='fa fa-file-text'></i> " + data.description + "\
            <div>" + data.user_firstname.capitalizeFirstLetter() + " " + data.user_lastname.capitalizeFirstLetter() + " " + data.user_middlename.capitalizeFirstLetter() + " @" + data.stage_name.capitalizeFirstLetter() + "</div><br />\
                    <label>" + data.label + "</label>\
                    <span class='label label-success'>Download</span>\
                    </a>";
        return str;
    }
}