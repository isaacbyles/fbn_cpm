//added 03-01-2016 GIT
function getIndex(data, process_id) {
    var resp = -1;
    $.each(data, function (i, val) {
        $.each(val, function (key, name) {

            if (key === "process_id" && name == process_id) {
                resp = i;
                // alert(key + " : " +i);

            }
        });

    });
    return resp;

}

//--------------------------
var WorkFlow = function () {
    this.current_stage_id = 0;
    this.next_stage_id = 0;
    this.process_id = 0;
}
var UserHandler = {
    menus: {},
    user: {},
    currentFormData: {},
    process: {},
    process_stages: {},
    init: function (d) {
        //try {
        var payload = JSON.parse(d);

        UserHandler.user = payload.user[0];


        UserHandler.process = payload.process;
        UserHandler.process_stages = payload.start_stage;


        //UserHandler.buildMenu(UserHandler.process, payload.start_stage);


        // Fetch Menu Items Here
        //PageHandler.sendRequest(PageHandler.Resources.report_api+UserHandler.user.id+'/main', {}, PageHandler.HTTPMethods.GET, function(links){
        PageHandler.sendRequest(PageHandler.Resources.new_report_api + UserHandler.user.id, {}, PageHandler.HTTPMethods.GET, function (links) {


            var theLinks = links.data;

            // Keys are corporate, litigation & perfection
            UserHandler.menus = _.union(theLinks.perfection.Reports, theLinks.corporate.Reports, theLinks.litigation.Reports);

            UserHandler.buildNewMenu(theLinks);

            var pay = payload.start_stage;
            $.each(pay, function (i, data) {

                UserHandler.process_stages[i] = data;
                //console.log(UserHandler.process_stages);
            });
            //payload.start_stage.forEach(function (h, i) {
            //	UserHandler.process_stages[h.process_id] = h;
            //    UserHandler.process_stages[i] = h;
            //});
        });
        // console.log(UserHandler.user.firstname + " " + UserHandler.user.lastname);
        $(".user_name").html((UserHandler.user.firstname + " " + UserHandler.user.lastname).toUpperCase());


        //} catch (e) {
        //    alert("Login data invalid. Parse Error while trying to parse User Session Data. This process cannot be completed");
        //    window.location.href = PageHandler.Resources.base_path;
        //}


        UserHandler.bindToDom();
    },
    buildNewMenu: function (tree) {
        //var menu_pane = $(target);
        for (var unit in tree) {
            // console.log(tree[unit].Reports.length);
            if (tree[unit].Reports.length) {
                var target = $('#' + unit + '-collapse');
                //tree[unit].Reports.forEach(function (item){
                ////if bond and guarantee, attach to bond-collapse
                //if(item.report_name.toLowerCase().indexOf("bond")>-1 && item.report_name.toLowerCase().indexOf("guarantee")>-1){
                //    target=$('#bond-collapse');
                //}else{
                //    target = $('#'+unit+'-collapse');
                //}
                ////-----------------------------
                //    target.append(T.templates.newUserNavItem(item));
                //});
                var resp = tree[unit].Reports;
                $.each(resp, function (i, item) {
                    if (item.report_name.toLowerCase().indexOf("bond") > -1 && item.report_name.toLowerCase().indexOf("guarantee") > -1) {
                        target = $('#bond-collapse');
                    } else {
                        target = $('#' + unit + '-collapse');
                    }
                    //-----------------------------
                    target.append(T.templates.newUserNavItem(item));
                });
            } else {
                $('#' + unit + '-menu').hide();
            }
        }
        // UserHandler.menus.forEach(function (item){
        // 	menu_pane.append(T.templates.newUserNavItem(item));
        // });

        // Letter Printing Menu
        $("#print-collapse").append('<li><a class="side-menu-link" id="print_link" href="#"><i class="fa fa-print"></i> <span>Bond Templates</span></a></li>');

        // corporate Unit Menu
        $("#print-collapse").append('<li><a class="side-menu-link" id="corporate_print_link" href="#"><i class="fa fa-print"></i> <span>Corporate Unit: Templates</span></a></li>');


        // Perfection Unit Menu
        $("#print-collapse").append('<li><a class="side-menu-link" id="perfection_print_link" href="#"><i class="fa fa-print"></i> <span>Perfection Unit: Templates</span></a></li>');

        // Report Link
        // $("#mainside-menu").append('<li id="report_rem" class="dynamicrep"><a class="side-menu-link" id="report_link" href="#"><i class="fa fa-bar-chart"></i> <span>Reports</span></a></li>');
    },
    /*buildMenu: function (process, start_stages) {
        if (process) {
            var menu_pane = $("#menu_pane");
            menu_pane.html('');
            var temp = [];
            process.forEach(function (v, i) {
                if (temp.indexOf(v.id) < 0) {
                    menu_pane.append(T.templates.userNavItem(v));
                    temp.push(v.id);
                }
            });
            start_stages.forEach(function (h, i) {
                UserHandler.process_stages[h.process_id] = h;
            });
        }
    }*/
};
UserHandler.d = {
    getCalendar: function (callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.calendar + UserHandler.user.id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    getNewRequestsList: function (report_id, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.new_report_api + [UserHandler.user.id, report_id].join('/'), {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    //getRequests: function (process_id, callback) {
    //    //var filter = user_id == -1 ? "" : "";
    //    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Request.ashx?rand="+randomString()+"&a=get&with_process=1&with_stage=1&with_init_user=1&with_current_user=1&process_id=" + process_id, {}, PageHandler.HTTPMethods.GET, function (response) {
    //        if (typeof callback == "function") {
    //            callback(response);
    //        }
    //    });
    //},
    createRequest: function (data, callback) {
        //var filter = user_id == -1 ? "" : "";

        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Request.ashx?rand=" + randomString() + "&a=create", data, PageHandler.HTTPMethods.POST, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    proceedRequest: function (data, callback) {
        //var filter = user_id == -1 ? "" : "";
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Request.ashx?rand=" + randomString() + "&a=proceed", data, PageHandler.HTTPMethods.POST, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    endRequest: function (data, callback) {
        //var filter = user_id == -1 ? "" : "";
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Request.ashx?rand=" + randomString() + "&a=end", data, PageHandler.HTTPMethods.POST, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },


    //festus comment
    getRequestsByUserID: function (user_id, callback) {
        //var filter = user_id == -1 ? "" : "";
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Request.ashx?rand=" + randomString() + "&a=get&with_process=1&with_stage=1&with_init_user=1&with_current_user=1&current_user_id=" + user_id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },



    //festus
    getAllHubRequests: function (callback) {
        //var filter = user_id == -1 ? "" : "";
        var user = UserHandler.user;
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/HubAllRequest.ashx?rand=" + randomString(), {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response.data);
            }
        });

    },

    getAllAbujaRegionRequests: function (callback) {

        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/RequestsRegion.ashx?rand=" + randomString() + "&region=Abuja", {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    getAllEnuguRegionRequests: function (callback) {

        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/RequestsRegion.ashx?rand=" + randomString() + "&region=Enugu", {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },

    getAllIbadanRegionRequests: function (callback) {

        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/RequestsRegion.ashx?rand=" + randomString() + "&region=Ibadan", {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },

    getAllBeninRegionRequests: function (callback) {

        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/RequestsRegion.ashx?rand=" + randomString() + "&region=Benin", {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },


    getAllKadunaRegionRequests: function (callback) {

        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/RequestsRegion.ashx?rand=" + randomString() + "&region=Kaduna", {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },

    getAllPHRegionRequests: function (callback) {

        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/RequestsRegion.ashx?rand=" + randomString() + "&region=Port Harcourt", {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    //festus
    getRequests: function (user_id, branch_id, role_id, callback) {
        //var filter = user_id == -1 ? "" : "";
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/RequestsRole.ashx?rand=" + randomString() + "&user_id=" + user_id + "&branch_id=" + branch_id + "&role_id=" + role_id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },

    //Search Request
    //getSearchRequests: function (user_id, branch_id, role_id, searchText, callback) {
    //    //var filter = user_id == -1 ? "" : "";
    //    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/FilterRequest.ashx?rand=" + randomString() + "&user_id=" + user_id + "&branch_id=" + branch_id + "&role_id=" + role_id + "&search=" + searchText, {}, PageHandler.HTTPMethods.GET, function (response) {
    //        if (typeof callback == "function") {
    //            callback(response);
    //        }
    //    });
    //},


    //created by GIT 17-01-2016 for all requests
    getAllCommentsByUserID: function (user_id, request_id, callback) {
        //var filter = user_id == -1 ? "" : "";
        var user = UserHandler.user;
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/AllComments.ashx/" + user.id + "/" + request_id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response.data);
            }
        });
    },

    //created by GIT 23-02-2016 for all requests
    getNewRef: function (request_id, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/RefNo.ashx?rand=" + randomString() + "&request_id=" + request_id + "&userId=" + UserHandler.user.id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response.data);
            }
        });
    },

    //created by GIT 17-01-2016 for all requests
    getAllRequestsByUserID: function (user_id, callback) {
        //var filter = user_id == -1 ? "" : "";
        var user = UserHandler.user;
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/AllRequests.ashx/" + user.id + "/" + user.role_id + "/" + user.branch_id + "/1/", {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response.data);
            }
        });

    },
    getAllBranchRequestsByUserID: function (branch_id, callback) {
        //var filter = user_id == -1 ? "" : "";
        var user = UserHandler.user;
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/BranchRequests.ashx?rand=" + randomString() + "&branch_id=" + branch_id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response.data);
            }
        });

    },
    getAllUsers: function (callback) {
        //var filter = user_id == -1 ? "" : "";
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/User.ashx?rand=' + randomString() + '&a=get', {}, PageHandler.HTTPMethods.GET, function (response) {


            if (typeof callback == 'function') {
                callback(response.data.data[0]);
            }
        });
    },

    getAllBranches: function (callback) {
        //var filter = user_id == -1 ? "" : "";
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Branch.ashx?rand=' + randomString() + '&a=get_all', {}, PageHandler.HTTPMethods.GET, function (response) {
            //if (response.status && response.data.status == 1) {

            //T.bindTo.branchList(PageHandler.Global.tbody_branchList, response.data.data, function () {
            //        PageHandler.BindToDocument();
            //    });
            //}
            if (typeof callback == 'function') {
                callback(response.data.data);
            }
        });
    },
    getAllOngoingRequestsByUserID: function (user_id, callback) {
        //var filter = user_id == -1 ? "" : "";
        var user = UserHandler.user;
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/AllOngoingRequests.ashx/" + user.id + "/" + user.role_id + "/" + user.branch_id + "/1/", {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response.data);
            }
        });

    },

    AdmingetAllRequests: function (callback) {
        //var filter = user_id == -1 ? "" : "";
        var user = UserHandler.user;
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Reassign_Request.ashx?rand=" + randomString(), {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response.data);
            }
        });

    },

    getAllUser: function (callback) {

        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/User.ashx?rand=" + randomString() + "&a=" + "get", {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response.data);
            }
        });

    },

    //    // Created By Adeyemi Salau
    // 26th September, 2015
    getReportsByUserID: function (user_id, repid, callback) {
        //var filter = user_id == -1 ? "" : "";
        PageHandler.sendXMLHttp(PageHandler.Resources.lawpavilion_api + "/Others.ashx/" + user_id + "/" + repid, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    makeComment: function (data, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Request.ashx?rand=" + randomString() + "&a=add_comment", data, PageHandler.HTTPMethods.POST, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    getComment: function (request_id, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Request.ashx?rand=" + randomString() + "&a=get_comment&with_stage=1&with_user=1&request_id=" + request_id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    //getResource: function (request_id, callback) {
    //    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Resource.ashx?rand="+randomString()+"&a=get&with_stage=1&with_user=1&request_id=" + request_id+ "&user_id=" + UserHandler.user.id + "&role_id="+UserHandler.user.role_id, {}, PageHandler.HTTPMethods.GET, function (response) {
    //        if (typeof callback == "function") {
    //            callback(response);
    //        }
    //    });
    //},
    getResource: function (request_id, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Resource.ashx?rand=" + randomString() + "&a=get&with_stage=1&with_user=1&request_id=" + request_id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    getNextStageDetail: function (current_id, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Process.ashx?rand=" + randomString() + "&a=get_workflow&with_next_stage=1&current_id=" + current_id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    getRequestForms: function (request_id, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Request.ashx?rand=" + randomString() + "&a=get_trail&with_form=1&with_user=1&with_stage=1&request_id=" + request_id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    getRequestFormsData: function (request_id, form_id, stage_id, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Request.ashx?rand=" + randomString() + "&a=get_form_data&with_user=1&request_id=" + request_id + "&stage_id=" + stage_id + "&form_id=" + form_id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    getRequestFormsDataTrailRecord: function (trail_id, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Request.ashx?rand=" + randomString() + "&a=get_form_data&request_trail_id=" + trail_id, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    }
    ,
    getRequestFormsDataDefinition: function (form_id, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Form.ashx?rand=" + randomString() + "&a=get_controls", { form_id: form_id }, PageHandler.HTTPMethods.POST, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    doSearch: function (term, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.search + [UserHandler.user.id, encodeURIComponent(term)].join('/'), {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    },
    getTemplateRequests: function (template, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.print_api + template, {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response.data);
            }
        });
    },
    getTemplateRequestData: function (template, request_id, request_trail_id, callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.print_api + [template, request_id, request_trail_id].join('/'), {}, PageHandler.HTTPMethods.GET, function (response) {
            if (typeof callback == "function") {
                callback(response.data);
            }
        });
    }
}
UserHandler.bindToDom = function () {
    /********** Javascript FOR USER ACTIONS **************/

    PageHandler.Handles.b(PageHandler.Handles.userajaxlinks).unbind('click').on('click', function (e) {
        var link = $(this).attr('data_url'); //user_page 
        var report_id = $(this).data('id');
        var process_id = $(this).data('process_id');
        var title = $(this).data('name');

        //var stage = UserHandler.process_stages[process_id];
        //GIT 03-01-2016
        if (getIndex(UserHandler.process_stages, process_id) == -1) { //edited by festus
            $("#start_stage").addClass("hidden");
        } else {

            var stage = UserHandler.process_stages[getIndex(UserHandler.process_stages, process_id)];


            if (link) {
                PageHandler.Handles.load(PageHandler.Global.user_page, link, function (d) {
                    $("#requests_home").html("Loading ...");

                    if (stage || UserHandler.user.role_name == "branch office" || UserHandler.user.role_name == "relationship manager" || UserHandler.user.role_name == "branch manager") {
                        $("#start_stage").removeClass("hidden");
                        $("#start_stage").attr("data-process", process_id);
                        $("#start_stage").attr("data-process_name", title);
                    } else {
                        $("#start_stage").addClass("hidden");
                    }


                    /*UserHandler.d.getRequests(process_id, function (response) {
                                        log(response);
                                        if (response.status && response.data.status == 1) {
                                            $("#requests_home").html("");
                                            response.data.data.forEach(function (v, i) {
                                                $("#requests_home").append(T.templates.requestTabularList(v, i + 1));
                                            });
                                            PageHandler.BindToDocument();
                                            UserHandler.bindToDom();
                                        } else {
                                            $("#requests_home").html("No Pending Request");
                                        }
                                    });*/
                    UserHandler.d.getNewRequestsList(report_id, function (response) {
                        if (response.status) {
                            $(PageHandler.Global.page_title).html(_.find(UserHandler.menus, function (m) { return m.report_id == report_id; }).report_name);
                            $("#requests_home").html("");
                            var completed = _.filter(response.data, function (r) { return r.status == 6 });
                            var pending = _.filter(response.data, function (r) { return r.status != 6 });

                            $.each(pending, function (i, v) {
                                $("#requests_home").append(T.templates.requestTabularList2(v, i + 1));
                            });
                            $.each(completed, function (i, v) {
                                $("#requests_completed").append(T.templates.requestTabularList2(v, i + 1));
                            });
                            //pending.forEach(function(v, i) {
                            //     $("#requests_home").append(T.templates.requestTabularList2(v, i + 1));
                            //});
                            //completed.forEach(function(v, i) {
                            //      $("#requests_completed").append(T.templates.requestTabularList2(v, i + 1));
                            //});

                            PageHandler.BindToDocument();
                            UserHandler.bindToDom();
                        } else {
                            $("#requests_home").html("No Pending Request");
                        }
                    });
                    PageHandler.BindToDocument();
                    UserHandler.bindToDom();
                });
            }

        }

        PageHandler.markSelectedItem(PageHandler.Handles.userajaxlinks, $(this));

        e.preventDefault();
        return false;

    });






    // Binding For Reports Link in Side Manu
    $(PageHandler.Global.report_link).unbind('click').on('click', function (e) {
        $(PageHandler.Global.page_title).html($(this).text());

        PageHandler.Handles.loadREPORTS(PageHandler.Global.user_page, PageHandler.Resources.report_home, function (d) {
        });

        PageHandler.markSelectedItem(PageHandler.Global.report_link, $(this));
        e.preventDefault();
        return false;
    });


    // Binding For Reports Link in Side Manu
    $(PageHandler.Global.schedule_link).unbind('click').on('click', function (e) {
        $(PageHandler.Global.page_title).html($(this).text());

        PageHandler.Handles.loadREPORTS(PageHandler.Global.user_page, PageHandler.Resources.schedule_home, function (d) {
        });

        PageHandler.markSelectedItem(PageHandler.Global.schedule_link, $(this));
        e.preventDefault();
        return false;
    });





    // Binding For Print Link in Side Manu
    $(PageHandler.Global.print_link).unbind('click').on('click', function (e) {
        $(PageHandler.Global.page_title).html($(this).text());
        PageHandler.markSelectedItem(PageHandler.Global.print_link, $(this));

        $('.theLetter').hide();

        PageHandler.Handles.loadREPORTS(PageHandler.Global.user_page, PageHandler.Resources.print_home, function (d) {
            var table = $('#print-table');
            table.hide();

            // Bind Evt Handlers
            $(PageHandler.Global.print_links).find('a').unbind('click').on('click', function (e) {

                var reportTemplate = $(this).data('template');
                table.hide();
                $('.theLetter').hide();

                // Get Requests for the selected Report Template
                UserHandler.d.getTemplateRequests(reportTemplate, function (response) {

                    var requestsToShow = response['Table_Data_1'];
                    pane = $(PageHandler.Global.print_target),
                    i = 1;

                    pane.html('');
                    _.each(requestsToShow, function (request) {
                        pane.append(T.templates.renderPrintRequests(request, i));
                        i++;
                    });

                    if (requestsToShow.length) {
                        table.show();

                        pane.find('.detail_link').unbind('click').on('click', function (e) {
                            var payload = $(this).data('payload');

                            //11/6/2016
                            var selectedTemplate = payload.title;
                            $("#HiddenField3").val(selectedTemplate);
                            var title = payload.title;
                            var mm = document.getElementById('Hidden1');
                            mm.value = title;

                            UserHandler.d.getTemplateRequestData(reportTemplate, payload.request_id, payload.request_trail_id, function (response) {
                                //console.log(response);
                                var refNo = response.Ref;
                                //var mm = document.getElementById('Hidden1');
                                //mm.value = refNo;
                                //var airtel = document.getElementById('airtel-0');
                                // airtel.innerHTML = refNo;
                                document.getElementById('airtel-0').innerHTML = refNo;
                                document.getElementById('name_of_contractor').innerHTML = response["name of contractor"];
                                document.getElementById('address_of_contractor').innerHTML = response["address of contractor"];
                                document.getElementById('contract_desc').innerHTML = response["contract description"];
                                document.getElementById('guraantee_amount_in_figure').innerHTML = response["fbn guaranteed amount(in figure)"];
                                document.getElementById('guraantee_amount_in_words').innerHTML = response["fbn guaranteed amount(in words)"];
                                document.getElementById('aps_figure').innerHTML = response["aps amount(in figure)"];
                                document.getElementById('aps_word').innerHTML = response["aps amount(in word)"];
                                document.getElementById('nddc-10').innerHTML = response["sign date"];
                                document.getElementById('iata-3').innerHTML = response["total amount payable(in figure)"];
                                document.getElementById('iata-4').innerHTML = response["total amount payable(in words)"]; //
                                document.getElementById('iata-5').innerHTML = response["offer start date"]; ///iata-6
                                document.getElementById('iata-6').innerHTML = response["expiry date"];
                                document.getElementById('iata-7').innerHTML = response["sign date"];


                                //bidbond
                                document.getElementById('bid-1').innerHTML = refNo;
                                document.getElementById('bid-2').innerHTML = response["name of employer"];
                                document.getElementById('bid-3').innerHTML = response["address of employer"];
                                document.getElementById('bid-4').innerHTML = response["name of contractor"];
                                document.getElementById('bid-5').innerHTML = response["address of contractor"];
                                document.getElementById('bid-6').innerHTML = response["bid number/lot number"];
                                document.getElementById('bid-8').innerHTML = response["guaranteed amount(in figure)"];
                                document.getElementById('bid-9').innerHTML = response["guaranteed amount(in word)"];
                                document.getElementById('bid-10').innerHTML = response["expiry date"];
                                document.getElementById('bid-11').innerHTML = response["sign date"];
                                document.getElementById('bid-12').innerHTML = response["tenor of guarantee"];
                                document.getElementById('bid-13').innerHTML = response["contract description"];


                                //draftcustom d-1
                                document.getElementById('d-1').innerHTML = response["description of goods"];
                                document.getElementById('d-2').innerHTML = response["(form m) no"];
                                document.getElementById('d-3').innerHTML = response["import duty payable (amount in figure)"];
                                document.getElementById('d-4').innerHTML = response["import duty payable (amount in word)"];
                                document.getElementById('d-5').innerHTML = response["custom bond amount(in figure)"];
                                document.getElementById('d-6').innerHTML = response["custom bond (amount in word)"];
                                document.getElementById('d-7').innerHTML = response["fbn sum payable (amount in figure)"];
                                document.getElementById('d-8').innerHTML = response["fbn sum payable (amount in words)"];
                                document.getElementById('d-9').innerHTML = response["fbn limitation (amount in figure)"];
                                document.getElementById('d-10').innerHTML = response["fbn limitation (amount in words)"];
                                document.getElementById('d-11').innerHTML = response["offer start date"];

                                if (response["tenor of guarantee"] === "") {

                                } else {
                                    document.getElementById('d-12').innerHTML = response["tenor of guarantee"];
                                }
                                if (response["sign date"] === "") {

                                } else {

                                    document.getElementById('d-13').innerHTML = response["sign date"];
                                }

                                //MTN
                                //sign date
                                document.getElementById('mtn-2').innerHTML = response["sign date"];


                                //EMT
                                document.getElementById('emt-1').innerHTML = response["name of bank"];
                                document.getElementById('emt-2').innerHTML = response["address of bank"];
                                document.getElementById('emt-3').innerHTML = response["total amount payable(in figure)"];
                                document.getElementById('emt-4').innerHTML = response["total amount payable(in word)"];
                                document.getElementById('emt-5').innerHTML = response["emts tell phone no"];
                                document.getElementById('emt-6').innerHTML = response["name of guarantor"];
                                document.getElementById('emt-7').innerHTML = response["tell no of guarantor"];
                                document.getElementById('emt-8').innerHTML = response["guarantor fax"];
                                document.getElementById('emt-9').innerHTML = response["guarantor attention"];
                                document.getElementById('emt-10').innerHTML = response["guarantor email"];
                                document.getElementById('emt-11').innerHTML = response["emts email"];

                                document.getElementById('emt-12').innerHTML = response["company address"];
                                document.getElementById('emt-13').innerHTML = response["company tell no."];
                                document.getElementById('emt-14').innerHTML = response["company fax"];
                                document.getElementById('emt-15').innerHTML = response["company attention"];
                                document.getElementById('emt-16').innerHTML = response["company email"];
                                document.getElementById('emt-17').innerHTML = response["sign date"];
                                //document.getElementById('emt-18').innerHTML = response["emts email"];
                                //emts tell phone no
                                var i = 0;
                                for (var a in response) {
                                    $('.' + reportTemplate + '-' + i).text(response[a]);

                                    i++;
                                }
                                // switch(reportTemplate) {
                                //     case 'airtel':

                                //     break;
                                // }
                                // Show the Letter Now and Process the Substitutions
                                table.hide();
                                $('#' + reportTemplate).show();

                            });

                        });
                    }

                    //$(PageHandler.Global.print_target).html('<h2 class="text-danger">'+response+"</h2>");
                });
            });
        });

        e.preventDefault();
        return false;
    });

    /*****************************************************/





    // Binding For Corporate Print Link in Side Menu
    $(PageHandler.Global.corporate_print_link).unbind('click').on('click', function (e) {
        $(PageHandler.Global.page_title).html($(this).text());
        PageHandler.markSelectedItem(PageHandler.Global.corporate_print_link, $(this));

        $('.theLetter').hide();

        PageHandler.Handles.loadREPORTS(PageHandler.Global.user_page, PageHandler.Resources.corporate_print_home, function (d) {
            var table = $('#corporate-print-table');
            table.hide();

            // Bind Evt Handlers
            $(PageHandler.Global.corporate_print_links).find('a').unbind('click').on('click', function (e) {

                var reportTemplate = $(this).data('template');
                table.hide();
                $('.theLetter').hide();

                // Get Requests for the selected Report Template
                UserHandler.d.getTemplateRequests(reportTemplate, function (response) {

                    var requestsToShow = response['Table_Data_1'];
                    pane = $(PageHandler.Global.corporate_print_target),
                    i = 1;

                    pane.html('');
                    _.each(requestsToShow, function (request) {
                        pane.append(T.templates.renderPrintRequests(request, i));
                        i++;
                    });

                    if (requestsToShow.length) {
                        table.show();

                        pane.find('.detail_link').unbind('click').on('click', function (e) {
                            var payload = $(this).data('payload');

                            var title = payload.title;
                            var mm = document.getElementById('Hidden1');
                            mm.value = title;

                            UserHandler.d.getTemplateRequestData(reportTemplate, payload.request_id, payload.request_trail_id, function (response) {

                                document.getElementById('insp-00').innerHTML = response["address of customer"];
                                document.getElementById('b-1').innerHTML = response.day;
                                document.getElementById('b-2').innerHTML = response.month;
                                document.getElementById('b-3').innerHTML = response.year;
                                document.getElementById('pledge-1').innerHTML = response["stock description"];
                                document.getElementById('pledge-000').innerHTML = response["stock description"];
                                document.getElementById('pledge-3').innerHTML = response["offer date"];
                                document.getElementById('pledge-4').innerHTML = response["type of facility"];
                                document.getElementById('pledge-5').innerHTML = response["name of customer"];
                                document.getElementById('pledge-6').innerHTML = response["address of customer"];
                                document.getElementById('pledge-11').innerHTML = response["sign (day)"] + " " + response["sign (month)"] + " " + response["sign (year)"];
                                document.getElementById('pledge-12').innerHTML = response["name of customer"];
                                document.getElementById('loan_biz-9').innerHTML = response["address of registered business name"];

                                document.getElementById('lop-0').innerHTML = response["Amount"];

                                //type of facility
                                document.getElementById('loan_biz-10').innerHTML = response["type of facility"];

                                document.getElementById('loan_biz-11').innerHTML = response["amount of facility in figures"];

                                document.getElementById('loan_biz-12').innerHTML = response["amount of facility in words"];

                                document.getElementById('loan_biz-13').innerHTML = response["tenor of facility in figures"];

                                document.getElementById('loan_biz-14').innerHTML = response["tenor of facility in words"];

                                document.getElementById('loan_biz-16').innerHTML = response["interest rate (%)"];

                                document.getElementById('loan_biz-15').innerHTML = response["period (days, month, years)"];
                                document.getElementById('loan_biz-17').innerHTML = response["date of offer letter"];
                                document.getElementById('loan_biz-18').innerHTML = response["repayment frequency (daily, monthly, others)"];


                                //name of warehousing agent

                                document.getElementById('warehouse-1').innerHTML = response["name of warehousing agent"];
                                document.getElementById('warehouse-2').innerHTML = response["name of customer"];
                                document.getElementById('warehouse-3').innerHTML = response["day"];
                                document.getElementById('warehouse-4').innerHTML = response["month"];
                                document.getElementById('warehouse-5').innerHTML = response["year"];
                                document.getElementById('warehouse-6').innerHTML = response["name of warehousing agent"];
                                document.getElementById('warehouse-7').innerHTML = response["address of warehousing agent"];
                                document.getElementById('warehouse-9').innerHTML = response["address of customer"];
                                document.getElementById('warehouse-10').innerHTML = response["purpose of the facility"];
                                document.getElementById('warehouse-1212').innerHTML = response["name of customer"];
                                document.getElementById('warehouse-11').innerHTML = response["address of warehouse"];
                                document.getElementById('warehouse-12').innerHTML = response["description of the goods"];


                                document.getElementById('warehouse-1111').innerHTML = response["name of warehousing agent"];
                                document.getElementById('warehouse-2222').innerHTML = response["name of customer"];

                                document.getElementById('receipt_biz-111').innerHTML = response["day"];
                                document.getElementById('receipt_biz-222').innerHTML = response["month"];
                                document.getElementById('receipt_biz-333').innerHTML = response["year"];

                                document.getElementById('receipt_llc-4').innerHTML = response["address of customer"];
                                document.getElementById('receipt_llc-5').innerHTML = response["day"];
                                document.getElementById('receipt_llc-6').innerHTML = response["month"];
                                document.getElementById('receipt_llc-7').innerHTML = response["year"];


                                document.getElementById('loan_ind-13').innerHTML = response["Agreement (day)"];
                                document.getElementById('loan_ind-2').innerHTML = response["name of customer"];
                                document.getElementById('loan_ind-22').innerHTML = response["address of customer"];
                                document.getElementById('loan_ind-17').innerHTML = response["name of legal officer"];

                                document.getElementById('loan_llc-22').innerHTML = response["name of legal officer"];//date of offer letter
                                document.getElementById('loan_llc-11').innerHTML = response["date of offer letter"];
                                document.getElementById('loan_llc-16').innerHTML = response["interest rate (%)"];
                                //loan_llc-22
                                //loan_ind - 17

                                //insp_agreement-9

                                var i = 0;
                                for (var a in response) {
                                    $('.' + reportTemplate + '-' + i).text(response[a]);

                                    i++;
                                }
                                // switch(reportTemplate) {
                                //     case 'airtel':

                                //     break;
                                // }
                                // Show the Letter Now and Process the Substitutions
                                table.hide();
                                $('#' + reportTemplate).show();

                            });

                        });
                    }

                    //$(PageHandler.Global.print_target).html('<h2 class="text-danger">'+response+"</h2>");
                });
            });
        });

        e.preventDefault();
        return false;
    });

    /*****************************************************/









    // Binding For Perfection Print Link in Side Menu
    $(PageHandler.Global.perfection_print_link).unbind('click').on('click', function (e) {
        $(PageHandler.Global.page_title).html($(this).text());
        PageHandler.markSelectedItem(PageHandler.Global.perfection_print_link, $(this));

        $('.theLetter').hide();

        PageHandler.Handles.loadREPORTS(PageHandler.Global.user_page, PageHandler.Resources.perfection_print_home, function (d) {
            var table = $('#perfection-print-table');
            table.hide();

            // Bind Evt Handlers
            $(PageHandler.Global.perfection_print_links).find('a').unbind('click').on('click', function (e) {

                var reportTemplate = $(this).data('template');
                table.hide();
                $('.theLetter').hide();

                // Get Requests for the selected Report Template
                UserHandler.d.getTemplateRequests(reportTemplate, function (response) {

                    var requestsToShow = response['Table_Data_1'];
                    pane = $(PageHandler.Global.perfection_print_target),
                    i = 1;

                    pane.html('');
                    _.each(requestsToShow, function (request) {
                        pane.append(T.templates.renderPrintRequests(request, i));
                        i++;
                    });

                    if (requestsToShow.length) {
                        table.show();

                        pane.find('.detail_link').unbind('click').on('click', function (e) {
                            var payload = $(this).data('payload');
                            var title = payload.title;
                            var mm = document.getElementById('Hidden1');
                            mm.value = title;


                            UserHandler.d.getTemplateRequestData(reportTemplate, payload.request_id, payload.request_trail_id, function (response) {

                                //console.log(response);
                                document.getElementById('mort_comp-2').innerHTML = response["borrowers address"];
                                document.getElementById('mort_comp-24').innerHTML = response["borrower's name"];
                                document.getElementById('mort_comp-8').innerHTML = response["amount in figures"];
                                document.getElementById('mort_comp-9').innerHTML = response["amount in words"];
                                document.getElementById('mort_comp-4').innerHTML = response["borrower's name"];
                                document.getElementById('mort_comp-23').innerHTML = response["month"];
                                document.getElementById('tlm_corp_corp-8').innerHTML = response["borrowers address"];
                                document.getElementById('tlm_corp_corp-10').innerHTML = response["surety address"];
                                //tlm_corp_corp - 10
                                //mort_comp-2tlm_corp_corp-8
                                var refNo = response.Ref;
                                //                                var mm = document.getElementById('Hidden1');
                                //                                mm.value = refNo;
                                var i = 0;
                                for (var a in response) {
                                    $('.' + reportTemplate + '-' + i).text(response[a]);

                                    i++;
                                }
                                // switch(reportTemplate) {
                                //     case 'airtel':

                                //     break;
                                // }
                                // Show the Letter Now and Process the Substitutions
                                table.hide();
                                $('#' + reportTemplate).show();

                            });

                        });
                    }

                    //$(PageHandler.Global.print_target).html('<h2 class="text-danger">'+response+"</h2>");
                });
            });
        });

        e.preventDefault();
        return false;
    });

    /*****************************************************/



    PageHandler.Handles.b("#start_stage").unbind('click').on('click', function (e) {

    }); //save_comment
    PageHandler.Handles.b("#save_comment").unbind('click').on('click', function (e) {
        var pid = $(this).data("process_id");
        var _stage_id = $(this).data("stage_id");
        var req_id = $(this).data("request_id");
        var _message = $("#comment_box").val();
        if (_message.trim().length > 0) {
            UserHandler.d.makeComment({
                stage_id: _stage_id,
                message: _message,
                process_id: pid,
                request_id: req_id
            }, function (response) {
                $("#comment_box").val("");
                UserHandler.d.getComment(req_id, function (resp) {
                    $("#comment_list").html("");
                    //if (resp.status && resp.data.status == 1) {
                    if (resp.data.data.length > 0) {
                        $.each(resp.data.data, function (i, v) {
                            //resp.data.data.forEach(function (v, i) {
                            $("#comment_list").append(T.templates.renderComment(v));
                        });
                    } else {
                        $("#comment_list").html("No comments");
                    }
                    //} else {
                    //  $("#comment_list").html("Failed to load comments");
                    //}
                });
            });
        } else {
            alert("Empty comment not allowed");
        }

    });

    PageHandler.Handles.b(".request_detail_link").unbind('click').on('click', function (e) {
        var req_id = $(this).data('id');
        var data = $(this).data('payload');
        //console.log(data);
        var link = "../Components/user_requestdetail.htm?rand=" + randomString(); //user_page

        if (data) {
            var today = new Date();
            var birth = new Date(data.created_date);
            var age = Math.ceil((today - birth) / 1000);
            var span = Math.ceil(data.lifespan / 60);
            var ttl = span - age;


            PageHandler.Handles.load(PageHandler.Global.user_page, link, function (d) {

                //alert(data.status);
                if (data.status == 6) {
                    $(".age-analysis").addClass("hidden");
                    $("#treat_request").hide();
                    $("#comment_area").hide();
                    $(".attach_doc").hide();
                    $("#page-title").html("<button class='btn btn-xs btn-success pull-right'  type='button'><strong style='font-family:verdana;'>Request Title : " + data.title + "</strong></button>");
                    $("#stage_user").html("<h3>Completed</h3>");

                }
                else if (data.status != 6) {
                    console.log(data);
                    // alert(data.stage_role_id);
                    // alert(UserHandler.user.role_id);
                    //alert("ssfsf");
                    if ((UserHandler.user.role_id == data.stage_role_id)) { //&& (UserHandler.user.id == data.current_user_id)
                        $("#treat_request").removeClass("hidden");
                        $("#treat_request").attr("data-process_id", data.process_id);
                        $("#treat_request").attr("data-form_id", data.stage_form_id);
                        $("#treat_request").attr("data-payload", JSON.stringify(data));
                        $(".attach_doc").removeClass("hidden");
                        $(".attach_doc").attr("data-id", req_id);
                        $("#req_title").html("<b>Request Title : </b>" + " " + data.title + "<br/>");
                        // $("#req_title").html("<b>Request Title : </b>" + " " + data.title); //bind request title
                        //var center = ;
                        $("#page-title").append("<button class='btn btn-xs btn-success pull-right'  type='button'><strong style='font-family:verdana;'>Request Title : " + data.title + "</strong></button>");
                        PageHandler.Handles.b(PageHandler.Global.attach_doc).unbind('click').on('click', function (e) {

                            var id = req_id;
                            var evt = $.Event("keypress");
                            evt.keyCode = 17;
                            evt.ctrlKey = true;

                            $(PageHandler.Global.attach_doc).trigger(evt);
                            window.location = PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=2";
                            // window.open(PageHandler.Resources.base_path + "RM/upload.aspx?request=" + id + "&r=2", 'Upload', 'toolbar=yes, scrollbars=yes, resizable=yes, top=50, left=500, width=400, height=400');
                        });
                    } else {
                        $("#treat_request").unbind('click').addClass("hidden");
                        //$("#comment_area").addClass("hidden");
                        $(".attach_doc").unbind('click').addClass("hidden");
                        if (data.status == 6) {
                            //alert("eee");
                            //  $("#req_title").removeClass('hidden').html("<b>Request Title : </b>" + " " + data.title + "<br/>");
                            $("#page-title").append("<button class='btn btn-xs btn-success pull-right'  type='button'><strong style='font-family:verdana;'>Request Title : " + data.title + "</strong></button>");
                            $("#stage_user").html("<h3>Completed</h3>");
                            $(".age-analysis").addClass("hidden");
                        } else {
                            // $("#req_title").removeClass('hidden').html("<b>Request Title : </b>" + " " + data.title + "<br/>"); //bind request title 
                            $("#page-title").append("<button class='btn btn-xs btn-success pull-right'  type='button'><strong style='font-family:verdana;'>Request Title : " + data.title + "</strong></button>");
                            $("#stage_user").html("<br/><br/><h3>Waiting for " + data.held_by.ucWords() + " - <span class='text-uppercase label label-danger'>" + data.current_stage + "</span></h3>");

                        }
                    }
                }
                // if()
                $("#comment_area").removeClass("hidden");
                $("#save_comment").attr("data-process_id", data.process_id);
                $("#save_comment").attr("data-request_id", req_id);
                $("#save_comment").attr("data-stage_id", data.stage_id);

                $("#life_span").html(secondstotime(span));
                $("#age").html(age);
                $("#ttl").html(ttl > 0 ? ttl : ttl + " (Past Expected Completion Date)");
                $("#accordion").html("Loading ...");
                $("#comment_list").html("Loading comments...");


                UserHandler.d.getRequestForms(req_id, function (response) {

                    if (response.status && response.data.status == 1) {
                        $("#accordion, #timeline").html("");
                        var theData = response.data.data;
                        theData = _.sortBy(theData, function (d) { return new Date(d.created_date).getTime(); }).reverse();
                        //theData.forEach(function (v, i) {
                        //    //$("#accordion").append(T.templates.formDataHistory(v));
                        //   $("#timeline").append(T.templates.formDataHistory(v));
                        //});

                        $.each(response.data.data, function (i, v) {
                            $("#timeline").append(T.templates.formDataHistory(v));
                        });
                        PageHandler.BindToDocument();
                        UserHandler.bindToDom();
                        $(".accord").eq(0).trigger('click');
                    } else {
                        //$("#requests_home").html("No Pending Request");
                    }
                });

                UserHandler.d.getResource(req_id, function (resp) {

                    $("#resource_list").html("");

                    //$('#page-title').html(_.find(UserHandler.menus, function(m){return m.report_id==report_id;}).report_name);

                    if (resp.status == 1) {



                        $.each(resp.data.data, function (i, v) {

                            $("#resource_list").append(T.templates.renderResourceItem(v));
                        });

                        //$("#resource_list").html("No Resource Attached");

                    } else {
                        $("#resource_list").html("Failed to load Resource");
                    }
                });

                UserHandler.d.getComment(req_id, function (resp) {
                    $("#comment_list").html("");
                    //alert(resp.data.status);

                    if (resp.data.data.length > 0) {
                        $.each(resp.data.data, function (i, v) {
                            //resp.data.data.forEach(function (v, i) {
                            $("#comment_list").append(T.templates.renderComment(v));
                        });
                    } else {
                        $("#comment_list").html("No comments");
                    }

                });


                PageHandler.BindToDocument();
                UserHandler.bindToDom();
            });
        }
    });


    //$(".save_update").on('click', function (e) {


    //    var check = $(e.target).data("loaded");

    //    var data = ($(e.target).data("payload"));
    //    var request_id = $(e.target).data("id");
    //    alert(request_id);
    //});


    $(".edit").on('click', function (e) {
        console.log(e);
        var target = $(this).data('target');
        var check = $(target).data("loaded");

        try {
            var data = ($(this).data("payload"));

            var request_id = $(target).data("id");
            //if (check == undefined) {
            $(target).attr("data-loaded", "1");
            var pane = $("#editModal").find('.modal-body');
            pane.html("<div class=''>Loading...</div>");
        

            UserHandler.d.getRequestFormsDataTrailRecord(data.id, function (resp) {

                if (resp.status && resp.data.status == 1) {
                    UserHandler.d.getRequestFormsDataDefinition(data.stage_form_id, function (form_def) {
                        pane.html("");
                        if (form_def.status && form_def.data.status == 1) {
                            var temp1 = {}; var atleast_one_data_loaded = false;
                            $.each(form_def.data.data, function (i, v) {

                                temp1[v.id] = v;
                            });
                            var temp2 = {};
                            $.each(resp.data.data.char, function (j, v1) {

                                try {
                                    pane.append(T.templates.renderDataTextFormat(temp1[v1.form_control_id], v1));
                                    atleast_one_data_loaded = true;
                                } catch (e) { }
                            });
                            $.each(resp.data.data.text, function (j, v1) {

                                try {
                                    pane.append(T.templates.renderDataTextFormat(temp1[v1.form_control_id], v1));
                                    atleast_one_data_loaded = true;
                                } catch (e) { }
                            });
                            $.each(resp.data.data.decimal, function (j, v1) {

                                try {
                                    pane.append(T.templates.renderDataTextFormat(temp1[v1.form_control_id], v1));
                                    atleast_one_data_loaded = true;
                                } catch (e) { }
                            });
                            $.each(resp.data.data.int, function (j, v1) {

                                try {
                                    pane.append(T.templates.renderDataTextFormat(temp1[v1.form_control_id], v1));
                                    atleast_one_data_loaded = true;
                                } catch (e) { }
                            });
                            $.each(resp.data.data.date, function (j, v1) {

                                try {
                                    pane.append(T.templates.renderDataTextFormat(temp1[v1.form_control_id], v1));
                                    atleast_one_data_loaded = true;
                                } catch (e) { }
                            });
                            if (!atleast_one_data_loaded) {
                                pane.html("<div class=''>No Content</div>");
                            }
                        } else {
                            pane.html("<div class=''>Invalid Request Data. History could not be loaded </div>");
                        }

                    });
                } else {
                    $(e.target).find('.panel-body').html("<div class=''>Invalid Request Data. History could not be loaded </div>");
                }


            });
        } catch (e) {
            $(target).find('.panel-body').html("<div class=''>Invalid Request Data. History could not be loaded </div>");
        }
        $('#editModal').modal('show');
    });

    $(".accord").on('click', function (e) {
        var target = $(this).data('target');
        var check = $(target).data("loaded");

        try {
            var data = ($(this).data("payload"));

            var request_id = $(target).data("id");
            if (check == undefined) {
                $(target).attr("data-loaded", "1");
                var pane = $(target).find('.panel-body');
                pane.html("<div class=''>Loading...</div>");
                if (typeof data.stage_form_id != "undefined" && data.stage_form_id != null) {

                    UserHandler.d.getRequestFormsDataTrailRecord(data.id, function (resp) {

                        if (resp.status && resp.data.status == 1) {
                            UserHandler.d.getRequestFormsDataDefinition(data.stage_form_id, function (form_def) {
                                pane.html("");
                                if (form_def.status && form_def.data.status == 1) {
                                    var temp1 = {}; var atleast_one_data_loaded = false;
                                    $.each(form_def.data.data, function (i, v) {

                                        temp1[v.id] = v;
                                    });
                                    var temp2 = {};
                                    $.each(resp.data.data.char, function (j, v1) {

                                        try {
                                            pane.append(T.templates.renderData(temp1[v1.form_control_id], v1));
                                            atleast_one_data_loaded = true;
                                        } catch (e) { }
                                    });
                                    $.each(resp.data.data.text, function (j, v1) {

                                        try {
                                            pane.append(T.templates.renderData(temp1[v1.form_control_id], v1));
                                            atleast_one_data_loaded = true;
                                        } catch (e) { }
                                    });
                                    $.each(resp.data.data.decimal, function (j, v1) {

                                        try {
                                            pane.append(T.templates.renderData(temp1[v1.form_control_id], v1));
                                            atleast_one_data_loaded = true;
                                        } catch (e) { }
                                    });
                                    $.each(resp.data.data.int, function (j, v1) {

                                        try {
                                            pane.append(T.templates.renderData(temp1[v1.form_control_id], v1));
                                            atleast_one_data_loaded = true;
                                        } catch (e) { }
                                    });
                                    $.each(resp.data.data.date, function (j, v1) {

                                        try {
                                            pane.append(T.templates.renderData(temp1[v1.form_control_id], v1));
                                            atleast_one_data_loaded = true;
                                        } catch (e) { }
                                    });
                                    if (!atleast_one_data_loaded) {
                                        pane.html("<div class=''>No Content</div>");
                                    }
                                } else {
                                    pane.html("<div class=''>Invalid Request Data. History could not be loaded </div>");
                                }

                            });
                        } else {
                            $(e.target).find('.panel-body').html("<div class=''>Invalid Request Data. History could not be loaded </div>");
                        }


                    });
                } else {
                    pane.html("<div class=''>Refer to Comment Area</div>");
                }

            } else {
                //$(e.target).find('.panel-body').html("<div class=''>Loaded</div>");
            }
        } catch (e) {
            $(target).find('.panel-body').html("<div class=''>Invalid Request Data. History could not be loaded </div>");
        }
    });

    $(".collapse ").on('show.bs.collapse', function (e) {


        var check = $(e.target).data("loaded");
        try {
            var data = ($(e.target).data("payload"));
            var request_id = $(e.target).data("id");

            if (check == undefined) {
                $(e.target).attr("data-loaded", "1");
                var pane = $(e.target).find('.panel-body');
                pane.html("<div class=''>Loading...</div>");
                if (typeof data.stage_form_id != "undefined" && data.stage_form_id != null) {
                    UserHandler.d.getRequestFormsDataTrailRecord(data.id, function (resp) {
                        if (resp.status && resp.data.status == 1) {
                            UserHandler.d.getRequestFormsDataDefinition(data.stage_form_id, function (form_def) {
                                pane.html("");
                                if (form_def.status && form_def.data.status == 1) {
                                    var temp1 = {}; var atleast_one_data_loaded = false;
                                    $.each(form_def.data.data, function (i, v) {

                                        temp1[v.id] = v;
                                    });

                                    var temp2 = {};
                                    $.each(resp.data.data.char, function (j, v1) {

                                        try {
                                            pane.append(T.templates.renderData(temp1[v1.form_control_id], v1));
                                            atleast_one_data_loaded = true;
                                        } catch (e) { }
                                    });
                                    $.each(resp.data.data.text, function (j, v1) {

                                        try {
                                            pane.append(T.templates.renderData(temp1[v1.form_control_id], v1));
                                            atleast_one_data_loaded = true;
                                        } catch (e) { }
                                    });
                                    $.each(resp.data.data.decimal, function (j, v1) {

                                        try {
                                            pane.append(T.templates.renderData(temp1[v1.form_control_id], v1));
                                            atleast_one_data_loaded = true;
                                        } catch (e) { }
                                    });
                                    $.each(resp.data.data.int, function (j, v1) {

                                        try {
                                            pane.append(T.templates.renderData(temp1[v1.form_control_id], v1));
                                            atleast_one_data_loaded = true;
                                        } catch (e) { }
                                    });
                                    $.each(resp.data.data.date, function (j, v1) {

                                        try {
                                            pane.append(T.templates.renderData(temp1[v1.form_control_id], v1));
                                            atleast_one_data_loaded = true;
                                        } catch (e) { }
                                    });
                                    if (!atleast_one_data_loaded) {
                                        pane.html("<div class=''>No Content</div>");
                                    }
                                } else {
                                    pane.html("<div class=''>Invalid Request Data. History could not be loaded </div>");
                                }

                            });
                        } else {
                            $(e.target).find('.panel-body').html("<div class=''>Invalid Request Data. History could not be loaded </div>");
                        }


                    });
                } else {
                    pane.html("<div class=''>Refer to Comment Area</div>");
                }

            } else {
                //$(e.target).find('.panel-body').html("<div class=''>Loaded</div>");
            }
        } catch (e) {
            $(e.target).find('.panel-body').html("<div class=''>Invalid Request Data. History could not be loaded </div>");
        }
    });
    $('.collapse').collapse({
        toggle: false
    });
}