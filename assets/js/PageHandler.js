
String.prototype.capitalizeFirstLetter = function () {
    return this.ucWords(); //this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.ucWords = function() {
	  //   example 1: ucwords('kevin van  zonneveld');
	  //   returns 1: 'Kevin Van  Zonneveld'
	  //   example 2: ucwords('HELLO WORLD');
	  //   returns 2: 'Hello World'
    if (this !== null) {
	return (this + '').toLowerCase().replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1) {
		return $1.toUpperCase();
});
    }else{
    return this;
    }
}

function randomString() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}


function log(data) {

  
}
function secondstotime(secs) {
    var t = new Date(1970, 0, 1);
    t.setSeconds(secs);
    var s = t.toTimeString().substr(0, 8);
    if (secs > 86399)
        s = Math.floor((t - Date.parse("1/1/70")) / 3600000) + s.substr(2);
    return s;
}

$.fn.serializeAssoc = function () {
    
};
$.fn.serializeObject = function () {
    
};

(function($){
    $.fn.serializeAssoc = function() {
      var data = {};
        $.each(this.serializeArray(), function (key, obj) {
            var a = obj.name.match(/(.*?)\[(.*?)\]/);
            if (a !== null) {
                var subName = a[1];
                var subKey = a[2];
                if (!data[subName]) data[subName] = [];
                if (data[subName][subKey]) {
                    if ($.isArray(data[subName][subKey])) {
                        data[subName][subKey].push(obj.value);
                    } else {
                        data[subName][subKey] = [];
                        data[subName][subKey].push(obj.value);
                    }
                } else {
                    data[subName][subKey] = obj.value;
                }
            } else {
                if (data[obj.name]) {
                    if ($.isArray(data[obj.name])) {
                        data[obj.name].push(obj.value);
                    } else {
                        data[obj.name] = [];
                        data[obj.name].push(obj.value);
                    }
                } else {
                    data[obj.name] = obj.value;
                }
            }
        });
        return data;
    };
    $.fn.serializeObject = function() {
      var data = {};
        $.each(this.serializeArray(), function (key, obj) {
            var a = obj.name.match(/(.*?)\[(.*?)\]/);
            if (a !== null) {
                var subName = new String(a[1]);
                var subKey = new String(a[2]);
                if (!data[subName]) data[subName] = {};
                if (data[subName][subKey]) {
                    if ($.isArray(data[subName][subKey])) {
                        data[subName][subKey].push(obj.value);
                    } else {
                        data[subName][subKey] = {};
                        data[subName][subKey].push(obj.value);
                    };
                } else {
                    data[subName][subKey] = obj.value;
                };
            } else {
                var keyName = new String(obj.name);
                if (data[keyName]) {
                    if ($.isArray(data[keyName])) {
                        data[keyName].push(obj.value);
                    } else {
                        data[keyName] = {};
                        data[keyName].push(obj.value);
                    };
                } else {
                    data[keyName] = obj.value;
                };
            };
        });
        return data;
    };
  })(jQuery);







var PageHandler = {};
PageHandler.Files = {
    "image/jpeg": ".jpg",
    "text/plain": ".txt",
    "application/vnd.ms-excel": ".xls",
    "application/pdf": ".pdf",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": ".xlsx",
    "application/msword": ".doc",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document": ".docx",
    "application/vnd.openxmlformats-officedocument.presentationml.presentation": ".pptx",
    "application/vnd.ms-powerpoint": ".ppt"
};
var FormRenderer = {
    renderNumberBox: function (label, identity, containerclass, cssclass, placeholder, data) {
        var str = "<div class='" + containerclass + "'>\
                    <label for='" + identity + "'>" + label.ucWords() + "</label>\
                    <input type='number' class='" + cssclass + "' value='" + data + "' id='d[" + identity + "]' name='d[" + identity + "]' placeholder='" + placeholder.ucWords() + "' min='0' max='9999999999999999' maxlength='16'>\
                  </div>";
        return str;
    },
    renderTextBox: function (label, identity, containerclass, cssclass, placeholder, data) {
        var str = "<div class='" + containerclass + "'>\
					<label for='" + identity + "'>" + label.ucWords() + "</label>\
					<input maxlength='250' type='text' class='" + cssclass + "' value='" + data + "' id='d[" + identity + "]' name='d[" + identity + "]' placeholder='" + placeholder.ucWords() + "'>\
				  </div>";
        return str;
    },
    renderTextArea: function (label, identity, containerclass, cssclass, placeholder, data) {
        var str = "<div class='" + containerclass + "'>\
					<label for='" + identity + "'>" + label.ucWords() + "</label>\
					<textarea style='height: 100px;' class='" + cssclass + "' value='" + data + "' id='d[" + identity + "]' name='d[" + identity + "]' placeholder='" + placeholder.ucWords() + "'></textarea>\
				  </div>";
        return str;
    },
    renderHiddenData: function (dataArray) {
        var str = "";
        for (var d in dataArray) {
            str += "<input type='hidden' name='c[" + d + "]' value='" + dataArray[d] + "'>";
        }
        return str;
    },
    renderRadio: function (label, identity, containerclass, cssclass, placeholder, data) {

        var str = "<div class='" + containerclass + "'><label for='" + identity + "'>" + label.ucWords() + "</label><br/>";

        if (typeof data != 'undefined') {
            $.each(data, function(i, v){
           
                str += "<label>\
									  <input name='d[" + identity + "]' type='radio' value='" + v.label.toLowerCase() + "'>&nbsp;&nbsp;" + v.label.toUpperCase() + "</label> <br/>";

            });
        }
        str += "</div>";
        return str;
    },
    renderCheckBox: function (label, identity, containerclass, cssclass, placeholder, data) {
      

        var str = "<div class='" + containerclass + "'><label for='" + identity + "'>" + label.ucWords() + "</label><br/>";

        if (typeof data != 'undefined') {
            $.each(data, function(i, v){
           
                str += "<label>\
									  <input name='d[" + identity + "][]' type='checkbox' value='" + v.label.toUpperCase() + "'>&nbsp;&nbsp;" + v.label.toUpperCase() + "</label> <br/>";

            });
        }
        str += "</div>";

      
        return str;
    },
    renderDropdown: function (label, identity, containerclass, cssclass, placeholder, data) {

        var str = "<div class='" + containerclass + "'><label for='" + identity + "'>" + label.ucWords() + "</label><br/><select class='" + cssclass + "' id='d[" + identity + "]' name='d[" + identity + "]'>";

        if (typeof data != 'undefined') {
            str += "<option value='-1'>Select...</option>";
            $.each(data, function(i, v){
           
                str += "<option value='" + v.label.toUpperCase() + "'>" + v.label.toUpperCase() + "</option>";

            });
        }
        str += "</select></div>";
        return str;
    },
    renderDateTimePicker: function (label, identity, containerclass, cssclass, placeholder, data, is_special) {
        
        var set_in_special_section = typeof is_special != "undefined" ? is_special : true;
        var section_lbl = set_in_special_section == true ? "c" : "d";
        var picker_selector = set_in_special_section == true ? "datetimepicker" : "datetimepicker2";
        var str = "<br/><div class='" + containerclass + "'>\
					<label for='" + identity + "'>" + label.ucWords() + "</label>\
					<input type='text' readonly class='" + picker_selector + " form-control" + cssclass + "' value='" + data + "' id='" + section_lbl + "[" + identity + "]' name='" + section_lbl + "[" + identity + "]' placeholder='" + placeholder + "'>\
				  </div>";
        return str;
    },
    renderFileUpload: function (label, identity, containerclass, cssclass, placeholder, data) {
        var str = "<div class='" + containerclass + "'>\
					<label for='" + identity + "'>" + label.ucWords() + "</label>\
					<input type='file' class='" + cssclass + "' value='" + data + "' id='d[" + identity + "]' name='d[" + identity + "]' placeholder='" + placeholder + "'>\
				  </div>";
        return str;
    },
    renderNote: function (note) {
        return "<div class='alert alert-info capitalize'>" + note.ucWords() + "</div>";
    },
    renderAddUpload: function (request_id, extra_class) {
        var extra_class_ref = typeof extra_class == "undefined" ? "" : extra_class;
        return "<button type='button' data-id='" + request_id + "' href='#' class='btn btn-warning pull-right " + extra_class_ref + " attach_doc'>Attach Document</button><br/>";
    },
    renderStartOfContainer: function (identity, cssclass) {
        return "<div id='" + identity + "' name='" + identity + "' class='" + cssclass + "'>";
    },
        renderSpecialButtons: function (data, form) {
       
        form.append("<br/><div class='form-group clearfix' role='group' >");
        $.each(data, function(i, v){
       
            form.append("<input data-next_stage_role_id='" + v.next_stage_role_id + "' data-stage_id='" + v.next_stage_id + "' class='btn control_lbl_btn btn-primary trigga' type='button' name='s[" + v.next_id + "]' value='" + v.next_stage_name.capitalizeFirstLetter() + "' /><br/><br/>");
        });
    },

    renderEndOfContainer: function (can_attach_form, render_buttons, request_id) {
        var chk = (typeof render_buttons == "undefined") ? true : render_buttons;
        var ls = '';
        
        if (chk) {
            ls += "<br/><div class='btn-group clearfix main_btn hidden' role='group' >\
				  <button type='submit' class='btn btn-primary trigga_proceed'>Submit</button></div>"; //<button type='reset' class='btn btn-default resetform'>Reset</button>\
            ls += "<div id='sendingmail' style='width:100%;font-family:verdana, sans-serif;font-size:15px;color:#ff0000'></div>";
        }

        if (typeof can_attach_form != "undefined" && can_attach_form) {
            var req = (typeof request_id == "undefined") ? 0 : request_id;
            ls += FormRenderer.renderAddUpload(req);
        }
        //ls += "</div>";
        return ls;
    }
}
PageHandler.render = function (element, data, formObj, can_attach_form, hidden_data, before_render_close, after_render_fxn, render_buttons, request_id) {

    

    var _form = $(element);
    var _request_id = (typeof request_id == "undefined") ? 0 : request_id;
    _form.html('');
    _form.append(FormRenderer.renderStartOfContainer('', ''));
    _form.append(FormRenderer.renderNote(formObj.description));
    if (typeof hidden_data != "undefined") {
        _form.append(FormRenderer.renderHiddenData(hidden_data));
    }

   
   
    $.each(data, function (i, v) {
        //data.forEach(function (v, i) {
        if (typeof UserHandler != "undefined" && typeof UserHandler.currentFormData != "undefined") {
            UserHandler.currentFormData[v.id] = v;
        }
        
        var dt;
        switch (v.type) {
            case 1:
                _form.append(FormRenderer.renderTextBox(v.label, v.id, 'form-group', 'form-control', v.label, ''));
                break;
            case 4:
                _form.append(FormRenderer.renderNumberBox(v.label, v.id, 'form-group', 'form-control', v.label, ''));
                break;
            case 7:
                if (v.data != "null") {
                    _form.append(FormRenderer.renderRadio(v.label, v.id, 'form-group', '', v.label, (JSON.parse(v.data)).options));
                }
                dt = JSON.parse(v.data).include_other_option;
               // console.log(v.id);
                if (dt === true) {
                    _form.append("<label id='handleotherslabel'><input type=radio value='others' data-id='"+v.id+"' id='handleOthers' class='handleOthers'>&nbsp;&nbsp;</input>OTHERS</label>");
                }
                break;
            case 8:
                if (v.data != "null") {
                    _form.append(FormRenderer.renderCheckBox(v.label, v.id, 'form-group', '', v.label, (JSON.parse(v.data)).options));
                }
                dt = JSON.parse(v.data).include_other_option;
                if (dt === true) {
                    //                    _form.append(FormRenderer.renderTextBox(v.label, v.id, 'form-group', 'form-control', v.label, ''));
                    _form.append("<label id='handleotherslabel'><input type=radio value='others' data-id='" + v.id + "' id='handleOthers' class='handleOthers'>&nbsp;&nbsp;</input>OTHERS</label>");
                }
                break;
            case 'file':
                _form.append(FormRenderer.renderFileUpload(v.label, v.id, 'form-group', '', v.label, ''));
                break;
            case 5:
                _form.append(FormRenderer.renderTextArea(v.label, v.id, 'form-group', 'form-control', v.label, ''));
                break;
            case 2:
                _form.append(FormRenderer.renderDateTimePicker(v.label, v.id, "", "", "Click to insert date and time", "", false));
                break;
            case 6:
                if (v.data != "null") {
                    _form.append(FormRenderer.renderDropdown(v.label, v.id, 'form-group', 'form-control', v.label, (JSON.parse(v.data)).options));
                }
               
                break;

        }

    });
    if (typeof before_render_close == "function") {
        var t = $("<div id='special_controls'></div>");
        _form.append(t);
        before_render_close(t);
        //alert(_form.html());
    }

    _form.append(FormRenderer.renderEndOfContainer(can_attach_form, render_buttons, _request_id));
    //alert(_form.html());
    var tempfxn = _form.onsubmit;
    _form.unbind('submit').on('submit', function (e) {
        var x = $(this).serializeArray();
        if (typeof tempfxn == "function") {
            tempfxn(x);
        }
        e.preventDefault();
        return false;
    });

    if (typeof after_render_fxn == "function") {
        after_render_fxn(_form);

    }
}
PageHandler.form = {
    descriptions: {}
}
var Form = function () {
    var that = this;
    this.name = "";
    this.description = "";
    this.control_data = null;
    this.packageControlData = function (data) {
        
        var objArr = [];
        /*var fieldType = {
        "text": "1",
        "paragraph": "5",
        "checkboxes": "8",
        "radio": "7",
        "date": "2",
        "dropdown": "6",
        "time": "",
        "number": "",
        "website": "",
        "file": "",
        "email": "",
        "price": "",
        "address": ""
        };*/
        var fieldType = {
            "text": "1",
            "paragraph": "5",
            "checkboxes": "8",
            "radio": "7",
            "date": "2",
            "dropdown": "6",
            "number": "4",
            "price": "3"
        };
        var controlObj = function () {
            this.type = "";
            this.label = "";
            this.note = "";
            this.is_required = 0;
            this.has_multiple = 0;
            this.data = null;
        };
        var temp_obj = null, temp_data = null;
        $.each(data.fields, function(i, v){
       // data.fields.forEach(function (v, i) {
           
            switch (v.field_type) {
                case 'checkboxes':
                    temp_data = v.field_options;
                    break;
                case 'radio':
                    temp_data = v.field_options;
                    break;
                case 'dropdown':
                    temp_data = v.field_options;
                    break;
                case 'databound':
                    temp_data = v.field_options;
                    break;

            }
            temp_obj = new controlObj();
            temp_obj.type = (typeof fieldType[v.field_type] != "undefined") ? fieldType[v.field_type] : "1";
            temp_obj.label = v.label;
            temp_obj.note = (typeof v.field_options.description != "undefined") ? v.field_options.description : "";
            temp_obj.data = JSON.stringify(temp_data);
            objArr.push(temp_obj);
        });
        that.control_data = JSON.stringify(objArr);
    }
    this.save = function (callback) {
        if (that.control_data != null) {
            PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Form.ashx?rand="+randomString()+"&a=add", { name: that.name, description: that.description, control_data_list: that.control_data }, PageHandler.HTTPMethods.POST, function (response) {
                if (typeof callback == "function") {
                    callback(response);
                }
            });
        }
    }
}
function Stage() {
    var that = this;
    this.name = "";
    this.description = "";
    this.process_id = 0;
    this.role_id = 0;
    this.type = 0;
    this.save = function (callback) {
        PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Process.ashx?rand=" + randomString() + "&a=add_stage",
         {
             name: that.name,
             description: that.description,
             process_id: that.process_id,
             role_id: that.role_id, 
             type: that.type 
         }, PageHandler.HTTPMethods.POST, function (response) {
            if (typeof callback == "function") {
                callback(response);
            }
        });
    }   
}
PageHandler.tempData = {
    checkedBranches:[],
    formDesignData:null
}
function treeData() {
    this.id = 0;
    this.label = '';
    this.children = []
}   
PageHandler.Global = {
    adminPane: '#admin_page',
    user_page: '#user_page',
    trade_page: '#trade_marks',
    view_branches: '.branch_link',
    role_link: '#role_link',
    view_users:'.user_link',
    branch_refresh: '#branch_refresh',
    process_link: '.process_link',
    process_list:'#process_list',
    branch_name: '#branch_name',
    street: '#street',
    town: '#town',
    state: '#state',
    region: '#region',
    country: '#country',
    branch_type: '#branch_type',
    page_alert:'.page_alert',
    branch_select: '#branch_select',
    role_refresh: '#role_refresh',
    form_refresh:'#form_refresh',
    role_name: '#role_name',
    addStageBtn:'#addStageBtn',
    stage_list: '.stage_list',
    attach_doc:'.attach_doc',
    publish_form : '#publish_form',
    role_select: '#role_select',
    form_item: '.form-item',
    role_drop_down: '.role_drop_down',
    stageform_dropdown :'.stageform-dropdown',
    process_reassign:'#process_reassign',
    ac_btn: '.ac_btn',
    edit_user: '.edit_user',
    sec_role_ac_btn: '.sec_role_ac_btn',
    add_sec_role: '#add-sec-role',
    remove_sec_role:'#remove-sec-role',
    all_requests: '#all_requests',
    form_list: '.form_list',
    form_link:'.form_link',
    process_list_item:'.process_list_item',
    role:'#create_role',
    role_ac_btn:'.role_ac_btn',
    edit_branch: '.edit_branch',
    edit_role: '.edit_role',
    add_form_to_stage_btn: '#add_form_to_stage_btn',
    remove_form_to_stage_btn: '#remove_form_to_stage_btn',
    edit_stage_link: '.edit_stage_link',
    delete_stage_link : '.delete_stage_link',
    tbody_branchList: '#tbody_branchList',
    tbody_roleList: '#tbody_roleList',
    user_list:'#user_list',
    users_refresh:'#users_refresh',
    create_branch: '#create_branch',
    create_user: '#create_user',
    stageList: '#stage',
    branch_chk: '.branch_chk',
    selectedbranch: '.selectedbranch',
    check_all_branches: '.check_all_branches',
    branch_alert: '#branch_alert',
    create_process:'#create_process',
    report_link: '#report_link',
    schedule_link: '#schedule_link',
    print_link: '#print_link',
    corporate_print_link: '#corporate_print_link',
    perfection_print_link: '#perfection_print_link',
    userPane: '#user_pane',
    page_title: '#page-title',
    print_links: '#print-links',
    print_target: '#print_target',
    corporate_print_links: '#corporate_print-links',
    corporate_print_target: '#corporate_print_target',
    perfection_print_links: '#perfection_print-links',
    perfection_print_target: '#perfection_print_target'
}
PageHandler.currentPage = null;
var MessageCodes = {
    ERROR: 1,
    BUSY:2,
    SUCCESS:0
}
PageHandler.Resources = {
    base_path: '../',
    api_path: '../API',
    notices: '../Components/notices.htm',
    admin_home: '../Components/admin_home.htm',
    roles: '../Components/roles.htm',
    admin_branch: '../Components/branch.htm',
    request_detail: '../Components/requestdetail.htm',
    report_api: '../LawPavilion/API/Reports.ashx/',
    new_report_api: '../LawPavilion/API/MainReports.ashx/',
    calendar: '../LawPavilion/API/Calendar.ashx/',
    search: '../LawPavilion/API/Search.ashx/',
    report_home: '../Components/report_home.htm',
    print_home: '../Components/print_home.htm',
    schedule_home: '../Components/schedule_home.htm',
    corporate_print_home: '../Components/corporate_print_home.htm',
    perfection_print_home: '../Components/perfection_print_home.htm',
    print_api: '../Lawpavilion/API/Bonds.ashx/',
    lawpavilion_api: '../LawPavilion/API',
    reportid: 0
}
PageHandler.HTTPMethods = {
    POST:'POST',GET:'GET'
}
PageHandler.sendRequest = function (url, data, method, callback) {
    $.ajax({
        url: url,
        type: method,
        contentType: "application/json",
        data: (data),
        success: function (response) {
            if (typeof callback === 'function') {
                callback({ status: true, data: response });
            }
        },
        error: function (error) {
            if (typeof callback === 'function') {
                callback({ status: false, data: error });
            }
        }
    });
}
PageHandler.sendXMLHttp = function (url, data, method, callback) {
    //if (typeof FormData == "undefined") {
    //    var data = [];
    //} else {
    //    var f = new FormData();
    //    for (var item in data) {
    //        f.append(item, data[item]);
    //    }
    //    var request = new XMLHttpRequest();
    //    request.open(method, url);

    //var f = new FormData();
    var f = "";
    var urlEncodedDataPairs = [];

    for (var name in data) {
        urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
    }

    //    for (var item in data) {
    //        f.append(item, data[item]);
    //    }

    f = urlEncodedDataPairs.join('&').replace(/%20/g, '+');
    var request = new XMLHttpRequest();
    request.open(method, url);

    if (method.toLowerCase() == 'post') {
        request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    }


        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                if (request.status == 200) {
                    if (typeof callback === 'function') {
                        callback({ status: true, data: JSON.parse(request.responseText) });
                    }
                } else {
                    if (typeof callback === 'function') {
                        callback({ status: false, data: 'Network error' });
                    }
                }
            }

        }
        request.send(f);
    }

PageHandler.Page = {
    buildMenu: function (data) {
        var userObj = JSON.parse(data);
        $(".user_name").html(userObj.Email);
    }
}
PageHandler.Handles = {
    ajaxlinks: '.ajax-link',
    userajaxlinks: '.userajax-link',
    processMsg: '.process-state',
    success_process: '.alert_success',
    error_process: '.alert_error',
    loading_process: '.loading',
    _setDisplayWindow: function (_window) {
        PageHandler.currentPage = PageHandler.Handles.b(_window);
    },
    _getDisplayWindow: function () {
        return PageHandler.currentPage;
    },
    b: function (handle) {
        return $(handle);
    },
    show: function (e) {
        $('#loading_modal').modal({
            backdrop: 'static',
            show: true
        });
        $(e).removeClass('hide');
    },
    hide: function (e) {
        $(e).addClass('hide');
        $('#loading_modal').modal('hide');
        $("div").remove(".modal-backdrop");
    },
    setText: function (e, text) {
        e.html(text);
    },
    load: function (e, resource, callback) {
        
        $(e).html('Loading...');
        $(e).load(resource, function (e) {

            if (typeof callback == 'function') {
                callback(e);
            }
            PageHandler.BindToDocument();
        });
    },
    // By Adeyemi Salau
    // 26th September, 2015
    loadREPORTS: function (e, resource, callback) {
        $(e).html('Loading...');
        $(e).load(resource, function (e) {
            if (typeof callback == 'function') {
                callback(e);
            }
        });
        
    }
}
PageHandler.showMessage = function (code, message) {
    var alert_handle = PageHandler.Handles.b(PageHandler.Handles.processMsg);
    PageHandler.Handles.hide(alert_handle);
    switch (code) {
        case MessageCodes.ERROR:
            PageHandler.Handles.setText(PageHandler.Handles.b(PageHandler.Handles.error_process), message);
            PageHandler.Handles.show(PageHandler.Handles.error_process);
            break;
        case MessageCodes.BUSY:
            PageHandler.Handles.show(PageHandler.Handles.loading_process);
            break;
        case MessageCodes.SUCCESS:
            PageHandler.Handles.setText(PageHandler.Handles.b(PageHandler.Handles.success_process), message);
            PageHandler.Handles.show(PageHandler.Handles.success_process);
            break;
    }
    return alert_handle;
}
PageHandler.changeBranchStatus = function (branch_id, status) {

    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Branch.ashx?rand='+randomString()+'&a=change_status', { id: branch_id, active_fg: status }, PageHandler.HTTPMethods.POST, function (response) {
        if (response.status && response.data.status == 1) {

            alert('Change Applied');
            PageHandler.getBranches();
        } else {
            alert('No change was made. #Reason:' + response.data.message);
        }
    });
}
PageHandler.changeBranchName = function (branch_id, _name, _street, _town, _state, _region, _country) {

    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Branch.ashx?rand='+randomString()+'&a=edit', { id: branch_id, name: _name, street: _street, town: _town, state: _state, region: _region, country: _country }, PageHandler.HTTPMethods.POST, function (response) {
       
        if (response.status && response.data.status == 1) {

            alert('Change Applied');
            PageHandler.getBranches();
        } else {
            alert('No change was made. #Reason:' + response.data.message);
        }
    });
}
PageHandler.getStats = function () {
    //active_count
    $(".statistics_pane").addClass("hidden");
    $("#nostatistics").removeClass("hidden").html("<h2>Loading Dashboard... Please wait.</h2>");
    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Stat.ashx?rand=' + randomString() + '&a=active_count', {}, PageHandler.HTTPMethods.GET, function (response) {
        $(".statistics_pane").addClass("hidden");
        if (response.status && response.data.status == 1) {
            
            $("#branch_count").html(response.data.data.branch);
            $("#user_count").html(response.data.data.user);
            $("#form_count").html(response.data.data.form);
            $("#process_count").html(response.data.data.process);
            $("#statistics").removeClass("hidden"); //.html("");
        } else {
            $("#nostatistics").removeClass("hidden").html("<h2>No Statistics</h2>");
        }
    });
}
PageHandler.changeRoleStatus = function (role_id, status) {
    PageHandler.makeChange('Role', { id: role_id, active_fg: status }, function (response) {
        if (response.status && response.data.status == 1) {

            alert('Change Applied');
            PageHandler.getRoles();
        } else {
            alert('No change was made. #Reason:' + response.data.message);
        }
    }, 'change_status');
}
PageHandler.changeUserStatus = function (user_id, status) {
    PageHandler.makeChange('User', { user_id: user_id, status: status }, function (response) {
        console.log(response);
            if (response.status && response.data.status == 1) {

                alert('Change Applied');
                PageHandler.getUsers();
            }else if (response.data.status == -1) {
                alert('You dont have permission to activate user you created');
                PageHandler.getUsers();
            }
            else {
                alert('No change was made. #Reason:' + response.data.message);
            }
        }, 'change_status');
    }

    PageHandler.resetPassword = function (user_id, password) {
        PageHandler.makeChange('User', { user_id: user_id, password: password }, function (response) {
            if (response.status && response.data.status == 1) {

                alert('Change Applied');
                PageHandler.getUsers();
            } else {
                alert('No change was made. #Reason:' + response.data.message);
            }
        }, 'reset_password');
    }

PageHandler.changeRoleName = function (role_id, _name) {
    PageHandler.makeChange('Role', { id: role_id, name: _name }, function (response) {
        if (response.status && response.data.status == 1) {

            alert('Change Applied');
            PageHandler.getRoles();
        } else {
            alert('No change was made. #Reason:' + response.data.message);
        }
    },'edit');

}
PageHandler.getForms = function (callback) {
    PageHandler.makeChange('Form', {}, function (response) {
        if (response.status && response.data.status == 1) {
            $.each(response.data.data, function(i,v){
           // response.data.data.forEach(function (v, i) {
                PageHandler.form.descriptions[v.id] = v;
            });
        }
        if (typeof callback == "function") {
            callback(response);
        }
    }, 'get');

}
PageHandler.getFormsCOntrols = function (form_id, callback) {
    PageHandler.makeChange('Form', {form_id:form_id}, function (response) {
        if (typeof callback == "function") {
            callback(response);
        }
    }, 'get_controls');

}
PageHandler.makeChange = function (component, data, callback, action) {
    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/' + component + '.ashx?rand=' + randomString() + '&a=' + action, data, PageHandler.HTTPMethods.POST, function (response) {
        if (typeof callback == 'function') {
            callback(response);
        }
    });
}
/**** Gets *********/
PageHandler.getBranches = function (callback) {
    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Branch.ashx?rand=' + randomString() + '&a=get_all', {}, PageHandler.HTTPMethods.GET, function (response) {
        if (response.status && response.data.status == 1) {
           
            T.bindTo.branchList(PageHandler.Global.tbody_branchList, response.data.data, function () {
                PageHandler.BindToDocument();
            });
        }
        if (typeof callback == 'function') {
            callback(response);
        }
    });
};
PageHandler.getProcesses = function (callback) {
    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Process.ashx?rand=' + randomString() + '&a=get', {}, PageHandler.HTTPMethods.GET, function (response) {
       
        if (typeof callback == 'function') {
            callback(response);
        }
    });
};


//PageHandler.getRoleUsers = function (role_id, callback) {
   
//    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/User.ashx?rand=' + randomString() + '&a=get', { role_id: role_id }, PageHandler.HTTPMethods.POST, function (response) {

//        if (typeof callback == 'function') {
//            callback(response);
//        }
//    });
//};

PageHandler.getRoleUsers = function (process_id, role_id, branch_id, callback) {
    //"/RequestsRole.ashx?rand="+randomString() +"&user_id=" + user_id+"&branch_id="+branch_id+"&role_id="+role_id
    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/GetUsersRole.ashx?rand=" + randomString() +"&process_id="+process_id + "&role_id=" + role_id + "&branch_id=" + branch_id, {}, PageHandler.HTTPMethods.POST, function (response) {

        if (typeof callback == 'function') {
            callback(response);
        }
    });
};




PageHandler.getFilteredUsers = function (process_id, user_id, role_id, branch_id, callback) {
    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/FilterUsers.ashx/FilterUsers/' + process_id + "/" + user_id + "/" + role_id + "/" + branch_id, {}, PageHandler.HTTPMethods.POST, function (response) {

        if (typeof callback == 'function') {
            callback(response);
        }
    });
};


PageHandler.getProcessStages = function (_process_id, callback) {
    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Process.ashx?rand=' + randomString() + '&a=get_stage&process_id=' + _process_id, {}, PageHandler.HTTPMethods.GET, function (response) {

        if (typeof callback == 'function') {
            callback(response);
        }
    });
};
PageHandler.getRoles = function (callback) {
    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Role.ashx?rand=' + randomString() + '&a=get_all', {}, PageHandler.HTTPMethods.GET, function (response) {
        if (response.status && response.data.status == 1) {
            T.bindTo.genericBinding(PageHandler.Global.tbody_roleList, T.templates.roleListItem, response.data.data, function () {
                PageHandler.BindToDocument();
            });
        }
        if (typeof callback == 'function') {
            callback(response);
        }
    });
};
PageHandler.getStage = function (stage_id,callback) {
    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Process.ashx?rand=' + randomString() + '&a=get_stage&id=' + stage_id, {}, PageHandler.HTTPMethods.GET, function (response) {

        if (typeof callback == 'function') {
            callback(response);
        }
    });
};
PageHandler.getUsers = function (callback) {
    PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/User.ashx?rand=' + randomString() + '&a=get', {}, PageHandler.HTTPMethods.GET, function (response) {

        if (response.status && response.data.status == 1) {
            T.bindTo.genericBinding(PageHandler.Global.user_list, T.templates.userListItem, response.data.data[0], function () {
                PageHandler.BindToDocument();
            });
        }
        if (typeof callback == 'function') {
            callback(response);
        }
    });
};

PageHandler.markSelectedItem = function (item, context) {
    $(item+', .side-menu-link').removeClass('active').parent('li').removeClass('active').parent().find('a').removeClass('active');
    context.parent('li').addClass('active');
}
/**********************/






PageHandler.BindToDocument = function () {
    PageHandler.Handles.b(PageHandler.Handles.ajaxlinks).unbind('click').on('click', function (e) {
        var link = $(this).attr('data_url');
        if (link) {
            var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
            PageHandler.Handles.load(PageHandler.Global.adminPane, link, function (d) {
                PageHandler.Handles.hide(alert_handle);
            });
            PageHandler.markSelectedItem(PageHandler.Handles.ajaxlinks, $(this));
            e.preventDefault();
            return false;
        }
    });

    //Handling others for FORM 8/3/2016
    // ajagbe

    PageHandler.Handles.b(".handleOthers").one('click', function (e) {
        var data_id = $(this).data("id");
        //alert(data_id);
        $(".form-group").find("input[name*='d[" + data_id + "]']").parent().css("visibility", "hidden").slideUp();
        var handleOthers = document.getElementById("handleOthers");
       // console.log(handleOthers);
        //handleOthers.length
        if (typeof handleOthers.length == 'undefined') {
           
            var handleotherslabel = document.getElementById("handleotherslabel");
            $(handleotherslabel).append("<br /><input class='handleOthers form-control' type='text' name='d[" + data_id + "]' id='d[" + data_id + "]' />").slideDown();
           
        }
        //$(".form-group").append().slideDown("<input type='text' name='Others value' id='othersval' />");
    });

    
    PageHandler.Handles.b(PageHandler.Global.edit_stage_link).unbind('click').on('click', function (e) {
        var id = $(this).attr('data-id');
        if ($('#stage_list_head' + id).attr('contenteditable')) {
            if ($('#stage_list_description' + id).html().trim().length > 0 && $('#stage_list_head' + id).html().trim().length > 0) {
                $("#edit_stage_link" + id).html("Edit");
                $('#stage_list_description' + id).removeAttr('contenteditable').removeAttr("style");
                $('#stage_list_head' + id).removeAttr('contenteditable').removeAttr("style");
                PageHandler.makeChange("Process", { stage_id: id, name: $('#stage_list_head' + id).html(), description: $('#stage_list_description' + id).html() }, function (response) {
                   
                    if (response.status && response.data.status == 1) {
                        alert("Stage edited successfully");
                    } else {
                        alert("Stage not edited. Refresh and try again.");
                    }
                }, "edit_stage_detail");
            } else {
                alert("Stage name and description cannot be empty");
            }
            PageHandler.BindToDocument();
        } else {
            $("#edit_stage_link" + id).html("Save Changes");
            $("#stage_list" + id).unbind("click");

            $('#stage_list_description' + id).attr('contenteditable', true).attr("style", "background-color:Lime !important");
            $('#stage_list_head' + id).attr('contenteditable', true).attr("style", "background-color:Lime !important");
        }
        e.preventDefault();
        return false;
    });

    PageHandler.Handles.b(PageHandler.Global.delete_stage_link).unbind('click').on('click', function (e) {
        if (confirm("Are you sure?")) {
            var id = $(this).attr('data-id');
            PageHandler.makeChange("Process", { stage_id: id }, function (response) {
                if (response.status && response.data.status == 1) {
                    alert("Stage deleted successfully");

                } else {
                    alert("Stage not deleted. Refresh and try again.");
                }
            }, "remove_stage");
        }
        e.preventDefault();
        return false;
    });

    PageHandler.Handles.b(".delete_process_link").unbind('click').on('click', function (e) {
        if (confirm("If this is an active process, removing it will take it down permanently. Are you sure?")) {
            var prompt_response = prompt("PLEASE TYPE \"DELETE\" TO CONTINUE");
            if (prompt_response.trim().length > 0 && prompt_response == "DELETE") {
                var id = $(this).attr('data-id');
                PageHandler.makeChange("Process", { id: id, status: 3 }, function (response) {
                    if (response.status && response.data.status == 1) {
                        alert("Process deleted successfully");
                        $(".process_link").trigger('click');
                    } else {
                        alert("Process not deleted. Refresh and try again.");
                    }
                }, "change_status");
            } else {
                alert("Invalid Prompt response");
            }
        }
        e.preventDefault();
        return false;
    });
    PageHandler.Handles.b(".edit_process_link").unbind('click').on('click', function (e) {
        if ($('#pname' + $(this).attr('data-id')).attr('contenteditable')) {
            if ($('#pname' + $(this).attr('data-id')).html().trim().length > 0 && $('#pdesc' + $(this).attr('data-id')).html().trim().length > 0) {
                $('#pname' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                $('#pdesc' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                PageHandler.makeChange("Process", { id: $(this).attr('data-id'), reassign_role_id: $(this).attr('data-resign_role_id'), name: $('#pname' + $(this).attr('data-id')).html().trim(), description: $('#pdesc' + $(this).attr('data-id')).html().trim() }, function (response) {
                  
                    if (response.status && response.data.status == 1) {
                        alert("Process updates successfully");
                        $(".process_link").trigger('click');
                    } else {
                        alert("Process not updated. Refresh and try again.");
                    }
                }, "edit");

            } else {
                alert('Process\'s detail cannot be empty. Click refresh to restore page or fill in the detail as required');
            }
        } else {
            $(this).html('Save Changes');
            $('#pname' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
            $('#pdesc' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
        }
        e.preventDefault();
        return false;
    });
    PageHandler.Handles.b(PageHandler.Global.addStageBtn).unbind('click').on('click', function (e) {
        var stage_name = $("#stage_name");
        var stage_description = $("#stage_description");
        var stage_role = $("#stage_role");
        var stage_type = $("#stage_type");
        var process_id = $("#process_id");
        if (stage_name.val().trim().length <= 0 ||
         stage_description.val().trim().length <= 0 ||
          stage_role.val().trim().length <= 0 ||
           stage_type.val().trim().length <= 0 ||
            process_id.val().trim().length <= 0) {
            alert("Empty fields not allowed. Please fill properly and try again.");
            return;
        }
        var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
        var stage = new Stage();
        stage.name = stage_name.val();
        stage.description = stage_description.val();
        stage.role_id = stage_role.val();
        stage.type = stage_type.val();
        stage.process_id = process_id.val();
        stage.save(function (response) {
            console.log(response);
            if (response.status && response.data.status == 1) {
                stage_name.val('');
                stage_description.val('');
                stage_role.val('');
                stage_type.val('');
                alert("Stage created successfully.");
            } else {
                alert("Error! Stage not created. Refresh the page and try again");
            }
            PageHandler.Handles.hide(alert_handle);
        });

        e.preventDefault();
        return false;
    });
    PageHandler.Handles.b("#view_refresh").unbind('click').on('click', function () {
        $(".process_link").trigger('click');
    });
    PageHandler.Handles.b(PageHandler.Global.process_list_item).unbind('click').on('click', function (e) {

        $(".selected_process").html($(this).data('name'));
        var process_id = $(this).attr('data-id');



        var status = $(this).data('status');
        $(".process_control").addClass("hidden");
        if (status == 1) {
            $(".published").removeClass("hidden");
        } else {
            $(".unpublished").removeClass("hidden");
        }

        if (process_id) {
            $("#process_id").val(process_id);



            var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
            PageHandler.getProcessStages(process_id, function (response) {
              
                T.bindTo.genericBinding(PageHandler.Global.stageList, T.templates.stageListItem, response.data.data, function () {
                    PageHandler.BindToDocument();

                    PageHandler.Handles.hide(alert_handle);
                });
            });
        }
        PageHandler.markSelectedItem(PageHandler.Global.process_list_item, $(this));
    });
    PageHandler.Handles.b(PageHandler.Global.create_process).unbind('click').on('click', function (e) {
        var process_description = $("#process_description"),
        process_name = $("#process_name"),
        process_reassign = $("#process_reassign");
        var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, ''); ;
        if (process_description.val().trim().length > 0 && process_name.val().trim().length > 0 && process_reassign.val().trim().length > 0) {
            PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Process.ashx?rand=' + randomString() + '&a=add', {
                name: process_name.val(),
                description: process_description.val(),
                reassign_role_id: process_reassign.val()
            }, PageHandler.HTTPMethods.POST, function (response) {
                if (response.status && response.data.status == 1) {
                    alert_handle = PageHandler.showMessage(MessageCodes.SUCCESS, 'Process successfully created.');

                } else {
                    alert_handle = PageHandler.showMessage(MessageCodes.ERROR, 'Process not created! Please ensure a process with this same name has not been created');
                }
            });
        } else {
            alert_handle = PageHandler.showMessage(MessageCodes.ERROR, 'Empty field detected. Please fill properly');
        }
        setTimeout(function () {
            PageHandler.Handles.hide(alert_handle);
        }, 2000);
    });
    //create_process
    PageHandler.Handles.b(PageHandler.Global.stage_list).unbind('click').on('click', function (e) {
        $("#form-views").removeClass("hidden");
        $("#form-views > .panel-heading > .panel-title").html("Heading");
        var id = $(this).attr("data-id");
        $("#stage_id").val(id);
        if (id) {
            PageHandler.getStage(id, function (response) {
                if (response.status && response.data.status == 1 && response.data.data.length > 0) {

                    var form_id = response.data.data[0].form_id;
                    if (form_id != null && typeof form_id != "undefined") {
                        $("#form_id").val(form_id);
                        PageHandler.Handles.b(PageHandler.Global.add_form_to_stage_btn).addClass('hidden');
                        PageHandler.Handles.b(PageHandler.Global.remove_form_to_stage_btn).removeClass('hidden');
                        var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
                     
                        PageHandler.getFormsCOntrols(form_id, function (response) {
                            if (response.status && response.data.status == 1 && PageHandler.form.descriptions[form_id] != "undefined") {
                               
                                PageHandler.render("#form", response.data.data, PageHandler.form.descriptions[form_id]);
                                PageHandler.Handles.hide(alert_handle);
                            } else {
                                alert("Error rendering this form! Refresh the page. If this continues, then you have to re-create this form");
                            }

                        });
                    } else {
                        PageHandler.Handles.b(PageHandler.Global.add_form_to_stage_btn).removeClass('hidden');
                        PageHandler.Handles.b(PageHandler.Global.remove_form_to_stage_btn).addClass('hidden');
                    }
                }

            });
        }
        PageHandler.markSelectedItem(PageHandler.Global.stage_list, $(this));
    });
    PageHandler.Handles.b(PageHandler.Global.add_form_to_stage_btn).unbind('click').on('click', function (e) {
        var stage_id = $("#stage_id").val();
        var form_id = $("#form_id").val();
        if (form_id != undefined && stage_id != undefined) {
            PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Process.ashx?rand='+randomString()+'&a=add_stage_form", { form_id: form_id,
                stage_id: stage_id
            }, PageHandler.HTTPMethods.POST, function (response) {
              
                if (response.status && response.data.status == 1) {
                    alert("Form added to stage successfully");
                } else {
                    alert("Error! Form not added to stage. Refresh and  try again");
                }
            });
        } else {
            alert("Selected Stage or Form not found. Refresh the page and try again");
        }
    });
    PageHandler.Handles.b(PageHandler.Global.remove_form_to_stage_btn).unbind('click').on('click', function (e) {
        var stage_id = $("#stage_id").val();
        if (stage_id != undefined) {
            PageHandler.sendXMLHttp(PageHandler.Resources.api_path + "/Process.ashx?rand='+randomString()+'&a=remove_stage_form", {
                stage_id: stage_id
            }, PageHandler.HTTPMethods.POST, function (response) {
               
                if (response.status && response.data.status == 1) {
                    alert("Form removed from stage successfully");
                } else {
                    alert("Error! Form not removed from stage. Refresh and  try again");
                }
            });
        } else {
            alert("Selected Stage or Form not found. Refresh the page and try again");
        }
    });
    PageHandler.Handles.b(PageHandler.Global.form_refresh).unbind('click').on('click', function (e) {
        PageHandler.Handles.b(PageHandler.Global.form_link).trigger('click');
    });
    PageHandler.Handles.b(PageHandler.Global.form_link).unbind('click').on('click', function (e) {
        var link = $(this).attr('data_url');
        if (link) {
            var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
            PageHandler.Handles.load(PageHandler.Global.adminPane, link, function (d) {
                PageHandler.getForms(function (response) {
                    T.bindTo.genericBinding(PageHandler.Global.form_list, T.templates.formListItem, response.data.data, function () {
                        PageHandler.BindToDocument();
                    });
                });
                PageHandler.Handles.hide(alert_handle);
            });
            PageHandler.markSelectedItem(PageHandler.Handles.ajaxlinks, $(this));
            e.preventDefault();
            return false;
        }
    });

    PageHandler.Handles.b(PageHandler.Global.publish_form).unbind('click').on('click', function (e) {
        var form_name = $("#form_name");
        var description = $("#description");
        if (form_name.val().trim().length <= 0 || description.val().trim().length <= 0) {
            alert("Form name cannot be empty.");
            return false;
        }
        if (PageHandler.tempData.formDesignData != null) {
            try {
                var f = new Form();
                f.name = form_name.val(); f.description = description.val();
                f.packageControlData(JSON.parse(PageHandler.tempData.formDesignData));
                f.save(function (response) {
                    console.log("FESTUS");
                    console.log(response);
                    if (response.status && response.data.status == 1) {
                        alert("Form created successfully");
                        PageHandler.Handles.b(PageHandler.Global.form_link).trigger("click");
                    } else {
                        alert("Error! Form not created, please refresh the page and try again as this could a network error");
                    }
                });
            } catch (e) {
                alert("Error! Form not created, please refresh the page and try again as this could a network error");
            }
        } else {
            alert("Changes on the form design layer not detected! Edit any section for changes");
        }

        e.preventDefault();
        return false;
    });
    PageHandler.Handles.b(PageHandler.Global.process_link).unbind('click').on('click', function (e) {
        var link = $(this).attr('data_url');
        if (link) {
            var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
            PageHandler.Handles.load(PageHandler.Global.adminPane, link, function (d) {

                PageHandler.getRoles(function (response) {
                    T.bindTo.genericBinding(PageHandler.Global.role_drop_down, T.templates.dropDown, response.data.data, function () {
                        PageHandler.BindToDocument();
                    });
                });
                PageHandler.getForms(function (response) {
                    T.bindTo.genericBinding(PageHandler.Global.stageform_dropdown, T.templates.dropDown, response.data.data, function () {
                        PageHandler.BindToDocument();
                    });
                });
                PageHandler.getProcesses(function (response) {
                    T.bindTo.genericBinding(PageHandler.Global.process_list, T.templates.processListItem, response.data.data, function () {
                        PageHandler.BindToDocument();
                        $(".process_list_item ").eq(0).trigger('click');
                    });
                });
                PageHandler.Handles.hide(alert_handle);

            });

            PageHandler.markSelectedItem(PageHandler.Handles.ajaxlinks, $(this));
            e.preventDefault();
            return false;
        }
    });
    PageHandler.Handles.b(PageHandler.Global.stageform_dropdown).unbind('change').on('change', function (e) {
        var form_id = $(this).val();
        var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
        $("#form_id").val(form_id);
     
        PageHandler.getFormsCOntrols(form_id, function (response) {
            if (response.status && response.data.status == 1 && PageHandler.form.descriptions[form_id] != "undefined") {
               
                PageHandler.render("#form", response.data.data, PageHandler.form.descriptions[form_id]);
                PageHandler.Handles.hide(alert_handle);
            } else {
                alert("Error rendering this form! Refresh the page. If this continues, then you have to re-create this form");
            }

        });
    });
    PageHandler.Handles.b(PageHandler.Global.view_users).unbind('click').on('click', function (e) {
        var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');

        PageHandler.markSelectedItem(PageHandler.Handles.ajaxlinks, $(this));
        var link = $(this).attr('data_url');
        if (link) {

            PageHandler.Handles.load(PageHandler.Global.adminPane, link, function (d) {

               
                var container = $("#user-list > tbody");
                UserHandler.d.getAllUsers(function (response) {
                    PageHandler.Handles.hide(alert_handle);
                   // console.log(response);
                    if (response.length) {

                        $.each(response, function (i, v2) {
                            container.append(T.templates.userListItem(v2, i + 1));
                        });

                        UserHandler.bindToDom();
                        PageHandler.BindToDocument();
                        $('#user-list').DataTable();
                    } else {
                        alert("No Requests Found");
                    }
                });

                PageHandler.BindToDocument();
                UserHandler.bindToDom();

               
            });
            e.preventDefault();
            return false;
        }

        e.preventDefault();
        return false;
    });
    //tunde
    PageHandler.Handles.b(PageHandler.Global.view_branches).unbind('click').on('click', function (e) {
        var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
        PageHandler.Handles.load(PageHandler.Global.adminPane, PageHandler.Resources.admin_branch, function (d) {

     
            var container = $("#branch-list > tbody");
            UserHandler.d.getAllBranches(function (response) {
                PageHandler.Handles.hide(alert_handle);
                console.log(response);
                if (response.length) {

                    $.each(response, function (i, v2) {
                        container.append(T.templates.branchListItem(v2, i + 1));
                    });

                    UserHandler.bindToDom();
                    PageHandler.BindToDocument();
                    $('#branch-list').DataTable();
                } else {
                    alert("No branch Found");
                }
            });

            PageHandler.BindToDocument();
            UserHandler.bindToDom();
        });
        PageHandler.markSelectedItem(PageHandler.Handles.ajaxlinks, $(this));
        e.preventDefault();
        return false;
    });
    PageHandler.Handles.b(PageHandler.Global.create_user).unbind('click').on('click', function (e) {
        //alert($('#role_select').val());
        //return;
       
        var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
        var email = $('#email'),
            password = $('#password'),
            branch_select = $('#branch_select'),
            role_select = $('#role_select'),
            firstname = $('#firstname'),
            middlename = $('#middlename'),
            lastname = $('#lastname'),
            telephone = $('#telephone'),
            initial = $('#initial');
        if (email.val().length > 0 &&
        password.val().length > 0 &&
        branch_select.val().length > 0 &&
        role_select.val().length > 0 &&
        firstname.val().length > 0 &&
        middlename.val().length > 0 &&
        lastname.val().length > 0 &&
        telephone.val().length > 0) {
            PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/User.ashx?rand=' + randomString() + '&a=add', {
                email: email.val(),
                password: password.val(),
                branch_id: branch_select.val(),
                role_id: role_select.val(),
                firstname: firstname.val(),
                middlename: middlename.val(),
                lastname: lastname.val(),
                telephone: telephone.val(),
                initial: initial.val()
            }, PageHandler.HTTPMethods.POST, function (response) {
               
                if (response.status && response.data.status == 1) {
                    alert('User created successfully');
                    PageHandler.getUsers();
                } else {
                    alert('Error creating user. Please fill all fields as requested and try again');
                }
                PageHandler.Handles.hide(alert_handle);
            });
        } else {
            alert('Form fields cannot be empty');
        }
        e.preventDefault();
        return false;
    });
    PageHandler.Handles.b(PageHandler.Global.role_link).unbind('click').on('click', function (e) {
        var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
        PageHandler.Handles.load(PageHandler.Global.adminPane, PageHandler.Resources.roles, function (d) {

            PageHandler.getRoles();
            PageHandler.Handles.hide(alert_handle);
        });
        PageHandler.markSelectedItem(PageHandler.Handles.ajaxlinks, $(this));
        e.preventDefault();
        return false;
    });
    PageHandler.Handles.b(PageHandler.Global.branch_refresh).unbind('click').on('click', function (e) {
        PageHandler.Handles.b(PageHandler.Global.view_branches).trigger('click');
    });
    PageHandler.Handles.b(".reset_user_ac_btn").unbind('click').on('click', function (e) {
        var new_pass = prompt("Enter new password for this user");
        if (new_pass.length > 0 && confirm("Are you sure?")) {
            PageHandler.resetPassword($(this).attr('data-id'), new_pass);
        }

    });

    PageHandler.Handles.b(".change_user_ac_btn").unbind('click').on('click', function (e) {

        if (confirm("Are you sure?")) {
            var status = 0;
            if ($(this).attr('click-action') == 'Active') {
                status = 2; // Change from Active to Disabled since current status is Active
            } else if ($(this).attr('click-action') == 'Disabled') {
                status = 1;
            }
            PageHandler.changeUserStatus($(this).attr('data-id'), status);
        }

    });
    PageHandler.Handles.b(PageHandler.Global.form_item).unbind('click').on('click', function (e) {
        $('.form_view').addClass('hidden');
        if ($(this).hasClass('new_form')) {
            $(".design").removeClass("hidden");
        } else {
            $(".preview").removeClass("hidden");
            var alert_handle = PageHandler.showMessage(MessageCodes.BUSY, '');
            var form_id = $(this).attr('data-id');
            PageHandler.getFormsCOntrols(form_id, function (response) {
                if (response.status && response.data.status == 1 && PageHandler.form.descriptions[form_id] != "undefined") {
                    PageHandler.render("#form", response.data.data, PageHandler.form.descriptions[form_id], false, null, null, function () {
                        PageHandler.BindToDocument();
                    });
                    PageHandler.Handles.hide(alert_handle);
                } else {
                    alert("Error rendering this form! Refresh the page. If this continues, then you have to re-create this form");
                }

            });
        }
        PageHandler.markSelectedItem($('.form-item'), $(this));
        $('#form_title').html($(this).html());
    });

    PageHandler.Handles.b(PageHandler.Global.ac_btn).unbind('click').on('click', function (e) {

        if (confirm("Are you sure?")) {
            var status = 0;
            if ($(this).attr('click-action') == 'Active') {
                status = 0; // Change from Active to Disabled since current status is Active
            } else if ($(this).attr('click-action') == 'Disabled') {
                status = 1;
            }
            PageHandler.changeBranchStatus($(this).attr('data-id'), status);
        }
    });
    PageHandler.Handles.b(PageHandler.Global.edit_branch).unbind('click').on('click', function (e) {

        if ($('#p' + $(this).attr('data-id')).attr('contenteditable')) {
            if ($('#p' + $(this).attr('data-id')).html().length > 0 &&
            $('#s' + $(this).attr('data-id')).html().length > 0 &&
            $('#t' + $(this).attr('data-id')).html().length > 0 &&
            $('#st' + $(this).attr('data-id')).html().length > 0 &&
            $('#r' + $(this).attr('data-id')).html().length > 0 &&
            $('#c' + $(this).attr('data-id')).html().length > 0) {
                $('#p' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                $('#s' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                $('#t' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                $('#st' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                $('#r' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                $('#c' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                PageHandler.changeBranchName($(this).attr('data-id'), $('#p' + $(this).attr('data-id')).html(), $('#s' + $(this).attr('data-id')).html(), $('#t' + $(this).attr('data-id')).html(), $('#st' + $(this).attr('data-id')).html(), $('#r' + $(this).attr('data-id')).html(), $('#c' + $(this).attr('data-id')).html());
                PageHandler.getBranches();
                $(this).html('Edit');
            } else {
                alert('Branch name cannot be empty. Click refresh to restore page or fill in a branch name');
            }
        } else {
            $(this).html('Save Changes');
            $('#p' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
            $('#s' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
            $('#t' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
            $('#st' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
            $('#r' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
            $('#c' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
        }
    });
    PageHandler.Handles.b(PageHandler.Global.edit_user).unbind('click').on('click', function (e) {

        if ($('#f' + $(this).attr('data-id')).attr('contenteditable')) {

            if ($('#f' + $(this).attr('data-id')).html().length > 0 && $('#l' + $(this).attr('data-id')).html().length > 0 &&
            $('#m' + $(this).attr('data-id')).html().length > 0 && $('#t' + $(this).attr('data-id')).html().length > 0) {
                $('#f' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                $('#l' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                $('#m' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                $('#t' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/User.ashx?rand=' + randomString() + '&a=edit_profile', {
                    user_id: $(this).attr('data-id'),
                    firstname: $('#f' + $(this).attr('data-id')).text(),
                    middlename: $('#m' + $(this).attr('data-id')).text(),
                    lastname: $('#l' + $(this).attr('data-id')).text(),
                    telephone: $('#t' + $(this).attr('data-id')).text()
                }, PageHandler.HTTPMethods.POST, function (response) {
                    if (response.status && response.data.status == 1) {
                        alert('Change made successfully.');
                        PageHandler.getUsers(); //edit_profile
                    } else {
                        alert('Changes not made.');
                    }

                    $(this).html('Edit');
                });

            } else {
                alert('User\'s detail cannot be empty. Click refresh to restore page or fill in the detail as required');
            }
        } else {
            $(this).html('Save Changes');
            $('#f' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
            $('#l' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
            //$('#m' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
            $('#t' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
        }
    });

    PageHandler.Handles.b(PageHandler.Global.sec_role_ac_btn).unbind('click').on('click', function (e) {
        var userid = $(this).data("id");
        //alert($(e).data("name"));
        $("#sec-title").html("MANAGE SECONDARY ROLES: " + $(this).data("name").toUpperCase() + " (" + $(this).data("role").toUpperCase() + ")");
        $.ajax({
            type: "POST",
            url: "../API/SecRole.ashx/" + userid,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var strBuffer = "";
                var parsed = $.parseJSON(JSON.stringify(data.available));
                $("#available-role").children().remove();

                $.each(parsed, function (i, jsondata) {
                    strBuffer = "<a href='#' class='list-group-item available' data-sectype='available' data-roleid='" + jsondata.id + "' data-user='" + userid + "'>" + (jsondata.name).toUpperCase() + "</a>";
                    $("#available-role").append(strBuffer);
                });
                parsed = $.parseJSON(JSON.stringify(data.assigned));
                $("#assigned-role").children().remove();
                $.each(parsed, function (i, jsondata) {
                    strBuffer = "<a href='#' class='list-group-item assigned' data-sectype='assigned' data-roleid='" + jsondata.id + "' data-user='" + userid + "'>" + (jsondata.name).toUpperCase() + "</a>";
                    $("#assigned-role").append(strBuffer);
                });

                $("#secRoleModal").modal("show");
                //bind add role and remove role buttons
                PageHandler.Handles.b(".list-group-item.available").unbind('click').on('click', function (e) { selectAddRole(this); });
                PageHandler.Handles.b(".list-group-item.assigned").unbind('click').on('click', function (e) { selectRemoveRole(this); });

                PageHandler.Handles.b(PageHandler.Global.add_sec_role).unbind('click').on('click', function (e) {
                    addRole(this);
                });
                PageHandler.Handles.b(PageHandler.Global.remove_sec_role).unbind('click').on('click', function (e) {
                    removeRole(this);
                });

            },
            error: function (XHR, errStatus, errorThrown) {
                var err = JSON.parse(XHR.responseText);
                errorMessage = err.Message;
                alert(errorMessage);
            }
        });


    });

    PageHandler.Handles.b(PageHandler.Global.users_refresh).unbind('click').on('click', function (e) {
        PageHandler.Handles.b(PageHandler.Global.view_users).trigger('click');
    });
    //PageHandler.Handles.b("#form_renderer").unbind('click').on('click', function (e) {

    //    var resp = $("#form_renderer").trigger(".generic-start");
    //    var title = Window.va + "  TERMINATED";
    //   // alert(Window.va);
    //    $(".modal-body #form_view #special_controls").val($("#title").val(title));
    //    $(".modal-footer").html("<button type='button' class='btn btn-danger btn_close' data-dismiss='modal' data-toggle=modal data-target=#form_renderer>Close</button>");
    //    $("#form_renderer").modal("show");
    //});


    PageHandler.Handles.b(".generic-start").unbind('click').on('click', function (e) {
        var datat = $(this).data("payload");
        Window.va = datat.title;
        //console.log(datat);
       


        if (UserHandler.user.role_id != "24") {

            alert("You don't have the permission to terminate this request. Please contact the Team Lead");
            $("#form_renderer").modal("toggle");
        }
    });
   
 
    //123
    PageHandler.Handles.b(".btn-reassign").unbind('click').on('click', function (e) {
       
        var payload = $(this).data('payload');
        var process = payload.process;
        var request = payload.request_title;
        var request_id = payload.request_id;
        var role_id = payload.role_id;
        var branch_id = payload.branch_id;
        var process_id = payload.process_id;
        var role_name = payload.role_name;
        var id = payload.current_user_id;
        
        var postData = { 'user_id':id, 'request_id': request_id, 'role_id': role_id, 'branch_id': branch_id, 'process_id': process_id };
        postData = JSON.stringify(postData);
       
        $('#reassign_request-modal-title').text(request.ucWords());
        $('#reassign_request_id').text(request_id);
        $('#role-role_user').text("To : " + role_name);
        $('#reassign_request-modal').modal('show');
        $.ajax({
            url: "../API/Assign.ashx?a=" + "getUserRole",
            type: "POST",
            data: postData,
            contenttype: "application/json",
            success: function(resp) {
                var parsed_branch = $.parseJSON(JSON.stringify(resp));
                
                var select = $("#role_user");
               
                select.empty().append('<option selected="selected" value="0">Please select</option>');
                $.each(parsed_branch, function (index, jsonData) {
                    select.append($("<option></option>").val(jsonData.id).html(jsonData.title));

                });
            },
            error:function(error) {
                
            }

        });
       
        
 
    });

    //FESTUS
    PageHandler.Handles.b("#save_reassign").unbind('click').on('click', function(e) {
        var requestId = $('#reassign_request_id').text();
        var user = $("#role_user").val();
        var postData = { 'request_id': requestId, 'user_id': user };
        postData = JSON.stringify(postData);
        $.ajax({
            url: "../API/Assign.ashx?a=" + "Reassign_Request",
            type: "POST",
            contenttype: "application/json",
            data: postData,
            success: function (response) {
                var parsed = $.parseJSON(JSON.stringify(response));
                $.each(parsed, function (i, jsondata) {
                    if (jsondata.Name === "success") {
                        alert("Reassign successful");
                        $('#reassign_request-modal').modal('toggle');
                        
                        location.reload(true);
                    } else {
                        alert("Reassign failed");
                    }
                });
            },
            error: function(xhr) {

            }
        });


    });


    PageHandler.Handles.b(".btn-comments").unbind('click').on('click', function (e) {
        var payload = $(this).data('payload');
        var process = payload.process;
        var request = payload.title;
       
        $('#allcomments-modal-title').text(process.toUpperCase() + ": " + request.ucWords());
        $('#allcomments-container').html('Loading...');
        $('#allcomments-modal').modal('show');
        UserHandler.d.getAllCommentsByUserID(UserHandler.user.id, payload.id, function (response) {
            $('#allcomments-container').html('');
            if (response.length) {
                $.each(response, function (i, v2) {
                //response.forEach(function (v2, i) {
                    $('#allcomments-container').append(T.templates.renderAllComments(v2, i + 1)).append('<hr />');
                });

            } else {
                $('#allcomments-container').html('<p>No comment found</p>');
            }
        });

    });

   
    PageHandler.Handles.b(".btn-gen-ref").unbind('click').on('click', function (e) {
        var payload = $(this).data('payload');
        var process = payload.process;
        var request = payload.id;
        if ($("#generated_ref_" + request).text().length) {
            alert("Reference Code already generated for this request");
            return;
        };

        var r = confirm("Generate Reference Code for Request?");
        if (!r) { return; }
        //alert("laloye");
        //return;

        UserHandler.d.getNewRef(request, function (response) {
            $("#generated_ref_" + request).text(response[0].new_ref);

        });

    });





    $('#getrequest_sent').on('click', function () {

        PageHandler.Handles._setDisplayWindow(PageHandler.Global.user_page);
        var link = "../Components/get_request.htm";

        PageHandler.Handles.load(PageHandler.Global.adminPane, link, function (d) {
            $('#page-title').html("ALL REQUESTS ");

            var container = $("#admin_allrequests-list > tbody");
         
            UserHandler.d.AdmingetAllRequests(function (response) {

                if (response.length) {
                    // alert(response);
                    $.each(response, function(i, v2) {
                    //response.forEach(function (v2, i) {
                        container.append(T.templates.renderAllAdminRequests(v2, i + 1));

                    });

                    UserHandler.bindToDom();
                    PageHandler.BindToDocument();
                    $('#admin_allrequests-list').DataTable();
                } else {
                    alert("No Requests Found");
                }
            });

            PageHandler.BindToDocument();
            UserHandler.bindToDom();


        });

    });

    $('#stage_move').on('click', function() {

        PageHandler.Handles._setDisplayWindow(PageHandler.Global.user_page);
        var link = "../Components/stage_mover.htm";

        PageHandler.Handles.load(PageHandler.Global.adminPane, link, function (d) {
            $('#page-title').html("ALL REQUESTS ");

            var container = $("#stage_requests-list > tbody");

            UserHandler.d.AdmingetAllRequests(function (response) {

                if (response.length) {
                    // alert(response);
                    $.each(response, function (i, v2) {
                        //response.forEach(function (v2, i) {
                        container.append(T.templates.renderAllAdminRequestsStage(v2, i + 1));

                    });

                    UserHandler.bindToDom();
                    PageHandler.BindToDocument();
                    $('#stage_requests-list').DataTable();
                } else {
                    alert("No Requests Found");
                }
            });

            PageHandler.BindToDocument();
            UserHandler.bindToDom();


        });
    });

    PageHandler.Handles.b(".btn-move_forward").unbind('click').on('click', function(e) {

        var payload = $(this).data('payload');
        Window.request_id_id=  payload.request_id;
        var stage_id = payload.stage_id;
        var process = payload.process;
        var request = payload.request_title;
        var request_id = payload.request_id;
        var role_id = payload.role_id;
      

        $('#stage_request_modal_title').text(request.ucWords());
        $('#stage_request_id').text(request_id);
        $("#role_user_data").addClass('hidden');
        $('#stage_request-modal').modal('show');
        $("#stage-container  > #content_result").removeClass('hidden');
        $.ajax({
            url: "../API/stageMover.ashx?rand=" + randomString() + "&a=" + "getOtherStage" + "&stage_id=" + stage_id,
            type: "GET",
            success: function(response) {
                $("#stage-container  > #content_result").html("");
                $.each(response, function(i, json) {
                    $("#stage-container  > #content_result").append("<input type='button' class='btn btn-primary' id='btn-load-role' data-request_id= '" + json.request_id + "' data-process_id='" + json.process_id + "' data-id='" + json.next_id + "' value= '" + json.name + "'/><br/><br/>");
                });
               
            },
            error: function(xhr) {

            }
        });

       
        

    });

    $("#stage-container").on('click', "#btn-load-role", function(e) {

        var stage_id = $(this).data("id");
        var process_id = $(this).data("process_id");
      //  var request_id = $(this).data("request_id");
       // alert(request_id);

        $.ajax({
            url: "../API/StageMover.ashx?rand=" + randomString() + "&a=" + "getUserRole" + "&nextstage_id=" + stage_id + "&process_id=" + process_id,
            type: "GET",
            success: function (response) {
               // $("#stage-container").html("");
                var parsed_branch = $.parseJSON(JSON.stringify(response));
                
                var select = $("#stage-container   #role_user_data");
                Window.stage_id_id = stage_id;
                //$("#stage-container  > #content_result").append("<input  type='hidden' id='stage' data-stage_id='" + stage_id + "' value='" + stage_id + "'/>");
                //$("#stage-container  > #content_result").append("<input  type='hidden' id='request_id' data-request_id='" + request_id + "' value='" + request_id + "'/>");
                $("#stage-container  > #content_result").addClass('hidden');
                $("#role_user_data").removeClass('hidden');

               
                select.empty().append('<option selected="selected" value="0">Please select</option>');
                $.each(parsed_branch, function (index, jsonData) {
                   // console.log(jsonData);
                    if (jsonData.role_id == 11) {
                        select.append($("<option></option>").val(jsonData.id).html(jsonData.FIRM));
                    } else {
                        select.append($("<option></option>").val(jsonData.id).html(jsonData.title));
                    }
                  

                });
            },
            error: function(xhr) {
                alert("Please try again later");
            },
        });
    });

    PageHandler.Handles.b("#stage_reassign").unbind('click').on('click', function(e) {
        var next_stage_id = Window.stage_id_id;
        var user = $("#role_user_data").val();
        var request = Window.request_id_id;
        

        $.ajax({
            url: "../API/StageMover.ashx?rand=" + randomString() + "&a=" + "save" + "&nextstage_id=" + next_stage_id + "&user=" + user + "&request=" + request,
            type: "GET",
            success: function(response) {
                var parsed = $.parseJSON(JSON.stringify(response));
                $.each(parsed, function (i, jsondata) {
                    if (jsondata.Name === "success") {
                        alert("Stage successfully moved !!! ");
                        $('#stage_request-modal').modal('toggle');

                        location.reload(true);
                    } else {
                        alert("Opps !!! Error Occurred");
                    }
                });
            },
            error: function(xhr) {

            }

        });

    });

    PageHandler.Handles.b(PageHandler.Global.role_refresh).unbind('click').on('click', function (e) {
        PageHandler.Handles.b(PageHandler.Global.role_link).trigger('click');
    });
    PageHandler.Handles.b(PageHandler.Global.check_all_branches).unbind('click').on('click', function (e) {
        if (!$(this).is(':checked')) {
            PageHandler.Handles.b(PageHandler.Global.branch_chk).prop('checked', true);
        } else {
            PageHandler.Handles.b(PageHandler.Global.branch_chk).prop('checked', false);
        }
        PageHandler.Handles.b(PageHandler.Global.branch_chk).trigger('click');
    });
    PageHandler.Handles.b(PageHandler.Global.branch_chk).unbind('click').on('click', function (e) {
        var id = $(this).attr('data-id');

        if ($(this).is(':checked')) {
            if (PageHandler.tempData.checkedBranches.indexOf(id) < 0) {
                PageHandler.tempData.checkedBranches.push(id);
            }
        } else {
            if (PageHandler.tempData.checkedBranches.indexOf(id) >= 0) {
                PageHandler.tempData.checkedBranches.splice(PageHandler.tempData.checkedBranches.indexOf(id), 1);
            }

        }
        if (PageHandler.tempData.checkedBranches.length > 0) {
            PageHandler.Handles.b(PageHandler.Global.selectedbranch).removeClass('disabled');
        } else {
            PageHandler.Handles.b(PageHandler.Global.selectedbranch).addClass('disabled');
        }

    });

    PageHandler.Handles.b(PageHandler.Global.create_branch).unbind('click').on('click', function (e) {
        var branch = PageHandler.Handles.b(PageHandler.Global.branch_name).val();
        var street = PageHandler.Handles.b(PageHandler.Global.street).val();
        var town = PageHandler.Handles.b(PageHandler.Global.town).val();
        var state = PageHandler.Handles.b(PageHandler.Global.state).val();
        var region = PageHandler.Handles.b(PageHandler.Global.region).val();
        var country = PageHandler.Handles.b(PageHandler.Global.country).val();
        var type = PageHandler.Handles.b(PageHandler.Global.branch_type).val();
        if (type.trim().length < 1) {
            k = PageHandler.Handles.b(PageHandler.Global.branch_alert).removeClass('alert-success').addClass('alert-danger').html(
                    '<b>Error!</b>. Branch not created. Branch Type must be specified'
                    ).removeClass('hide');
            if (k != null) {
                setTimeout(function () { k.addClass('hide'); }, 3000);
            }
            return;
        }
        if (branch.trim().length > 0) {
            var d = { name: branch,
                street: street,
                town: town,
                state: state,
                region: region,
                country: country,
                type: type
            }; var k = null;

            PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Branch.ashx?rand=' + randomString() + '&a=add', d, PageHandler.HTTPMethods.POST, function (response) {
             
                if (response.status && response.data.status == 1) {
                    //k = PageHandler.Handles.b(PageHandler.Global.branch_alert).removeClass('alert-danger').addClass('alert-success').html(
                    //'<b>Success!</b>. Branch created successfully.'
                    //).removeClass('hide');
                    alert("Branch created successfully");
                    PageHandler.getBranches();
                } else {
                    //k = PageHandler.Handles.b(PageHandler.Global.branch_alert).removeClass('alert-success').addClass('alert-danger').html(
                    //'<b>Error!</b>. Branch not created. #Reason:' + response.data.message
                    //).removeClass('hide');
                    alert("Branch not created");
                    PageHandler.getBranches();
                }
                PageHandler.Handles.b(PageHandler.Global.branch_name).val('');
                if (k != null) {
                    setTimeout(function () { k.addClass('hide'); }, 3000);
                }
            });
        } else {
            //k = PageHandler.Handles.b(PageHandler.Global.branch_alert).removeClass('alert-success').addClass('alert-danger').html(
            //        '<b>Error!</b>. Branch not created. Branch name cannot be empty'
            //        ).removeClass('hide');
            alert("Branch not created. Branch name cannot be empty");
            if (k != null) {
                setTimeout(function () { k.addClass('hide'); }, 3000);
            }
        }

    });

//    PageHandler.Handles.b(PageHandler.Global.create_role).unbind('click').on('click', function (e) {
////        alert("FESTUS");
//        var role = PageHandler.Handles.b(PageHandler.Global.role_name).val();
//        if (role.trim().length > 0) {
//            var d = { name: role }; var k = null;

//            PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Role.ashx?rand=' + randomString() + '&a=add', d, PageHandler.HTTPMethods.POST, function (response) {
               
//                if (response.status && response.data.status == 1) {
//                    k = PageHandler.Handles.b(PageHandler.Global.branch_alert).removeClass('alert-danger').addClass('alert-success').html(
//                    '<b>Success!</b>. Role created successfully.'
//                    ).removeClass('hide');
//                    PageHandler.getBranches();
//                } else {
//                    k = PageHandler.Handles.b(PageHandler.Global.branch_alert).removeClass('alert-success').addClass('alert-danger').html(
//                    '<b>Error!</b>. Role not created. #Reason:' + response.data.message
//                    ).removeClass('hide');
//                }
//                PageHandler.Handles.b(PageHandler.Global.role_name).val('');
//                if (k != null) {
//                    setTimeout(function () { k.addClass('hide'); }, 3000);
//                }
//            });
//        } else {
//            k = PageHandler.Handles.b(PageHandler.Global.branch_alert).removeClass('alert-success').addClass('alert-danger').html(
//                    '<b>Error!</b>. Role not created. Role name cannot be empty'
//                    ).removeClass('hide');
//            if (k != null) {
//                setTimeout(function () { k.addClass('hide'); }, 3000);
//            }
//        }

//    });

    PageHandler.Handles.b("#create_role").click(function(e) {
        e.preventDefault();
        var role = PageHandler.Handles.b(PageHandler.Global.role_name).val();
       // alert(role);
        if (role.trim().length > 0) {
            var d = { name: role }; var k = null;

            PageHandler.sendXMLHttp(PageHandler.Resources.api_path + '/Role.ashx?rand=' + randomString() + '&a=add', d, PageHandler.HTTPMethods.POST, function (response) {
                console.log(response);
                if (response.status == 1) {
                    k = PageHandler.Handles.b(PageHandler.Global.branch_alert).removeClass('alert-danger').addClass('alert-success').html(
                    '<b>Success!</b>. Role created successfully.'
                    ).removeClass('hide');
                    PageHandler.getBranches();
                    return false;
                } else {
                    k = PageHandler.Handles.b(PageHandler.Global.branch_alert).removeClass('alert-success').addClass('alert-danger').html(
                    '<b>Error!</b>. Role not created. #Reason:' + response.data.message
                    ).removeClass('hide');
                }
                PageHandler.Handles.b(PageHandler.Global.role_name).val('');
                if (k != null) {
                    setTimeout(function () { k.addClass('hide'); }, 3000);
                }
            });
        } else {
            k = PageHandler.Handles.b(PageHandler.Global.branch_alert).removeClass('alert-success').addClass('alert-danger').html(
                    '<b>Error!</b>. Role not created. Role name cannot be empty'
                    ).removeClass('hide');
            if (k != null) {
                setTimeout(function () { k.addClass('hide'); }, 3000);
            }
        }
    });

    PageHandler.Handles.b(PageHandler.Global.role_ac_btn).unbind('click').on('click', function (e) {

        if (confirm("Are you sure?")) {
            var status = 0;
            if ($(this).attr('click-action') == 'Active') {
                status = 0; // Change from Active to Disabled since current status is Active
            } else if ($(this).attr('click-action') == 'Disabled') {
                status = 1;
            }
            PageHandler.changeRoleStatus($(this).attr('data-id'), status);
        }
    });
    PageHandler.Handles.b(PageHandler.Global.edit_role).unbind('click').on('click', function (e) {

        if ($('#p' + $(this).attr('data-id')).attr('contenteditable')) {
            if ($('#p' + $(this).attr('data-id')).html().length > 0) {
                $('#p' + $(this).attr('data-id')).removeAttr('contenteditable').removeAttr("style");
                PageHandler.changeRoleName($(this).attr('data-id'), $('#p' + $(this).attr('data-id')).html());
                PageHandler.getRoles();
                $(this).html('Edit');
            } else {
                alert('Role name cannot be empty. Click refresh to restore page or fill in a role name');
            }
        } else {
            $(this).html('Save Changes');
            $('#p' + $(this).attr('data-id')).attr('contenteditable', true).attr("style", "background-color:Lime !important");
        }
    });

    PageHandler.Handles.b(".trigga").unbind('click').on('click', function () {
        var next_stage_id = $(this).data('stage_id');
        var next_stage_role_id = $(this).data('next_stage_role_id');
        $(".main_btn").removeClass("hidden");
        $(".control_lbl_x").addClass("hidden");
        $(".control_lbl_btn").addClass("hidden");
        $(".roles_next" + next_stage_role_id).removeClass("hidden");
        $(".label" + next_stage_role_id).removeClass("hidden");
        $("#next_stage").val(next_stage_id);
        //submitOverride($("form"));
    });

    PageHandler.Handles.b(".role_list").unbind('change').on('change', function () {

        $("#next_user_id").val($(this).val());
        //submitOverride($("form"));
    });

    var allow_times = [];
    for (var t = 8; t < 18; t++) {
        allow_times.push(t + ":00");
    }
    $('.datetimepicker').datetimepicker({
        allowTimes: allow_times,
        minDate: 0,
        defaultDate: new Date()
    });
    $('.datetimepicker2').datetimepicker({
        timepicker: false,
        format: 'Y/m/d'
    });

    $('#receipt_certificate_date').datetimepicker({
        timepicker: false,
        format: 'Y/m/d'
    });


   // $("span.timeago").timeago();

}


PageHandler.Process = {
    parseData: function (map, process_detail) {
        var processMap = {};
        var treeMap = [];
        var tempMap = {};
        var relationship = {};
        $.each(process_detail, function(i, v){
      
            processMap[v.id] = v;
        });
        var t = null; var t2 = null;
        $.each(map, function(i, v){
       
            t = new treeData();

            t.id = v.from;
            if (t.id in processMap) {
                t.label = processMap[t.id].name;
            }
            t2 = new treeData();
            t2.id = v.to;
            if (t2.id in processMap) {
                t2.label = processMap[t2.id].name;
            }
            if (v.from in relationship) {
                var pid = relationship[v.from];
                tempMap[pid].children.push(t);
            } else {
                relationship[t2.id] = t.id;
                t.children.push(t2);
                tempMap[t.id] = t;
            }
            //treeMap.push(t);
            t = t2 = null;
        });
        for (var v in tempMap) {
            treeMap.push(tempMap[v]);
        }
       
        return [{ id: 0, label: "Initialization Process", children: treeMap}];
    }
}
