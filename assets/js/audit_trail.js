﻿$(document).ready(function () {
    function randomString() {
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = 8;
        var randomstring = '';
        for (var i = 0; i < string_length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1);
        }
        return randomstring;
    }

    //$('#dataTable').DataTable({
    //    "ajax": '../API/AuditApi.ashx'+ '?rand=' + randomString(),

    //    "columns": [
    //        { "data": "id" },
    //        { "data": "user_id" },
    //        { "data": "action" },
    //        { "data": "details" },
    //        { "data": "timestamp" },
    //        { "data": "ip" }
          
    //    ]

    //});
    $.ajax({
        url: "../API/AuditApi.ashx" + '?rand=' + randomString(),
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            console.log(response);
            var parsed = $.parseJSON(JSON.stringify(response.data));
            var container = $("#audit_dataTable > tbody");
            $.each(parsed, function (i, v2) {
                container.append(T.templates.renderAudits(v2, i + 1));
            });
            $('#audit_dataTable').DataTable();
        },
        error: function(error) {
            console.log(error);
        }
    });

    $('#printAudit').on('click', function () {

        var dataTable = document.getElementById("dataTable");
        //console.log(dataTable);
        var d = new Date();
        var title = "Audit Trail";
        var printText = "Generated on " + d.toLocaleString();
        var widS = parseInt(screen.width - 100);
        var heiS = parseInt(screen.height - 100);
        var x = window.open('', '', 'toolbar = no, scrollbars = yes, resizable = yes,width=' + widS + ',height=' + heiS);
        x.document.write('<style type="text/css">.printtext {font-family:verdana, sans-serif;font-size:10px;float:right;font-style: italic;} h1 {font-family:verdana, sans-serif;font-weight:400;} table, th, td {font-size:12px;font-family:verdana, sans-serif;border-collapse: collapse; border: thin solid #ccc;} </style>');
        x.document.write('<center><h1>' + title + '</h1></center><table cellpadding=4>' + $(dataTable).html() + '</table><div class="printtext">' + printText + '</div>');
        x.print();

    });

    $('#excelAudit').on('click', function () {

     
        var exportData = document.getElementById("exportData");
        var send = $(exportData).html();
        var d = new Date();
        var printText = "Generated on " + d.toLocaleString();

        $("body").prepend("<form method='post' action='../components/exports/AuditExcel.ashx'  id='tempForm'><textarea id='data' name='data'>" + send + " </textarea> <input type='hidden' name='nameDataValue' value='" + printText + "' ></form>");
        $('#tempForm').submit();


    });
});