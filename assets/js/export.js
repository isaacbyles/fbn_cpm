﻿//$(".buttonactions").hide();

$(".dropdown-menu li").click(function (e) {

    e.preventDefault();
   
    $(".dropdown-menu").find("a").removeClass("selectedLi");
    $(this).find("a").addClass("selectedLi");
    var title = $(this).find("a").text();
    window.Var1 = title; //pass title into window
//    var mm = document.getElementById('Hidden2');
//    mm.value = title;
});

$(".printDoc").on("click", function(e) {

    e.preventDefault();
    var title_of_temp = window.Var1; //get title out of window
    var title = $.trim(title_of_temp);
    var com1 = "Advanced APG Template";
    var com2 = "Airtel Template Bank Guarantee - 2013";
    var com3 = "APG - NDDC Template";
    var com4 = "BID BOND TEMPLATE_new";
    var com5 = "MTN BG template review";
    var com6 = "EMTS BG TEMPLATE";
    var com7 = "BG - IATA II";
    var com8 = "DRAFT CUSTOM BG";
    var com_1 = "NON BANK STANDARD FORMAT";


    var com9 = "INSPECTION AND MONITORING AGREEMENT";
    var com10 = "LETTER OF HYPOTHECATION BUSINESS";
    var com11 = "LETTER OF HYPOTHECATION LLC";
    var com12 = "LETTER OF PLEDGE ATC(s)";
    var com13 = "LOAN AGREEMENT INDIVIDUAL";
    var com14 = "LOAN AGREEMENT BUSINESS";
    var com15 = "LOAN AGREEMENT LIMTED LIABILITY COMPANIES";
    var com16 = "TRIPARTITE WAREHOUSING AGREEMENT";
    var com17 = "TRUST RECEIPT- BUSINESS";
    var com18 = "TRUST RECEIPT- LIMITED LIABILITY COMPANIES";

    var com19 = "TLM FOR CORPORATE BORROWER AND INDIVIDUAL SURETY";
    var com20 = "TLM FOR CORPORATE BORROWER AND CORPORATE SURETY";
    var com21 = "LEGAL MORTGAGE FOR COMPANY";
    var com22 = "DIRECT LEGAL MORTGAGE FOR INDIVIDUAL";
    var com23 = "ALL ASSET DEBENTURE";
    var inText = "";
    var nameData = "";

    //var dt = $("#Hidden33").val();
    //alert(dt);
    var Text = "";


    var frame1 = "";
    var frameDoc = "";

    if (title === com1) { //print Advance APG Template

        inText = $("#adv").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    } else if (title === com2) { //print airtel
        inText = $("#airtel").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    } else if (title === com3) { //print nddc
        inText = $("#nddc").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    } else if (title === com4) { // print bidbond

        inText = $("#bidbond").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    } else if (title === com5) { // print mtn
        inText = $("#mtn").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    } else if (title === com6) { //print emts
        inText = $("#emts").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    } else if (title === com7) { //print iata 
        inText = $("#iata").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    } else if (title === com8) { //print draftcustom
        inText = $("#draftcustom").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    if (title === com9) { //print draftcustom
        inText = $("#insp_agreement").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com10) { //print draftcustom
        inText = $("#hypo_business").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com11) { //print draftcustom
        inText = $("#hypo_llc").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com12) { //print draftcustom
        inText = $("#pledge").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com13) { //print draftcustom
        inText = $("#loan_ind").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com14) { //print draftcustom
        inText = $("#loan_biz").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com15) { //print draftcustom
        inText = $("#loan_llc").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com16) { //print draftcustom
        inText = $("#wearhouse").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com17) { //print draftcustom
        inText = $("#receipt_biz").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com18) { //print draftcustom
        inText = $("#receipt_llc").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com19) { //start
        inText = $("#tlm_corp_ind").html();
        nameData = $("#Hidden1").val();

        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com20) { //start
        inText = $("#tlm_corp_corp").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com21) { //start
        inText = $("#mort_comp").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com22) { //start
        inText = $("#mort_ind").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
    else if (title === com23) { //start
        inText = $("#asset_deb").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }

    else if (title === com_1) { //start
        inText = $("#nonbankformat").html();
        nameData = $("#Hidden1").val();
        frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute" });
        $("body").append(frame1);
        frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>' + nameData + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(inText);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);
    }
});

$(".postPDF").on("click", function (e) {

    e.preventDefault();
    var title_of_temp = window.Var1; //get title out of window
    var title = $.trim(title_of_temp);
    //alert(title);

     var templateTitle = $("#HiddenField3").val();
     //alert(templateTitle);

    var com1 = "Advanced APG Template";
    var com2 = "Airtel Template Bank Guarantee - 2013";
    var com3 = "APG - NDDC Template";
    var com4 = "BID BOND TEMPLATE_new";
    var com5 = "MTN BG template review";
    var com6 = "EMTS BG TEMPLATE";
    var com7 = "BG - IATA II";
    var com8 = "DRAFT CUSTOM BG";
    var com_1 = "NON BANK STANDARD FORMAT";


    var com9 = "INSPECTION AND MONITORING AGREEMENT";
    var com10 = "LETTER OF HYPOTHECATION BUSINESS";
    var com11 = "LETTER OF HYPOTHECATION LLC";
    var com12 = "LETTER OF PLEDGE ATC(s)";
    var com13 = "LOAN AGREEMENT INDIVIDUAL";
    var com14 = "LOAN AGREEMENT BUSINESS";
    var com15 = "LOAN AGREEMENT LIMTED LIABILITY COMPANIES";
    var com16 = "TRIPARTITE WAREHOUSING AGREEMENT";
    var com17 = "TRUST RECEIPT- BUSINESS";
    var com18 = "TRUST RECEIPT- LIMITED LIABILITY COMPANIES";

    var com19 = "TLM FOR CORPORATE BORROWER AND INDIVIDUAL SURETY";
    var com20 = "TLM FOR CORPORATE BORROWER AND CORPORATE SURETY";
    var com21 = "LEGAL MORTGAGE FOR COMPANY";
    var com22 = "DIRECT LEGAL MORTGAGE FOR INDIVIDUAL";
    var com23 = "ALL ASSET DEBENTURE";
    var inText = "";
    var nameData = "";
    var Text = "";

    var $html = "";
    if (title === com1) {

        inText = $("#adv").html();

        nameData = $("#Hidden1").val();
        $html = '';
        // $html = 'div>' + inText + '</div>';


        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>" + inText + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com2) {
        inText = $("#airtel").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';



        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>" + inText + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();

    } else if (title === com3) {

        inText = $("#nddc").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';



        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com4) {

        inText = $("#bidbond").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';


        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com5) {
        inText = $("#mtn").html();
        nameData = $("#Hidden1").val();
        $html = '';
        // $html = 'div>' + inText + '</div>';


        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com6) {
        inText = $("#emts").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx' id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com7) {
        inText = $("#iata").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com8) {
        inText = $("#draftcustom").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com9) {
        inText = $("#insp_agreement").html();

        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com10) {
        inText = $("#hypo_business").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com11) {
        inText = $("#hypo_llc").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com12) {
        inText = $("#pledge").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com13) {
        inText = $("#loan_ind").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com14) {
        inText = $("#loan_biz").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com15) {
        inText = $("#loan_llc").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx' id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com16) {
        inText = $("#wearhouse").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx' id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com17) {
        inText = $("#receipt_biz").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com18) {
        inText = $("#receipt_llc").html();

        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com19) {
        inText = $("#tlm_corp_ind").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com20) {
        inText = $("#tlm_corp_corp").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com21) {
        inText = $("#mort_comp").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx' id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com22) {
        inText = $("#mort_ind").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx' id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com23) {
        inText = $("#asset_deb").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com_1) {
        inText = $("#nonbankformat").html();
        nameData = $("#Hidden1").val();
        $html = '';
        //$html = 'div>' + inText + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/PdfSave.ashx'  id='tempForm'><textarea id='data' name='data'>'" + inText + "' </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
});


$(".postWord").on("click", function(e) {

    e.preventDefault();
    var title_of_temp = window.Var1; //get title out of window
    var title = $.trim(title_of_temp);

    var templateTitle = $("#HiddenField3").val();

    var com1 = "Advanced APG Template";
    var com2 = "Airtel Template Bank Guarantee - 2013";
    var com3 = "APG - NDDC Template";
    var com4 = "BID BOND TEMPLATE_new";
    var com5 = "MTN BG template review";
    var com6 = "EMTS BG TEMPLATE";
    var com7 = "BG - IATA II";
    var com8 = "DRAFT CUSTOM BG";
    var com_1 = "NON BANK STANDARD FORMAT";


    var com9 = "INSPECTION AND MONITORING AGREEMENT";
    var com10 = "LETTER OF HYPOTHECATION BUSINESS";
    var com11 = "LETTER OF HYPOTHECATION LLC";
    var com12 = "LETTER OF PLEDGE ATC(s)";
    var com13 = "LOAN AGREEMENT INDIVIDUAL";
    var com14 = "LOAN AGREEMENT BUSINESS";
    var com15 = "LOAN AGREEMENT LIMTED LIABILITY COMPANIES";
    var com16 = "TRIPARTITE WAREHOUSING AGREEMENT";
    var com17 = "TRUST RECEIPT- BUSINESS";
    var com18 = "TRUST RECEIPT- LIMITED LIABILITY COMPANIES";

    var com19 = "TLM FOR CORPORATE BORROWER AND INDIVIDUAL SURETY";
    var com20 = "TLM FOR CORPORATE BORROWER AND CORPORATE SURETY";
    var com21 = "LEGAL MORTGAGE FOR COMPANY";
    var com22 = "DIRECT LEGAL MORTGAGE FOR INDIVIDUAL";
    var com23 = "ALL ASSET DEBENTURE";
    var inText = "";
    var nameData = "";
    var Text = "";

    var $html = "";
    if (title === com1) { //Template for adv

        Text = $("#adv").html();
        nameData = $("#Hidden1").val();

        //$html = 'div>' + Text + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();

    } else if (title === com2) { //Template for airtel
        Text = $("#airtel").html();
        nameData = $("#Hidden1").val();

        // $html = 'div>' + Text + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com3) { //Template for nddc
        Text = $("#nddc").html();
        nameData = $("#Hidden1").val();

        // $html = 'div>' + Text + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com4) { //Template for bidbond
        Text = $("#bidbond").html();
        nameData = $("#Hidden1").val();

        // $html = 'div>' + Text + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com5) { //Template for MTN
        Text = $("#mtn").html();
        nameData = $("#Hidden1").val();

        //$html = 'div>' + Text + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com7) { //Template for iata
        Text = $("#iata").html();
        nameData = $("#Hidden1").val();

        // $html = 'div>' + Text + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com6) { //Template for emts
        //alert("emts");
        Text = $("#emts").html();
        nameData = $("#Hidden1").val();

        //$html = 'div>' + Text + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    } else if (title === com8) { //Template for draftcustom
        Text = $("#draftcustom").html();
        nameData = $("#Hidden1").val();

        // $html = 'div>' + Text + '</div>';

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com9) { //Template for draftcustom
        Text = $("#insp_agreement").html();
        //Text = escape(Text);
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com10) { //Template for draftcustom
        Text = $("#hypo_business").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com11) { //Template for draftcustom
        Text = $("#hypo_llc").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com12) { //Template for draftcustom
        Text = $("#pledge").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com13) { //Template for draftcustom
        Text = $("#loan_ind").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com14) { //Template for draftcustom
        Text = $("#loan_biz").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com15) { //Template for draftcustom
        Text = $("#loan_llc").html();
        nameData = $("#Hidden1").val();


        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com16) { //Template for draftcustom
        Text = $("#wearhouse").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx' id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com17) { //Template for draftcustom
        Text = $("#receipt_biz").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com18) { //Template for draftcustom
        Text = $("#receipt_llc").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    if (title === com19) { //Template for draftcustom
        Text = $("#tlm_corp_ind").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx' id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com20) { //Template for draftcustom
        Text = $("#tlm_corp_corp").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + "</textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com21) { //Template for draftcustom
        Text = $("#mort_comp").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com22) { //Template for draftcustom
        Text = $("#mort_ind").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
    else if (title === com23) { //Template for draftcustom
        Text = $("#asset_deb").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }

    else if (title === com_1) { //Template for draftcustom
        Text = $("#nonbankformat").html();
        nameData = $("#Hidden1").val();

        $("body").prepend("<form method='post' action='../components/exports/Word.ashx'  id='tempForm'><textarea id='data' name='data'>" + Text + " </textarea> <input type='hidden' name='nameDataValue' value='" + nameData + "' ><input type='hidden' name='title' value='" + templateTitle + "'/></form>");
        $('#tempForm').submit();
    }
});
