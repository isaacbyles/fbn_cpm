﻿//$("#dataTables").DataTable();

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

$(".buttonactions").click(function () {
    var dataTables = document.getElementById("dataTables");
    if ($(this).attr("id") == "print") {
        // Here We Print
        var title = $(".nav-tabs").find("a").text();
        var d = new Date();
        var printText = "Generated on " + d.toLocaleString();
        var widS = parseInt(screen.width - 100);
        var heiS = parseInt(screen.height - 100);
        var x = window.open('', '', 'toolbar = no, scrollbars = yes, resizable = yes,width=' + widS + ',height=' + heiS);
        x.document.write('<style type="text/css">.printtext {font-family:verdana, sans-serif;font-size:10px;float:right;font-style: italic;} h1 {font-family:verdana, sans-serif;font-weight:400;} table, th, td {font-size:12px;font-family:verdana, sans-serif;border-collapse: collapse; border: thin solid #ccc;} </style>');
        x.document.write('<center><h1>' + title + '</h1></center><table cellpadding=4>' + $(dataTables).html() + '</table><div class="printtext">' + printText + '</div>');
        x.print();
    } else if ($(this).attr("id") == "pdf") {
        // Here we render PDF
//       
            var title = $(".nav-tabs").find("a").text();
            var d = new Date();
            var printText = "Generated on " + d.toLocaleString();
            var $html = '<html><head><style type="text/css">.printtext {font-family:verdana, sans-serif;font-size:10px;float:right;font-style: italic;} h1 {font-family:verdana, sans-serif;font-weight:400;} table, th, td {font-size:12px;font-family:verdana, sans-serif;border-collapse: collapse; border: thin solid #ccc;} </style></head>';
            $html += '<center><h1>' + title + '</h1></center><table cellpadding=4>' + $(dataTables).html() + '</table><div class="printtext">' + printText + '</div>';
            $("body").prepend("<form method='post' action='../components/exports/pdf.ashx'  id='tempForm'><textarea id='data' name='data'>'" + $html + "' </textarea>  </form>");
            $('#tempForm').submit();

        //.remove();
    } else if ($(this).attr("id") == "excel") {
        // Here we generate Excel
        var title = $(".nav-tabs").find("a").text();
        var d = new Date();
        var printText = "Generated on " + d.toLocaleString();
        var $html = '<html><head><style type="text/css">.printtext {font-family:verdana, sans-serif;font-size:10px;float:right;font-style: italic;} h1 {font-family:verdana, sans-serif;font-weight:400;} table, th, td {font-size:12px;font-family:verdana, sans-serif;border-collapse: collapse; border: thin solid #ccc;} </style></head>';
        $html += '<center><h1>' + title + '</h1></center><table cellpadding=4>' + $(dataTables).html() + '</table><div class="printtext">' + printText + '</div>';
        $("<form>")
        .attr("action", '../Components/exports/excel.ashx')
        .attr("method", "post")
        .append(
            $("input").attr("type", "hidden").attr("name", "html").attr("value", $html)
        )
        .appendTo("body")
        .submit();
    }
});