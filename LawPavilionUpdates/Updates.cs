﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using CaseManager.HttpServerTest.Logger;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CaseManager.LawPavilionUpdates
{
    public class Updates
    {

        //get table and date modified
        public const string Supreme = "cases";
        public const string SupremeAnalysis = "analysis";
        public const string CourtOfAppeal = "cases_ca";
        public const string CourtOfAppealAnalysis = "analysis_ca";
        public const string FHC = "cases_fhc";
        public const string FHCAnalysis = "analysis_fhc";
        public const string TAT = "cases_tat";
        public const string TATAnalysis = "analysis_tat";
        public const string NIC = "cases_nic";
        public const string NICAnalysis = "analysis_nic";
        public const string HighCourtFCT = "cases_states";
        public const string HighCourtFCTAnalysis = "analysis_states";
        public const string subsidiaries_legislations = "subsidiaries_legislations";
        public const string laws_sections = "laws_sections";
        public const string area_of_law = "area_of_law";
        public const string library_books = "library_books";
        public const string state_law_section = "state_law_section";
        public const string SCHEDULES = "schedules";
        public const string page_nic = "pages_nic";
        public const string page_state = "pages_states";
        public const string page_tat = "pages_tat";
        public const string state_law_subsubsection = "state_law_subsubsection";
        public const string STATE_LAWS_SCHEDULES = "state_laws_schedules";
        public const string STATE_SUBSIDIARY_LEGISLATION = "state_subsidairy_legislation";
        public const string STATE_SUBSIDIARY_SUBSECTION = "state_subsidiary_subsection";
        public const string STATE_SUBSIDIARY_SUBSUBSECTION = "state_subsidiary_subsubsection";
        public const string SUBSIDIARY_SUBSECTION = "subsidiary_subsection";
        public const string SUBSIDIARY_SUBSUBSECTION = "subsidiary_subsubsection";
        public const string TBL_FORM = "tbl_form";
        public const string REGULATIONS = "regulations";
        public const string REGULATIONS_PARTS = "regulations_part";

        public static string BASE_URL = "http://www.lawpavilionplus.com/cases_update/index.php";

        //, 
        //    SCHEDULES, page_nic, page_state, page_tat,
        //    state_law_subsubsection, STATE_LAWS_SCHEDULES, STATE_SUBSIDIARY_LEGISLATION, STATE_SUBSIDIARY_SUBSECTION,
        //    STATE_SUBSIDIARY_SUBSUBSECTION, SUBSIDIARY_SUBSECTION, SUBSIDIARY_SUBSUBSECTION, TBL_FORM, REGULATIONS,
        //    REGULATIONS_PARTS,FHC
        public string[] _casesss =
        {
            Supreme, CourtOfAppeal, library_books,  TAT, NIC, HighCourtFCT, laws_sections,
            subsidiaries_legislations, state_law_section
        };
       
        private string HttpPostRequest(string url, Dictionary<string, string> postParameters)
        {
            string postData = postParameters.Keys.Aggregate("", (current, key) => current + (HttpUtility.UrlEncode(key) + "=" + HttpUtility.UrlEncode(postParameters[key]) + "&"));

            //  postData.
            HttpWebRequest myHttpWebRequest = (HttpWebRequest) HttpWebRequest.Create(url);
            myHttpWebRequest.Method = "POST";
            myHttpWebRequest.Timeout = 10000;
            myHttpWebRequest.KeepAlive = true;
            byte[] data = Encoding.ASCII.GetBytes(postData);

            myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
            myHttpWebRequest.ContentLength = data.Length;

            Stream requestStream = myHttpWebRequest.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();

            using (HttpWebResponse myHttpWebResponse = (HttpWebResponse) myHttpWebRequest.GetResponse())
            {

                Stream responseStream = myHttpWebResponse.GetResponseStream();


                StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);

                
                string pageContent = myStreamReader.ReadToEnd();
                JObject o = JObject.Parse(pageContent);
                string code = o.First.First.ToString();
                if (code == "200")
                {
                    RootObject obj = JsonConvert.DeserializeObject<RootObject>(pageContent);


                    switch (obj.table_name)
                    {
                        case Supreme:
                            GetAllCases(obj.data, obj.table_name);
                            break;

                        case CourtOfAppeal:
                            GetAllCourtofAppeal(obj.data, obj.table_name);
                            break;
                        case FHC:
                            GetAllFHC(obj.data, obj.table_name);
                            break;
                        case TAT:
                            GetAllTAT(obj.data, obj.table_name);
                            break;
                        case NIC:
                            GetAllNIC(obj.data, obj.table_name);
                            break;
                        case HighCourtFCT:
                            GetAllHighCourtFCT(obj.data, obj.table_name);
                            break;
                        case subsidiaries_legislations:
                            Getsubsidiaries_legislations(obj.data, obj.table_name);
                            break;
                        case laws_sections:
                            Getlaws_sections(obj.data, obj.table_name);
                            break;
                        case CourtOfAppealAnalysis:
                            GetAllCourtofAppealAnalysis(obj.data, obj.table_name);
                            break;
                        case NICAnalysis:
                            GetAllNICAnalysis(obj.data, obj.table_name);
                            break;

                        case HighCourtFCTAnalysis:
                            GetAllStatesAnalysis(obj.data, obj.table_name);
                            break;

                        case TATAnalysis:
                            GetTATAnalysis(obj.data, obj.table_name);
                            break;

                        case library_books:
                            GetAllLibraryBooks(obj.data, obj.table_name);
                            break;

                        case state_law_section:
                            GetAllStateLaw(obj.data, obj.table_name);
                            break;

                        //case SCHEDULES:
                        //    GetAlllaw_Sechudle(obj.data, obj.table_name);
                        //    break;

                        //case page_nic:
                        //    GetAllPagesNIC(obj.data, obj.table_name);
                        //    break;

                        //case page_state:
                        //    GetAllPagesStates(obj.data, obj.table_name);
                        //    break;

                        //case page_tat:
                        //    GetAllPagesTat(obj.data, obj.table_name);
                        //    break;

                        //case REGULATIONS:
                        //    GetAllRegulations(obj.data, obj.table_name);
                        //    break;

                        //case REGULATIONS_PARTS:
                        //    GetAllRegulationsPart(obj.data, obj.table_name);
                        //    break;

                        //case STATE_LAWS_SCHEDULES:
                        //    GetAllSTATE_LAWS_SCHEDULES(obj.data, obj.table_name);
                        //    break;

                        //case STATE_SUBSIDIARY_LEGISLATION:
                        //    GetAllSTATE_SUBSIDIARY_LEGISLATION(obj.data, obj.table_name);
                        //    break;

                        //case STATE_SUBSIDIARY_SUBSECTION:
                        //    GetAllSTATE_SUBSIDIARY_SUBSECTION(obj.data, obj.table_name);
                        //    break;

                        //case STATE_SUBSIDIARY_SUBSUBSECTION:
                        //    GetAllSTATE_SUBSIDIARY_SUBSUBSECTION(obj.data, obj.table_name);
                        //    break;

                        //case TBL_FORM:
                        //    GetAllTBLFORM(obj.data, obj.table_name);
                        //    break;
                    }
                }

                else if (code == "500")
                {
                    pageContent = "Error Occurred While fetching data";
                }

                myStreamReader.Close();
                if (responseStream != null) responseStream.Close();

                myHttpWebResponse.Close();

                return pageContent;
            }
        }

        private void GetAllTBLFORM(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE pk = '" + item.pk +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET title= '" + item.title + "', [title2]= '" +
                                    convertQuotes(item.title2) + "', type='"+item.type+"', court='"+item.court+"', state='"+item.state+"', notes='"+convertQuotes(item.notes)+"', content='"+convertQuotes(item.content)+"' dt_modified = '" + item.dt_modified +
                                    "', deleted = '" + item.deleted +
                                    "' WHERE pk = '" + item.pk + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([title], [title2], [type], court, state, notes,content, deleted,dt_modified) VALUES " +
                                      "('" + item.title + "', '" + convertQuotes(item.title2) + "', '" +
                                      convertQuotes(item.type) + "','" +
                                      convertQuotes(item.court) + "',  '" + item.state + "', '" +
                                      convertQuotes(item.notes) + "', '" + convertQuotes(item.content) + "','" +
                                      item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        private void GetAllSTATE_SUBSIDIARY_SUBSUBSECTION(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE name = '" + item.name +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET subsubsection_number= '" + item.subsubsection_number + "', [subsubsection_content]= '" +
                                    convertQuotes(item.subsubsection_content) + "', dt_modified = '" + item.dt_modified +
                                    "', deleted = '" + item.deleted +
                                    "', dt_modified= '" + item.dt_modified + "' WHERE name = '" + item.name + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([pk], [name], [subsubsection_number], [subsubsection_content], subsection_desc, state, deleted,dt_modified) VALUES " +
                                      "('" + item.pk + "', '" + convertQuotes(item.name) + "', '" +
                                      convertQuotes(item.subsubsection_number) + "','" +
                                      convertQuotes(item.subsubsection_content) + "',  '"+item.subsection_desc+"', '"+item.state+"','" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        private void GetAllSTATE_SUBSIDIARY_SUBSECTION(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE name = '" + item.name +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET section_number= '" + item.section_number + "', [subsection_content]= '" +
                                    convertQuotes(item.subsection_content) + "', dt_modified = '" + item.dt_modified +
                                    "', deleted = '" + item.deleted +
                                    "', dt_modified= '" + item.dt_modified + "' WHERE name = '" + item.name + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([pk], [name], [section_number], [part_number], [subsection_Number], [subsection_content],state, subsection_desc,  deleted,dt_modified) VALUES " +
                                      "('" + item.pk + "', '" + convertQuotes(item.name) + "', '" +
                                      convertQuotes(item.section_number) + "','" +
                                      convertQuotes(item.part_number) + "',  '" + item.subsection_Number + "', '"+item.subsection_content+"', '" + item.state + "','" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        private void GetAllSTATE_SUBSIDIARY_LEGISLATION(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE pk = '" + item.pk +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET attached_to= '" + item.attached_to + "', [name]= '" +
                                    convertQuotes(item.name) + "', type = '" + item.type +
                                    "', long_title = '" + convertQuotes(item.long_title) +
                                    "', dt_modified= '" + item.dt_modified + "' WHERE pk = '" + item.pk + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([attached_to],[name], [type], [long_title], position,status, state,volume,annex, deleted,dt_modified) VALUES " +
                                      "('" + item.attached_to + "', '" + convertQuotes(item.name) + "', '" +
                                      convertQuotes(item.type) + "','" +
                                      convertQuotes(item.long_title) + "',  '"+item.position+"', '"+item.status+"', '" + item.state + "', '" + item.volume + "', '"+item.annex+"','" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        private void GetAllSTATE_LAWS_SCHEDULES(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE name = '" + item.name +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET type= '" + item.type + "', [schedule_number]= '" +
                                    convertQuotes(item.schedule_number) + "', content = '" + convertQuotes(item.content) +
                                    "', deleted = '" + item.deleted +
                                    "',dt_modified = '" + item.dt_modified +
                                    "'  WHERE name = '" + item.name + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([name], [type], [schedule_number], [content], deleted,dt_modified) VALUES " +
                                      "('" + item.name + "', '" + convertQuotes(item.type) + "', '" +
                                      convertQuotes(item.schedule_number) + "','" +
                                      convertQuotes(item.content) + "','" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        private void GetAllRegulationsPart(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE agency = '" + item.agency +
                                  "' and part = '" + item.part + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET agency= '" + item.agency + "', [type]= '" +
                                    convertQuotes(item.type) + "', title = '" + convertQuotes(item.title) +
                                    "', part='" + item.part + "', part_description='" + item.part_description +
                                    "', deleted = '" + item.deleted +
                                    "',dt_modified = '" + item.dt_modified +
                                    "'  WHERE part = '" + item.part + "' and agency='" + item.agency + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([agency], [type], [title], [part],part_description, part_content, deleted,dt_modified) VALUES " +
                                      "('" + item.agency + "', '" + convertQuotes(item.type) + "', '" +
                                      convertQuotes(item.title) + "','" +
                                      convertQuotes(item.part) + "','" + convertQuotes(item.part_description) + "', '" +
                                      convertQuotes(item.part_content) + "', '" + item.deleted + "', '" +
                                      item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        private void GetAllRegulations(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE  pk = '" + item.pk + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET agency= '" + item.agency + "', [type]= '" +
                                    convertQuotes(item.type) + "', table_of_content = '" + convertQuotes(item.table_of_content) +
                                    "', front_matter='" + convertQuotes(item.front_matter) + "', end_matter='" + convertQuotes(item.end_matter) +
                                    "', deleted = '" + item.deleted +
                                    "',dt_modified = '" + item.dt_modified +
                                    "'  WHERE pk = '" + item.pk + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([agency],[year], [type],[table_of_content], [front_matter],[end_matter], [content], [schedule], [long_title],[short_title],[comment], deleted,dt_modified) VALUES " +
                                      "('" + item.agency + "', '"+item.year+"' ,'" + convertQuotes(item.type) + "', '" +
                                      convertQuotes(item.table_of_content) + "','" +
                                      convertQuotes(item.front_matter) + "','" + convertQuotes(item.end_matter) + "', '" +
                                      convertQuotes(item.content) + "', '" + convertQuotes(item.schedule) + "' , '"+convertQuotes(item.long_title)+"', '"+convertQuotes(item.short_title)+"','"+convertQuotes(item.comment)+"' ,'" + item.deleted + "', '" +
                                      item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        private void GetAllCases(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                //SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (item.suitno == "SC.524/2014")
                    {

                    }
                    else
                    {
                        // if data exist
                        //update the previous data
                        string update = @"UPDATE " + table + "  SET [date]= '" + item.date + "', case_title= '" +
                                        convertQuotes(item.case_title) + "', case_title2 = '" +
                                        convertQuotes(item.case_title2) + "', summary = '" + convertQuotes(item.summary) +
                                        "', fullreport = '" + convertQuotes(item.fullreport) + "', othercitation1= '" +
                                        convertQuotes(item.othercitation1) + "', othercitation2= '" +
                                        convertQuotes(item.othercitation2) + "', othercitation3='" +
                                        convertQuotes(item.othercitation3) + "', appelant='" +
                                        convertQuotes(item.appelant) + "', defendant='" + convertQuotes(item.defendant) +
                                        "', appellant_counsel='" + convertQuotes(item.appellant_counsel) + "', judge1='" +
                                        convertQuotes(item.judge1) + "', judge2='" + convertQuotes(item.judge2) +
                                        "', judge3='" + convertQuotes(item.judge3) + "', judge4='" +
                                        convertQuotes(item.judge4) + "', judge5='" + convertQuotes(item.judge5) +
                                        "', judge6='" + convertQuotes(item.judge6) + "', judge7='" +
                                        convertQuotes(item.judge7) + "', judge8='" + convertQuotes(item.Judge8) +
                                        "', status = '" + item.status + "', html = '" + convertQuotes(item.html) +
                                        "', dt_modified='" + (item.dt_modified) + "', deleted = '" + item.deleted +
                                        "' WHERE suitno = '" + item.suitno + "'";

                        using (
                            SqlConnection myConnection =
                                new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                        using (SqlCommand myCommand = new SqlCommand())
                        {

                            myConnection.Open();
                            myCommand.CommandType = CommandType.Text;
                            myCommand.CommandTimeout = 0;
                            myCommand.CommandText = update;
                            myCommand.Connection = myConnection;

                            int i = myCommand.ExecuteNonQuery();
                            if (i == 1)
                            {
                                Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                            }
                            else
                            {
                                Logger.writelog("Error Occurred while Updating existing data " + " " + " " +
                                                DateTime.Now);
                            }
                            myConnection.Close();
                        }
                        myconnection.Close();

                    }
                }
                else
                {
                    //SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " (date, suitno, case_title, case_title2, summary, fullreport, fullreport_html,othercitation1, othercitation2, othercitation3, appelant, defendant, appellant_counsel, judge1,judge2, judge3, judge4, judge5, judge6, judge7, judge8, status,html,dt_modified,deleted) VALUES " +
                                      "('" + item.date + "', '" + convertQuotes(item.suitno) + "', '" +
                                      convertQuotes(item.case_title) + "','" +
                                      convertQuotes(item.case_title2) + "', '" + convertQuotes(item.summary) +
                                      "', '" + convertQuotes(item.fullreport) + "', '" +
                                      convertQuotes(item.fullreport_html) + "', '" +
                                      convertQuotes(item.othercitation1) + "', '" +
                                      convertQuotes(item.othercitation2) + "', '" +
                                      convertQuotes(item.othercitation3) + "', '" + convertQuotes(item.appelant) +
                                      "', '" + convertQuotes(item.defendant) + "', '" +
                                      convertQuotes(item.appellant_counsel) + "', '" +
                                      convertQuotes(item.judge1) + "', '" + convertQuotes(item.judge2) + "', '" +
                                      convertQuotes(item.judge3) + "', '" +
                                      convertQuotes(item.judge4) + "', '" + convertQuotes(item.judge5) + "', '" +
                                      convertQuotes(item.judge6) + "', '" +
                                      convertQuotes(item.judge7) + "', '" + convertQuotes(item.Judge8) + "', '" +
                                      convertQuotes(item.status) + "', '" + convertQuotes(item.html) +
                                      "', '" + item.dt_modified + "', '" + item.deleted + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        conn.Close();
                    }



                    #endregion

                }
            }
        }


        public void GetAllCourtofAppeal(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region

                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                     myconnection.Close();
                    myconnection.Dispose();
                    // if data exist
                    //update the previous data

                    string updates = @"UPDATE " + table + "  SET date= '" + item.date + "', case_title= '" +
                                    item.case_title + "', case_title2 = '" +
                                    item.case_title2 + "', summary = '" + convertQuotes(item.summary) +
                                    "', fullreport = '" + convertQuotes(item.fullreport) + "', fullreport_html='" +
                                    convertQuotes(item.fullreport_html) + "', othercitation1= '" +
                                    item.othercitation1 + "', othercitation2= '" +
                                    item.othercitation2 + "', othercitation3='" +
                                    item.othercitation3 + "', appelant='" +
                                    item.appelant + "', defendant='" + item.defendant +
                                    "', appellant_counsel='" + item.appellant_counsel + "', judge1='" +
                                    item.judge1 + "', judge2='" + item.judge2 +
                                    "', judge3='" + item.judge3 + "', judge4='" +
                                    item.judge4 + "', judge5='" + item.judge5 +
                                    "', judge6='" + item.judge6 + "', judge7='" +
                                    item.judge7 + "', judge8='" + item.Judge8 +
                                    "', status = '" + item.status + "', html = '" + item.html +
                                    "', division = '" + item.division + "',dt_modified='" +
                                    item.dt_modified + "', deleted = '" + item.deleted + "' WHERE suitno='" +
                                    item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    {
                        myConnection.Open();
                        using (SqlTransaction tran = myConnection.BeginTransaction())
                        {
                            try
                            {
                                using (SqlCommand myCommand = myConnection.CreateCommand())
                                {
                                    myCommand.CommandText = updates;
                                    myCommand.Transaction = tran as SqlTransaction;
                                   
                                    myCommand.CommandTimeout = 50;

                                    int i = myCommand.ExecuteNonQuery();

                                    if (i == 1)
                                    {
                                        Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                                    }
                                    else
                                    {
                                        Logger.writelog("Error Occurred while Updating existing data " + " " + " " +
                                                        DateTime.Now);
                                    }
                                    myConnection.Close();
                                }
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                Logger.writelog("CATCH eRROR :    Error Occurred while Updating existing data " + " " + " " +
                                                        DateTime.Now);
                            }

                            myconnection.Close();


                        }
                    }
                }
                else
                {
                    //SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " (date, suitno, case_title, case_title2, summary, fullreport, fullreport_html,othercitation1, othercitation2, othercitation3, appelant, defendant, appellant_counsel, judge1,judge2, judge3, judge4, judge5, judge6, judge7, judge8, status,html,division,dt_modified,deleted) VALUES " +
                                      "('" + item.date + "', '" + convertQuotes(item.suitno) + "', '" +
                                      convertQuotes(item.case_title) + "','" +
                                      convertQuotes(item.case_title2) + "', '" + convertQuotes(item.summary) +
                                      "', '" + convertQuotes(item.fullreport) + "', '" +
                                      convertQuotes(item.fullreport_html) + "', '" +
                                      convertQuotes(item.othercitation1) + "', '" +
                                      convertQuotes(item.othercitation2) + "', '" +
                                      convertQuotes(item.othercitation3) + "', '" + convertQuotes(item.appelant) +
                                      "', '" + convertQuotes(item.defendant) + "', '" +
                                      convertQuotes(item.appellant_counsel) + "', '" +
                                      convertQuotes(item.judge1) + "', '" + convertQuotes(item.judge2) + "', '" +
                                      convertQuotes(item.judge3) + "', '" +
                                      convertQuotes(item.judge4) + "', '" + convertQuotes(item.judge5) + "', '" +
                                      convertQuotes(item.judge6) + "', '" +
                                      convertQuotes(item.judge7) + "', '" + convertQuotes(item.Judge8) + "', '" +
                                      convertQuotes(item.status) + "', '" + convertQuotes(item.html) +
                                      "', '" + convertQuotes(item.division) + "', '" + item.dt_modified + "', '" +
                                      item.deleted + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;

                        cmd.CommandTimeout = 1000;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        conn.Close();
                    }



                    #endregion

                }
            }
            }
        

        public void GetAllAnalysis(List<LPV> court, string table) //analysis
        {
            foreach (var item in court)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "' and dt_modified ='" + item.dt_modified + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    string update = @"UPDATE " + table + "  SET year='" + item.year + "', date= '" + item.date +
                                    "', [legal head]= '" +
                                    item.legal_head + "', subjectmatter = '" +
                                    item.subjectmatter + "', issues1 = '" + item.issues1 +
                                    "', principle = '" + item.principle + "', referencedcases1='" +
                                    item.referencedcases1 + "', referencedcases2= '" +
                                    item.referencedcases2 + "', referencedcases3= '" +
                                    item.referencedcases3 + "', referencedcases4='" +
                                    item.referencedcases4 + "', referencedcases5='" +
                                    item.referencedcases5 + "', referencedcases6='" + item.referencedcases6 +
                                    "', referencedcases7='" + item.referencedcases7 + "', referencedcases8='" +
                                    item.referencedcases8 + "', referencedcases9='" + item.referencedcases9 +
                                    "', referencedcases10='" + item.referencedcases10 + "', referencedcases11='" +
                                    item.referencedcases11 + "', referencedcases12='" + item.referencedcases12 +
                                    "', referencedcases13='" + item.referencedcases13 + "', referencedcases14='" +
                                    item.referencedcases14 + "', referencedcases15='" + item.referencedcases15 +
                                    "', referencedcases16 = '" + item.referencedcases16 + "', referencedcases17 = '" +
                                    item.referencedcases17 +
                                    "', referencedcases18 = '" + item.referencedcases18 + "',referencedcases19='" +
                                    item.referencedcases19 + "', referencedcases20 = '" + item.referencedcases20 +
                                    "' , referencedcases21 = '" + item.referencedcases21 + "', referencedcases22  = '" +
                                    item.referencedcases22 + "', referencedcases23 = '" + item.referencedcases23 +
                                    "', referencedcases24='" + item.referencedcases24 + "', referencedcases25='" +
                                    item.referencedcases25 + "', referencedcases26 ='" + item.referencedcases26 +
                                    "', referencedcases27='" + item.referencedcases27 + "', referencedcases28='" +
                                    item.referencedcases28 + "', referencedcases29='" + item.referencedcases29 +
                                    "', referencedcases30='" + item.referencedcases30 + "', referencedcases31='" +
                                    item.referencedcases31 + "', referencedcases32 = '" + item.referencedcases32 +
                                    "', referencedcases33='" + item.referencedcases33 + "', referencedcases34='" +
                                    item.referencedcases34 + "', referencedcases35='" + item.referencedcases35 +
                                    "', referencedcases36='" + item.referencedcases36 + "', referencedcases37='" +
                                    item.referencedcases37 + "', referencedcases38='" + item.referencedcases38 +
                                    "', referencedcases39='" + item.referencedcases39 + "', referencedcases40='" +
                                    item.referencedcases40 + "', referencedcases41='" + item.referencedcases41 +
                                    "', suitno1='" + item.suitno1 + "', suitno2='" + item.suitno2 + "', suitno3='" +
                                    item.suitno3 + "', suitno4='" + item.suitno4 + "', suitno5='" + item.suitno5 +
                                    "', suitno6='" + item.suitno6 + "', suitno7='" + item.suitno7 + "', suitno8='" +
                                    item.suitno8 + "', suitno9='" + item.suitno9 + "', suitno10='" + item.suitno11 +
                                    "', suitno12='" + item.suitno12 + "', suitno13='" + item.suitno13 + "', suitno14='" +
                                    item.suitno14 + "', suitno15='" + item.suitno15 + "', suitno16='" + item.suitno16 +
                                    "', suitno17='" + item.suitno17 + "', suitno18='" + item.suitno18 + "', suitno19='" +
                                    item.suitno19 + "', suitno20='" + item.suitno20 + "', suitno21='" + item.suitno21 +
                                    "', suitno22='" + item.suitno22 + "', suitno23='" + item.suitno23 +
                                    "', suitno24='" + item.suitno24 + "', suitno25='" + item.suitno25 + "', suitno26='" +
                                    item.suitno26 + "', suitno27='" + item.suitno27 + "', suitno28='" + item.suitno28 +
                                    "', suitno29='" + item.suitno29 + "', suitno30='" + item.suitno30 + "', suitno31='" +
                                    item.suitno31 + "', suitno32='" + item.suitno32 + "', suitno33='" + item.suitno33 +
                                    "', suitno34='" + item.suitno34 + "', suitno35='" + item.suitno35 + "', suitno36='" +
                                    item.suitno36 + "', suitno37='" + item.suitno37 + "', suitno38='" + item.suitno38 +
                                    "', suitno39='" + item.suitno39 + "', suitno40='" + item.suitno40 + "', suitno41='" +
                                    item.suitno41 + "' , statutes1='" + item.statutes1 + "', statutes2='" +
                                    item.statutes2 + "', statutes3='" + item.statutes3 + "', statutes4='" +
                                    item.statutes4 + "', statutes5='" + item.statutes5 + "', statutes6='" +
                                    item.statutes6 + "', statutes7='" + item.statutes7 + "', statutes8='" +
                                    item.statutes8 + "', statutes9='" + item.statutes9 + "', statutes10='" +
                                    item.statutes10 + "', statutes11='" + item.statutes11 + "', statutes12='" +
                                    item.statutes12 + "', statutes13='" + item.statutes13 + "', statutes14='" +
                                    item.statutes14 + "', statutes15='" + item.statutes15 + "', overruled='" +
                                    item.overruled + "',overruled_case='" + item.overruled_case +
                                    "', overruled_suitno='" + item.overruled_suitno + "', additional_note='" +
                                    item.additional_note + "', pk='" + item.pk + "', anchored='" + item.anchored +
                                    "', dt_modified='" + item.dt_modified + "', deleted='" + item.deleted +
                                    "' WHERE suitno='" +
                                    item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 500;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }

                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " (year, date, suitno, [legal head], subjectmatter, issues1, principle,referencedcases1, referencedcases2, referencedcases3, referencedcases4, referencedcases5, referencedcases6, referencedcases7,referencedcases8, referencedcases9, referencedcases10, referencedcases11, referencedcases12, referencedcases13, referencedcases14, referencedcases15,referencedcases16,referencedcases17,referencedcases18,referencedcases19,referencedcases20,referencedcases21,referencedcases22,referencedcases23,referencedcases24,referencedcases25,referencedcases26,referencedcases27,referencedcases28,referencedcases29,referencedcases30,referencedcases31,referencedcases32,referencedcases33,referencedcases34,referencedcases35,referencedcases36,referencedcases37,referencedcases38,referencedcases39,referencedcases40,referencedcases41,suitno1,suitno2,suitno3,suitno4,suitno5,suitno6,suitno7,suitno8,suitno9,suitno10,suitno11,suitno12,suitno13,suitno14,suitno15,suitno16,suitno17,suitno18,suitno19,suitno20,suitno21,suitno22,suitno23,suitno24,suitno25,suitno26,suitno27,suitno28,suitno29,suitno30,suitno31,suitno32,suitno33,suitno34,suitno35,suitno36,suitno37,suitno38,suitno39,suitno40,suitno41,statutes1,statutes2,statutes3,statutes4,statutes5,statutes6,statutes7,statutes8,statutes9,statutes10,statutes11,statutes12,statutes13,statutes14,statutes15,overruled,overruled_case,overruled_suitno,additional_note,pk,anchored,dt_modified,deleted) VALUES " +
                                      "('" + item.year + "','" + item.date + "', '" + item.suitno + "', '" +
                                      item.legal_head + "','" +
                                      item.subjectmatter + "', '" + item.issues1 +
                                      "', '" + item.principle + "', '" +
                                      item.referencedcases1 + "', '" +
                                      item.referencedcases2 + "', '" +
                                      item.referencedcases3 + "', '" +
                                      item.referencedcases4 + "', '" + item.referencedcases5 +
                                      "', '" + item.referencedcases6 + "', '" +
                                      item.referencedcases7 + "', '" +
                                      item.referencedcases8 + "', '" + item.referencedcases9 + "', '" +
                                      item.referencedcases10 + "', '" +
                                      item.referencedcases11 + "', '" + item.referencedcases12 + "', '" +
                                      item.referencedcases13 + "', '" +
                                      item.referencedcases14 + "', '" + item.referencedcases15 + "', '" +
                                      item.referencedcases16 + "', '" + item.referencedcases17 +
                                      "', '" + item.referencedcases18 + "', '" + item.referencedcases19 + "', '" +
                                      item.referencedcases20 + "', '" +
                                      item.referencedcases21 + "','" +
                                      item.referencedcases22 + "','" +
                                      item.referencedcases23 + "','" +
                                      item.referencedcases24 + "','" +
                                      item.referencedcases25 + "','" +
                                      item.referencedcases26 + "','" +
                                      item.referencedcases27 + "','" +
                                      item.referencedcases28 + "','" +
                                      item.referencedcases29 + "','" +
                                      item.referencedcases30 + "','" +
                                      item.referencedcases31 + "','" +
                                      item.referencedcases32 + "','" +
                                      item.referencedcases33 + "','" +
                                      item.referencedcases34 + "','" +
                                      item.referencedcases35 + "','" +
                                      item.referencedcases36 + "','" +
                                      item.referencedcases37 + "','" +
                                      item.referencedcases38 + "','" +
                                      item.referencedcases39 + "','" +
                                      item.referencedcases40 + "','" +
                                      item.referencedcases41 + "','" +
                                      item.suitno1 + "','" +
                                      item.suitno2 + "','" +
                                      item.suitno3 + "','" +
                                      item.suitno4 + "','" +
                                      item.suitno5 + "','" +
                                      item.suitno6 + "','" +
                                      item.suitno7 + "','" +
                                      item.suitno8 + "','" +
                                      item.suitno9 + "','" +
                                      item.suitno10 + "','" +
                                      item.suitno11+ "','" +
                                      item.suitno12 + "','" +
                                      item.suitno13 + "','" +
                                      item.suitno14 + "','" +
                                      item.suitno15+ "','" +
                                      item.suitno16 + "','" +
                                      item.suitno17 + "','" +
                                      item.suitno18 + "','" +
                                      item.suitno19 + "','" +
                                      item.suitno20 + "','" +
                                      item.suitno21 + "','" +
                                      item.suitno22 + "','" +
                                      item.suitno23 + "','" +
                                      item.suitno24 + "','" +
                                      item.suitno25 + "','" +
                                      item.suitno26 + "','" +
                                      item.suitno27 + "','" +
                                      item.suitno28 + "','" +
                                      item.suitno29 + "','" +
                                      item.suitno30 + "','" +
                                      item.suitno31 + "','" +
                                      item.suitno32 + "','" +
                                      item.suitno33 + "','" +
                                      item.suitno34 + "','" +
                                      item.suitno35 + "','" +
                                      item.suitno36 + "','" +
                                      item.suitno37 + "','" +
                                      item.suitno38 + "','" +
                                      item.suitno39 + "','" +
                                      item.suitno40 + "','" +
                                      item.suitno41 + "','" +
                                      item.statutes1 + "','" +
                                      item.statutes2 + "','" +
                                      item.statutes3 + "','" +
                                      item.statutes4 + "','" +
                                      item.statutes5 + "','" +
                                      item.statutes6 + "','" +
                                      item.statutes7 + "','" +
                                      item.statutes8 + "','" +
                                      item.statutes9 + "','" +
                                      item.statutes10 + "','" +
                                      item.statutes11+ "','" +
                                      item.statutes12 + "','" +
                                      item.statutes13 + "', '" +
                                      item.statutes14 + "','" +
                                      item.statutes15 + "','" +
                                      item.overruled + "','" +
                                      item.overruled_case + "','" +
                                      item.overruled_suitno + "','" +
                                      item.additional_note + "','" +
                                      item.pk + "','" +
                                      item.anchored + "','" +
                                      item.dt_modified + "','" +
                                      item.deleted + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                    #endregion

                }
            }

        }

        public void GetAllFHC(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region

                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data
                    myconnection.Close();
                    myconnection.Dispose();

                    string updates = @"UPDATE " + table + "  SET date= '" + item.date + "', case_title= '" +
                                     item.case_title + "', case_title2 = '" +
                                     item.case_title2 + "', summary = '" + convertQuotes(item.summary) +
                                     "', fullreport = '" + convertQuotes(item.fullreport) + "', fullreport_html='" +
                                     convertQuotes(item.fullreport_html) + "', othercitation1= '" +
                                     item.othercitation1 + "', othercitation2= '" +
                                     item.othercitation2 + "', othercitation3='" +
                                     item.othercitation3 + "', appelant='" +
                                     item.appelant + "', defendant='" + item.defendant +
                                     "', appellant_counsel='" + item.appellant_counsel + "', judge1='" +
                                     item.judge1 + "', judge2='" + item.judge2 +
                                     "', judge3='" + item.judge3 + "', judge4='" +
                                     item.judge4 + "', judge5='" + item.judge5 +
                                     "', judge6='" + item.judge6 + "', judge7='" +
                                     item.judge7 + "', judge8='" + item.Judge8 +
                                     "', status = '" + item.status + "', html = '" + item.html +
                                     "', division = '" + item.division + "',stage='" + item.stage + "',done_by='" +
                                     item.done_by + "', appeal_status='" + item.appeal_status + "', type='" + item.type +
                                     "', parties='" + item.parties + "', case_origin='" + item.case_origin +
                                     "', republish='" + item.republish + "',dt_modified='" +
                                     item.dt_modified + "', deleted = '" + item.deleted + "' WHERE suitno='" +
                                     item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))

                    {
                        myConnection.Open();
                        using (SqlTransaction tran = myConnection.BeginTransaction())
                        {
                            try
                            {
                                using (SqlCommand myCommand = myConnection.CreateCommand())
                                {
                                    myCommand.CommandText = updates;
                                    myCommand.Transaction = tran as SqlTransaction;

                                    myCommand.CommandTimeout = 50;

                                    int i = myCommand.ExecuteNonQuery();

                                    if (i == 1)
                                    {
                                        Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                                    }
                                    else
                                    {
                                        Logger.writelog("Error Occurred while Updating existing data " + " " + " " +
                                                        DateTime.Now);
                                    }
                                    myConnection.Close();
                                }
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                Logger.writelog("CATCH eRROR :    Error Occurred while Updating existing data " + " " +
                                                " " +
                                                DateTime.Now);
                            }
                            myconnection.Close();


                        }
                    }
                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " (date, suitno, case_title, case_title2, summary, fullreport, fullreport_html,othercitation1, othercitation2, othercitation3, appelant, defendant, appellant_counsel, judge1,judge2, judge3, judge4, judge5, judge6, judge7, judge8, status,html,division,stage,done_by,appeal_status,type,parties,case_origin,republish,deleted,dt_modified) VALUES " +
                                      "('" + item.date + "', '" + convertQuotes(item.suitno) + "', '" +
                                      convertQuotes(item.case_title) + "','" +
                                      convertQuotes(item.case_title2) + "', '" + convertQuotes(item.summary) +
                                      "', '" + convertQuotes(item.fullreport) + "', '" +
                                      convertQuotes(item.fullreport_html) + "', '" +
                                      convertQuotes(item.othercitation1) + "', '" +
                                      convertQuotes(item.othercitation2) + "', '" +
                                      convertQuotes(item.othercitation3) + "', '" + convertQuotes(item.appelant) +
                                      "', '" + convertQuotes(item.defendant) + "', '" +
                                      convertQuotes(item.appellant_counsel) + "', '" +
                                      convertQuotes(item.judge1) + "', '" + convertQuotes(item.judge2) + "', '" +
                                      convertQuotes(item.judge3) + "', '" +
                                      convertQuotes(item.judge4) + "', '" + convertQuotes(item.judge5) + "', '" +
                                      convertQuotes(item.judge6) + "', '" +
                                      convertQuotes(item.judge7) + "', '" + convertQuotes(item.Judge8) + "', '" +
                                      convertQuotes(item.status) + "', '" + convertQuotes(item.html) +
                                      "', '" + convertQuotes(item.division) + "','" + convertQuotes(item.stage) +
                                      "' , '" + item.done_by + "' , '" + item.appeal_status + "', '" + item.type +
                                      "', '" + item.parties + "', '" + item.case_origin + "', '" + item.republish +
                                      "', '" + item.deleted + "', '" + item.dt_modified + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;

                        cmd.CommandTimeout = 1000;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        conn.Close();
                    }



                    #endregion

                }
            }
        }


        public void GetAllTAT(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region

                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data
                    myconnection.Close();
                    myconnection.Dispose();
                    string update = @"UPDATE " + table + "  SET date= '" + item.date + "', case_title= '" +
                                    item.case_title + "', case_title2 = '" +
                                    item.case_title2 + "', summary = '" + convertQuotes(item.summary) +
                                    "', fullreport = '" + convertQuotes(item.fullreport) + "', fullreport_html='" +
                                    convertQuotes(item.fullreport_html) + "', othercitation1= '" +
                                    item.othercitation1 + "', othercitation2= '" +
                                    item.othercitation2 + "', othercitation3='" +
                                    item.othercitation3 + "', appelant='" +
                                    item.appelant + "', defendant='" + item.defendant +
                                    "', appellant_counsel='" + item.appellant_counsel + "', judge1='" +
                                    item.judge1 + "', judge2='" + item.judge2 +
                                    "', judge3='" + item.judge3 + "', judge4='" +
                                    item.judge4 + "', judge5='" + item.judge5 +
                                    "', judge6='" + item.judge6 + "', judge7='" +
                                    item.judge7 + "', judge8='" + item.Judge8 +
                                    "', status = '" + item.status + "', html = '" + item.html +
                                    "', division = '" + item.division + "',stage='" + item.stage + "',done_by='" +
                                    item.done_by + "', appeal_status='" + item.appeal_status + "', type='" + item.type +
                                    "', parties='" + item.parties + "', case_origin='" + item.case_origin +
                                    "', republish='" + item.republish + "',dt_modified='" +
                                    item.dt_modified + "', deleted = '" + item.deleted + "' WHERE suitno='" +
                                    item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    {
                        {
                            myConnection.Open();
                            using (SqlTransaction tran = myConnection.BeginTransaction())
                            {
                                try
                                {
                                    using (SqlCommand myCommand = myConnection.CreateCommand())
                                    {
                                        myCommand.CommandText = update;
                                        myCommand.Transaction = tran as SqlTransaction;

                                        myCommand.CommandTimeout = 50;

                                        int i = myCommand.ExecuteNonQuery();

                                        if (i == 1)
                                        {
                                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                                        }
                                        else
                                        {
                                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " +
                                                            DateTime.Now);
                                        }
                                        myConnection.Close();
                                    }
                                    tran.Commit();
                                }
                                catch (Exception ex)
                                {
                                    Logger.writelog("CATCH eRROR :    Error Occurred while Updating existing data " + " " +
                                                    " " +
                                                    DateTime.Now);
                                }
                                myconnection.Close();


                            }
                        }
                    }
                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " (date, suitno, case_title, case_title2, summary, fullreport, fullreport_html,othercitation1, othercitation2, othercitation3, appelant, defendant, appellant_counsel, judge1,judge2, judge3, judge4, judge5, judge6, judge7, judge8, status,html,division,stage,done_by,appeal_status,type,parties,case_origin,republish,deleted,dt_modified) VALUES " +
                                      "('" + item.date + "', '" + convertQuotes(item.suitno) + "', '" +
                                      convertQuotes(item.case_title) + "','" +
                                      convertQuotes(item.case_title2) + "', '" + convertQuotes(item.summary) +
                                      "', '" + convertQuotes(item.fullreport) + "', '" +
                                      convertQuotes(item.fullreport_html) + "', '" +
                                      convertQuotes(item.othercitation1) + "', '" +
                                      convertQuotes(item.othercitation2) + "', '" +
                                      convertQuotes(item.othercitation3) + "', '" + convertQuotes(item.appelant) +
                                      "', '" + convertQuotes(item.defendant) + "', '" +
                                      convertQuotes(item.appellant_counsel) + "', '" +
                                      convertQuotes(item.judge1) + "', '" + convertQuotes(item.judge2) + "', '" +
                                      convertQuotes(item.judge3) + "', '" +
                                      convertQuotes(item.judge4) + "', '" + convertQuotes(item.judge5) + "', '" +
                                      convertQuotes(item.judge6) + "', '" +
                                      convertQuotes(item.judge7) + "', '" + convertQuotes(item.Judge8) + "', '" +
                                      convertQuotes(item.status) + "', '" + convertQuotes(item.html) +
                                      "', '" + convertQuotes(item.division) + "','" + convertQuotes(item.stage) +
                                      "' , '" + item.done_by + "' , '" + item.appeal_status + "', '" + item.type +
                                      "', '" + item.parties + "', '" + item.case_origin + "', '" + item.republish +
                                      "', '" + item.deleted + "', '" + item.dt_modified + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;

                        cmd.CommandTimeout = 1000;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        conn.Close();
                    }



                #endregion

                }
            }
        }


        public void GetAllNIC(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region

                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data
                    myconnection.Close();
                    myconnection.Dispose();
                    string update = @"UPDATE " + table + "  SET date= '" + item.date + "', case_title= '" +
                                    item.case_title + "', case_title2 = '" +
                                    item.case_title2 + "', summary = '" + convertQuotes(item.summary) +
                                    "', fullreport = '" + convertQuotes(item.fullreport) + "', fullreport_html='" +
                                    convertQuotes(item.fullreport_html) + "', othercitation1= '" +
                                    item.othercitation1 + "', othercitation2= '" +
                                    item.othercitation2 + "', othercitation3='" +
                                    item.othercitation3 + "', appelant='" +
                                    item.appelant + "', defendant='" + item.defendant +
                                    "', appellant_counsel='" + item.appellant_counsel + "', judge1='" +
                                    item.judge1 + "', judge2='" + item.judge2 +
                                    "', judge3='" + item.judge3 + "', judge4='" +
                                    item.judge4 + "', judge5='" + item.judge5 +
                                    "', judge6='" + item.judge6 + "', judge7='" +
                                    item.judge7 + "', judge8='" + item.Judge8 +
                                    "', status = '" + item.status + "', html = '" + item.html +
                                    "', division = '" + item.division + "',stage='" + item.stage + "',done_by='" +
                                    item.done_by + "', appeal_status='" + item.appeal_status + "', type='" + item.type +
                                    "', parties='" + item.parties + "', case_origin='" + item.case_origin +
                                    "', republish='" + item.republish + "',dt_modified='" +
                                    item.dt_modified + "', deleted = '" + item.deleted + "' WHERE suitno='" +
                                    item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    {
                        myConnection.Open();
                        using (SqlTransaction tran = myConnection.BeginTransaction())
                        {
                            try
                            {
                                using (SqlCommand myCommand = myConnection.CreateCommand())
                                {
                                    myCommand.CommandText = update;
                                    myCommand.Transaction = tran as SqlTransaction;

                                    myCommand.CommandTimeout = 50;

                                    int i = myCommand.ExecuteNonQuery();

                                    if (i == 1)
                                    {
                                        Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                                    }
                                    else
                                    {
                                        Logger.writelog("Error Occurred while Updating existing data " + " " + " " +
                                                        DateTime.Now);
                                    }
                                    myConnection.Close();
                                }
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                Logger.writelog("CATCH eRROR :    Error Occurred while Updating existing data " + " " +
                                                " " +
                                                DateTime.Now);
                            }
                            myconnection.Close();


                        }
                    }
                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " (date, suitno, case_title, case_title2, summary, fullreport, fullreport_html,othercitation1, othercitation2, othercitation3, appelant, defendant, appellant_counsel, judge1,judge2, judge3, judge4, judge5, judge6, judge7, judge8, status,html,division,stage,done_by,appeal_status,type,parties,case_origin,republish,deleted,dt_modified) VALUES " +
                                      "('" + item.date + "', '" + convertQuotes(item.suitno) + "', '" +
                                      convertQuotes(item.case_title) + "','" +
                                      convertQuotes(item.case_title2) + "', '" + convertQuotes(item.summary) +
                                      "', '" + convertQuotes(item.fullreport) + "', '" +
                                      convertQuotes(item.fullreport_html) + "', '" +
                                      convertQuotes(item.othercitation1) + "', '" +
                                      convertQuotes(item.othercitation2) + "', '" +
                                      convertQuotes(item.othercitation3) + "', '" + convertQuotes(item.appelant) +
                                      "', '" + convertQuotes(item.defendant) + "', '" +
                                      convertQuotes(item.appellant_counsel) + "', '" +
                                      convertQuotes(item.judge1) + "', '" + convertQuotes(item.judge2) + "', '" +
                                      convertQuotes(item.judge3) + "', '" +
                                      convertQuotes(item.judge4) + "', '" + convertQuotes(item.judge5) + "', '" +
                                      convertQuotes(item.judge6) + "', '" +
                                      convertQuotes(item.judge7) + "', '" + convertQuotes(item.Judge8) + "', '" +
                                      convertQuotes(item.status) + "', '" + convertQuotes(item.html) +
                                      "', '" + convertQuotes(item.division) + "','" + convertQuotes(item.stage) +
                                      "' , '" + item.done_by + "' , '" + item.appeal_status + "', '" + item.type +
                                      "', '" + item.parties + "', '" + item.case_origin + "', '" + item.republish +
                                      "', '" + item.deleted + "', '" + item.dt_modified + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;

                        cmd.CommandTimeout = 1000;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        conn.Close();
                    }



                #endregion

                }
            } 
        }


        public void GetAllHighCourtFCT(List<LPV> court , string table)
        {
            foreach (var item in court)
            {
                #region

                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "' and dt_modified = '" + item.dt_modified + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data
                    myconnection.Close();
                    myconnection.Dispose();
                    string update = @"UPDATE " + table + "  SET  case_title= '" +
                                    item.case_title + "', case_title2 = '" +
                                    item.case_title2 + "', summary = '" + convertQuotes(item.summary) +
                                    "', fullreport = '" + convertQuotes(item.fullreport) + "', fullreport_html='" +
                                    convertQuotes(item.fullreport_html) + "', othercitation1= '" +
                                    item.othercitation1 + "', othercitation2= '" +
                                    item.othercitation2 + "', othercitation3='" +
                                    item.othercitation3 + "', appelant='" +
                                    item.appelant + "', defendant='" + item.defendant +
                                    "', appellant_counsel='" + item.appellant_counsel + "', judge1='" +
                                    item.judge1 + "', judge2='" + item.judge2 +
                                    "', judge3='" + item.judge3 + "', judge4='" +
                                    item.judge4 + "', judge5='" + item.judge5 +
                                    "', judge6='" + item.judge6 + "', judge7='" +
                                    item.judge7 + "', judge8='" + item.Judge8 +
                                    "', status = '" + item.status + "', html = '" + item.html +
                                    "', division = '" + item.division + "',stage='" + item.stage + "',done_by='" +
                                    item.done_by + "', appeal_status='" + item.appeal_status + "', type='" + item.type +
                                    "', parties='" + item.parties + "', case_origin='" + item.case_origin +
                                    "', republish='" + item.republish + "', judge_id='" + item.judge_id + "', state='" +
                                    item.state + "',dt_modified='" +
                                    item.dt_modified + "', deleted = '" + item.deleted + "' WHERE suitno='" +
                                    item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    {
                        myConnection.Open();
                        using (SqlTransaction tran = myConnection.BeginTransaction())
                        {
                            try
                            {
                                using (SqlCommand myCommand = myConnection.CreateCommand())
                                {
                                    myCommand.CommandText = update;
                                    myCommand.Transaction = tran as SqlTransaction;

                                    myCommand.CommandTimeout = 50;

                                    int i = myCommand.ExecuteNonQuery();

                                    if (i == 1)
                                    {
                                        Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                                    }
                                    else
                                    {
                                        Logger.writelog("Error Occurred while Updating existing data " + " " + " " +
                                                        DateTime.Now);
                                    }
                                    myConnection.Close();
                                }
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                Logger.writelog("CATCH eRROR :    Error Occurred while Updating existing data " + " " +
                                                " " +
                                                DateTime.Now);
                            }
                            myconnection.Close();


                        }
                    }

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    if (item.date == "0000-00-00")
                    {
                        item.date = DateTime.Now.ToString();
                    }
                    string sqlQuery = @"INSERT INTO " + table +
                                      " (date, suitno, case_title, case_title2, summary, fullreport_html,othercitation1, othercitation2, othercitation3, appelant, defendant, appellant_counsel, defendant_counsel, status,html,division,stage,done_by,appeal_status,type,parties,case_origin,republish, judge_id,state,deleted,dt_modified) VALUES " +
                                      "('" +item.date + "', '" + convertQuotes(item.suitno) + "', '" +
                                      convertQuotes(item.case_title) + "','" +
                                      convertQuotes(item.case_title2) + "', '" + convertQuotes(item.summary) +
                                      "', '" +
                                      convertQuotes(item.fullreport_html) + "', '" +
                                      convertQuotes(item.othercitation1) + "', '" +
                                      convertQuotes(item.othercitation2) + "', '" +
                                      convertQuotes(item.othercitation3) + "', '" + convertQuotes(item.appelant) +
                                      "', '" + convertQuotes(item.defendant) + "', '" +
                                      convertQuotes(item.appellant_counsel) + "', '" +
                                      convertQuotes(item.defendant_counsel) + "', '" +
                                      convertQuotes(item.status) + "', '" + convertQuotes(item.html) +
                                      "', '" + convertQuotes(item.division) + "','" + convertQuotes(item.stage) +
                                      "' , '" + item.done_by + "' , '" + item.appeal_status + "', '" + item.type +
                                      "', '" + item.parties + "', '" + item.case_origin + "', '" + item.republish +
                                      "', '" + item.judge_id + "', '" + item.state + "','" + item.deleted + "', '" +
                                      item.dt_modified + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;

                        cmd.CommandTimeout = 1000;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        conn.Close();
                    }



                #endregion

                }
            } 
        }

        public void Getsubsidiaries_legislations(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region

                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE pk = '" + item.pk +
                                  "' and dt_modified = '" + item.dt_modified + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data
                    myconnection.Close();
                    myconnection.Dispose();
                    string update = @"UPDATE " + table + "  SET legalhead= '" + item.legal_head + "', part_number= '" +
                                    item.part_number + "', part_Desc = '" +
                                    item.part_Desc + "', subpart_Desc = '" + convertQuotes(item.subpart_Desc) +
                                    "', section = '" + convertQuotes(item.section) + "', section_Number='" +
                                    convertQuotes(item.section_Number) + "', section_Desc= '" +
                                    item.section_Desc + "', court= '" +
                                    item.court + "', section_Content='" +
                                    item.section_Content + "', lfntitle='" +
                                    item.lfntitle + "', citation1='" + item.citation1 +
                                    "', citation2='" + item.citation2 + "', table='" +
                                    item.table + "', link='" + item.link +
                                    "', year='" + item.year + "', pk='" +
                                    item.pk + "', dt_modified='" +
                                    item.dt_modified + "', deleted = '" + item.deleted + "' WHERE pk='" +
                                    item.pk + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    {
                        myConnection.Open();
                        using (SqlTransaction tran = myConnection.BeginTransaction())
                        {
                            try
                            {
                                using (SqlCommand myCommand = myConnection.CreateCommand())
                                {
                                    myCommand.CommandText = update;
                                    myCommand.Transaction = tran as SqlTransaction;

                                    myCommand.CommandTimeout = 50;

                                    int i = myCommand.ExecuteNonQuery();

                                    if (i == 1)
                                    {
                                        Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                                    }
                                    else
                                    {
                                        Logger.writelog("Error Occurred while Updating existing data " + " " + " " +
                                                        DateTime.Now);
                                    }
                                    myConnection.Close();
                                }
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                Logger.writelog("CATCH eRROR :    Error Occurred while Updating existing data " + " " +
                                                " " +
                                                DateTime.Now);
                            }
                            myconnection.Close();


                        }
                    }

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " (legalhead, part_number, subpart_Desc, section, section_Number, section_Desc, court,section_Content, lfntitle, citation1, citation2, table, link, year,pk, dt_modified, deleted) VALUES " +
                                      "('" + item.legal_head + "', '" + convertQuotes(item.part_number) + "', '" +
                                      convertQuotes(item.subpart_Desc) + "','" +
                                      convertQuotes(item.section) + "', '" + convertQuotes(item.section_Number) +
                                      "', '" + convertQuotes(item.section_Desc) + "', '" +
                                      convertQuotes(item.court) + "', '" +
                                      convertQuotes(item.section_Content) + "', '" +
                                      convertQuotes(item.lfntitle) + "', '" +
                                      convertQuotes(item.citation1) + "', '" + convertQuotes(item.citation2) +
                                      "', '" + convertQuotes(item.table) + "', '" +
                                      convertQuotes(item.link) + "', '" +
                                      convertQuotes(item.year) + "', '" + convertQuotes(item.pk) + "', '" +
                                      item.dt_modified + "', '" + item.deleted + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;

                        cmd.CommandTimeout = 1000;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        conn.Close();
                    }



                #endregion

                }
            } 
        }

        public void Getlaws_sections(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region

                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE pk = '" + item.pk +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data
                    myconnection.Close();
                    myconnection.Dispose();
                    string update = @"UPDATE " + table + "  SET legalhead= '" + convertQuotes(item.legal_head) + "', chapter_number='" + item.chapter_number + "',chapter_Desc='" + item.chapter_Desc + "',  part_number= '" +
                                    item.part_number + "', part_Desc = '" +
                                    convertQuotes(item.part_Desc) + "', subpart_Desc = '" + convertQuotes(item.subpart_Desc) +
                                    "', section = '" + convertQuotes(item.section) + "', section_Number='" +
                                    convertQuotes(item.section_Number) + "', section_Desc= '" +
                                    convertQuotes(item.section_Desc) + "', court= '" +
                                    item.court + "', section_Content='" +
                                    convertQuotes(item.section_Content)  + "', lfntitle='" +
                                    convertQuotes(item.lfntitle) + "', citation1='" + convertQuotes(item.citation1) +
                                    "', citation2='" + (item.citation2) + "', [table]='" +
                                    convertQuotes(item.table) + "', link='" + item.link +
                                    "', year='" + item.year + "',  dt_modified='" +
                                    item.dt_modified + "', deleted = '" + item.deleted + "' WHERE pk='" +
                                    item.pk + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    {
                        myConnection.Open();
                        using (SqlTransaction tran = myConnection.BeginTransaction())
                        {
                            try
                            {
                                using (SqlCommand myCommand = myConnection.CreateCommand())
                                {
                                    myCommand.CommandText = update;
                                    myCommand.Transaction = tran as SqlTransaction;

                                    myCommand.CommandTimeout = 50;

                                    int i = myCommand.ExecuteNonQuery();

                                    if (i == 1)
                                    {
                                        Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                                    }
                                    else
                                    {
                                        Logger.writelog("Error Occurred while Updating existing data " + " " + " " +
                                                        DateTime.Now);
                                    }
                                    myConnection.Close();
                                }
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                Logger.writelog("CATCH ERROR :    Error Occurred while Updating existing data " + " " +
                                                " " +
                                                DateTime.Now);
                            }
                            myconnection.Close();


                        }
                    }



                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"SET IDENTITY_INSERT  " + table + " ON   INSERT INTO " + table +
                                      " (legalhead,chapter_number,chapter_Desc, part_number, subpart_Desc, section, section_Number, section_Desc, court,section_Content, lfntitle, citation1, citation2, [table], link, year,pk, dt_modified, deleted) VALUES " +
                                      "('" + item.legal_head + "', '" + item.chapter_number + "', '" + item.chapter_Desc +
                                      "','" + convertQuotes(item.part_number) + "', '" +
                                      convertQuotes(item.subpart_Desc) + "','" +
                                      convertQuotes(item.section) + "', '" + convertQuotes(item.section_Number) +
                                      "', '" + convertQuotes(item.section_Desc) + "', '" +
                                      convertQuotes(item.court) + "', '" +
                                      convertQuotes(item.section_Content) + "', '" +
                                      convertQuotes(item.lfntitle) + "', '" +
                                      convertQuotes(item.citation1) + "', '" + convertQuotes(item.citation2) +
                                      "', '" + convertQuotes(item.table) + "', '" +
                                      convertQuotes(item.link) + "', '" +
                                      convertQuotes(item.year) + "', '" + convertQuotes(item.pk) + "', '" +
                                      item.dt_modified + "', '" + item.deleted + "') SET IDENTITY_INSERT " + table + " OFF ";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;

                        cmd.CommandTimeout = 1000;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        conn.Close();
                    }



                #endregion

                }
            } 
        }

        public void GetAllCourtofAppealAnalysis(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region
                SqlConnection.ClearAllPools();

                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "' and dt_modified = '" + item.dt_modified + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    string update = @"UPDATE " + table + "  SET year='" + item.year + "', date= '" + item.date +
                                    "', [legal head]= '" +
                                    item.legal_head + "', subjectmatter = '" +
                                    item.subjectmatter + "', issues1 = '" + item.issues1 +
                                    "', principle = '" + item.principle + "', referencedcases1='" +
                                    item.referencedcases1 + "', referencedcases2= '" +
                                    item.referencedcases2 + "', referencedcases3= '" +
                                    item.referencedcases3 + "', referencedcases4='" +
                                    item.referencedcases4 + "', referencedcases5='" +
                                    item.referencedcases5 + "', referencedcases6='" + item.referencedcases6 +
                                    "', referencedcases7='" + item.referencedcases7 + "', referencedcases8='" +
                                    item.referencedcases8 + "', referencedcases9='" + item.referencedcases9 +
                                    "', referencedcases10='" + item.referencedcases10 + "', referencedcases11='" +
                                    item.referencedcases11 + "', referencedcases12='" + item.referencedcases12 +
                                    "', referencedcases13='" + item.referencedcases13 + "', referencedcases14='" +
                                    item.referencedcases14 + "', referencedcases15='" + item.referencedcases15 +
                                    "', referencedcases16 = '" + item.referencedcases16 + "', referencedcases17 = '" +
                                    item.referencedcases17 +
                                    "', referencedcases18 = '" + item.referencedcases18 + "',referencedcases19='" +
                                    item.referencedcases19 + "', referencedcases20 = '" + item.referencedcases20 +
                                    "' , referencedcases21 = '" + item.referencedcases21 + "', referencedcases22  = '" +
                                    item.referencedcases22 + "', referencedcases23 = '" + item.referencedcases23 +
                                    "', referencedcases24='" + item.referencedcases24 + "', referencedcases25='" +
                                    item.referencedcases25 + "', referencedcases26 ='" + item.referencedcases26 +
                                    "', referencedcases27='" + item.referencedcases27 + "', referencedcases28='" +
                                    item.referencedcases28 + "', referencedcases29='" + item.referencedcases29 +
                                    "', referencedcases30='" + item.referencedcases30 + "', referencedcases31='" +
                                    item.referencedcases31 + "', referencedcases32 = '" + item.referencedcases32 +
                                    "', referencedcases33='" + item.referencedcases33 + "', referencedcases34='" +
                                    item.referencedcases34 + "', referencedcases35='" + item.referencedcases35 +
                                    "', referencedcases36='" + item.referencedcases36 + "', referencedcases37='" +
                                    item.referencedcases37 + "', referencedcases38='" + item.referencedcases38 +
                                    "', referencedcases39='" + item.referencedcases39 + "', referencedcases40='" +
                                    item.referencedcases40 + "', referencedcases41='" + item.referencedcases41 +
                                    "', suitno1='" + item.suitno1 + "', suitno2='" + item.suitno2 + "', suitno3='" +
                                    item.suitno3 + "', suitno4='" + item.suitno4 + "', suitno5='" + item.suitno5 +
                                    "', suitno6='" + item.suitno6 + "', suitno7='" + item.suitno7 + "', suitno8='" +
                                    item.suitno8 + "', suitno9='" + item.suitno9 + "', suitno10='" + item.suitno11 +
                                    "', suitno12='" + item.suitno12 + "', suitno13='" + item.suitno13 + "', suitno14='" +
                                    item.suitno14 + "', suitno15='" + item.suitno15 + "', suitno16='" + item.suitno16 +
                                    "', suitno17='" + item.suitno17 + "', suitno18='" + item.suitno18 + "', suitno19='" +
                                    item.suitno19 + "', suitno20='" + item.suitno20 + "', suitno21='" + item.suitno21 +
                                    "', suitno22='" + item.suitno22 + "', suitno23='" + item.suitno23 +
                                    "', suitno24='" + item.suitno24 + "', suitno25='" + item.suitno25 + "', suitno26='" +
                                    item.suitno26 + "', suitno27='" + item.suitno27 + "', suitno28='" + item.suitno28 +
                                    "', suitno29='" + item.suitno29 + "', suitno30='" + item.suitno30 + "', suitno31='" +
                                    item.suitno31 + "', suitno32='" + item.suitno32 + "', suitno33='" + item.suitno33 +
                                    "', suitno34='" + item.suitno34 + "', suitno35='" + item.suitno35 + "', suitno36='" +
                                    item.suitno36 + "', suitno37='" + item.suitno37 + "', suitno38='" + item.suitno38 +
                                    "', suitno39='" + item.suitno39 + "', suitno40='" + item.suitno40 + "', suitno41='" +
                                    item.suitno41 + "' , statutes1='" + item.statutes1 + "', statutes2='" +
                                    item.statutes2 + "', statutes3='" + item.statutes3 + "', statutes4='" +
                                    item.statutes4 + "', statutes5='" + item.statutes5 + "', statutes6='" +
                                    item.statutes6 + "', statutes7='" + item.statutes7 + "', statutes8='" +
                                    item.statutes8 + "', statutes9='" + item.statutes9 + "', statutes10='" +
                                    item.statutes10 + "', statutes11='" + item.statutes11 + "', statutes12='" +
                                    item.statutes12 + "', statutes13='" + item.statutes13 + "', statutes14='" +
                                    item.statutes14 + "', statutes15='" + item.statutes15 + "', overruled='" +
                                    item.overruled + "',overruled_case='" + item.overruled_case +
                                    "', overruled_suitno='" + item.overruled_suitno + "', additional_note='" +
                                    item.additional_note + "', pk='" + item.pk + "', anchored='" + item.anchored +
                                    "', dt_modified='" + item.dt_modified + "', deleted='" + item.deleted +
                                    "' WHERE suitno='" +
                                    item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 500;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }

                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();

                    string sqlQuery = @"INSERT INTO " + table +
                                      " (year, date, suitno, [legal head], subjectmatter, issues1, principle,referencedcases1, referencedcases2, referencedcases3, referencedcases4, referencedcases5, referencedcases6, referencedcases7,referencedcases8, referencedcases9, referencedcases10, referencedcases11, referencedcases12, referencedcases13, referencedcases14, referencedcases15,referencedcases16,referencedcases17,referencedcases18,referencedcases19,referencedcases20,referencedcases21,referencedcases22,referencedcases23,referencedcases24,referencedcases25,referencedcases26,referencedcases27,referencedcases28,referencedcases29,referencedcases30,referencedcases31,referencedcases32,referencedcases33,referencedcases34,referencedcases35,referencedcases36,referencedcases37,referencedcases38,referencedcases39,referencedcases40,referencedcases41,suitno1,suitno2,suitno3,suitno4,suitno5,suitno6,suitno7,suitno8,suitno9,suitno10,suitno11,suitno12,suitno13,suitno14,suitno15,suitno16,suitno17,suitno18,suitno19,suitno20,suitno21,suitno22,suitno23,suitno24,suitno25,suitno26,suitno27,suitno28,suitno29,suitno30,suitno31,suitno32,suitno33,suitno34,suitno35,suitno36,suitno37,suitno38,suitno39,suitno40,suitno41,statutes1,statutes2,statutes3,statutes4,statutes5,statutes6,statutes7,statutes8,statutes9,statutes10,statutes11,statutes12,statutes13,statutes14,statutes15,overruled,overruled_case,overruled_suitno,additional_note,pk,anchored,dt_modified,deleted) VALUES " +
                                      "('" + item.year + "','" + item.date + "', '" + item.suitno + "', '" +
                                      item.legal_head + "','" +
                                      item.subjectmatter + "', '" + item.issues1 +
                                      "', '" + item.principle + "', '" +
                                      item.referencedcases1 + "', '" +
                                      item.referencedcases2 + "', '" +
                                      item.referencedcases3 + "', '" +
                                      item.referencedcases4 + "', '" + item.referencedcases5 +
                                      "', '" + item.referencedcases6 + "', '" +
                                      item.referencedcases7 + "', '" +
                                      item.referencedcases8 + "', '" + item.referencedcases9 + "', '" +
                                      item.referencedcases10 + "', '" +
                                      item.referencedcases11 + "', '" + item.referencedcases12 + "', '" +
                                      item.referencedcases13 + "', '" +
                                      item.referencedcases14 + "', '" + item.referencedcases15 + "', '" +
                                      item.referencedcases16 + "', '" + item.referencedcases17 +
                                      "', '" + item.referencedcases18 + "', '" + item.referencedcases19 + "', '" +
                                      item.referencedcases20 + "', '" +
                                      item.referencedcases21 + "','" +
                                      item.referencedcases22 + "','" +
                                      item.referencedcases23 + "','" +
                                      item.referencedcases24 + "','" +
                                      item.referencedcases25 + "','" +
                                      item.referencedcases26 + "','" +
                                      item.referencedcases27 + "','" +
                                      item.referencedcases28 + "','" +
                                      item.referencedcases29 + "','" +
                                      item.referencedcases30 + "','" +
                                      item.referencedcases31 + "','" +
                                      item.referencedcases32 + "','" +
                                      item.referencedcases33 + "','" +
                                      item.referencedcases34 + "','" +
                                      item.referencedcases35 + "','" +
                                      item.referencedcases36 + "','" +
                                      item.referencedcases37 + "','" +
                                      item.referencedcases38 + "','" +
                                      item.referencedcases39 + "','" +
                                      item.referencedcases40 + "','" +
                                      item.referencedcases41 + "','" +
                                      item.suitno1 + "','" +
                                      item.suitno2 + "','" +
                                      item.suitno3 + "','" +
                                      item.suitno4 + "','" +
                                      item.suitno5 + "','" +
                                      item.suitno6 + "','" +
                                      item.suitno7 + "','" +
                                      item.suitno8 + "','" +
                                      item.suitno9 + "','" +
                                      item.suitno10 + "','" +
                                      item.suitno11 + "','" +
                                      item.suitno12 + "','" +
                                      item.suitno13 + "','" +
                                      item.suitno14 + "','" +
                                      item.suitno15 + "','" +
                                      item.suitno16 + "','" +
                                      item.suitno17 + "','" +
                                      item.suitno18 + "','" +
                                      item.suitno19 + "','" +
                                      item.suitno20 + "','" +
                                      item.suitno21 + "','" +
                                      item.suitno22 + "','" +
                                      item.suitno23 + "','" +
                                      item.suitno24 + "','" +
                                      item.suitno25 + "','" +
                                      item.suitno26 + "','" +
                                      item.suitno27 + "','" +
                                      item.suitno28 + "','" +
                                      item.suitno29 + "','" +
                                      item.suitno30 + "','" +
                                      item.suitno31 + "','" +
                                      item.suitno32 + "','" +
                                      item.suitno33 + "','" +
                                      item.suitno34 + "','" +
                                      item.suitno35 + "','" +
                                      item.suitno36 + "','" +
                                      item.suitno37 + "','" +
                                      item.suitno38 + "','" +
                                      item.suitno39 + "','" +
                                      item.suitno40 + "','" +
                                      item.suitno41 + "','" +
                                      item.statutes1 + "','" +
                                      item.statutes2 + "','" +
                                      item.statutes3 + "','" +
                                      item.statutes4 + "','" +
                                      item.statutes5 + "','" +
                                      item.statutes6 + "','" +
                                      item.statutes7 + "','" +
                                      item.statutes8 + "','" +
                                      item.statutes9 + "','" +
                                      item.statutes10 + "','" +
                                      item.statutes11 + "','" +
                                      item.statutes12 + "','" +
                                      item.statutes13 + "', '" +
                                      item.statutes14 + "','" +
                                      item.statutes15 + "','" +
                                      item.overruled + "','" +
                                      item.overruled_case + "','" +
                                      item.overruled_suitno + "','" +
                                      item.additional_note + "','" +
                                      item.pk + "','" +
                                      item.anchored + "','" +
                                      item.dt_modified + "','" +
                                      item.deleted + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        public void GetAllFHCAnalysis(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region
                SqlConnection.ClearAllPools();

                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "' and dt_modified = '" + item.dt_modified + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    string update = @"UPDATE " + table + "  SET year='" + item.year + "', date= '" + item.date +
                                    "', [legal head]= '" +
                                    item.legal_head + "', subjectmatter = '" +
                                    item.subjectmatter + "', issues1 = '" + item.issues1 +
                                    "', principle = '" + item.principle + "', referencedcases1='" +
                                    item.referencedcases1 + "', referencedcases2= '" +
                                    item.referencedcases2 + "', referencedcases3= '" +
                                    item.referencedcases3 + "', referencedcases4='" +
                                    item.referencedcases4 + "', referencedcases5='" +
                                    item.referencedcases5 + "', referencedcases6='" + item.referencedcases6 +
                                    "', referencedcases7='" + item.referencedcases7 + "', referencedcases8='" +
                                    item.referencedcases8 + "', referencedcases9='" + item.referencedcases9 +
                                    "', referencedcases10='" + item.referencedcases10 + "', referencedcases11='" +
                                    item.referencedcases11 + "', referencedcases12='" + item.referencedcases12 +
                                    "', referencedcases13='" + item.referencedcases13 + "', referencedcases14='" +
                                    item.referencedcases14 + "', referencedcases15='" + item.referencedcases15 +
                                    "', referencedcases16 = '" + item.referencedcases16 + "', referencedcases17 = '" +
                                    item.referencedcases17 +
                                    "', referencedcases18 = '" + item.referencedcases18 + "',referencedcases19='" +
                                    item.referencedcases19 + "', referencedcases20 = '" + item.referencedcases20 +
                                    "' , referencedcases21 = '" + item.referencedcases21 + "', referencedcases22  = '" +
                                    item.referencedcases22 + "', referencedcases23 = '" + item.referencedcases23 +
                                    "', referencedcases24='" + item.referencedcases24 + "', referencedcases25='" +
                                    item.referencedcases25 + "', referencedcases26 ='" + item.referencedcases26 +
                                    "', referencedcases27='" + item.referencedcases27 + "', referencedcases28='" +
                                    item.referencedcases28 + "', referencedcases29='" + item.referencedcases29 +
                                    "', referencedcases30='" + item.referencedcases30 + "', referencedcases31='" +
                                    item.referencedcases31 + "', referencedcases32 = '" + item.referencedcases32 +
                                    "', referencedcases33='" + item.referencedcases33 + "', referencedcases34='" +
                                    item.referencedcases34 + "', referencedcases35='" + item.referencedcases35 +
                                    "', referencedcases36='" + item.referencedcases36 + "', referencedcases37='" +
                                    item.referencedcases37 + "', referencedcases38='" + item.referencedcases38 +
                                    "', referencedcases39='" + item.referencedcases39 + "', referencedcases40='" +
                                    item.referencedcases40 + "', referencedcases41='" + item.referencedcases41 +
                                    "', suitno1='" + item.suitno1 + "', suitno2='" + item.suitno2 + "', suitno3='" +
                                    item.suitno3 + "', suitno4='" + item.suitno4 + "', suitno5='" + item.suitno5 +
                                    "', suitno6='" + item.suitno6 + "', suitno7='" + item.suitno7 + "', suitno8='" +
                                    item.suitno8 + "', suitno9='" + item.suitno9 + "', suitno10='" + item.suitno11 +
                                    "', suitno12='" + item.suitno12 + "', suitno13='" + item.suitno13 + "', suitno14='" +
                                    item.suitno14 + "', suitno15='" + item.suitno15 + "', suitno16='" + item.suitno16 +
                                    "', suitno17='" + item.suitno17 + "', suitno18='" + item.suitno18 + "', suitno19='" +
                                    item.suitno19 + "', suitno20='" + item.suitno20 + "', suitno21='" + item.suitno21 +
                                    "', suitno22='" + item.suitno22 + "', suitno23='" + item.suitno23 +
                                    "', suitno24='" + item.suitno24 + "', suitno25='" + item.suitno25 + "', suitno26='" +
                                    item.suitno26 + "', suitno27='" + item.suitno27 + "', suitno28='" + item.suitno28 +
                                    "', suitno29='" + item.suitno29 + "', suitno30='" + item.suitno30 + "', suitno31='" +
                                    item.suitno31 + "', suitno32='" + item.suitno32 + "', suitno33='" + item.suitno33 +
                                    "', suitno34='" + item.suitno34 + "', suitno35='" + item.suitno35 + "', suitno36='" +
                                    item.suitno36 + "', suitno37='" + item.suitno37 + "', suitno38='" + item.suitno38 +
                                    "', suitno39='" + item.suitno39 + "', suitno40='" + item.suitno40 + "', suitno41='" +
                                    item.suitno41 + "' , statutes1='" + item.statutes1 + "', statutes2='" +
                                    item.statutes2 + "', statutes3='" + item.statutes3 + "', statutes4='" +
                                    item.statutes4 + "', statutes5='" + item.statutes5 + "', statutes6='" +
                                    item.statutes6 + "', statutes7='" + item.statutes7 + "', statutes8='" +
                                    item.statutes8 + "', statutes9='" + item.statutes9 + "', statutes10='" +
                                    item.statutes10 + "', statutes11='" + item.statutes11 + "', statutes12='" +
                                    item.statutes12 + "', statutes13='" + item.statutes13 + "', statutes14='" +
                                    item.statutes14 + "', statutes15='" + item.statutes15 + "', overruled='" +
                                    item.overruled + "',overruled_case='" + item.overruled_case +
                                    "', overruled_suitno='" + item.overruled_suitno + "', additional_note='" +
                                    item.additional_note + "', pk='" + item.pk + "', anchored='" + item.anchored +
                                    "', dt_modified='" + item.dt_modified + "', deleted='" + item.deleted +
                                    "' WHERE suitno='" +
                                    item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 500;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }

                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();

                    string sqlQuery = @"INSERT INTO " + table +
                                      " (year, date, suitno, [legal head], subjectmatter, issues1, principle,referencedcases1, referencedcases2, referencedcases3, referencedcases4, referencedcases5, referencedcases6, referencedcases7,referencedcases8, referencedcases9, referencedcases10, referencedcases11, referencedcases12, referencedcases13, referencedcases14, referencedcases15,referencedcases16,referencedcases17,referencedcases18,referencedcases19,referencedcases20,referencedcases21,referencedcases22,referencedcases23,referencedcases24,referencedcases25,referencedcases26,referencedcases27,referencedcases28,referencedcases29,referencedcases30,referencedcases31,referencedcases32,referencedcases33,referencedcases34,referencedcases35,referencedcases36,referencedcases37,referencedcases38,referencedcases39,referencedcases40,referencedcases41,suitno1,suitno2,suitno3,suitno4,suitno5,suitno6,suitno7,suitno8,suitno9,suitno10,suitno11,suitno12,suitno13,suitno14,suitno15,suitno16,suitno17,suitno18,suitno19,suitno20,suitno21,suitno22,suitno23,suitno24,suitno25,suitno26,suitno27,suitno28,suitno29,suitno30,suitno31,suitno32,suitno33,suitno34,suitno35,suitno36,suitno37,suitno38,suitno39,suitno40,suitno41,statutes1,statutes2,statutes3,statutes4,statutes5,statutes6,statutes7,statutes8,statutes9,statutes10,statutes11,statutes12,statutes13,statutes14,statutes15,overruled,overruled_case,overruled_suitno,additional_note,pk,anchored, judge_id,from_page,from_para,to_page,to_para,dt_modified,deleted) VALUES " +
                                      "('" + item.year + "','" + item.date + "', '" + item.suitno + "', '" +
                                      item.legal_head + "','" +
                                      item.subjectmatter + "', '" + item.issues1 +
                                      "', '" + item.principle + "', '" +
                                      item.referencedcases1 + "', '" +
                                      item.referencedcases2 + "', '" +
                                      item.referencedcases3 + "', '" +
                                      item.referencedcases4 + "', '" + item.referencedcases5 +
                                      "', '" + item.referencedcases6 + "', '" +
                                      item.referencedcases7 + "', '" +
                                      item.referencedcases8 + "', '" + item.referencedcases9 + "', '" +
                                      item.referencedcases10 + "', '" +
                                      item.referencedcases11 + "', '" + item.referencedcases12 + "', '" +
                                      item.referencedcases13 + "', '" +
                                      item.referencedcases14 + "', '" + item.referencedcases15 + "', '" +
                                      item.referencedcases16 + "', '" + item.referencedcases17 +
                                      "', '" + item.referencedcases18 + "', '" + item.referencedcases19 + "', '" +
                                      item.referencedcases20 + "', '" +
                                      item.referencedcases21 + "','" +
                                      item.referencedcases22 + "','" +
                                      item.referencedcases23 + "','" +
                                      item.referencedcases24 + "','" +
                                      item.referencedcases25 + "','" +
                                      item.referencedcases26 + "','" +
                                      item.referencedcases27 + "','" +
                                      item.referencedcases28 + "','" +
                                      item.referencedcases29 + "','" +
                                      item.referencedcases30 + "','" +
                                      item.referencedcases31 + "','" +
                                      item.referencedcases32 + "','" +
                                      item.referencedcases33 + "','" +
                                      item.referencedcases34 + "','" +
                                      item.referencedcases35 + "','" +
                                      item.referencedcases36 + "','" +
                                      item.referencedcases37 + "','" +
                                      item.referencedcases38 + "','" +
                                      item.referencedcases39 + "','" +
                                      item.referencedcases40 + "','" +
                                      item.referencedcases41 + "','" +
                                      item.suitno1 + "','" +
                                      item.suitno2 + "','" +
                                      item.suitno3 + "','" +
                                      item.suitno4 + "','" +
                                      item.suitno5 + "','" +
                                      item.suitno6 + "','" +
                                      item.suitno7 + "','" +
                                      item.suitno8 + "','" +
                                      item.suitno9 + "','" +
                                      item.suitno10 + "','" +
                                      item.suitno11 + "','" +
                                      item.suitno12 + "','" +
                                      item.suitno13 + "','" +
                                      item.suitno14 + "','" +
                                      item.suitno15 + "','" +
                                      item.suitno16 + "','" +
                                      item.suitno17 + "','" +
                                      item.suitno18 + "','" +
                                      item.suitno19 + "','" +
                                      item.suitno20 + "','" +
                                      item.suitno21 + "','" +
                                      item.suitno22 + "','" +
                                      item.suitno23 + "','" +
                                      item.suitno24 + "','" +
                                      item.suitno25 + "','" +
                                      item.suitno26 + "','" +
                                      item.suitno27 + "','" +
                                      item.suitno28 + "','" +
                                      item.suitno29 + "','" +
                                      item.suitno30 + "','" +
                                      item.suitno31 + "','" +
                                      item.suitno32 + "','" +
                                      item.suitno33 + "','" +
                                      item.suitno34 + "','" +
                                      item.suitno35 + "','" +
                                      item.suitno36 + "','" +
                                      item.suitno37 + "','" +
                                      item.suitno38 + "','" +
                                      item.suitno39 + "','" +
                                      item.suitno40 + "','" +
                                      item.suitno41 + "','" +
                                      item.statutes1 + "','" +
                                      item.statutes2 + "','" +
                                      item.statutes3 + "','" +
                                      item.statutes4 + "','" +
                                      item.statutes5 + "','" +
                                      item.statutes6 + "','" +
                                      item.statutes7 + "','" +
                                      item.statutes8 + "','" +
                                      item.statutes9 + "','" +
                                      item.statutes10 + "','" +
                                      item.statutes11 + "','" +
                                      item.statutes12 + "','" +
                                      item.statutes13 + "', '" +
                                      item.statutes14 + "','" +
                                      item.statutes15 + "','" +
                                      item.overruled + "','" +
                                      item.overruled_case + "','" +
                                      item.overruled_suitno + "','" +
                                      item.additional_note + "','" +
                                      item.pk + "','" +
                                      item.anchored + "',  '" + item.judge_id + "', '" + item.from_page + "', '" +
                                      item.from_para + "', '" + item.to_page + "', '" + item.to_para + "', '" +
                                      item.dt_modified + "','" +
                                      item.deleted + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        public void GetAllNICAnalysis(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region
                SqlConnection.ClearAllPools();

                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "' and dt_modified = '" + item.dt_modified + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    string update = @"UPDATE " + table + "  SET year='" + item.year + "', date= '" + item.date +
                                    "', [legal head]= '" +
                                    item.legal_head + "', subjectmatter = '" +
                                    item.subjectmatter + "', issues1 = '" + item.issues1 +
                                    "', principle = '" + item.principle + "', referencedcases1='" +
                                    item.referencedcases1 + "', referencedcases2= '" +
                                    item.referencedcases2 + "', referencedcases3= '" +
                                    item.referencedcases3 + "', referencedcases4='" +
                                    item.referencedcases4 + "', referencedcases5='" +
                                    item.referencedcases5 + "', referencedcases6='" + item.referencedcases6 +
                                    "', referencedcases7='" + item.referencedcases7 + "', referencedcases8='" +
                                    item.referencedcases8 + "', referencedcases9='" + item.referencedcases9 +
                                    "', referencedcases10='" + item.referencedcases10 + "', referencedcases11='" +
                                    item.referencedcases11 + "', referencedcases12='" + item.referencedcases12 +
                                    "', referencedcases13='" + item.referencedcases13 + "', referencedcases14='" +
                                    item.referencedcases14 + "', referencedcases15='" + item.referencedcases15 +
                                    "', referencedcases16 = '" + item.referencedcases16 + "', referencedcases17 = '" +
                                    item.referencedcases17 +
                                    "', referencedcases18 = '" + item.referencedcases18 + "',referencedcases19='" +
                                    item.referencedcases19 + "', referencedcases20 = '" + item.referencedcases20 +
                                    "' , referencedcases21 = '" + item.referencedcases21 + "', referencedcases22  = '" +
                                    item.referencedcases22 + "', referencedcases23 = '" + item.referencedcases23 +
                                    "', referencedcases24='" + item.referencedcases24 + "', referencedcases25='" +
                                    item.referencedcases25 + "', referencedcases26 ='" + item.referencedcases26 +
                                    "', referencedcases27='" + item.referencedcases27 + "', referencedcases28='" +
                                    item.referencedcases28 + "', referencedcases29='" + item.referencedcases29 +
                                    "', referencedcases30='" + item.referencedcases30 + "', referencedcases31='" +
                                    item.referencedcases31 + "', referencedcases32 = '" + item.referencedcases32 +
                                    "', referencedcases33='" + item.referencedcases33 + "', referencedcases34='" +
                                    item.referencedcases34 + "', referencedcases35='" + item.referencedcases35 +
                                    "', referencedcases36='" + item.referencedcases36 + "', referencedcases37='" +
                                    item.referencedcases37 + "', referencedcases38='" + item.referencedcases38 +
                                    "', referencedcases39='" + item.referencedcases39 + "', referencedcases40='" +
                                    item.referencedcases40 + "', referencedcases41='" + item.referencedcases41 +
                                    "', suitno1='" + item.suitno1 + "', suitno2='" + item.suitno2 + "', suitno3='" +
                                    item.suitno3 + "', suitno4='" + item.suitno4 + "', suitno5='" + item.suitno5 +
                                    "', suitno6='" + item.suitno6 + "', suitno7='" + item.suitno7 + "', suitno8='" +
                                    item.suitno8 + "', suitno9='" + item.suitno9 + "', suitno10='" + item.suitno11 +
                                    "', suitno12='" + item.suitno12 + "', suitno13='" + item.suitno13 + "', suitno14='" +
                                    item.suitno14 + "', suitno15='" + item.suitno15 + "', suitno16='" + item.suitno16 +
                                    "', suitno17='" + item.suitno17 + "', suitno18='" + item.suitno18 + "', suitno19='" +
                                    item.suitno19 + "', suitno20='" + item.suitno20 + "', suitno21='" + item.suitno21 +
                                    "', suitno22='" + item.suitno22 + "', suitno23='" + item.suitno23 +
                                    "', suitno24='" + item.suitno24 + "', suitno25='" + item.suitno25 + "', suitno26='" +
                                    item.suitno26 + "', suitno27='" + item.suitno27 + "', suitno28='" + item.suitno28 +
                                    "', suitno29='" + item.suitno29 + "', suitno30='" + item.suitno30 + "', suitno31='" +
                                    item.suitno31 + "', suitno32='" + item.suitno32 + "', suitno33='" + item.suitno33 +
                                    "', suitno34='" + item.suitno34 + "', suitno35='" + item.suitno35 + "', suitno36='" +
                                    item.suitno36 + "', suitno37='" + item.suitno37 + "', suitno38='" + item.suitno38 +
                                    "', suitno39='" + item.suitno39 + "', suitno40='" + item.suitno40 + "', suitno41='" +
                                    item.suitno41 + "' , statutes1='" + item.statutes1 + "', statutes2='" +
                                    item.statutes2 + "', statutes3='" + item.statutes3 + "', statutes4='" +
                                    item.statutes4 + "', statutes5='" + item.statutes5 + "', statutes6='" +
                                    item.statutes6 + "', statutes7='" + item.statutes7 + "', statutes8='" +
                                    item.statutes8 + "', statutes9='" + item.statutes9 + "', statutes10='" +
                                    item.statutes10 + "', statutes11='" + item.statutes11 + "', statutes12='" +
                                    item.statutes12 + "', statutes13='" + item.statutes13 + "', statutes14='" +
                                    item.statutes14 + "', statutes15='" + item.statutes15 + "', overruled='" +
                                    item.overruled + "',overruled_case='" + item.overruled_case +
                                    "', overruled_suitno='" + item.overruled_suitno + "', additional_note='" +
                                    item.additional_note + "', pk='" + item.pk + "', anchored='" + item.anchored +
                                    "', dt_modified='" + item.dt_modified + "', deleted='" + item.deleted +
                                    "' WHERE suitno='" +
                                    item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 500;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }

                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();

                    string sqlQuery = @"INSERT INTO " + table +
                                      " (year, date, suitno, [legal head], subjectmatter, issues1, principle,referencedcases1, referencedcases2, referencedcases3, referencedcases4, referencedcases5, referencedcases6, referencedcases7,referencedcases8, referencedcases9, referencedcases10, referencedcases11, referencedcases12, referencedcases13, referencedcases14, referencedcases15,referencedcases16,referencedcases17,referencedcases18,referencedcases19,referencedcases20,referencedcases21,referencedcases22,referencedcases23,referencedcases24,referencedcases25,referencedcases26,referencedcases27,referencedcases28,referencedcases29,referencedcases30,referencedcases31,referencedcases32,referencedcases33,referencedcases34,referencedcases35,referencedcases36,referencedcases37,referencedcases38,referencedcases39,referencedcases40,referencedcases41,suitno1,suitno2,suitno3,suitno4,suitno5,suitno6,suitno7,suitno8,suitno9,suitno10,suitno11,suitno12,suitno13,suitno14,suitno15,suitno16,suitno17,suitno18,suitno19,suitno20,suitno21,suitno22,suitno23,suitno24,suitno25,suitno26,suitno27,suitno28,suitno29,suitno30,suitno31,suitno32,suitno33,suitno34,suitno35,suitno36,suitno37,suitno38,suitno39,suitno40,suitno41,statutes1,statutes2,statutes3,statutes4,statutes5,statutes6,statutes7,statutes8,statutes9,statutes10,statutes11,statutes12,statutes13,statutes14,statutes15,overruled,overruled_case,overruled_suitno,additional_note,pk,anchored, judge_id,from_page,from_para,to_page,to_para,dt_modified,deleted) VALUES " +
                                      "('" + item.year + "','" + item.date + "', '" + item.suitno + "', '" +
                                      item.legal_head + "','" +
                                      item.subjectmatter + "', '" + item.issues1 +
                                      "', '" + item.principle + "', '" +
                                      item.referencedcases1 + "', '" +
                                      item.referencedcases2 + "', '" +
                                      item.referencedcases3 + "', '" +
                                      item.referencedcases4 + "', '" + item.referencedcases5 +
                                      "', '" + item.referencedcases6 + "', '" +
                                      item.referencedcases7 + "', '" +
                                      item.referencedcases8 + "', '" + item.referencedcases9 + "', '" +
                                      item.referencedcases10 + "', '" +
                                      item.referencedcases11 + "', '" + item.referencedcases12 + "', '" +
                                      item.referencedcases13 + "', '" +
                                      item.referencedcases14 + "', '" + item.referencedcases15 + "', '" +
                                      item.referencedcases16 + "', '" + item.referencedcases17 +
                                      "', '" + item.referencedcases18 + "', '" + item.referencedcases19 + "', '" +
                                      item.referencedcases20 + "', '" +
                                      item.referencedcases21 + "','" +
                                      item.referencedcases22 + "','" +
                                      item.referencedcases23 + "','" +
                                      item.referencedcases24 + "','" +
                                      item.referencedcases25 + "','" +
                                      item.referencedcases26 + "','" +
                                      item.referencedcases27 + "','" +
                                      item.referencedcases28 + "','" +
                                      item.referencedcases29 + "','" +
                                      item.referencedcases30 + "','" +
                                      item.referencedcases31 + "','" +
                                      item.referencedcases32 + "','" +
                                      item.referencedcases33 + "','" +
                                      item.referencedcases34 + "','" +
                                      item.referencedcases35 + "','" +
                                      item.referencedcases36 + "','" +
                                      item.referencedcases37 + "','" +
                                      item.referencedcases38 + "','" +
                                      item.referencedcases39 + "','" +
                                      item.referencedcases40 + "','" +
                                      item.referencedcases41 + "','" +
                                      item.suitno1 + "','" +
                                      item.suitno2 + "','" +
                                      item.suitno3 + "','" +
                                      item.suitno4 + "','" +
                                      item.suitno5 + "','" +
                                      item.suitno6 + "','" +
                                      item.suitno7 + "','" +
                                      item.suitno8 + "','" +
                                      item.suitno9 + "','" +
                                      item.suitno10 + "','" +
                                      item.suitno11 + "','" +
                                      item.suitno12 + "','" +
                                      item.suitno13 + "','" +
                                      item.suitno14 + "','" +
                                      item.suitno15 + "','" +
                                      item.suitno16 + "','" +
                                      item.suitno17 + "','" +
                                      item.suitno18 + "','" +
                                      item.suitno19 + "','" +
                                      item.suitno20 + "','" +
                                      item.suitno21 + "','" +
                                      item.suitno22 + "','" +
                                      item.suitno23 + "','" +
                                      item.suitno24 + "','" +
                                      item.suitno25 + "','" +
                                      item.suitno26 + "','" +
                                      item.suitno27 + "','" +
                                      item.suitno28 + "','" +
                                      item.suitno29 + "','" +
                                      item.suitno30 + "','" +
                                      item.suitno31 + "','" +
                                      item.suitno32 + "','" +
                                      item.suitno33 + "','" +
                                      item.suitno34 + "','" +
                                      item.suitno35 + "','" +
                                      item.suitno36 + "','" +
                                      item.suitno37 + "','" +
                                      item.suitno38 + "','" +
                                      item.suitno39 + "','" +
                                      item.suitno40 + "','" +
                                      item.suitno41 + "','" +
                                      item.statutes1 + "','" +
                                      item.statutes2 + "','" +
                                      item.statutes3 + "','" +
                                      item.statutes4 + "','" +
                                      item.statutes5 + "','" +
                                      item.statutes6 + "','" +
                                      item.statutes7 + "','" +
                                      item.statutes8 + "','" +
                                      item.statutes9 + "','" +
                                      item.statutes10 + "','" +
                                      item.statutes11 + "','" +
                                      item.statutes12 + "','" +
                                      item.statutes13 + "', '" +
                                      item.statutes14 + "','" +
                                      item.statutes15 + "','" +
                                      item.overruled + "','" +
                                      item.overruled_case + "','" +
                                      item.overruled_suitno + "','" +
                                      item.additional_note + "','" +
                                      item.pk + "','" +
                                      item.anchored + "',  '" + item.judge_id + "', '" + item.from_page + "', '" +
                                      item.from_para + "', '" + item.to_page + "', '" + item.to_para + "', '" +
                                      item.dt_modified + "','" +
                                      item.deleted + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        public void GetAllStatesAnalysis(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region
                SqlConnection.ClearAllPools();

                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "' and dt_modified = '" + item.dt_modified + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    string update = @"UPDATE " + table + "  SET year='" + item.year + "', date= '" + item.date +
                                    "', [legal head]= '" +
                                    item.legal_head + "', subjectmatter = '" +
                                    item.subjectmatter + "', issues1 = '" + item.issues1 +
                                    "', principle = '" + item.principle + "', referencedcases1='" +
                                    item.referencedcases1 + "', referencedcases2= '" +
                                    item.referencedcases2 + "', referencedcases3= '" +
                                    item.referencedcases3 + "', referencedcases4='" +
                                    item.referencedcases4 + "', referencedcases5='" +
                                    item.referencedcases5 + "', referencedcases6='" + item.referencedcases6 +
                                    "', referencedcases7='" + item.referencedcases7 + "', referencedcases8='" +
                                    item.referencedcases8 + "', referencedcases9='" + item.referencedcases9 +
                                    "', referencedcases10='" + item.referencedcases10 + "', referencedcases11='" +
                                    item.referencedcases11 + "', referencedcases12='" + item.referencedcases12 +
                                    "', referencedcases13='" + item.referencedcases13 + "', referencedcases14='" +
                                    item.referencedcases14 + "', referencedcases15='" + item.referencedcases15 +
                                    "', referencedcases16 = '" + item.referencedcases16 + "', referencedcases17 = '" +
                                    item.referencedcases17 +
                                    "', referencedcases18 = '" + item.referencedcases18 + "',referencedcases19='" +
                                    item.referencedcases19 + "', referencedcases20 = '" + item.referencedcases20 +
                                    "' , referencedcases21 = '" + item.referencedcases21 + "', referencedcases22  = '" +
                                    item.referencedcases22 + "', referencedcases23 = '" + item.referencedcases23 +
                                    "', referencedcases24='" + item.referencedcases24 + "', referencedcases25='" +
                                    item.referencedcases25 + "', referencedcases26 ='" + item.referencedcases26 +
                                    "', referencedcases27='" + item.referencedcases27 + "', referencedcases28='" +
                                    item.referencedcases28 + "', referencedcases29='" + item.referencedcases29 +
                                    "', referencedcases30='" + item.referencedcases30 + "', referencedcases31='" +
                                    item.referencedcases31 + "', referencedcases32 = '" + item.referencedcases32 +
                                    "', referencedcases33='" + item.referencedcases33 + "', referencedcases34='" +
                                    item.referencedcases34 + "', referencedcases35='" + item.referencedcases35 +
                                    "', referencedcases36='" + item.referencedcases36 + "', referencedcases37='" +
                                    item.referencedcases37 + "', referencedcases38='" + item.referencedcases38 +
                                    "', referencedcases39='" + item.referencedcases39 + "', referencedcases40='" +
                                    item.referencedcases40 + "', referencedcases41='" + item.referencedcases41 +
                                    "', suitno1='" + item.suitno1 + "', suitno2='" + item.suitno2 + "', suitno3='" +
                                    item.suitno3 + "', suitno4='" + item.suitno4 + "', suitno5='" + item.suitno5 +
                                    "', suitno6='" + item.suitno6 + "', suitno7='" + item.suitno7 + "', suitno8='" +
                                    item.suitno8 + "', suitno9='" + item.suitno9 + "', suitno10='" + item.suitno11 +
                                    "', suitno12='" + item.suitno12 + "', suitno13='" + item.suitno13 + "', suitno14='" +
                                    item.suitno14 + "', suitno15='" + item.suitno15 + "', suitno16='" + item.suitno16 +
                                    "', suitno17='" + item.suitno17 + "', suitno18='" + item.suitno18 + "', suitno19='" +
                                    item.suitno19 + "', suitno20='" + item.suitno20 + "', suitno21='" + item.suitno21 +
                                    "', suitno22='" + item.suitno22 + "', suitno23='" + item.suitno23 +
                                    "', suitno24='" + item.suitno24 + "', suitno25='" + item.suitno25 + "', suitno26='" +
                                    item.suitno26 + "', suitno27='" + item.suitno27 + "', suitno28='" + item.suitno28 +
                                    "', suitno29='" + item.suitno29 + "', suitno30='" + item.suitno30 + "', suitno31='" +
                                    item.suitno31 + "', suitno32='" + item.suitno32 + "', suitno33='" + item.suitno33 +
                                    "', suitno34='" + item.suitno34 + "', suitno35='" + item.suitno35 + "', suitno36='" +
                                    item.suitno36 + "', suitno37='" + item.suitno37 + "', suitno38='" + item.suitno38 +
                                    "', suitno39='" + item.suitno39 + "', suitno40='" + item.suitno40 + "', suitno41='" +
                                    item.suitno41 + "' , statutes1='" + item.statutes1 + "', statutes2='" +
                                    item.statutes2 + "', statutes3='" + item.statutes3 + "', statutes4='" +
                                    item.statutes4 + "', statutes5='" + item.statutes5 + "', statutes6='" +
                                    item.statutes6 + "', statutes7='" + item.statutes7 + "', statutes8='" +
                                    item.statutes8 + "', statutes9='" + item.statutes9 + "', statutes10='" +
                                    item.statutes10 + "', statutes11='" + item.statutes11 + "', statutes12='" +
                                    item.statutes12 + "', statutes13='" + item.statutes13 + "', statutes14='" +
                                    item.statutes14 + "', statutes15='" + item.statutes15 + "', overruled='" +
                                    item.overruled + "',overruled_case='" + item.overruled_case +
                                    "', overruled_suitno='" + item.overruled_suitno + "', additional_note='" +
                                    item.additional_note + "', pk='" + item.pk + "', anchored='" + item.anchored +
                                    "', dt_modified='" + item.dt_modified + "', deleted='" + item.deleted +
                                    "' WHERE suitno='" +
                                    item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 500;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }

                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();

                    string sqlQuery = @"INSERT INTO " + table +
                                      " (year, date, suitno, [legal head], subjectmatter, issues1, principle,referencedcases1, referencedcases2, referencedcases3, referencedcases4, referencedcases5, referencedcases6, referencedcases7,referencedcases8, referencedcases9, referencedcases10, referencedcases11, referencedcases12, referencedcases13, referencedcases14, referencedcases15,referencedcases16,referencedcases17,referencedcases18,referencedcases19,referencedcases20,referencedcases21,referencedcases22,referencedcases23,referencedcases24,referencedcases25,referencedcases26,referencedcases27,referencedcases28,referencedcases29,referencedcases30,referencedcases31,referencedcases32,referencedcases33,referencedcases34,referencedcases35,referencedcases36,referencedcases37,referencedcases38,referencedcases39,referencedcases40,referencedcases41,suitno1,suitno2,suitno3,suitno4,suitno5,suitno6,suitno7,suitno8,suitno9,suitno10,suitno11,suitno12,suitno13,suitno14,suitno15,suitno16,suitno17,suitno18,suitno19,suitno20,suitno21,suitno22,suitno23,suitno24,suitno25,suitno26,suitno27,suitno28,suitno29,suitno30,suitno31,suitno32,suitno33,suitno34,suitno35,suitno36,suitno37,suitno38,suitno39,suitno40,suitno41,statutes1,statutes2,statutes3,statutes4,statutes5,statutes6,statutes7,statutes8,statutes9,statutes10,statutes11,statutes12,statutes13,statutes14,statutes15,overruled,overruled_case,overruled_suitno,additional_note,pk,anchored, judge_id,from_page,from_para,to_page,to_para,dt_modified,deleted) VALUES " +
                                      "('" + item.year + "','" + item.date + "', '" + item.suitno + "', '" +
                                      item.legal_head + "','" +
                                      item.subjectmatter + "', '" + item.issues1 +
                                      "', '" + item.principle + "', '" +
                                      item.referencedcases1 + "', '" +
                                      item.referencedcases2 + "', '" +
                                      item.referencedcases3 + "', '" +
                                      item.referencedcases4 + "', '" + item.referencedcases5 +
                                      "', '" + item.referencedcases6 + "', '" +
                                      item.referencedcases7 + "', '" +
                                      item.referencedcases8 + "', '" + item.referencedcases9 + "', '" +
                                      item.referencedcases10 + "', '" +
                                      item.referencedcases11 + "', '" + item.referencedcases12 + "', '" +
                                      item.referencedcases13 + "', '" +
                                      item.referencedcases14 + "', '" + item.referencedcases15 + "', '" +
                                      item.referencedcases16 + "', '" + item.referencedcases17 +
                                      "', '" + item.referencedcases18 + "', '" + item.referencedcases19 + "', '" +
                                      item.referencedcases20 + "', '" +
                                      item.referencedcases21 + "','" +
                                      item.referencedcases22 + "','" +
                                      item.referencedcases23 + "','" +
                                      item.referencedcases24 + "','" +
                                      item.referencedcases25 + "','" +
                                      item.referencedcases26 + "','" +
                                      item.referencedcases27 + "','" +
                                      item.referencedcases28 + "','" +
                                      item.referencedcases29 + "','" +
                                      item.referencedcases30 + "','" +
                                      item.referencedcases31 + "','" +
                                      item.referencedcases32 + "','" +
                                      item.referencedcases33 + "','" +
                                      item.referencedcases34 + "','" +
                                      item.referencedcases35 + "','" +
                                      item.referencedcases36 + "','" +
                                      item.referencedcases37 + "','" +
                                      item.referencedcases38 + "','" +
                                      item.referencedcases39 + "','" +
                                      item.referencedcases40 + "','" +
                                      item.referencedcases41 + "','" +
                                      item.suitno1 + "','" +
                                      item.suitno2 + "','" +
                                      item.suitno3 + "','" +
                                      item.suitno4 + "','" +
                                      item.suitno5 + "','" +
                                      item.suitno6 + "','" +
                                      item.suitno7 + "','" +
                                      item.suitno8 + "','" +
                                      item.suitno9 + "','" +
                                      item.suitno10 + "','" +
                                      item.suitno11 + "','" +
                                      item.suitno12 + "','" +
                                      item.suitno13 + "','" +
                                      item.suitno14 + "','" +
                                      item.suitno15 + "','" +
                                      item.suitno16 + "','" +
                                      item.suitno17 + "','" +
                                      item.suitno18 + "','" +
                                      item.suitno19 + "','" +
                                      item.suitno20 + "','" +
                                      item.suitno21 + "','" +
                                      item.suitno22 + "','" +
                                      item.suitno23 + "','" +
                                      item.suitno24 + "','" +
                                      item.suitno25 + "','" +
                                      item.suitno26 + "','" +
                                      item.suitno27 + "','" +
                                      item.suitno28 + "','" +
                                      item.suitno29 + "','" +
                                      item.suitno30 + "','" +
                                      item.suitno31 + "','" +
                                      item.suitno32 + "','" +
                                      item.suitno33 + "','" +
                                      item.suitno34 + "','" +
                                      item.suitno35 + "','" +
                                      item.suitno36 + "','" +
                                      item.suitno37 + "','" +
                                      item.suitno38 + "','" +
                                      item.suitno39 + "','" +
                                      item.suitno40 + "','" +
                                      item.suitno41 + "','" +
                                      item.statutes1 + "','" +
                                      item.statutes2 + "','" +
                                      item.statutes3 + "','" +
                                      item.statutes4 + "','" +
                                      item.statutes5 + "','" +
                                      item.statutes6 + "','" +
                                      item.statutes7 + "','" +
                                      item.statutes8 + "','" +
                                      item.statutes9 + "','" +
                                      item.statutes10 + "','" +
                                      item.statutes11 + "','" +
                                      item.statutes12 + "','" +
                                      item.statutes13 + "', '" +
                                      item.statutes14 + "','" +
                                      item.statutes15 + "','" +
                                      item.overruled + "','" +
                                      item.overruled_case + "','" +
                                      item.overruled_suitno + "','" +
                                      item.additional_note + "','" +
                                      item.pk + "','" +
                                      item.anchored + "',  '" + item.judge_id + "', '" + item.from_page + "', '" +
                                      item.from_para + "', '" + item.to_page + "', '" + item.to_para + "', '" +
                                      item.dt_modified + "','" +
                                      item.deleted + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        private void GetTATAnalysis(List<LPV> court, string table)
        {
            foreach (var item in court)
            {
                #region
                SqlConnection.ClearAllPools();

                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "' and dt_modified = '" + item.dt_modified + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 500;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    string update = @"UPDATE " + table + "  SET year='" + item.year + "', date= '" + item.date +
                                    "', [legal head]= '" +
                                    item.legal_head + "', subjectmatter = '" +
                                    item.subjectmatter + "', issues1 = '" + item.issues1 +
                                    "', principle = '" + item.principle + "', referencedcases1='" +
                                    item.referencedcases1 + "', referencedcases2= '" +
                                    item.referencedcases2 + "', referencedcases3= '" +
                                    item.referencedcases3 + "', referencedcases4='" +
                                    item.referencedcases4 + "', referencedcases5='" +
                                    item.referencedcases5 + "', referencedcases6='" + item.referencedcases6 +
                                    "', referencedcases7='" + item.referencedcases7 + "', referencedcases8='" +
                                    item.referencedcases8 + "', referencedcases9='" + item.referencedcases9 +
                                    "', referencedcases10='" + item.referencedcases10 + "', referencedcases11='" +
                                    item.referencedcases11 + "', referencedcases12='" + item.referencedcases12 +
                                    "', referencedcases13='" + item.referencedcases13 + "', referencedcases14='" +
                                    item.referencedcases14 + "', referencedcases15='" + item.referencedcases15 +
                                    "', referencedcases16 = '" + item.referencedcases16 + "', referencedcases17 = '" +
                                    item.referencedcases17 +
                                    "', referencedcases18 = '" + item.referencedcases18 + "',referencedcases19='" +
                                    item.referencedcases19 + "', referencedcases20 = '" + item.referencedcases20 +
                                    "' , referencedcases21 = '" + item.referencedcases21 + "', referencedcases22  = '" +
                                    item.referencedcases22 + "', referencedcases23 = '" + item.referencedcases23 +
                                    "', referencedcases24='" + item.referencedcases24 + "', referencedcases25='" +
                                    item.referencedcases25 + "', referencedcases26 ='" + item.referencedcases26 +
                                    "', referencedcases27='" + item.referencedcases27 + "', referencedcases28='" +
                                    item.referencedcases28 + "', referencedcases29='" + item.referencedcases29 +
                                    "', referencedcases30='" + item.referencedcases30 + "', referencedcases31='" +
                                    item.referencedcases31 + "', referencedcases32 = '" + item.referencedcases32 +
                                    "', referencedcases33='" + item.referencedcases33 + "', referencedcases34='" +
                                    item.referencedcases34 + "', referencedcases35='" + item.referencedcases35 +
                                    "', referencedcases36='" + item.referencedcases36 + "', referencedcases37='" +
                                    item.referencedcases37 + "', referencedcases38='" + item.referencedcases38 +
                                    "', referencedcases39='" + item.referencedcases39 + "', referencedcases40='" +
                                    item.referencedcases40 + "', referencedcases41='" + item.referencedcases41 +
                                    "', suitno1='" + item.suitno1 + "', suitno2='" + item.suitno2 + "', suitno3='" +
                                    item.suitno3 + "', suitno4='" + item.suitno4 + "', suitno5='" + item.suitno5 +
                                    "', suitno6='" + item.suitno6 + "', suitno7='" + item.suitno7 + "', suitno8='" +
                                    item.suitno8 + "', suitno9='" + item.suitno9 + "', suitno10='" + item.suitno11 +
                                    "', suitno12='" + item.suitno12 + "', suitno13='" + item.suitno13 + "', suitno14='" +
                                    item.suitno14 + "', suitno15='" + item.suitno15 + "', suitno16='" + item.suitno16 +
                                    "', suitno17='" + item.suitno17 + "', suitno18='" + item.suitno18 + "', suitno19='" +
                                    item.suitno19 + "', suitno20='" + item.suitno20 + "', suitno21='" + item.suitno21 +
                                    "', suitno22='" + item.suitno22 + "', suitno23='" + item.suitno23 +
                                    "', suitno24='" + item.suitno24 + "', suitno25='" + item.suitno25 + "', suitno26='" +
                                    item.suitno26 + "', suitno27='" + item.suitno27 + "', suitno28='" + item.suitno28 +
                                    "', suitno29='" + item.suitno29 + "', suitno30='" + item.suitno30 + "', suitno31='" +
                                    item.suitno31 + "', suitno32='" + item.suitno32 + "', suitno33='" + item.suitno33 +
                                    "', suitno34='" + item.suitno34 + "', suitno35='" + item.suitno35 + "', suitno36='" +
                                    item.suitno36 + "', suitno37='" + item.suitno37 + "', suitno38='" + item.suitno38 +
                                    "', suitno39='" + item.suitno39 + "', suitno40='" + item.suitno40 + "', suitno41='" +
                                    item.suitno41 + "' , statutes1='" + item.statutes1 + "', statutes2='" +
                                    item.statutes2 + "', statutes3='" + item.statutes3 + "', statutes4='" +
                                    item.statutes4 + "', statutes5='" + item.statutes5 + "', statutes6='" +
                                    item.statutes6 + "', statutes7='" + item.statutes7 + "', statutes8='" +
                                    item.statutes8 + "', statutes9='" + item.statutes9 + "', statutes10='" +
                                    item.statutes10 + "', statutes11='" + item.statutes11 + "', statutes12='" +
                                    item.statutes12 + "', statutes13='" + item.statutes13 + "', statutes14='" +
                                    item.statutes14 + "', statutes15='" + item.statutes15 + "', overruled='" +
                                    item.overruled + "',overruled_case='" + item.overruled_case +
                                    "', overruled_suitno='" + item.overruled_suitno + "', additional_note='" +
                                    item.additional_note + "', pk='" + item.pk + "', anchored='" + item.anchored +
                                    "', dt_modified='" + item.dt_modified + "', deleted='" + item.deleted +
                                    "' WHERE suitno='" +
                                    item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 500;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }

                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();

                    string sqlQuery = @"INSERT INTO " + table +
                                      " (year, date, suitno, [legal head], subjectmatter, issues1, principle,referencedcases1, referencedcases2, referencedcases3, referencedcases4, referencedcases5, referencedcases6, referencedcases7,referencedcases8, referencedcases9, referencedcases10, referencedcases11, referencedcases12, referencedcases13, referencedcases14, referencedcases15,referencedcases16,referencedcases17,referencedcases18,referencedcases19,referencedcases20,referencedcases21,referencedcases22,referencedcases23,referencedcases24,referencedcases25,referencedcases26,referencedcases27,referencedcases28,referencedcases29,referencedcases30,referencedcases31,referencedcases32,referencedcases33,referencedcases34,referencedcases35,referencedcases36,referencedcases37,referencedcases38,referencedcases39,referencedcases40,referencedcases41,suitno1,suitno2,suitno3,suitno4,suitno5,suitno6,suitno7,suitno8,suitno9,suitno10,suitno11,suitno12,suitno13,suitno14,suitno15,suitno16,suitno17,suitno18,suitno19,suitno20,suitno21,suitno22,suitno23,suitno24,suitno25,suitno26,suitno27,suitno28,suitno29,suitno30,suitno31,suitno32,suitno33,suitno34,suitno35,suitno36,suitno37,suitno38,suitno39,suitno40,suitno41,statutes1,statutes2,statutes3,statutes4,statutes5,statutes6,statutes7,statutes8,statutes9,statutes10,statutes11,statutes12,statutes13,statutes14,statutes15,overruled,overruled_case,overruled_suitno,additional_note,pk,anchored, judge_id,from_page,from_para,to_page,to_para,dt_modified,deleted) VALUES " +
                                      "('" + item.year + "','" + item.date + "', '" + item.suitno + "', '" +
                                      item.legal_head + "','" +
                                      item.subjectmatter + "', '" + item.issues1 +
                                      "', '" + item.principle + "', '" +
                                      item.referencedcases1 + "', '" +
                                      item.referencedcases2 + "', '" +
                                      item.referencedcases3 + "', '" +
                                      item.referencedcases4 + "', '" + item.referencedcases5 +
                                      "', '" + item.referencedcases6 + "', '" +
                                      item.referencedcases7 + "', '" +
                                      item.referencedcases8 + "', '" + item.referencedcases9 + "', '" +
                                      item.referencedcases10 + "', '" +
                                      item.referencedcases11 + "', '" + item.referencedcases12 + "', '" +
                                      item.referencedcases13 + "', '" +
                                      item.referencedcases14 + "', '" + item.referencedcases15 + "', '" +
                                      item.referencedcases16 + "', '" + item.referencedcases17 +
                                      "', '" + item.referencedcases18 + "', '" + item.referencedcases19 + "', '" +
                                      item.referencedcases20 + "', '" +
                                      item.referencedcases21 + "','" +
                                      item.referencedcases22 + "','" +
                                      item.referencedcases23 + "','" +
                                      item.referencedcases24 + "','" +
                                      item.referencedcases25 + "','" +
                                      item.referencedcases26 + "','" +
                                      item.referencedcases27 + "','" +
                                      item.referencedcases28 + "','" +
                                      item.referencedcases29 + "','" +
                                      item.referencedcases30 + "','" +
                                      item.referencedcases31 + "','" +
                                      item.referencedcases32 + "','" +
                                      item.referencedcases33 + "','" +
                                      item.referencedcases34 + "','" +
                                      item.referencedcases35 + "','" +
                                      item.referencedcases36 + "','" +
                                      item.referencedcases37 + "','" +
                                      item.referencedcases38 + "','" +
                                      item.referencedcases39 + "','" +
                                      item.referencedcases40 + "','" +
                                      item.referencedcases41 + "','" +
                                      item.suitno1 + "','" +
                                      item.suitno2 + "','" +
                                      item.suitno3 + "','" +
                                      item.suitno4 + "','" +
                                      item.suitno5 + "','" +
                                      item.suitno6 + "','" +
                                      item.suitno7 + "','" +
                                      item.suitno8 + "','" +
                                      item.suitno9 + "','" +
                                      item.suitno10 + "','" +
                                      item.suitno11 + "','" +
                                      item.suitno12 + "','" +
                                      item.suitno13 + "','" +
                                      item.suitno14 + "','" +
                                      item.suitno15 + "','" +
                                      item.suitno16 + "','" +
                                      item.suitno17 + "','" +
                                      item.suitno18 + "','" +
                                      item.suitno19 + "','" +
                                      item.suitno20 + "','" +
                                      item.suitno21 + "','" +
                                      item.suitno22 + "','" +
                                      item.suitno23 + "','" +
                                      item.suitno24 + "','" +
                                      item.suitno25 + "','" +
                                      item.suitno26 + "','" +
                                      item.suitno27 + "','" +
                                      item.suitno28 + "','" +
                                      item.suitno29 + "','" +
                                      item.suitno30 + "','" +
                                      item.suitno31 + "','" +
                                      item.suitno32 + "','" +
                                      item.suitno33 + "','" +
                                      item.suitno34 + "','" +
                                      item.suitno35 + "','" +
                                      item.suitno36 + "','" +
                                      item.suitno37 + "','" +
                                      item.suitno38 + "','" +
                                      item.suitno39 + "','" +
                                      item.suitno40 + "','" +
                                      item.suitno41 + "','" +
                                      item.statutes1 + "','" +
                                      item.statutes2 + "','" +
                                      item.statutes3 + "','" +
                                      item.statutes4 + "','" +
                                      item.statutes5 + "','" +
                                      item.statutes6 + "','" +
                                      item.statutes7 + "','" +
                                      item.statutes8 + "','" +
                                      item.statutes9 + "','" +
                                      item.statutes10 + "','" +
                                      item.statutes11 + "','" +
                                      item.statutes12 + "','" +
                                      item.statutes13 + "', '" +
                                      item.statutes14 + "','" +
                                      item.statutes15 + "','" +
                                      item.overruled + "','" +
                                      item.overruled_case + "','" +
                                      item.overruled_suitno + "','" +
                                      item.additional_note + "','" +
                                      item.pk + "','" +
                                      item.anchored + "',  '" + item.judge_id + "', '" + item.from_page + "', '" +
                                      item.from_para + "', '" + item.to_page + "', '" + item.to_para + "', '" +
                                      item.dt_modified + "','" +
                                      item.deleted + "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }


        private void GetAllLibraryBooks(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "' and dt_modified = '" + item.dt_modified + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET booktitle= '" + item.booktitle + "', [desc]= '" +
                                    convertQuotes(item.desc) + "', subsection_id = '" +
                                    item.subsection_id + "', cat_id = '" + item.cat_id +
                                    "', years = '" + item.years + "', certified_by='" +
                                    convertQuotes(item.certified_by) + "', status = '" + item.status +
                                    "', dt_modified='" + (item.dt_modified) + "', deleted = '" + item.deleted +
                                    "' WHERE suitno = '" + item.suitno + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([booktitle], [desc], [subsection_id], [suitno], [cat_id], [years], [certified_by],[status],deleted,dt_modified) VALUES " +
                                      "('" + item.booktitle + "', '" + convertQuotes(item.desc) + "', '" +
                                      item.subsection_id + "','" +
                                      convertQuotes(item.suitno) + "', '" + item.cat_id +
                                      "', '" + item.years + "', '" +
                                      convertQuotes(item.certified_by) + "', '" +
                                      convertQuotes(item.status) + "', '" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();

                       

                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        public void GetAllPagesNIC(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 100000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET content= '" + convertQuotes(item.content) +
                                    "',  dt_modified = '" + item.dt_modified +
                                    "', deleted = '" + item.deleted +
                                    "' WHERE suitno = '" + item.suitno +
                                    "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 100000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([suitno], [page_no2], [content], [page_no], [deleted], [dt_modified]) VALUES " +
                                      "('" + item.suitno + "', '" + convertQuotes(item.page_no2) + "', '" +
                                      convertQuotes(item.content) + "','" +
                                      convertQuotes(item.page_no) + "', '" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 100000;
                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        conn.Close();
                    }



                #endregion

                }
            }
            
        }

        public void GetAllPagesStates(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET content= '" + convertQuotes(item.content) +
                                    "',  dt_modified = '" + item.dt_modified +
                                    "', deleted = '" + item.deleted +
                                    "' WHERE suitno = '" + item.suitno +
                                    "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([suitno], [page_no2], [content], [page_no], [deleted], [dt_modified]) VALUES " +
                                      "('" + item.suitno + "', '" + convertQuotes(item.page_no2) + "', '" +
                                      convertQuotes(item.content) + "','" +
                                      convertQuotes(item.page_no) + "', '" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }

        }


        public void GetAllPagesTat(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE suitno = '" + item.suitno +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET content= '" + convertQuotes(item.content) +
                                    "',  dt_modified = '" + item.dt_modified +
                                    "', deleted = '" + item.deleted +
                                    "' WHERE suitno = '" + item.suitno +
                                    "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([suitno],  [page_no], [content], [deleted], [dt_modified]) VALUES " +
                                      "('" + item.suitno + "', '" + convertQuotes(item.page_no) + "', '" +
                                      convertQuotes(item.content) + "','" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();
                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }

        }

        private void GetAllStateLaw(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE section_Number = '" + item.section_Number +
                                  "' and name = '" + convertQuotes(item.name) + "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET part_Desc= '" + convertQuotes(item.part_Desc) + "', [section_Desc]= '" +
                                    convertQuotes(item.section_Desc) + "', state = '" +
                                    convertQuotes(item.state) + "', dt_modified = '" + item.dt_modified +
                                    "', deleted = '" + item.deleted +
                                    "' WHERE section_Number = '" + convertQuotes(item.section_Number) +
                                    "' and name = '" + convertQuotes(item.name )+ "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([name], [chapter_number], [chapter_desc], [part_number], [part_Desc], [subpart_number], [subpart_Desc],[section_Number],section_Desc,state,deleted,dt_modified) VALUES " +
                                      "('" + convertQuotes(item.name) + "', '" + convertQuotes(item.chapter_number) + "', '" +
                                      convertQuotes(item.chapter_Desc )+ "','" +
                                      convertQuotes(item.part_number) + "', '" + convertQuotes(item.part_Desc) +
                                      "', '" + convertQuotes(item.subpart_number) + "', '" +
                                      convertQuotes(item.subpart_Desc) + "', '" +
                                      convertQuotes(item.section_Number) + "',   '" + convertQuotes(item.section_Desc) + "', '" + convertQuotes(item.state) + "', '" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();
                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }

        private void GetAlllaw_subsubsection(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE name = '" + convertQuotes(item.name) +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    string update = @"UPDATE " + table + "  SET subsubsection_number= '" + convertQuotes(item.subsubsection_number) + "', [subsubsection_content]= '" +
                                    convertQuotes(item.subsubsection_content) + "', dt_modified = '" + item.dt_modified +
                                    "', deleted = '" + item.deleted +
                                    "', dt_modified= '" + item.dt_modified + "' WHERE name = '" + convertQuotes(item.name) + "'";

                    using (
                        SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    {
                        myConnection.Open();
                        myCommand.CommandTimeout = 1000;
                        int i = myCommand.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                        }
                        myConnection.Close();
                    }
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([pk], [name], [subsubsection_number], [subsubsection_content], deleted,dt_modified) VALUES " +
                                      "('" + item.pk + "', '" + convertQuotes(item.name) + "', '" +
                                      convertQuotes(item.subsubsection_number) + "','" +
                                      convertQuotes(item.subsubsection_content) + "','" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }



                #endregion

                }
            }
        }



        private void GetAlllaw_Sechudle(List<LPV> casess, string table)
        {
            foreach (var item in casess)
            {
                #region
                SqlConnection.ClearAllPools();
                //check if the suitno exist
                string sqlCheck = @"SELECT * FROM " + table + " WHERE name = '" + convertQuotes(item.name) +
                                  "'";
                SqlConnection myconnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]);
                SqlCommand cmdConnection = new SqlCommand(sqlCheck, myconnection);

                myconnection.Open();
                cmdConnection.CommandTimeout = 1000;
                SqlDataReader rd = cmdConnection.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    // if data exist
                    //update the previous data

                    //string update = @"UPDATE " + table + "  SET type= '" + item.type + "', [schedule_number]= '" +
                    //                convertQuotes(item.schedule_number) + "', deleted = '" + item.deleted +
                    //                "',dt_modified = '" + item.dt_modified +
                    //                "'  WHERE name = '" + convertQuotes(item.name) + "' and type='" + item.type + "'";

                    //using (
                    //    SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    //using (SqlCommand myCommand = new SqlCommand(update, myConnection))
                    //{
                    //    myConnection.Open();
                    //    myCommand.CommandTimeout = 1000;
                    //    int i = myCommand.ExecuteNonQuery();
                    //    if (i == 1)
                    //    {
                    //        Logger.writelog("Update Code successful " + " " + " " + DateTime.Now);
                    //    }
                    //    else
                    //    {
                    //        Logger.writelog("Error Occurred while Updating existing data " + " " + " " + DateTime.Now);
                    //    }
                    //    myConnection.Close();
                    //}
                    myconnection.Close();

                }
                else
                {
                    SqlConnection.ClearAllPools();
                    string sqlQuery = @"INSERT INTO " + table +
                                      " ([name], [type], [schedule_number], [content], deleted,dt_modified) VALUES " +
                                      "('" + convertQuotes(item.name) + "',  '"+convertQuotes(item.type)+"',  '" +
                                      convertQuotes(item.schedule_number) + "', CONVERT(varbinary(max),'" + convertQuotes(item.content) + "'),'" + item.deleted + "', '" + item.dt_modified +
                                      "')";

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlQuery;
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 500;


                        conn.Open();



                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {
                            Logger.writelog("Update Code Inserted " + " " + " " + DateTime.Now);
                        }
                        else
                        {
                            Logger.writelog("Error occurred " + " " + " " + DateTime.Now);
                        }
                        // conn.Close();
                    }

                #endregion

                }
            }
        }


        public string all()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            string res = "";

            foreach (var item in _casesss)
            {
                string date = GetDateModifed(item);
                DateTime dateValue = DateTime.Parse(date);
                string formatForMySql = dateValue.ToString("yyyy-MM-dd HH:mm");
                _dictionary.Clear();
                _dictionary.Add("table", item);
                _dictionary.Add("dt_modified", formatForMySql.Replace("/", "-"));
                res = HttpPostRequest(BASE_URL, _dictionary);
                if (res != "")
                {
                    Logger.writelog("Fetched:" + " " + res);
                }
                else
                {
                    return "";
                }
            }
            return res;
        }

        private string convertQuotes(string str)
        {
            if (str == "" || str == null)
            {
                return "";
            }
            else
            {

                return str.Replace("'", "''");
            }

        }

        public string GetDateModifed(string table)
        {
            object obj = null;
            string sql = @"SELECT dt_modified from "+ table + " order by dt_modified desc";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["connSTKnew"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                obj = cmd.ExecuteScalar();
            }
            return obj.ToString();
        }

        public class  UpdateData
        {
            public string table { get; set; }
            public string dt_modified { get; set; }
        }
    }
}