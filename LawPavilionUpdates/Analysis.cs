﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.LawPavilionUpdates
{
    public class Analysis
    {

        public class RootObject
        {
            public string year { get; set; }
            public string date { get; set; }
            public string suitno { get; set; }
            public string __invalid_name__legal_head { get; set; }
            public object subjectmatter { get; set; }
            public object issues1 { get; set; }
            public object principle { get; set; }
            public string referencedcases1 { get; set; }
            public string referencedcases2 { get; set; }
            public string referencedcases3 { get; set; }
            public string referencedcases4 { get; set; }
            public string referencedcases5 { get; set; }
            public object referencedcases6 { get; set; }
            public object referencedcases7 { get; set; }
            public object referencedcases8 { get; set; }
            public object referencedcases9 { get; set; }
            public object referencedcases10 { get; set; }
            public object referencedcases11 { get; set; }
            public object referencedcases12 { get; set; }
            public object referencedcases13 { get; set; }
            public object referencedcases14 { get; set; }
            public object referencedcases15 { get; set; }
            public object referencedcases16 { get; set; }
            public object referencedcases17 { get; set; }
            public object referencedcases18 { get; set; }
            public object referencedcases19 { get; set; }
            public object referencedcases20 { get; set; }
            public object referencedcases21 { get; set; }
            public object referencedcases22 { get; set; }
            public object referencedcases23 { get; set; }
            public object referencedcases24 { get; set; }
            public object referencedcases25 { get; set; }
            public object referencedcases26 { get; set; }
            public object referencedcases27 { get; set; }
            public object referencedcases28 { get; set; }
            public object referencedcases29 { get; set; }
            public object referencedcases30 { get; set; }
            public object referencedcases31 { get; set; }
            public object referencedcases32 { get; set; }
            public object referencedcases33 { get; set; }
            public object referencedcases34 { get; set; }
            public object referencedcases35 { get; set; }
            public object referencedcases36 { get; set; }
            public object referencedcases37 { get; set; }
            public object referencedcases38 { get; set; }
            public object referencedcases39 { get; set; }
            public object referencedcases40 { get; set; }
            public object referencedcases41 { get; set; }
            public object suitno1 { get; set; }
            public object suitno2 { get; set; }
            public object suitno3 { get; set; }
            public object suitno4 { get; set; }
            public object suitno5 { get; set; }
            public object suitno6 { get; set; }
            public object suitno7 { get; set; }
            public object suitno8 { get; set; }
            public object suitno9 { get; set; }
            public object suitno10 { get; set; }
            public object suitno11 { get; set; }
            public object suitno12 { get; set; }
            public object suitno13 { get; set; }
            public object suitno14 { get; set; }
            public object suitno15 { get; set; }
            public object suitno16 { get; set; }
            public object suitno17 { get; set; }
            public object suitno18 { get; set; }
            public object suitno19 { get; set; }
            public object suitno20 { get; set; }
            public object suitno21 { get; set; }
            public object suitno22 { get; set; }
            public object suitno23 { get; set; }
            public object suitno24 { get; set; }
            public object suitno25 { get; set; }
            public object suitno26 { get; set; }
            public object suitno27 { get; set; }
            public object suitno28 { get; set; }
            public object suitno29 { get; set; }
            public object suitno30 { get; set; }
            public object suitno31 { get; set; }
            public object suitno32 { get; set; }
            public object suitno33 { get; set; }
            public object suitno34 { get; set; }
            public object suitno35 { get; set; }
            public object suitno36 { get; set; }
            public object suitno37 { get; set; }
            public object suitno38 { get; set; }
            public object suitno39 { get; set; }
            public object suitno40 { get; set; }
            public object suitno41 { get; set; }
            public string statutes1 { get; set; }
            public string statutes2 { get; set; }
            public object statutes3 { get; set; }
            public object statutes4 { get; set; }
            public object statutes5 { get; set; }
            public object statutes6 { get; set; }
            public object statutes7 { get; set; }
            public object statutes8 { get; set; }
            public object statutes9 { get; set; }
            public object statutes10 { get; set; }
            public object statutes11 { get; set; }
            public object statutes12 { get; set; }
            public object statutes13 { get; set; }
            public object statutes14 { get; set; }
            public object statutes15 { get; set; }
            public string overruled { get; set; }
            public object overruled_case { get; set; }
            public string overruled_suitno { get; set; }
            public string additional_note { get; set; }
            public string pk { get; set; }
            public string anchored { get; set; }
            public string dt_modified { get; set; }
            public string deleted { get; set; }
        }

    }
}