﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CaseManager.Logic;
using CaseManager.Util;
using CaseManager.Model;
using Newtonsoft.Json;

namespace CaseManager.MasterPages
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        public static string user;
        public static string role;
        public Object _roleID;
        public Object _userID;
        private HttpCookie cookie = new HttpCookie("ID");
        private HttpCookie cookie2 = new HttpCookie("RoleID");

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Auth.isActive())
            //{
            //    if (!Auth.isAdmin())
            //    {
            //        Response.Redirect("~/logout.aspx", true);
            //    }

            //}
            //else
            //{
            //    Response.Redirect("~/logout.aspx", true);
            //}

            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
                HttpServerTest.Logger.Logger.writelog(_userID.ToString() + "coo");
            }


            HttpServerTest.Logger.Logger.writelog(_userID.ToString());

            User u = Temp.GetUser((string) _userID);

            UserDetails ud = new UserDetails();

            Label myLabel = this.FindControl("currentUserDesc") as Label;
            myLabel.Text = "SIGNED IN AS: " + ud.getRoleDesc(u.RoleId.ToString()).ToUpper();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "proxy",
                "PageHandler.Page.buildMenu('" + JsonConvert.SerializeObject(u) + "');", true);

            

        }
    }
}