﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script;
using System.Data;
using CaseManager.Logic;
using CaseManager.Util;
using CaseManager.Model;
using Newtonsoft.Json;

namespace CaseManager.MasterPages
{
    public partial class Dasboard : System.Web.UI.MasterPage
    {
        public static object UserID;
        public static object _id;
        public  Object _roleID;
        public  Object _userID;
        private HttpCookie cookie = new HttpCookie("ID");
        private HttpCookie cookie2 = new HttpCookie("RoleID");

        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.ClearHeaders();
            //Response.ClearHeaders();

            Response.AppendHeader("Cache-Control", "no-cache"); //HTTP 1.1
            Response.AppendHeader("Cache-Control", "no-store"); // HTTP 1.1
            Response.AppendHeader("Cache-Control", "must-revalidate"); // HTTP 1.1
            Response.AppendHeader("Cache-Control", "max-stale=0"); // HTTP 1.1 
            Response.AppendHeader("Cache-Control", "post-check=0"); // HTTP 1.1 
            Response.AppendHeader("Cache-Control", "pre-check=0"); // HTTP 1.1 
            Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0 
            Response.AppendHeader("Expires", "Sat, 12 Oct 1991 05:00:00 GMT"); // HTTP 1.0\


            if ((Request.Browser.Browser.ToLower() == "ie") && (Request.Browser.MajorVersion < 9))
            {
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetMaxAge(TimeSpan.FromMilliseconds(1));
                Response.Cache.SetExpires(DateTime.UtcNow);
            }
            else
            {
                Response.Cache.SetCacheability(HttpCacheability.NoCache); //IE set to not cache
                Response.Cache.SetNoStore(); //Firefox/Chrome not to cache
                Response.Cache.SetExpires(DateTime.UtcNow); //for safe measure expire it immediately
            }
            //if (Request.QueryString.Get("uni") != null)
            //{
            //    string data = Crypt.Decode(Request.QueryString.Get("uni"));  // new method
            //}
            //if (HttpContext.Current.Session["ID"] != null)
            //{
            //    _userID = HttpContext.Current.Session["ID"];
            //}
            //if (HttpContext.Current.Session["RoleID"] != null)
            //{
            //    _roleID = HttpContext.Current.Session["RoleID"];
            //}

            cookie = HttpContext.Current.Request.Cookies["ID"];
            cookie2 = HttpContext.Current.Request.Cookies["RoleID"];
            if (cookie == null && cookie2 == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            else 
            {
                _userID = cookie.Value;
                _roleID = cookie2.Value;

                DataSet result = CaseManager.Util.Temp.GetUserLogin(_userID.ToString());
                if (result.Tables[0].Rows[0].ItemArray[3].ToString() == Constant.ROLE_ADMIN.ToString())
                {
                    Response.Redirect("~/Admin/", true);
                }
                else
                {
                    // cookie2 = HttpContext.Current.Request.Cookies["RoleID"];

                    DataSet process_result = Temp.GetUserProcess(_roleID.ToString());
                    DataSet stage_result = Temp.GetStartStage(_roleID.ToString());

                    result.Tables[0].TableName = "user";
                    stage_result.Tables[0].TableName = "start_stage";
                    process_result.Tables[0].TableName = "process";


                    result.Tables.Add(process_result.Tables[0].Copy());
                    result.Tables.Add(stage_result.Tables[0].Copy());



                    DataSet u = result;
                    UserDetails ud = new UserDetails();
                    try
                    {
                        Label myLabel = this.FindControl("currentUserDesc") as Label;
                        myLabel.Text = "SIGNED IN AS: " +
                                       ud.getRoleDesc(u.Tables["user"].Rows[0]["role_id"].ToString()).ToUpper() +
                                       " (" +
                                       ud.getRegion(u.Tables["user"].Rows[0]["branch_id"].ToString()).ToUpper() +
                                       ") " +
                                       ud.get_Branch(u.Tables["user"].Rows[0]["branch_id"].ToString()).ToUpper();
                    }
                    catch (Exception ex)
                    {
                        //Response.Redirect("~/Default.aspx");
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "proxy",
                        "UserHandler.init('" + JsonConvert.SerializeObject(u) + "');", true);
                }
            }
        }
    }
}