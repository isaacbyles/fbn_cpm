﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using CaseManager.Util;
using CaseManager.Logic;
using CaseManager.LawPavilion.Util;
using System.Data;
using System.Web.SessionState;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for AllComments
    /// </summary>
    public class AllComments : IHttpHandler
    {
        private LPCMConnection objConnection;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
           
            objConnection = new LPCMConnection();
            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);
            context.Response.Write(json);
        }
        private dynamic Handler(string pathinfo)
        {

            String[] parameters = pathinfo.Split('/');
            if (parameters.Length != 3) { return null; }
            string user_id = parameters[1];
            string request_id = parameters[2];

            return getComments(user_id,request_id);
        }

        private dynamic getComments(string user_id, string request_id)
        {
            string query = @"select request.id as request_id,request.title,process.id as process_id,stage.name as stage_name,stage.[description] as stage_description,label,value,concat(firstname,' ',lastname) as username,request_trail.created_date from request 
                            join process on request.process_id = process.id
                            join stage on process.id = stage.process_id 
                            join stage_form on stage.id = stage_form.stage_id
                            join form_control on stage_form.form_id = form_control.form_id 
                            join process_form_text_data on request.id = process_form_text_data.request_id and process_form_text_data.form_control_id = form_control.id
                            join request_trail on request.id = request_trail.request_id and request_trail.stage_id = stage.id
                            join [user] on request_trail.user_id = [user].id 
                            join [profile] on [user].id = [profile].user_id
                            where label like 'comment%' and request.id = " + request_id +@"
                            order by request.id,request_trail.id";
            objConnection.Sql = query;
            return objConnection.GetTable;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}