﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using CaseManager.Model;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Policy;
using System.Web.Script.Serialization;
using CaseManager.Logic;
using CaseManager.Util;
using Newtonsoft.Json;
using Logger = CaseManager.HttpServerTest.Logger.Logger;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for ExternalSolicitor
    /// </summary>
    public class ExternalSolicitor : IHttpHandler
    {
        static AuditLogic audit = new AuditLogic();
        private static string DEFAULT_MAIL_BOX = "tundeesanju@gmail.com";
        private static string UnDecrpytPassword;
        private static int ext_id;
        private HttpContext context;
        private static string external_solicitor_email;
        private static string external_solicitor_name;

        //private static AuditLogic audit = new AuditLogic();
        private HttpCookie cookie;
        private Object _userID;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            this.context = context;
            context.Response.ContentType = "application/json";

            string action = context.Request.QueryString["a"];
            DataTable response = null;
            switch (action)
            {
                case "add":
                    response = this.SaveExternal(context);
                    break;
                case "getall":
                    response = this.GetExternal();
                    break;
                case "edit":
                    response = this.UpdateExternal(context);
                    break;
                case "delete":
                    response = this.DeleteExternal(context);
                    break;
                case "reset":
                    response = this.ResetExternal(context);
                    break;
            }
            string json = JsonConvert.SerializeObject(response, Formatting.Indented);
            context.Response.Write(json);

        }

        public object SaveUsers(string email, string password, short role_id, int branch_id)
        {
            //CaseManager.Model.User u = new CaseManager.Model.User(email, password, branch_id, role_id);
            //u.save();
            string sql = @"INSERT INTO [user]
                            (email, password, branch_id, role_id, created_date, modified_date, status, adminID) 
                            OUTPUT INSERTED.ID 
                            VALUES ('" + email + "', '" + password + "', '" + branch_id + "', '" + role_id + "', '" +
                          DateTime.Now + "', '" +
                          DateTime.Now + "', '" + Model.Status.ACTIVE + "' , '" + _userID + "')";
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["conn"]);
            sqlConnection.Open();
            SqlCommand cmd = new SqlCommand(sql, sqlConnection);
            object id = cmd.ExecuteScalar();

            return id;

        }

        private DataTable SaveExternal(HttpContext ctx)
        {
            DataTable dt = new DataTable();
            DataColumn Name = new DataColumn("Result", typeof (string));
            dt.Columns.Add(Name);
            DataRow dr = dt.NewRow();
            ExternalSolicitorModel external = Deserialize<ExternalSolicitorModel>(ctx);
            string _password = Util.PasswordGen.generate(external.PASSWORD);
            int branch_id = Convert.ToInt32(ConfigurationManager.AppSettings["Branch"].ToString());
            short role_id = 11;
            //string status = "1";
            try
            {
                object id = SaveUsers(external.EMAIL, _password, role_id, branch_id); // store user
                int _id = Convert.ToInt32(id);
                bool resp = SaveProfile(_id, external.FIRM, "", "es", external.TEL); // store profile

                string query =
                    @"insert into external_solicitors (id, FIRM, ADDRESS, TEL, EMAIL, WEBSITE, FAX,PHONE,PASSWORD, role_id) values ('" +
                    id + "', '" + external.FIRM + "', '" + external.ADDRESS + "', '" + external.TEL + "', '" +
                    external.EMAIL + "', '" + external.WEBSITE + "', '" + external.FAX + "', '" + external.PHONE +
                    "', '" +
                    _password + "', '" + role_id + "')";
                using (var conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                using (var cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    int i = cmd.ExecuteNonQuery();
                    if (i == 1)
                    {
                        cookie = HttpContext.Current.Request.Cookies["ID"];

                        if (cookie == null)
                        {
                            HttpContext.Current.Response.Redirect("~/logout.aspx", true);
                        }
                        if (_userID == null)
                        {
                            _userID = cookie.Value;
                            Logger.writelog(_userID.ToString() + "coo");
                        }
                        DateTime tds = DateTime.Now;
                        string _username = UsersClass.GetName(_userID.ToString());
                        bool res = audit.SaveAudit(_username, "External Solicitor Created : " + "<b>[" + external.FIRM + "]</b>", "External Solicitor", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
                        dr["Result"] = "Save";
                      
                    }
                    else
                    {
                        dr["Result"] = "Not Save";
                    }

                    dt.Rows.Add(dr);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {


            }
            return dr.Table;
        }

        private DataTable GetExternal()
        {
            SqlConnection conn = null;
            SqlDataAdapter rd = null;
            DataSet ds = new DataSet();
            
            string query = @"select *  from external_solicitors";
            using (conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (var cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);

                rd.Fill(ds);
            }
            conn.Close();
            return ds.Tables[0];
        }


        private DataTable UpdateExternal(HttpContext ctx)
        {
            SqlDataAdapter rd = null;
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataColumn Name = new DataColumn("Name", typeof(string));
            dt.Columns.Add(Name);
            DataRow dr = dt.NewRow();

            ExternalSolicitorModel objUser = Deserialize<ExternalSolicitorModel>(ctx);

            
            string query = @"update external_solicitors  Set FIRM ='" + objUser.FIRM +
                           "', EMAIL = '" + objUser.EMAIL + "', ADDRESS = '" + objUser.ADDRESS + "', TEL ='" +
                           objUser.TEL + "' where id ='" + objUser.id + "'";
            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (var cmd = new SqlCommand(query, conn))
            {
                conn.Open();

                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                   
                   
                    dr["Name"] = "success";
                }
                else
                {
                    dr["Name"] = "Failed";
                }
                dt.Rows.Add(dr);

            }
            return dr.Table;
        }


        private DataTable DeleteExternal(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataColumn Name = new DataColumn("result", typeof(string));
            dt.Columns.Add(Name);
            DataRow dr = dt.NewRow();
            ExternalSolicitorModel objUser = Deserialize<ExternalSolicitorModel>(ctx);
            string query = @"delete from external_solicitors Where id=" + objUser.id;
            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (var cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                    dr["result"] = "Deleted";
                }
                else
                {
                    dr["result"] = "Not Deleted";
                }
                dt.Rows.Add(dr);
            }
            return dr.Table;
        }

        private DataTable ResetExternal(HttpContext ctx)
        {
            
            SqlDataAdapter rd = null;
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataColumn Name = new DataColumn("Name", typeof(string));
            dt.Columns.Add(Name);
            DataRow dr = dt.NewRow();

            ExternalSolicitorModel objUser = Deserialize<ExternalSolicitorModel>(ctx);
            UnDecrpytPassword = objUser.PASSWORD.Trim();
            ext_id = objUser.id;
            string _password = Util.PasswordGen.generate(objUser.PASSWORD);
            string query = @"update external_solicitors  Set PASSWORD ='" + _password +
                           "' where id ='" + objUser.id + "'";
            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (var cmd = new SqlCommand(query, conn))
            {
                conn.Open();

                int i = cmd.ExecuteNonQuery();
                if (i == 1)
                {
                     //get External Solicitor Email Address
                    string getEmail = "select EMAIL, FIRM from external_solicitors WHERE  id= " + ext_id;
                    using(SqlConnection con =  new  SqlConnection(ConfigurationManager.AppSettings["conn"]))
                    using (SqlCommand cmdd = new SqlCommand(getEmail, con))
                    {
                        con.Open();
                        SqlDataReader reader = cmdd.ExecuteReader(CommandBehavior.CloseConnection);
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                external_solicitor_email = reader.GetString(0); //get email
                                external_solicitor_name = reader.GetString(1);

                            }
                        }
                      


                        //Mail External Solicitor about the New Password Created 
                        MailMessage ms = new MailMessage(DEFAULT_MAIL_BOX, external_solicitor_email);
                        ms.Subject = "Password Reset !!!";

                        string body = "Hello " + external_solicitor_name + ",";
                        body += "<br /><br />Please your password has been reset<br /><br />";
                        body += "Password :  <b>" + UnDecrpytPassword + " </b>";
                        body += "<br /><br />Thanks";
                        ms.Body = body;
                        ms.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.gmail.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential("CASE MANAGER", "Password");
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(ms);

                    }
                    
                    dr["Name"] = "Reset";
                }
                else
                {
                    dr["Name"] = "Failed";
                }
                dt.Rows.Add(dr);

            }
             return dr.Table;
        }

        private bool SaveProfile(int user_id, string fn, string ln, string mn, string tl)
        {
            int stat = 1;
            string inti = String.Empty;
            string sql =
                @"INSERT INTO [profile] (user_id,  firstname, lastname,middlename, telephone,created_date,modified_date, status,initial) VALUES ('" +
                user_id + "', '" + fn + "', '" + ln + "', '" + mn + "', '" + tl + "', '" + DateTime.Now + "', '" +
                DateTime.Now + "', '" + stat + "', '" + inti + "')";
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                int o = cmd.ExecuteNonQuery();
                if (o == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                conn.Close();
            }
        }
        
        
        public T Deserialize<T>(HttpContext context)
        {
            string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();
            var obj = (T)new JavaScriptSerializer().Deserialize<T>(jsonData);
            return obj;
        }


        public class ExternalSolicitorModel
        {
            public int id { get; set; }
            public string FIRM { get; set; }
            public string EMAIL { get; set; }
            public string ADDRESS { get; set; }
            public string TEL { get; set; }
            public string WEBSITE { get; set; }
            public string FAX { get; set; }
            public string PHONE { get; set; }
            public string PASSWORD { get; set; }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}