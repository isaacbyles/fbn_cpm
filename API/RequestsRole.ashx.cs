﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using CaseManager.LawPavilion.Util;
using Newtonsoft.Json;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for RequestsRole
    /// </summary>
    public class RequestsRole : IHttpHandler
    {
        private LPCMConnection objConnection;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            objConnection = new LPCMConnection();
            context.Response.ContentType = "application/json";
            string user_id = context.Request.QueryString.Get("user_id");
            string role_id = context.Request.QueryString.Get("role_id");
            string branch_id = context.Request.QueryString.Get("branch_id");
            string json = JsonConvert.SerializeObject(GetRequest(user_id, role_id, branch_id), Formatting.Indented);
            context.Response.Write(json);

        }
      
        public dynamic GetRequest(string user_id, string role_id, string branch_id)
        {
            //DataSet ds = new DataSet();
            //SqlDataAdapter rd = null;
            //RequestTrail.RequestSmall objUser = Deserialize<RequestTrail.RequestSmall>(ctx);
            //string report_unit = getProcessUnit(objUser.role_id);
            //string region = getUserRegion(objUser.branch_id);
            string query = "";
            if (role_id == "15" || role_id == "24" || role_id == "25")
            {
                string _unit = GetUserUnit(user_id);
               query = @"

		    SELECT  [request].*,	
		    [process].name AS process_name,
            [process].description AS process_description,
            [process].reassign_role_id AS process_reassign_role_id,
            [process].created_date AS process_created_date,
            [process].modified_date AS process_modified_date,
            [process].status AS process_status,
		    [profile].firstname as init_user_firstname,
		    [profile].lastname as init_user_lastname,
		    [profile].middlename as init_user_middlename, 
		    [stage].role_id AS stage_role_id,
            [stage].type AS stage_type,
            [stage].name AS stage_name,
            [stage].description AS stage_description,
            [stage].created_date AS stage_created_date,
            [stage].modified_date AS stage_modified_date,
            [stage].status AS stage_status,
		    [stage_form].form_id AS stage_form_id,
		    [form].name AS form_name,
		    [form].description AS form_description

		    FROM [lpcm].[dbo].[request]
		    inner join [process] on [process].id = request.process_id 
		    inner join [user] on [user].id = request.current_user_id				
		    inner join [stage] on request.stage_id = [stage].id					
		    left join  [stage_form] on [stage_form].stage_id = stage.id			
		    left join  [form] on [form].id = [stage_form].form_id
		    inner join [role] on [role].id = [user].role_id
		    inner join [branch] on branch.id = [user].branch_id
		    inner join [profile] on [profile].user_id = [user].id
		    inner join [profile] p on p.user_id = [request].current_user_id
            inner join [report] on [report].process_id = process.id
	        where  [request].status <> 6  and [request].[user_role] ='" + role_id+ "' and report_unit= '"+_unit+"'   order by modified_date desc";
                //and current_user_id ='" + user_id + "'
            }
            else if (role_id == "29")
            {
                query = @"SELECT  [request].*,	
		    [process].name AS process_name,
            [process].description AS process_description,
            [process].reassign_role_id AS process_reassign_role_id,
            [process].created_date AS process_created_date,
            [process].modified_date AS process_modified_date,
            [process].status AS process_status,
		    [profile].firstname as init_user_firstname,
		    [profile].lastname as init_user_lastname,
		    [profile].middlename as init_user_middlename, 
		    [stage].role_id AS stage_role_id,
            [stage].type AS stage_type,
            [stage].name AS stage_name,
            [stage].description AS stage_description,
            [stage].created_date AS stage_created_date,
            [stage].modified_date AS stage_modified_date,
            [stage].status AS stage_status,
		    [stage_form].form_id AS stage_form_id,
		    [form].name AS form_name,
		    [form].description AS form_description

		    FROM [lpcm].[dbo].[request]
		    inner join [process] on [process].id = request.process_id 
		    inner join [user] on [user].id = request.current_user_id				
		    inner join [stage] on request.stage_id = [stage].id					
		    left join  [stage_form] on [stage_form].stage_id = stage.id			
		    left join  [form] on [form].id = [stage_form].form_id
		    inner join [role] on [role].id = [user].role_id
		    inner join [branch] on branch.id = [user].branch_id
		    inner join [profile] on [profile].user_id = [user].id
		    inner join [profile] p on p.user_id = [request].current_user_id
 
	        where [role].id= '" + role_id + "' and [request].status <> 6  and branch_id = '" + branch_id +
                        "' order by modified_date desc";
            }
            else
            {
                query = @"SELECT  [request].*,	
		    [process].name AS process_name,
            [process].description AS process_description,
            [process].reassign_role_id AS process_reassign_role_id,
            [process].created_date AS process_created_date,
            [process].modified_date AS process_modified_date,
            [process].status AS process_status,
		    [profile].firstname as init_user_firstname,
		    [profile].lastname as init_user_lastname,
		    [profile].middlename as init_user_middlename, 
		    [stage].role_id AS stage_role_id,
            [stage].type AS stage_type,
            [stage].name AS stage_name,
            [stage].description AS stage_description,
            [stage].created_date AS stage_created_date,
            [stage].modified_date AS stage_modified_date,
            [stage].status AS stage_status,
		    [stage_form].form_id AS stage_form_id,
		    [form].name AS form_name,
		    [form].description AS form_description

		    FROM [lpcm].[dbo].[request]
		    inner join [process] on [process].id = request.process_id 
		    inner join [user] on [user].id = request.current_user_id				
		    inner join [stage] on request.stage_id = [stage].id					
		    left join  [stage_form] on [stage_form].stage_id = stage.id			
		    left join  [form] on [form].id = [stage_form].form_id
		    inner join [role] on [role].id = [user].role_id
		    inner join [branch] on branch.id = [user].branch_id
		    inner join [profile] on [profile].user_id = [user].id
		    inner join [profile] p on p.user_id = [request].current_user_id
 
	        where [request].status <> 6 and [role].id= '" + role_id + "' and current_user_id ='" + user_id + "' order by modified_date desc";
            }
            objConnection.Sql = query;
            return objConnection.GetTable;
        }

        private string GetProcessUnit(string process_id)
        {
            DataTable dt = new DataTable();
            if (process_id == null)
            {
            }
            else
            {
                objConnection.Sql = "select report_unit from report where process_id=" + process_id;
                dt = objConnection.GetTable;

            }
            return dt.Rows[0]["report_unit"].ToString();
        }

        private string GetUserRegion(string branch_id)
        {
            DataTable dt = new DataTable();
            if (branch_id == null)
            {
            } 
            else
            {
                objConnection.Sql = "select region from [branch] where id=" + branch_id;
                dt = objConnection.GetTable;

            }
            return dt.Rows[0]["region"].ToString();
        }

        //get litigation, hub, corporate, branch and perfection
        public string GetUserUnit(string user_id)
        {
            DataTable dt = new DataTable();
            if (user_id == null)
            {
            }
            else
            {
                objConnection.Sql = "select middlename from [profile] where user_id =" + user_id;
                dt = objConnection.GetTable;

            }
            return dt.Rows[0]["middlename"].ToString();
        }
        
        
        public T Deserialize<T>(HttpContext context)
        {
            string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();
            var obj = (T)new JavaScriptSerializer().Deserialize<T>(jsonData);
            return obj;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}