﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CaseManager.LawPavilion.Util;
using CaseManager.Util;
using Newtonsoft.Json;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for GetUsersRole
    /// </summary>
    public class GetUsersRole : IHttpHandler
    {
        private LPCMConnection objConnection;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            objConnection = new LPCMConnection();
            context.Response.ContentType = "application/json";
            string process_id = context.Request.QueryString.Get("process_id");
            string role_id = context.Request.QueryString.Get("role_id");
            string branch_id = context.Request.QueryString.Get("branch_id");
            DataSet data = getUsers(process_id, role_id, branch_id);
            string dd=  Response.json(1, Model.Base.toAssociative(data), "");
            context.Response.Write(dd);
        }

        private DataSet getUsers(string process_id, string role_id, string branch_id)
        {   DataSet ds = new DataSet();
                string process_unit = getProcessUnit(process_id);
                string user_region = getUserRegion(branch_id);
                string query = null;
            if (process_id == "24")
            {
                if (role_id == "15")
                {
                    query = @" SELECT DISTINCT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  ,[role_id]
                                 ,[branch].region
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
	                           ,role.name as role_name
                                ,role.id as role_id
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = [user].branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
							  inner join [report] on [report].report_unit = profile.middlename
							  where role_id = '" + role_id + "'  and [user].[status] = 1 and  profile.middlename='" + process_unit +
                            "' or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '" +
                            role_id + "')) > 0";
                }
                else
                {
                    query = @" SELECT DISTINCT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  ,[role_id]
                                 ,[branch].region
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
	                           ,role.name as role_name
                                ,role.id as role_id
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = [user].branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
							  inner join [report] on [report].report_unit = profile.middlename
							  where role_id = '" + role_id + "'  and [user].[status] = 1 or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '" +
                            role_id + "')) > 0";
                }
            }
            else
            {
                if (role_id == "15" && user_region == "Lagos".ToUpper())
                {

                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '" + role_id + "'  and region ='" + user_region +
                            "' and [user].status=1 and profile.middlename='" + process_unit +
                            "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='Lagos' and [profile].middlename = '" +
                            process_unit + "')";


                }
                else if (role_id == "15" && user_region != "Lagos".ToUpper())
                {
                    query = @" SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id
     
                              FROM [lpcm].[dbo].[user]    
                              inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                              where                                  
                            (role_id = '" + role_id + "'  and region ='" + user_region +
                            "' and [user].status=1 and profile.middlename= 'hub') UNION select [user].[id]  ,[email] ,[password] ,[branch_id],[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name,role.id as role_id from user_role inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id  where ([user_role].role_id ='" +
                            role_id + "' and [user].status=1 and region ='" + user_region +
                            "' and profile.middlename= 'hub') ";
                }
                else if (role_id == "25" && user_region == "Lagos".ToUpper())
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '" + role_id + "'  and region ='" + user_region +
                            "' and [user].status=1 and profile.middlename='" + process_unit +
                            "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='Lagos' and [profile].middlename = '" +
                            process_unit + "')";

                }
                else if (role_id == "25" && user_region != "Lagos".ToUpper())
                {
                    query = @" SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id
     
                              FROM [lpcm].[dbo].[user]    
                              inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                              where                                  
                            (role_id = '" + role_id + "'  and region ='" + user_region +
                            "' and [user].status=1 and profile.middlename= 'hub') UNION select [user].[id]  ,[email] ,[password] ,[branch_id],[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name,role.id as role_id from user_role inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id  where ([user_role].role_id ='" +
                            role_id + "' and [user].status=1 and region ='" + user_region +
                            "' and profile.middlename= 'hub') ";
                }
                else if (role_id == "24" && user_region == "Lagos".ToUpper())
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '" + role_id + "'  and region ='" + user_region +
                            "' and [user].status=1 and profile.middlename='" + process_unit +
                            "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='Lagos' and [profile].middlename = '" +
                            process_unit + "')";

                }
                else if (role_id == "24" && user_region != "Lagos".ToUpper())
                {
                    query = @" SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id
     
                              FROM [lpcm].[dbo].[user]    
                              inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                              where                                  
                            (role_id = '" + role_id + "'  and region ='" + user_region +
                            "' and [user].status=1 and profile.middlename= 'hub') UNION select [user].[id]  ,[email] ,[password] ,[branch_id],[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name,role.id as role_id from user_role inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id  where ([user_role].role_id ='" +
                            role_id + "' and [user].status=1 and region ='" + user_region +
                            "' and profile.middlename= 'hub') ";
                }

                else if (role_id == "9")
                {
                    query = @" SELECT DISTINCT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  ,[role_id]
                                 ,[branch].region
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
	                           ,role.name as role_name
                                ,role.id as role_id
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = [user].branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
							  where role_id = '" + role_id +
                            "' and [user].[status] = 1   or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '" +
                            role_id + "')) > 0";
                }
                else if (role_id == "11")
                {
                    query = @"SELECT DISTINCT  [user].[id]
                                        ,[user].[email]
                                        ,[user].[password]
                                        ,[branch_id]
                                        ,[user].[role_id] 
                                        ,[created_date]
                                        ,[modified_date]
                                        ,[status]
	                                    ,FIRM,
	                          CASE [user].role_id
                            WHEN 11 THEN 'External Solicitor'
                          
                            END  as [role_name] 
                                        FROM [lpcm].[dbo].[user]
                                        inner join [external_solicitors] on [external_solicitors].id = [user].id
                                        where [external_solicitors].role_id = '" +
                            ConfigurationManager.AppSettings["EXT"] + "'";
                }
                else

                {
                    query = @" SELECT DISTINCT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  ,[role_id]
                                 ,[branch].region
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
	                           ,role.id as role_id,
                                role.name as role_name
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = [user].branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
							 	 where role_id = '" + role_id +
                            "' and [user].[status] = 1  or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '" +
                            role_id + "')) > 0";
                }
            }
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds);
            }
            return ds;

        }

        private string getProcessUnit(string process_id)
        {
            objConnection.Sql = "select report_unit from report where process_id=" + process_id;
            DataTable dt = objConnection.GetTable;
            return dt.Rows[0]["report_unit"].ToString();
        }
        
        private string getUserRegion(string branch_id)
        {
            objConnection.Sql = "select region from [branch] where id=" + branch_id;
            DataTable dt = objConnection.GetTable;
            return dt.Rows[0]["region"].ToString();
        }


      
        public bool IsReusable
        {
            get { return false; }
        }
    }
}