﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using CaseManager.Util;
using CaseManager.Logic;
using System.Data;
using System.Web.SessionState;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for Process
    /// </summary>
    public class Process : IHttpHandler
    {
        private ProcessLogic logic = new ProcessLogic();
        private HttpContext context;
        static AuditLogic audit = new AuditLogic();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            Model.Base.has_error = false;
            this.context = context;
            context.Response.ContentType = Constant.JSON_CONTENT_TYPE;
            string action = context.Request.QueryString["a"];
            string response = "";
            switch (action)
            {
                case "add":
                    response = this.add();
                    break;
                case "edit":
                    response = this.edit();
                    break;
                case "change_status":
                    response = this.changeStatus();
                    break;
                case "get":
                    response = this.get();
                    break;
                case "add_stage":
                    response = this.addStage();
                    break;
                case "edit_stage_detail":
                    response = this.editStageDetails();
                    break;
                case "add_stage_form":
                    response = this.addStageForm();
                    break;
                case "remove_stage_form":
                    response = this.removeStageForm();
                    break;
                case "get_stage":
                    response = this.getStages();
                    break;
                case "remove_stage":
                    response = this.removeStage();
                    break;
                case "set_workflow":
                    response = this.setWorkFlow();
                    break;
                case "get_workflow":
                    response = this.getWorkFlow();
                    break;
                default:
                    break;
            }

            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #region Logic API
        public string add()
        {
            string name = this.context.Request.Form.Get("name");
            string description = this.context.Request.Form.Get("description");
            string reassign_role_id = this.context.Request.Form.Get("reassign_role_id");

            List<string> required_fields = new List<string>();
            required_fields.Add(name);
            required_fields.Add(description);
            required_fields.Add(reassign_role_id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            int id = this.logic.add(
                name,
                description,
                short.Parse(reassign_role_id)
                );


            //string _id = this.context.Session["User_id"].ToString();
            //DateTime tds = DateTime.Now;
            //bool res = audit.SaveAudit(_id, "Process Created : " + "<b>[" + name + "] </b>", "Process", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
            return Response.json(this.logic.Status, id, this.logic.Message);
        }

        public string edit()
        {
            string id = this.context.Request.Form.Get("id");
            string name = this.context.Request.Form.Get("name");
            string description = this.context.Request.Form.Get("description");
            string reassign_role_id = this.context.Request.Form.Get("reassign_role_id");

            List<string> required_fields = new List<string>();
            required_fields.Add(id);
            required_fields.Add(name);
            required_fields.Add(description);
            required_fields.Add(reassign_role_id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.edit(
                int.Parse(id),
                name,
                description,
                short.Parse(reassign_role_id)                
                );
            return Response.json(this.logic.Status, null, this.logic.Message);
        }


        public string changeStatus()
        {
            string id = this.context.Request.Form.Get("id");
            string status = this.context.Request.Form.Get("status");

            List<string> required_fields = new List<string>();
            required_fields.Add(id);
            required_fields.Add(status);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.changeStatus(int.Parse(id), short.Parse(status));
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        public string get()
        {

            string id = this.context.Request.QueryString.Get("id");
            string reassign_role_id = this.context.Request.QueryString.Get("reassign_role_id");
            string status = this.context.Request.QueryString.Get("status");
            string offset = this.context.Request.QueryString.Get("offset");
            string count = this.context.Request.QueryString.Get("count");

            Dictionary<string, object> filter = new Dictionary<string, object>();
            if (reassign_role_id != null) filter["reassign_role_id"] = short.Parse(reassign_role_id);
            if (id != null) filter["id"] = int.Parse(id);
            if (status != null) filter["status"] = short.Parse(status);

            int? _offset;
            int? _count;
            if (offset == null || count == null)
            {
                _offset = null;
                _count = null;
            }
            else
            {
                _offset = int.Parse(offset);
                _count = int.Parse(count);
            }

            DataTable data = this.logic.get(filter, _offset, _count);
            return Response.json(this.logic.Status, Model.Base.toAssociative(data), this.logic.Message);
        }

        public string addStage()
        {
            string process_id = this.context.Request.Form.Get("process_id");
            string role_id = this.context.Request.Form.Get("role_id");
            string name = this.context.Request.Form.Get("name");
            string description = this.context.Request.Form.Get("description");
            string form_id = this.context.Request.Form.Get("form_id");
            string type = this.context.Request.Form.Get("type"); // Normal -> 1 and Start -> 2

            List<string> required_fields = new List<string>();
            required_fields.Add(process_id);
            required_fields.Add(role_id);
            required_fields.Add(name);
            required_fields.Add(type);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            int? _form_id;
            if (form_id == null) _form_id = null;
            else _form_id = int.Parse(form_id);

            int id = this.logic.addStage(
                int.Parse(process_id),
                short.Parse(role_id),
                name,
                description,
                _form_id,
                short.Parse(type)
                );

            return Response.json(this.logic.Status, id, this.logic.Message);
        }

        public string editStageDetails()
        {
            string stage_id = this.context.Request.Form.Get("stage_id");
            string name = this.context.Request.Form.Get("name");
            string description = this.context.Request.Form.Get("description");

            List<string> required_fields = new List<string>();
            required_fields.Add(stage_id);
            required_fields.Add(name);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.editStageDetails(
                int.Parse(stage_id),
                name,
                description
                );
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        public string addStageForm()
        {
            string stage_id = this.context.Request.Form.Get("stage_id");
            string form_id = this.context.Request.Form.Get("form_id");

            List<string> required_fields = new List<string>();
            required_fields.Add(stage_id);
            required_fields.Add(form_id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.addStageForm(
                int.Parse(stage_id),
                int.Parse(form_id)
                );
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        public string removeStageForm()
        {
            string stage_id = this.context.Request.Form.Get("stage_id");

            List<string> required_fields = new List<string>();
            required_fields.Add(stage_id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.removeStageForm(
                int.Parse(stage_id)
                );
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        public string getStages()
        {
            string id = this.context.Request.QueryString.Get("id");
            string process_id = this.context.Request.QueryString.Get("process_id");
            string role_id = this.context.Request.QueryString.Get("role_id");
            string type = this.context.Request.QueryString.Get("type");
            string status = this.context.Request.QueryString.Get("status");
            string offset = this.context.Request.QueryString.Get("offset");
            string count = this.context.Request.QueryString.Get("count");
            string with_process = this.context.Request.QueryString.Get("with_process");

            Dictionary<string, object> filter = new Dictionary<string, object>();
            if (process_id != null) filter["process_id"] = int.Parse(process_id);
            if (role_id != null) filter["role_id"] = short.Parse(role_id);
            if (type != null) filter["type"] = short.Parse(type);
            if (id != null) filter["id"] = int.Parse(id);
            if (status != null) filter["status"] = short.Parse(status);

            List<string> fetch_with = new List<string>();
            if (with_process != null) fetch_with.Add("process");

            int? _offset;
            int? _count;
            if (offset == null || count == null)
            {
                _offset = null;
                _count = null;
            }
            else
            {
                _offset = int.Parse(offset);
                _count = int.Parse(count);
            }

            DataTable data = this.logic.getStages(filter, fetch_with, _offset, _count);
            return Response.json(this.logic.Status, Model.Base.toAssociative(data), this.logic.Message);
        }

        public string removeStage()
        {
            string stage_id = this.context.Request.Form.Get("stage_id");

            List<string> required_fields = new List<string>();
            required_fields.Add(stage_id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.removeStage(
                int.Parse(stage_id)
                );
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        public string setWorkFlow()
        {
            string process_id = this.context.Request.Form.Get("process_id");
            string stage_precedence = this.context.Request.Form.Get("stage_precedence");

            List<string> required_fields = new List<string>();
            required_fields.Add(process_id);
            required_fields.Add(stage_precedence);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            IEnumerable<Dictionary<string, string>> _stage_precedence = JsonConvert.DeserializeObject<IEnumerable<Dictionary<string, string>>>(stage_precedence);

            this.logic.setWorkFlow(
                int.Parse(process_id),
                _stage_precedence
                );
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        public string getWorkFlow()
        {
            string with_next_stage = this.context.Request.QueryString.Get("with_next_stage");
            string with_current_stage = this.context.Request.QueryString.Get("with_current_stage");
            string process_id = this.context.Request.QueryString.Get("process_id");
            string current_id = this.context.Request.QueryString.Get("current_id");
            string next_id = this.context.Request.QueryString.Get("next_id");

            Dictionary<string, object> filter = new Dictionary<string, object>();
            if (process_id != null) filter["process_id"] = int.Parse(process_id);
            if (current_id != null) filter["current_id"] = int.Parse(current_id);
            if (next_id != null) filter["current_id"] = int.Parse(next_id);

            List<string> fetch_with = new List<string>();
            if (with_next_stage != null) fetch_with.Add("next_stage");
            if (with_current_stage != null) fetch_with.Add("current_stage");

            DataTable data = this.logic.getWorkFlow(filter, fetch_with);
            return Response.json(this.logic.Status, Model.Base.toAssociative(data), this.logic.Message);
        }
        #endregion
    }
}