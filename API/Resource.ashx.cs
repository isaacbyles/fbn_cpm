﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using CaseManager.Util;
using CaseManager.Logic;
using System.Data;
using System.Data.SqlClient;
using System.Web.SessionState;
using CaseManager.LawPavilion.Util;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for Resource
    /// </summary>
    public class Resource : IHttpHandler, IRequiresSessionState
    {
        private ResourceLogic logic = new ResourceLogic();
        private HttpContext context;
        private LPCMConnection objConnection;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            objConnection = new LPCMConnection();
            Model.Base.has_error = false;
            this.context = context;
            context.Response.ContentType = Constant.JSON_CONTENT_TYPE;
            string action = context.Request.QueryString["a"];
            string response = "";
            switch (action)
            {
                case "add":
                    response = this.add();
                    break;
                case "edit":
                    response = this.editDetails();
                    break;
                case "change_status":
                    response = this.changeStatus();
                    break;
                case "get":
                    response = this.get();
                    break;
                default:
                    break;
            }

            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #region Logic API
        public string add()
        {
            string request_id = this.context.Request.Form.Get("request_id");
            string label = this.context.Request.Form.Get("label");
            string description = this.context.Request.Form.Get("description");
            string mime_type = this.context.Request.Form.Get("mime_type");
            string route = this.context.Request.Form.Get("route");

            List<string> required_fields = new List<string>();
            required_fields.Add(request_id);
            required_fields.Add(label);
            required_fields.Add(description);
            required_fields.Add(mime_type);
            required_fields.Add(route);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            long id = this.logic.add(
                long.Parse(request_id),
                label,
                description,
                mime_type,
                short.Parse(route)
                );

            return Response.json(this.logic.Status, id, this.logic.Message);
        }

        public string editDetails()
        {
            string resource_id = this.context.Request.Form.Get("resource_id");
            string label = this.context.Request.Form.Get("label");
            string description = this.context.Request.Form.Get("description");

            List<string> required_fields = new List<string>();
            required_fields.Add(resource_id);
            required_fields.Add(label);
            required_fields.Add(description);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.editDetails(long.Parse(resource_id), label, description);
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        public string changeStatus()
        {
            string resource_id = this.context.Request.Form.Get("resource_id");
            string status = this.context.Request.Form.Get("status");

            List<string> required_fields = new List<string>();
            required_fields.Add(resource_id);
            required_fields.Add(status);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.changeStatus(long.Parse(resource_id), short.Parse(status));
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        public string get()
        {
            #region


            string id = this.context.Request.QueryString.Get("id");
            string request_id = this.context.Request.QueryString.Get("request_id");
            string stage_id = this.context.Request.QueryString.Get("stage_id");
            string user_id = this.context.Request.QueryString.Get("user_id");
            string label = this.context.Request.QueryString.Get("label");
            string status = this.context.Request.QueryString.Get("status");
            string offset = this.context.Request.QueryString.Get("offset");
            string count = this.context.Request.QueryString.Get("count");

            string with_user = this.context.Request.QueryString.Get("with_user");
            string with_stage = this.context.Request.QueryString.Get("with_stage");

            Dictionary<string, object> filter = new Dictionary<string, object>();
            if (id != null) filter["id"] = long.Parse(id);
            if (request_id != null) filter["request_id"] = long.Parse(request_id);
            if (stage_id != null) filter["stage_id"] = int.Parse(stage_id);
            if (user_id != null) filter["user_id"] = int.Parse(user_id);
            if (status != null) filter["status"] = short.Parse(status);
            if (label != null) filter["label"] = int.Parse(label);

            List<string> fetch_with = new List<string>();
            if (with_user != null) fetch_with.Add("user");
            if (with_stage != null) fetch_with.Add("stage");

            int? _offset;
            int? _count;
            if (offset == null || count == null)
            {
                _offset = null;
                _count = null;
            }
            else
            {
                _offset = int.Parse(offset);
                _count = int.Parse(count);
            }
            DataTable data = this.logic.get(filter, fetch_with, _offset, _count);
            return Response.json(this.logic.Status, (data == null) ? null : data, this.logic.Message);
        }


        #endregion
//            DataSet ds = new DataSet();
//            string request_id = this.context.Request.QueryString.Get("request_id");
//            string user_id = this.context.Request.QueryString.Get("user_id");
//            string role_id = this.context.Request.QueryString.Get("role_id");
//            string _branch = getUserBranch(user_id);
//            string query = null;
//            if (role_id == "29")
//            {
//                query = @"SELECT [resource].*, 
//                                [profile].firstname AS user_firstname, 
//                                [profile].lastname AS user_lastname, 
//                                [profile].middlename AS user_middlename, 
//                                [stage].name AS stage_name FROM [resource] 
//                                 INNER JOIN [profile] ON [profile].user_id = [resource].user_id   
//                                 INNER JOIN stage ON stage.id = resource.stage_id  
//                                 where request_id = '" + request_id + "' and [resource].role_id='" + role_id +
//                        "'";
//            }
//            else if (role_id == "11")
//            {
//                query = @"SELECT [resource].*, 
//                                [profile].firstname AS user_firstname, 
//                                [profile].lastname AS user_lastname, 
//                                [profile].middlename AS user_middlename, 
//                                [stage].name AS stage_name FROM [resource] 
//                                 INNER JOIN [profile] ON [profile].user_id = [resource].user_id   
//                                 INNER JOIN stage ON stage.id = resource.stage_id  
//                                 where request_id = '" + request_id + "' and [resource].role_id='" + role_id + "'";
//            }
//            else
//            {
//                query = @"SELECT [resource].*, 
//                                [profile].firstname AS user_firstname, 
//                                [profile].lastname AS user_lastname, 
//                                [profile].middlename AS user_middlename, 
//                                [stage].name AS stage_name FROM [resource] 
//                                 INNER JOIN [profile] ON [profile].user_id = [resource].user_id   
//                                 INNER JOIN stage ON stage.id = resource.stage_id  
//                                 where request_id = '" + request_id + "'";
//            }
//            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
//            using (SqlCommand cmd = new SqlCommand(query,  conn))
//            {
//                conn.Open();
//                SqlDataAdapter rd = new SqlDataAdapter(cmd);
//                rd.Fill(ds);
//            }

//            DataTable data = ds.Tables[0];
//            return Response.json(this.logic.Status, data, this.logic.Message);
//        }
        #endregion

        private string getUserBranch(string userID)
        {
            objConnection.Sql = @"SELECT [user].id,
		                            [email]
                                  ,[password]
                                  ,[branch_id]
                                  ,[role_id]
                                  ,[created_date]
                                  ,[modified_date]
                                  ,[status]
	                              ,branch.name as branch_name
                                    ,branch.id as branch_id
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = [user].branch_id
                              where [user].id = '" + userID +
                                "'";
            DataTable dt = objConnection.GetTable;
            return dt.Rows[0]["branch_id"].ToString();
        }
    }
}