﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using CaseManager.Util;
using CaseManager.Logic;
using System.Data;
using System.Web.SessionState;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for Branch
    /// </summary>
    public class Branch : IHttpHandler, IRequiresSessionState
    {

        private BranchLogic logic = new BranchLogic();
        private HttpContext context;
        private HttpCookie cookie;
        private Object _userID;


        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            Model.Base.has_error = false;
            this.context = context;
            context.Response.ContentType = Constant.JSON_CONTENT_TYPE;
            string action = context.Request.QueryString["a"];
            string response = "";
            switch (action)
            {
                case "add":
                    response = this.add();
                    break;
                case "edit":
                    response = this.edit();
                    break;
                case "change_status":
                    response = this.changeActivefg();
                    break;
                case "get_all":
                    response = this.getAll();
                    break;
                default:
                    break;
            }

            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #region Logic API
        private string add()
        {
            string name = this.context.Request.Form.Get("name");
            string street = this.context.Request.Form.Get("street");
            string town = this.context.Request.Form.Get("town");
            string state = this.context.Request.Form.Get("state");
            string region = this.context.Request.Form.Get("region");
            string country = this.context.Request.Form.Get("country");
            string type = this.context.Request.Form.Get("type");

            List<string> required_fields = new List<string>();
            required_fields.Add(name);
            required_fields.Add(street);
            required_fields.Add(town);
            required_fields.Add(state);
            required_fields.Add(region);
            required_fields.Add(country);
            required_fields.Add(type);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            object id = this.logic.add(name, street, town, state, region, country,type);
            return Response.json(this.logic.Status, id, this.logic.Message);
        }

        private string edit()
        {
            string id = this.context.Request.Form.Get("id");
            string name = this.context.Request.Form.Get("name");
            string street = this.context.Request.Form.Get("street");
            string town = this.context.Request.Form.Get("town");
            string state = this.context.Request.Form.Get("state");
            string region = this.context.Request.Form.Get("region");
            string country = this.context.Request.Form.Get("country");

            List<string> required_fields = new List<string>();
            required_fields.Add(id);
            required_fields.Add(name);
            required_fields.Add(street);
            required_fields.Add(town);
            required_fields.Add(state);
            required_fields.Add(region);
            required_fields.Add(country);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.edit(int.Parse(id), name, street, town, state, region, country);
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string changeActivefg()
        {
            string id = this.context.Request.Form.Get("id");
            string active_fg = this.context.Request.Form.Get("active_fg"); // 0 or 1

            List<string> required_fields = new List<string>();
            required_fields.Add(id);
            required_fields.Add(active_fg);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.changeActivefg(int.Parse(id), (active_fg.Trim() == "1"));
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string getAll()
        {
            string active_fg = this.context.Request.QueryString.Get("active_fg"); // 0 or 1

            bool? _active_fg = null;
            if (active_fg != null)
            {
                _active_fg = (active_fg.Trim() == "1");
            }
            DataTable result = this.logic.getAll(_active_fg);

            return Response.json(this.logic.Status, Model.Base.toAssociative(result), this.logic.Message);
        }
        #endregion
    }
}