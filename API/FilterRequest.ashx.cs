﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CaseManager.LawPavilion.Util;
using Newtonsoft.Json;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for FilterRequest
    /// </summary>
    public class FilterRequest : IHttpHandler
    {
        private LPCMConnection objConnection;
        private HttpContext _context;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            this._context = context;
            objConnection = new LPCMConnection();
            context.Response.ContentType = "application/json";
            string user_id = context.Request.QueryString.Get("user_id");
            string role_id = context.Request.QueryString.Get("role_id");
            string branch_id = context.Request.QueryString.Get("branch_id");
            string searchText = context.Request.QueryString.Get("search");
            string json = JsonConvert.SerializeObject(FilterRequests(user_id, branch_id, role_id, searchText),
                Formatting.Indented);
            context.Response.Write(json);
        }

        public dynamic FilterRequests(string user_id, string branch_id, string role_id, string searchQuery)
        {
           
            string _unit = GetUserUnit(user_id);

            string sql = @"SELECT  [request].*,	
		    [process].name AS process_name,
            [process].description AS process_description,
            [process].reassign_role_id AS process_reassign_role_id,
            [process].created_date AS process_created_date,
            [process].modified_date AS process_modified_date,
            [process].status AS process_status,
		    [profile].firstname as init_user_firstname,
		    [profile].lastname as init_user_lastname,
		    [profile].middlename as init_user_middlename, 
		    [stage].role_id AS stage_role_id,
            [stage].type AS stage_type,
            [stage].name AS stage_name,
            [stage].description AS stage_description,
            [stage].created_date AS stage_created_date,
            [stage].modified_date AS stage_modified_date,
            [stage].status AS stage_status,
		    [stage_form].form_id AS stage_form_id,
		    [form].name AS form_name,
		    [form].description AS form_description

		    FROM [lpcm].[dbo].[request]
		    inner join [process] on [process].id = request.process_id 
		    inner join [user] on [user].id = request.current_user_id				
		    inner join [stage] on request.stage_id = [stage].id					
		    left join  [stage_form] on [stage_form].stage_id = stage.id			
		    left join  [form] on [form].id = [stage_form].form_id
		    inner join [role] on [role].id = [user].role_id
		    inner join [branch] on branch.id = [user].branch_id
		    inner join [profile] on [profile].user_id = [user].id
		    inner join [profile] p on p.user_id = [request].current_user_id
 
	        where ([role].id= '" + role_id + "' and branch_id = '" + branch_id + "' and [profile].middlename='" + _unit +
                         "') and title like '%" + searchQuery + "%'";

            objConnection.Sql = sql;
            return objConnection.GetTable;

           // return ds;
        }
        public string GetUserUnit(string user_id)
        {
            DataTable dt = new DataTable();
            if (user_id == null)
            {
            }
            else
            {
                objConnection.Sql = "select middlename from [profile] where user_id =" + user_id;
                dt = objConnection.GetTable;

            }
            return dt.Rows[0]["middlename"].ToString();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}