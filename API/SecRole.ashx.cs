﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Util;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for SecRole
    /// </summary>
    /// 

    public class Connection
    {
        public LPCMConnection objConnection = new LPCMConnection();

    }

    public class result
    {
        public int status { get; set; }
    }
    public class SecRoles
    {
        public object available { get; set; }
        public object assigned { get; set; }
        
    }

    public class SecRole : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";

            // objConnection = new LPCMConnection();


            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo, context.Request.Url.AbsoluteUri), Formatting.Indented);


            context.Response.Write(json);
        }


        public dynamic Handler(string pathinfo, string url)
        {
            string[] route = pathinfo.Split('/');
            

            switch (route.Length)
            {
                case 2:
                    return this.getSecRoles(route[1]);
                case 4:
                    if (route[2] == "remove")
                    {
                        return this.getRemoveSecRole(route[1],route[3]);
                    }
                    else
                    {
                        return this.getAddSecRole(route[1], route[3]);
                    }

                default:
                    return null;
            }

        }

        public dynamic getSecRoles(string userid)
        {
            try
            {
                DataSet ds = new DataSet("SecondaryRoles");

                //available roles
                string sqlQuery = @"select id,name from [role] where active_fg = 1 and id not in 
                                    (select role_id from [user] where id = "+userid+@") and id not in
                                    (select role_id from [user_role] where user_id = "+userid+") order by name";
                Connection con = new Connection();
                con.objConnection.Sql = sqlQuery;
                DataTable dtTable = con.objConnection.GetTable;
                dtTable.TableName = "available";
                ds.Tables.Add(dtTable);

                //assigned secondary roles
                sqlQuery = @"select role_id as id,name from [user_role] join [role] 
                            on [user_role].[role_id] = [role].[id] where user_id = " + userid+" order by name";
                con.objConnection.Sql = sqlQuery;
                dtTable = con.objConnection.GetTable;
                dtTable.TableName = "assigned";
                ds.Tables.Add(dtTable);

                //return result
                return ds;
                //SecRoles secRoles = new SecRoles();
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public dynamic getRemoveSecRole(string userid, string roleid)
        {
            result result = new result();
            try
            {

                string sqlQuery = @"delete from user_role OUTPUT DELETED.role_id,DELETED.user_id where user_id = "+userid+" and role_id = "+roleid;
                Connection con = new Connection();
                con.objConnection.Sql = sqlQuery;
                DataTable dtTable = con.objConnection.GetTable;
                result.status = 1;
                return result;
                //SecRoles secRoles = new SecRoles();
            }
            catch (Exception e)
            {

                result.status = 0;
                return result;
            }
        }



        public dynamic getAddSecRole(string userid, string roleid)
        {
            result result = new result();
            try
            {

                string sqlQuery = @"insert into user_role OUTPUT INSERTED.role_id,INSERTED.user_id values("+userid+","+roleid+")";
                Connection con = new Connection();
                con.objConnection.Sql = sqlQuery;
                DataTable dtTable = con.objConnection.GetTable;
                result.status = 1;
                return result;
                //SecRoles secRoles = new SecRoles();
            }
            catch (Exception e)
            {
                
                result.status = 0;
                return result;
            }
        }



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}