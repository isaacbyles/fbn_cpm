﻿using CaseManager.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for FormContentUpdate
    /// </summary>
    public class FormContentUpdate : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();
                int response = 0;
                FormContent objUsr = Deserialize<FormContent>(strJson);
                if (objUsr != null)
                {
                    string ID = objUsr.ID;
                    string value = objUsr.Value;
                    string form_control_id = objUsr.Form_Control_Id;
                    string type = GetFormControlType(form_control_id);
                    try
                    {
                        switch (type)
                        {
                            case "TEXT":
                                InsertUpdateTEXT(objUsr);

                                break;
                            case "INT":
                                InsertUpdateINT(objUsr);

                                break;
                            case "CHAR":
                                InsertUpdateCHAR(objUsr);

                                break;
                            case "DECIMAL":
                                InsertUpdateDECIMAL(objUsr);

                                break;
                            case "DATE":
                                InsertUpdateDATE(objUsr);

                                break;
                            default:
                                break;

                        }

                       
                    }

                    catch (Exception ex)
                    {
                        
                    }

                }
                else
                {
                    context.Response.Write("No Data");
                }
                string json = JsonConvert.SerializeObject("1", Formatting.Indented);
                context.Response.Write(json);
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("2", Formatting.Indented);
                context.Response.Write(json);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public T Deserialize<T>(string context)
        {
            string jsonData = context;
            var obj = (T)new JavaScriptSerializer().Deserialize<T>(jsonData);
            return obj;
        }


        //get the form control id to fetch form control type

        private string GetFormControlType(string id)
        {

            int _id = int.Parse(id);

            using (DAL.lpcmDataContext ctx = new DAL.lpcmDataContext(ConfigurationManager.AppSettings["conn"]))
            {
                var query = (from k in ctx.form_controls
                             where k.id == _id

                             select k).Single();
                if (query.type == 1)
                {
                    return "CHAR";
                }
                else if (query.type == 2)
                {

                    return "DATE";

                }
                else if (query.type == 3)
                {

                    return "DECIMAL";

                }
                else if (query.type == 4)
                {

                    return "INT";

                }
                else if (query.type == 5)
                {

                    return "TEXT";

                }
                else
                {
                    return "";
                }
                //  return query.type.ToString(); // return type (e.g TEXT, CHAR, INT, DECIMAL)
            }

        }

        //TEXT
        private void InsertUpdateTEXT(FormContent content)
        {

            using (DAL.lpcmDataContext ctx = new DAL.lpcmDataContext(ConfigurationManager.AppSettings["conn"]))
            {
                long _id = long.Parse(content.ID);
                var query = (from k in ctx.process_form_text_datas
                             where k.id == _id
                             select k).First();
                query.value = content.Value.ToString();
                ctx.SubmitChanges();
            }
        }

        //INT
        private void InsertUpdateINT(FormContent content)
        {
            using (DAL.lpcmDataContext ctx = new DAL.lpcmDataContext(ConfigurationManager.AppSettings["conn"]))
            {
                long _id = long.Parse(content.ID);
                var query = (from k in ctx.process_form_int_datas
                             where k.id == _id
                             select k).First();
                query.value = content.Value.ToString();
                ctx.SubmitChanges();
            }
        }

        //CHAR
        private void InsertUpdateCHAR(FormContent content)
        {
            using (DAL.lpcmDataContext ctx = new DAL.lpcmDataContext(ConfigurationManager.AppSettings["conn"]))
            {
                long _id = long.Parse(content.ID);
                var query = (from k in ctx.process_form_char_datas
                             where k.id == _id
                             select k).First();
                query.value = content.Value.ToString();
                ctx.SubmitChanges();
            }
        }

        //DATE
        private void InsertUpdateDATE(FormContent content)
        {
            using (DAL.lpcmDataContext ctx = new DAL.lpcmDataContext(ConfigurationManager.AppSettings["conn"]))
            {
                long _id = long.Parse(content.ID);
                var query = (from k in ctx.process_form_date_datas
                             where k.id == _id
                             select k).First();
                query.value = DateTime.Parse(content.Value);
                ctx.SubmitChanges();
            }
        }

        //DECIMAL 
        private void InsertUpdateDECIMAL(FormContent content)
        {
            using (DAL.lpcmDataContext ctx = new DAL.lpcmDataContext(ConfigurationManager.AppSettings["conn"]))
            {
                long _id = long.Parse(content.ID);
                var query = (from k in ctx.process_form_decimal_datas
                             where k.id == _id
                             select k).First();
                query.value = decimal.Parse(content.Value.ToString());
                ctx.SubmitChanges();
            }
        }
    }
}