﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for UserRegion
    /// </summary>
    public class UserRegion : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            string userId = context.Request.QueryString.Get("userid");
            string json = JsonConvert.SerializeObject(Region(userId), Formatting.Indented);
            context.Response.Write(json);
        }

        private dynamic Region(string user)
        {
            Dictionary<string, string> resp = new Dictionary<string, string>();
         
            string sql = @"SELECT region
                              FROM [lpcm].[dbo].[user]
                              inner join [branch] on [branch].id = [user].branch_id
                              where [user].id = '" + user + "'";
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        resp.Add("resp", rd.GetString(0));
                    }
                }
                conn.Close();
            }
            return resp;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}