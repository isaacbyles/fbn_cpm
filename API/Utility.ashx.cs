﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for Utility
    /// </summary>
    public class Utility : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection SelectedFiles = context.Request.Files;
                for (int i = 0; i < SelectedFiles.Count; i++)
                {
                    HttpPostedFile PostedFile = SelectedFiles[i];
                    string FileName = context.Server.MapPath("~/resources/" + PostedFile.FileName);
                    PostedFile.SaveAs(FileName);
                }
            }

            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("Please Select Files");
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("Files Uploaded Successfully!!");
        }




        public bool IsReusable
        {
            get
            {
                return false;
            }

        }
    }
}