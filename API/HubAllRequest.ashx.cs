﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for HubAllRequest
    /// </summary>
    public class HubAllRequest : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            DataSet response = this.GetHubRequest();
            string json = JsonConvert.SerializeObject(response, Formatting.Indented);
            context.Response.Write(json);
        }

        public DataSet GetHubRequest()
        {

            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;
            string query = @"SELECT   request.[id] as id,
		                    request.[title] As title,
		                    request.[process_id] as process_id,
		                    request.[status] as req_status, 
		                    request.[lifespan] As lifespan,
		                    request.[id] as request_id,
		                    request.[modified_date] as modified_date,
		                    request.[stage_id]
                            ,request.[init_user_id]
                            ,request.[current_user_id] AS current_user_id
	                        ,request.[created_date] AS created_date
                            ,request.[ref_no]
	                        ,[process].[name] AS process
	                        ,concat([profile].firstname, ' ', [profile].lastname) as created_by  
	                        ,[branch].name as branch
                            ,[stage].name as current_stage,
		                    [stage].role_id AS stage_role_id,
                            [stage].type AS stage_type,
                            concat(p.firstname, ' ', p.lastname)  as [held_by],
                            [stage].description AS stage_description,
                            [stage].created_date AS stage_created_date,
                            [stage].modified_date AS stage_modified_date,
                            [stage].status AS stage_status,
		                    [stage_form].form_id AS stage_form_id,
		                    [form].name AS form_name,
		                    [form].description AS form_description,
		                    CASE request.[status]
                            WHEN 6 THEN 'completed'
                            WHEN 5 THEN 'on-going'
                            WHEN 4 THEN 'terminated'
                            END  as [status_desc]
                            FROM [lpcm].[dbo].[request] 

                            inner join [process]  on [process].id = request.process_id
                            inner join [stage]    on [stage].id = request.stage_id
                            inner join [profile]  on [profile].user_id = request.init_user_id
		                    inner join [profile] p on p.user_id = request.current_user_id
                            inner join [user]     on [user].id = request.current_user_id
		                    left join  [stage_form] on [stage_form].stage_id = stage.id			
		                    left join  [form] on [form].id = [stage_form].form_id
		                    inner join [branch]   on [branch].id = [user].branch_id 
		                    where [branch].region <> 'Lagos' order by modified_date desc";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))

            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "Hub");
                conn.Close();
            }

            return ds;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}