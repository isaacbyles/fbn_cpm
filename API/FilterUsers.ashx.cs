﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using CaseManager.LawPavilion.Models;
using CaseManager.LawPavilion.Util;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for FilterUsers
    /// </summary>
    public class FilterUsers : IHttpHandler
    {

        private LPCMConnection objConnection;
        //private DataTable dt;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");

            objConnection = new LPCMConnection();
            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);
            context.Response.Write(json);
        }

        private dynamic Handler(string pathinfo)
        {
            string type = "";
            string process_id = "";
            string user_id = "";
            string role_id = "";
            string branch_id = "";
            String[] parameters = pathinfo.Split('/');
            if(parameters.Length != 6 || parameters[1] != "FilterUsers"){
                return null;
            }

            type = parameters[1]; process_id = parameters[2]; user_id = parameters[3]; role_id = parameters[4]; branch_id = parameters[5];
            return this.getUsers(process_id, user_id, role_id,branch_id);
        }

            private dynamic getUsers(string process_id,string user_id, string role_id, string branch_id){
                string process_unit = getProcessUnit(process_id);
                string user_region = getUserRegion(branch_id);
                string query;
                if (process_unit != null)
                {
                    if (process_unit == "perfection")
                    {
                        //next stage users must be from head office
                        user_region = "Lagos";
                    }
                    query = @"select [user].id,
                                        [user].email,
                                        [user].branch_id,
                                        [user].role_id,
                                        [user].created_date,
                                        [user].modified_date,
                                        [user].status,
                                        profile.firstname,
                                        profile.lastname,
                                        profile.middlename,
                                        profile.telephone,
                                        profile.created_date AS profile_created_date,
                                        profile.modified_date AS profile_modified_date,
                                        profile.status AS profile_status,
                                        branch.name AS branch_name,
                                        role.name AS role_name from [user]inner Join profile on [user].id = profile.user_id inner Join role on [user].role_id = role.id inner Join branch on [user].branch_id = branch.id where 
                                        branch.active_fg = 1 AND 
                                        role.active_fg = 1 AND
                                        ([user].role_id = " + role_id + @"  or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = " + role_id + @") > 0)) AND
                                        [user].branch_id in (select id from [branch] where region = '" +user_region+"'); ";
                    objConnection.Sql = query;
                    DataTable dt = objConnection.GetTable;
                    return dt;

                }else{
                    return null;
                }
            }

            private dynamic getEmails(string process_id,string user_id, string role_id)
            {
                return null;
            }

            private string getProcessUnit(string process_id)
            {
                objConnection.Sql = "select report_unit from report where process_id="+process_id;
                DataTable dt = objConnection.GetTable;
                return dt.Rows[0]["report_unit"].ToString();
            }

            private string getUserRegion(string branch_id)
            {
                objConnection.Sql = "select region from [branch] where id=" + branch_id;
                DataTable dt = objConnection.GetTable;
                return dt.Rows[0]["region"].ToString();
            }
     
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /********************************** Added 27th Jan 2016 ****************************************************/
        /********************************** Adeyemi Salau *********************************************************/

        public dynamic HandlerNonWeb(string process_id, string user_id, string role_id, string branch_id)
        {
            objConnection = new LPCMConnection();
            if (process_id == "" || user_id == "" || role_id == "" || branch_id == "")
            {
                return null;
            }
            //return this.getUsersNonWeb(process_id, user_id, role_id, branch_id);

            return JsonConvert.SerializeObject(this.getUsersNonWeb(process_id, user_id, role_id, branch_id), Formatting.Indented);
        }

        private dynamic getUsersNonWeb(string process_id, string user_id, string role_id, string branch_id)
        {

            string process_unit = getProcessUnitNonWeb(process_id);
            string user_region = getUserRegionNonWeb(branch_id);
            string query;
            if (process_unit != null)
            {
                if (process_unit == "perfection")
                {
                    //next stage users must be from head office
                    user_region = "Lagos";
                }
                query = @"select [user].id,
                                        [user].email,
                                        [user].branch_id,
                                        [user].role_id,
                                        [user].created_date,
                                        [user].modified_date,
                                        [user].status,
                                        profile.firstname,
                                        profile.lastname,
                                        profile.middlename,
                                        profile.telephone,
                                        profile.created_date AS profile_created_date,
                                        profile.modified_date AS profile_modified_date,
                                        profile.status AS profile_status,
                                        branch.name AS branch_name,
                                        role.name AS role_name from [user]inner Join profile on [user].id = profile.user_id inner Join role on [user].role_id = role.id inner Join branch on [user].branch_id = branch.id where 
                                        branch.active_fg = 1 AND 
                                        role.active_fg = 1 AND
                                        ([user].role_id = " + role_id + @"  or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = " + role_id + @") > 0)) AND
                                        [user].branch_id in (select id from [branch] where region = '" + user_region + "'); ";
                objConnection.Sql = query;
                DataTable dt = objConnection.GetTable;
                return dt;

            }
            else
            {
                return null;
            }
        }

        private string getProcessUnitNonWeb(string process_id)
        {
            objConnection.Sql = "select report_unit from report where process_id="+process_id;
            DataTable dt = objConnection.GetTable;
            return dt.Rows[0]["report_unit"].ToString();
        }

        private string getUserRegionNonWeb(string branch_id)
        {
            objConnection.Sql = "select region from [branch] where id=" + branch_id;
            DataTable dt = objConnection.GetTable;
            return dt.Rows[0]["region"].ToString();
        }

        /********************************** Added 27th Jan 2016 ****************************************************/
    }
}