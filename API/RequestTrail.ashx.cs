﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Script.Serialization;
using CaseManager.LawPavilion.Util;
using Newtonsoft.Json;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for RequestTrail
    /// </summary>
    public class RequestTrail : IHttpHandler
    {
        private LPCMConnection objConnection;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            objConnection = new LPCMConnection();
            context.Response.ContentType = "application/json";
            string action = context.Request.QueryString["a"];
            DataSet response = null;
            switch (action)
            {
                case "getRoleBranch" :
                    response = this.GetBranchUser(context);
                    break;

                case "branch":
                    response = this.Branch(context);
                    break;
                case "getRoleExternal":
                    response = this.GetExternalSolicitor(context);
                    break;

                case "getRoleTeamLead":
                    response = this.GetTeamLead(context);
                    break;

                case "getGetTeamLeadEs":
                    response = this.GetTeamLeadES(context);
                    break;
                case "getTrail" :
                    response = this.GetUserId(context);
                    break;

                case "getLegalOfficerES":
                    response = this.GetLegalOfficerES(context);
                    break;
                case "getTrailWithReq":
                    response = this.GetRequestTrail_With_RequestId(context);
                    break;

                case "getUnitHead":
                    response = this.GetUnitHead(context);
                    break;

                case "getGetUnitHeadEs":
                    response = this.GetUnitHeadEs(context);
                    break;
                case "getLegalOfficer":
                    response = this.GetLegalOfficer(context);
                    break;
                    
                case "getHODLEGAL":
                    response = this.GetHODLegal(context);
                    break;

                case "getIC":
                    response = this.GetInternalControlUnit(context);
                    break;

                case "getCam":
                    response = this.GetCAM(context);
                    break;

                case "getInternalAudit":
                    response = this.GetInternalAudit(context);
                    break;

                case "getLEGALSECRETARY":
                    response = this.GetLEGALSECRETARY(context);
                    break;

                case "AllHQLegalOfficer":
                    response = this.AllHQLegalOfficer(context);
                    break;


            }
            
            string json = JsonConvert.SerializeObject(response, Formatting.Indented);
            context.Response.Write(json);
        }

        public DataSet GetUserId(HttpContext ctx)
        {
            DataSet ds =  new DataSet();
            SqlDataAdapter rd = null;
            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
            string query = @"SELECT * FROM [lpcm].[dbo].[request_trail] rt
                            inner join [lpcm].[dbo].[request] req on req.id = rt.request_id
                            where rt.request_id = '" + objUser.request_id + "'  and rt.stage_id ='" + objUser.stage_id+"'";

            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "Users");

            }

            return ds;
        }

        //Get List of Request Trail with Request Id only

        public DataSet GetRequestTrail_With_RequestId(HttpContext ctx)
        {
            
            DataSet ds =  new DataSet();
            SqlDataAdapter rd = null;
            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
            string query = @"SELECT * FROM [lpcm].[dbo].[request_trail] rt
                            inner join [lpcm].[dbo].[request] req on req.id = rt.request_id
                            where rt.request_id = '" + objUser.request_id +"'";

            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "Users_Team");

            }

            return ds;
        }

        public DataSet GetBranchUser(HttpContext ctx)
        {
            DataSet ds= new DataSet();
            SqlDataAdapter rd = null;
            
            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
            string region = getUserRegion(objUser.branch_id);
            regionUser reg = getUnit(objUser.request_id, objUser.stage_id);
            string query = @"
                           select [user].id,
                                        [user].email,
                                        [user].branch_id,
                                       -- [user].role_id,
                                        [user].created_date,
                                        [user].modified_date,
                                        [user].[status],
                                        [profile].firstname,
                                        profile.lastname,
                                        profile.middlename,
                                        profile.telephone,
                                        profile.created_date AS profile_created_date,
                                        profile.modified_date AS profile_modified_date,
                                        profile.status AS profile_status,
                                        branch.name AS branch_name,
                                        role.name AS role_name,
                                        [firstname],
                                        [lastname],
                                        [firstname] + ' ' + [lastname] AS title,
                                        CASE 
                                          WHEN ([user].role_id) is not null THEN '29'
                            
                                        END  as role_id

										from [user]
										inner Join profile on [user].id = profile.user_id 
										inner Join [role] on [role].id  = [user].role_id
										inner Join branch on  branch.id = [user].branch_id
										left Join [user_role] on [user_role].role_id= [role].id
										where [user].role_id = 29 and region='" + reg.region + "' and [user].status = 1 or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = 29) > 0) ";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "UsersBranch");
                conn.Close();
            }
            return ds;
        }



        public DataSet Branch(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;

            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
            string region = getUserRegion(objUser.branch_id);
            regionUser reg = getUnit(objUser.request_id, objUser.stage_id);
            string query = @"
                           select [user].id,
                                        [user].email,
                                        [user].branch_id,
                                       -- [user].role_id,
                                        [user].created_date,
                                        [user].modified_date,
                                        [user].[status],
                                        [profile].firstname,
                                        profile.lastname,
                                        profile.middlename,
                                        profile.telephone,
                                        profile.created_date AS profile_created_date,
                                        profile.modified_date AS profile_modified_date,
                                        profile.status AS profile_status,
                                        branch.name AS branch_name,
                                        role.name AS role_name,
                                        [firstname],
                                        [lastname],
                                        [firstname] + ' ' + [lastname] AS title,
                                        CASE 
                                          WHEN ([user].role_id) is not null THEN '29'
                            
                                        END  as role_id

										from [user]
										inner Join profile on [user].id = profile.user_id 
										inner Join [role] on [role].id  = [user].role_id
										inner Join branch on  branch.id = [user].branch_id
										left Join [user_role] on [user_role].role_id= [role].id
										where [user].role_id = 29 and region='" + region + "' and [user].status = 1 or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = 29) > 0) ";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "UsersBranch");

            }
            return ds;
        }

        public DataSet GetExternalSolicitor(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;
            string query = @"SELECT DISTINCT  [user].[id]
                                        ,[user].[email]
                                        ,[user].[password]
                                        ,[branch_id]
                                        ,[user].[role_id]
                                        ,[created_date]
                                        ,[modified_date]
                                        ,[status]
	                                    ,FIRM,
	                          CASE [user].role_id
                            WHEN 11 THEN 'External Solicitor'
                          
                            END  as [role_name] 
                                        FROM [lpcm].[dbo].[user]
                                        inner join [external_solicitors] on [external_solicitors].id = [user].id
                                        where [external_solicitors].role_id = '" +
                        ConfigurationManager.AppSettings["EXT"] + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "ExternalSolicitor");

            }
            return ds;
        }

        private DataSet GetTeamLead(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;

            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
           
            string report_unit = getProcessUnit(objUser.process_id);
            string region = getUserRegion(objUser.branch_id);
            string query = String.Empty;

            //get the region of the initial unit head  if exist
            regionUser reg = getUnit(objUser.request_id, objUser.stage_id);
            if (reg.region == null)
            {
                if (region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '24'  and region ='" + region +
                                   "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='24' and [user].status=1 and region ='" +
                                   region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '24'  and region ='" + region +
                               "' and [user].status=1 and profile.middlename='" + report_unit +
                               "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='24' and [user].status=1 and region ='"+region+"' and [profile].middlename = '" +
                               report_unit + "')";
                }

            }
            else
                if (reg.region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '24'  and region ='" + reg.region +
                            "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='24' and [user].status=1 and region ='" +
                            reg.region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '24'  and region ='" + region +
                            "' and [user].status=1 and profile.middlename='" + report_unit +
                            "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='24' and [user].status=1 and region ='"+region+"' and [profile].middlename = '" +
                            report_unit + "')";
                }


            #region 

//            if (region != "Lagos")
//            {
//                query = @"SELECT [user].[id]
//                                ,[email]
//                                 ,[password]
//                                 ,[branch_id]
//                                ,[role_id]
//                                ,[branch].region
//                              ,[firstname]
//                             ,[lastname]
//	                             ,[firstname] + ' ' + [lastname] AS title
//                                ,role.name as role_name
//	                           ,role.id as role_id, [report].report_unit
//                          
//                                FROM [lpcm].[dbo].[user]    
//                                inner join branch on branch.id = branch_id
//                            inner join [role] on [role].id = [user].role_id
//                            inner join [profile] on [profile].user_id = [user].id
//                            inner join [report] on [report].report_unit = profile.middlename
//                             where                                  
//                            (role_id = '25'  and region ='" + region +
//                               "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='25' and [user].status=1 and region ='" +
//                               region + "')";
//            }
//            else
//            {
//                query = @"SELECT [user].[id]
//                                ,[email]
//                                 ,[password]
//                                 ,[branch_id]
//                                ,[role_id]
//                                ,[branch].region
//                              ,[firstname]
//                             ,[lastname]
//	                             ,[firstname] + ' ' + [lastname] AS title
//                                ,role.name as role_name
//	                           ,role.id as role_id, [report].report_unit 
//     
//                                FROM [lpcm].[dbo].[user]    
//                                inner join branch on branch.id = branch_id
//                            inner join [role] on [role].id = [user].role_id
//                            inner join [profile] on [profile].user_id = [user].id
//                            inner join [report] on [report].report_unit = profile.middlename
//                             where                                  
//                            (role_id = '25'  and region ='" + region +
//                           "' and [user].status=1 and profile.middlename='" + report_unit +
//                           "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='Lagos' and [profile].middlename = '" +
//                           report_unit + "')";
//            }
//             string report_unit = getProcessUnit(objUser.process_id);
//            string query = @"
//                           
//                            SELECT DISTINCT [user].[id]
//                                  ,[email]
//                                  ,[password]
//                                  ,[branch_id]
//                                 -- ,[user].[role_id]
//                                 ,[branch].region
//	                             ,[report].report_unit,
//                                 [firstname],
//                                [lastname],
//                                [firstname] + ' ' + [lastname] AS title,
//                           CASE 
//                              WHEN ([user].role_id) is not null THEN '24'
//                            
//                            END  as role_id
//                              FROM [lpcm].[dbo].[user]
//                              inner join branch on branch.id = branch_id
//                              inner join [role] on [role].id = [user].role_id
//                              inner join [profile] on [profile].user_id = [user].id
//                              inner join report  on report.report_unit = [profile].middlename 
//                              left Join [user_role] on [user_role].role_id= [role].id
//                              where [user].role_id =24 and [user].status = 1 and branch_id = '" + objUser.branch_id + "' and report_unit= '" + report_unit + "' or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = 24) > 0)";

            #endregion

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "UsersTeamLead");

            }
            return ds;
        }
        
        //get unit head list
        private DataSet GetUnitHead(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;
            
            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
            string report_unit = getProcessUnit(objUser.process_id);
            string region = getUserRegion(objUser.branch_id);
            string query = String.Empty;

            //get the region of the initial unit head  if exist
            regionUser reg = getUnit(objUser.request_id, objUser.stage_id);
            if (reg.region == null)
            {
                if (region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '25'  and region ='" + region +
                                   "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='25' and [user].status=1 and region ='" +
                                   region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '25'  and region ='" + region +
                               "' and [user].status=1 and profile.middlename='" + report_unit +
                               "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='25' and [user].status=1 and region ='"+region+"' and [profile].middlename = '" +
                               report_unit + "')";
                }

            }
            else 
            if (reg.region != "Lagos")
            {
                query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '25'  and region ='" + reg.region +
                        "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='25' and [user].status=1 and region ='" +
                        reg.region + "')";
            }
            else
            {
                query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '25'  and region ='" + region +
                        "' and [user].status=1 and profile.middlename='" + report_unit +
                        "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='25' and [user].status=1 and region ='"+region+"' and [profile].middlename = '" +
                        report_unit + "')";
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "UsersUnitHead");

            }
            return ds;
        }

        private DataSet GetUnitHeadEs(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;

            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
            string report_unit = getProcessUnit(objUser.process_id);
            string region = GetInitialUser(objUser.request_id);
            string query = String.Empty;

            //get the region of the initial unit head  if exist
            regionUser reg = getUnit(objUser.request_id, objUser.stage_id);
            if (reg.region == null)
            {
                if (region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '25'  and region ='" + region +
                                   "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='25' and [user].status=1 and region ='" +
                                   region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '25'  and region ='" + region +
                               "' and [user].status=1 and profile.middlename='" + report_unit +
                               "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='25' and [user].status=1 and region ='" + region + "' and [profile].middlename = '" +
                               report_unit + "')";
                }

            }
            else
                if (reg.region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '25'  and region ='" + reg.region +
                            "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='25' and [user].status=1 and region ='" +
                            reg.region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '25'  and region ='" + region +
                            "' and [user].status=1 and profile.middlename='" + report_unit +
                            "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='25' and [user].status=1 and region ='" + region + "' and [profile].middlename = '" +
                            report_unit + "')";
                }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "UsersUnitHeadES");

            }
            return ds;
        }
        //get Legal Officer

        private DataSet GetTeamLeadES(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;

            RequestSmall objUser = Deserialize<RequestSmall>(ctx);

            string report_unit = getProcessUnit(objUser.process_id);
            string region = GetInitialUser(objUser.request_id);
            string query = String.Empty;

            //get the region of the initial unit head  if exist
            regionUser reg = getUnit(objUser.request_id, objUser.stage_id);
            if (reg.region == null)
            {
                if (region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '24'  and region ='" + region +
                                   "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='24' and [user].status=1 and region ='" +
                                   region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '24'  and region ='" + region +
                               "' and [user].status=1 and profile.middlename='" + report_unit +
                               "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='24' and [user].status=1 and region ='" + region + "' and [profile].middlename = '" +
                               report_unit + "')";
                }

            }
            else
                if (reg.region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '24'  and region ='" + reg.region +
                            "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='24' and [user].status=1 and region ='" +
                            reg.region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '24'  and region ='" + region +
                            "' and [user].status=1 and profile.middlename='" + report_unit +
                            "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='24' and [user].status=1 and region ='" + region + "' and [profile].middlename = '" +
                            report_unit + "')";
                }


         
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "UsersTeamLeadES");

            }
            return ds;
        }

        private string GetInitialUser(string req)
        {
            int id = 0;

            string sql = @"SELECT [init_user_id] FROM [request] WHERE id='" + req + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        id = rd.GetInt32(0);
                    }
                }
                conn.Close();
            }
            string GetRegionBy = GetRegionById(id.ToString());
            return GetRegionBy;
        }

        private string GetRegionById(string id)
        {
           string reg=String.Empty;

            string sql = @"SELECT [user].[id]
	                          , region
                          FROM [lpcm].[dbo].[user]
                          inner join [branch] on [branch].id = [user].branch_id
                          where [user].id= '" + id + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        reg = rd.GetString(1);
                    }
                }
                conn.Close();
            }

            return reg.ToString();
        }


        private DataSet AllHQLegalOfficer(HttpContext ctx)
        {

            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;

            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
            string region = getUserRegion(objUser.branch_id);

            string sql = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '15'  and region ='"+region+"' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" + region + "')";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "AllHQ");
                conn.Close();
            }
            return ds;
        }


        private DataSet GetLegalOfficer(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;
            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
            string report_unit = getProcessUnit(objUser.process_id);
            string region = getUserRegion(objUser.branch_id);
            string role_id = ConfigurationManager.AppSettings["LO"].ToString();
            
            #region

//            string query = @"SELECT DISTINCT [user].[id]
//                                  ,[email]
//                                  ,[password]
//                                  ,[branch_id]
//                                  ,[role_id]
//                                 ,[branch].region
//	                              ,[firstname]
//	                              ,[lastname]
//	                             ,[firstname] + ' ' + [lastname] AS title
//                                ,role.name as role_name
//	                           ,role.id as role_id
//                              FROM [lpcm].[dbo].[user]
//                              inner join branch on branch.id = [user].branch_id
//                              inner join [role] on [role].id = [user].role_id
//                              inner join [profile] on [profile].user_id = [user].id
//							  where role_id = '15'  and [user].[status] = 1 and  (region = '" + region +
//                        "' and profile.middlename='hub' or profile.middlename = '" + report_unit +
//   



            // "') or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '15'))>0";;

            #endregion

            string query = string.Empty;
            regionUser reg = getUnit(objUser.request_id, objUser.stage_id);

            #region 

//            if (reg.region == null)
//            {

//                if (role_id == "15" && user_region == "Lagos")
//                {

//                    query = @"SELECT [user].[id]
//                                ,[email]
//                                 ,[password]
//                                 ,[branch_id]
//                                ,[role_id]
//                                ,[branch].region
//                              ,[firstname]
//                             ,[lastname]
//	                             ,[firstname] + ' ' + [lastname] AS title
//                                ,role.name as role_name
//	                           ,role.id as role_id, [report].report_unit 
//     
//                                FROM [lpcm].[dbo].[user]    
//                                inner join branch on branch.id = branch_id
//                            inner join [role] on [role].id = [user].role_id
//                            inner join [profile] on [profile].user_id = [user].id
//                            inner join [report] on [report].report_unit = profile.middlename
//                             where                                  
//                            (role_id = '" + role_id + "'  and region ='" + user_region +
//                            "' and [user].status=1 and profile.middlename='" + process_unit +
//                            "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='"+user_region+"' and [profile].middlename = '" +
//                            process_unit + "')";
//                }
//                else
//                {
//                    query = @"SELECT [user].[id]
//                                ,[email]
//                                 ,[password]
//                                 ,[branch_id]
//                                ,[role_id]
//                                ,[branch].region
//                              ,[firstname]
//                             ,[lastname]
//	                             ,[firstname] + ' ' + [lastname] AS title
//                                ,role.name as role_name
//	                           ,role.id as role_id, [report].report_unit
//                          
//                                FROM [lpcm].[dbo].[user]    
//                                inner join branch on branch.id = branch_id
//                            inner join [role] on [role].id = [user].role_id
//                            inner join [profile] on [profile].user_id = [user].id
//                            inner join [report] on [report].report_unit = profile.middlename
//                             where                                  
//                            (role_id = '15'  and region ='" + reg.region +
//                            "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" +
//                            reg.region + "')";
//                }
//            }
//            else if (role_id == "15" && reg.region != "Lagos")
//            {

//                query = @"SELECT [user].[id]
//                                ,[email]
//                                 ,[password]
//                                 ,[branch_id]
//                                ,[role_id]
//                                ,[branch].region
//                              ,[firstname]
//                             ,[lastname]
//	                             ,[firstname] + ' ' + [lastname] AS title
//                                ,role.name as role_name
//	                           ,role.id as role_id, [report].report_unit
//                          
//                                FROM [lpcm].[dbo].[user]    
//                                inner join branch on branch.id = branch_id
//                            inner join [role] on [role].id = [user].role_id
//                            inner join [profile] on [profile].user_id = [user].id
//                            inner join [report] on [report].report_unit = profile.middlename
//                             where                                  
//                            (role_id = '15'  and region ='" + reg.region +
//                           "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" +
//                           reg.region + "')";
//            }

            #endregion
            if (reg.region == null)
            {
                if (region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '15'  and region ='" + region +
                                   "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" +
                                   region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '15'  and region ='" + region +
                               "' and [user].status=1 and profile.middlename='" + report_unit +
                               "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" + region + "' and [profile].middlename = '" +
                               report_unit + "')";
                }

            }
            else
                if (reg.region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '15'  and region ='" + reg.region +
                            "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" +
                            reg.region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '15'  and region ='" + region +
                            "' and [user].status=1 and profile.middlename='" + report_unit +
                            "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" + region + "' and [profile].middlename = '" +
                            report_unit + "')";
                }
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "LegalOfficer");

            }
            return ds;

        }


        private DataSet GetLegalOfficerES(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;
            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
            string report_unit = getProcessUnit(objUser.process_id);
            string region = GetInitialUser(objUser.request_id);
            string role_id = ConfigurationManager.AppSettings["LO"].ToString();

         

            string query = string.Empty;
            regionUser reg = getUnit(objUser.request_id, objUser.stage_id);

      
            if (reg.region == null)
            {
                if (region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '15'  and region ='" + region +
                                   "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" +
                                   region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '15'  and region ='" + region +
                               "' and [user].status=1 and profile.middlename='" + report_unit +
                               "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" + region + "' and [profile].middlename = '" +
                               report_unit + "')";
                }

            }
            else
                if (reg.region != "Lagos")
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit
                          
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '15'  and region ='" + reg.region +
                            "' and [user].status=1) UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" +
                            reg.region + "')";
                }
                else
                {
                    query = @"SELECT [user].[id]
                                ,[email]
                                 ,[password]
                                 ,[branch_id]
                                ,[role_id]
                                ,[branch].region
                              ,[firstname]
                             ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id, [report].report_unit 
     
                                FROM [lpcm].[dbo].[user]    
                                inner join branch on branch.id = branch_id
                            inner join [role] on [role].id = [user].role_id
                            inner join [profile] on [profile].user_id = [user].id
                            inner join [report] on [report].report_unit = profile.middlename
                             where                                  
                            (role_id = '15'  and region ='" + region +
                            "' and [user].status=1 and profile.middlename='" + report_unit +
                            "') UNION select [user].[id] ,[email] ,[password] ,[branch_id] ,[user_role].[role_id] ,[branch].region ,[firstname] ,[lastname] ,[firstname] + ' ' + [lastname] AS title ,role.name as role_name ,role.id as role_id, [report].report_unit from user_role  inner join [user] on [user].id = [user_role].user_id inner join branch on branch.id = branch_id inner join [role] on [role].id = [user].role_id inner join [profile] on [profile].user_id = [user].id inner join [report] on [report].report_unit = profile.middlename where ([user_role].role_id ='15' and [user].status=1 and region ='" + region + "' and [profile].middlename = '" +
                            report_unit + "')";
                }
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "LegalOfficerES");

            }
            return ds;

        }


        //get Hod Legal

        public DataSet GetHODLegal(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;

            RequestSmall objUser = Deserialize<RequestSmall>(ctx);
            string report_unit = getProcessUnit(objUser.process_id);

            //            string query = @"
            //                            SELECT DISTINCT [user].[id]
            //                                  ,[email]
            //                                  ,[password]
            //                                  ,[branch_id]
            //                                  ,[user].[role_id]
            //                                 ,[branch].region
            //	                              ,[report].report_unit,
            //                                    [firstname],
            //                                [lastname],
            //                                [firstname] + ' ' + [lastname] AS title
            //                              FROM [lpcm].[dbo].[user]
            //                              inner join branch on branch.id = branch_id
            //                              inner join [role] on [role].id = [user].role_id
            //                              inner join [profile] on [profile].user_id = [user].id
            //                                left Join [user_role] on [user_role].role_id= [role].id
            //                            inner join report  on report.report_unit = [profile].middlename 
            //                              where [user].role_id =9 and branch_id = '" + objUser.branch_id + "' and report_unit= '" + report_unit + "' or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = 9) > 0)";

            string query = @"  SELECT DISTINCT  [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                --,[user].[role_id]
                                  ,[branch].region as branch_region,
	                               [firstname],
                                   [lastname],
                                   [firstname] + ' ' + [lastname] AS title,
								CASE 
                              WHEN ([user].role_id) is not null THEN '9'
                            
                            END  as role_id
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
                             left Join [user_role] on [user_role].role_id= [role].id
                            ----inner join report  on report.report_unit = [profile].middlename 
                              where [user].role_id = 9 and [user].status = 1  or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = 9) > 0) ";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "HODLEGAL");

            }
            return ds;
        }


        // get Internal Control
        public DataSet GetInternalControlUnit(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;
            string query = @"
                            SELECT [id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  --,[user].[role_id]
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title,
 CASE 
                              WHEN ([user].role_id) is not null THEN '18'
                            
                            END  as role_id
                              FROM [lpcm].[dbo].[user] 
                              inner join [profile]  on [user].id = [profile].user_id 
  
where [user].role_id=18 and [user].[status]= 1   or  ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = 18) > 0)";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "IC");

            }
            return ds;
        }

        public DataSet GetCAM(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;
            string query = @"
                            SELECT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                 -- ,[user].[role_id]
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title,
                            CASE 
                              WHEN ([user].role_id) is not null THEN '26'
                            
                            END  as role_id
                              FROM [lpcm].[dbo].[user] 
                              inner join [profile]  on [user].id = [profile].user_id 
                              inner join [role] on [role].id= [user].role_id
                              left Join [user_role] on [user_role].role_id= [role].id
                              where [user].role_id=26 and [user].[status] = 1 or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = 26) > 0)";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "CAM");

            }
            return ds;
        }

        public DataSet GetInternalAudit(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;
            string query = @"
                            SELECT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                 -- ,[user].[role_id]
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title,
                            CASE 
                              WHEN ([user].role_id) is not null THEN '17'
                            
                            END  as role_id
                              FROM [lpcm].[dbo].[user] 
                              inner join [profile]  on [user].id = [profile].user_id 
                              inner join [role] on [role].id= [user].role_id
                              left Join [user_role] on [user_role].role_id= [role].id
                            where [user].role_id=17 and [user].[status] = 1 or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = 17) > 0)";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "IA");

            }
            return ds;
        }


        public DataSet GetLEGALSECRETARY(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter rd = null;
            string query = @"
                            SELECT distinct [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  --,[user].[role_id]
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title,
                            CASE 
                              WHEN ([user].role_id) is not null THEN '27'
                            
                            END  as role_id
                              FROM [lpcm].[dbo].[user] 

                              inner join [profile]  on [user].id = [profile].user_id 
							  inner join [role] on [role].id= [user].role_id
  left Join [user_role] on [user_role].role_id= [role].id
where [user].role_id=27 and [user].[status] = 1 or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = 27) > 0)";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "LS");

            }
            return ds;
        }

        private string getProcessUnit(string process_id)
        {
            DataTable dt= new DataTable();
            if (process_id == null)
            {
            }
            else
            {
                objConnection.Sql = "select report_unit from report where process_id=" + process_id;
                dt = objConnection.GetTable;
               
            }
            return dt.Rows[0]["report_unit"].ToString();
        }

        private string getUserRegion(string branch_id)
        {
             DataTable dt =new DataTable();
            if (branch_id == null)
            {
            }
            else
            {
                objConnection.Sql = "select region from [branch] where id=" + branch_id;
                 dt = objConnection.GetTable;
               
            }
            return dt.Rows[0]["region"].ToString();
        }

        private regionUser getUnit(string req, string stg)
        {
            #region 

//DataTable dt = new DataTable();
            //if (req == null && stg == null )
            //{
            //}
            //else
            //{
            //    objConnection.Sql = "select user_id from [request_trail] where request_id='" + req+"' and stage_id = '"+stg+"'";
            //    dt = objConnection.GetTable;
            //    if (dt.Rows[0]["user_id"].ToString() == null) { }

            //}
            //return dt.Rows[0]["user_id"].ToString();

            #endregion

            int str = 0;
            regionUser user = new regionUser();
            string sql = @"select user_id, region from [request_trail] 
                            inner join [user] on [user].id = [request_trail].user_id
                            inner join [branch] on [branch].id = [user].branch_id 
                            where request_id='" + req + "' and stage_id = '" + stg + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    while (rd.Read())
                    {
                        user.user_id = rd.GetInt32(0);
                        user.region = rd.GetString(1);
                    }
                }
                return user;
            }
        }
        public T Deserialize<T>(HttpContext context)
        {
            string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();
            var obj = (T)new JavaScriptSerializer().Deserialize<T>(jsonData);
            return obj;
        }


        public class RequestSmall
        {
            public string request_id  { get; set; }
            public string stage_id { get; set; }
            public string process_id { get; set; }
            public string branch_id { get; set; }
            public string role_id { get; set; }
            public string user_id { get; set; }
        }

        public class regionUser
        {
            public int user_id { get; set; }
            public string region { get; set; }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}