﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using CaseManager.LawPavilion.Util;
using CaseManager.Logic;
using CaseManager.Util;
using Newtonsoft.Json;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for Assign
    /// </summary>
    public class Assign : IHttpHandler
    {
        private LPCMConnection objConnection;
        private static AuditLogic audit = new AuditLogic();
        private HttpCookie cookie = new HttpCookie("ID");
        public Object _userID;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            objConnection = new LPCMConnection();
            context.Response.ContentType = "application/json";
            string action = context.Request.QueryString["a"];
            DataTable dd = null;
            switch (action)
            {
                case "getUserRole":
                    dd = this.GetRoleUsers(context);
                    break;

                case "Reassign_Request":
                    dd = this.Reassign(context);
                    break;
            }

            string json = JsonConvert.SerializeObject(dd, Formatting.Indented);
            context.Response.Write(json);
            
        }


        private DataTable GetRoleUsers(HttpContext ctx)
        {
            DataSet ds = new DataSet();
            RequestAssign objUser = Deserialize<RequestAssign>(ctx);
            string report_unit = getProcessUnit(objUser.process_id);
            string user_unit = getUserUnit(objUser.user_id);
            string branch_region = getUserRegion(objUser.branch_id);
            string role_id_check = objUser.role_id;
            string query = "";
            if (role_id_check == "15" && branch_region != "Lagos") //legal officer
            {
                query = @"
                    SELECT DISTINCT [user].[id]
                                  ,[role_id]
                                  ,[firstname]
	                              ,[lastname],
								  profile.middlename
	                              ,[firstname] + ' ' + [lastname] AS title
	                              FROM [lpcm].[dbo].[user]
                                  inner join branch on branch.id = branch_id
                                  inner join [role] on [role].id = [user].role_id
                                  inner join [profile] on [profile].user_id = [user].id 
                                  where role_id = '" + objUser.role_id + "'  and region ='" + branch_region +
                        "'  and [profile].middlename ='" + report_unit + "'  or [profile].middlename ='hub'";
            }
            else if (role_id_check == "15" && branch_region == "Lagos")
            {
               /* query = @"
                   SELECT DISTINCT [user].[id]
                                  ,[role_id]
                                  ,[firstname]
	                              ,[lastname],
								  profile.middlename
	                              ,[firstname] + ' ' + [lastname] AS title
	                              FROM [lpcm].[dbo].[user]
                                  inner join branch on branch.id = branch_id
                                  inner join [role] on [role].id = [user].role_id
                                  inner join [profile] on [profile].user_id = [user].id
                                     CASE 
                              WHEN ([user].role_id) is not null THEN '24'
                            
                            END  as role_id
                                  where role_id = '" + objUser.role_id + "'  and region ='" + branch_region +
                        "'  and [profile].middlename ='" + report_unit + "' or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '15'))>0";
*/
                query = @"SELECT DISTINCT [user].[id]
                                 ,[firstname]
	                              ,[lastname],
								  profile.middlename
	                              ,[firstname] + ' ' + [lastname] AS title,
								    CASE 
                              WHEN ([user].role_id) is not null THEN '15'
                            END  as role_id
	                              FROM [lpcm].[dbo].[user]
                                  inner join branch on branch.id = branch_id
                                  inner join [role] on [role].id = [user].role_id
                                  inner join [profile] on [profile].user_id = [user].id
                                   
                                  where role_id = '" + objUser.role_id + "'  and region ='" + branch_region +
                        "'  and [profile].middlename ='" + report_unit +
                        "' or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '" +
                        objUser.role_id + "'))>0";
            }
            else{
            query = @"
                    SELECT DISTINCT [user].[id]
                                  ,[role_id]
                                  ,[firstname]
	                              ,[lastname]
	                              ,[firstname] + ' ' + [lastname] AS title
	                          FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
                              where role_id ='" + objUser.role_id + "'  ";



                /*and branch_id ='" + objUser.branch_id +
                              "' and profile.middlename='" + user_unit + "'*/
            }
            using(SqlConnection conn =  new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd  = new SqlDataAdapter(cmd);
                rd.Fill(ds);
            }
            return ds.Tables[0];
        }

        private DataTable Reassign(HttpContext ctx)
        {
            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                _userID = cookie.Value;
                HttpServerTest.Logger.Logger.writelog(_userID.ToString() + "coo");
            }
            

            DataSet ds= new DataSet();
             DataTable dt = new DataTable();
            DataColumn Name = new DataColumn("Name", typeof(string));
            
            dt.Columns.Add(Name);
            DataRow dr = dt.NewRow();
            RequestAssign objUser = Deserialize<RequestAssign>(ctx);

            string query = @"UPDATE [request] SET current_user_id = '" + objUser.user_id + "' WHERE id = " +
                           objUser.request_id;
            using(SqlConnection conn =  new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                int i = cmd.ExecuteNonQuery();
               
                if (i == 1)
                {
                    dr["Name"] = "success";
                }
                else
                {
                    dr["Name"] = "Failed";
                }
                dt.Rows.Add(dr);
            }
            DateTime tds = DateTime.Now;
            string _username = UsersClass.GetName(_userID.ToString());
            string name = GetRequestTitle(objUser.request_id);
            bool res = audit.SaveAudit(_username, "Reassign Request : " + "<b>[" + name + "]</b>", "Request", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());
            return dr.Table;
        }

        private string GetRequestTitle(string request_id)
        {
            string title = String.Empty;
            string sql = @"select title from [request] where id= '" + request_id + "'";
            using(SqlConnection conn= new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        title = (string) rd["title"];
                    }
                }

                conn.Close();
            }
            return title;
        }
        private string getProcessUnit(string process_id)
        {
            DataTable dt = new DataTable();
            if (process_id == null)
            {
            }
            else
            {
                objConnection.Sql = "select report_unit from report where process_id=" + process_id;
                dt = objConnection.GetTable;

            }
            return dt.Rows[0]["report_unit"].ToString();
        }

        private string getUserUnit(string user_id) //return litigation, perfection, corporate or hub
        {
            DataTable dt = new DataTable();
            if (user_id == null)
            {
            }
            else
            {
                objConnection.Sql = @"SELECT  [middlename]      
                                      FROM [lpcm].[dbo].[profile]
                                      inner join [user] on [user].id = [profile].user_id
                                      where [user].id=" + user_id;
                dt = objConnection.GetTable;

            }
            return dt.Rows[0]["middlename"].ToString();
        }
        
        private string getUserRegion(string branch_id)
        {
            DataTable dt = new DataTable();
            if (branch_id == null)
            {
            }
            else
            {
                objConnection.Sql = "select region from [branch] where id=" + branch_id;
                dt = objConnection.GetTable;

            }
            return dt.Rows[0]["region"].ToString();
        }
     

        public T Deserialize<T>(HttpContext context)
        {
            string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();
            var obj = (T)new JavaScriptSerializer().Deserialize<T>(jsonData);
            return obj;
        }


        public class RequestAssign
        {
            public string request_id { get; set; }
            public string stage_id { get; set; }
            public string process_id { get; set; }
            public string branch_id { get; set; }
            public string role_id { get; set; }
            public string user_id { get; set; }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}