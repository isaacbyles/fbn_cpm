﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using CaseManager.Util;
using CaseManager.Logic;
using System.Data;
using System.Data.SqlClient;
using System.Web.SessionState;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for Request
    /// </summary>
    public class Request : IHttpHandler, IRequiresSessionState
    {
        private RequestLogic logic = new RequestLogic();
        private HttpContext context;
        static AuditLogic audit = new AuditLogic();
        static string _id  = " ";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");

            Model.Base.has_error = false;
            this.context = context;
            context.Response.ContentType = Constant.JSON_CONTENT_TYPE;
            string action = context.Request.QueryString["a"];
            string response = "";
            switch (action)
            {
                case "create":
                    response = this.create();
                    break;
                case "proceed":
                    response = this.proceed();
                    break;
                case "end":
                    response = this.end();
                    break;
                case "cancel":
                    response = this.cancel();
                    break;
                case "reassign":
                    response = this.reassign();
                    break;
                case "get":
                    response = this.get();
                    break;
                case "get_trail":
                    response = this.getTrail();
                    break;
                case "get_form":
                    response = this.getForms();
                    break;
                case "get_form_data":
                    response = this.getFormData();
                    break;
                case "add_comment":
                    response = this.addComment();
                    break;
                case "get_comment":
                    response = this.getComments();
                    break;
                default:
                    break;
            }

            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #region Logic API
        private string create()
        {
            string next_stage_id = this.context.Request.Form.Get("next_stage_id");
            string next_user_id = this.context.Request.Form.Get("next_user_id");
            string title = this.context.Request.Form.Get("title");
            string description = this.context.Request.Form.Get("description");
            string remark = this.context.Request.Form.Get("remark");
            string note = this.context.Request.Form.Get("note");
            string lifespan = this.context.Request.Form.Get("lifespan");
            string form_id = this.context.Request.Form.Get("form_id");
            string form_data_list = this.context.Request.Form.Get("form_data_list");

            List<string> required_fields = new List<string>();
            required_fields.Add(next_stage_id);
            required_fields.Add(next_user_id);
            required_fields.Add(title);
            required_fields.Add(description);
            required_fields.Add(remark);
            required_fields.Add(note);
            //required_fields.Add(lifespan);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            int? _form_id;
            List<Dictionary<string, object>> _form_data_list;

            if (form_id == null)
            {
                _form_id = null;
                _form_data_list = null;
            }
            else
            {
                _form_id = int.Parse(form_id);
                _form_data_list = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(form_data_list);
            }

            long id = this.logic.create(
                int.Parse(next_stage_id),
                int.Parse(next_user_id),
                title,
                description,
                remark,
                note,
                int.Parse(lifespan),
                _form_id,
                _form_data_list
                );
            
            UserDetails ud = new UserDetails();
          
            if (HttpContext.Current.Session["ID"] != null)
            {
                 _id = HttpContext.Current.Session["ID"].ToString();
            }
           
           
            DateTime tds = DateTime.Now;
            bool res = audit.SaveAudit(_id, "Request Created : " + "<b>[" + title + "]</b>", "Request", SystemInformation.GetIPAddress(), SystemInformation.GetMACAddress());

            return Response.json(this.logic.Status, id, this.logic.Message);
        }

        private string proceed()
        {
            string id = this.context.Request.Form.Get("id");
            string next_stage_id = this.context.Request.Form.Get("next_stage_id");
            string next_user_id = this.context.Request.Form.Get("next_user_id");
            string form_id = this.context.Request.Form.Get("form_id");
            string form_data_list = this.context.Request.Form.Get("form_data_list");

            List<string> required_fields = new List<string>();
            required_fields.Add(next_stage_id);
            required_fields.Add(id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            int? _form_id;
            List<Dictionary<string, object>> _form_data_list;

            if (form_id == null)
            {
                _form_id = null;
                _form_data_list = null;
            }
            else
            {
                _form_id = int.Parse(form_id);
                _form_data_list = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(form_data_list);
            }

            int? _next_user_id;
            if (next_user_id == null)
            {
                _next_user_id = null;
            }
            else
            {
                _next_user_id = int.Parse(next_user_id);
            }

            this.logic.proceed(
                long.Parse(id),
                int.Parse(next_stage_id),
                _next_user_id,
                _form_id,
                _form_data_list
                );

            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string end()
        {
            string id = this.context.Request.Form.Get("id");
            string form_id = this.context.Request.Form.Get("form_id");
            string form_data_list = this.context.Request.Form.Get("form_data_list");

            List<string> required_fields = new List<string>();
            required_fields.Add(id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            int? _form_id;
            List<Dictionary<string, object>> _form_data_list;

            if (form_id == null)
            {
                _form_id = null;
                _form_data_list = null;
            }
            else
            {
                _form_id = int.Parse(form_id);
                _form_data_list = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(form_data_list);
            }

            this.logic.end(long.Parse(id), _form_id, _form_data_list);

            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string cancel()
        {
            string id = this.context.Request.Form.Get("id");

            List<string> required_fields = new List<string>();
            required_fields.Add(id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.cancel(long.Parse(id));

            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string reassign()
        {
            string id = this.context.Request.Form.Get("id");
            string new_user_id = this.context.Request.Form.Get("new_user_id");

            List<string> required_fields = new List<string>();
            required_fields.Add(id);
            required_fields.Add(new_user_id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.reassign(long.Parse(id), int.Parse(new_user_id));

            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string get()
        {
            string id = this.context.Request.QueryString.Get("id");
            string process_id = this.context.Request.QueryString.Get("process_id");
            string stage_id = this.context.Request.QueryString.Get("stage_id");
            string init_user_id = this.context.Request.QueryString.Get("init_user_id");
            string current_user_id = this.context.Request.QueryString.Get("current_user_id");
            string status = this.context.Request.QueryString.Get("status");
            string offset = this.context.Request.QueryString.Get("offset");
            string count = this.context.Request.QueryString.Get("count");

            string with_process = this.context.Request.QueryString.Get("with_process");
            string with_stage = this.context.Request.QueryString.Get("with_stage");
            string with_init_user = this.context.Request.QueryString.Get("with_init_user");
            string with_current_user = this.context.Request.QueryString.Get("with_current_user");

            Dictionary<string, object> filter = new Dictionary<string, object>();
            if (id != null) filter["id"] = long.Parse(id);
            if (process_id != null) filter["process_id"] = int.Parse(process_id);
            if (stage_id != null) filter["stage_id"] = int.Parse(stage_id);
            if (init_user_id != null) filter["init_user_id"] = int.Parse(init_user_id);
            if (current_user_id != null) filter["current_user_id"] = int.Parse(current_user_id);
            if (status != null) filter["status"] = short.Parse(status);

            List<string> fetch_with = new List<string>();
            if (with_process != null) fetch_with.Add("process");
            if (with_stage != null) fetch_with.Add("stage");
            if (with_init_user != null) fetch_with.Add("init_user");
            if (with_current_user != null) fetch_with.Add("current_user");

            int? _offset;
            int? _count;
            if (offset == null || count == null)
            {
                _offset = null;
                _count = null;
            }
            else
            {
                _offset = int.Parse(offset);
                _count = int.Parse(count);
            }

            DataTable data = this.logic.get(filter, fetch_with, _offset, _count);
            return Response.json(this.logic.Status, Model.Base.toAssociative(data), this.logic.Message);
        }

        private string getTrail()
        {
            string request_id = this.context.Request.QueryString.Get("request_id");
            string stage_id = this.context.Request.QueryString.Get("stage_id");
            string user_id = this.context.Request.QueryString.Get("user_id");
            string offset = this.context.Request.QueryString.Get("offset");
            string count = this.context.Request.QueryString.Get("count");

            string with_user = this.context.Request.QueryString.Get("with_user");
            string with_stage = this.context.Request.QueryString.Get("with_stage");

            Dictionary<string, object> filter = new Dictionary<string, object>();
            if (request_id != null) filter["request_id"] = long.Parse(request_id);
            if (stage_id != null) filter["stage_id"] = int.Parse(stage_id);
            if (user_id != null) filter["user_id"] = int.Parse(user_id);

            List<string> fetch_with = new List<string>();
            if (with_user != null) fetch_with.Add("user");
            if (with_stage != null) fetch_with.Add("stage");

            int? _offset;
            int? _count;
            if (offset == null || count == null)
            {
                _offset = null;
                _count = null;
            }
            else
            {
                _offset = int.Parse(offset);
                _count = int.Parse(count);
            }

            DataTable data = this.logic.getTrail(filter, fetch_with, _offset, _count);
            return Response.json(this.logic.Status, Model.Base.toAssociative(data), this.logic.Message);
        }

        private string getForms()
        {
            string request_id = this.context.Request.QueryString.Get("request_id");
            string request_trail_id = this.context.Request.QueryString.Get("request_trail_id");
            string form_id = this.context.Request.QueryString.Get("form_id");
            string stage_id = this.context.Request.QueryString.Get("stage_id");
            string user_id = this.context.Request.QueryString.Get("user_id");
            string status = this.context.Request.QueryString.Get("status");
            string offset = this.context.Request.QueryString.Get("offset");
            string count = this.context.Request.QueryString.Get("count");

            string with_user = this.context.Request.QueryString.Get("with_user");
            string with_form = this.context.Request.QueryString.Get("with_form");

            Dictionary<string, object> filter = new Dictionary<string, object>();
            if (request_id != null) filter["request_id"] = long.Parse(request_id);
            if (request_trail_id != null) filter["request_trail_id"] = long.Parse(request_trail_id);
            if (stage_id != null) filter["stage_id"] = int.Parse(stage_id);
            if (form_id != null) filter["form_id"] = int.Parse(form_id);
            if (status != null) filter["status"] = short.Parse(status);
            if (user_id != null) filter["user_id"] = int.Parse(user_id);

            List<string> fetch_with = new List<string>();
            if (with_user != null) fetch_with.Add("user");
            if (with_form != null) fetch_with.Add("form");

            int? _offset;
            int? _count;
            if (offset == null || count == null)
            {
                _offset = null;
                _count = null;
            }
            else
            {
                _offset = int.Parse(offset);
                _count = int.Parse(count);
            }

            DataTable data = this.logic.getForms(filter, fetch_with, _offset, _count);
            return Response.json(this.logic.Status, Model.Base.toAssociative(data), this.logic.Message);
        }

        private string getFormData()
        {
            string request_trail_id = this.context.Request.QueryString.Get("request_trail_id");

            List<string> required_fields = new List<string>();
            required_fields.Add(request_trail_id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            DataSet form_data = this.logic.getFormData(long.Parse(request_trail_id));
            Dictionary<string, IEnumerable<Dictionary<string, object>>> set = new Dictionary<string, IEnumerable<Dictionary<string, object>>>();
            if (form_data != null)
            {
                set["char"] = Model.Base.toAssociative(form_data.Tables[0]);
                set["date"] = Model.Base.toAssociative(form_data.Tables[1]);
                set["decimal"] = Model.Base.toAssociative(form_data.Tables[2]);
                set["int"] = Model.Base.toAssociative(form_data.Tables[3]);
                set["text"] = Model.Base.toAssociative(form_data.Tables[4]);
            }
            return Response.json(this.logic.Status, (form_data == null) ? null : set, this.logic.Message);
        }

        private string addComment()
        {
            string request_id = this.context.Request.Form.Get("request_id");
            string process_id = this.context.Request.Form.Get("process_id");
            string stage_id = this.context.Request.Form.Get("stage_id");
            string message = this.context.Request.Form.Get("message");

            List<string> required_fields = new List<string>();
            required_fields.Add(request_id);
            required_fields.Add(process_id);
            required_fields.Add(stage_id);
            required_fields.Add(message);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            long id = this.logic.addComment(long.Parse(request_id), int.Parse(process_id), int.Parse(stage_id), message);
            return Response.json(this.logic.Status, id, this.logic.Message);
        }

        private string getComments()
        {
            string id = this.context.Request.QueryString.Get("id");
            string process_id = this.context.Request.QueryString.Get("process_id");
            string stage_id = this.context.Request.QueryString.Get("stage_id");
            string request_id = this.context.Request.QueryString.Get("request_id");
            string user_id = this.context.Request.QueryString.Get("user_id");
            string status = this.context.Request.QueryString.Get("status");
            string offset = this.context.Request.QueryString.Get("offset");
            string count = this.context.Request.QueryString.Get("count");

            string with_stage = this.context.Request.QueryString.Get("with_stage");
            string with_user = this.context.Request.QueryString.Get("with_user");

            Dictionary<string, object> filter = new Dictionary<string, object>();
            if (id != null) filter["id"] = long.Parse(id);
            if (process_id != null) filter["process_id"] = int.Parse(process_id);
            if (stage_id != null) filter["stage_id"] = int.Parse(stage_id);
            if (request_id != null) filter["request_id"] = long.Parse(request_id);
            if (user_id != null) filter["user_id"] = int.Parse(user_id);
            if (status != null) filter["status"] = short.Parse(status);

            List<string> fetch_with = new List<string>();
            if (with_stage != null) fetch_with.Add("stage");
            if (with_user != null) fetch_with.Add("user");

            int? _offset;
            int? _count;
            if (offset == null || count == null)
            {
                _offset = null;
                _count = null;
            }
            else
            {
                _offset = int.Parse(offset);
                _count = int.Parse(count);
            }
            DataTable dt = new DataTable();
            string sql =
                @"SELECT [comment].*, [profile].firstname AS user_firstname, [profile].lastname AS user_lastname, [profile].middlename AS user_middlename, [stage].name AS stage_name FROM [comment]  INNER JOIN [profile] ON [profile].user_id = [comment].user_id   INNER JOIN stage ON stage.id = comment.stage_id  WHERE [comment].request_id='" +
                request_id + "' AND [comment].status=1 ORDER BY modified_date DESC";
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(dt);
            }

           // DataTable data = this.logic.getComments(filter, fetch_with, _offset, _count);
            return Response.json(this.logic.Status, Model.Base.toAssociative(dt), this.logic.Message);
        }
        #endregion
    }
}