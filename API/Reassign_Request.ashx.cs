﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using CaseManager.LawPavilion.Util;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for Reassign_Request
    /// </summary>
    public class Reassign_Request : IHttpHandler
    {
        private LPCMConnection objConnection;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            objConnection = new LPCMConnection();
            string json = JsonConvert.SerializeObject(GetAllRequests(), Formatting.Indented);
            context.Response.Write(json);
        }
       
        //get all request sent by init_user_id
        private dynamic GetAllRequests()
        {
           
            string query = @"
                            SELECT  [request].[id] as request_id
                                  ,request.[process_id] as process_id
                                  ,[request].[stage_id] as stage_id
                                   ,[init_user_id] as created_user
                                  ,[current_user_id] as current_user_id
                                  ,request.[title] as request_title
                                  ,[lifespan] as lifespan
                                  ,request.[description] as description
                                   ,request.[created_date] as created_date
                                  ,[remark] as remark
                                    ,[note] as  note
                                  ,[ref_no] as ref_no
	                              ,[user].id as user_id
	                              ,process.name as process
	                              ,stage.name as stage_name
								  ,branch.name as branch_name
								  ,role.name as role_name
								  ,branch.id as branch_id
								  ,concat([profile].firstname , +' ' +  [profile].lastname) as title,
                                  [role].id as role_id,
                                    CASE request.[status]
                                    WHEN 6 THEN 'completed'
                                    WHEN 5 THEN 'on-going'
                                    WHEN 4 THEN 'terminated'
                                    END  as [status_desc]
                                  FROM [lpcm].[dbo].[request]
                                  inner join [user] on [user].id = [request].init_user_id
                                  inner join [stage] on [stage].id = [request].stage_id
                                  inner join [process] on [process].id = [request].process_id
                                  inner join [branch] on [branch].id = [user].branch_id
							      inner join [profile] on [profile].user_id = [user].id
                                   inner join [role] on [role].id = [stage].role_id
								   where [request].status = 5 order by request_id desc";

            objConnection.Sql = query;
            return objConnection.GetTable;
        }
       
        
       
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}