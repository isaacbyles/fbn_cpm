﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for ValidationAPI
    /// </summary>
    public class ValidationAPI : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            var definition = new { email = "" };
            string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();

            var jsonData = JsonConvert.DeserializeAnonymousType(strJson, definition);

            var refNo = jsonData.email.Trim();

            string get = ValidateUsername(refNo);
            string json = JsonConvert.SerializeObject(get, Formatting.Indented);
            context.Response.Write(json);
        }

        public string ValidateUsername(string email)
        {
            string result = "";
            string query = @"select EMAIL from external_solicitors where EMAIL ='" + email + "'";
            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (var cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                if(rd.HasRows)
                {
                    
                   result = "Exist";
                   
                }
                else
                {
                    result = "Not Exist";
                }

            }
            return result;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}