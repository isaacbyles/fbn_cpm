﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using CaseManager.Util;
using CaseManager.Logic;
using System.Data;
using System.Web.SessionState;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for Form
    /// </summary>
    public class Form : IHttpHandler, IRequiresSessionState
    {
        private FormLogic logic = new FormLogic();
        private HttpContext context;
        static AuditLogic audit = new AuditLogic();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            Model.Base.has_error = false;
            this.context = context;
            context.Response.ContentType = Constant.JSON_CONTENT_TYPE;
            string action = context.Request.QueryString["a"];
            string response = "";
            switch (action)
            {
                case "add":
                    response = this.add();
                    break;
                case "edit":
                    response = this.edit();
                    break;
                case "change_status":
                    response = this.changeStatus();
                    break;
                case "get_controls":
                    response = this.getControls();
                    break;
                case "get":
                    response = this.get();
                    break;
                default:
                    break;
            }

            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #region Logic API
        public string add()
        {
            string name = this.context.Request.Form.Get("name");
            string description = this.context.Request.Form.Get("description");
            string control_data_list = this.context.Request.Form.Get("control_data_list");

            List<string> required_fields = new List<string>();
            required_fields.Add(name);
            required_fields.Add(description);
            required_fields.Add(control_data_list);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            IEnumerable<Dictionary<string, string>> _control_data_list = JsonConvert.DeserializeObject<IEnumerable<Dictionary<string, string>>>(control_data_list);

            object id = this.logic.add(
                name,
                description, 
                _control_data_list
                );
            return Response.json(this.logic.Status, id, this.logic.Message);
        }

        public string edit()
        {
            string form_id = this.context.Request.Form.Get("form_id");
            string name = this.context.Request.Form.Get("name");
            string description = this.context.Request.Form.Get("description");
            string control_data_list = this.context.Request.Form.Get("control_data_list");

            List<string> required_fields = new List<string>();
            required_fields.Add(form_id);
            required_fields.Add(name);
            required_fields.Add(description);
            required_fields.Add(control_data_list);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            IEnumerable<Dictionary<string, string>> _control_data_list = JsonConvert.DeserializeObject<IEnumerable<Dictionary<string, string>>>(control_data_list);

            this.logic.edit(
                int.Parse(form_id),
                name,
                description,
                _control_data_list
                );
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        public string changeStatus()
        {
            string form_id = this.context.Request.Form.Get("form_id");
            string status = this.context.Request.Form.Get("status");

            List<string> required_fields = new List<string>();
            required_fields.Add(form_id);
            required_fields.Add(status);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.changeStatus(int.Parse(form_id), short.Parse(status));
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        public string get()
        {

            string id = this.context.Request.Form.Get("id");
            string name = this.context.Request.Form.Get("name");
            string status = this.context.Request.Form.Get("status");
            string offset = this.context.Request.Form.Get("offset");
            string count = this.context.Request.Form.Get("count");

            Dictionary<string, object> filter = new Dictionary<string, object>();
            if (name != null) filter["name"] = name;
            if (id != null) filter["id"] = int.Parse(id);
            if (status != null) filter["status"] = short.Parse(status);

            int? _offset;
            int? _count;
            if (offset == null || count == null)
            {
                _offset = null;
                _count = null;
            }
            else
            {
                _offset = int.Parse(offset);
                _count = int.Parse(count);
            }

            DataTable data = this.logic.get(filter, _offset, _count);
            return Response.json(this.logic.Status, Model.Base.toAssociative(data), this.logic.Message);
        }

        public string getControls()
        {
            string form_id = this.context.Request.Form.Get("form_id");

            if (form_id == null)
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            DataTable data = this.logic.getControls(int.Parse(form_id));
            return Response.json(this.logic.Status, Model.Base.toAssociative(data), this.logic.Message);
        }
        #endregion
    }
}