﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using CaseManager.Util;
using CaseManager.Logic;
using CaseManager.LawPavilion.Util;
using System.Data;
using System.Data.SqlClient;
using System.Web.SessionState;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for RefNo
    /// </summary>
    public class RefNo : IHttpHandler
    {
        private LPCMConnection objConnection;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            objConnection = new LPCMConnection();
            string request_id = context.Request.QueryString.Get("request_id");
            string userId = context.Request.QueryString.Get("userId");
            string json = JsonConvert.SerializeObject(getRefno(request_id, userId), Formatting.Indented);
            context.Response.Write(json);
        }

        //private dynamic Handler(string pathinfo)
        //{

        //    String[] parameters = pathinfo.Split('/');
        //    if (parameters.Length != 2) { return ""; }
        //    return getRefno(parameters[1]);
        //}

        private dynamic getRefno(string requestID, string _userId)
        {
            string strSerial = "0";
            string strInitial = "";
            string strDate = "";
            string strUser = "";
            List<string> roleList = new List<string>();
            string pri_role = null;
            string sec_role = null;
            string created_date = null;

            
            try {
                string query = @"SELECT isnull(max(cast(substring(ref_no,CHARINDEX('/',ref_no)+1,8) as bigint)),0) as last_serial from request where ref_no not like 'LIT%'";
                objConnection.Sql = query;
                DataTable dt = objConnection.GetTable;
                foreach (DataRow dr in dt.Rows)
                {
                    strSerial = dr["last_serial"].ToString();
                }

                strSerial = (Convert.ToInt32(strSerial) + 1).ToString().PadLeft(8,'0');

                query = @"SELECT * FROM [lpcm].[dbo].[request_trail] 
                                 inner join [lpcm].[dbo].[request] req on req.id = request_trail.request_id
		                         inner join [profile] on [profile].user_id = request_trail.user_id
                                 where request_trail.request_id = '" + requestID + "'";
                objConnection.Sql = query;
              
                //fetch user Initial
                string _getUser = @"SELECT   [id]
                                          ,[email]
                                          ,[password]
                                          ,[branch_id]
                                          ,user_role.[role_id] as sec_role
	                                      ,[user].role_id as pri_role
                                          ,[user].[created_date] as created_date
                                          ,[user].[modified_date]
                                          ,[user].[status]
	                                      , [profile].initial as initial
                                FROM [lpcm].[dbo].[user]
                                inner join [profile] on [profile].user_id = [user].id
                                left join [user_role] on [user_role].user_id = [user].id
                                where [user].id = '" + _userId + "'";
                using (SqlConnection conn =  new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                using (SqlCommand cmd = new SqlCommand(_getUser, conn))
                {
                    conn.Open();
                    SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    if (rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            pri_role = rd["pri_role"].ToString();
                            sec_role = rd["sec_role"].ToString();
                            strInitial = rd["initial"].ToString();
                            created_date = rd["created_date"].ToString();

                            roleList.Add(sec_role);
                            roleList.Add(pri_role);
                        }
                    }

                }
                string LO = ConfigurationManager.AppSettings["LO"].ToString();
               
                if (roleList.Contains(LO.ToString()))
                {
                    string initial = strInitial; // get the initial
                    string date = created_date;

                    if (date != null) strDate = date.Split('/')[1] + "/" + date.Split('/')[2];
                    strSerial = "FBNLS/" + strSerial + "/" + initial + "/" + strDate.Split(' ')[0];
                    //insert generated serial into request table an return output
                    query = @"update request set ref_no = '" + strSerial + @"' 
                        output inserted.ref_no as new_ref where id = " + requestID;
                    objConnection.Sql = query;
                    dt = objConnection.GetTable;
                }
                return dt;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}