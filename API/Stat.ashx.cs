﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using CaseManager.Util;
using CaseManager.Logic;
using System.Data;
using System.Web.SessionState;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for Stat
    /// </summary>
    public class Stat : IHttpHandler
    {
        private StatLogic logic = new StatLogic();
        private HttpContext context;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            Model.Base.has_error = false;
            this.context = context;
            context.Response.ContentType = Constant.JSON_CONTENT_TYPE;
            string action = context.Request.QueryString["a"];
            string response = "";
            switch (action)
            {
                case "active_count":
                    response = this.activeCount();
                    break;
                default:
                    break;
            }

            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #region Logic API
        private string activeCount()
        {
            DataSet result = this.logic.getActiveCount();
            //Dictionary<string, IEnumerable<Dictionary<string, object>>> set = new Dictionary<string, IEnumerable<Dictionary<string, object>>>();
            Dictionary<string, int> set = new Dictionary<string, int>();
            if (result != null){
                set["branch"] = int.Parse(result.Tables[0].Rows[0]["count"].ToString());
                set["process"] = int.Parse(result.Tables[1].Rows[0]["count"].ToString());
                set["form"] = int.Parse(result.Tables[2].Rows[0]["count"].ToString());
                set["user"] = int.Parse(result.Tables[3].Rows[0]["count"].ToString());
            }

            return Response.json(this.logic.Status, (result == null) ? null : set, this.logic.Message);
        }
        #endregion
    }
}