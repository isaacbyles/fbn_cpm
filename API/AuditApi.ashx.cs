﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for AuditApi
    /// </summary>
    public class AuditApi : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");

            string action = context.Request.QueryString["a"];
            DataSet response = GetAll();
            switch (action)
            {
                case "getall":
                    response = this.GetAll();
                    break;
            }

            string json = JsonConvert.SerializeObject(response, Formatting.Indented);
            context.Response.Write(json);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public DataSet GetAll()
        {
            DataSet dss = new DataSet();
            string order = ConfigurationManager.AppSettings["order"];
            SqlDataAdapter rd = null;
            string query = @"select * from [lpcm].[dbo].[tbl_audit] order by [timestamp] desc";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using(SqlCommand cmd  = new SqlCommand(query, conn))
            {   
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(dss, "data");
                conn.Close();
            }

            return dss;
        }
    }
}