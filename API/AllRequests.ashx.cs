﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using CaseManager.Util;
using CaseManager.Logic;
using CaseManager.LawPavilion.Util;
using System.Data;
using System.Web.SessionState;


namespace CaseManager.API
{
    /// <summary>
    /// Summary description for AllRequests
    /// </summary>
    public class AllRequests : IHttpHandler
    {
        private LPCMConnection objConnection;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            context.Response.ContentType = "application/json";
            objConnection = new LPCMConnection();
            string json = JsonConvert.SerializeObject(Handler(context.Request.PathInfo), Formatting.Indented);
            context.Response.Write(json);
        }

        private dynamic Handler(string pathinfo)
        {

            String[] parameters = pathinfo.Split('/');
            if (parameters.Length != 6) { return null; }
            string user_id = parameters[1];
            string role_id = parameters[2];
            string branch_id = parameters[3];
            string page = parameters[4];
            string filter = parameters[5];
            return getRequests(user_id, role_id, branch_id, page, filter);
        }

        private dynamic getRequests(string user_id, string role_id, string branch_id, string page, string filter)
        {
            string strRoleName = "";
            string strOffice = "";
            string strUnit = "";
            //get user unit and branch details
            string query = @"select [user].id,[profile].middlename,branch.[type],[role].name as role_name,[role].id as role_id from 
                            [user] join [profile] on [user].id = [profile].user_id 
                            join [branch] on [user].branch_id = branch.id 
                            join [role] on [user].role_id = [role].id where [user].id = " + user_id;
            objConnection.Sql = query;
            DataTable dt = objConnection.GetTable;
            foreach (DataRow dr in dt.Rows)
            {

                strRoleName = dr["role_name"].ToString();
                strOffice = dr["type"].ToString();
                strUnit = dr["middlename"].ToString();
            }
            switch (strOffice)
            {
                case "head":
                    return getHead(user_id, role_id, strUnit);

                case "zonal":
                    return getZonal(user_id, role_id, strUnit);

                case "branch":
                    return getBranch(user_id, role_id, strUnit);

                default:
                    return null;
            }



        }


        public dynamic getHead(string user_id, string role_id, string unit)
        {
            string qry = "";
            switch (unit)
            {
                case "litigation":
                case "corporate":
                case "perfection":
                case "hub":
                    qry = @"select request.id,request.title,request.process_id as process_id, request.[status] as [status],lifespan,request.id as request_id,request.[modified_date] as [modified_date],
                            (select stage.[name] from [stage] where id = (select max(next_id) from stage_precedence where process_id = request.process_id)) as last_stage,  
                            (select [name] from process where process.id = request.process_id) as process,
                            (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = init_user_id) as created_by,
                            (select branch.name from branch where id = (select branch_id from [user] where id = init_user_id)) as branch,
                            created_date, 
                            (select id from stage where [type] = 2 and process_id = request.process_id) as start_stage_id,
                            (select form_id from stage_form where stage_id = (select id from stage where [type] = 2 and process_id = request.process_id)) as start_form_id,
                            (select name from stage where id = stage_id) as current_stage, 
                            (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = current_user_id) as [held_by],
                            CASE request.[status]
                            WHEN 6 THEN 'completed'
                            WHEN 5 THEN 'on-going'
                            WHEN 4 THEN 'terminated'
                            END  as [status_desc],
                            CASE request.process_id
                            WHEN 19 THEN 'block'
                            WHEN 24 THEN 'block'
                            ELSE 'none'
                            END as [show_gen_btn],
                            CASE request.process_id
                            WHEN 13 THEN 'block'
                            WHEN 25 THEN 'block'
                            ELSE 'none'
                            END as [show_term_btn],
                            isnull(ref_no,'') as ref_no from request where request.status = 6 order by modified_date desc";
                    break;

                default:
                    qry = @"select request.id,request.title,request.process_id as process_id, request.[status] as [status],lifespan,request.id as request_id,request.[modified_date] as [modified_date],
                            (select stage.[name] from [stage] where id = (select max(next_id) from stage_precedence where process_id = request.process_id)) as last_stage,  
                            (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = init_user_id) as created_by,
                            (select branch.name from branch where id = (select branch_id from [user] where id = init_user_id)) as branch,
                            created_date, 
                            (select id from stage where [type] = 2 and process_id = request.process_id) as start_stage_id,
                            (select [name] from process where process.id = request.process_id) as process,
                            (select form_id from stage_form where stage_id = (select id from stage where [type] = 2 and process_id = request.process_id)) as start_form_id,
                            (select name from stage where id = stage_id) as current_stage, 
                            (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = current_user_id) as [current_user],
                            CASE request.[status]
                            WHEN 6 THEN 'completed'
                            WHEN 5 THEN 'on-going'
                            WHEN 4 THEN 'terminated'
                            END  as [status],
                            CASE request.process_id
                            WHEN 19 THEN 'block'
                            WHEN 24 THEN 'block'
                            ELSE 'none'
                            END as [show_gen_btn],
                            CASE request.process_id
                            WHEN 13 THEN 'block'
                            WHEN 25 THEN 'block'
                            ELSE 'none'
                            END as [show_term_btn],
                            isnull(ref_no,'') as ref_no from request 
                            where request.id in (select distinct request_id from request_trail join [user] on 
                            request_trail.user_id = [user].id where 
                            role_id =" + role_id + " and request.status = 6 and branch_id =(select branch_id from [user] where id = " + user_id + ")) order by modified_date desc";
                    break;
            }
            if (qry == "")
            {
                return null;
            }
            else
            {
                objConnection.Sql = qry;
                return objConnection.GetTable;
            }

        }

        public dynamic getZonal(string user_id, string role_id, string unit)
        {
            //get region of user
            string region = "";
            objConnection.Sql = "select region from branch where id = (select branch_id from [user] where id = " + user_id + ")";
            DataTable dtReg = objConnection.GetTable;
            foreach (DataRow dr in dtReg.Rows)
            {
                region = dr["region"].ToString();
            }
            if (region == null || region == "") { return null; }
            string qry = @"select request.id,request.title,request.process_id as process_id, request.[status] as [status],lifespan,request.id as request_id,request.[modified_date] as [modified_date],
                            (select stage.[name] from [stage] where id = (select max(next_id) from stage_precedence where process_id = request.process_id)) as last_stage,  
                            (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = init_user_id) as created_by,
                            (select branch.name from branch where id = (select branch_id from [user] where id = init_user_id)) as branch,
                            created_date, 
                            (select id from stage where [type] = 2 and process_id = request.process_id) as start_stage_id,
                            (select form_id from stage_form where stage_id = (select id from stage where [type] = 2 and process_id = request.process_id)) as start_form_id,
                            (select name from stage where id = stage_id) as current_stage, 
                            (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = current_user_id) as [current_user],
                            (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = current_user_id) as [held_by],
                           CASE request.[status]
                            WHEN 6 THEN 'completed'
                            WHEN 5 THEN 'on-going'
                            WHEN 4 THEN 'terminated'
                            END  as [status_desc],
                            CASE request.process_id
                            WHEN 19 THEN 'block'
                            WHEN 24 THEN 'block'
                            ELSE 'none'
                            END as [show_gen_btn],
                            CASE request.process_id
                            WHEN 13 THEN 'block'
                            WHEN 25 THEN 'block'
                            ELSE 'none'
                            END as [show_term_btn],
                            isnull(ref_no,'') as ref_no from request 
                            where request.id in (select distinct request_id from request_trail join [user] on 
                            request_trail.user_id = [user].id where 
                            role_id =" + role_id + " and request.status = 6 and branch_id in (select branch_id from [branch] where region = '" + region + "')) order by modified_date desc";
            objConnection.Sql = qry;
            return objConnection.GetTable;

        }

        public dynamic getBranch(string user_id, string role_id, string unit)
        {
            string qry = @"select request.id,request.title,request.process_id as process_id, request.[status] as [status],lifespan,request.id as request_id,request.[modified_date] as [modified_date],
                            (select stage.[name] from [stage] where id = (select max(next_id) from stage_precedence where process_id = request.process_id)) as last_stage,  
                            (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = init_user_id) as created_by,
                            (select branch.name from branch where id = (select branch_id from [user] where id = init_user_id)) as branch,
                            created_date, 
                            (select id from stage where [type] = 2 and process_id = request.process_id) as start_stage_id,
                            (select [name] from process where process.id = request.process_id) as process,
                            (select form_id from stage_form where stage_id = (select id from stage where [type] = 2 and process_id = request.process_id)) as start_form_id,
                            (select name from stage where id = stage_id) as current_stage, 
                            (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = current_user_id) as [current_user],
                            (select concat(firstname,' ',lastname) from [profile] where [profile].user_id = current_user_id) as [held_by],
                            CASE request.[status]
                            WHEN 6 THEN 'completed'
                            WHEN 5 THEN 'on-going'
                            WHEN 4 THEN 'terminated'
                            END  as [status_desc],
                            CASE request.process_id
                            WHEN 19 THEN 'block'
                            WHEN 24 THEN 'block'
                            ELSE 'none'
                            END as [show_gen_btn],
                            CASE request.process_id
                            WHEN 13 THEN 'block'
                            WHEN 25 THEN 'block'
                            ELSE 'none'
                            END as [show_term_btn],
                            isnull(ref_no,'') as ref_no from request 
                            where request.id in (select distinct request_id from request_form where user_id in (select id from [user] where branch_id = (select branch_id from [user] where id = " + user_id + "))) order by modified_date desc";
            objConnection.Sql = qry;
            return objConnection.GetTable;
        }



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}