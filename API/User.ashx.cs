﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using CaseManager.Util;
using CaseManager.Logic;
using System.Data;
using System.Web.SessionState;
using CaseManager.LawPavilion.Util;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for User
    /// </summary>
    public class User : IHttpHandler, IRequiresSessionState
    {
        private UserLogic logic = new UserLogic();
        private HttpContext context;
        private LPCMConnection objConnection;
        static AuditLogic audit = new AuditLogic();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            objConnection = new LPCMConnection();
            Model.Base.has_error = false;
            this.context = context;
            context.Response.ContentType = Constant.JSON_CONTENT_TYPE;
            string action = context.Request.QueryString["a"];
            string response = "";
            switch (action)
            {
                case "add":
                    response = this.add();
                    break;
                case "change_password":
                    response = this.changePassword();
                    break;
                case "edit_profile":
                    response = this.editProfile();
                    break;
                case "change_priviledge":
                    response = this.changePriviledge();
                    break;
                case "change_status":
                    response = this.changeStatus();
                    break;
                case "reset_password":
                    response = this.resetPassword();
                    break;
                case "login":
                    response = this.login();
                    break;
                case "logout":
                    response = this.logout();
                    break;
                case "get":
                    response = this.get();
                    break;
                default:
                    break;
            }

            context.Response.Write(response);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #region Logic API
        private static int[] StringToIntArray(string myNumbers)
        {
            List<int> myIntegers = new List<int>();
            Array.ForEach(myNumbers.Split(",".ToCharArray()), s =>
            {
                int currentInt;
                if (Int32.TryParse(s, out currentInt))
                    myIntegers.Add(currentInt);
            });
            return myIntegers.ToArray();
        }

        private string add()
        {
            string email = this.context.Request.Form.Get("email");
            string password = this.context.Request.Form.Get("password");
            string branch_id = this.context.Request.Form.Get("branch_id");
            string role_id = this.context.Request.Form.Get("role_id");

            string firstname = this.context.Request.Form.Get("firstname");
            string lastname = this.context.Request.Form.Get("lastname");
            string middlename = this.context.Request.Form.Get("middlename");
            string telephone = this.context.Request.Form.Get("telephone");
            string initial = this.context.Request.Form.Get("initial");
            
            List<string> required_fields = new List<string>();
            required_fields.Add(email);
            required_fields.Add(password);
            required_fields.Add(branch_id);
            required_fields.Add(role_id);
            required_fields.Add(firstname);
            required_fields.Add(lastname);
            required_fields.Add(initial);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }
            //split role_id in array of integer
            object id = this.logic.add(
                email,
                password,
                int.Parse(branch_id),
                short.Parse(role_id),
                firstname,
                lastname,
                middlename,
                telephone, initial
                );

            return Response.json(this.logic.Status, id, this.logic.Message);
        }

        private string changePassword()
        {
            string oldPassword = this.context.Request.Form.Get("old_password");
            string newPassword = this.context.Request.Form.Get("new_password");

            List<string> required_fields = new List<string>();
            required_fields.Add(oldPassword);
            required_fields.Add(newPassword);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.changePassword(oldPassword, newPassword);
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string editProfile()
        {
            string user_id = this.context.Request.Form.Get("user_id");
            string firstname = this.context.Request.Form.Get("firstname");
            string lastname = this.context.Request.Form.Get("lastname");
            string middlename = this.context.Request.Form.Get("middlename");
            string telephone = this.context.Request.Form.Get("telephone");

            List<string> required_fields = new List<string>();
            required_fields.Add(user_id);
            required_fields.Add(firstname);
            required_fields.Add(lastname);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.editProfile(int.Parse(user_id), firstname, lastname, middlename, telephone);
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string changePriviledge()
        {
            string user_id = this.context.Request.Form.Get("user_id");
            string branch_id = this.context.Request.Form.Get("branch_id");
            string role_id = this.context.Request.Form.Get("role_id");

            List<string> required_fields = new List<string>();
            required_fields.Add(user_id);
            required_fields.Add(branch_id);
            required_fields.Add(role_id);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.changePriviledge(int.Parse(user_id), int.Parse(branch_id), short.Parse(role_id));
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string changeStatus()
        {
            string user_id = this.context.Request.Form.Get("user_id");
            string status = this.context.Request.Form.Get("status");

            List<string> required_fields = new List<string>();
            required_fields.Add(user_id);
            required_fields.Add(status);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.changeStatus(int.Parse(user_id), short.Parse(status));
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string resetPassword()
        {
            string user_id = this.context.Request.Form.Get("user_id");
            string password = this.context.Request.Form.Get("password");

            List<string> required_fields = new List<string>();
            required_fields.Add(user_id);
            required_fields.Add(password);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            this.logic.resetPassword(int.Parse(user_id), password);
            return Response.json(this.logic.Status, null, this.logic.Message);
        }

        private string login()
        {
            string email = this.context.Request.Form.Get("email");
            string password = this.context.Request.Form.Get("password");

            List<string> required_fields = new List<string>();
            required_fields.Add(email);
            required_fields.Add(password);

            if (required_fields.Contains(null))
            {
                return Response.json(Logic.Base.STATUS_ERROR, ResponseMessage.MISSING_REQUIRED_FIELDS);
            }

            DataSet loginData = this.logic.login(email, password);

            string[] table_labels = {"user", "process", "start_stage"};
            Dictionary<string, IEnumerable<Dictionary<string, object>>> set = new Dictionary<string, IEnumerable<Dictionary<string, object>>>();
            if (loginData != null)
            {
                set["user"] = Model.Base.toAssociative(loginData.Tables["user"]);
                set["process"] = Model.Base.toAssociative(loginData.Tables["process"]);
                set["start_stage"] = Model.Base.toAssociative(loginData.Tables["start_stage"]);
            }
            
            return Response.json(this.logic.Status, (loginData == null) ? null : set, this.logic.Message);
        }

        private string logout()
        {
            this.logic.logout();
            return Response.json(Logic.Base.STATUS_SUCCESS, null, "");
        }

        private string get()
        {
            string email = this.context.Request.Form.Get("email");
            string role_id = this.context.Request.Form.Get("role_id");
            string branch_id = this.context.Request.Form.Get("branch_id");
            string process_id = this.context.Request.Form.Get("process_id");
            string id = this.context.Request.Form.Get("id");
            string status = this.context.Request.Form.Get("status");
            string offset = this.context.Request.Form.Get("offset");
            string count = this.context.Request.Form.Get("count");

            Dictionary<string, object> filter = new Dictionary<string, object>();
            if (email != null) filter["email"] = email;
            if (role_id != null) filter["role_id"] = short.Parse(role_id);
           // if (branch_id != null) filter["branch_id"] = int.Parse(branch_id);
            if (id != null) filter["id"] = int.Parse(id);
            if (status != null) filter["status"] = short.Parse(status);

            int? _offset;
            int? _count;
            if (offset == null || count == null)
            {
                _offset = null;
                _count = null;
            }
            else
            {
                _offset = int.Parse(offset);
                _count = int.Parse(count);
            }

            DataSet data = this.logic.get(filter, _offset, _count);
           

            return Response.json(this.logic.Status, Model.Base.toAssociative(data), this.logic.Message);
        }

        private string getProcessUnit(string process_id)
        {
            DataTable dt = new DataTable();
            if (process_id == null)
            {
            }
            else
            {
                objConnection.Sql = "select report_unit from report where process_id=" + process_id;
                dt = objConnection.GetTable;

            }
            return dt.Rows[0]["report_unit"].ToString();
        }

        #endregion
    }
}