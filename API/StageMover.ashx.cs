﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CaseManager.LawPavilion.Util;
using Newtonsoft.Json;

namespace CaseManager.API
{
    /// <summary>
    /// Summary description for StageMover
    /// </summary>
    public class StageMover : IHttpHandler
    {
        private LPCMConnection objConnection;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            context.Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            context.Response.AppendHeader("Pragma", "no-cache");
            context.Response.AppendHeader("Expires", "-1");


            objConnection = new LPCMConnection();
            context.Response.ContentType = "application/json";
            string action = context.Request.QueryString["a"];
            string stage_id = context.Request.QueryString.Get("stage_id");
            string next_stage_id = context.Request.QueryString.Get("next_stage_id");
            string nextstage_id = context.Request.QueryString.Get("nextstage_id");
            string process_id = context.Request.QueryString.Get("process_id");
            string role_id = context.Request.QueryString.Get("role_id");
            string branch_id = context.Request.QueryString.Get("branch_id");
            string request_id = context.Request.QueryString.Get("request");
            string user = context.Request.QueryString.Get("user");
            DataTable response = null;

            switch (action)
            {
                case "getBranch":
                    response = this.getListofBranch();
                    break;
                case "getOtherStage":
                    response = this.GetOthersStage(stage_id);
                    break;
                case "getUserRole":
                    response = this.GetUserRole(nextstage_id, process_id);
                    break;
                case "save":
                    response = this.Save(nextstage_id, user, request_id);
                    break;

            }

            string json = JsonConvert.SerializeObject(response, Formatting.Indented);
            context.Response.Write(json);
        }

        //get other stage both forward / backward
        public DataTable GetOthersStage(string current_stage_id)
        {
            DataSet  ds = new DataSet();
            string sql = @"SELECT [role_id]
                              ,[type]
                              ,[name]
                              ,[description]
                              ,[created_date]
                              ,[modified_date],stage_precedence.current_id as current_id, stage_precedence.process_id as process_id, stage_precedence.next_id as next_id
                          FROM [lpcm].[dbo].[stage]
                          inner join [stage_precedence] on [stage_precedence].next_id = [stage].id
                          where stage_precedence.current_id = '" + current_stage_id + "'";

            using(SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                con.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "OtherStage");
            }
            return ds.Tables[0];
        }

        public string GetNextStageRole(string stage_id)
        {
            string role = "";
            string sql = "";
            sql = @"SELECT role_id FROM stage WHERE id ='" + stage_id + "'";
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        role = rd.GetByte(0).ToString();
                    }
                }
            }
            return role;
        }

        //get users role
        public DataTable GetUserRole(string next_stage_id, string process_id)
        {
            DataSet ds = new DataSet();
            string process_unit = getProcessUnit(process_id);
            string role_id = GetNextStageRole(next_stage_id);
            string query = null;
            if (role_id == "15")
            {
               
                query = @" SELECT DISTINCT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  ,[role_id]
                                 ,[branch].region
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
                                ,role.name as role_name
	                           ,role.id as role_id
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = [user].branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
							  where role_id = '" + role_id + "' and (profile.middlename='hub' or profile.middlename = '" + process_unit +
                        "') or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '15'))>0";
            }
            else if (role_id == "25")
            {
                query = @" SELECT DISTINCT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  ,[role_id]
                                 ,[branch].region
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
	                           ,role.name as role_name
                                ,role.id as role_id
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = [user].branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
							  inner join [report] on [report].report_unit = profile.middlename
							  where role_id = '" + role_id + "' and profile.middlename='" + process_unit + "' or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '" + role_id + "')) > 0";
            }
            else if (role_id == "24")
            {
                query = @" SELECT DISTINCT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  ,[role_id]
                                 ,[branch].region
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
	                           ,role.name as role_name
                                ,role.id as role_id
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = [user].branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
							  inner join [report] on [report].report_unit = profile.middlename
							  where role_id = '" + role_id + "' and profile.middlename='" + process_unit + "' or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '" + role_id + "')) > 0";
            }
            else if (role_id == "9")
            {
                query = @" SELECT DISTINCT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  ,[role_id]
                                 ,[branch].region
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
	                           ,role.name as role_name
                                ,role.id as role_id
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = [user].branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
							  where role_id = '" + role_id +
                        "'  or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '" +
                        role_id + "')) > 0";
            }
            else if (role_id == "11")
            {
                query = @"SELECT DISTINCT  [user].[id]
                                        ,[user].[email]
                                        ,[user].[password]
                                        ,[branch_id]
                                        ,[user].[role_id]
                                        ,[created_date]
                                        ,[modified_date]
                                        ,[status]
	                                    ,FIRM,
	                          CASE [user].role_id
                            WHEN 11 THEN 'External Solicitor'
                          
                            END  as [role_name] 
                                        FROM [lpcm].[dbo].[user]
                                        inner join [external_solicitors] on [external_solicitors].id = [user].id
                                        where [external_solicitors].role_id = '" +
                        ConfigurationManager.AppSettings["EXT"] + "'";
            }
            else
            {
                query = @" SELECT DISTINCT [user].[id]
                                  ,[email]
                                  ,[password]
                                  ,[branch_id]
                                  ,[role_id]
                                 ,[branch].region
	                              ,[firstname]
	                              ,[lastname]
	                             ,[firstname] + ' ' + [lastname] AS title
	                           ,role.id as role_id,
                                role.name as role_name
                              FROM [lpcm].[dbo].[user]
                              inner join branch on branch.id = [user].branch_id
                              inner join [role] on [role].id = [user].role_id
                              inner join [profile] on [profile].user_id = [user].id
							 	 where role_id = '" + role_id + "' or ((select count(*) from user_role where user_role.user_id = [user].id and user_role.role_id = '" + role_id + "')) > 0";
            }
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds, "UserRole");
            }
            return ds.Tables[0];

        }

        private string getProcessUnit(string process_id)
        {
            objConnection.Sql = "select report_unit from report where process_id=" + process_id;
            DataTable dt = objConnection.GetTable;
            return dt.Rows[0]["report_unit"].ToString();
        }
        private string getUserRegion(string branch_id)
        {
            objConnection.Sql = "select region from [branch] where id=" + branch_id;
            DataTable dt = objConnection.GetTable;
            return dt.Rows[0]["region"].ToString();
        }
        private DataTable getListofBranch()
        {
            DataSet ds = new DataSet();
            string sql = @"SELECT [id], [name] from branch";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataAdapter rd = new SqlDataAdapter(cmd);
                rd.Fill(ds);
                conn.Close();
            }

            return ds.Tables[0];
        }

        private DataTable Save(string _nextstage_id, string _user, string _request_id)
        {
            int i = 0;
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataColumn Name = new DataColumn("Name", typeof(string));
            string user_role = getRoleByStage_id(_nextstage_id);
            dt.Columns.Add(Name);
            DataRow dr = dt.NewRow();

            string sql = @"SELECT  [id]
                          ,[process_id]
                          ,[stage_id]
                          ,[init_user_id]
                          ,[current_user_id]
                          ,[title]
                          ,[lifespan]
                          ,[created_date]
                          ,[modified_date]
                          ,[status]
                          ,[description]
                          ,[remark]
                          ,[note]
                          ,[ref_no]
                      FROM [lpcm].[dbo].[request]
                      where id = '" + _request_id + "'";
            string update = @"UPDATE [lpcm].[dbo].[request] SET [current_user_id] = '" + _user + "', [stage_id] = '" +
                            _nextstage_id + "', user_role ='" + user_role + "' WHERE id ='" + _request_id + "'";
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rdd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rdd.HasRows)
                {
                    i = 1;
                }
                conn.Close();
            }
            if (i == 1)
            {
                using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                using (SqlCommand cmd2 = new SqlCommand(update, conn2))
                {
                    conn2.Open();
                    int ii = cmd2.ExecuteNonQuery();
                    if (ii == 1)
                    {
                        dr["Name"] = "success";
                    }
                    else
                    {
                        dr["Name"] = "Failed";
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dr.Table;
        }

        private string getRoleByStage_id(string stage_id)
        {
            DataTable dt = new DataTable();
            if (stage_id == null)
            {
            }
            else
            {
                objConnection.Sql = "SELECT [role_id] FROM [stage] where id='" + stage_id + "'";
                dt = objConnection.GetTable;

            }
            return dt.Rows[0]["role_id"].ToString();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}