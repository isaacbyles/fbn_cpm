﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Web;
using System.Web.Services;
using CaseManager.HttpServerTest.Handler;
using CaseManager.HttpServerTest.Model;
using CaseManager.Util;
using HttpServer.Sessions;
using HttpServerTest.Handler;
using HttpServerTest.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Form = CaseManager.API.Form;
using PasswordGen = HttpServerTest.Handler.PasswordGen;
using CaseManager.DAL;
using CaseManager.Model;

namespace CaseManager.EXT
{
    /// <summary>
    /// Summary description for ExternalSolicitorWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

    public class ExternalSolicitorWebService : System.Web.Services.WebService
    {
        private String json = null;
        private String json_result = null;
        private byte[] buffer;
        private UserProfile profile = null;
        private LoginHandle log = new LoginHandle();
        private MemorySessionStore mem = new MemorySessionStore();
        private IHttpSession session;

        private SessionHandler sess = new SessionHandler();


        [WebMethod(EnableSession = true)]
        public string Login(string email, string password)
        {
            Session.Clear();
            mem.ExpireTime = 60;
            mem.Cleanup();
            buffer = System.Text.Encoding.UTF8.GetBytes("");
            //  Session["user_id"] = "";

            if (email != null && password != null)
            {
                HttpServerTest.Logger.Logger.writelog("Login with email: " + email + DateTime.Now);
                String encryptPass = PasswordGen.generate(password);

                DataTable tab = log.userValidity(email);
                HttpServerTest.Logger.Logger.writelog("Data: " + tab + DateTime.Now);
                String validEmail = null;
                String validPass = null;
                String name = null;
                int count = 0;
                foreach (DataRow row in tab.Rows)
                {
                    validEmail = row["email"].ToString().ToLower();
                    count = Convert.ToInt32(row["loginattempt"]);
                    validPass = row["password"].ToString();
                    name = row["FirstName"].ToString() + " " + row["LastName"].ToString();

                }
                if (email.ToLower().Equals(validEmail) && validEmail != null)
                {

                    if (count < 3)
                    {
                        if (encryptPass.Equals(validPass))
                        {

                            json = log.login(email, encryptPass, Session.SessionID);
                            profile = log.profile;

                            HttpContext.Current.Session["user_id"] = profile.User_id;
                            HttpContext.Current.Session["SessionID"] = Session.SessionID;


                            buffer = Encoding.UTF8.GetBytes(json);
                            log.updateAuditTrail(name, "Successful Login ", "Login");
                            log.externalUpdate(0, "valid", email);
                            HttpServerTest.Logger.Logger.writelog("Login with " + email + " successful");

                        }
                        else
                        {
                            json = "[{\"loginDetails\":[{\"Login_status\" :\"unsuccesful\"}]}]";
                            buffer = Encoding.UTF8.GetBytes(json);
                            count++;
                            String status = null;
                            if (count >= 3)
                            {
                                count = 3;
                                status = "blocked";
                                log.updateAuditTrail(name, "Unsuccessful Login: User Blocked", "Login");
                                HttpServerTest.Logger.Logger.writelog("Login from " + email + "has been blocked");
                            }
                            else
                            {
                                status = "valid";
                                log.updateAuditTrail(name, "Unsuccessful Login: Wrong Password", "Login");
                            }
                            log.externalUpdate(count, status, email);

                        }
                    }
                    else if (count == 3 || count > 3)
                    {
                        json = "[{\"loginDetails\":[{\"Login_status\" :\"blocked\"}]}]";
                        buffer = Encoding.UTF8.GetBytes(json);
                        log.updateAuditTrail(name,
                            "Unsuccessful Login: User Has been blocked (<b>" + email + "</b>)", "Login");
                        HttpServerTest.Logger.Logger.writelog("Login from " + email + "has been blocked");
                    }

                }
                else
                {
                    json = "[{\"loginDetails\":[{\"Login_status\" :\"unsuccesful\"}]}]";
                    buffer = Encoding.UTF8.GetBytes(json);
                    log.updateAuditTrail("", "Unsuccessful Login: Wrong Email(<b>" + email + "</b>)", "Login");
                }
            }
            else
            {
                json = "[{\"loginDetails\":[{\"Login_status\" :\"unsuccesful\"}]}]";
                buffer = Encoding.UTF8.GetBytes(json);
            }
            HttpServerTest.Logger.Logger.writelog("Data Response: " + json + DateTime.Now);
            return json;
        }

        [WebMethod(EnableSession = true)]
        public String Requests(string session_id, string process_id, string user_id)
        {
            HttpServerTest.Logger.Logger.writelog("111111/requests service was called with process_id: " + process_id +
                                                  " and session_id:" + session_id + " " + user_id);

            HttpServerTest.Logger.Logger.writelog("22222/requests service was called with process_id: " + process_id +
                                                  " and session_id:" + session_id + " " + user_id);

            HttpServerTest.Logger.Logger.writelog("session successfully loaded in /request service");
            //  String user_id = getSessionUser;
            HandleRequests requestList = new HandleRequests();
            HttpServerTest.Logger.Logger.writelog("Fetching Request with the process_id: " + process_id);
            String reqs = requestList.getList(user_id, process_id);
            json = reqs;
            buffer = Encoding.UTF8.GetBytes(reqs);



            return json; //sending to php
        }

        [WebMethod(EnableSession = true)]
        public String Stages(string session_id, string request_id, string user_id)
        {

            HttpServerTest.Logger.Logger.writelog("/stage service was called with Request_id: " + request_id +
                                                  " and session_id:" +
                                                  session_id + " " + user_id);


            HttpServerTest.Logger.Logger.writelog("Session Successfully Loaded For /stage service");

            FormValueLabel value = new FormValueLabel();
            HttpServerTest.Logger.Logger.writelog("Fetching RequestTrail with the request_id: " + request_id);
            String reqs = value.valueLabel(request_id, user_id);
            json = reqs;
            HttpServerTest.Logger.Logger.writelog("Encoding Data To Buffer");
            return json;
        }

        [WebMethod(EnableSession = true)]
        public String Calender(string session_id, string user_id)
        {

            HttpServerTest.Logger.Logger.writelog("/calender service was called for external Solicitor" + " " +
                                                  session_id);

            HttpServerTest.Logger.Logger.writelog("/USER ID =" + " " + user_id);

            ExternalSolicitorCalender sol = new ExternalSolicitorCalender();
            HttpServerTest.Logger.Logger.writelog("Getting Data to populate the the calendar for external solicitor");
            HttpServerTest.Logger.Logger.writelog("Serializing data recieved from calendar to son");
            json = JsonConvert.SerializeObject(sol.getCalendar(user_id), Formatting.Indented);
            HttpServerTest.Logger.Logger.writelog("JSON RESULT" + " " + json);



            return json;
        }

        [WebMethod(EnableSession = true)]
        public String save(String json)
        {

            RequestJson requestJson = new RequestJson();
            HttpServerTest.Logger.Logger.writelog("Getting the request body from the client" + " " + json);
            requestJson = JsonConvert.DeserializeObject<RequestJson>(json);

            String request_id = requestJson.request_id;
            String user_id = requestJson.user_id;
            String next_stage_id = requestJson.next_stage_id;
            String next_user_id = requestJson.next_user_id;
            List<attachmentsInnerJson> attachments = requestJson.attachments;


            //byte[] byteArray = requestJson.file;
            //String extension = requestJson.ext;


            String form_id = GetFormId(next_stage_id).ToString();

            List<RequestInnerJson> form_data = requestJson.form_Data;

            var i = 1;
            foreach (var data in form_data)
            {

                HttpServerTest.Logger.Logger.writelog("Form Data " + i + " with control_id:" + data.control_id +
                                                      " and Value:" +
                                                      data.value + " will be saved");
                i++;
            }
            HttpServerTest.Logger.Logger.writelog("break");
            HttpServerTest.Logger.Logger.writelog("break");
            TreatRequest treat = new TreatRequest(request_id, user_id, next_stage_id, next_user_id, form_id,
                form_data);

            bool resp = treat.RequestForm();

            HttpServerTest.Logger.Logger.writelog("break");
            HttpServerTest.Logger.Logger.writelog("Result:" + " " + resp);
            string result = "";
            Dictionary<String, String> respp = new Dictionary<string, string>();

            if (resp == true)
            {


                #region

                //try to decompile to file

                foreach (var file in attachments)
                {

                    HttpServerTest.Logger.Logger.writelog("Uploaded File :" + file.fileExtention);
                    byte[] byteArray = file.filecontent;

                    MemoryStream fs = new MemoryStream(byteArray);
                    BinaryReader br = new BinaryReader(fs);
                    byte[] bytes = br.ReadBytes((Int32)fs.Length);

                    //Save the Byte Array as File.
                    //Path.Combine()
                    string filePath = "~/resources/" + DateTime.Now.ToLongTimeString().Replace(':', '-') + file.fileExtention;
                    File.WriteAllBytes(Server.MapPath(filePath), bytes);
                    //save the data to database


                    HttpServerTest.Logger.Logger.writelog("BREAK");

                }

                respp.Add("statusCode", "201");
                HttpServerTest.Logger.Logger.writelog("Success REP " + respp.Values);
                result = JsonConvert.SerializeObject(respp, Formatting.Indented);
                return result;
                #endregion
            }
            else
            {

                respp.Add("statusCode", "404");
                HttpServerTest.Logger.Logger.writelog("Error REP " + respp.Values);
                result = JsonConvert.SerializeObject(respp, Formatting.Indented);
                return result;
            }

        }


        [WebMethod(EnableSession = true)]
        public String Pending(string session_id, string user_id)
        {
            if (HttpContext.Current.Session != null)
            {

                LoginPendingRequest pReq = new LoginPendingRequest(user_id);

                dynamic collectionWrapper = new
                {

                    PendingRequest = pReq.pendingRequest()

                };

                json = JsonConvert.SerializeObject(collectionWrapper, Formatting.Indented);
                buffer = Encoding.UTF8.GetBytes(json);
            }
            else
            {
                buffer =
                    Encoding.UTF8.GetBytes(
                        "[{\"loginDetails\":[{\"Login_status\" :\"Session expired\"}]}]");
            }

            return json;

        }

        [WebMethod(EnableSession = true)]
        public void Logout(string session_id)
        {
            Session.Clear();
            Session.Abandon();
        }


        private int GetFormId(string stage_id)
        {
            int form_id = 0;
            string sql = @"SELECT form_id FROM [stage_form] WHERE stage_id = @stage_id";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand())
            {
                conn.Open();

                cmd.CommandText = sql;
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@stage_id", stage_id);
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {

                    while (rd.Read())
                    {
                        form_id = (int)rd["form_id"];
                    }
                }
                conn.Close();
                return form_id;
            }
        }

        [WebMethod(EnableSession = true)]
        public String UpdatePassword(string user_id, string newPassword, string oldPassword)
        {
            Dictionary<String, String> respp = new Dictionary<string, string>();
            string result = checkOldPassword(user_id, oldPassword, newPassword);
            if (result == PasswordContant.no_exist.ToString())
            {
                respp.Add("statusCode", HttpStatusCode.NotFound.ToString());
                result = JsonConvert.SerializeObject(respp, Formatting.Indented);
                return result;

            }
            else if (result == PasswordContant.exist.ToString())
            {
                newPassword = PasswordGen.generate(newPassword);


                string sql = @"UPDATE external_solicitors set password ='" + newPassword + "' WHERE id='" + user_id + "'";
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        int i = cmd.ExecuteNonQuery();
                        if (i == 1)
                        {



                            string sql2 = @"UPDATE [user] set [password] ='" + newPassword +
                                          "' WHERE id='" + int.Parse(user_id) + "'";
                            using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                            using (SqlCommand cmd2 = new SqlCommand(sql2, conn2))
                            {
                                conn2.Open();
                                int iii = cmd2.ExecuteNonQuery();
                                if (iii == 1)
                                {
                                    respp.Add("statusCode", HttpStatusCode.Created.ToString());
                                    result = JsonConvert.SerializeObject(respp, Formatting.Indented);
                                }
                                else
                                {

                                    respp.Add("statusCode", HttpStatusCode.InternalServerError.ToString());
                                    result = JsonConvert.SerializeObject(respp, Formatting.Indented);

                                }

                                conn2.Close();

                            }
                        }
                    }

                    conn.Close();
                }
                return result;

            }
            else
            {
                respp.Add("statusCode", HttpStatusCode.NotImplemented.ToString());
                result = JsonConvert.SerializeObject(respp, Formatting.Indented);
                return result;
            }

            #region
            //Dictionary<String, String> respp = new Dictionary<string, string>();
            ////check if the old password exist



            //#region


            //#endregion
            //string sql = @"UPDATE external_solicitors set password ='" + PasswordGen.generate(newPassword) + "' WHERE id='" + user_id + "'";
            //using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            //{
            //    conn.Open();
            //    using (SqlCommand cmd = new SqlCommand(sql, conn))
            //    {
            //        int i = cmd.ExecuteNonQuery();
            //        if (i == 1)
            //        {

            //            //update user table
            //            string sql2 = @"UPDATE user set password ='" + newPassword + "' WHERE id='" + user_id + "'";
            //            using (SqlConnection conn2 = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            //            using (SqlCommand cmd2 = new SqlCommand(sql2, conn2))
            //            {
            //                conn2.Open();
            //                int iii = cmd2.ExecuteNonQuery();
            //                if (iii == 1)
            //                {
            //                    respp.Add("statusCode", HttpStatusCode.OK.ToString());
            //                    result = JsonConvert.SerializeObject(respp, Formatting.Indented);
            //                    return result;
            //                }
            //                else
            //                {
            //                    respp.Add("statusCode", HttpStatusCode.BadRequest.ToString());
            //                    result = JsonConvert.SerializeObject(respp, Formatting.Indented);
            //                    return result;
            //                }
            //            }

            //        }
            //        else
            //        {
            //            respp.Add("statusCode", HttpStatusCode.BadRequest.ToString());
            //            result = JsonConvert.SerializeObject(respp, Formatting.Indented);
            //            return result;
            //        }

            //    }
            //}
            #endregion

        }

        private string checkOldPassword(string user_id, string old, string newPassowrd)
        {
            //method check for old password
            string convertedPassword = PasswordGen.generate(old);


            string constr = ConfigurationManager.AppSettings["conn"];
            lpcmDataContext _ctx = new lpcmDataContext(constr);
            var query = (from k in _ctx.users
                         where k.password.Equals(convertedPassword) && k.id.Equals(int.Parse(user_id))
                         select k).FirstOrDefault();
            if (query == null)
            {
                return PasswordContant.no_exist.ToString();
            }
            else
            {
                query.password = PasswordGen.generate(newPassowrd);
                try
                {

                    //var query2 = (from k in _ctx.external_solicitors
                    //              where k.password.Equals(convertedPassword)
                    //              select k).FirstOrDefault();
                    //query2.password = PasswordGen.generate(newPassowrd);
                    //_ctx.SubmitChanges();
                    return PasswordContant.
                        exist.ToString();
                }
                catch (SqlException ex)
                {
                    return ex.ToString();
                }

            }


        }

        public List<string> GetExtension()
        {
            return new List<string> { ".pdf", ".doc", ".docx" };
        }
    }
}
