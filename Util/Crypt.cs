﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Util
{
    public class Crypt
    {
        public static string Encoded(string encodeMe)
        {
            byte[] encoded = System.Text.Encoding.ASCII.GetBytes(encodeMe);
            return Convert.ToBase64String(encoded);
        }

        public static string Decode(string decodeMe)
        {
            byte[] encoded = Convert.FromBase64String(decodeMe);
            return System.Text.Encoding.ASCII.GetString(encoded);
        }

    }
}