﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace CaseManager.RoleDescriptor
{
    public class PageRenderer
    {
        private HttpSessionState _session;
        public PageRenderer(HttpSessionState session)
        {
            this._session = session;
        }
        public HttpSessionState getCurrentSession()
        {
            return this._session;
        }
        /*public List<LinkObject> getMenus()
        {
            if (this._session == null)
                return null;

            if(this._session["roles"] == null)
            {
                return null;
            }
            String[] role_menus = (String[])this._session["roles"];
            List<LinkObject> l = new List<LinkObject>();
            return l;
        }*/

    }



    class LinkObject
    {
        public String href { get; set; }
        public String Text { get; set; }

        public LinkObject(string href, string text)
        {
            this.href = href;
            this.Text = text;
        }
        override
        public String ToString()
        {
            return this.Text;
        }
    }
}