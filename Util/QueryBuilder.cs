﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Util
{
    public class QueryBuilder
    {
        public QueryBuilder()
        {

        }

        #region Data Members
        private List<string> _columns = new List<string>();
        private string _from = "";
        private List<string> _joins = new List<string>();
        private List<string> _where = new List<string>();
        private string _group_by = "";
        private string _order_by = "";

        public enum JoinType { LeftJoin, InnerJoin };
        #endregion 

        #region Properties

        #endregion

        #region Method
        public void column(string column)
        {
            this._columns.Add(column);
        }

        public void columns(params string[] columns)
        {
            this._columns.AddRange(columns);
        }

        public void from(string table)
        {
            this._from = table;
        }

        public void innerJoin(string table, string criteria)
        {
            this._joins.Add(" INNER JOIN " + table + " ON " + criteria + " ");
        }

        public void leftJoin(string table, string criteria)
        {
            this._joins.Add(" LEFT JOIN " + table + " ON " + criteria + " ");
        }

        public void join(string table, string criteria, JoinType join_type)
        {
            switch (join_type)
            {
                case JoinType.InnerJoin:
                    this.innerJoin(table, criteria);
                    break;
                case JoinType.LeftJoin:
                    this.leftJoin(table, criteria);
                    break;
                default:
                    break;
            }
        }

        public void groupBy(string grouping)
        {
            this._group_by = " GROUP BY " + grouping;
        }

        public void orderBy(string order, bool in_ascending)
        {
            string sorting_order  = (in_ascending) ? " ASC" : " DESC";
            this._order_by = " ORDER BY " + order + sorting_order;
        }

        public void where(string condition)
        {
            this._where.Add(condition);
        }

        public string build()
        {
            string columns = (this._columns.Count == 0) ? "*" : string.Join(", ", this._columns);
            string where = (this._where.Count == 0) ? "" : (" WHERE " + string.Join(" AND ", this._where));
            string join = " " + string.Join(" ", this._joins);

            string sql = "SELECT "
                + columns
                + " FROM " + this._from
                + join
                + where
                + this._group_by
                + this._order_by + ";";

            return sql;
        }

        public string build(int offset, int count, string over_field, string dummy_table_name)
        {
            string columns = (this._columns.Count == 0) ? "*" : string.Join(", ", this._columns);
            string where = (this._where.Count == 0) ? "" : (" WHERE " + string.Join(" AND ", this._where));
            string join = " " + string.Join(" ", this._joins);
            int upper_limit = offset + count;

            string sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY " + over_field + ") AS RowNum, "
                + columns
                + " FROM " + this._from
                + join
                + where
                + this._group_by
                + this._order_by 
                + ") AS " + dummy_table_name + " WHERE RowNum >= " + offset.ToString() 
                + " AND RowNum < " + upper_limit.ToString()
                + " ORDER BY RowNum;";

            return sql;
        }
        #endregion
    }
}