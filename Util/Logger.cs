﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.IO;

namespace CaseManager.Util
{
    public class Logger
    {
        public static Boolean LogError(string errText)
        {
            //todo: remove this code
            try
            {
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + @"Log\Error\" + DateTime.Now.ToString("dd-MMM-yyyy") + @".txt", DateTime.Now + System.Environment.NewLine + errText + System.Environment.NewLine);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}