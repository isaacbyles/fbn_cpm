﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Web;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Data.SqlClient;

namespace CaseManager.Util
{
    public static class Utility
    {
        public static DataTable UploadContents(string FilePath,string connString)
        {
            DataTable dt = new DataTable();
            OleDbConnection oleDbConn = new OleDbConnection(connString);
            try
            {
                oleDbConn.Open();
                DataSet ds = new DataSet();
                DataTable dtSchema = oleDbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                string firstSheetName = dtSchema.Rows[0]["TABLE_NAME"].ToString();
                new OleDbDataAdapter("SELECT * FROM [" + firstSheetName + "]", oleDbConn).Fill(ds);
                //OleDbCommand oleDbComm = new OleDbCommand("SELECT * FROM [sheet1$]", oleDbConn);
                //OleDbDataAdapter oledata = new OleDbDataAdapter(dtSchema);
                //
                //oledata.Fill(ds);
                dt = ds.Tables[0];                
            }
            catch (Exception  ex)
            {
                
            }
            finally
            {
                oleDbConn.Close();
            }
            return dt;
        }
    }
}