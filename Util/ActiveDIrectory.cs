﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;

namespace CaseManager.Util
{

    public class ActiveDIrectory
    {
        

        private string _path;
        private string _filterAttribute;


        public string Authenticate(string userName, string password)
        {
            string authentic = "false";
            try
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://192.168.1.33", userName, password);
              //  entry.Path = "LDAP://192.168.1.33/OU=Users,DC=fbn, DC=com";
                object nativeObject = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + userName + ")";
                search.PropertiesToLoad.Add("cn");
                search.PropertiesToLoad.Add("mail");
                SearchResult result = search.FindOne();

                if (null == result)
                {
                    return "false";
                }

                _path = result.Path;
                _filterAttribute = (string)result.Properties["cn"][0];

                authentic = (string) result.Properties["mail"][0];
            }
            catch (DirectoryServicesCOMException) { }
            return authentic;
        }

        public string AuthenticateUser(string username)
        {
           // string username = TextBox1.Text.Trim();
            string result = "";
            try
            {
                
                PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "nigeria.firstbank.local",
                    "OU=GenericUsers, dc=NIGERIA,dc=FIRSTBANK,dc=LOCAL");
                UserPrincipal qbeUser = UserPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, username);

                if (qbeUser != null)
                {
                    result = qbeUser.EmailAddress;
                   
                }
                else
                {
                    result = "Not Generic User";
                }
            }
            catch (Exception ex)
            {
                result = ex.InnerException.ToString();
            }
            return result;
        }


        public string AuthenticatePass(string username, string password)
        {
            // string username = TextBox1.Text.Trim();
            string result = "";
            try
            {

                PrincipalContext ctx = new PrincipalContext(ContextType.Domain, "nigeria.firstbank.local",
                    "OU=GenericUsers, dc=NIGERIA,dc=FIRSTBANK,dc=LOCAL");

                bool pd = ctx.ValidateCredentials(username, password);
                if (pd == true)
                {
                    /*UserPrincipal qbeUser = UserPrincipal.FindByIdentity(ctx, username);

                    if (qbeUser != null)
                    {
                        result = qbeUser.EmailAddress;

                    }
                    else
                    {
                        result = "Not Generic User";
                    }*/
                    result = "true";
                }
                else
                {
                    result = "false";
                }
            }
            catch (Exception ex)
            {
                result = ex.InnerException.ToString();
            }
            return result;
        }

        private string getEmail()
        {
            string email = "";
            DirectorySearcher search = new DirectorySearcher(_path);
            search.Filter = "(cn=" + _filterAttribute + ")";
            search.PropertiesToLoad.Add("mail");
            //StringBuilder groupNames = new StringBuilder();

            try
            {
                SearchResult result = search.FindOne();
                //int propertyCount = result.Properties["memberOf"].Count;

                //int equalsIndex, commaIndex;


                email = (string)result.Properties["mail"][0];

            }
            catch (Exception ex)
            {
                //throw new Exception("Error obtaining group names. " + ex.Message);
                return "false";
            }
            return email.ToString();
        }
    }


}