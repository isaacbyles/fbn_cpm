﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CaseManager.Util
{
    public class IPStorage
    {
        public static void SaveIp(string user_id, string Ip)
        {
            string sql = @"INSERT INTO IPSTORAGETBL (user_id, ip) VALUES (@user_id, @ip)";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@user_id", user_id);
                cmd.Parameters.AddWithValue("@ip", Ip);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }


        public static string FetchIp(string user_id)
        {
            string ip = String.Empty;
            string sql = @"SELECT ip FROM IPSTORAGETBL WHERE user_id ='" + user_id + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        ip = (string) rd["ip"];
                    }
                }
                conn.Close();
                return ip;
            }
        }
    }
}