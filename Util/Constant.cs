﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Util
{
    public class Constant
    {
        public const string JSON_CONTENT_TYPE = "application/json";

        public const int ROLE_ADMIN = 1;
    }
}