﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaseManager.Util
{
    public class TempStorage
    {
        private static object theLocker =new object();
        private static object theLocker2 = new object();

        private static object theData;
        private static object theData2;
        public static object TheData
        {
            get
            {
                lock (theLocker)
                {
                    return theData;
                }
            }
            set
            {
                lock (theLocker)
                {
                    theData = value;
                }
            }
        }

        public static object TheData2
        {
            get
            {
                lock (theLocker2)
                {
                    return theData2;
                }
            }
            set
            {
                lock (theLocker2)
                {
                    theData2 = value;
                }
            }
        }
    }
}