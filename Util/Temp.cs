﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CaseManager.Model;


namespace CaseManager.Util
{
    public class Temp
    {

        public static void SetUser(User user)
        {
            string SqlCheck = "SELECT *  FROM tmp_data WHERE id ='" + user.Id + "'";
            string sql =
                @"INSERT INTO tmp_data (id, Email,BranchId,RoleId,CreatedDate, ModifiedDate,Status) VALUES ('" +
                user.Id + "', '" + user.Email + "', '" + user.BranchId + "','" + user.RoleId + "', '" + user.CreatedDate +
                "', '" +
                user.ModifiedDate + "', '" + user.Status + "')";
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using(SqlConnection conn2 = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(SqlCheck, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {

                }
                else
                {
                    conn.Close();
                    conn2.Open();
                    SqlCommand cmd2 = new SqlCommand(sql, conn2);
                    int i = cmd2.ExecuteNonQuery();
                    // ID = user.Id; // store in session
                    HttpContext.Current.Session["USERID"] = user.Id;
                    conn2.Close();
                }
                
            }

            
        }

        public static User GetUser(string userID)
        {
            User user = new User();
            string sql = "SELECT *  FROM tmp_data WHERE id ='" + userID + "'";
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {
                    if (rd.Read())
                    {
                        user = new User
                        {
                            Id = rd.GetInt32(0),
                            Email = rd.IsDBNull(1) ? null : rd.GetString(1),
                            BranchId = int.Parse(rd.IsDBNull(2) ? null : rd.GetString(2)),
                            RoleId = short.Parse(rd.IsDBNull(3) ? null : rd.GetString(3)),
                            CreatedDate = rd.GetDateTime(4),
                            ModifiedDate = rd.GetDateTime(5),
                            Status = short.Parse(rd.IsDBNull(6) ? null : rd.GetString(6))
                        };

                    }
                   
                }
                conn.Close();

            }
            
            return user;
        }

        public static void SetUserLogin(string id, string email, string branch_id, string role_id, string created_date, string modified_date, string status, string firstname, string lastname, string middlename, string telephone, string profile_created_date, string profile_modified_date, string profile_status, string branch_name, string role_name)
        {
            string SqlCheck = "SELECT *  FROM tmp_data_login WHERE id ='" + id + "'";
            string sql =
                @"INSERT INTO tmp_data_login (id, email,branch_id,role_id,created_date, modified_date,status, firstname, lastname,middlename,telephone,profile_created_date,profile_modified_date,profile_status,branch_name,role_name) VALUES ('" +
               id + "', '" + email + "', '" + branch_id + "','" + role_id + "', '" + created_date +
                "', '" +
                modified_date + "', '" + status + "', '" + firstname + "', '" + lastname + "', '" + middlename + "', '" + telephone + "', '" + profile_created_date + "', '" + profile_modified_date + "', '" + profile_status + "', '" + branch_name + "', '" + role_name + "')";
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using(SqlConnection conn2 = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(SqlCheck, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {

                }
                else
                {
                    conn.Close();
                    conn2.Open();
                    SqlCommand cmd2 = new SqlCommand(sql, conn2);
                    int i = cmd2.ExecuteNonQuery();
                    //LOGINID = id; //store temporary 
                    conn2.Close();
                }
               
                
            }
            
           
            
        }

        public static DataSet GetUserLogin(string userID)
        {
            DataSet dsd = new DataSet();
            SqlDataAdapter rd;
            string sql = @"SELECT * FROM tmp_data_login WHERE  id ='" + userID + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(dsd, "user");
                conn.Close();
                
            }
            
            return dsd;
        }

        public static void SetUserProcess(string user_role, string process_id, string name, string desc,
            string reassign_role_id, string created_date, string modified_date, string status)
        {
            string SqlCheck = "SELECT *  FROM tmp_data_process WHERE role_id ='" + user_role + "' and process_id= '" +
                              process_id + "' and name='" + name + "' and description ='" + desc + "'";
            string sql =
                @"INSERT INTO tmp_data_process (role_id, process_id, name,description,reassign_role_id,created_date, modified_date,status) VALUES ('" +
                user_role + "', '" + process_id + "', '" + name + "','" + desc + "', '" + reassign_role_id +
                "', '" +
                created_date + "', '" + modified_date + "', '" + status + "')";
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using(SqlConnection conn2 = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(SqlCheck, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {

                }
                else
                {
                    conn.Close();
                    conn2.Open();
                    SqlCommand cmd2 = new SqlCommand(sql, conn2);
                    int i = cmd2.ExecuteNonQuery();

                    conn2.Close();
                }

            }

           
        }

        public static DataSet GetUserProcess(string role_id)
        {
            DataSet dsd = new DataSet();
            SqlDataAdapter rd;
            string sql = @"SELECT * FROM tmp_data_process WHERE  role_id ='" + role_id + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(dsd, "process");
                conn.Close();
            }
            return dsd;
            
        }


        public static void SetStartStage(string stage_id, string process_id, string role_id, string type, string name,
            string description, string created_date, string modified_date, string status, string form_id,
            string form_name, string form_description)
        {
            string SqlCheck = "SELECT *  FROM tmp_data_start_stage WHERE role_id ='" + role_id + "' and id = '" +
                              stage_id + "' and process_id = '" + process_id + "' and type= '" + type + "'";

            string sql =
                @"INSERT INTO tmp_data_start_stage (id,  process_id, role_id, type, name,description,created_date, modified_date,status, form_id, form_name, form_description) VALUES ('" +
                stage_id + "', '" + process_id + "', '" + role_id + "','" + type + "', '" + name +
                "','" +
                description + "', '" +
                created_date + "', '" + modified_date + "', '" + status + "', '" + form_id + "', '" + form_name + "', '" +
                form_description + "')";
            using(SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using(SqlConnection conn2 = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(SqlCheck, conn))
            {
                conn.Open();
                SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (rd.HasRows)
                {

                }
                else
                {
                    conn.Close();
                    conn2.Open();
                    SqlCommand cmd2 = new SqlCommand(sql, conn2);
                    int i = cmd2.ExecuteNonQuery();

                    conn2.Close();
                }

            }

           

        }


        public static DataSet GetStartStage(string role_id)
        {
            DataSet dsd = new DataSet();
            SqlDataAdapter rd;
            string sql = @"SELECT * FROM tmp_data_start_stage WHERE  role_id ='" + role_id + "'";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                rd = new SqlDataAdapter(cmd);
                rd.Fill(dsd, "start_stage");
                conn.Close();
            }
            return dsd;
        }


        public static void CreateProcessTable()
        {
            string query = "IF OBJECT_ID('tmp_data_process', 'U') IS NULL ";
            query += "BEGIN ";
            query += "CREATE TABLE tmp_data_process (";
            query += "[role_id] VARCHAR(50) NOT NULL,";
            query += "[process_id] VARCHAR(100) NOT  NULL,";
            query += "[name] VARCHAR(250)  NOT NULL,";
            query += "[description] VARCHAR(250) NOT NULL,";
            query += "[reassign_role_id] VARCHAR(250) NOT NULL,";
            query += "[created_date] VARCHAR(250) NOT NULL,";
            query += "[modified_date] VARCHAR(250) NOT NULL,";
            query += "[status] VARCHAR(250) NOT NULL";
            query += ")";
            query += " END";
            string constr = ConfigurationManager.AppSettings["conn"];
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }


        public static void CreateStageTable()
        {
            string query = "IF OBJECT_ID('tmp_data_start_stage', 'U') IS NULL ";
            query += "BEGIN ";
            query += "CREATE TABLE tmp_data_start_stage (";
            query += "[id] VARCHAR(50)  NOT NULL,";
            query += "[process_id] VARCHAR(100) NOT NULL,";
            query += "[role_id] VARCHAR(50) NOT NULL,";
            query += "[type] VARCHAR(100) NOT NULL,";
            query += "[name] VARCHAR(250) NOT NULL,";
            query += "[description] VARCHAR(250) NOT  NULL,";
            query += "[created_date] VARCHAR(250) NOT NULL,";
            query += "[modified_date] VARCHAR(250) NOT NULL,";
            query += "[status] VARCHAR(250) NOT NULL,";
            query += "[form_id] VARCHAR(250) NOT NULL,";
            query += "[form_name] VARCHAR(250) NOT NULL,";
            query += "[form_description] VARCHAR(250) NOT NULL";
            query += ")";
            query += " END";
            string constr = ConfigurationManager.AppSettings["conn"];
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

        public static void CreateUserAdminData()
        {
            string query = "IF OBJECT_ID('tmp_data', 'U') IS NULL ";
            query += "BEGIN ";
            query += "CREATE TABLE tmp_data (";
            query += "[id] int NOT NULL,";
            query += "[Email] VARCHAR(2100)  NULL,";
            query += "[BranchId] VARCHAR(250)  NULL,";
            query += "[RoleId] VARCHAR(50)  NULL,";
            query += "[CreatedDate] [datetime]  NULL,";
            query += "[ModifiedDate] [datetime]  NULL,";
            query += "[status] VARCHAR(250)  NULL";
            query += ")";
            query += " END";
            string constr = ConfigurationManager.AppSettings["conn"];
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

        public static void CreateUserDatasetData()
        {
            string query = "IF OBJECT_ID('tmp_data_login', 'U') IS NULL ";
            query += "BEGIN ";
            query += "CREATE TABLE tmp_data_login (";
            query += "[id] int NOT NULL,";
            query += "[email] VARCHAR(2100)  NULL,";
            query += "[branch_id] VARCHAR(250)  NULL,";
            query += "[role_id] VARCHAR(50)  NULL,";
            query += "[created_date] [varchar](50)  NULL,";
            query += "[modified_date] [varchar](50)  NULL,";
            query += "[status] VARCHAR(250)  NULL,";
            query += "[firstname] [varchar](250)  NULL,";
            query += "[lastname] [varchar](250)  NULL,";
            query += "[middlename] [varchar](150)  NULL,";
            query += "[telephone] [varchar](150)  NULL,";
            query += "[profile_created_date] [varchar](150)  NULL,";
            query += "[profile_modified_date] [varchar](150)  NULL,";
          
            query += "[profile_status] [varchar](150)  NULL,";
            query += "[branch_name] [varchar](150)  NULL,";
            query += "[role_name] [varchar](250)  NULL";
            query += ")";
            query += " END";
            string constr = ConfigurationManager.AppSettings["conn"];
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
    }
}