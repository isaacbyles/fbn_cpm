﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.SessionState;

using CaseManager.Model;
using System.Data;
using System.Data.SqlClient;
using CaseManager.Logic;
using HttpServer.Sessions;

namespace CaseManager.Util
{
    public class Auth
    {
        private static HttpCookie cookie = new HttpCookie("ID");
        private static AuditLogic audit = new AuditLogic();
        private static HttpCookie cookie2 = new HttpCookie("RoleID");
        public static SessionHandler se = new SessionHandler();
        public static Object _roleID;
        public static Object _userID;
        public static object ID;
        public static object LOGINID;
        public static User getInfo()
        {
            return (User) HttpContext.Current.Session["user"];
        }

        public static bool isAdmin()
        {
            User u = (User) HttpContext.Current.Session["user"];

            if (u == null) return false;

            return (u.RoleId == Constant.ROLE_ADMIN);
        }

        public static void set(User user)
        {
            HttpContext.Current.Session["user"] = user;
            
            

        }

        public static bool isActive()
        {
            bool res = false;
            cookie = HttpContext.Current.Request.Cookies["ID"];

            if (cookie == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            if (_userID == null)
            {
                if (cookie != null) _userID = cookie.Value;
            }
            User u = Temp.GetUser(_userID.ToString());
            if (u == null)
            {
                return false;
            }
            else
            {
                res = (u.Status == Status.ACTIVE);
            }


            return res;
        }

        public static User get()
        {

            User user = (User) HttpContext.Current.Session["user"];

            return user;
        }

        #region

        //public static bool isLoggedIn()
        //{
        //    User u = Auth.getInfo();
        //    return (u != null);
        //}

        #endregion


        public static void clear()
        {
            cookie = HttpContext.Current.Request.Cookies["ID"];
            cookie2 = HttpContext.Current.Request.Cookies["RoleID"];
            if (cookie == null || cookie2 == null)
            {
                HttpContext.Current.Response.Redirect("~/logout.aspx", true);
            }
            else
            {
                User user = Temp.GetUser(cookie.Value);
                if (user == null)
                {
                    HttpContext.Current.Response.Redirect("~/logout.aspx", true);
                }
                else
                {
                    string sql = @"DELETE FROM tmp_data WHERE id= '" + user.Id + "'";
                    string sql2 = @"DELETE FROM tmp_data_login WHERE id= '" + user.Id + "'";
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        conn.Open();
                        int i = cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]))
                    using (SqlCommand cmd = new SqlCommand(sql2, conn))
                    {
                        conn.Open();
                        int i = cmd.ExecuteNonQuery();
                        conn.Close();
                    }

                }
                //object c = cookie.Value;
                //cookie.Expires = DateTime.Now.AddDays(-1);
                //cookie2.Expires = DateTime.Now.AddDays(-1);
                //HttpContext.Current.Response.Cookies.Add(cookie);
                //HttpContext.Current.Session.Remove("ID");
                //HttpContext.Current.Session.Remove("RoleID");
                //HttpContext.Current.Session.Abandon();
                //HttpContext.Current.Session.Clear();

                HttpCookie aCookie;
                string cookieName = null;
                int limit = HttpContext.Current.Request.Cookies.Count;
                for (int i = 0; i < limit; i++)
                {
                    var httpCookie = HttpContext.Current.Request.Cookies[i];
                    if (httpCookie != null)
                        cookieName = httpCookie.Name;
                    aCookie = new HttpCookie(cookieName);
                    aCookie.Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Current.Response.Cookies.Add(aCookie);
                }
            }
        }

        public static void setLoginData(DataSet data)
        {
            HttpContext context = HttpContext.Current;
            context.Session["login_data"] = data;
            //string id = data.Tables[0].Rows[0].ItemArray[0].ToString();
            //string email = data.Tables[0].Rows[0].ItemArray[1].ToString();
            //string branch_id = data.Tables[0].Rows[0].ItemArray[2].ToString();
            //string role_id = data.Tables[0].Rows[0].ItemArray[3].ToString();
            //string created_date = data.Tables[0].Rows[0].ItemArray[4].ToString();
            //string modified_date = data.Tables[0].Rows[0].ItemArray[5].ToString();
            //string status = data.Tables[0].Rows[0].ItemArray[6].ToString();
            //string firstname = data.Tables[0].Rows[0].ItemArray[7].ToString();
            //string lastname = data.Tables[0].Rows[0].ItemArray[8].ToString();
            //string middlename = data.Tables[0].Rows[0].ItemArray[9].ToString();

            //string telephone = data.Tables[0].Rows[0].ItemArray[10].ToString();

            //string profile_created_date = data.Tables[0].Rows[0].ItemArray[11].ToString();

            //string profile_modified_date = data.Tables[0].Rows[0].ItemArray[12].ToString();

            //string profile_status = data.Tables[0].Rows[0].ItemArray[13].ToString();

            //string branch_name = data.Tables[0].Rows[0].ItemArray[14].ToString();

            //string role_name = data.Tables[0].Rows[0].ItemArray[15].ToString();


            //string SqlCheck = "SELECT *  FROM tmp_data_login WHERE id ='" + id + "'";
            //string sql =
            //    @"INSERT INTO tmp_data_login (id, email,branch_id,role_id,created_date, modified_date,status, firstname, lastname,middlename,telephone,profile_created_date,profile_modified_date,profile_status,branch_name,role_name) VALUES ('" +
            //   id + "', '" + email + "', '" + branch_id + "','" + role_id + "', '" + created_date +
            //    "', '" +
            //    modified_date + "', '" + status + "', '"+firstname+"', '"+lastname+"', '"+middlename+"', '"+telephone+"', '"+profile_created_date+"', '"+profile_modified_date+"', '"+profile_status+"', '"+branch_name+"', '"+role_name+"')";
            //SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["conn"]);
            //SqlConnection conn2 = new SqlConnection(ConfigurationManager.AppSettings["conn"]);
            //SqlCommand cmd = new SqlCommand(SqlCheck, conn);

            //conn.Open();
            //SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //if (rd.HasRows)
            //{

            //}
            //else
            //{
            //    conn.Close();
            //    conn2.Open();
            //    SqlCommand cmd2 = new SqlCommand(sql, conn2);
            //    int i = cmd2.ExecuteNonQuery();
            //    LOGINID = id; //store temporary 
            //    conn2.Close();
            //}
            
        }

        public static DataSet getLoginData()
        {
            return (DataSet) HttpContext.Current.Session["login_data"];
        }
    }
}