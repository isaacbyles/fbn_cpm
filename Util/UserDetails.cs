﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CaseManager.Logic;
using CaseManager.LawPavilion.Util;
using CaseManager.Model;

namespace CaseManager.Util
{

    public class UserDetails
    {
        public string getBranch(int branch_id)
        {
            try
            {
                string sqlQuery = @"select [name] from [branch] where [id] = " + branch_id.ToString();
                LPCMConnection objConnection = new LPCMConnection();
                objConnection.Sql = sqlQuery;
                return objConnection.GetTable.Rows[0]["name"].ToString();
            }
            catch (Exception ex)
            {
                return "";
            }

        }


        public string get_Branch(String branch_id)
        {
            try
            {
                string sqlQuery = @"select [name] from [branch] where [id] = " + branch_id.ToString();
                LPCMConnection objConnection = new LPCMConnection();
                objConnection.Sql = sqlQuery;
                return objConnection.GetTable.Rows[0]["name"].ToString();
            }
            catch (Exception ex)
            {
                return "";
            }

        }
        public string getRegion(string branch_id)
        {

            try
            {
                string sqlQuery = @"select [region] from [branch] where [id] = " + branch_id.ToString();
                LPCMConnection objConnection = new LPCMConnection();
                objConnection.Sql = sqlQuery;
                return objConnection.GetTable.Rows[0]["region"].ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string getRoleDesc(string role_id)
        {
            try
            {
                string sqlQuery = @"select [name] from [role] where [id] = " + role_id;
                LPCMConnection objConnection = new LPCMConnection();
                objConnection.Sql = sqlQuery;
                return objConnection.GetTable.Rows[0]["name"].ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

    }
}