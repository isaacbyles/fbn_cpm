﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace CaseManager.Util
{
    public class WebService
    {
        private string soapResult = String.Empty;
        private string[] result = null;

        private static HttpWebRequest CreateWebRequest()
        {
            HttpWebRequest webRequest =
                (HttpWebRequest) WebRequest.Create(@"http://172.16.249.199/FBNServices/Service.asmx?op=ADUserDetails");
            webRequest.Headers.Add(@"SOAP:Action");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        public string Authenticate(string username)
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            string soapEnvelope =
                @"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"">
                <s:Body xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                    <ADUserDetails xmlns=""http://tempuri.org/"">
                        <username>{0}</username>
                    </ADUserDetails>
                </s:Body>
              </s:Envelope>";
            
            soapEnvelopeXml.LoadXml(string.Format(soapEnvelope, username));
            try
            {
                using (Stream stream = request.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }

                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        //soapResult = rd.ReadLine();
                        XmlTextReader reader = new XmlTextReader(rd);
                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                soapResult = reader.Value.Trim();
                                result = soapResult.Split('|');
                            }
                        }

                    }
                }
                return result[1];
            }
            catch (WebException ex)
            {
                Console.Write(ex.ToString());
                return "false";
            }
            
        }

        public string AuthenticateWithPassword(string username, string password)
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            string soapEnvelope =
                #region

//           @"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"">
//                <s:Body xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
//                    <ADUserDetails xmlns=""http://tempuri.org/"">
//                        <username>{0}</username>
//                    </ADUserDetails>
//                </s:Body>
//              </s:Envelope>";
                #endregion

                //USers
                @"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"">
                    <s:Body xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
                        <Heelow xmlns=""http://tempuri.org/"">
                            <username>{0}</username>
                            <password>{1}</password>
                        </Heelow>
                    </s:Body>
                   </s:Envelope>";

            soapEnvelopeXml.LoadXml(string.Format(soapEnvelope, username, password));
            try
            {
                using (Stream stream = request.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }

                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        //soapResult = rd.ReadLine();
                        XmlTextReader reader = new XmlTextReader(rd);
                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                soapResult = reader.Value.Trim();
                                result = soapResult.Split('|');
                            }
                        }

                    }
                }

            }
            catch (WebException ex)
            {
                Console.Write(ex.ToString());
            }
            return result[1];
        }
    }

}