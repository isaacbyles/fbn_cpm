﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using CoExDataAccess;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;

namespace CaseManager.Util
{
    public class DataAccess
    {
        #region Constructor
        public DataAccess()
        {
            this._config = new DBConfig(DBConfig.DBPlatforms.SQL);
            this.SHOW_SYS_ERROR_MSG = int.Parse(ConfigurationManager.AppSettings.Get("SHOW_SYS_ERROR")) == 1;
        }

        public DataAccess(DBConfig.DBPlatforms platform)
        {
            this._config = new DBConfig(platform);
            this.SHOW_SYS_ERROR_MSG = int.Parse(ConfigurationManager.AppSettings.Get("SHOW_SYS_ERROR")) == 1;
        }
        #endregion

        #region Data Members
        protected DBConfig _config;
        private Exception _app_error = null;
        private bool _hasDbError = false;
        private Boolean SHOW_SYS_ERROR_MSG = false;
        public const String DEFAULT_ERROR_MSG = "An error has occured";
        #endregion

        #region Properties
        /// <summary>
        /// It gets and sets the DBConfig object used by the Base
        /// </summary>
        public DBConfig Config
        {
            get { return this._config; }
            set { this._config = value; }
        }

        /// <summary>
        /// It gets error message for any Exception that may occur in any App_Code method
        /// </summary>
        public string ErrorMessage
        {
            get { return this._app_error.Message; }
        }

        /// <summary>
        /// It get error type for any Exception that may occur
        /// </summary>
        public string ErrorType
        {
            get { return this._app_error.GetType().ToString(); }
        }

        /// <summary>
        /// It checks if an error has occured
        /// </summary>
        public bool HasError
        {
            get { return (this._app_error != null) || (this._hasDbError); }
        }

        /// <summary>
        /// It gets and sets Exceptions from the App_Code Classes
        /// </summary>
        public Exception AppError
        {
            get { return this._app_error; }
            set { this._app_error = value; }
        }

        /// <summary>
        /// It sets and gets if a database error has occured
        /// </summary>
        public bool HasDbError
        {
            get { return this._hasDbError; }
        }

        public bool HasAppError
        {
            get { return this._app_error != null; }
        }
        #endregion

        #region Methods
        public DataSet Fetch(String sql, Dictionary<String, Parameter> parameterDict)
        {
            if (this.HasError) return null;

            DataSet result;
            DBConnection con = new DBConnection(this.Config);
            try
            {
                using (con)
                {
                    con.Open();
                    DBCommand cmd = new DBCommand(sql, con);
                    cmd.CommandType = CommandType.Text;
                    foreach (KeyValuePair<string, Parameter> entry in parameterDict)
                    {
                        if (entry.Value.Value == null)
                        {
                            cmd.AddParameterWithValue(entry.Key, entry.Value.Value);
                        }
                        else
                        {
                            switch (entry.Value.Type)
                            {
                                case Parameter.ParameterType.BIGINT:
                                    cmd.AddParameterWithValue(entry.Key, long.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.DECIMAL:
                                    cmd.AddParameterWithValue(entry.Key, decimal.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.INT:
                                    cmd.AddParameterWithValue(entry.Key, int.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.FLOAT:
                                    cmd.AddParameterWithValue(entry.Key, float.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.DOUBLE:
                                    cmd.AddParameterWithValue(entry.Key, double.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.DATETIME:
                                    cmd.AddParameterWithValue(entry.Key, DateTime.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.BOOLEAN:
                                    cmd.AddParameterWithValue(entry.Key, bool.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.STRING:
                                default:
                                    cmd.AddParameterWithValue(entry.Key, entry.Value.Value.ToString());
                                    break;
                            }
                        }
                    }

                    result = cmd.ExecuteMultiQuery();
                    this._hasDbError = result == null;
                }
            }
            catch (Exception ex)
            {
                this.AppError = (SHOW_SYS_ERROR_MSG) ? ex : new Exception(DEFAULT_ERROR_MSG);
                result = null;
            }
            finally
            {
                con.Dispose();
            }
            return result;
        }

        public int ExecuteNonQuery(String sql, Dictionary<String, Parameter> parameterDict)
        {
            if (this.HasError) return -1;

            int result;
            DBConnection con = new DBConnection(this.Config);
            try
            {
                using (con)
                {
                    con.Open();
                    DBCommand cmd = new DBCommand(sql, con);
                    cmd.CommandType = CommandType.Text;
                    foreach (KeyValuePair<string, Parameter> entry in parameterDict)
                    {
                        if (entry.Value.Value == null)
                        {
                            cmd.AddParameterWithValue(entry.Key, entry.Value.Value);
                        }
                        else
                        {
                            switch (entry.Value.Type)
                            {
                                case Parameter.ParameterType.BIGINT:
                                    cmd.AddParameterWithValue(entry.Key, long.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.DECIMAL:
                                    cmd.AddParameterWithValue(entry.Key, decimal.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.INT:
                                    cmd.AddParameterWithValue(entry.Key, int.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.FLOAT:
                                    cmd.AddParameterWithValue(entry.Key, float.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.DOUBLE:
                                    cmd.AddParameterWithValue(entry.Key, double.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.DATETIME:
                                    cmd.AddParameterWithValue(entry.Key, DateTime.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.BOOLEAN:
                                    cmd.AddParameterWithValue(entry.Key, bool.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.STRING:
                                default:
                                    cmd.AddParameterWithValue(entry.Key, entry.Value.Value.ToString());
                                    break;
                            }
                        }
                    }

                    result = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                this.AppError = (SHOW_SYS_ERROR_MSG) ? ex : new Exception(DEFAULT_ERROR_MSG);
                result = -1;
            }
            finally
            {
                con.Dispose();
            }
            return result;
        }

        public object Add(String sql, Dictionary<String, Parameter> parameterDict)
        {
            if (this.HasError) return -1;

            object result;
            DBConnection con = new DBConnection(this.Config);
            try
            {
                using (con)
                {
                    con.Open();
                    DBCommand cmd = new DBCommand(sql, con);
                    cmd.CommandType = CommandType.Text;
                    foreach (KeyValuePair<string, Parameter> entry in parameterDict)
                    {
                        if (entry.Value.Value == null)
                        {
                            cmd.AddParameterWithValue(entry.Key, entry.Value.Value);
                        }
                        else
                        {
                            switch (entry.Value.Type)
                            {
                                    
                                case Parameter.ParameterType.BIGINT:
                                    cmd.AddParameterWithValue(entry.Key, long.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.DECIMAL:
                                    cmd.AddParameterWithValue(entry.Key, decimal.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.INT:
                                    cmd.AddParameterWithValue(entry.Key, entry.Value.Value.ToString()); //int.Parse() edited
                                    break;
                                case Parameter.ParameterType.FLOAT:
                                    cmd.AddParameterWithValue(entry.Key, float.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.DOUBLE:
                                    cmd.AddParameterWithValue(entry.Key, double.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.DATETIME:
                                    cmd.AddParameterWithValue(entry.Key, DateTime.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.BOOLEAN:
                                    cmd.AddParameterWithValue(entry.Key, bool.Parse(entry.Value.Value.ToString()));
                                    break;
                                case Parameter.ParameterType.STRING:
                                    cmd.AddParameterWithValue(entry.Key, entry.Value.Value.ToString());
                                    break;
                                default:
                                    cmd.AddParameterWithValue(entry.Key, entry.Value.Value.ToString());
                                    break;
                            }
                        }
                    }

                    result = cmd.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                this.AppError = (SHOW_SYS_ERROR_MSG) ? ex : new Exception(DEFAULT_ERROR_MSG);
                result = -1;
            }
            finally
            {
                con.Dispose();
            }
            return result;
        }

        #endregion
    }

    public class Parameter
    {
        public Parameter(Object value, ParameterType type)
        {
            this.value = value;
            this.type = type;
        }

        private Object value;
        private ParameterType type;
        public enum ParameterType
        {
            INT,
            FLOAT,
            DATETIME,
            STRING,
            DECIMAL,
            DOUBLE,
            BOOLEAN,
            UNKNOWN,
            BIGINT
        }

        public Object Value
        {
            get { return this.value; }
        }

        public ParameterType Type
        {
            get { return this.type; }
        }
    }
}