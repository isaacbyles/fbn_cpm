﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Web;
using Newtonsoft.Json;

namespace CaseManager.Util
{
    public class SystemInformation
    {

        public static string GetMACAddress()
        {

            String sMacAddress = string.Empty;
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {

                if (nic.NetworkInterfaceType != NetworkInterfaceType.Ethernet)
                {

                }
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    sMacAddress = nic.GetPhysicalAddress().ToString();
                    break;

                }
            }

            return sMacAddress;
        }

        public static String GetIPAddress()
        {
            String ip =
                HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            return ip;
        }

        public static string ip()
        {
            String ip = HttpContext.Current.Request.ServerVariables.Get("HTTP_X_FORWARDED_FOR");
            if (!string.IsNullOrEmpty(ip))
            {
                string[] ipRange = ip.Split(',');
                int le = ipRange.Length - 1;
                string trueIP = ipRange[le];
            }
            else
            {
                ip = HttpContext.Current.Request.ServerVariables.Get("REMOTE_ADDR");
            }
            return ip;
        }
        public static string GetIP()
        {
            string ss = "";
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            foreach (string key in context.Request.Params.Keys)
            {

                context.Response.Write(String.Format("{0}: {1}<br />", key, context.Request.Params[key]));
            }
            context.Response.Write(context.Request.Headers["Via"]);
            context.Response.Write(context.Request.Headers["X-Forwarded-For"]);
            return "";

          
        }

    }
}