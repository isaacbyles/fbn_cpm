﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace CaseManager.Util
{
    public class SessionHandler : IRequiresSessionState
    {
        public SessionHandler() { }
        public object GetSession(string key)
        {
            object session = HttpContext.Current.Session[key];
            return session;
        }
        public void SetSession(object data, string key)
        {
            HttpContext.Current.Session.Add(key, data);
        }

    }
}