﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using System.Web.UI;
using System.Web.Caching;
using System.Web.SessionState;
using CaseManager.HttpServerTest;
using CaseManager.LawPavilionUpdates;
using CaseManager.Search;
using CaseManager.Util;
using Newtonsoft.Json.Linq;
using Logger = CaseManager.HttpServerTest.Logger.Logger;

//using CaseManager.LawPavilionUpdates;

namespace CaseManager
{
    public class Global : System.Web.HttpApplication
    {
        private static HttpRequest initialRequest;
        private static HttpResponse initialResponse;
        private static HttpCookie cookie;
        static Global()
        {
            initialRequest = HttpContext.Current.Request;
            initialResponse = HttpContext.Current.Response;
            cookie = new HttpCookie("IP");
        }

        //HttpServer.HttpServer.HttpServer _server = new HttpServer.HttpServer.HttpServer();
        protected void Application_Start(object sender, EventArgs e)
        {
            //store the Ip address



            //var DailyTime = "01:00:00";
            //var timeParts = DailyTime.Split(new char[1] { ':' });

            //var dateNow = DateTime.Now;
            //var date = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day,
            //           int.Parse(timeParts[0]), int.Parse(timeParts[1]), int.Parse(timeParts[2]));
            //TimeSpan ts;
            //if (date > dateNow)
            //    ts = date - dateNow;
            //else
            //{
            //    date = date.AddDays(1);
            //    ts = date - dateNow;
            //}

            //Updates updates = new Updates();
            //updates.all();
            //TaskEx.Delay(ts).ContinueWith((x) => updates.all());
            //Updates2 updates2 = new Updates2();
            //updates2.all();
            //TaskEx.Delay(ts).ContinueWith((x) => updates2.all());

            ///*************/

            //database ds = new database();
            //DataTable dt = ds.Value;
            //ds.createIndex(dt);

            /************/


            Temp.CreateProcessTable();
            Temp.CreateStageTable();
            Temp.CreateUserAdminData();
            CaseManager.Util.Temp.CreateUserDatasetData();



            #region

            //_server.Add(new HttpModleCustom());
            //_server.Start(System.Net.IPAddress.Any, 443);

            //Logger.writelog("FBN External Solicitors Server Started");

            //var timer = new Timer();

            //// Set the Interval to 10 minutes.
            //timer.Interval = 60000;
            //timer.Enabled = true;

            //timer.Elapsed += (send, ex) =>
            //{

            //    using (TcpClient tcpClient = new TcpClient("localhost", 443))
            //    {
            //        string clientIPAddress = ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString();
            //        NetworkStream ns = tcpClient.GetStream();
            //        StreamWriter sw = new StreamWriter(ns);
            //        StreamReader sr = new StreamReader(ns);

            //        sw.WriteLine("test message");

            //        sw.Flush();
            //        tcpClient.Close();
            //    }
            //};

            #endregion

            //run at a specfied time



        }



        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Logger.writeiplog(SystemInformation.GetIPAddress().ToString());
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {


        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
        }

    }
}