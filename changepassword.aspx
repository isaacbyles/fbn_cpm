﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="changepassword.aspx.cs" Inherits="CaseManager.changepassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link href="assets/css/bootstrap.min.css" rel="Stylesheet" />
    <link href="assets/css/metisMenu.min.css" rel="Stylesheet" />
    <link href="assets/css/sb-admin-2.css" rel="Stylesheet" />
    <title>Law Pavilion :: Case Manager</title>
</head>
<body>
    <form id="form1" runat="server">
     <div>
         <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
            
                <div class="login-panel panel panel-default">
                <img width="100%" src="assets/images/logo.png" />
                    <div class="panel-heading">
                        
                        <h3 class="panel-title">Change Password</h3>
                    </div>
                    <div class="panel-body">
                        <asp:Panel ID="status_message" CssClass="alert hidden" runat="server">
                            <asp:Label ID="alert_message" runat="server" Text="Label"></asp:Label>
                        </asp:Panel>
                            <div>
                           
                            <fieldset>
                                <div class="alert alert-info">
                                New Password must not be less than 6 characters
                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <asp:TextBox ID="new_password" CssClass="form-control" type="password" placeholder="New Password" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <asp:TextBox ID="confirm_password" CssClass="form-control" type="password" 
                                        placeholder="Confirm New Password" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                               
                                <!-- Change this to a button or input when using this as a form -->
                                <asp:Button ID="changepwdBtn" runat="server" Text="Continue" 
                                    CssClass="btn btn-lg btn-primary btn-block" onclick="changepwdBtn_Click" />
                            </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
     <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/metisMenu.min.js"></script>
    <script type="text/javascript" src="assets/js/sb-admin-2.js"></script>
    </form>
</body>
</html>
